/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Class for available actions for smoke suites on tabs.
 *
 * @author elopio
 *
 * @param <D>
 *          The data object class of the tab.
 * @param <T>
 *          The GUI class of the tab.
 */
public abstract class SmokeTabScript<D extends DataObject, T extends GeneratedTab<D>> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The application main page. */
  protected final MainPage mainPage;

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public SmokeTabScript(MainPage mainPage) {
    this.mainPage = mainPage;
  }

  /**
   * Select and return the tab.
   *
   * Before this, the window must be opened.
   *
   * @return the tab GUI object.
   */
  protected abstract T selectTab();

  /**
   * Get the data of the selected record in this tab.
   *
   * @return the record data.
   */
  public DataObject getData() {
    T tab = selectTab();
    return tab.getData();
  }

  /**
   * Get a data field of the selected record in this tab.
   *
   * @param field
   *          The name of the field to get.
   *
   * @return the value of the field.
   */
  public Object getData(String field) {
    T tab = selectTab();
    return tab.getData(field);
  }

  /**
   * Create a record.
   *
   * @param data
   *          The data of the record that will be created.
   */
  public void create(D data) {
    // If it is near to midnight, we wait 5 minutes to avoid change date false positives
    avoidDangerousTime();
    logger.info("Create record with data: {}.", data);
    T tab = selectTab();
    // TODO L1: Reduce the following static sleep. False positive for not loading Lines tab fields
    // was happening
    Sleep.trySleep();
    tab.createRecord(data);
  }

  /**
   * Create a record.
   *
   * @param data
   *          The data of the record that will be created.
   */
  public void createOnFormWithoutSaving(D data) {
    // If it is near to midnight, we wait 5 minutes to avoid change date false positives
    avoidDangerousTime();
    logger.info("Create record with data: {}. without save", data);
    T tab = selectTab();
    // TODO L1: Reduce the following static sleep. False positive for not loading Lines tab fields
    // was happening
    Sleep.trySleep();
    tab.createOnFormWithoutSaving(data);
  }

  /**
   * Sleep 5 minutes if currentTime is after 23:54:59 to avoid date change errors.
   */
  private void avoidDangerousTime() {
    if (OBDate.currentTimeIsAfterHHMMSS(23, 54, 59)) {
      logger.debug("It is: {} We will wait until next day to avoid date assert errors",
          OBDate::getSystemTime);
      Sleep.trySleep(301000);
    }
  }

  /**
   * Edit the selected record.
   *
   * @param data
   *          The data of the edited record.
   */
  public void edit(D data) {
    logger.info("Edit currently selected record with data: {}.", data);
    T tab = selectTab();
    tab.editRecord(data);
  }

  /**
   * Filter the records.
   *
   * @param data
   *          The data of the filter to apply.
   */
  public void filter(D data) {
    logger.info("Filter records using as filter: {}.", data);
    T tab = selectTab();
    tab.filter(data);
  }

  /**
   * Select a record.
   *
   * @param data
   *          The data of the record that will be selected.
   */
  public void select(D data) {
    logger.info("Select the record with data: {}.", data);
    T tab = selectTab();
    tab.select(data);
  }

  /**
   * Open the selected record.
   *
   */
  public void openSelectedRecord() {
    logger.info("Open the selected record.");
    T tab = selectTab();
    tab.open();
  }

  /**
   * Sort Ascending using the context menu of the column header button of the given column in the
   * DataObject
   *
   * @param data
   *          The data with the name (key) of the column that will be sorted
   */
  public void sortAscending(D data) {
    logger.info("Sort Ascending using using the data: {}.", data);
    T tab = selectTab();
    tab.sortAscending(data);
  }

  /**
   * Sort Descending using the context menu of the column header button of the given column in the
   * DataObject
   *
   * @param data
   *          The data with the name (key) of the column that will be sorted
   */
  public void sortDescending(D data) {
    logger.info("Sort Descending using using the data: {}.", data);
    T tab = selectTab();
    tab.sortDescending(data);
  }

  /**
   * [DEBUG] Method to return a tab
   */
  public GeneratedTab<D> getTab() {
    logger.info("Select the tab.");
    T tab = selectTab();
    return tab;
  }

  /**
   * Verify the data of the selected record.
   *
   * One record must be selected first.
   *
   * @param data
   *          The expected data of the record.
   */
  public void assertData(D data) {
    logger.info("Assert that the data of the selected record record matches: {}.", data);
    T tab = selectTab();
    tab.assertData(data);
  }

  /**
   * Assert display logic visibility
   *
   * @param visibleDisplayLogic
   *          DataOject with the fields whose visibility has to be asserted.
   */
  public void assertDisplayLogicVisible(DataObject visibleDisplayLogic) {
    logger.trace("Asserting display logic visibility.");
    T tab = selectTab();
    tab.assertDisplayLogicVisible(visibleDisplayLogic);
  }

  /**
   * Assert display logic enable status
   *
   * @param enabledDisplayLogic
   *          DataOject with the fields whose enable status has to be asserted.
   */
  public void assertDisplayLogicEnabled(DataObject enabledDisplayLogic) {
    logger.trace("Asserting display logic visibility.");
    T tab = selectTab();
    tab.assertDisplayLogicEnabled(enabledDisplayLogic);
  }

  /**
   * Assert display logic expanded status
   *
   * @param expandedDisplayLogic
   *          DataOject with the fields whose expanded status has to be asserted.
   */
  public void assertDisplayLogicExpanded(DataObject expandedDisplayLogic) {
    logger.trace("Asserting display logic visibility.");
    T tab = selectTab();
    tab.assertDisplayLogicExpanded(expandedDisplayLogic);
  }

  /**
   * Get the number of records in the tab.
   *
   * @return The number of records in the tab.
   */
  public int getRecordCount() {
    T tab = selectTab();
    return tab.getRecordCount();
  }

  /**
   * Verify the number of records in the tab.
   *
   * @param expectedCount
   *          The expected number of records.
   */
  public void assertCount(int expectedCount) {
    logger.debug("Assert that the number of records is {}.", expectedCount);
    T tab = selectTab();
    // TODO L: Reduce this sleep if it is feasible. Right now, 3000ms were not enough
    Sleep.trySleep(8000);
    tab.assertCount(expectedCount);
  }

  /**
   * Assert that the record was saved.
   */
  public void assertSaved() {
    T tab = selectTab();
    tab.assertSaved();
  }

  /**
   * Assert that the process completed successfully.
   */
  public void assertProcessCompletedSuccessfully() {
    T tab = selectTab();
    tab.assertProcessCompletedSuccessfully();
  }

  /**
   * Assert that the process completed successfully.
   *
   * TODO see issue 17291 - https://issues.openbravo.com/view.php?id=17291.
   */
  public void assertProcessCompletedSuccessfully2() {
    T tab = selectTab();
    tab.assertProcessCompletedSuccessfully2();
  }

  /**
   * Assert that the process completed successfully.
   *
   * TODO see issue 17291 - https://issues.openbravo.com/view.php?id=17291.
   *
   * @param message
   *          The expected message body.
   */
  public void assertProcessCompletedSuccessfully2(String message) {
    T tab = selectTab();
    tab.assertProcessCompletedSuccessfully2(message);
  }

  /**
   * Assert the process completed successfully message, using regular expression to match the body.
   *
   * @param message
   *          A regular expression for the expected message body.
   * @return a list with all the capturing groups of the regular expression.
   */
  public List<String> assertProcessCompletedSuccessfullyContentMatch(String message) {
    T tab = selectTab();
    return tab.assertProcessCompletedSuccessfullyContentMatch(message);
  }

  public void cancelOnGrid() {
    T tab = selectTab();
    tab.cancelOnGrid();
  }

  public void closeForm() {
    T tab = selectTab();
    tab.closeForm();
  }

  public void selectWithoutFiltering(int row) {
    T tab = selectTab();
    if (tab.isOnFormView()) {
      tab.closeForm();
    }
    tab.selectWithoutFiltering(row);
  }

  public void save() {
    T tab = selectTab();
    tab.saveEdition();
  }

}
