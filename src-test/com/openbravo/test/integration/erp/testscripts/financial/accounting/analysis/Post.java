/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Pablo Luján <plu@openbravo.com>,
 *  David Miguélez <david.miguelez@openbravo.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis;

import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the Post window.
 *
 * @author elopio
 */
public class Post {

  /** The application main page. */
  protected final MainPage mainPage;

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public Post(MainPage mainPage) {
    this.mainPage = mainPage;
  }

  /**
   * Open the Session Preferences window.
   *
   * @return this test scrip.
   */
  public Post open() {
    PostWindow window = new PostWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the Post tab.
   *
   * @return the Post tab GUI object.
   */
  protected PostWindow selectTab() {
    PostWindow window = (PostWindow) mainPage.getView(PostWindow.TITLE);
    mainPage.getTabSetMain().selectTab(PostWindow.TITLE);
    window.waitForFrame();
    return window;
  }

  protected PostWindow getTab() {
    PostWindow window = (PostWindow) mainPage.getView(PostWindow.TITLE);
    return window;
  }

  /**
   * Assert the number of lines in a journal.
   *
   * @param count
   *          The expected number of lines.
   */
  public void assertJournalLinesCount(int count) {
    getTab().assertJournalLinesCount(count);
  }

  /**
   * Assert data of a general ledger journal line.
   *
   * @param index
   *          The index of the line.
   * @param accountNumber
   *          The number of the account.
   * @param accountName
   *          The name of the account.
   * @param credit
   *          The amount credited.
   * @param debit
   *          The amount of the debit.
   */
  public void assertJournalLine(int index, String accountNumber, String accountName, String credit,
      String debit) {
    getTab().assertJournalLine(index, accountNumber, accountName, credit, debit);
  }

  /**
   * Assert data of an array of general ledger journal lines.
   *
   * @param journalEntryLines
   *          Array of the general ledger journal lines.
   */
  public void assertJournalLines2(String[][] journalEntryLines) {
    getTab().assertJournalLines2(journalEntryLines);
  }
}
