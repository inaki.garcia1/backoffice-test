/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.procurement.transactions.goodsreceipt;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes actions on user Create Goods Receipt Lines From Pop Up
 *
 * @author elopio
 *
 */
public class CreateGoodsReceiptLinesFrom extends OBClassicPopUp {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Identifier of the Order combobox */
  @SuppressWarnings("unused")
  private static final String COMBO_BOX_ORDER = "inpPurchaseOrder";
  /** Identifier of the Invoice combobox */
  @SuppressWarnings("unused")
  private static final String COMBO_BOX_INVOICE = "inpInvoice";
  /** Locator of the lines check box */
  @SuppressWarnings("unused")
  private static final String CHECK_BOX_LINES = "xpath=//input[@name='inpId']";

  /**
   * Class constructor
   *
   * @param registryKey
   *          The key of the object in the test registry array
   */
  public CreateGoodsReceiptLinesFrom(String registryKey) {
    super(TestRegistry.getObjectString(registryKey));
  }

  /**
   * Create lines from an order
   *
   * @param locatorAlias
   *          the alias of warehouse locator
   * @param order
   *          the purchase order
   */
  public void createLinesFrom(String locatorAlias, String order) {

    // selectPopUpWithOKButton();
    // SeleniumSingleton.INSTANCE.select(COMBO_BOX_ORDER, "label="
    // + SeleniumSingleton.INSTANCE.getSelectOptions(COMBO_BOX_ORDER)[1]);
    //
    // Sleep.trySleep(3000);
    // SeleniumSingleton.INSTANCE.click(LocatorSelectorPopUp.BUTTON_LOCATOR);
    //
    // final LocatorSelectorPopUp locatorSelector = new LocatorSelectorPopUp();
    // locatorSelector.selectLocator(locatorAlias);
    //
    // SeleniumSingleton.INSTANCE.selectWindow("");
    // SeleniumSingleton.INSTANCE.selectFrame("process");
    // SeleniumSingleton.INSTANCE.selectFrame("frameButton");
    //
    // // XXX find a better way to do this
    // if (!SeleniumSingleton.INSTANCE.isChecked(CHECK_BOX_LINES)) {
    // SeleniumSingleton.INSTANCE.click(CHECK_BOX_LINES);
    // }
    // clickOK();
  }

  /**
   * Select a Pop Up with ok button. This function waits until the ok button is present.
   */
  @Override
  public void selectPopUpWithOKButton() {
    Sleep.execSingletonActionWithFWait(
        SeleniumSingleton.INSTANCE.switchTo().frame("process"), NoSuchFrameException.class, 10000);
    SeleniumSingleton.INSTANCE.switchTo().frame("frameButton");
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting for button OK to be loaded.");
        WebElement element;
        try {
          element = SeleniumSingleton.INSTANCE.findElementById(BUTTON_OK);
        } catch (NoSuchElementException exception) {
          return false;
        }
        return element.isDisplayed();
      }
    });
  }
}
