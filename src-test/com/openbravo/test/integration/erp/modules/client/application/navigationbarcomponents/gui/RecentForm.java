/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui;

import java.util.NoSuchElementException;

import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Canvas;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions on OpenbravoERP recent form built upon SmartClient user interface library.
 *
 * @author elopio
 *
 */
public class RecentForm extends Canvas {

  /**
   * Formatting string used to get the number of items in the form. The parameter is the object
   * string of the form.
   */
  private static final String FORMAT_SMART_CLIENT_GET_ITEMS_COUNT = "%s.getMembers().size()";
  /**
   * Formatting string used to get an item in the form. The first parameter is the object string of
   * the form. The second is the item index.
   */
  private static final String FORMAT_SMART_CLIENT_GET_ITEM_BY_INDEX = "%s.getMembers()[%d]";

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   */
  public RecentForm(String objectString) {
    super(objectString);
  }

  /**
   * Get the index of a recent item.
   *
   * @param text
   *          The text of the item.
   * @return the index of the item.
   */
  private int getRecentItemIndex(String text) {
    long size = (Long) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_ITEMS_COUNT, objectString));
    for (int index = 0; index < size; index++) {
      Button itemButton = new Button(
          String.format(FORMAT_SMART_CLIENT_GET_ITEM_BY_INDEX, objectString, index));
      String currentItemText = itemButton.getTitle();
      if (currentItemText.equals(text)) {
        return index;
      }
    }
    throw new NoSuchElementException();
  }

  /**
   * Select a recent item.
   *
   * @param text
   *          The text of the recent item.
   */
  public void selectRecentItem(String text) {
    int index = getRecentItemIndex(text);
    Button itemButton = new Button(
        String.format(FORMAT_SMART_CLIENT_GET_ITEM_BY_INDEX, objectString, index));
    itemButton.smartClientClick();
  }
}
