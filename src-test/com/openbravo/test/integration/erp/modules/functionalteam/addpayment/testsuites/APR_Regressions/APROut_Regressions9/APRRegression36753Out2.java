/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions9;

import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.AddPaymentGrid;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount.PaymentMethod;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 36753
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class APRRegression36753Out2 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  AccountData accountData;
  PaymentMethodData paymentMethodData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoice1LineData;
  PurchaseInvoiceLinesData purchaseInvoice1LineVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoice1HeaderVerificationData;
  PaymentPlanData invoice1PendingPaymentOutPlanData;
  PurchaseInvoiceLinesData purchaseInvoice2LineData;
  PurchaseInvoiceLinesData purchaseInvoice2LineVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoice2HeaderVerificationData;
  PaymentPlanData invoice2PendingPaymentOutPlanData;
  PaymentOutHeaderData paymentInHeaderData;
  PaymentOutHeaderData paymentInHeaderVerificationData;
  PaymentOutLinesData paymentInLines1VerificationData;
  PaymentOutLinesData paymentInLines2VerificationData;
  PaymentOutHeaderData paymentInDetailsHeaderData;
  PaymentPlanData invoice1PaidPaymentOutPlanData;
  PaymentDetailsData invoice1PaidPaymentOutDetailsData;
  PaymentPlanData invoice2PaidPaymentOutPlanData;
  PaymentDetailsData invoice2PaidPaymentOutDetailsData;
  PaymentOutHeaderData paymentReactivatedInHeaderVerificationData;
  PaymentOutHeaderData paymentInExecutedHeaderData;
  String[][] journalEntryLines1;
  String[][] journalEntryLines2;
  PaymentPlanData invoice1ExecutedPaymentOutPlanData;
  PaymentDetailsData invoice1ExecutedPaymentOutDetailsData;
  String[][] journalEntryLines3;
  String[][] journalEntryLines4;
  PaymentPlanData invoice2ExecutedPaymentOutPlanData;
  PaymentDetailsData invoice2ExecutedPaymentOutDetailsData;
  String[][] journalEntryLines5;
  String[][] journalEntryLines6;

  /**
   * Class constructor.
   *
   */
  public APRRegression36753Out2(AccountData accountData, PaymentMethodData paymentMethodData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoice1LineData,
      PurchaseInvoiceLinesData purchaseInvoice1LineVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoice1HeaderVerificationData,
      PaymentPlanData invoice1PendingPaymentOutPlanData,
      PurchaseInvoiceLinesData purchaseInvoice2LineData,
      PurchaseInvoiceLinesData purchaseInvoice2LineVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoice2HeaderVerificationData,
      PaymentPlanData invoice2PendingPaymentOutPlanData, PaymentOutHeaderData paymentInHeaderData,
      PaymentOutHeaderData paymentInHeaderVerificationData,
      PaymentOutLinesData paymentInLines1VerificationData,
      PaymentOutLinesData paymentInLines2VerificationData,
      PaymentOutHeaderData paymentInDetailsHeaderData,
      PaymentPlanData invoice1PaidPaymentOutPlanData,
      PaymentDetailsData invoice1PaidPaymentOutDetailsData,
      PaymentPlanData invoice2PaidPaymentOutPlanData,
      PaymentDetailsData invoice2PaidPaymentOutDetailsData,
      PaymentOutHeaderData paymentReactivatedInHeaderVerificationData,
      String[][] journalEntryLines1, String[][] journalEntryLines2,
      PaymentPlanData invoice1ExecutedPaymentOutPlanData,
      PaymentDetailsData invoice1ExecutedPaymentOutDetailsData, String[][] journalEntryLines3,
      String[][] journalEntryLines4, PaymentPlanData invoice2ExecutedPaymentOutPlanData,
      PaymentDetailsData invoice2ExecutedPaymentOutDetailsData, String[][] journalEntryLines5,
      String[][] journalEntryLines6, PaymentOutHeaderData paymentInExecutedHeaderData) {
    this.accountData = accountData;
    this.paymentMethodData = paymentMethodData;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoice1LineData = purchaseInvoice1LineData;
    this.purchaseInvoice1LineVerificationData = purchaseInvoice1LineVerificationData;
    this.completedPurchaseInvoice1HeaderVerificationData = completedPurchaseInvoice1HeaderVerificationData;
    this.invoice1PendingPaymentOutPlanData = invoice1PendingPaymentOutPlanData;
    this.purchaseInvoice2LineData = purchaseInvoice2LineData;
    this.purchaseInvoice2LineVerificationData = purchaseInvoice2LineVerificationData;
    this.completedPurchaseInvoice2HeaderVerificationData = completedPurchaseInvoice2HeaderVerificationData;
    this.invoice2PendingPaymentOutPlanData = invoice2PendingPaymentOutPlanData;
    this.paymentInHeaderData = paymentInHeaderData;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    this.paymentInLines1VerificationData = paymentInLines1VerificationData;
    this.paymentInLines2VerificationData = paymentInLines2VerificationData;
    this.paymentInDetailsHeaderData = paymentInDetailsHeaderData;
    this.invoice1PaidPaymentOutPlanData = invoice1PaidPaymentOutPlanData;
    this.invoice1PaidPaymentOutDetailsData = invoice1PaidPaymentOutDetailsData;
    this.invoice2PaidPaymentOutPlanData = invoice2PaidPaymentOutPlanData;
    this.invoice2PaidPaymentOutDetailsData = invoice2PaidPaymentOutDetailsData;
    this.paymentReactivatedInHeaderVerificationData = paymentReactivatedInHeaderVerificationData;
    this.paymentInExecutedHeaderData = paymentInExecutedHeaderData;
    this.journalEntryLines1 = journalEntryLines1;
    this.journalEntryLines2 = journalEntryLines2;
    this.invoice1ExecutedPaymentOutPlanData = invoice1ExecutedPaymentOutPlanData;
    this.invoice1ExecutedPaymentOutDetailsData = invoice1ExecutedPaymentOutDetailsData;
    this.journalEntryLines3 = journalEntryLines3;
    this.journalEntryLines4 = journalEntryLines4;
    this.invoice2ExecutedPaymentOutPlanData = invoice2ExecutedPaymentOutPlanData;
    this.invoice2ExecutedPaymentOutDetailsData = invoice2ExecutedPaymentOutDetailsData;
    this.journalEntryLines5 = journalEntryLines5;
    this.journalEntryLines6 = journalEntryLines6;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> PurchaseOrderValues() {
    Object[][] data = new Object[][] { {

        new AccountData.Builder().name("Accounting Documents EURO").build(),

        new PaymentMethodData.Builder().automaticDeposit(false)
            .payoutExecutionType("Automatic")
            .payoutExecutionProcess("Simple Execution Process")
            .payoutDeferred(true)
            .build(),

        new PurchaseInvoiceHeaderData.Builder().organization("USA")
            .transactionDocument("AP Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentTerms("90 days")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().transactionDocument("AP Invoice")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .priceList("Purchase")
            .paymentTerms("90 days")
            .build(),

        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("RMC")
                .priceListVersionName("Purchase")
                .build())
            .invoicedQuantity("10")
            .tax("VAT 10% USA")
            .build(),

        new PurchaseInvoiceLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("20.00")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("22.00")
            .summedLineAmount("20.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new PaymentPlanData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("22.00")
            .paid("0.00")
            .outstanding("22.00")
            .currency("EUR")
            .daysOverdue("0")
            .numberOfPayments("0")
            .build(),

        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("RMA")
                .priceListVersionName("Purchase")
                .build())
            .invoicedQuantity("15")
            .tax("VAT 10% USA")
            .build(),

        new PurchaseInvoiceLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("30.00")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("33.00")
            .summedLineAmount("30.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new PaymentPlanData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("33.00")
            .paid("0.00")
            .outstanding("33.00")
            .currency("EUR")
            .daysOverdue("0")
            .numberOfPayments("0")
            .build(),

        new PaymentOutHeaderData.Builder().organization("USA")
            .documentType("AP Payment")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .account("Accounting Documents EURO - EUR")
            .build(),

        new PaymentOutHeaderData.Builder().organization("USA")
            .documentType("AP Payment")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("0.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Awaiting Payment")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new PaymentOutLinesData.Builder().invoiceAmount("22.00")
            .expected("22.00")
            .paid("22.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PaymentOutLinesData.Builder().invoiceAmount("33.00")
            .expected("33.00")
            .paid("33.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PaymentOutHeaderData.Builder().organization("USA")
            .documentType("AP Payment")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("55.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Awaiting Execution")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new PaymentPlanData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("22.00")
            .paid("0.00")
            .outstanding("22.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new PaymentDetailsData.Builder().paid("22.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("22.00")
            .account("Accounting Documents EURO - EUR")
            .status("Awaiting Execution")
            .build(),

        new PaymentPlanData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("33.00")
            .paid("0.00")
            .outstanding("33.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new PaymentDetailsData.Builder().paid("33.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("33.00")
            .account("Accounting Documents EURO - EUR")
            .status("Awaiting Execution")
            .build(),

        new PaymentOutHeaderData.Builder().organization("USA")
            .documentType("AP Payment")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("0.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Awaiting Payment")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new String[][] { { "40000", "Proveedores (euros)", "55.00", "" },
            { "40100", "Proveedores efectos comerciales a pagar", "", "55.00" } },
        new String[][] { { "21100", "Accounts payable", "137.50", "" },
            { "11300", "Bank in transit", "", "137.50" } },

        new PaymentPlanData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("22.00")
            .paid("22.00")
            .outstanding("0.00")
            .currency("EUR")
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new PaymentDetailsData.Builder().paid("22.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("22.00")
            .account("Accounting Documents EURO - EUR")
            .status("Payment Made")
            .build(),

        new String[][] { { "60000", "Compras de mercaderías", "20.00", "" },
            { "47200", "Hacienda Pública IVA soportado", "2.00", "" },
            { "40000", "Proveedores (euros)", "", "22.00" }, },

        new String[][] { { "51200", "Service costs", "50.00", "" },
            { "11600", "Tax Receivables", "5.00", "" },
            { "21100", "Accounts payable", "", "55.00" }, },

        new PaymentPlanData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("33.00")
            .paid("33.00")
            .outstanding("0.00")
            .currency("EUR")
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new PaymentDetailsData.Builder().paid("33.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("33.00")
            .account("Accounting Documents EURO - EUR")
            .status("Payment Made")
            .build(),

        new String[][] { { "60000", "Compras de mercaderías", "30.00", "" },
            { "47200", "Hacienda Pública IVA soportado", "3.00", "" },
            { "40000", "Proveedores (euros)", "", "33.00" }, },

        new String[][] { { "51200", "Service costs", "75.00", "" },
            { "11600", "Tax Receivables", "7.50", "" },
            { "21100", "Accounts payable", "", "82.50" }, },

        new PaymentOutHeaderData.Builder().organization("USA")
            .documentType("AP Payment")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("55.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Payment Made")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 36753 Paying two invoice from payment out
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression36753Out2Test() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression36753Out2] Test regression 36753 Paying two invoice from payment out**");

    // Change payment method configuration
    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountData);
    PaymentMethod paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod
        .select(new PaymentMethodData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build());
    paymentMethod.edit(paymentMethodData);

    // // Get Business partner current balance
    // BusinessPartnerWindow businessPartnerWindow = new BusinessPartnerWindow();
    // mainPage.openView(businessPartnerWindow);
    // mainPage.waitForDataToLoad();
    //
    // BusinessPartnerTab businessPartnerTab = businessPartnerWindow.selectBusinessPartnerTab();
    // businessPartnerTab.select(new
    // BusinessPartnerData.Builder().name("Vendor A").searchKey("CUSA")
    // .build());
    // String currentBalance = (String) businessPartnerTab.getData("creditUsed");
    // currentBalance = currentBalance.replace(",", "");
    // BigDecimal bpCurrentBalance = new BigDecimal(currentBalance);

    // Create a Purchase invoice
    PurchaseInvoice invoice = new PurchaseInvoice(mainPage).open();
    invoice.create(purchaseInvoiceHeaderData);
    invoice.assertSaved();
    invoice.assertData(purchaseInvoiceHeaderVerificationData);

    PurchaseInvoice.Lines invoiceLines = invoice.new Lines(mainPage);
    invoiceLines.create(purchaseInvoice1LineData);
    invoiceLines.assertData(purchaseInvoice1LineVerificationData);

    invoice.complete();
    invoice.assertProcessCompletedSuccessfully2();
    invoice.assertData(completedPurchaseInvoice1HeaderVerificationData);
    String invoice1No = (String) invoice.getData("documentNo");

    // Check invoice1 payment plan
    PurchaseInvoice.PaymentOutPlan invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoice1PendingPaymentOutPlanData);

    // Create another invoice
    invoice = new PurchaseInvoice(mainPage).open();
    invoice.create(purchaseInvoiceHeaderData);
    invoice.assertSaved();
    invoice.assertData(purchaseInvoiceHeaderVerificationData);

    invoiceLines = invoice.new Lines(mainPage);
    invoiceLines.create(purchaseInvoice2LineData);
    invoiceLines.assertData(purchaseInvoice2LineVerificationData);

    invoice.complete();
    invoice.assertProcessCompletedSuccessfully2();
    invoice.assertData(completedPurchaseInvoice2HeaderVerificationData);

    // Check invoice2 payment plan
    invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoice2PendingPaymentOutPlanData);

    String invoice2No = (String) invoice.getData("documentNo");

    // // Check Business Partner current balance
    // bpCurrentBalance = bpCurrentBalance.add(new BigDecimal(
    // (String) completedPurchaseInvoice1HeaderVerificationData.getDataField("grandTotalAmount")));
    // bpCurrentBalance = bpCurrentBalance.add(new BigDecimal(
    // (String) completedPurchaseInvoice2HeaderVerificationData.getDataField("grandTotalAmount")));
    // DecimalFormat format = new DecimalFormat("#,##0.00");
    // businessPartnerWindow = new BusinessPartnerWindow();
    // mainPage.openView(businessPartnerWindow);
    // mainPage.waitForDataToLoad();
    //
    // businessPartnerTab = businessPartnerWindow.selectBusinessPartnerTab();
    // businessPartnerTab.select(new
    // BusinessPartnerData.Builder().name("Vendor A").searchKey("CUSA")
    // .build());
    // businessPartnerTab.assertData(new BusinessPartnerData.Builder().creditUsed(
    // format.format(bpCurrentBalance)).build());

    // Create a payment out
    PaymentOut payment = new PaymentOut(mainPage).open();
    payment.create(paymentInHeaderData);
    payment.assertData(paymentInHeaderVerificationData);

    addPaymentOutDetails(payment.addDetailsOpen(), invoice1No, invoice2No);

    PaymentOut.Lines paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(2);
    paymentInLines
        .select(new PaymentOutLinesData.Builder().paid("22.00").invoiceno(invoice1No).build());
    paymentInLines.assertData((PaymentOutLinesData) paymentInLines1VerificationData
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoice1No));

    paymentInLines
        .select(new PaymentOutLinesData.Builder().paid("33.00").invoiceno(invoice2No).build());
    paymentInLines.assertData((PaymentOutLinesData) paymentInLines2VerificationData
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoice2No));

    payment.assertData(paymentInDetailsHeaderData);
    String paymentNo = (String) payment.getData("documentNo");

    // Check invoice1 payment plan
    invoice = new PurchaseInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    invoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoice1No).build());

    invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoice1PaidPaymentOutPlanData);

    PurchaseInvoice.PaymentOutPlan.PaymentOutDetails invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(
        mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoice1PaidPaymentOutDetailsData);

    // Check invoice2 payment plan
    invoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoice2No).build());

    invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoice2PaidPaymentOutPlanData);

    invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoice2PaidPaymentOutDetailsData);

    // Reactivate and delete Payment out lines
    payment = new PaymentOut(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentOutHeaderData.Builder().documentNo(paymentNo).build());
    payment.reactivate("Reactivate and Delete Lines");
    payment.assertProcessCompletedSuccessfully2();
    payment.assertData(paymentReactivatedInHeaderVerificationData);

    // Check invoice1 payment plan
    invoice = new PurchaseInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    invoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoice1No).build());
    invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoice1PendingPaymentOutPlanData);

    // Check invoice2 payment plan
    invoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoice2No).build());
    invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoice2PendingPaymentOutPlanData);

    // Add Payment details
    payment = new PaymentOut(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentOutHeaderData.Builder().documentNo(paymentNo).build());

    addPaymentOutDetails(payment.addDetailsOpen(), invoice1No, invoice2No);

    // Check payment and lines
    payment.assertData(paymentInDetailsHeaderData);

    paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(2);
    paymentInLines
        .select(new PaymentOutLinesData.Builder().paid("22.00").invoiceno(invoice1No).build());
    paymentInLines.assertData((PaymentOutLinesData) paymentInLines1VerificationData
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoice1No));

    paymentInLines
        .select(new PaymentOutLinesData.Builder().paid("33.00").invoiceno(invoice2No).build());
    paymentInLines.assertData((PaymentOutLinesData) paymentInLines2VerificationData
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoice2No));

    // Check invoice1 payment plan
    invoice = new PurchaseInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    invoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoice1No).build());
    invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoice1PaidPaymentOutPlanData);

    // Check invoice2 payment plan
    invoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoice2No).build());
    invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoice2PaidPaymentOutPlanData);

    // Execute Payment out and check
    payment = new PaymentOut(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentOutHeaderData.Builder().documentNo(paymentNo).build());
    payment.open();
    payment.execute();
    payment.assertData(paymentInExecutedHeaderData);

    // Check payment lines
    paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(2);
    paymentInLines
        .select(new PaymentOutLinesData.Builder().paid("22.00").invoiceno(invoice1No).build());
    paymentInLines.assertData((PaymentOutLinesData) paymentInLines1VerificationData
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoice1No));

    paymentInLines
        .select(new PaymentOutLinesData.Builder().paid("33.00").invoiceno(invoice2No).build());
    paymentInLines.assertData((PaymentOutLinesData) paymentInLines2VerificationData
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoice2No));

    // Post Payment out and check
    payment.open();
    payment.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines1.length);
    post.assertJournalLines2(journalEntryLines1);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines2.length);
    post.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Check invoice1 payment plan
    invoice = new PurchaseInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    invoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoice1No).build());

    invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoice1ExecutedPaymentOutPlanData);

    invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoice1ExecutedPaymentOutDetailsData);

    invoice.open();
    invoice.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines3.length);
    post.assertJournalLines2(journalEntryLines3);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines4.length);
    post.assertJournalLines2(journalEntryLines4);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Check invoice2 payment plan
    invoice = new PurchaseInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    invoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoice2No).build());

    invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoice2ExecutedPaymentOutPlanData);

    invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoice2ExecutedPaymentOutDetailsData);

    // Post invoice2 and check post
    invoice.open();
    invoice.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines5.length);
    post.assertJournalLines2(journalEntryLines5);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines6.length);
    post.assertJournalLines2(journalEntryLines6);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Restore the payment method configuration
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountData);
    paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod
        .select(new PaymentMethodData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build());
    paymentMethod.edit(new PaymentMethodData.Builder().automaticDeposit(false)
        .payoutExecutionType("Manual")
        .build());

    logger.info(
        "** End of test case [APRRegression36753Out2] Test regression 36753 Paying two invoice from payment out**");
  }

  private void addPaymentOutDetails(AddPaymentProcess paymentDetails, String invoice1No,
      String invoice2No) {
    paymentDetails.setParameterValue("transaction_type", "Invoices");

    AddPaymentGrid orderInvoiceGrid = paymentDetails.getOrderInvoiceGrid();
    orderInvoiceGrid.clearFilters();
    orderInvoiceGrid.waitForDataToLoad();
    orderInvoiceGrid.unselectAll();
    orderInvoiceGrid.waitForDataToLoad();
    @SuppressWarnings("unchecked")
    List<Map<String, Object>> rows = (List<Map<String, Object>>) orderInvoiceGrid.getRows();
    int rowNumber = 0;
    for (Map<String, Object> row : rows) {
      if (StringUtils.equals((String) row.get("invoiceNo"), invoice1No)
          || StringUtils.equals((String) row.get("invoiceNo"), invoice2No)) {
        orderInvoiceGrid.selectRecord(rowNumber);
      }
      rowNumber++;
    }
    @SuppressWarnings("unchecked")
    List<Map<String, Object>> selectedRows = (List<Map<String, Object>>) orderInvoiceGrid
        .getSelectedRows();
    assertTrue("There must be two rows selected", selectedRows.size() == 2);
    paymentDetails.collapseOrder();
    paymentDetails.process("Process Made Payment(s) and Withdrawal");
  }
}
