/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.enterprise.initialorganizationsetup;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.data.generalsetup.AccountingDimensions;
import com.openbravo.test.integration.erp.data.generalsetup.ModuleReferenceData;
import com.openbravo.test.integration.erp.data.generalsetup.enterprise.initialorganizationsetup.InitialOrganizationSetupData;
import com.openbravo.test.integration.erp.gui.general.InitialSetup;
import com.openbravo.test.integration.erp.gui.popups.AddressSelectorPopUp;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;
import com.openbravo.test.integration.selenium.Attribute;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on OpenbravoERP Initial Organization Setup process.
 *
 * @author elopio
 *
 */
public class InitialOrganizationSetupProcess extends InitialSetup {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();
  /** The window title. */
  private static final String TITLE = "Initial organization setup";

  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.GENERAL_SETUP, Menu.ENTERPRISE,
      Menu.INITIAL_ORGANIZATION_SETUP };

  /* Client elements */
  /** Identifier of the organization text field. */
  private static String TEXT_FIELD_ORGANIZATION = "paramOrganization";
  /** Identifier of the organization user name text field. */
  private static String TEXT_FIELD_ORGANIZATION_USER_NAME = "paramOrgClient";
  /** Identifier of the password text field. */
  private static String TEXT_FIELD_PASSWORD = "inpPassword";
  /** Identifier of the password text field. */
  private static String TEXT_FIELD_CONFIRM_PASSWORD = "inpConfirmPassword";
  /** Identifier of the organization type combo box. */
  private static String COMBO_BOX_ORGANIZATION_TYPE = "inpOrgType";
  /** Identifier of the parent organization combo box. */
  private static String COMBO_BOX_PARENT_ORGANIZATION = "inpParentOrg";

  /**
   * Formatting string used to show module data next to the checkbox. The first parameter is the
   * module description. The second is the version. The third is the language.
   */
  private static final String FORMAT_LABEL_REFERENCE_DATA = "%s - %s - %s";
  /** The maximum of characters in the Reference Data label. */
  private static final int MAX_LABEL_LENGTH_REFERENCE_DATA = 90;
  /** Formatting string used to locate the checkboxes in reference data. */
  private static String FORMAT_CHECK_BOX_REFERENCE_DATA = "(//div[@id='name' and @class='Tree_Text_Title' and text() = '%s']/../..)//input[@type = 'checkbox']";

  // TODO file an issue. Address selector should be named "SELECTOR" as is
  // done elsewhere.
  /** Name of the Address Selector pop up. */
  protected String POPUP_NAME_ADDRESS_SELECTOR = "SELECTOR_LOCATION";

  /**
   * Class constructor.
   *
   */
  public InitialOrganizationSetupProcess() {
    super(TITLE, MENU_PATH);
  }

  /**
   * Set up the initial organization.
   *
   * @param initialOrganizationSetupData
   *          The initial organization setup data.
   */
  public void setUp(InitialOrganizationSetupData initialOrganizationSetupData) {
    if (initialOrganizationSetupData.getOrganization() != null) {
      // FIXME: This clear is to workaround issue
      // https://issues.openbravo.com/view.php?id=16730
      // TODO L3: Remove the following try catch once tests are stable due to Gecko insert
      try {
        SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_ORGANIZATION).clear();
      } catch (StaleElementReferenceException sere) {
        logger.trace(
            "StaleElementReferenceException was thrown and caught. Sleep and try clearing again.");
        Sleep.trySleep(7500);
        try {
          SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_ORGANIZATION).clear();
        } catch (StaleElementReferenceException sere2) {
          Sleep.trySleep();
          logger.trace("Two StaleElementReferenceException in a row. Avoiding to clear.");
        }
      } catch (InvalidElementStateException iese) {
        Sleep.trySleep();
        SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_ORGANIZATION).clear();
      }
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_ORGANIZATION)
          .sendKeys(initialOrganizationSetupData.getOrganization());
    }
    if (initialOrganizationSetupData.getOrganizationUserName() != null) {
      // FIXME: This clear is to workaround issue
      // https://issues.openbravo.com/view.php?id=16730
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_ORGANIZATION_USER_NAME).clear();
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_ORGANIZATION_USER_NAME)
          .sendKeys(initialOrganizationSetupData.getOrganizationUserName());
    }
    if (initialOrganizationSetupData.getPassword() != null) {
      // FIXME: This clear is to workaround issue
      // https://issues.openbravo.com/view.php?id=16730
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_PASSWORD).clear();
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_PASSWORD)
          .sendKeys(initialOrganizationSetupData.getPassword());
    }
    if (initialOrganizationSetupData.getPasswordConfirmation() != null) {
      // FIXME: This clear is to workaround issue
      // https://issues.openbravo.com/view.php?id=16730
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_CONFIRM_PASSWORD).clear();
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_CONFIRM_PASSWORD)
          .sendKeys(initialOrganizationSetupData.getPasswordConfirmation());
    }
    if (initialOrganizationSetupData.getOrganizationType() != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_ORGANIZATION_TYPE))
          .selectByVisibleText(initialOrganizationSetupData.getOrganizationType());
    }
    if (initialOrganizationSetupData.getParentOrganization() != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_PARENT_ORGANIZATION))
          .selectByVisibleText(initialOrganizationSetupData.getParentOrganization());
    }
    if (initialOrganizationSetupData.getLocation() != null) {
      String windowHandle = SeleniumSingleton.INSTANCE.getWindowHandle();
      SeleniumSingleton.INSTANCE.findElementById(AddressSelectorPopUp.BUTTON_ADDRESS).click();
      // TODO L0: Reduce sleep once it is stable
      Sleep.trySleep(3500);
      final AddressSelectorPopUp addressSelector = new AddressSelectorPopUp(
          POPUP_NAME_ADDRESS_SELECTOR);
      addressSelector.selectAddress(initialOrganizationSetupData.getLocation());
      SeleniumSingleton.INSTANCE.switchTo().window(windowHandle);
      SeleniumSingleton.INSTANCE.switchTo().defaultContent();
      // TODO L0: Remove static sleep
      Sleep.trySleep(10000);
      waitForFrame();
    }
    if (initialOrganizationSetupData.getInitialSetupData().incluceAccounting() != null) {
      final WebElement checkBoxIncludeAccounting = SeleniumSingleton.INSTANCE
          .findElementByName(CHECK_BOX_INCLUDE_ACCOUNTING);
      if (!initialOrganizationSetupData.getInitialSetupData()
          .incluceAccounting()
          .equals(checkBoxIncludeAccounting.isSelected())) {
        checkBoxIncludeAccounting.click();
      }
    }
    if (initialOrganizationSetupData.getInitialSetupData() != null) {
      if (initialOrganizationSetupData.getInitialSetupData().getAccountingFile() != null) {
        SeleniumSingleton.INSTANCE.findElementById(FILE_INPUT_ACCOUNTING_FILE)
            .sendKeys(initialOrganizationSetupData.getInitialSetupData().getAccountingFile());
      }
      if (initialOrganizationSetupData.getInitialSetupData().getCurrency() != null) {
        new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_CURRENCY))
            .selectByVisibleText(initialOrganizationSetupData.getInitialSetupData().getCurrency());
      }
      if (initialOrganizationSetupData.getInitialSetupData().getReferenceDataList() != null) {
        for (final ModuleReferenceData module : initialOrganizationSetupData.getInitialSetupData()
            .getReferenceDataList()) {
          String moduleLabel = String.format(FORMAT_LABEL_REFERENCE_DATA, module.getDescription(),
              module.getName(), module.getLanguage() + "  "); // These two extra spaces are due an
          // ERP issue that will be not solved
          if (moduleLabel.length() > MAX_LABEL_LENGTH_REFERENCE_DATA) {
            moduleLabel = moduleLabel.substring(0, MAX_LABEL_LENGTH_REFERENCE_DATA);
          }
          if (!SeleniumSingleton.INSTANCE
              .findElementByXPath(String.format(FORMAT_CHECK_BOX_REFERENCE_DATA, moduleLabel))
              .isSelected()) {
            // XXX workaround because the click function doesn't work with these checkboxes.
            String checkBoxId = SeleniumSingleton.INSTANCE
                .findElementByXPath(String.format(FORMAT_CHECK_BOX_REFERENCE_DATA, moduleLabel))
                .getAttribute(Attribute.ID.getName());
            SeleniumSingleton.INSTANCE.executeScript(
                String.format("document.getElementById('%s').checked = true", checkBoxId));
          }
        }
      }
      // FIXME remove the accounting dimensions class.
      setAccountingDimensions(new AccountingDimensions(
          initialOrganizationSetupData.getInitialSetupData().hasBusinessPartnerDimension(),
          initialOrganizationSetupData.getInitialSetupData().hasProductDimension(),
          initialOrganizationSetupData.getInitialSetupData().hasProjectDimension(),
          initialOrganizationSetupData.getInitialSetupData().hasCampaignDimension(),
          initialOrganizationSetupData.getInitialSetupData().hasSalesRegionDimension()));
      clickOK();
    }
  }

  /**
   * Click OK button and wait for the page to load
   */
  @Override
  protected void clickOK() {
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_OK).click();
    // TODO Selenium2
    // SeleniumSingleton.INSTANCE.waitForPageToLoad(INITIAL_ORGANIZATION_SETUP_TIMEOUT);
  }
}
