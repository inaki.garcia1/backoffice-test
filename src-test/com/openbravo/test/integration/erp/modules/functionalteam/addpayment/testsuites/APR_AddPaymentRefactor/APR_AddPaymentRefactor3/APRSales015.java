/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2019 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_AddPaymentRefactor.APR_AddPaymentRefactor3;

import static org.junit.Assert.assertFalse;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.AddPaymentGrid;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;

/**
 * Test issue 40545
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class APRSales015 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData[] salesOrderLinesData;
  SalesOrderLinesData[] salesOrderLinesVerificationData;
  SalesOrderHeaderData[] bookedSalesOrderHeaderVerificationData;

  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData[] salesInvoiceLinesData1;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData[] salesInvoiceLinesData2;
  SalesInvoiceLinesData[] salesInvoiceLinesVerificationData2;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData2;

  PaymentInHeaderData paymentInHeaderData;
  PaymentInHeaderData paymentInHeaderVerificationData;
  PaymentInLinesData paymentInLinesVerificationData;

  /**
   * Class constructor.
   *
   */
  public APRSales015(SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData,
      SalesOrderLinesData salesOrderLinesData1,
      SalesOrderLinesData salesOrderLinesVerificationData1,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData1,
      SalesOrderLinesData salesOrderLinesData2,
      SalesOrderLinesData salesOrderLinesVerificationData2,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData2,
      SalesOrderLinesData salesOrderLinesData3,
      SalesOrderLinesData salesOrderLinesVerificationData3,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData3,
      SalesOrderLinesData salesOrderLinesData4,
      SalesOrderLinesData salesOrderLinesVerificationData4,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData4,
      SalesOrderLinesData salesOrderLinesData5_1,
      SalesOrderLinesData salesOrderLinesVerificationData5_1,
      SalesOrderLinesData salesOrderLinesData5_2,
      SalesOrderLinesData salesOrderLinesVerificationData5_2,
      SalesOrderLinesData salesOrderLinesData5_3,
      SalesOrderLinesData salesOrderLinesVerificationData5_3,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData5,
      SalesInvoiceHeaderData salesInvoiceHeaderData1,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData1,
      SalesInvoiceLinesData salesInvoiceLinesData1, SalesInvoiceLinesData salesInvoiceLinesData2,
      SalesInvoiceLinesData salesInvoiceLinesData3, SalesInvoiceLinesData salesInvoiceLinesData4,
      SalesInvoiceLinesData salesInvoiceLinesData5, SalesInvoiceLinesData salesInvoiceLinesData6,
      SalesInvoiceLinesData salesInvoiceLinesData7,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData1,
      SalesInvoiceLinesData salesInvoiceLinesData2_1,
      SalesInvoiceLinesData salesInvoiceLinesVerificationData2_1,
      SalesInvoiceLinesData salesInvoiceLinesData2_2,
      SalesInvoiceLinesData salesInvoiceLinesVerificationData2_2,
      SalesInvoiceLinesData salesInvoiceLinesData2_3,
      SalesInvoiceLinesData salesInvoiceLinesVerificationData2_3,
      SalesInvoiceLinesData salesInvoiceLinesData2_4,
      SalesInvoiceLinesData salesInvoiceLinesVerificationData2_4,
      SalesInvoiceLinesData salesInvoiceLinesData2_5,
      SalesInvoiceLinesData salesInvoiceLinesVerificationData2_5,
      SalesInvoiceLinesData salesInvoiceLinesData2_6,
      SalesInvoiceLinesData salesInvoiceLinesVerificationData2_6,
      SalesInvoiceLinesData salesInvoiceLinesData2_7,
      SalesInvoiceLinesData salesInvoiceLinesVerificationData2_7,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData2,
      PaymentInHeaderData paymentInHeaderData,
      PaymentInHeaderData paymentInHeaderVerificationData) {
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;

    this.salesOrderLinesData = new SalesOrderLinesData[] { salesOrderLinesData1,
        salesOrderLinesData2, salesOrderLinesData3, salesOrderLinesData4, salesOrderLinesData5_1,
        salesOrderLinesData5_2, salesOrderLinesData5_3 };
    this.salesOrderLinesVerificationData = new SalesOrderLinesData[] {
        salesOrderLinesVerificationData1, salesOrderLinesVerificationData2,
        salesOrderLinesVerificationData3, salesOrderLinesVerificationData4,
        salesOrderLinesVerificationData5_1, salesOrderLinesVerificationData5_2,
        salesOrderLinesVerificationData5_3 };
    this.bookedSalesOrderHeaderVerificationData = new SalesOrderHeaderData[] {
        bookedSalesOrderHeaderVerificationData1, bookedSalesOrderHeaderVerificationData2,
        bookedSalesOrderHeaderVerificationData3, bookedSalesOrderHeaderVerificationData4,
        bookedSalesOrderHeaderVerificationData5 };

    this.salesInvoiceHeaderData = salesInvoiceHeaderData1;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData1;
    this.salesInvoiceLinesData1 = new SalesInvoiceLinesData[] { salesInvoiceLinesData1,
        salesInvoiceLinesData2, salesInvoiceLinesData3, salesInvoiceLinesData4,
        salesInvoiceLinesData5, salesInvoiceLinesData6, salesInvoiceLinesData7 };
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData1;

    this.salesInvoiceLinesData2 = new SalesInvoiceLinesData[] { salesInvoiceLinesData2_1,
        salesInvoiceLinesData2_2, salesInvoiceLinesData2_3, salesInvoiceLinesData2_4,
        salesInvoiceLinesData2_5, salesInvoiceLinesData2_6, salesInvoiceLinesData2_7 };

    this.salesInvoiceLinesVerificationData2 = new SalesInvoiceLinesData[] {
        salesInvoiceLinesVerificationData2_1, salesInvoiceLinesVerificationData2_2,
        salesInvoiceLinesVerificationData2_3, salesInvoiceLinesVerificationData2_4,
        salesInvoiceLinesVerificationData2_5, salesInvoiceLinesVerificationData2_6,
        salesInvoiceLinesVerificationData2_7 };

    this.completedSalesInvoiceHeaderVerificationData2 = completedSalesInvoiceHeaderVerificationData2;

    this.paymentInHeaderData = paymentInHeaderData;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new SalesOrderHeaderData.Builder().transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .warehouse("Spain warehouse")
            .invoiceTerms("Immediate")
            .build(),
        new SalesOrderHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .invoiceAddress(".Pamplona, Street Customer center nº1")
            .warehouse("Spain warehouse")
            .priceList("Customer A")
            .paymentMethod("1 (Spain)")
            .paymentTerms("30 days, 5")
            .invoiceTerms("Immediate")
            .build(),
        // Order 1
        new SalesOrderLinesData.Builder()
            .product(new ProductSelectorData.Builder().searchKey("FGA")
                .priceListVersion("Customer A")
                .build())
            .orderedQuantity("1")
            .unitPrice("21.95")
            .listPrice("21.95")
            .tax("VAT 21%")
            .build(),
        new SalesOrderLinesData.Builder().uOM("Bag")
            .orderedQuantity("1")
            .unitPrice("21.95")
            .listPrice("21.95")
            .tax("VAT 21%")
            .lineNetAmount("21.95")
            .build(),
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("21.95")
            .grandTotalAmount("26.56")
            .currency("EUR")
            .build(),
        // Order 2
        new SalesOrderLinesData.Builder()
            .product(new ProductSelectorData.Builder().searchKey("FGB")
                .priceListVersion("Customer A")
                .build())
            .orderedQuantity("3")
            .unitPrice("25.00")
            .listPrice("25.00")
            .tax("VAT 21%")
            .build(),
        new SalesOrderLinesData.Builder().uOM("Bag")
            .orderedQuantity("3")
            .unitPrice("25.00")
            .listPrice("25.00")
            .tax("VAT 21%")
            .lineNetAmount("75.00")
            .build(),
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("75.00")
            .grandTotalAmount("90.75")
            .currency("EUR")
            .build(),
        // Order 3
        new SalesOrderLinesData.Builder()
            .product(new ProductSelectorData.Builder().searchKey("FGC")
                .priceListVersion("Customer A")
                .build())
            .orderedQuantity("-12")
            .unitPrice("12")
            .listPrice("12")
            .tax("VAT 21%")
            .build(),
        new SalesOrderLinesData.Builder().uOM("Bag")
            .orderedQuantity("-12")
            .unitPrice("12.00")
            .listPrice("12.00")
            .tax("VAT 21%")
            .lineNetAmount("-144.00")
            .build(),
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("-144.00")
            .grandTotalAmount("-174.24")
            .currency("EUR")
            .build(),
        // Order 4
        new SalesOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("DGA").priceListVersion("Sales").build())
            .orderedQuantity("15")
            .unitPrice("0.86")
            .listPrice("0.86")
            .tax("VAT 21%")
            .build(),
        new SalesOrderLinesData.Builder().uOM("Bag")
            .orderedQuantity("15")
            .unitPrice("0.86")
            .listPrice("0.86")
            .tax("VAT 21%")
            .lineNetAmount("12.90")
            .build(),
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("12.90")
            .grandTotalAmount("15.61")
            .currency("EUR")
            .build(),
        // Order 5
        new SalesOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("DGB").priceListVersion("Sales").build())
            .orderedQuantity("20")
            .unitPrice("0.35")
            .listPrice("0.35")
            .tax("VAT 21%")
            .build(),
        new SalesOrderLinesData.Builder().uOM("Bag")
            .orderedQuantity("20")
            .unitPrice("0.35")
            .listPrice("0.35")
            .tax("VAT 21%")
            .lineNetAmount("7.00")
            .build(),
        new SalesOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("DGC").priceListVersion("Sales").build())
            .orderedQuantity("1")
            .unitPrice("25.00")
            .listPrice("25.00")
            .tax("VAT 21%")
            .build(),
        new SalesOrderLinesData.Builder().uOM("Bag")
            .orderedQuantity("1")
            .unitPrice("25.00")
            .listPrice("25.00")
            .tax("VAT 21%")
            .lineNetAmount("25.00")
            .build(),
        new SalesOrderLinesData.Builder()
            .product(new ProductSelectorData.Builder().searchKey("SB")
                .priceListVersion("Customer A")
                .build())
            .orderedQuantity("1")
            .unitPrice("13.20")
            .listPrice("13.20")
            .tax("VAT 21%")
            .build(),
        new SalesOrderLinesData.Builder().uOM("Unit")
            .orderedQuantity("1")
            .unitPrice("13.20")
            .listPrice("13.20")
            .tax("VAT 21%")
            .lineNetAmount("13.20")
            .build(),
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("45.20")
            .grandTotalAmount("54.69")
            .currency("EUR")
            .build(),
        // Invoice1 Data
        new SalesInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("1 (Spain)")
            .build(),
        new SalesInvoiceHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentTerms("30 days, 5")
            .build(),

        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good A").build())
            .invoicedQuantity("1")
            .uOM("Bag")
            .unitPrice("21.95")
            .listPrice("21.95")
            .tax("VAT 21%")
            .lineNetAmount("21.95")
            .organization("Spain")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good B").build())
            .invoicedQuantity("3")
            .uOM("Bag")
            .unitPrice("25.00")
            .listPrice("25.00")
            .tax("VAT 21%")
            .lineNetAmount("75.00")
            .organization("Spain")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good C").build())
            .invoicedQuantity("-12")
            .uOM("Bag")
            .unitPrice("12.00")
            .listPrice("12.00")
            .tax("VAT 21%")
            .lineNetAmount("-144.00")
            .organization("Spain")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(
                new ProductSimpleSelectorData.Builder().productName("Distribution good A").build())
            .invoicedQuantity("15")
            .uOM("Bag")
            .unitPrice("0.86")
            .listPrice("0.86")
            .tax("VAT 21%")
            .lineNetAmount("12.90")
            .organization("Spain")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(
                new ProductSimpleSelectorData.Builder().productName("Distribution good B").build())
            .invoicedQuantity("20")
            .uOM("Bag")
            .unitPrice("0.35")
            .listPrice("0.35")
            .tax("VAT 21%")
            .lineNetAmount("7.00")
            .organization("Spain")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(
                new ProductSimpleSelectorData.Builder().productName("Distribution good C").build())
            .invoicedQuantity("1")
            .uOM("Bag")
            .unitPrice("25.00")
            .listPrice("25.00")
            .tax("VAT 21%")
            .lineNetAmount("25.00")
            .organization("Spain")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Soccer Ball").build())
            .invoicedQuantity("1")
            .uOM("Unit")
            .unitPrice("13.20")
            .listPrice("13.20")
            .tax("VAT 21%")
            .lineNetAmount("13.20")
            .organization("Spain")
            .build(),
        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("13.37")
            .summedLineAmount("11.05")
            .currency("EUR")
            .paymentComplete(false)
            .build(),
        // Invoice2 Lines Data
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good A").build())
            .invoicedQuantity("-1")
            .unitPrice("21.95")
            .listPrice("21.95")
            .tax("VAT 21%")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good A").build())
            .invoicedQuantity("-1")
            .uOM("Bag")
            .unitPrice("21.95")
            .listPrice("21.95")
            .tax("VAT 21%")
            .lineNetAmount("-21.95")
            .organization("Spain")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good B").build())
            .invoicedQuantity("-3")
            .unitPrice("25.00")
            .listPrice("25.00")
            .tax("VAT 21%")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good B").build())
            .invoicedQuantity("-3")
            .uOM("Bag")
            .unitPrice("25.00")
            .listPrice("25.00")
            .tax("VAT 21%")
            .lineNetAmount("-75.00")
            .organization("Spain")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good C").build())
            .invoicedQuantity("12")
            .unitPrice("12.00")
            .listPrice("12.00")
            .tax("VAT 21%")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good C").build())
            .invoicedQuantity("12")
            .uOM("Bag")
            .unitPrice("12.00")
            .listPrice("12.00")
            .tax("VAT 21%")
            .lineNetAmount("144.00")
            .organization("Spain")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("DGA")
                .productName("Distribution good A")
                .build())
            .invoicedQuantity("-15")
            .unitPrice("0.86")
            .listPrice("0.86")
            .tax("VAT 21%")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(
                new ProductSimpleSelectorData.Builder().productName("Distribution good A").build())
            .invoicedQuantity("-15")
            .uOM("Bag")
            .unitPrice("0.86")
            .listPrice("0.86")
            .tax("VAT 21%")
            .lineNetAmount("-12.90")
            .organization("Spain")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("DGB")
                .productName("Distribution good B")
                .build())
            .invoicedQuantity("-20")
            .unitPrice("0.35")
            .listPrice("0.35")
            .tax("VAT 21%")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(
                new ProductSimpleSelectorData.Builder().productName("Distribution good B").build())
            .invoicedQuantity("-20")
            .uOM("Bag")
            .unitPrice("0.35")
            .listPrice("0.35")
            .tax("VAT 21%")
            .lineNetAmount("-7.00")
            .organization("Spain")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("DGC")
                .productName("Distribution good C")
                .build())
            .invoicedQuantity("-1")
            .unitPrice("25.00")
            .listPrice("25.00")
            .tax("VAT 21%")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(
                new ProductSimpleSelectorData.Builder().productName("Distribution good C").build())
            .invoicedQuantity("-1")
            .uOM("Bag")
            .unitPrice("25.00")
            .listPrice("25.00")
            .tax("VAT 21%")
            .lineNetAmount("-25.00")
            .organization("Spain")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Soccer Ball").build())
            .invoicedQuantity("-1")
            .unitPrice("13.20")
            .listPrice("13.20")
            .tax("VAT 21%")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Soccer Ball").build())
            .invoicedQuantity("-1")
            .uOM("Unit")
            .unitPrice("13.20")
            .listPrice("13.20")
            .tax("VAT 21%")
            .lineNetAmount("-13.20")
            .organization("Spain")
            .build(),
        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("-13.37")
            .summedLineAmount("-11.05")
            .currency("EUR")
            .paymentComplete(false)
            .build(),
        // Payment In
        new PaymentInHeaderData.Builder().organization("Spain")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("1 (Spain)")
            .amount("0.00")
            .account("Spain Bank - EUR")
            .build(),

        new PaymentInHeaderData.Builder().organization("Spain")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("1 (Spain)")
            .amount("0.00")
            .account("Spain Bank - EUR")
            .build(), } };
    return Arrays.asList(data);
  }

  /**
   * Test issue 40545
   *
   * @throws ParseException
   */
  @Test
  public void SALIssue40545Test() throws ParseException {
    logger.info("** Start of test case [SALIssue40545] Test issue 40545. **");

    String[] salesOrdersIdentifier = new String[bookedSalesOrderHeaderVerificationData.length];
    SalesOrder salesOrder = new SalesOrder(mainPage).open();

    for (int i = 0; i < bookedSalesOrderHeaderVerificationData.length; i++) {
      salesOrder.create(salesOrderHeaderData);
      salesOrder.assertSaved();
      salesOrder.assertData(salesOrderHeaderVerficationData);

      if (i == bookedSalesOrderHeaderVerificationData.length - 1) {
        for (int j = i; j < salesOrderLinesVerificationData.length; j++) {
          SalesOrder.Lines salesOrderLines = salesOrder.new Lines(mainPage);
          salesOrderLines.create(salesOrderLinesData[j]);
          salesOrderLines.assertSaved();
          salesOrderLines.assertData(salesOrderLinesVerificationData[j]);
        }
      } else {
        SalesOrder.Lines salesOrderLines = salesOrder.new Lines(mainPage);
        salesOrderLines.create(salesOrderLinesData[i]);
        salesOrderLines.assertSaved();
        salesOrderLines.assertData(salesOrderLinesVerificationData[i]);
      }
      salesOrder.book();
      salesOrder.assertProcessCompletedSuccessfully2();
      salesOrder.assertData(bookedSalesOrderHeaderVerificationData[i]);

      salesOrdersIdentifier[i] = String.format("%s - %s - %s",
          salesOrder.getData("documentNo").toString(), salesOrder.getData("orderDate"),
          bookedSalesOrderHeaderVerificationData[i].getDataField("grandTotalAmount"));
    }

    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    String invoice1 = (String) salesInvoice.getData("documentNo");

    for (String orderIdentifier : salesOrdersIdentifier) {
      salesInvoice.createLinesFrom(orderIdentifier);
      salesInvoice.assertProcessCompletedSuccessfully();
    }

    SalesInvoice.Lines salesInvoiceLine = salesInvoice.new Lines(mainPage);
    salesInvoiceLine.assertCount(7);
    for (SalesInvoiceLinesData invoiceLineData : salesInvoiceLinesData1) {
      salesInvoiceLine.select(new SalesInvoiceLinesData.Builder()
          .invoicedQuantity(invoiceLineData.getDataField("invoicedQuantity").toString())
          .unitPrice(invoiceLineData.getDataField("unitPrice").toString())
          .build());
      salesInvoiceLine.openSelectedRecord();
      salesInvoiceLine.assertData(invoiceLineData);
    }

    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);

    salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    String invoice2 = (String) salesInvoice.getData("documentNo");

    for (int i = 0; i < salesInvoiceLinesData2.length; i++) {
      SalesInvoice.Lines salesInvoiceLines = salesInvoice.new Lines(mainPage);
      salesInvoiceLines.create(salesInvoiceLinesData2[i]);
      salesInvoiceLines.assertSaved();
      salesInvoiceLines.assertData(salesInvoiceLinesVerificationData2[i]);
    }

    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData2);

    PaymentIn paymentIn = new PaymentIn(mainPage).open();
    paymentIn.create(paymentInHeaderData);
    paymentIn.assertSaved();

    String invoicePayment = String.format("%s or %s", invoice1, invoice2);

    paymentIn.addDetails("Invoices", invoicePayment, "Process Received Payment(s) and Deposit");

    PaymentIn.Lines paymentInLines = paymentIn.new Lines(mainPage);
    paymentInLines.assertCount(6);

    paymentIn = new PaymentIn(mainPage).open();
    paymentIn.create(paymentInHeaderData);
    paymentIn.assertSaved();

    AddPaymentProcess addDetailPayment = paymentIn.addDetailsOpen();
    AddPaymentGrid orderInvoiceGrid = addDetailPayment.getOrderInvoiceGrid();
    @SuppressWarnings("unchecked")
    List<Map<String, Object>> rows = (List<Map<String, Object>>) orderInvoiceGrid.getRows();
    for (Map<String, Object> rowg : rows) {
      logger.info(rowg.get("invoiceNo"));
      assertFalse("Invoice should not appear", rowg.get("invoiceNo").equals(invoice1));
      assertFalse("Invoice should not appear", rowg.get("invoiceNo").equals(invoice2));
    }

    logger.info("** End of test case [SALIssue40545] Test issue 40545. **");
  }
}
