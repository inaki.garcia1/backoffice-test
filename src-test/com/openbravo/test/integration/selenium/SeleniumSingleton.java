/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2019 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.selenium;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriverLogLevel;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Selenium Singleton class, accessible from every class in the project.
 *
 * @author elopio
 *
 */
public class SeleniumSingleton {

  /** The Remote Web Driver singleton instance. This is used to execute actions on web pages. */
  // public static AgnosticRemoteWebDriver INSTANCE = null;
  public static RemoteWebDriverInt INSTANCE = null;
  /** The Wait singleton instance. This is used to wait for things to happen on web pages. */
  public static Wait<WebDriver> WAIT = null;
  /** Static variable with name **/
  private static String browserName;
  /** Static variables with implicit wait time in seconds, and implicit wait set status **/
  private static long implicitWaitTime = 5;
  private static boolean isImplicitlyWaiting = true;

  /**
   * Start the selenium instance.
   */
  public static void start() {
    browserName = ConfigurationProperties.INSTANCE.getBrowser();
    // -JAVA -JAR IS LAUNCHED FROM. If we launch from pi-smoke-unstable/bin, for example,
    // geckodriver must be placed in pi-smoke-unstable/bin.
    setBrowserDriver();
    DesiredCapabilities capabilities = getDesiredCapabilities();
    capabilities = setMarionetteForFirefox(capabilities);
    capabilities.setBrowserName(browserName);
    capabilities.setJavascriptEnabled(true);
    capabilities.setCapability(CapabilityType.SUPPORTS_ALERTS, true);
    capabilities.setCapability(CapabilityType.TAKES_SCREENSHOT, true);

    // The capability named as "overlappingCheckDisabled" is added to fix problems with firefox
    // executions:
    // With this option enabled the user can click on overlapped elements like, f.e. on checkboxes
    // that have labels over them.
    // TODO L1: Check if overlappingCheckDisabled is still present. Otherwise, remove it
    capabilities.setCapability("overlappingCheckDisabled", true);

    WebDriver driver = getRemoteWebDriver(capabilities);
    driver.manage().window().setSize(new Dimension(1024, 768));
    INSTANCE = (RemoteWebDriverInt) new Augmenter().augment(driver);
    // setUpRemoteWebDriverInstance(driver);
    setImplicitWait(implicitWaitTime, TimeUnit.SECONDS);
    INSTANCE.manage().timeouts().setScriptTimeout(3000, TimeUnit.SECONDS);
    INSTANCE.manage().timeouts().pageLoadTimeout(3000, TimeUnit.SECONDS);
    WAIT = new WebDriverWait(INSTANCE, 60);
  }

  public static String getBrowserName() {
    return browserName;
  }

  public static boolean runsChrome() {
    return browserName.equals("chrome");
  }

  public static void toggleImplicitWait() {
    if (isImplicitlyWaiting) {
      setImplicitWait(0, TimeUnit.SECONDS);
      isImplicitlyWaiting = false;
    } else {
      setImplicitWait(implicitWaitTime, TimeUnit.SECONDS);
      isImplicitlyWaiting = true;
    }
  }

  private static WebDriver getRemoteWebDriver(DesiredCapabilities capabilities) {
    if (browserName.equals("chrome")) {
      // Options is the new way of working with browsers. Capabilities are converted into options
      // and, then, used by browsers
      ChromeOptions chromeOptions = new ChromeOptions();
      chromeOptions.merge(capabilities);
      return new RemoteWebDriverChrome(chromeOptions);
    } else {

      FirefoxOptions firefoxOptions = new FirefoxOptions();
      firefoxOptions.merge(capabilities);
      firefoxOptions.setLogLevel(FirefoxDriverLogLevel.ERROR);
      return new RemoteWebDriverFirefox(firefoxOptions);
    }
  }

  private static void setImplicitWait(long implicitWaitTime, TimeUnit timeUnit) {
    INSTANCE.manage().timeouts().implicitlyWait(implicitWaitTime, timeUnit);
  }

  private static DesiredCapabilities setMarionetteForFirefox(DesiredCapabilities capabilities) {
    if (!browserName.equals("chrome")) {
      capabilities.setCapability("marionette", true);
    }
    return capabilities;
  }

  private static void setBrowserDriver() {
    if (browserName.equals("chrome")) {
      System.setProperty("webdriver.chrome.driver", "chromedriver");
    } else {
      System.setProperty("webdriver.gecko.driver", "geckodriver");
    }
  }

  private static DesiredCapabilities getDesiredCapabilities() {
    if (browserName.equals("chrome")) {
      return getChromeCapabilities();
    } else {
      return getFirefoxCapabilities();
    }
  }

  private static LoggingPreferences getJSConsoleErrorLogsCapability() {
    LoggingPreferences logPrefs = new LoggingPreferences();
    logPrefs.enable(LogType.BROWSER, Level.ALL);
    return logPrefs;
  }

  private static DesiredCapabilities getFirefoxCapabilities() {
    DesiredCapabilities capabilities = DesiredCapabilities.firefox();
    capabilities.setCapability(CapabilityType.LOGGING_PREFS, getJSConsoleErrorLogsCapability());
    return capabilities;
  }

  private static DesiredCapabilities getChromeCapabilities() {
    DesiredCapabilities capabilities = DesiredCapabilities.chrome();
    // Setting ChromeOptions
    ChromeOptions chromeOptions = new ChromeOptions();
    Map<String, Object> prefs = new HashMap<String, Object>();
    // The following two prefs (credentials_enable_service and password_manager_enabled are required
    // to avoid "do you want google chrome to update your password for this site" message)
    prefs.put("credentials_enable_service", false);
    prefs.put("profile.password_manager_enabled", false);
    chromeOptions.addArguments("disable-extensions");
    chromeOptions.addArguments("disable-infobars");
    chromeOptions.setExperimentalOption("prefs", prefs);
    // chromeOptions.setExperimentalOption("excludeSwitches", Arrays.asList("enable-automation"));
    capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
    capabilities.setCapability(CapabilityType.LOGGING_PREFS, getJSConsoleErrorLogsCapability());
    return capabilities;
  }
}
