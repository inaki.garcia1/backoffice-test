/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2019 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */
package com.openbravo.test.integration.selenium;

import java.util.function.Function;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxOptions;

/**
 * Extended Remote Web Driver with the addition of the Smart Client locator.
 *
 * @author elopio
 *
 */
public class RemoteWebDriverFirefox extends org.openqa.selenium.firefox.FirefoxDriver
    implements RemoteWebDriverInt, FindsByScLocator {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Formatting string used to get an element using the Smart Client locator. */
  private static final String FORMAT_SMART_CLIENT_GET_ELEMENT_BY_LOCATOR = "window.isc.AutoTest.getElement(\"%s\")";

  /**
   * Class constructor.
   */
  public RemoteWebDriverFirefox() {
  }

  public RemoteWebDriverFirefox(FirefoxOptions firefoxOptions) {
    super(firefoxOptions);
  }

  /**
   * Find an element using the Smart Client locator.
   *
   * @param using
   *          The Smart Client locator of the element.
   *
   * @param ignoreSleep
   *          To be set to true if Selenium native smart sleep is to be ignored
   * @return the web element
   */
  @Override
  public WebElement findElementByScLocator(String using, boolean ignoreSleep) {
    if (ignoreSleep) {
      return findElementByScLocatorSkeleton(using);
    }
    return findElementByScLocator(using);
  }

  @Override
  public WebElement findElementByScLocator(String using) {
    return Sleep.setFluentWait(300, WebDriverException.class)
        .until(new Function<WebDriver, WebElement>() {
          @Override
          public WebElement apply(WebDriver webDriver) {
            return findElementByScLocatorSkeleton(using);
          }
        });
  }

  /**
   * Private skeleton method for findElementByScLocator
   *
   * @param using
   *          The Smart Client locator of the element.
   * @return the web element
   */
  private WebElement findElementByScLocatorSkeleton(String using) {
    // Remove the leading identifier of the locator, if present.
    String param = using.replace("scLocator=", "").replace("/element", "");
    // Replace all double quotes by single quotes in order to evaluate them using
    // JavaScript.
    param = param.replaceAll("\"", "'");
    WebElement element;
    String elementLocator = String.format(FORMAT_SMART_CLIENT_GET_ELEMENT_BY_LOCATOR, param);

    try {
      element = (WebElement) executeScriptWithReturn(elementLocator);
    } catch (StaleElementReferenceException e) {
      logger.warn(
          String.format("StaleElementReferenceException for locator [%s], trying it once again...",
              elementLocator));
      element = (WebElement) executeScriptWithReturn(elementLocator);
    }
    if (element == null) {
      throw new NoSuchElementException(param);
    } else {
      return element;
    }
  }

  /**
   * Find an element by id with added fluent wait.
   *
   * @param comboId
   *          The id of the element of the element.
   * @param timeoutInMs
   *          The timeout for the fluent wait
   * @return the web element
   */
  @Override
  public WebElement findElementById(String comboId, int timeoutInMs) {
    return Sleep.setFluentWait(1000, timeoutInMs, WebDriverException.class)
        .until(new Function<WebDriver, WebElement>() {
          @Override
          public WebElement apply(WebDriver webDriver) {
            return findElementById(comboId);
          }
        });
  }

  /**
   * Find an element using link text with added fluent wait.
   *
   * @param linkRebuildNow
   *          The link text of the element of the element.
   * @return the web element
   */
  @Override
  public WebElement findElementByLinkTextWithFWait(String linkRebuildNow) {
    return Sleep.setFluentWait(300, WebDriverException.class)
        .until(new Function<WebDriver, WebElement>() {
          @Override
          public WebElement apply(WebDriver webDriver) {
            return findElementByLinkText(linkRebuildNow);
          }
        });
  }

  /**
   * Execute a script that returns a value.
   *
   * @param script
   *          The script to execute.
   * @param args
   *          The arguments of the execution.
   * @return the return value of the script execution.
   */
  @Override
  public Object executeScriptWithReturn(String script, Object... args) {
    String scriptWithReturn = script;
    if (!script.startsWith("return ")) {
      scriptWithReturn = "return " + scriptWithReturn;
    }
    // TODO L: Instead of treat data the following way, it can be interesting to change how data is
    // treated above (Canvas:352 isEnabledSmartClient()).
    try {
      if (scriptWithReturn.contains("==null")) {
        return executeScript(scriptWithReturn, args);
      } else {
        return executeScript(scriptWithReturn, args);
      }
    } catch (JavascriptException jse) {
      if (jse.toString().contains("SyntaxError: missing } after function body")) {
        return null;
      } else {
        // JavascriptException has been caught, let's wait and try again
        Sleep.trySleep();
        return executeScript(scriptWithReturn, args);
      }
    } catch (WebDriverException wde) {
      if (wde.toString().contains("SyntaxError: Unexpected end of input")) {
        return null;
      } else {
        return executeScript(scriptWithReturn, args);
      }
    }
  }
}
