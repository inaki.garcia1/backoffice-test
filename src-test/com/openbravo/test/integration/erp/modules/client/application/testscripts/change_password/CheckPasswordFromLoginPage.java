/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.openbravo.test.integration.erp.modules.client.application.testscripts.change_password;

import static org.junit.Assert.assertFalse;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.ChangeExpiredPasswordData;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.gui.LoginPage;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Test that PasswordStrengthChecker rejects a weak password in Login page
 *
 * @author jarmendariz
 */
@RunWith(Parameterized.class)
public class CheckPasswordFromLoginPage extends CreateTemporaryUserTest {

  private static final String TEST_USER_PREFIX = "testpassword_";
  private static final String TEST_USER_PASSWORD = "test";
  private static final String TEST_USER_ROLE = "F&BUser";
  private static Logger logger = LogManager.getLogger();
  private ChangeExpiredPasswordData changeExpiredPasswordData;

  public CheckPasswordFromLoginPage(ChangeExpiredPasswordData changePasswordData) {
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
    changeExpiredPasswordData = changePasswordData;
  }

  @Parameters
  public static Collection<Object[]> SalesOrderValues() {
    Object[][] data = new Object[][] {
        { new ChangeExpiredPasswordData.Builder().newPassword("short")
            .confirmPassword("short")
            .build() } };
    return Arrays.asList(data);
  }

  @Test
  public void expiredPasswordChangeShouldBeRejected() {
    LoginPage loginPage = attemptLoginWithUserWithExpiredPassword();

    logger.info("Attempting to change password");
    loginPage.changeExpiredPassword(changeExpiredPasswordData);
    Sleep.trySleep();

    verifyNewPasswordIsRejected(loginPage);
  }

  private LoginPage attemptLoginWithUserWithExpiredPassword() {
    logger.info("Creating test user");
    String testUser = TEST_USER_PREFIX + System.currentTimeMillis();
    createTestUserWithExpiredPassword(testUser, TEST_USER_PASSWORD, TEST_USER_ROLE);

    logger.info("Moving to login page");
    logout();
    LoginPage loginPage = loginWith(
        new LogInData.Builder().userName(testUser).password(TEST_USER_PASSWORD).build());

    logger.info("User password is expired. Change password fields should appear");
    loginPage.verifyPasswordExpired();
    return loginPage;
  }

  private void verifyNewPasswordIsRejected(LoginPage loginPage) {
    if (mainPage.isViewOpen("Workspace")) {
      logger.info("Logged in with a weak password. Test fails");
      assertFalse("Logged in!", true);
    } else {
      logger.info("Not logged in. Checking error message appeared");
      loginPage.verifyInvalidPassword();
    }
  }
}
