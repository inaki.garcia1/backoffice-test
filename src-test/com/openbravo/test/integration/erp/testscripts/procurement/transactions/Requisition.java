/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.procurement.transactions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.procurement.transactions.requisition.RequisitionHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.requisition.RequisitionLinesData;
import com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseorder.PurchaseOrderWindow;
import com.openbravo.test.integration.erp.gui.procurement.transactions.requisition.HeaderTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.requisition.LinesTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.requisition.RequisitionWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on the window.
 *
 * @author Pablo Lujan
 */
public class Requisition extends SmokeTabScript<RequisitionHeaderData, HeaderTab>
    implements SmokeWindowScript<PurchaseOrderWindow> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  protected static final String IDENTIFIER_NEED_BY_DATE = "needByDate_dateTextField";

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public Requisition(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Open the window.
   *
   * @return the opened window.
   */
  @Override
  public Requisition open() {
    final RequisitionWindow window = new RequisitionWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the tab.
   *
   * Before this, the window must be opened.
   *
   * @return the tab GUI object.
   */
  @Override
  protected HeaderTab selectTab() {
    RequisitionWindow window = (RequisitionWindow) mainPage.getView(RequisitionWindow.TITLE);
    mainPage.getTabSetMain().selectTab(RequisitionWindow.TITLE);
    HeaderTab tab = (HeaderTab) window.getTopLevelTab();
    tab.waitUntilSelected();
    return tab;
  }

  /**
   * Complete the order.
   *
   */
  public void complete() {
    logger.info("Completing requisition.");
    final HeaderTab tab = selectTab();
    tab.complete();
  }

  /**
   * Executes and verifies actions on the Purchase Order Lines tab.
   *
   * @author elopio
   *
   */
  public class Lines extends SmokeTabScript<RequisitionLinesData, LinesTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Lines(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Lines tab.
     *
     * Before this, the window must be opened and this tab has to be accessible from the currently
     * selected one.
     *
     * @return the Header tab GUI object.
     */
    @Override
    protected LinesTab selectTab() {
      final RequisitionWindow window = (RequisitionWindow) mainPage
          .getView(RequisitionWindow.TITLE);
      final LinesTab tab = (LinesTab) window.selectTab(LinesTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }

    public void setNeedByDate(String date) {
      SeleniumSingleton.INSTANCE.findElementByName("needByDate_dateTextField").sendKeys(date);
    }
  }

}
