/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.initialdataload.testscripts.masterdata.process.importwindow;

import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.modules.initialdataload.data.masterdata.process.importwindow.ImportData;
import com.openbravo.test.integration.erp.modules.initialdataload.gui.masterdata.process.importwindow.ImportForm;

/**
 * Executes and verifies actions on Import the window.
 *
 * @author Pablo Lujan
 */
public class Import {

  /**
   * Validate a file.
   *
   * @param mainPage
   *          The application main page.
   * @param data
   *          The data for the import process.
   */
  public static void validateFile(MainPage mainPage, ImportData data) {
    ImportForm importForm = new ImportForm();
    mainPage.openView(importForm);

    importForm.validateFile(data);
  }

  /**
   * Verify the validation process.
   *
   * @param mainPage
   *          The application main page.
   * @param totalRows
   *          The number of rows in the file.
   * @param rowsValidated
   *          The number of rows validated.
   * @param rowsRejected
   *          The number of rows rejected.
   */
  public static void verifyValidationProcessCompletedSuccessfully(MainPage mainPage, int totalRows,
      int rowsValidated, int rowsRejected) {
    ImportForm importForm = new ImportForm();
    mainPage.openView(importForm);

    importForm.verifyValidationProcessCompletedSuccessfully(totalRows, rowsValidated, rowsRejected);
  }

  /**
   * Process a file.
   *
   * @param mainPage
   *          The application main page.
   * @param data
   *          The data for the import process.
   */
  public static void processFile(MainPage mainPage, ImportData data) {
    ImportForm importForm = new ImportForm();
    mainPage.openView(importForm);

    importForm.processFile(data);
  }

  /**
   * Verify the import process.
   *
   * @param mainPage
   *          The application main page.
   * @param totalRows
   *          The number of rows in the file.
   * @param rowsProcessed
   *          The number of rows processed.
   * @param rowsRejected
   *          The number of rows rejected.
   */
  public static void verifyImportProcessCompletedSuccessfully(MainPage mainPage, int totalRows,
      int rowsProcessed, int rowsRejected) {
    ImportForm importForm = new ImportForm();
    mainPage.openView(importForm);

    importForm.verifyImportProcessCompletedSuccessfully(totalRows, rowsProcessed, rowsRejected);
  }

}
