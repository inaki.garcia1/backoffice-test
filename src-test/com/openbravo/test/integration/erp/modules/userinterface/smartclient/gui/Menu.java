/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2014 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.StaleElementReferenceException;

import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes actions on OpenbravoERP menus built upon SmartClient user interface library.
 *
 * @author elopio
 *
 */
public class Menu extends Canvas {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Formatting string to locate the form. The first parameter is object string. The second is a
   * list of items separated by commas.
   *
   * Note: the getSCLocator expects a list of string arguments. It has been tried to pass in an
   * array of strings but this did not work, apparently Selenium does not accept a fixed array as a
   * parameter.
   */
  private final static String FORMAT_SMART_CLIENT_GET_MENU_ITEM_LOCATOR = "%s.getSCLocator(%s)";

  /**
   * In case of failure trying to open the menu, it is retried up to this limit
   */
  private static final int MAX_RETRIES = 50;

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   */
  public Menu(String objectString) {
    super(objectString);
  }

  /**
   * Get the SmartClient Locator of a menu item.
   *
   * @param menuPath
   *          The path of the menu item, separated by commas, with each item inside quotes.
   * @return the SmartClient Locator of the menu item.
   */
  private String getMenuItemLocator(String menuPath) {
    return (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
        String.format(FORMAT_SMART_CLIENT_GET_MENU_ITEM_LOCATOR, objectString, menuPath));
  }

  /**
   * Select an item from the menu.
   *
   * @param items
   *          the path of the item to select.
   */
  public void select(String... items) {
    select(0, items);
  }

  private void select(int retrying, String... items) {
    final StringBuilder stringBuilder = new StringBuilder();

    if (retrying > MAX_RETRIES) {
      throw new RuntimeException(
          String.format("Giving up trying to open menu after %s retrials", retrying));
    }

    for (String item : items) {
      if (stringBuilder.length() > 0) {
        stringBuilder.append(",");
      }
      stringBuilder.append("'" + item + "'");
      logger.debug("Selecting menu item '{}'.", item);
      try {
        // logger.debug("BEFORE: menuItem WebElement is going to be created");
        // final WebElement menuItem = SeleniumSingleton.INSTANCE
        // .findElementByScLocator(getMenuItemLocator(stringBuilder.toString()));
        logger.trace("BEFORE: Just before 'if'");
        // The following line is set like that due to StaleElementReferenceException
        if (!isMenuItemLocatorDisplayed(stringBuilder.toString())) {
          // Sometimes item is not displayed, let's retry
          logger.debug("Menu item '{}' is not visible, retrying opening menu", item);
          select(retrying + 1, items);
          return;
        }
        // menuItem.click was commented after updating to Selenium 3.3.1 + Gecko + FF 52.1.2esr.
        // Several StaleElementReferenceException were being thrown in SALa trying to access MENU.
        // menuItem.click();
        logger.trace("BEFORE: Clicking");
        SeleniumSingleton.INSTANCE
            .findElementByScLocator(getMenuItemLocator(stringBuilder.toString()))
            .click();
        logger.trace("AFTER: Clicked");
        Sleep.trySleep(7);
      } catch (Exception e) {
        // Even the element is displayed it sometimes fails with ElementNotVisibleException or
        // StaleElementReferenceException. TODO: research why, by now let's retry again
        logger.debug("Failed opening menu item '{}' ({}), retrying opening menu", item,
            e.getClass());
        select(retrying + 1, items);
        return;
      }
    }
  }

  /**
   * Return true if the menu item is present.
   *
   * @param item
   *          The text of the menu item.
   * @return true if the menu item is present.
   */
  public boolean isItemPresent(String item) {
    return SeleniumSingleton.INSTANCE.findElementByScLocator(getMenuItemLocator(item)) != null;
  }

  /**
   * Return true if the menu item is displayed.
   *
   * @param menuPath
   *          The String with the Menu Item Path
   * @return true if the menu item is displayed.
   */
  private boolean isMenuItemLocatorDisplayed(String menuPath) {
    boolean result = false;
    int attempts = 0;
    while (attempts < 10) {
      try {
        result = SeleniumSingleton.INSTANCE.findElementByScLocator(getMenuItemLocator(menuPath))
            .isDisplayed();
        result = true;
        break;
      } catch (StaleElementReferenceException sere) {
        logger.warn(
            "StaleElementReferenceException thrown checking whether Menu Item Locator is displayed ");
      }
      attempts++;
    }
    return result;
  }

}
