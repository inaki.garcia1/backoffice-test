/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui.searchitems;

import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.InputField;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.SearchItem;

/**
 * Executes actions on OpenbravoERP search items.
 *
 * @author elopio
 *
 */
public abstract class OBSearchItem implements InputField {

  /* Components. */
  /** The search item. */
  protected final SearchItem searchItem;

  /**
   * Class constructor.
   *
   * @param searchItem
   *          The search item of this selector.
   */
  public OBSearchItem(SearchItem searchItem) {
    this.searchItem = searchItem;
  }

  /**
   * Get the attribute search item value.
   *
   * @return the attribute search item value.
   */
  @Override
  public String getValue() {
    return searchItem.getValue();
  }

  /**
   * Set the attribute search item value.
   *
   * @param value
   *          The value to set.
   */
  @Override
  public abstract void setValue(Object value);

  /**
   * Set the focus on the search item.
   */
  @Override
  public void focus() {
    searchItem.focus();
  }

  /**
   * Wait until the search item is enabled.
   */
  @Override
  public void waitUntilEnabled() {
    searchItem.waitUntilEnabled();
  }

  /**
   * Return true if the field is present.
   *
   * @return true if the field is present. Otherwise, false.
   */
  @Override
  public boolean isPresent() {
    return searchItem.isPresent();
  }

  /**
   * Return true if the field is visible.
   *
   * @return true if the field is visible. Otherwise, false.
   */
  @Override
  public boolean isVisible() {
    return searchItem.isVisible();
  }

  /**
   * Return true if the field is enabled.
   *
   * @return true if the field is enabled. Otherwise, false.
   */
  @Override
  public boolean isEnabled() {
    return searchItem.isEnabled();
  }
}
