/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.masterdata.product;

import com.openbravo.test.integration.erp.modules.client.application.gui.StandardWindow;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;

/**
 * Executes and verify actions on the Product standard window of OpenbravoERP.
 *
 * @author elopio
 *
 */
public class ProductWindow extends StandardWindow {

  /** The window title. */
  @SuppressWarnings("hiding")
  public static final String TITLE = "Product";
  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.MASTER_DATA_MANAGEMENT,
      Menu.PRODUCT };

  /**
   * Class constructor.
   */
  public ProductWindow() {
    super(TITLE, MENU_PATH);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void load() {
    ProductTab productTab = new ProductTab();
    addTopLevelTab(productTab);
    super.load();
  }

  /**
   * Select and return the Product tab.
   *
   * @return the Product tab.
   */
  public ProductTab selectProductTab() {
    return (ProductTab) selectTab(ProductTab.IDENTIFIER);
  }

  /**
   * Select and return the Characteristic tab.
   *
   * @return the Characteristic tab.
   */
  public CharacteristicsTab selectCharacteristicsTab() {
    return (CharacteristicsTab) selectTab(CharacteristicsTab.IDENTIFIER);
  }

  /**
   * Select and return the Price tab.
   *
   * @return the Price tab.
   */
  public PriceTab selectPriceTab() {
    return (PriceTab) selectTab(PriceTab.IDENTIFIER);
  }

  /**
   * Select and return the Characteristic configuration tab.
   *
   * @return the Characteristic configuration tab.
   */
  public ProductCharacteristicConfigurationTab selectCharacteristicConfigurationTab() {
    return (ProductCharacteristicConfigurationTab) selectTab(
        ProductCharacteristicConfigurationTab.IDENTIFIER);
  }

  /**
   * Select and return the AUM tab.
   *
   * @return the AUM tab.
   */
  public AUMTab selectAUMTab() {
    return (AUMTab) selectTab(AUMTab.IDENTIFIER);
  }

  /**
   * Select and return the Stock tab.
   *
   * @return the Stock tab.
   */
  public ProductStockTab selectStockTab() {
    return (ProductStockTab) selectTab(ProductStockTab.IDENTIFIER);
  }
}
