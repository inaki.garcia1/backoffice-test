/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.accounting.setup.glitem;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for GLItemData
 *
 * @author plujan
 *
 */
public class GLItemData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the taxCategory value
     *
     * Description: A classification of tax options based on similar characteristics or attributes.
     *
     * @param value
     *          The taxCategory value.
     * @return The builder for this class.
     */
    public Builder taxCategory(String value) {
      this.dataFields.put("taxCategory", value);
      return this;
    }

    /**
     * Set the enableInFinancialInvoices value
     *
     * Description: It is a flag to mark some glitem to be shown in booking a Financial Invoice in
     * the Invoice Line window.
     *
     * @param value
     *          The enableInFinancialInvoices value.
     * @return The builder for this class.
     */
    public Builder enableInFinancialInvoices(Boolean value) {
      this.dataFields.put("enableInFinancialInvoices", value);
      return this;
    }

    /**
     * Set the tax value
     *
     * Description: The percentage of money requested by the government for this specified product
     * or transaction.
     *
     * @param value
     *          The tax value.
     * @return The builder for this class.
     */
    public Builder tax(String value) {
      this.dataFields.put("tax", value);
      return this;
    }

    /**
     * Set the enableInCash value
     *
     * Description: Enable In Cash
     *
     * @param value
     *          The enableInCash value.
     * @return The builder for this class.
     */
    public Builder enableInCash(Boolean value) {
      this.dataFields.put("enableInCash", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public GLItemData build() {
      return new GLItemData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private GLItemData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
