/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.masterdata.businesspartner.classic;

import org.openqa.selenium.WebElement;

import com.openbravo.test.integration.erp.data.masterdata.businesspartner.LocationAddressData;
import com.openbravo.test.integration.erp.data.selectors.LocationSelectorData;
import com.openbravo.test.integration.erp.gui.popups.AddressSelectorPopUp;
import com.openbravo.test.integration.erp.gui.popups.AddressSelectorPopUpSc;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Location/Address tab.
 *
 * @author elopio
 *
 */
public class LocationAddressTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Location/Address";
  /** The tab level. */
  private static final int LEVEL = 0;

  /* GUI components. */
  /** Identifier of the Customer tab. */
  public static final String TAB = "tabname222";

  /** Identifier of the Ship to Address checkbox. */
  private static final String CHECK_BOX_SHIP_TO_ADDRESS = "IsShipTo";
  /** Identifier of the Invoice to Address checkbox. */
  private static final String CHECK_BOX_INVOICE_TO_ADDRESS = "IsBillTo";
  /** Identifier of the Pay from Address checkbox. */
  @SuppressWarnings("unused")
  private static final String CHECK_BOX_PAY_FROM_ADDRESS = "IsPayFrom";
  /** Identifier of the Remit to Address checkbox. */
  @SuppressWarnings("unused")
  private static final String CHECK_BOX_REMIT_TO_ADDRESS = "IsRemitTo";

  /**
   * Class constructor
   *
   */
  public LocationAddressTab() {
    super(TITLE, LEVEL, TAB);
  }

  /**
   * Create a new Location/Address.
   *
   * @param data
   *          The data of the Location/Address that will be created.
   */
  public void create(LocationAddressData data) {
    clickCreateNewRecord();
    if (data.getDataField("locationAddress") != null) {
      String windowHandle = SeleniumSingleton.INSTANCE.getWindowHandle();
      SeleniumSingleton.INSTANCE.findElementById(AddressSelectorPopUp.BUTTON_ADDRESS).click();
      final AddressSelectorPopUpSc addressSelector = new AddressSelectorPopUpSc();
      addressSelector.selectAddress((LocationSelectorData) data.getDataField("locationAddress"));
      SeleniumSingleton.INSTANCE.switchTo().window(windowHandle);
      waitForFrame();
    }
    if (data.getDataField("shipToAddress") != null) {
      WebElement checkBoxShipToAddress = SeleniumSingleton.INSTANCE
          .findElementById(CHECK_BOX_SHIP_TO_ADDRESS);
      if (!data.getDataField("shipToAddress").equals(checkBoxShipToAddress.isSelected())) {
        checkBoxShipToAddress.click();
      }
    }
    if (data.getDataField("invoiceToAddress") != null) {
      WebElement checkBoxInvoiceToAddress = SeleniumSingleton.INSTANCE
          .findElementById(CHECK_BOX_INVOICE_TO_ADDRESS);
      if (!data.getDataField("invoiceToAddress").equals(checkBoxInvoiceToAddress.isSelected())) {
        checkBoxInvoiceToAddress.click();
      }
    }
    clickSave();
  }
}
