/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:15
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.warehouse.transactions.goodstransaction;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.LocatorSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ShipmentReceiptLineSelectorData;

/**
 *
 * Class for GoodsTransactionData
 *
 * @author plujan
 *
 */
public class GoodsTransactionData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the uOM value
     *
     * Description: A non monetary unit of measure.
     *
     * @param value
     *          The uOM value.
     * @return The builder for this class.
     */
    public Builder uOM(String value) {
      this.dataFields.put("uOM", value);
      return this;
    }

    /**
     * Set the orderUOM value
     *
     * Description: The unit of measure being used for the request.
     *
     * @param value
     *          The orderUOM value.
     * @return The builder for this class.
     */
    public Builder orderUOM(String value) {
      this.dataFields.put("orderUOM", value);
      return this;
    }

    /**
     * Set the orderQuantity value
     *
     * Description: The number of a certain item involved in the transaction shown in units which
     * differ from the standard UOM.
     *
     * @param value
     *          The orderQuantity value.
     * @return The builder for this class.
     */
    public Builder orderQuantity(String value) {
      this.dataFields.put("orderQuantity", value);
      return this;
    }

    /**
     * Set the attributeSetValue value
     *
     * Description: An attribute associated with a product as part of an attribute set.
     *
     * @param value
     *          The attributeSetValue value.
     * @return The builder for this class.
     */
    public Builder attributeSetValue(String value) {
      this.dataFields.put("attributeSetValue", value);
      return this;
    }

    /**
     * Set the projectIssue value
     *
     * Description: x not implemented
     *
     * @param value
     *          The projectIssue value.
     * @return The builder for this class.
     */
    public Builder projectIssue(String value) {
      this.dataFields.put("projectIssue", value);
      return this;
    }

    /**
     * Set the movementDate value
     *
     * Description: The date that a certain item is moved from one location to another.
     *
     * @param value
     *          The movementDate value.
     * @return The builder for this class.
     */
    public Builder movementDate(String value) {
      this.dataFields.put("movementDate", value);
      return this;
    }

    /**
     * Set the storageBin value
     *
     * Description: A set of coordinates (x y z) which help locate an item in a warehouse.
     *
     * @param value
     *          The storageBin value.
     * @return The builder for this class.
     */
    public Builder storageBin(LocatorSelectorData value) {
      this.dataFields.put("storageBin", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the movementQuantity value
     *
     * Description: The number of items being moved from one location to another.
     *
     * @param value
     *          The movementQuantity value.
     * @return The builder for this class.
     */
    public Builder movementQuantity(String value) {
      this.dataFields.put("movementQuantity", value);
      return this;
    }

    /**
     * Set the productionLine value
     *
     * Description: A statement displaying one item or action in the application.
     *
     * @param value
     *          The productionLine value.
     * @return The builder for this class.
     */
    public Builder productionLine(String value) {
      this.dataFields.put("productionLine", value);
      return this;
    }

    /**
     * Set the goodsShipmentLine value
     *
     * Description: A statement displaying one item charge or movement in a shipment.
     *
     * @param value
     *          The goodsShipmentLine value.
     * @return The builder for this class.
     */
    public Builder goodsShipmentLine(ShipmentReceiptLineSelectorData value) {
      this.dataFields.put("goodsShipmentLine", value);
      return this;
    }

    /**
     * Set the movementLine value
     *
     * Description: An often automatically generated identifier for a movement line.
     *
     * @param value
     *          The movementLine value.
     * @return The builder for this class.
     */
    public Builder movementLine(String value) {
      this.dataFields.put("movementLine", value);
      return this;
    }

    /**
     * Set the physicalInventoryLine value
     *
     * Description: A statement displaying one item in the physical inventory list.
     *
     * @param value
     *          The physicalInventoryLine value.
     * @return The builder for this class.
     */
    public Builder physicalInventoryLine(String value) {
      this.dataFields.put("physicalInventoryLine", value);
      return this;
    }

    /**
     * Set the movementType value
     *
     * Description: The type of a certain item being moved from one location to another.
     *
     * @param value
     *          The movementType value.
     * @return The builder for this class.
     */
    public Builder movementType(String value) {
      this.dataFields.put("movementType", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public GoodsTransactionData build() {
      return new GoodsTransactionData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private GoodsTransactionData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
