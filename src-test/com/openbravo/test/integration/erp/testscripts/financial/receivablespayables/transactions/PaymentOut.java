/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentDisplayLogic;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutLinesData;
import com.openbravo.test.integration.erp.data.sharedtabs.ExchangeRatesData;
import com.openbravo.test.integration.erp.data.sharedtabs.UsedCreditSourceData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentout.ExchangeRatesTab;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentout.PaymentOutLinesTab;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentout.PaymentOutTab;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentout.PaymentOutWindow;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentout.UsedCreditSourceTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.FormItem;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on the window.
 *
 * @author Pablo Lujan
 */
public class PaymentOut extends SmokeTabScript<PaymentOutHeaderData, PaymentOutTab>
    implements SmokeWindowScript<PaymentOutWindow> {

  /** Registry key of the post button. */
  private static final String REGISTRY_KEY_BUTTON_POST = "org.openbravo.client.application.toolbar.button.posted.F7A52FDAAA0346EFA07D53C125B40404";

  /** Identifier of the delete accounting entry checkbox */
  protected static final String DELETE_CHECKBOX = "inpEliminar";

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public PaymentOut(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Open the window.
   *
   * @return the opened window.
   */
  @Override
  public PaymentOut open() {
    final PaymentOutWindow window = new PaymentOutWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the tab.
   *
   * Before this, the window must be opened.
   *
   * @return the tab GUI object.
   */
  @Override
  protected PaymentOutTab selectTab() {
    final PaymentOutWindow window = (PaymentOutWindow) mainPage.getView(PaymentOutWindow.TITLE);
    mainPage.getTabSetMain().selectTab(PaymentOutWindow.TITLE);
    final PaymentOutTab tab = (PaymentOutTab) window.getTopLevelTab();
    tab.waitUntilSelected();
    return tab;
  }

  /**
   * Assert the data of the add payment out popup.
   *
   * @param payingTo
   *          The business partner that will be payed.
   * @param amount
   *          The amount of the payment.
   * @param transactionType
   *          The type of the transaction.
   */
  public void assertAddPaymentOutData(String payingTo, String amount, String transactionType) {
    final PaymentOutTab tab = selectTab();
    tab.assertAddPaymentOutData(payingTo, amount, transactionType);
  }

  /**
   * Add payment out details.
   *
   * @param transactionType
   *          The type of the transaction.
   * @param documentNumber
   *          The number of the document.
   * @param action
   *          The process action to request after the payment.
   */
  public void addDetails(String transactionType, String documentNumber, String action) {
    logger.info("Adding Payment details.");
    final PaymentOutTab tab = selectTab();
    tab.addPaymentOut(transactionType, documentNumber, action);
  }

  public AddPaymentProcess addDetailsOpen() {
    logger.info("Adding Payment details.");
    final PaymentOutTab tab = selectTab();
    AddPaymentProcess addPaymentProcess = tab.addPaymentOutOpen();
    return addPaymentProcess;
  }

  /**
   * Check whether the payment out is already posted.
   */
  public boolean isPosted() {
    logger.debug("Checking whether the payment out is already posted or not.");
    Button buttonPost = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_POST));
    return (buttonPost.getText().equals("Unpost"));
  }

  /**
   * Post the payment out.
   */
  public void post() {
    logger.debug("Posting the payment out.");
    Button buttonPost = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_POST));
    buttonPost.click();
    OBClassicPopUp popUp = new OBClassicPopUp(TestRegistry
        .getObjectString("org.openbravo.classicpopup./PaymentOut/Header_Edition.html")) {
      @Override
      protected void selectFrame() {
        SeleniumSingleton.INSTANCE.switchTo().frame("process");
      }
    };
    popUp.selectPopUpWithOKButton();
    popUp.clickOK();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    // XXX: <performance issues> Waiting until all Journal Entries window are open to prevent
    // overlap and/or interruption by the actions following
    Sleep.setFluentWait(300, 600000).until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver driver) {
        String strOfJsScript = (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
            "document.getElementsByClassName('OBTabBarButtonMainTitleSelected').item(0).textContent");
        String strComp2 = " - Main";
        String strComp3 = " - USA U...";
        if (strOfJsScript.toLowerCase().contains(strComp2.toLowerCase())) {
          logger.trace("Sleeping for the first 'Main US/A/Euro' Journal Entry tab to load");
          Sleep.trySleep(2000);
        }
        return strOfJsScript.toLowerCase().contains(strComp3.toLowerCase());
      }
    });
  }

  /**
   * UnPost the payment out.
   */
  public void unpost() {
    logger.debug("Unposting the payment out.");
    Button buttonUnpost = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_POST));
    buttonUnpost.click();
    OBClassicPopUp popUp = new OBClassicPopUp(TestRegistry
        .getObjectString("org.openbravo.classicpopup./PaymentOut/Header_Edition.html")) {
      @Override
      protected void selectFrame() {
        SeleniumSingleton.INSTANCE.switchTo().frame("process");
      }
    };
    popUp.selectPopUpWithOKButton();
    popUp.verifyElementsAreVisible(new String[] { DELETE_CHECKBOX });
    if (!SeleniumSingleton.INSTANCE.findElementByName(DELETE_CHECKBOX).isSelected()) {
      SeleniumSingleton.INSTANCE.findElementByName(DELETE_CHECKBOX).click();
    }
    popUp.clickOK();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    Sleep.trySleep();
  }

  /**
   * Execute the payment in.
   */
  public void execute() {
    final PaymentOutTab tab = selectTab();
    tab.execute();
  }

  /**
   * Execute the payment in.
   */
  public void execute(String number) {
    final PaymentOutTab tab = selectTab();
    tab.execute(number);
  }

  /**
   * Reactivate the payment in.
   *
   * @param action
   */
  public void reactivate(String action) {
    final PaymentOutTab tab = selectTab();
    tab.reactivate(action);
  }

  /**
   * Reverse the payment in.
   *
   * @param action
   *          The reverse action to request for the payment.
   * @param date
   *          The reverse date to request for the payment.
   */
  public void reverse(String action, String date) {
    final PaymentOutTab tab = selectTab();
    tab.reverse(action, date);
  }

  public void assertPaymentCreatedSuccessfully() {
    Sleep.trySleep(3000);
    selectTab().assertPaymentCreatedSuccessfully();
    logger.trace("-- Payment Created Successfully correctly asserted");
  }

  public void assertPaymentExecutedSuccessfully() {
    selectTab().assertPaymentExecutedSuccessfully();
    logger.trace("-- Payment Executed Successfully correctly asserted");
  }

  public void assertRefundedPaymentCreatedSuccessfully() {
    selectTab().assertRefundedPaymentCreatedSuccessfully();
  }

  public void assertFinancialAccountInactive(String financialAccount) {
    selectTab().assertFinancialAccountInactive(financialAccount);
  }

  public void assertPaymentMethodInactive(String paymentMethod) {
    selectTab().assertPaymentMethodInactive(paymentMethod);
  }

  public void assertFinancialAccountAndPaymentMethodInactive(String financialAccount,
      String paymentMethod) {
    selectTab().assertFinancialAccountAndPaymentMethodInactive(financialAccount, paymentMethod);
  }

  public FormItem getField(String fieldName) {
    return this.getField(fieldName);
  }

  /**
   * Assert display logic visibility from AddPayment Popup
   *
   * @param visibleDisplayLogic
   *          The AddPaymentDisplayLogic with the fields whose visibility has to be asserted.
   */
  public void assertAddPaymentDisplayLogicVisible(AddPaymentDisplayLogic visibleDisplayLogic) {
    logger.info("Asserting display logic visibility from AddPayment Popup.");
    final PaymentOutTab tab = selectTab();
    tab.assertAddPaymentDisplayLogicVisible(visibleDisplayLogic);
  }

  /**
   * Assert display logic enable status from AddPayment Popup
   *
   * @param enabledDisplayLogic
   *          DataOject with the fields whose enable status has to be asserted.
   */
  public void assertAddPaymentDisplayLogicEnabled(AddPaymentDisplayLogic enabledDisplayLogic) {
    logger.info("Asserting display logic enable status from AddPayment Popup.");
    final PaymentOutTab tab = selectTab();
    tab.assertAddPaymentDisplayLogicEnabled(enabledDisplayLogic);
  }

  /**
   * Assert display logic expanded status from AddPayment Popup
   *
   * @param expandedDisplayLogic
   *          DataOject with the fields whose expanded status has to be asserted.
   */
  public void assertAddPaymentDisplayLogicExpanded(AddPaymentDisplayLogic expandedDisplayLogic) {
    logger.info("Asserting display logic expanded status from AddPayment Popup.");
    final PaymentOutTab tab = selectTab();
    tab.assertAddPaymentDisplayLogicExpanded(expandedDisplayLogic);
  }

  /**
   * Executes and verifies actions on the Payment Out Lines tab.
   *
   * @author elopio
   *
   */
  public class Lines extends SmokeTabScript<PaymentOutLinesData, PaymentOutLinesTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Lines(MainPage mainPage) {
      super(mainPage);
    }

    public void clearFilters() {
      PaymentOutLinesTab tab = selectTab();
      tab.clearFilters();
    }

    /**
     * Select and return the Lines tab.
     *
     * Before this, the Payment Out window must be opened and this tab has to be accessible from the
     * currently selected one.
     *
     * @return the Lines tab GUI object.
     */
    @Override
    protected PaymentOutLinesTab selectTab() {
      PaymentOutWindow window = (PaymentOutWindow) mainPage.getView(PaymentOutWindow.TITLE);
      PaymentOutLinesTab tab = (PaymentOutLinesTab) window.selectTab(PaymentOutLinesTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }

  }

  /**
   * Executes and verifies actions on the PaymentOut Exchange rates tab.
   *
   * @author David Miguélez
   *
   */
  public class ExchangeRates extends SmokeTabScript<ExchangeRatesData, ExchangeRatesTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public ExchangeRates(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Exchange rates tab.
     *
     * Before this, the PaymentOut window must be opened and this tab has to be accessible from the
     * currently selected one.
     *
     * @return the Header tab GUI object.
     */
    @Override
    protected ExchangeRatesTab selectTab() {
      PaymentOutWindow window = (PaymentOutWindow) mainPage.getView(PaymentOutWindow.TITLE);
      ExchangeRatesTab tab = (ExchangeRatesTab) window.selectTab(ExchangeRatesTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }

  }

  public class UsedCreditSource extends SmokeTabScript<UsedCreditSourceData, UsedCreditSourceTab> {

    public UsedCreditSource(MainPage mainPage) {
      super(mainPage);
    }

    @Override
    protected UsedCreditSourceTab selectTab() {
      PaymentOutWindow window = (PaymentOutWindow) mainPage.getView(PaymentOutWindow.TITLE);
      UsedCreditSourceTab tab = (UsedCreditSourceTab) window
          .selectTab(UsedCreditSourceTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }
  }
}
