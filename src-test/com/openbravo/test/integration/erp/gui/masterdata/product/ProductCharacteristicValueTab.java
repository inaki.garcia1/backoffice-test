package com.openbravo.test.integration.erp.gui.masterdata.product;

import com.openbravo.test.integration.erp.data.masterdata.product.ProductCharacteristicValueData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;

/**
 * Executes and verifies actions on OpenbravoERP Product Characteristic - Value tab.
 *
 * @author jbueno
 *
 */
public class ProductCharacteristicValueTab extends GeneratedTab<ProductCharacteristicValueData> {

  /** The tab title. */
  static final String TITLE = "Value";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  static final String IDENTIFIER = "367D9DF685DA42A38CAE641D30A0BEBF";
  /** The tab level. */
  private static final int LEVEL = 1;

  public ProductCharacteristicValueTab(GeneratedTab<?> parentTab) {
    super(TITLE, IDENTIFIER, LEVEL, parentTab);
  }

}
