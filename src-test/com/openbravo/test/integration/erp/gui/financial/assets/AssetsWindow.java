/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.assets;

import com.openbravo.test.integration.erp.modules.client.application.gui.StandardWindow;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;

/**
 * Executes and verify actions on the Assets standard window of OpenbravoERP.
 *
 * @author inigo.sanchez
 *
 */
public class AssetsWindow extends StandardWindow {

  /** The window title. */
  @SuppressWarnings("hiding")
  public static final String TITLE = "Assets";
  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.FINANCIAL_MANAGEMENT, Menu.ASSETS,
      Menu.ASSETS };

  /**
   * Class constructor.
   *
   */
  public AssetsWindow() {
    super(TITLE, MENU_PATH);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void load() {
    AssetsHeaderTab headerTab = new AssetsHeaderTab();
    addTopLevelTab(headerTab);
    super.load();
  }

  /**
   * Select and return the Assets header tab.
   *
   * @return the Assets header tab.
   */
  public AssetsHeaderTab selectHeaderTab() {
    return (AssetsHeaderTab) selectTab(AssetsHeaderTab.IDENTIFIER);
  }

}
