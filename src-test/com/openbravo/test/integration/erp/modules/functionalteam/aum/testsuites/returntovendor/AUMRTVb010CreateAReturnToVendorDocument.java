/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2018 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2016-03-03 21:37:14
 * Contributor(s):
 *   Nono Carballo <nonofce@gmail.com>
 *************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.returntovendor;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ReturnToVendorLineSelectorData;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.ReturnToVendor;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.GoodsShipment;
import com.openbravo.test.integration.selenium.Sleep;

/**
 *
 * Class for Test AUMRTVb010 Create a Return to vendor document
 *
 * @author Nono Carballo
 *
 */
@RunWith(Parameterized.class)
public class AUMRTVb010CreateAReturnToVendorDocument extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager
      .getLogger();

  /** Data for Test */
  GoodsReceiptHeaderData goodsReceiptHeader;
  GoodsReceiptLinesData goodsReceiptLine;
  /** The return to vendor header data. */
  ReturnToVendorHeaderData returnToVendorHeaderData;
  /** The data to verify the return to vendor header creation. */
  ReturnToVendorHeaderData returnToVendorHeaderVerificationData;
  /** The booked return to vendor verification data. */
  ReturnToVendorHeaderData bookedReturnToVendorHeaderVerificationData;

  GoodsShipmentHeaderData goodsShipmentHeaderData;
  GoodsShipmentLinesData goodsShipmentLinesData;

  public AUMRTVb010CreateAReturnToVendorDocument(GoodsReceiptHeaderData goodsReceiptHeader,
      GoodsReceiptLinesData goodsReceiptLine, ReturnToVendorHeaderData returnToVendorHeaderData,
      ReturnToVendorHeaderData returnToVendorHeaderVerificationData,
      ReturnToVendorHeaderData bookedReturnToVendorHeaderVerificationData,
      GoodsShipmentHeaderData goodsShipmentHeaderData,
      GoodsShipmentLinesData goodsShipmentLinesData) {
    this.goodsReceiptHeader = goodsReceiptHeader;
    this.goodsReceiptLine = goodsReceiptLine;
    this.returnToVendorHeaderData = returnToVendorHeaderData;
    this.returnToVendorHeaderVerificationData = returnToVendorHeaderVerificationData;
    this.bookedReturnToVendorHeaderVerificationData = bookedReturnToVendorHeaderVerificationData;
    this.goodsShipmentHeaderData = goodsShipmentHeaderData;
    this.goodsShipmentLinesData = goodsShipmentLinesData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  @Parameters
  public static Collection<Object[]> returnToVendorValues() {
    Object[][] data = new Object[][] { {
        new GoodsReceiptHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .build(),
        new GoodsReceiptLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("Basket Ball").build())
            .operativeQuantity("10")
            .storageBin("L02")
            .build(),
        new ReturnToVendorHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .build(),
        new ReturnToVendorHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Pamplona")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .transactionDocument("RTV Order")
            .warehouse("Spain warehouse")
            .paymentMethod("1 (Spain)")
            .paymentTerms("30 days, 5")
            .priceList("Sport items")
            .grandTotalAmount("0.00")
            .documentStatus("Draft")
            .build(),
        new ReturnToVendorHeaderData.Builder().documentStatus("Booked").build(),

        new GoodsShipmentHeaderData.Builder().organization("Spain")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),

        new GoodsShipmentLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("Basket Ball").build())
            .operativeQuantity("9")
            .storageBin("L02")
            .build() } };
    return Arrays.asList(data);
  }

  @Test
  public void AUMRTVb010test() throws ParseException {
    logger.debug("** Start test case [AUMRTVb010] Create a Return to vendor document **");

    GoodsReceipt goodsReceipt = new GoodsReceipt(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    goodsReceipt.create(goodsReceiptHeader);
    goodsReceipt.assertSaved();
    GoodsReceipt.Lines goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.create(goodsReceiptLine);
    goodsReceiptLines.assertSaved();
    goodsReceipt.complete();
    goodsReceipt.assertProcessCompletedSuccessfully2();
    String documentNo = (String) goodsReceipt.getData("documentNo");

    final ReturnToVendor returnToVendor = (ReturnToVendor) new ReturnToVendor(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    returnToVendor.create(returnToVendorHeaderData);
    returnToVendor.assertSaved();
    returnToVendor.assertData(returnToVendorHeaderVerificationData);
    PickAndExecuteWindow<ReturnToVendorLineSelectorData> popup = returnToVendor
        .openPickAndEditLines();
    popup.filter(new ReturnToVendorLineSelectorData.Builder().product("Basket Ball")
        .shipmentNumber(documentNo)
        .build());
    popup.edit(new ReturnToVendorLineSelectorData.Builder().returned("8")
        .returnedUOM("Ounce")
        .returnReason("Defective")
        .build());
    popup.process();
    returnToVendor.book();
    returnToVendor.assertProcessCompletedSuccessfully2();
    returnToVendor.assertData(bookedReturnToVendorHeaderVerificationData);

    // Deliver the qty to correctly execute the test AUMRTVb030test
    GoodsShipment goodsShipment = new GoodsShipment(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    goodsShipment.create(goodsShipmentHeaderData);
    goodsShipment.assertSaved();

    GoodsShipment.Lines goodsShipmentLines = goodsShipment.new Lines(mainPage);
    goodsShipmentLines.create(goodsShipmentLinesData);
    goodsShipmentLines.assertSaved();

    goodsShipment.complete();
    goodsShipment.assertProcessCompletedSuccessfully2();

    logger.debug("** End test case [AUMRTVb010] Create a Return to vendor document **");
  }

}
