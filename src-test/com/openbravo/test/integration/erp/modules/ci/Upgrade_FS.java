/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Juan Pablo Aroztegi <juanpablo.aroztegi@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.ci;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.common.TestFiles;
import com.openbravo.test.integration.erp.common.TimeoutConstants;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.generalsetup.ModuleReferenceData;
import com.openbravo.test.integration.erp.gui.LoginPage;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.modulemanagement.ModuleManagement;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Execute the upgrade test using the file system to get the OBXs. 1. Select OBXs 2. Install 3.
 * Rebuild
 */
@RunWith(Parameterized.class)
public class Upgrade_FS extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  String fileObxList;
  Vector<String> obxList;

  /**
   * Class constructor.
   *
   * @throws IOException
   */
  public Upgrade_FS(String fileObxList) throws IOException {
    obxList = new Vector<String>();
    BufferedReader bf = new BufferedReader(
        new FileReader(TestFiles.getRelativeFilePath(fileObxList)));
    String line;
    while ((line = bf.readLine()) != null) {
      obxList.add(line);
    }
    bf.close();
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   * @throws IOException
   *
   */
  @Parameters
  public static Collection<Object[]> setupProductValues() throws IOException {
    return Arrays.asList(new Object[][] { {

        // File with the list of OBXs ordered to install
        "fileObxList"

        } });
  }

  /**
   * Test the upgrade.
   *
   * @throws IOException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void upgrade_FS() throws IOException, OpenbravoERPTestException {
    logger.info("** Start of test case [Upgrade_FS] Ugrade by file system **");

    boolean isRebuildRequired = false;
    String obxFilePath;
    Iterator<String> modulesList = obxList.iterator();
    while (modulesList.hasNext()) {
      obxFilePath = modulesList.next();
      ModuleReferenceData module = new ModuleReferenceData.Builder().obxPath(obxFilePath).build();
      if (ModuleManagement.AddModules.installModuleFromFileSystem(mainPage, module)) {
        if (!isRebuildRequired) {
          isRebuildRequired = true;
        }
      }
    }
    final LoginPage loginPage = new LoginPage();
    LoginPage.LOGIN_URL = "/security/Login";
    LoginPage.upgraded = true;
    if (isRebuildRequired) {
      ModuleManagement.InstalledModules.rebuildSystemAndRestartServletContainer(mainPage);
      loginPage.waitForFieldsToAppear(TimeoutConstants.REBUILD_TIMEOUT);
    } else {
      mainPage.quit();
    }
    logger.info("** End of test case [Upgrade_FS] Ugrade by file system **");
    loginPage.verifyLogout();
  }

  /**
   * Logout from Openbravo.
   */
  @Override
  public void logout() {
    // Do nothing. The rebuild will logout the user.
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
