/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.security.user;

import com.openbravo.test.integration.erp.data.generalsetup.security.user.UserRolesData;
import com.openbravo.test.integration.erp.gui.general.security.user.classic.UserRolesTab;
import com.openbravo.test.integration.erp.gui.general.security.user.classic.UserWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the User Roles tab of OpenbravoERP.
 *
 * @author Pablo Lujan
 */
public class UserRolesClassicScript {

  /**
   * Creates the user roles.
   *
   * @param mainPage
   *          The application main page.
   * @param userRolesData
   *          User roles data
   */
  public static void create(MainPage mainPage, UserRolesData userRolesData) {
    UserWindow userWindow = new UserWindow();
    mainPage.openView(userWindow);

    UserRolesTab userRolesTab = userWindow.selectUserRolesTab();
    userRolesTab.create(userRolesData);
    userRolesTab.verifySave();
  }
}
