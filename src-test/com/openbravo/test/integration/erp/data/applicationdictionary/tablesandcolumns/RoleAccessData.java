/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.applicationdictionary.tablesandcolumns;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for RoleAccessData
 *
 * @author plujan
 *
 */
public class RoleAccessData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the table value
     *
     * Description: A dictionary table used for this tab that points to the database table.
     *
     * @param value
     *          The table value.
     * @return The builder for this class.
     */
    public Builder table(String value) {
      this.dataFields.put("table", value);
      return this;
    }

    /**
     * Set the exclude value
     *
     * Description: A selection permitting or denying a role access to specified data.
     *
     * @param value
     *          The exclude value.
     * @return The builder for this class.
     */
    public Builder exclude(Boolean value) {
      this.dataFields.put("exclude", value);
      return this;
    }

    /**
     * Set the role value
     *
     * Description: The profile of security for the user defining what windows and tabs they can
     * see.
     *
     * @param value
     *          The role value.
     * @return The builder for this class.
     */
    public Builder role(String value) {
      this.dataFields.put("role", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the readOnly value
     *
     * Description: An object which may only be viewed not edited.
     *
     * @param value
     *          The readOnly value.
     * @return The builder for this class.
     */
    public Builder readOnly(Boolean value) {
      this.dataFields.put("readOnly", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public RoleAccessData build() {
      return new RoleAccessData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private RoleAccessData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
