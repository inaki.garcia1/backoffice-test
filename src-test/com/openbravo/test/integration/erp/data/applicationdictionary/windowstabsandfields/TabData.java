/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.applicationdictionary.windowstabsandfields;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for TabData
 *
 * @author plujan
 *
 */
public class TabData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the uIPattern value
     *
     * Description: Defines the UI Pattern
     *
     * @param value
     *          The uIPattern value.
     * @return The builder for this class.
     */
    public Builder uIPattern(String value) {
      this.dataFields.put("uIPattern", value);
      return this;
    }

    /**
     * Set the hqlwhereclause value
     *
     * Description: Hqlwhereclause
     *
     * @param value
     *          The hqlwhereclause value.
     * @return The builder for this class.
     */
    public Builder hqlwhereclause(String value) {
      this.dataFields.put("hqlwhereclause", value);
      return this;
    }

    /**
     * Set the hqlfilterclause value
     *
     * Description: Hqlfilterclause
     *
     * @param value
     *          The hqlfilterclause value.
     * @return The builder for this class.
     */
    public Builder hqlfilterclause(String value) {
      this.dataFields.put("hqlfilterclause", value);
      return this;
    }

    /**
     * Set the module value
     *
     * Description: Module
     *
     * @param value
     *          The module value.
     * @return The builder for this class.
     */
    public Builder module(String value) {
      this.dataFields.put("module", value);
      return this;
    }

    /**
     * Set the hqlorderbyclause value
     *
     * Description: Hqlorderbyclause
     *
     * @param value
     *          The hqlorderbyclause value.
     * @return The builder for this class.
     */
    public Builder hqlorderbyclause(String value) {
      this.dataFields.put("hqlorderbyclause", value);
      return this;
    }

    /**
     * Set the masterDetailForm value
     *
     * Description: The form referenced in the edition command
     *
     * @param value
     *          The masterDetailForm value.
     * @return The builder for this class.
     */
    public Builder masterDetailForm(String value) {
      this.dataFields.put("masterDetailForm", value);
      return this;
    }

    /**
     * Set the filterClause value
     *
     * Description: Filter clause
     *
     * @param value
     *          The filterClause value.
     * @return The builder for this class.
     */
    public Builder filterClause(String value) {
      this.dataFields.put("filterClause", value);
      return this;
    }

    /**
     * Set the includedTab value
     *
     * Description: Included Tab in this Tab (Master Dateail)
     *
     * @param value
     *          The includedTab value.
     * @return The builder for this class.
     */
    public Builder includedTab(String value) {
      this.dataFields.put("includedTab", value);
      return this;
    }

    /**
     * Set the orderColumn value
     *
     * Description: Column determining the order
     *
     * @param value
     *          The orderColumn value.
     * @return The builder for this class.
     */
    public Builder orderColumn(String value) {
      this.dataFields.put("orderColumn", value);
      return this;
    }

    /**
     * Set the includedColumn value
     *
     * Description: Column determining if a Table Column is included in Ordering
     *
     * @param value
     *          The includedColumn value.
     * @return The builder for this class.
     */
    public Builder includedColumn(String value) {
      this.dataFields.put("includedColumn", value);
      return this;
    }

    /**
     * Set the sequenceTab value
     *
     * Description: The Tab determines the Order
     *
     * @param value
     *          The sequenceTab value.
     * @return The builder for this class.
     */
    public Builder sequenceTab(Boolean value) {
      this.dataFields.put("sequenceTab", value);
      return this;
    }

    /**
     * Set the tabLevel value
     *
     * Description: Hierarchical Tab Level (0 = top)
     *
     * @param value
     *          The tabLevel value.
     * @return The builder for this class.
     */
    public Builder tabLevel(String value) {
      this.dataFields.put("tabLevel", value);
      return this;
    }

    /**
     * Set the image value
     *
     * Description: A visual picture used to describe an item.
     *
     * @param value
     *          The image value.
     * @return The builder for this class.
     */
    public Builder image(String value) {
      this.dataFields.put("image", value);
      return this;
    }

    /**
     * Set the process value
     *
     * Description: A series of actions carried out in sequential order.
     *
     * @param value
     *          The process value.
     * @return The builder for this class.
     */
    public Builder process(String value) {
      this.dataFields.put("process", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the sQLWhereClause value
     *
     * Description: A specification of the SQL WHERE clause used for permanently filtering displayed
     * data.
     *
     * @param value
     *          The sQLWhereClause value.
     * @return The builder for this class.
     */
    public Builder sQLWhereClause(String value) {
      this.dataFields.put("sQLWhereClause", value);
      return this;
    }

    /**
     * Set the sQLOrderByClause value
     *
     * Description: A specification of the SQL ORDER BY clause used for a displayed default sort of
     * records.
     *
     * @param value
     *          The sQLOrderByClause value.
     * @return The builder for this class.
     */
    public Builder sQLOrderByClause(String value) {
      this.dataFields.put("sQLOrderByClause", value);
      return this;
    }

    /**
     * Set the column value
     *
     * Description: A link to the database column of the table.
     *
     * @param value
     *          The column value.
     * @return The builder for this class.
     */
    public Builder column(String value) {
      this.dataFields.put("column", value);
      return this;
    }

    /**
     * Set the translationTab value
     *
     * Description: This tab contains translation information
     *
     * @param value
     *          The translationTab value.
     * @return The builder for this class.
     */
    public Builder translationTab(Boolean value) {
      this.dataFields.put("translationTab", value);
      return this;
    }

    /**
     * Set the accountingTab value
     *
     * Description: This tab contain accounting information
     *
     * @param value
     *          The accountingTab value.
     * @return The builder for this class.
     */
    public Builder accountingTab(Boolean value) {
      this.dataFields.put("accountingTab", value);
      return this;
    }

    /**
     * Set the treeIncluded value
     *
     * Description: Window has Tree Graph
     *
     * @param value
     *          The treeIncluded value.
     * @return The builder for this class.
     */
    public Builder treeIncluded(Boolean value) {
      this.dataFields.put("treeIncluded", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the table value
     *
     * Description: A dictionary table used for this tab that points to the database table.
     *
     * @param value
     *          The table value.
     * @return The builder for this class.
     */
    public Builder table(String value) {
      this.dataFields.put("table", value);
      return this;
    }

    /**
     * Set the defaultEditMode value
     *
     * Description: Default for toggle between Single- and Multi-Row (Grid) Layout
     *
     * @param value
     *          The defaultEditMode value.
     * @return The builder for this class.
     */
    public Builder defaultEditMode(Boolean value) {
      this.dataFields.put("defaultEditMode", value);
      return this;
    }

    /**
     * Set the sequenceNumber value
     *
     * Description: The order of records in a specified document.
     *
     * @param value
     *          The sequenceNumber value.
     * @return The builder for this class.
     */
    public Builder sequenceNumber(String value) {
      this.dataFields.put("sequenceNumber", value);
      return this;
    }

    /**
     * Set the helpComment value
     *
     * Description: A comment that adds additional information to help users work with fields.
     *
     * @param value
     *          The helpComment value.
     * @return The builder for this class.
     */
    public Builder helpComment(String value) {
      this.dataFields.put("helpComment", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the window value
     *
     * Description: A work area which can be used to create view edit and process a record.
     *
     * @param value
     *          The window value.
     * @return The builder for this class.
     */
    public Builder window(String value) {
      this.dataFields.put("window", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public TabData build() {
      return new TabData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private TabData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
