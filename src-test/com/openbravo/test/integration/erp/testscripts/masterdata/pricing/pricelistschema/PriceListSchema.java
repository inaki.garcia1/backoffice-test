/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.masterdata.pricing.pricelistschema;

import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelistschema.PriceListSchemaHeaderData;
import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelistschema.PriceListSchemaLinesData;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;

/**
 * Executes and verifies actions on the Price List Schema classic window of OpenbravoERP.
 *
 * @author Pablo Lujan
 */
public class PriceListSchema {

  /**
   * Executes and verifies actions on the Price List Schema Header tab of OpenbravoERP.
   *
   * @author plujan
   *
   */
  public static class Header {

    /**
     * Creates the record.
     *
     * @param mainPage
     *          The application main page.
     * @param priceListSchemaHeaderData
     *          Price List Schema Header data.
     */
    public static void create(MainPage mainPage,
        PriceListSchemaHeaderData priceListSchemaHeaderData) {
      if (mainPage.isOnNewLayout()) {
        PriceListSchemaHeaderScript.create(mainPage, priceListSchemaHeaderData);
      } else {
        PriceListSchemaHeaderClassicScript.create(mainPage, priceListSchemaHeaderData);
      }
    }

    /**
     * Select the record.
     *
     * @param mainPage
     *          The application main page.
     * @param priceListSchemaHeaderData
     *          Price List Schema Header data.
     */
    public static void select(MainPage mainPage,
        PriceListSchemaHeaderData priceListSchemaHeaderData) {
      if (mainPage.isOnNewLayout()) {
        PriceListSchemaHeaderScript.select(mainPage, priceListSchemaHeaderData);
      } else {
        PriceListSchemaHeaderClassicScript.select(mainPage, priceListSchemaHeaderData);
      }
    }
  }

  /**
   * Executes and verifies actions on the Price List Schema Lines tab of OpenbravoERP.
   *
   * @author plujan
   *
   */
  public static class Lines {

    /**
     * Creates the record.
     *
     * @param mainPage
     *          The application main page.
     * @param priceListSchemaLinesData
     *          Price List Schema Header Line data
     * @throws OpenbravoERPTestException
     */
    public static void create(MainPage mainPage, PriceListSchemaLinesData priceListSchemaLinesData)
        throws OpenbravoERPTestException {
      if (mainPage.isOnNewLayout()) {
        PriceListSchemaLinesScript.create(mainPage, priceListSchemaLinesData);
      } else {
        PriceListSchemaLinesClassicScript.create(mainPage, priceListSchemaLinesData);
      }
    }

  }

}
