/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac Collazo <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions5;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddTransactionData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.bankstatement.BankStatementHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.BankStatementLinesData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddTransactionProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 35477
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class APRRegression35477Out extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  TransactionsData transactionData;
  TransactionsData transactionVerificationData;
  TransactionsData paymentAmountData;
  TransactionsData paymentAmountVerificationData;
  TransactionsData withdrawalAmountData;
  TransactionsData withdrawalAmountVerificationData;
  BankStatementHeaderData bankStatementHeaderData;
  BankStatementHeaderData bankStatementHeaderVerificationData;
  BankStatementLinesData bankStatementLinesData;
  BankStatementLinesData bankStatementLinesVerificationData;
  AddTransactionData addTransactionWithdrawlDataVerification;

  /**
   * Class constructor.
   *
   */
  public APRRegression35477Out(TransactionsData transactionData,
      TransactionsData transactionVerificationData, TransactionsData paymentAmountData,
      TransactionsData paymentAmountVerificationData, TransactionsData withdrawalAmountData,
      TransactionsData withdrawalAmountVerificationData,
      BankStatementHeaderData bankStatementHeaderData,
      BankStatementHeaderData bankStatementHeaderVerificationData,
      BankStatementLinesData bankStatementLinesData,
      BankStatementLinesData bankStatementLinesVerificationData,
      AddTransactionData addTransactionWithdrawlDataVerification) {
    this.transactionData = transactionData;
    this.transactionVerificationData = transactionVerificationData;
    this.paymentAmountData = paymentAmountData;
    this.paymentAmountVerificationData = paymentAmountVerificationData;
    this.withdrawalAmountData = withdrawalAmountData;
    this.withdrawalAmountVerificationData = withdrawalAmountVerificationData;
    this.bankStatementHeaderData = bankStatementHeaderData;
    this.bankStatementHeaderVerificationData = bankStatementHeaderVerificationData;
    this.bankStatementLinesData = bankStatementLinesData;
    this.bankStatementLinesVerificationData = bankStatementLinesVerificationData;
    this.addTransactionWithdrawlDataVerification = addTransactionWithdrawlDataVerification;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {

        new TransactionsData.Builder().transactionType("Bank fee")
            .paymentAmount("20.00")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),

        new TransactionsData.Builder().transactionType("Bank fee")
            .paymentAmount("20.00")
            .depositAmount("0.00")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),

        new TransactionsData.Builder().paymentAmount("10.00").build(),

        new TransactionsData.Builder().paymentAmount("10.00").depositAmount("0.00").build(),

        new TransactionsData.Builder().paymentAmount("10.00").build(),

        new TransactionsData.Builder().paymentAmount("10.00").depositAmount("0.00").build(),

        new BankStatementHeaderData.Builder().name("BankExample")
            .transactionDate(OBDate.addMonthsToSystemDate(1))
            .importDate(OBDate.addMonthsToSystemDate(1))
            .build(),

        new BankStatementHeaderData.Builder().name("BankExample")
            .documentType("Bank Statement File")
            .build(),

        new BankStatementLinesData.Builder().dramount("10.00")
            .transactionDate(OBDate.addMonthsToSystemDate(1))
            .build(),

        new BankStatementLinesData.Builder().dramount("10.00").cramount("0.00").build(),

        new AddTransactionData.Builder().withdrawalamt("10.00").depositamt("0.00").build(),

        } };
    return Arrays.asList(data);
  }

  /**
   *
   *
   *
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression35477OutTest() throws ParseException {
    logger.info("** Start of test case [APRRegression35477Out] Test regression 35477Out **");

    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.filter(new AccountData.Builder().name("Spain Bank").build());

    String date = OBDate.getSystemDate();

    FinancialAccount.Transaction transactions = financialAccount.new Transaction(mainPage);
    transactionData.addDataField("transactionDate", OBDate.addDaysToDate(date, 1));
    transactionData.addDataField("dateAcct", OBDate.addDaysToDate(date, 1));
    transactions.create(transactionData);
    transactions.assertSaved();
    transactions.assertData(transactionVerificationData);

    transactions.edit(paymentAmountData);
    transactions.assertSaved();
    transactions.assertData(paymentAmountVerificationData);

    transactions.edit(withdrawalAmountData);
    transactions.assertSaved();
    transactions.assertData(withdrawalAmountData);

    transactions.edit(paymentAmountData);
    transactions.assertSaved();
    transactions.assertData(paymentAmountVerificationData);
    transactions.process();
    transactions.assertProcessCompletedSuccessfully2();
    transactions.assertData(new TransactionsData.Builder().status("Withdrawn not Cleared").build());

    String faName = "Issue35477Purchase";
    bankStatementLinesData.addDataField("referenceNo", faName);

    FinancialAccount.BankStatements bankStatement = financialAccount.new BankStatements(mainPage);
    bankStatement.create(bankStatementHeaderData);
    bankStatement.assertSaved();
    bankStatement.assertData(bankStatementHeaderData);

    FinancialAccount.BankStatements.BankStatementLines bankStatementLines = bankStatement.new BankStatementLines(
        mainPage);
    bankStatementLines.create(bankStatementLinesData);
    bankStatementLines.assertSaved();
    bankStatementLines.assertData(bankStatementLinesData);
    bankStatement.process();
    bankStatement.assertProcessCompletedSuccessfully2();

    @SuppressWarnings("rawtypes")
    MatchStatementProcess matchStatementProcess = financialAccount.openMatchStatement(false);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("referenceNo", faName);

    AddTransactionProcess transactionProcess = matchStatementProcess.clickAdd(0);

    transactionProcess.setParameterValue("trxtype", "Bank fee");
    transactionProcess.assertData(addTransactionWithdrawlDataVerification);

    transactionProcess.setParameterValue("trxtype", "BP Withdrawal");
    transactionProcess.setParameterValue("description", "");
    transactionProcess.assertData(addTransactionWithdrawlDataVerification);

    logger.info("** End of test case [APRRegression35477Out] Test regression 35477 **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
