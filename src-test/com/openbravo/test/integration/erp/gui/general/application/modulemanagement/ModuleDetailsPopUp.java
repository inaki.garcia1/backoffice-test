/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.application.modulemanagement;

import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicButtonPopUp;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions on module details pop up.
 *
 * @author elopio
 *
 */
public class ModuleDetailsPopUp extends OBClassicButtonPopUp {

  /** Identifier of the module version label. */
  private static final String LABEL_MODULE_VERSION = "moduleVersion";

  /**
   * Class constructor.
   *
   */
  public ModuleDetailsPopUp() {
    super();
  }

  /**
   * Get the version of the module.
   *
   * @return the version of the module.
   */
  public String getModuleVersion() {
    selectPopUpWithOKButton();
    return SeleniumSingleton.INSTANCE.findElementById(LABEL_MODULE_VERSION).getText();
  }

  /**
   * Click OK button and wait for the page to load
   */
  @Override
  public void clickOK() {
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_OK).click();
  }

}
