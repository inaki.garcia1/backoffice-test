/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Rafael Queralta <rafaelcuba81@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.enterprise.organization;

import com.openbravo.test.integration.erp.data.warehouse.setup.warehouseandstoragebins.WarehouseData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;

/**
 * Executes and verifies actions on OpenbravoERP Warehouse tab.
 *
 * @author elopio
 *
 */
public class WarehouseTab extends GeneratedTab<WarehouseData> {

  /** The tab title. */
  static final String TITLE = "Warehouse";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "9F030341690C4BB3A3C15835AEC0FF39";
  /** The tab level. */
  private static final int LEVEL = 1;

  /**
   * Class constructor.
   *
   */
  public WarehouseTab(GeneratedTab<?> parentTab) {
    super(TITLE, IDENTIFIER, LEVEL, parentTab);
  }
}
