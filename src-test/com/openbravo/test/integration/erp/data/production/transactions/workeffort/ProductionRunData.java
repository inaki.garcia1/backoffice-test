/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.production.transactions.workeffort;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ProductionRunData
 *
 * @author plujan
 *
 */
public class ProductionRunData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the costCenterVersion value
     *
     * Description: The cost center being used during a specified time period.
     *
     * @param value
     *          The costCenterVersion value.
     * @return The builder for this class.
     */
    public Builder costCenterVersion(String value) {
      this.dataFields.put("costCenterVersion", value);
      return this;
    }

    /**
     * Set the outsourced value
     *
     * Description: A decision to have a task or phase completed by an external business partner.
     *
     * @param value
     *          The outsourced value.
     * @return The builder for this class.
     */
    public Builder outsourced(Boolean value) {
      this.dataFields.put("outsourced", value);
      return this;
    }

    /**
     * Set the conversionRate value
     *
     * Description: Defines the conversion between Quantity and Process Quantity
     *
     * @param value
     *          The conversionRate value.
     * @return The builder for this class.
     */
    public Builder conversionRate(String value) {
      this.dataFields.put("conversionRate", value);
      return this;
    }

    /**
     * Set the processQuantity value
     *
     * Description: The number of Process Units required to be produced.
     *
     * @param value
     *          The processQuantity value.
     * @return The builder for this class.
     */
    public Builder processQuantity(String value) {
      this.dataFields.put("processQuantity", value);
      return this;
    }

    /**
     * Set the processUnit value
     *
     * Description: The name of the main final product obtained by executing a process plan.
     *
     * @param value
     *          The processUnit value.
     * @return The builder for this class.
     */
    public Builder processUnit(String value) {
      this.dataFields.put("processUnit", value);
      return this;
    }

    /**
     * Set the costCenterUse value
     *
     * Description: Cost Center Use
     *
     * @param value
     *          The costCenterUse value.
     * @return The builder for this class.
     */
    public Builder costCenterUse(String value) {
      this.dataFields.put("costCenterUse", value);
      return this;
    }

    /**
     * Set the processed value
     *
     * Description: A confirmation that the associated documents or requests are processed.
     *
     * @param value
     *          The processed value.
     * @return The builder for this class.
     */
    public Builder processed(Boolean value) {
      this.dataFields.put("processed", value);
      return this;
    }

    /**
     * Set the rejectedQuantity value
     *
     * Description: Rejected Quantity
     *
     * @param value
     *          The rejectedQuantity value.
     * @return The builder for this class.
     */
    public Builder rejectedQuantity(String value) {
      this.dataFields.put("rejectedQuantity", value);
      return this;
    }

    /**
     * Set the requiredQuantity value
     *
     * Description: Required Quantity
     *
     * @param value
     *          The requiredQuantity value.
     * @return The builder for this class.
     */
    public Builder requiredQuantity(String value) {
      this.dataFields.put("requiredQuantity", value);
      return this;
    }

    /**
     * Set the lineNo value
     *
     * Description: A line stating the position of this request in the document.
     *
     * @param value
     *          The lineNo value.
     * @return The builder for this class.
     */
    public Builder lineNo(String value) {
      this.dataFields.put("lineNo", value);
      return this;
    }

    /**
     * Set the wRPhase value
     *
     * Description: WR Phase
     *
     * @param value
     *          The wRPhase value.
     * @return The builder for this class.
     */
    public Builder wRPhase(String value) {
      this.dataFields.put("wRPhase", value);
      return this;
    }

    /**
     * Set the production value
     *
     * Description: An indication that an item is being used in production.
     *
     * @param value
     *          The production value.
     * @return The builder for this class.
     */
    public Builder production(String value) {
      this.dataFields.put("production", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the productionQuantity value
     *
     * Description: Production Quantity
     *
     * @param value
     *          The productionQuantity value.
     * @return The builder for this class.
     */
    public Builder productionQuantity(String value) {
      this.dataFields.put("productionQuantity", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ProductionRunData build() {
      return new ProductionRunData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ProductionRunData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
