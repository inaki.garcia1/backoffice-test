/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.sales.transactions.goodsshipment;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.gui.popups.LocatorSelectorPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes actions on user Create Lines From Pop Up.
 *
 * @author elopio
 *
 */
public class CreateLinesFromPopUp extends OBClassicPopUp {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Identifier of the Order combo box. */
  private static final String COMBO_BOX_ORDER = "inpPurchaseOrder";
  /** Identifier of the lines check box. */
  private static final String CHECK_BOX_LINES = "inpId";

  /**
   * Class constructor.
   *
   */
  public CreateLinesFromPopUp() {
    super(TestRegistry
        .getObjectString("org.openbravo.classicpopup./GoodsShipment/Header_Edition.html"));
  }

  /**
   * Select the pop up frame.
   */
  @Override
  protected void selectFrame() {
    SeleniumSingleton.INSTANCE.switchTo().frame("process");
    SeleniumSingleton.INSTANCE.switchTo().frame("frameButton");
  }

  /**
   * Create lines from an order.
   *
   * @param warehouseAlias
   *          The alias of the warehouse.
   * @param order
   *          The order data. This is a string with the form: number - date - amount.
   */
  public void createLinesFrom(String warehouseAlias, String order) {
    createLinesFrom(warehouseAlias, order, 0);
  }

  /**
   * Create lines from an order.
   *
   * @param warehouseAlias
   *          The alias of the warehouse.
   * @param order
   *          The order data. This is a string with the form: number - date - amount.
   * @param lineNo
   *          The line number to select or 0 to select all
   */
  public void createLinesFrom(String warehouseAlias, String order, int lineNo) {
    selectPopUpWithOKButton();
    // TODO L1: Remove the following static sleep when no required
    Sleep.trySleep(1000);
    String windowHandle = SeleniumSingleton.INSTANCE.getWindowHandle();
    SeleniumSingleton.INSTANCE.findElementById(LocatorSelectorPopUp.BUTTON_LOCATOR).click();
    new LocatorSelectorPopUp().selectLocator(warehouseAlias);
    SeleniumSingleton.INSTANCE.switchTo().window(windowHandle);
    waitForFrame();
    // XXX This try/catch statement is a workaround to avoid issue
    // https://issues.openbravo.com/view.php?id=31374
    try {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_ORDER))
          .selectByVisibleText(order);
    } catch (NoSuchElementException nsee) {
      String tryOrder = order;
      int pos = tryOrder.length() - 1;
      if ((tryOrder.charAt(pos - 2) == '.') || (tryOrder.charAt(pos - 2) == ',')) {
        if (tryOrder.charAt(pos) == '0') {
          tryOrder = tryOrder.substring(0, pos);
          if (tryOrder.charAt(pos - 1) == '0') {
            // (pos - 2) to remove also the decimal separator (',' or '.')
            tryOrder = tryOrder.substring(0, pos - 2);
          }
        }
      }
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_ORDER))
          .selectByVisibleText(tryOrder);
      logger.warn("It has been necessary no manipulate order from {} to {} because of issue 31374",
          order, tryOrder);
    }
    Sleep.trySleep();
    int lineCount = 1;
    for (WebElement record : SeleniumSingleton.INSTANCE.findElementsByName(CHECK_BOX_LINES)) {
      if (lineCount == lineNo || lineNo == 0) {
        record.click();
      }
      lineCount++;
    }
    clickOK();
  }

}
