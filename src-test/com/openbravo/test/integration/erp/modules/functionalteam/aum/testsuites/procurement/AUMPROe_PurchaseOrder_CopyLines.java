/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Nono Carballo <nonofce@gmail.com>.
 ************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.procurement;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartner.BusinessPartnerData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.gui.masterdata.businesspartner.BusinessPartnerTab;
import com.openbravo.test.integration.erp.gui.masterdata.businesspartner.BusinessPartnerWindow;
import com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseorder.PurchaseOrderWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * AUMPROe Test case. Copy Lines
 *
 * @author nonofce
 *
 */

@RunWith(Parameterized.class)
public class AUMPROe_PurchaseOrder_CopyLines extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for [AUMPROe010] Create purchase order and Copy Lines. */
  /** The Business Partner Vendor A */
  BusinessPartnerData bpartnerVA;
  /** The comsumption days for VA */
  BusinessPartnerData bpartnerVAcomsumption;
  /** The purchase order header data. */
  PurchaseOrderHeaderData purchaseOrderHeaderData;

  public AUMPROe_PurchaseOrder_CopyLines(BusinessPartnerData bpartnerVA,
      BusinessPartnerData bpartnerVAcomsumption, PurchaseOrderHeaderData purchaseOrderHeaderData) {
    super();
    this.bpartnerVA = bpartnerVA;
    this.bpartnerVAcomsumption = bpartnerVAcomsumption;
    this.purchaseOrderHeaderData = purchaseOrderHeaderData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  @Parameters
  public static Collection<Object[]> createValues() {

    return Arrays.asList(new Object[][] { {
        // Parameters for [AUMPROe010]
        new BusinessPartnerData.Builder().searchKey("VA").build(),
        new BusinessPartnerData.Builder().consumptionDays("30").build(),
        new PurchaseOrderHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .build() } });
  }

  @Test
  public void AUMPROe_PurchaseOrder_CopyLines_test() {
    logger.info("** Start of test case [AUMPROe010] Create Purchase Order. **");

    final BusinessPartnerWindow bpwindow = new BusinessPartnerWindow();
    mainPage.openView(bpwindow);
    final BusinessPartnerTab bptab = bpwindow.selectBusinessPartnerTab();
    bptab.select(bpartnerVA);
    bptab.editRecord(bpartnerVAcomsumption);

    final PurchaseOrder purchaseOrder = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    purchaseOrder.create(purchaseOrderHeaderData);
    purchaseOrder.assertSaved();
    purchaseOrder.copyLines(true);
    final PurchaseOrder.Lines purchaseOrderLines = purchaseOrder.new Lines(mainPage);

    PurchaseOrderWindow window = (PurchaseOrderWindow) mainPage.getView(PurchaseOrderWindow.TITLE);
    mainPage.openView(window);
    window.selectPurchaseOrderLinesTab().waitUntilSelected();
    purchaseOrderLines.assertCount(1);

    logger.info("** End of test case [AUMPROe010] Create Purchase Order. **");
  }
}
