/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.sales.testsuites.SAL_Regressions;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.data.generalsetup.client.client.ClientData;
import com.openbravo.test.integration.erp.data.generalsetup.security.role.OrgAccessData;
import com.openbravo.test.integration.erp.data.generalsetup.security.role.RoleData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.OrganizationSelectorData;
import com.openbravo.test.integration.erp.gui.general.security.role.RoleWindow;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesinvoice.SalesInvoiceHeaderTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.generalsetup.client.Client;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test regression 33040
 *
 * @author nonofce
 *
 */

@RunWith(Parameterized.class)
public class SALRegression33040 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  ClientData clientData;
  RoleData financeRole;
  RoleData financeRoleEdit;
  OrgAccessData orgAccessData;
  SalesInvoiceHeaderData invoiceData;

  public SALRegression33040(ClientData clientData, RoleData financeRole, RoleData financeRoleEdit,
      OrgAccessData orgAccessData, SalesInvoiceHeaderData invoiceData) {
    super();
    this.clientData = clientData;
    this.financeRole = financeRole;
    this.financeRoleEdit = financeRoleEdit;
    this.orgAccessData = orgAccessData;
    this.invoiceData = invoiceData;

    logInData = new LogInData.Builder().userName("Openbravo").password("openbravo").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> testValues() {
    Object[][] data = new Object[][] { {

        new ClientData.Builder().centralMaintenance(false).build(),
        new RoleData.Builder().name("Espa Finance").build(),
        new RoleData.Builder().userLevel("Organization").build(),
        new OrgAccessData.Builder()
            .organization(new OrganizationSelectorData.Builder().name("S.A").build())
            .orgAdmin(false)
            .build(),

        new SalesInvoiceHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Alimentos y Supermercados, S.A")
                    .build())
            .build()

        } };
    return Arrays.asList(data);
  }

  @Test
  public void SalRegression33040() {
    logger
        .info("** Start of test case [SAL33040] Unhomogeneous behavior depending on Org Access **");

    Client client = (Client) new Client(mainPage).open();
    client.edit(clientData);
    client.assertSaved();

    RoleWindow roleWindow = new RoleWindow();
    mainPage.openView(roleWindow);
    roleWindow.selectRoleTab().filter(financeRole);
    roleWindow.selectRoleTab().editRecord(financeRoleEdit);
    roleWindow.selectRoleTab().assertSaved();

    mainPage.closeAllViews();
    Sleep.trySleep();
    mainPage.changeProfile(
        new ProfileData.Builder().role("F&B España, S.A - Finance - F&B International Group")
            .organization("F&B España - Región Norte")
            .build());

    SalesInvoice invoice = new SalesInvoice(mainPage).open();
    SalesInvoiceHeaderTab header = (SalesInvoiceHeaderTab) invoice.getTab();
    header.createOnFormWithoutSaving(invoiceData);

    DataObject visibility = new DataObject();
    visibility.addDataField("organization", true);
    visibility.addDataField("businessPartner", true);
    invoice.assertDisplayLogicVisible(visibility);

    mainPage.closeAllViews();
    Sleep.trySleep();
    mainPage.changeProfile(
        new ProfileData.Builder().role("F&B International Group Admin - F&B International Group")
            .organization("F&B España - Región Norte")
            .build());

    mainPage.openView(roleWindow);
    roleWindow.selectRoleTab().filter(financeRole);
    roleWindow.selectOrgAccessTab().filter(orgAccessData);
    Sleep.trySleep();
    int orgCount = roleWindow.selectOrgAccessTab().getRecordCount();
    if (orgCount > 0) {
      roleWindow.selectOrgAccessTab().deleteMultipleRecordsOneCheckboxOnGrid();
    }
    roleWindow.selectOrgAccessTab().clearFilters();
    Sleep.trySleep();
    roleWindow.selectOrgAccessTab().assertCount(2);

    mainPage.closeAllViews();
    Sleep.trySleep();
    mainPage.changeProfile(
        new ProfileData.Builder().role("F&B España, S.A - Finance - F&B International Group")
            .organization("F&B España - Región Norte")
            .build());

    invoice = new SalesInvoice(mainPage).open();
    header = (SalesInvoiceHeaderTab) invoice.getTab();
    header.createOnFormWithoutSaving(invoiceData);
    invoice.assertDisplayLogicVisible(visibility);

    mainPage.closeAllViews();
    Sleep.trySleep();
    mainPage.changeProfile(
        new ProfileData.Builder().role("F&B International Group Admin - F&B International Group")
            .organization("F&B España - Región Norte")
            .build());

    mainPage.openView(roleWindow);
    roleWindow.selectRoleTab().filter(financeRole);
    roleWindow.selectRoleTab()
        .editRecord(new RoleData.Builder().userLevel("Client+Organization").build());
    roleWindow.selectRoleTab().assertSaved();
    roleWindow.selectOrgAccessTab()
        .createRecord(new OrgAccessData.Builder()
            .organization(new OrganizationSelectorData.Builder().name("S.A").build())
            .build());
    roleWindow.selectOrgAccessTab().assertSaved();

    client = (Client) new Client(mainPage).open();
    client.edit(new ClientData.Builder().centralMaintenance(true).build());
    client.assertSaved();

    logger.info("** End of test case [SAL33040] Unhomogeneous behavior depending on Org Access **");
  }
}
