/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2010-12-28 18:05:34
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice;

import java.util.List;

import com.openbravo.test.integration.erp.data.procurement.transactions.JournalEntryLineData;

/**
 *
 * @author plujan
 *
 */
public class PurchaseInvoiceHeaderPostData {

  /* Data fields */
  /** Journal Entry */
  private List<JournalEntryLineData> journalEntry;

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /* Builder fields */
    /** Journal Entry */
    private List<JournalEntryLineData> journalEntry = null;

    /**
     * Set the Journal Entry value.
     *
     * @param value
     *          The Journal Entry value.
     * @return The builder for this class.
     */
    public Builder journalEntry(List<JournalEntryLineData> value) {
      this.journalEntry = value;
      return this;
    }

    /**
     * Set the fields for this object that are required on the ERP.
     *
     * @param requiredJournalEntry
     *          The Journal Entry value.
     * @return The builder for this class
     */
    public Builder requiredFields(List<JournalEntryLineData> requiredJournalEntry) {
      this.journalEntry = requiredJournalEntry;
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PurchaseInvoiceHeaderPostData build() {
      return new PurchaseInvoiceHeaderPostData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PurchaseInvoiceHeaderPostData(Builder builder) {
    journalEntry = builder.journalEntry;

  }

  public List<JournalEntryLineData> getJournalEntry() {
    return journalEntry;
  }

  @Override
  public String toString() {
    return "PurchaseInvoiceHeaderPostData [journalEntry=" + journalEntry + "]";
  }

}
