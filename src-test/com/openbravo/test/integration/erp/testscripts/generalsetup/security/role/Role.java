/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Iñaki Garcia <inaki.garcia@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.security.role;

import com.openbravo.test.integration.erp.data.generalsetup.security.role.FormAccessData;
import com.openbravo.test.integration.erp.data.generalsetup.security.role.OrgAccessData;
import com.openbravo.test.integration.erp.data.generalsetup.security.role.ProcessAccessData;
import com.openbravo.test.integration.erp.data.generalsetup.security.role.RoleData;
import com.openbravo.test.integration.erp.data.generalsetup.security.role.UserAssignmentData;
import com.openbravo.test.integration.erp.data.generalsetup.security.role.WindowAccessData;
import com.openbravo.test.integration.erp.data.generalsetup.security.user.GrantAccessPopUpData;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.selenium.NotImplementedException;

/**
 * Executes and verifies actions on the User classic window of OpenbravoERP.
 *
 * @author Pablo Lujan
 */
public class Role {

  /**
   * Executes and verifies actions on the Role tab of OpenbravoERP.
   *
   * @author plujan
   *
   */
  public static class RoleTab {

    /**
     * Create a record.
     *
     * @param mainPage
     *          The application main page.
     * @param roleData
     *          Record data.
     */
    public static void create(MainPage mainPage, RoleData roleData) {
      if (mainPage.isOnNewLayout()) {
        RoleScript.create(mainPage, roleData);
      } else {
        RoleClassicScript.create(mainPage, roleData);
      }
    }

    /**
     * Grant access to the role.
     *
     * @param mainPage
     *          The application main page.
     * @param grantAccessData
     *          The grant access data.
     */
    public static void grantAccess(MainPage mainPage, GrantAccessPopUpData grantAccessData) {
      throw new NotImplementedException();
      // RoleScript.grantAccess(mainPage, grantAccessData);
    }

    /**
     * Verify the record was properly created.
     *
     * @param mainPage
     *          The application main page.
     * @param roleData
     *          The user data .
     */
    public static void verify(MainPage mainPage, RoleData roleData) {
      if (mainPage.isOnNewLayout()) {
        RoleScript.verify(mainPage, roleData);
      } else {
        RoleClassicScript.verify(mainPage, roleData);
      }
    }

    /**
     * Executes and verifies actions on the Org Access tab of OpenbravoERP.
     *
     * @author plujan
     *
     */
    public static class OrgAccess {

      /**
       * Creates the organization access.
       *
       * @param mainPage
       *          The application main page.
       * @param orgAccessData
       *          The organization access data.
       */
      public static void create(MainPage mainPage, OrgAccessData orgAccessData) {
        if (mainPage.isOnNewLayout()) {
          OrgAccessScript.create(mainPage, orgAccessData);
        } else {
          OrgAccessClassicScript.create(mainPage, orgAccessData);
        }
      }

    }

    /**
     * Executes and verifies actions on the User Assignment tab of OpenbravoERP.
     *
     * @author plujan
     *
     */
    public static class UserAssignment {

      /**
       * Creates the record.
       *
       * @param mainPage
       *          The application main page.
       * @param userAssignmentData
       *          Data for the record
       */
      public static void create(MainPage mainPage, UserAssignmentData userAssignmentData) {
        if (mainPage.isOnNewLayout()) {
          UserAssignmentScript.create(mainPage, userAssignmentData);
        } else {
          UserAssignmentClassicScript.create(mainPage, userAssignmentData);
        }
      }

    }

    /**
     * Executes and verifies actions on the Window Access tab of OpenbravoERP.
     *
     * @author inaki.garcia
     *
     */
    public static class WindowAccess {

      /**
       * Creates the window access.
       *
       * @param mainPage
       *          The application main page.
       * @param windowAccessData
       *          The window access data.
       */
      public static void create(MainPage mainPage, WindowAccessData windowAccessData) {
        if (mainPage.isOnNewLayout()) {
          WindowAccessScript.create(mainPage, windowAccessData);
        }
      }

    }

    /**
     * Executes and verifies actions on the Form Access tab of OpenbravoERP.
     *
     * @author inaki.garcia
     *
     */
    public static class FormAccess {

      /**
       * Creates the form access.
       *
       * @param mainPage
       *          The application main page.
       * @param formAccessData
       *          The form access data.
       */
      public static void create(MainPage mainPage, FormAccessData formAccessData) {
        if (mainPage.isOnNewLayout()) {
          FormAccessScript.create(mainPage, formAccessData);
        }
      }

    }

    /**
     * Executes and verifies actions on the Window Access tab of OpenbravoERP.
     *
     * @author lorenzo.fidalgo
     *
     */
    public static class ProcessAccess {

      /**
       * Creates the window access.
       *
       * @param mainPage
       *          The application main page.
       * @param processAccessData
       *          The window access data.
       */
      public static void create(MainPage mainPage, ProcessAccessData processAccessData) {
        if (mainPage.isOnNewLayout()) {
          ProcessAccessScript.create(mainPage, processAccessData);
        }
      }

    }

  }

}
