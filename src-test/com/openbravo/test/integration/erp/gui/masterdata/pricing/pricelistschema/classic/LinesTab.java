/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.masterdata.pricing.pricelistschema.classic;

import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelistschema.PriceListSchemaLinesData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Price List Schema Lines tab.
 *
 * @author elopio
 *
 */
public class LinesTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Lines";
  /** The tab level. */
  private static final int LEVEL = 1;

  /** Identifier of the Lines tab */
  public static final String TAB = "tabname405";
  /** Identifier of the product category combo box */
  @SuppressWarnings("unused")
  private static final String COMBO_BOX_PRODUCT_CATEGORY = "reportM_Product_Category_ID_S";
  /** Identifier of the list price discount text field */
  private static final String TEXT_FIELD_LISTPRICEDISCOUNT = "List_Discount";
  /** Identifier of the standard price discount text field */
  private static final String TEXT_FIELD_STANDARDPRICEDISCOUNT = "Std_Discount";

  /**
   * Class constructor
   *
   */
  public LinesTab() {
    super(TITLE, LEVEL, TAB);
  }

  /**
   * Create a new Price Schema Line
   *
   * @param data
   *          The data of the Line to create.
   */
  public void create(PriceListSchemaLinesData data) throws OpenbravoERPTestException {
    clickCreateNewRecord();
    if (data.getDataField("listPriceDiscount") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_LISTPRICEDISCOUNT)
          .sendKeys(data.getDataField("listPriceDiscount").toString());
    }
    if (data.getDataField("standardPriceDiscount") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_STANDARDPRICEDISCOUNT)
          .sendKeys(data.getDataField("standardPriceDiscount").toString());
    }
    clickSave();

  }
}
