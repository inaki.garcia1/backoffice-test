/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.production.setup.machinecategory;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for PeriodicityData
 *
 * @author plujan
 *
 */
public class PeriodicityData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the excludeWeekends value
     *
     * Description: Exclude Weekends
     *
     * @param value
     *          The excludeWeekends value.
     * @return The builder for this class.
     */
    public Builder excludeWeekends(Boolean value) {
      this.dataFields.put("excludeWeekends", value);
      return this;
    }

    /**
     * Set the startingDay value
     *
     * Description: Starting Day
     *
     * @param value
     *          The startingDay value.
     * @return The builder for this class.
     */
    public Builder startingDay(String value) {
      this.dataFields.put("startingDay", value);
      return this;
    }

    /**
     * Set the shift value
     *
     * Description: A partition of the workday into intervals.
     *
     * @param value
     *          The shift value.
     * @return The builder for this class.
     */
    public Builder shift(String value) {
      this.dataFields.put("shift", value);
      return this;
    }

    /**
     * Set the dayOfTheMonth value
     *
     * Description: Day of the Month
     *
     * @param value
     *          The dayOfTheMonth value.
     * @return The builder for this class.
     */
    public Builder dayOfTheMonth(String value) {
      this.dataFields.put("dayOfTheMonth", value);
      return this;
    }

    /**
     * Set the weekday value
     *
     * Description: Any day of the week excluding Saturday and Sunday.
     *
     * @param value
     *          The weekday value.
     * @return The builder for this class.
     */
    public Builder weekday(String value) {
      this.dataFields.put("weekday", value);
      return this;
    }

    /**
     * Set the maintenance value
     *
     * Description: The act of ensuring the proper working order for a specified item.
     *
     * @param value
     *          The maintenance value.
     * @return The builder for this class.
     */
    public Builder maintenance(String value) {
      this.dataFields.put("maintenance", value);
      return this;
    }

    /**
     * Set the periodicityType value
     *
     * Description: Periodicity Type
     *
     * @param value
     *          The periodicityType value.
     * @return The builder for this class.
     */
    public Builder periodicityType(String value) {
      this.dataFields.put("periodicityType", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PeriodicityData build() {
      return new PeriodicityData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PeriodicityData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
