/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentstatus;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for PaymentStatusManagementLinesData
 *
 * @author plujan
 *
 */
public class PaymentStatusManagementLinesData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the lineNo value
     *
     * Description: A line stating the position of this request in the document.
     *
     * @param value
     *          The lineNo value.
     * @return The builder for this class.
     */
    public Builder lineNo(String value) {
      this.dataFields.put("lineNo", value);
      return this;
    }

    /**
     * Set the payment value
     *
     * Description: A obligation to pay or a right to collect for a specified item or service.
     *
     * @param value
     *          The payment value.
     * @return The builder for this class.
     */
    public Builder payment(String value) {
      this.dataFields.put("payment", value);
      return this;
    }

    /**
     * Set the finalStatus value
     *
     * Description: Status to
     *
     * @param value
     *          The finalStatus value.
     * @return The builder for this class.
     */
    public Builder finalStatus(String value) {
      this.dataFields.put("finalStatus", value);
      return this;
    }

    /**
     * Set the currentStatus value
     *
     * Description: Status From
     *
     * @param value
     *          The currentStatus value.
     * @return The builder for this class.
     */
    public Builder currentStatus(String value) {
      this.dataFields.put("currentStatus", value);
      return this;
    }

    /**
     * Set the paymentManagement value
     *
     * Description: Debt Payment Management
     *
     * @param value
     *          The paymentManagement value.
     * @return The builder for this class.
     */
    public Builder paymentManagement(String value) {
      this.dataFields.put("paymentManagement", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PaymentStatusManagementLinesData build() {
      return new PaymentStatusManagementLinesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PaymentStatusManagementLinesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
