/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testsuites.standardview.grid;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.assets.assets.AssetsData;
import com.openbravo.test.integration.erp.gui.financial.assets.AssetsHeaderTab;
import com.openbravo.test.integration.erp.gui.financial.assets.AssetsWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;

/**
 * This tests ensures that display logic works properly in grid view.
 *
 * @author inigo.sanchez
 */
@RunWith(Parameterized.class)
public class GDLa010_DisplayLogicInGrid extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  // Data for [GDLa010]
  AssetsData assetsData;
  AssetsData assetsDataDepreciated;
  AssetsData assetsDataCalculateType;
  List<String> nameFieldsToCheck = Arrays.asList("depreciate", "calculateType", "depreciationType");

  /**
   * Class constructor.
   *
   * @param assetsData
   * @param assetsDataDepreciated
   * @param assetsDataCalculateType
   */
  public GDLa010_DisplayLogicInGrid(AssetsData assetsData, AssetsData assetsDataDepreciated,
      AssetsData assetsDataCalculateType) {
    this.assetsData = assetsData;
    this.assetsDataDepreciated = assetsDataDepreciated;
    this.assetsDataCalculateType = assetsDataCalculateType;
    logInData = new LogInData.Builder().userName("Openbravo").password("openbravo").build();
  }

  /**
   * Test parameters.
   *
   * @return Collection of AssetsData arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> assetsValues() {
    Object[][] data = new Object[][] {
        { new AssetsData.Builder().searchKey("TestAssetGrid").name("Test Asset Grid").build(),
            new AssetsData.Builder().depreciate(true).build(),
            new AssetsData.Builder().calculateType("Time").build() } };
    return Arrays.asList(data);
  }

  /**
   * This test case checks that display logic works properly in grid view. This test create a record
   * in Assets window and verify if some cells have their correct visibility. Note that display
   * logic in grid view is showing as a read only.
   *
   * @throws IOException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void testDisplayLogicGrid() throws IOException, OpenbravoERPTestException {
    logger.info("** Start of test case [GDLa030]**");

    // Open Assets window and select header tab.
    AssetsWindow assetsWindow = new AssetsWindow();
    mainPage.openView(assetsWindow);
    AssetsHeaderTab assetsHeaderTab = (AssetsHeaderTab) assetsWindow
        .selectTab(AssetsHeaderTab.IDENTIFIER);

    // Create a new asset in grid view for testing.
    assetsHeaderTab.createOnGridAndSave(assetsData);
    assetsHeaderTab.assertSaved();

    // Select and start editing record and check if isDepreciate is displayed.
    assetsHeaderTab.select(assetsData);
    assetsHeaderTab.startEditionOnGrid(0);
    assertVisibilityInGridView(assetsHeaderTab, Arrays.asList(true, false, false));
    assetsHeaderTab.saveEdition();
    assetsHeaderTab.assertSaved();

    // Select Depreciate checkbox.
    assetsHeaderTab.editOnGrid(assetsDataDepreciated);
    assetsHeaderTab.assertSaved();

    // Start editing record and check that visibility is correct.
    assetsHeaderTab.startEditionOnGrid(0);
    assertVisibilityInGridView(assetsHeaderTab, Arrays.asList(true, true, true));
    assetsHeaderTab.saveEdition();
    assetsHeaderTab.assertSaved();

    // Select Time value in Calculate type selector.
    assetsHeaderTab.editOnGrid(assetsDataCalculateType);
    assetsHeaderTab.assertSaved();

    // Start editing record and check that visibility is correct.
    assetsHeaderTab.startEditionOnGrid(0);
    assertVisibilityInGridView(assetsHeaderTab, Arrays.asList(true, true, true));
    assetsHeaderTab.saveEdition();
    assetsHeaderTab.assertSaved();

    assetsHeaderTab.deleteOnGrid();
    logger.info("** End of test case [GDLa010]**");
  }

  /**
   * Checks if nameFieldsToCheck cells are enabled.
   *
   * @param headerTab
   *          Where the fields are checked.
   * @param visibility
   *          Visibilities of the cells.
   */
  private void assertVisibilityInGridView(AssetsHeaderTab headerTab, List<Boolean> visibility) {
    logger.info(
        "Checking if cells are enabled or disbaled. Display logic in grid is showing as read only cells.");
    // Make sure the "nameFieldsToCheck" list fields take into account the order of "visibility"
    // list that defines the visibility of fields.
    int numField = 0;
    for (String nameField : nameFieldsToCheck) {
      logger.info("Checking {} field visibility", nameField);
      assertThat(nameField + " visibility is not correct.",
          headerTab.getField(nameField, false).isEnabled(), is(visibility.get(numField)));
      numField++;
    }
  }
}
