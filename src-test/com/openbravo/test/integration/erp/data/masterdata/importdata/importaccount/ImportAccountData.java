/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.importdata.importaccount;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ImportAccountData
 *
 * @author plujan
 *
 */
public class ImportAccountData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the elementLevel value
     *
     * Description: Element Level
     *
     * @param value
     *          The elementLevel value.
     * @return The builder for this class.
     */
    public Builder elementLevel(String value) {
      this.dataFields.put("elementLevel", value);
      return this;
    }

    /**
     * Set the parentKey value
     *
     * Description: Key if the Parent
     *
     * @param value
     *          The parentKey value.
     * @return The builder for this class.
     */
    public Builder parentKey(String value) {
      this.dataFields.put("parentKey", value);
      return this;
    }

    /**
     * Set the summaryLevel value
     *
     * Description: A means of grouping fields in order to view or hide additional information.
     *
     * @param value
     *          The summaryLevel value.
     * @return The builder for this class.
     */
    public Builder summaryLevel(Boolean value) {
      this.dataFields.put("summaryLevel", value);
      return this;
    }

    /**
     * Set the parentAccount value
     *
     * Description: The parent (summary) account
     *
     * @param value
     *          The parentAccount value.
     * @return The builder for this class.
     */
    public Builder parentAccount(String value) {
      this.dataFields.put("parentAccount", value);
      return this;
    }

    /**
     * Set the importErrorMessage value
     *
     * Description: A message that appears when errors occur during the importing process.
     *
     * @param value
     *          The importErrorMessage value.
     * @return The builder for this class.
     */
    public Builder importErrorMessage(String value) {
      this.dataFields.put("importErrorMessage", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the importProcessComplete value
     *
     * Description: A indication that the desired process has been completed successfully.
     *
     * @param value
     *          The importProcessComplete value.
     * @return The builder for this class.
     */
    public Builder importProcessComplete(Boolean value) {
      this.dataFields.put("importProcessComplete", value);
      return this;
    }

    /**
     * Set the elementName value
     *
     * Description: Name of the Element
     *
     * @param value
     *          The elementName value.
     * @return The builder for this class.
     */
    public Builder elementName(String value) {
      this.dataFields.put("elementName", value);
      return this;
    }

    /**
     * Set the column value
     *
     * Description: A link to the database column of the table.
     *
     * @param value
     *          The column value.
     * @return The builder for this class.
     */
    public Builder column(String value) {
      this.dataFields.put("column", value);
      return this;
    }

    /**
     * Set the searchKey value
     *
     * Description: A fast method for finding a particular record.
     *
     * @param value
     *          The searchKey value.
     * @return The builder for this class.
     */
    public Builder searchKey(String value) {
      this.dataFields.put("searchKey", value);
      return this;
    }

    /**
     * Set the documentControlled value
     *
     * Description: Control account - If an account is controlled by a document you cannot post
     * manually to it
     *
     * @param value
     *          The documentControlled value.
     * @return The builder for this class.
     */
    public Builder documentControlled(Boolean value) {
      this.dataFields.put("documentControlled", value);
      return this;
    }

    /**
     * Set the accountSign value
     *
     * Description: Indicates the Natural Sign of the Account as a Debit or Credit
     *
     * @param value
     *          The accountSign value.
     * @return The builder for this class.
     */
    public Builder accountSign(String value) {
      this.dataFields.put("accountSign", value);
      return this;
    }

    /**
     * Set the accountingElement value
     *
     * Description: A unique identifier for an account type.
     *
     * @param value
     *          The accountingElement value.
     * @return The builder for this class.
     */
    public Builder accountingElement(String value) {
      this.dataFields.put("accountingElement", value);
      return this;
    }

    /**
     * Set the postActual value
     *
     * Description: Actual Values can be posted
     *
     * @param value
     *          The postActual value.
     * @return The builder for this class.
     */
    public Builder postActual(Boolean value) {
      this.dataFields.put("postActual", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the accountType value
     *
     * Description: Indicates the type of account
     *
     * @param value
     *          The accountType value.
     * @return The builder for this class.
     */
    public Builder accountType(String value) {
      this.dataFields.put("accountType", value);
      return this;
    }

    /**
     * Set the defaultAccount value
     *
     * Description: Name of the Default Account Column
     *
     * @param value
     *          The defaultAccount value.
     * @return The builder for this class.
     */
    public Builder defaultAccount(String value) {
      this.dataFields.put("defaultAccount", value);
      return this;
    }

    /**
     * Set the processed value
     *
     * Description: A confirmation that the associated documents or requests are processed.
     *
     * @param value
     *          The processed value.
     * @return The builder for this class.
     */
    public Builder processed(Boolean value) {
      this.dataFields.put("processed", value);
      return this;
    }

    /**
     * Set the postBudget value
     *
     * Description: Budget values can be posted
     *
     * @param value
     *          The postBudget value.
     * @return The builder for this class.
     */
    public Builder postBudget(Boolean value) {
      this.dataFields.put("postBudget", value);
      return this;
    }

    /**
     * Set the postStatistical value
     *
     * Description: Post statistical quantities to this account?
     *
     * @param value
     *          The postStatistical value.
     * @return The builder for this class.
     */
    public Builder postStatistical(Boolean value) {
      this.dataFields.put("postStatistical", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the accountElement value
     *
     * Description: A identification code for an account type.
     *
     * @param value
     *          The accountElement value.
     * @return The builder for this class.
     */
    public Builder accountElement(String value) {
      this.dataFields.put("accountElement", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the postEncumbrance value
     *
     * Description: Post commitments to this account
     *
     * @param value
     *          The postEncumbrance value.
     * @return The builder for this class.
     */
    public Builder postEncumbrance(Boolean value) {
      this.dataFields.put("postEncumbrance", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ImportAccountData build() {
      return new ImportAccountData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ImportAccountData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
