/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.product;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ProductData
 *
 * @author jbueno
 *
 */
public class CharacteristicsData extends DataObject {

  /**
   * Class builder
   *
   * @author jbueno
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the characteristic value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The chracteristic value.
     * @return The builder for this class.
     */
    public Builder characteristic(String value) {
      this.dataFields.put("characteristic", value);
      return this;
    }

    /**
     * Set the variant value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The variant value.
     * @return The builder for this class.
     */
    public Builder variant(Boolean value) {
      this.dataFields.put("variant", value);
      return this;
    }

    /**
     * Set the explode configuration tab value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The variant value.
     * @return The builder for this class.
     */
    public Builder explode(Boolean value) {
      this.dataFields.put("explodeConfigurationTab", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public CharacteristicsData build() {
      return new CharacteristicsData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private CharacteristicsData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
