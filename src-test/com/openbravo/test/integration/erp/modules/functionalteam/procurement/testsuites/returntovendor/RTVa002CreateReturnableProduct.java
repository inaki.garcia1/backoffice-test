/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2017 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2017-05-29 09:37:14
 * Contributor(s):
 *   Alejandro Matos <alekosmp86@gmail.com>
 *************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.procurement.testsuites.returntovendor;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.masterdata.product.PriceData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductData;
import com.openbravo.test.integration.erp.gui.masterdata.product.PriceTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.masterdata.product.Product;
import com.openbravo.test.integration.selenium.Sleep;

/**
 *
 * Class for Test AUMRTVa000
 *
 * @author Alejandro Matos
 *
 */
@RunWith(Parameterized.class)
public class RTVa002CreateReturnableProduct extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Data for Test */
  ProductData filterProductData;
  ProductData productData;
  PriceData productPrice;
  PriceData salesProductPrice;

  public RTVa002CreateReturnableProduct(ProductData filterProductData, ProductData productData,
      PriceData productPrice, PriceData salesProductPrice) {
    super();
    this.filterProductData = filterProductData;
    this.productData = productData;
    this.productPrice = productPrice;
    this.salesProductPrice = salesProductPrice;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  @Parameters
  public static Collection<Object[]> returnToVendorValues() {
    Object[][] data = new Object[][] {
        { new ProductData.Builder().name("Volley Ball").searchKey("VB").build(),
            new ProductData.Builder().organization("*")
                .name("Volley Ball")
                .searchKey("VB")
                .uOM("Unit")
                .productCategory("Distribution Goods")
                .taxCategory("VAT 10%")
                .build(),
            new PriceData.Builder().priceListVersion("Sport items")
                .listPrice("28.50")
                .standardPrice("28.50")
                .build(),

            new PriceData.Builder().priceListVersion("Sales")
                .listPrice("28.50")
                .standardPrice("28.50")
                .build() } };
    return Arrays.asList(data);
  }

  @Test
  public void RTVa002test() throws ParseException {
    logger.debug("** Start test case [RTVa002] Create returnable product **");

    int recordCount = Product.ProductTab.getRecordCount(mainPage, this.filterProductData);

    if (recordCount == 0) {
      ProductWindow productWindow = Product.ProductTab.open(mainPage);
      ProductTab productTab = productWindow.selectProductTab();
      mainPage.waitForDataToLoad();
      Sleep.trySleep();
      productTab.createRecord(productData);
      productTab.assertSaved();

      PriceTab priceTab = productWindow.selectPriceTab();
      priceTab.createRecord(productPrice);
      priceTab.assertSaved();

      priceTab.createRecord(salesProductPrice);
      priceTab.assertSaved();
    }

    logger.debug("** End test case [RTVa002] Create and configure a returnable product **");
  }
}
