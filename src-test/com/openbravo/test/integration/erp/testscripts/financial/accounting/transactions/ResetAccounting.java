/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   David Miguélez <david.miguelez@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.financial.accounting.transactions;

import com.openbravo.test.integration.erp.gui.financial.accounting.transactions.ResetAccountingProcess;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Resets the accounting data.
 *
 * @author David Miguélez
 */

public class ResetAccounting {

  /** The application main page. */
  protected final MainPage mainPage;

  ResetAccountingProcess resetAccountingProcess;

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public ResetAccounting(MainPage mainPage) {
    this.mainPage = mainPage;
  }

  /**
   * Open the Reset Accounting pop up..
   *
   * @return this script.
   */
  public ResetAccounting open() {
    mainPage.selectWindow();
    mainPage.getNavigationBar().getComponentMenu().select(ResetAccountingProcess.MENU_PATH);
    return this;
  }

  /**
   * Resets the accounting information.
   *
   * @param client
   *          The name of the client.
   * @param organization
   *          The name of the organization.
   */
  public void resetAccounting(String client, String organization) {
    resetAccountingProcess = new ResetAccountingProcess();
    resetAccountingProcess.resetAccounting(client, organization);
  }

}
