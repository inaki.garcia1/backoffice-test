/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testsuites.smoke.financial;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.PaymentSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Execute the Setup Client and Organization flow of the smoke test suite.
 *
 * @author Pablo Lujan
 */
@RunWith(Parameterized.class)
public class FINe_FinancialAccountAddPayment extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  /* Data for [FINd010] Create Purchase Invoice */
  /** The purchase invoice header data. */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  /** The data to verify the purchase invoice header. */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  /** The purchase invoice lines data. */
  PurchaseInvoiceLinesData purchaseInvoiceLinesData;
  /** The data to verify the purchase invoice lines creation. */
  PurchaseInvoiceLinesData purchaseInvoiceLinesVerificationData;
  /** The data to verify the completed purchase invoice header. */
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderData;

  /* Data for [FINe020] Add Payment. */
  /** The account data. */
  AccountData accountData;
  /** The transaction verification data. */
  TransactionsData transactionLinesData;
  /** The add payment totals verification data. */
  AddPaymentPopUpData addPaymentTotalsVerificationData;
  /** The purchase invoice header verification data. */
  PurchaseInvoiceHeaderData payedPurchaseInvoiceHeaderVerificationData;
  /** The data to edit in AddPayment popup. */
  AddPaymentPopUpData editPayment;

  /**
   * Class constructor.
   *
   * @param purchaseInvoiceHeaderData
   *          The purchase invoice header data.
   * @param purchaseInvoiceHeaderVerificationData
   *          The data to verify the purchase invoice header.
   * @param purchaseInvoiceLinesData
   *          The purchase invoice lines data.
   * @param purchaseInvoiceLinesVerificationData
   *          The data to verify the purchase invoice lines creation.
   * @param completedPurchaseInvoiceHeaderData
   *          The data to verify the completed purchase invoice header.
   * @param accountData
   *          The account data.
   * @param transactionLinesData
   *          The transaction lines data.
   */
  public FINe_FinancialAccountAddPayment(PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLinesData,
      PurchaseInvoiceLinesData purchaseInvoiceLinesVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderData, AccountData accountData,
      TransactionsData transactionLinesData, AddPaymentPopUpData addPaymentTotalsVerificationData,
      PurchaseInvoiceHeaderData payedPurchaseInvoiceHeaderVerificationData,
      AddPaymentPopUpData editPayment) {
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLinesData = purchaseInvoiceLinesData;
    this.purchaseInvoiceLinesVerificationData = purchaseInvoiceLinesVerificationData;
    this.completedPurchaseInvoiceHeaderData = completedPurchaseInvoiceHeaderData;
    this.accountData = accountData;
    this.transactionLinesData = transactionLinesData;
    this.addPaymentTotalsVerificationData = addPaymentTotalsVerificationData;
    this.payedPurchaseInvoiceHeaderVerificationData = payedPurchaseInvoiceHeaderVerificationData;
    this.editPayment = editPayment;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   * @throws IOException
   *
   */
  @Parameters
  public static Collection<DataObject[]> paymentProposalValues() throws IOException {
    DataObject[][] data = new DataObject[][] { {
        // Parameters for [FINe010] Create Purchase Invoice
        new PurchaseInvoiceHeaderData.Builder().transactionDocument("AP Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .build(),
        new PurchaseInvoiceHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .invoiceDate(OBDate.CURRENT_DATE)
            .priceList("Purchase")
            .paymentMethod("1 (Spain)")
            .paymentTerms("90 days")
            .build(),
        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("RMA")
                .priceListVersionName("Purchase")
                .build())
            .invoicedQuantity("11.2")
            .build(),
        new PurchaseInvoiceLinesData.Builder().uOM("Bag")
            .listPrice("2.00")
            .unitPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("22.40")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().totalPaid("0.00")
            .outstandingAmount("24.64")
            .daysTillDue("90")
            .dueAmount("0.00")
            .paymentComplete(false)
            .summedLineAmount("22.40")
            .grandTotalAmount("24.64")
            .documentStatus("Completed")
            .build(),
        // Parameters for [FINe020] Add Payment.
        new AccountData.Builder().name("Spain Cashbook").build(),
        new TransactionsData.Builder().transactionType("BP Withdrawal").build(),
        new AddPaymentPopUpData.Builder().amount_gl_items("0.00")
            .amount_inv_ords("24.64")
            .total("24.64")
            .difference("0.00")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().paymentComplete(true)
            .documentStatus("Completed")
            .summedLineAmount("22.40")
            .grandTotalAmount("24.64")
            .currency("EUR")
            .build(),
        new AddPaymentPopUpData.Builder().fin_paymentmethod_id("1 (Spain)").build(), } };
    return Arrays.asList(data);
  }

  /**
   * This test case proves that you can create the payment proposal.
   *
   * @throws IOException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void paymentShouldBeAddedInFinancialAccount()
      throws IOException, OpenbravoERPTestException {
    logger.info("** Start of test case [FINe010] Create Purchase Invoice. **");
    final PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    PurchaseInvoice.Lines purchaseInvoiceLines = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLines.create(purchaseInvoiceLinesData);
    purchaseInvoiceLines.assertSaved();
    purchaseInvoiceLines.assertData(purchaseInvoiceLinesVerificationData);
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderData);
    String purchaseInvoiceNumber = (String) purchaseInvoice.getData("documentNo");
    logger.info("** End of test case [FINe010] Create Purchase Invoice. **");

    logger.info("** Start of test case [FINe020] Add Payment. **");
    PaymentSelectorData addPaymentData = new PaymentSelectorData.Builder().amount("24.64")
        .addPayment(addPaymentTotalsVerificationData)
        .addEditPayment(editPayment)
        .build();
    addPaymentData.addDataField("invoiceNo", purchaseInvoiceNumber);
    transactionLinesData.addDataField("finPayment", addPaymentData);

    final FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountData);
    FinancialAccount.Transaction transactions = financialAccount.new Transaction(mainPage);

    addPaymentData.addDataField("currentTab", financialAccount.getTab());
    addPaymentData.addDataField("transactions", transactions);

    Sleep.trySleep(1000);
    transactions.create(transactionLinesData);
    transactions.process();

    purchaseInvoice.open();
    purchaseInvoice
        .select(new PurchaseInvoiceHeaderData.Builder().documentNo(purchaseInvoiceNumber).build());
    purchaseInvoice.assertData(payedPurchaseInvoiceHeaderVerificationData);
    logger.info("** End of test case [FINe020] Add Payment. **");
  }
}
