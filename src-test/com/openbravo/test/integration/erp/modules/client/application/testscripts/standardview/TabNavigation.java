/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.standardview;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.gui.applicationdictionary.windowstabsandfields.WindowsTabsAndFieldsWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Test the navigation between tabs in the standard view.
 *
 * @author elopio
 *
 */
public class TabNavigation extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Test menu item selection.
   */
  @Test
  public void tabsShouldBeOpened() {
    mainPage.changeProfile(new ProfileData.Builder().role("System Administrator - System")
        .client("System")
        .organization("*")
        .build());

    WindowsTabsAndFieldsWindow windowsTabsAndFieldsWindow = new WindowsTabsAndFieldsWindow();
    mainPage.openView(windowsTabsAndFieldsWindow);

    logger.info("Select first level child.");
    windowsTabsAndFieldsWindow.selectTabTab();
    logger.info("Select second level child.");
    windowsTabsAndFieldsWindow.selectFieldTab();
    logger.info("Select third level child.");
    windowsTabsAndFieldsWindow.selectFieldTranslationTab();
    logger.info("Select a parent tab.");
    windowsTabsAndFieldsWindow.selectFieldTab();
    logger.info("Select sibling tab.");
    windowsTabsAndFieldsWindow.selectTabTranslationTab();
    logger.info("Select a sibling of the parent tab.");
    windowsTabsAndFieldsWindow.selectWindowTranslationTab();
    // TODO Select top tab.
  }

}
