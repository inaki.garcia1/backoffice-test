/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.financial.accounting.setup.fiscalcalendar;

import com.openbravo.test.integration.erp.data.financial.accounting.setup.fiscalcalendar.CalendarData;
import com.openbravo.test.integration.erp.data.financial.accounting.setup.fiscalcalendar.PeriodData;
import com.openbravo.test.integration.erp.data.financial.accounting.setup.fiscalcalendar.YearData;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;

/**
 * Executes and verifies actions on the Fiscal Calendar classic window of OpenbravoERP.
 *
 * @author elopio
 */
public class FiscalCalendar {

  /**
   * Executes and verifies actions on the Calendar tab of OpenbravoERP.
   *
   * @author elopio
   *
   */
  public static class CalendarTab {

    /**
     * Create a calendar record.
     *
     * @param mainPage
     *          The application main page.
     * @param calendarData
     *          The data of the calendar that will be created.
     */
    public static void create(MainPage mainPage, CalendarData calendarData) {
      if (mainPage.isOnNewLayout()) {
        CalendarTabScript.create(mainPage, calendarData);
      } else {
        CalendarTabClassicScript.create(mainPage, calendarData);
      }
    }

    /**
     * Executes and verifies actions on the Year tab of OpenbravoERP.
     *
     * @author elopio
     *
     */
    public static class YearTab {

      /**
       * Create a year record.
       *
       * @param mainPage
       *          The application main page.
       * @param yearData
       *          The data of the year that will be created.
       */
      public static void create(MainPage mainPage, YearData yearData) {
        if (mainPage.isOnNewLayout()) {
          YearTabScript.create(mainPage, yearData);
        } else {
          YearTabClassicScript.create(mainPage, yearData);
        }
      }

      /**
       * Create the periods of a year.
       *
       * @param mainPage
       *          The application main page.
       * @throws OpenbravoERPTestException
       */
      public static void createPeriods(MainPage mainPage) throws OpenbravoERPTestException {
        if (mainPage.isOnNewLayout()) {
          YearTabScript.createPeriods(mainPage);
        } else {
          YearTabClassicScript.createPeriods(mainPage);
        }
      }

      /**
       * Executes and verifies actions on the Year tab of OpenbravoERP.
       *
       * @author elopio
       *
       */
      public static class PeriodTab {

        /**
         * Select a record.
         *
         * @param mainPage
         *          The application main page.
         * @param periodData
         *          The record to select.
         * @throws OpenbravoERPTestException
         */
        public static void select(MainPage mainPage, PeriodData periodData)
            throws OpenbravoERPTestException {
          if (mainPage.isOnNewLayout()) {
            PeriodTabScript.select(mainPage, periodData);
          } else {
            PeriodTabClassicScript.select(mainPage, periodData);
          }
        }

        /**
         * Verify the number of records.
         *
         * @param mainPage
         *          The application main page.
         * @param count
         *          The expected number of records.
         * @throws OpenbravoERPTestException
         */
        public static void verifyCount(MainPage mainPage, int count)
            throws OpenbravoERPTestException {
          if (mainPage.isOnNewLayout()) {
            PeriodTabScript.verifyCount(mainPage, count);
          } else {
            PeriodTabClassicScript.verifyCount(mainPage, count);
          }
        }
      }
    }

  }
}
