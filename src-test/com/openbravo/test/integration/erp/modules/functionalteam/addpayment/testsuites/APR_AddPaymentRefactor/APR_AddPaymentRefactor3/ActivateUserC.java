/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_AddPaymentRefactor.APR_AddPaymentRefactor3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.data.generalsetup.security.user.UserData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.generalsetup.security.user.User;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Activate the user C
 */
public class ActivateUserC extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Class constructor.
   */
  public ActivateUserC() {
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  /**
   * Test the log in and log out.
   */
  @Test
  public void userShouldBeActivated() {
    logger.info("** Start of test case [ActivateUserC] **");
    mainPage.changeProfile(new ProfileData.Builder().role("QA Testing Admin - QA Testing")
        .client("QA Testing")
        .organization("*")
        .warehouse("Spain East warehouse")
        .build());
    User user = new User(mainPage).open();
    user.select(new UserData.Builder().username("userC").build());

    if (!(Boolean) user.getData().getDataField("active")) {
      user.edit(new UserData.Builder().active(true).build());
      user.assertSaved();
    }
    logger.info("** End of test case [ActivateUserC] **");
  }
}
