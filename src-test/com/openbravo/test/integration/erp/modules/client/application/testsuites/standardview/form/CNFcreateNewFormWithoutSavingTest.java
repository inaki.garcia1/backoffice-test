/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Naroa Iriarte <naroa.iriarte@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testsuites.standardview.form;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.data.selectors.SelectorDataObject;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.HeaderTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.LinesTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.SalesOrderWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBForm;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.SelectorField;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * This tests ensures that in the lines tab of sales order window, when clicking the "Create new
 * record in form view" having the record fully filled and without having it previously saved, does
 * not fill the "Product" field in the new record.
 *
 * @author Naroa Iriarte
 */
@RunWith(Parameterized.class)
public class CNFcreateNewFormWithoutSavingTest extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  /* Data for [CNF010] */

  /** The data of the product selector. */
  ProductSelectorData productSelectorData;

  /**
   * Class constructor.
   *
   * @param productSelectorData
   *          The data of the product selector.
   */
  public CNFcreateNewFormWithoutSavingTest(ProductSelectorData productSelectorData) {
    this.productSelectorData = productSelectorData;
    logInData = new LogInData.Builder().userName("Openbravo").password("openbravo").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of ProductSelectorData arrays with data for the test
   *
   */
  @Parameters
  public static Collection<ProductSelectorData[]> fieldValues() throws IOException {
    return Arrays.asList(new ProductSelectorData[][] {
        { new ProductSelectorData.Builder().name("Agua sin Gas 1L").build() } });
  }

  /**
   * This test case checks that the Product field is empty after filling all the fields of the tab
   * lines and, without saving, clicking the create a new record in form view button.
   */
  @SuppressWarnings("unchecked")
  @Test
  public void productSelectorShouldBeEmpty() throws IOException, OpenbravoERPTestException {
    logger.info("** Start of test case [CNF010]**");
    mainPage.getNavigationBar()
        .changeProfile(new ProfileData.Builder()
            .role("F&B International Group Admin - F&B International Group")
            .client("F&B International Group")
            .organization("*")
            .warehouse("España Región Norte")
            .build());
    SalesOrderWindow salesOrderWindow = new SalesOrderWindow();
    mainPage.openView(salesOrderWindow);
    HeaderTab headerTab = (HeaderTab) salesOrderWindow.selectTab(HeaderTab.IDENTIFIER);
    headerTab.createRecord(new SalesOrderHeaderData.Builder().businessPartner(
        new BusinessPartnerSelectorData.Builder().name("Alimentos y Supermercados, S.A").build())
        .build());

    LinesTab linesTab = (LinesTab) salesOrderWindow.selectTab(LinesTab.IDENTIFIER);
    linesTab.clickCreateNewInForm();
    OBForm form = linesTab.getForm();
    form.getField("product");
    SelectorField<SelectorDataObject> mySelector = (SelectorField<SelectorDataObject>) form
        .getField("product");
    mySelector.selectFromComboBox(productSelectorData);
    linesTab.waitForDataToLoad();
    Sleep.trySleep();
    linesTab.clickCreateNewInForm();
    Sleep.trySleep();
    linesTab.waitForDataToLoad();
    assertThat("Product field should be empty and it is not.", mySelector.getValue().toString(),
        is(equalTo("")));
    logger.info("** End of test case [CNF010]**");
  }
}
