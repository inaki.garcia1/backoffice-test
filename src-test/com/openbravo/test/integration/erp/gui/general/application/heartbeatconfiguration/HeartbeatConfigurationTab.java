/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.application.heartbeatconfiguration;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.openbravo.test.integration.erp.data.generalsetup.application.heartbeatconfiguration.HeartbeatConfigurationData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;

/**
 * Executes and verifies actions on OpenbravoERP Heartbeat Configuration tab.
 *
 * @author elopio
 *
 */
public class HeartbeatConfigurationTab extends GeneratedTab<HeartbeatConfigurationData> {

  /**
   * Enumerator with the values of heartbeat status.
   *
   * @author elopio
   *
   */
  private enum Status {
    DISABLED("Enable Heartbeat"), ENABLED("Disable Heartbeat");

    /** The text of the button displayed while on the status. */
    private final String buttonText;

    /**
     * Enumerator constructor.
     *
     * @param buttonText
     *          The text of the button displayed while on the status.
     */
    private Status(String buttonText) {
      this.buttonText = buttonText;
    }

    /**
     * Get the text of the button displayed while on the status.
     *
     * @return the text of the button displayed while on the status.
     */
    public String buttonText() {
      return buttonText;
    }
  }

  /** The tab title. */
  static final String TITLE = "Heartbeat Configuration";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  static final String IDENTIFIER = "1005400005";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /**
   * Formatting string for the registry key of the Enable Heartbeat button. The parameter is the tab
   * identifier.
   */
  @SuppressWarnings("unused")
  private static final String FORMAT_REGISTRY_KEY_ENABLE_HEARTBEAT_BUTTON = "org.openbravo.client.application.toolbar.button.enableHeartbeat.%s";
  /**
   * Formatting string for the registry key of the Disable Heartbeat button. The parameter is the
   * tab identifier.
   */
  @SuppressWarnings("unused")
  private static final String FORMAT_REGISTRY_KEY_DISABLE_HEARTBEAT_BUTTON = "";

  private static final String FORMAT_REGISTRY_KEY_BUTTON_TEST_HEARTBEAT = "org.openbravo.client.application.toolbar.button.testHeartbeat.%s";

  /** The Enable Heartbeat button. */
  // private Button buttonEnableHeartbeat;
  /** The Disable Heartbeat button. */
  // private Button buttonDisableHeartbeat;

  private Button buttonTestHeartbeat;

  /**
   * Class constructor.
   *
   */
  public HeartbeatConfigurationTab() {
    super(TITLE, IDENTIFIER);
    // buttonEnableHeartbeat = new Button(String.format(
    // FORMAT_REGISTRY_KEY_ENABLE_HEARTBEAT_BUTTON, identifier));
    // buttonDisableHeartbeat = new Button(String.format(
    // FORMAT_REGISTRY_KEY_DISABLE_HEARTBEAT_BUTTON, identifier));
    buttonTestHeartbeat = new Button(TestRegistry
        .getObjectString(String.format(FORMAT_REGISTRY_KEY_BUTTON_TEST_HEARTBEAT, identifier)));
  }

  /**
   * Indicates whether the heartbeat is enabled or not.
   *
   * @return true if heartbeat is enabled. Otherwise, false.
   */
  public boolean isHeartbeatEnabled() {
    return buttonTestHeartbeat.getRealTitle().equals(Status.ENABLED.buttonText());
  }

  /**
   * Enable Heartbeat.
   */
  public void enableHeartbeat() {
    buttonTestHeartbeat.smartClientClick();
  }

  /**
   * Disable Heartbeat
   */
  public void disableHeartbeat() {
    buttonTestHeartbeat.smartClientClick();
  }

  /**
   * Assert the status of the heartbeat application.
   *
   * @param status
   *          The expected status.
   */
  private void assertHeartbeatStatus(Status status) {
    assertTrue("Heartbeat button is not visible.", buttonTestHeartbeat.isVisible());
    switch (status) {
      case DISABLED:
        assertFalse("Heartbeat is enabled.", isHeartbeatEnabled());
        break;
      case ENABLED:
        assertTrue("Heartbeat is disabled.", isHeartbeatEnabled());
        break;
    }
  }

  /**
   * Assert that the heartbeat status is enabled.
   */
  public void assertEnabledStatus() {
    assertHeartbeatStatus(Status.ENABLED);
  }

  /**
   * Assert that the heartbeat status is active.
   */
  public void assertDisabledStatus() {
    assertHeartbeatStatus(Status.DISABLED);
  }
}
