/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui;

/**
 * Executes and verify actions on an OpenbravoERP view, part of the client application module.
 *
 * @author elopio
 *
 */
public abstract class View {

  /** The title of the view. */
  private final String title;
  /** The path to the view on the menu. */
  private final String[] menuPath;

  /**
   * Class constructor
   *
   * @param title
   *          The title of the view.
   * @param menuPath
   *          The path to open the view from the menu.
   */
  public View(String title, String... menuPath) {
    this.title = title;
    this.menuPath = menuPath;
  }

  /**
   * Class constructor
   *
   * @param title
   *          The title of the view.
   */
  public View(String title) {
    this.title = title;
    this.menuPath = null;
  }

  /**
   * Load the GUI components of the view.
   */
  public abstract void load();

  /**
   * Get the view title.
   *
   * @return the title of the view.
   */
  public String getTitle() {
    return title;
  }

  /**
   * Get the menu path.
   *
   * @return the path to open the view from the menu.
   */
  public String[] getMenuPath() {
    return menuPath;
  }

}
