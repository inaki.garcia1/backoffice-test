/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.application.instanceactivation;

/**
 * Class for Activate Online data.
 *
 * @author elopio
 *
 */
public class ActivateOnlineData {

  /** Formatting string used to get a string representation of this object. */
  private static final String FORMAT_STRING_REPRESENTATION = "[Activate Online data: instance purpose=%s, system key=%s]";

  /* Data fields. */
  /** The instance purpose. */
  private String purpose;
  /** The system key. */
  private String systemKey;

  /**
   * Class builder.
   *
   * @author elopio
   *
   */
  public static class Builder {
    /** The instance purpose. */
    private String purpose = null;
    /** The system key. */
    private String systemKey = null;

    /**
     * Set the instance purpose value.
     *
     * @param value
     *          The instance purpose.
     * @return the builder for this class.
     */
    public Builder purpose(String value) {
      this.purpose = value;
      return this;
    }

    /**
     * Set the system key value.
     *
     * @param value
     *          The system key.
     * @return the builder for this class.
     */
    public Builder systemKey(String value) {
      this.systemKey = value;
      return this;
    }

    /**
     * Set the fields for this object that are required on the ERP.
     *
     * @param purposeValue
     *          The instance purpose.
     * @param systemKeyValue
     *          The system key.
     * @return the builder for this class.
     */
    public Builder requiredFields(String purposeValue, String systemKeyValue) {
      this.purpose = purposeValue;
      this.systemKey = systemKeyValue;
      return this;
    }

    /**
     * Build the data object.
     *
     * @return the data object.
     */
    public ActivateOnlineData build() {
      return new ActivateOnlineData(this);
    }
  }

  /**
   * Class constructor.
   *
   * @param builder
   *          The object builder.
   */
  private ActivateOnlineData(Builder builder) {
    purpose = builder.purpose;
    systemKey = builder.systemKey;
  }

  /**
   * Get the instance purpose.
   *
   * @return the instance purpose.
   */
  public String getPurpose() {
    return purpose;
  }

  /**
   * Get the system key.
   *
   * @return the system key.
   */
  public String getSystemKey() {
    return systemKey;
  }

  /**
   * Get the string representation of this data object.
   *
   * @return the string representation.
   */
  @Override
  public String toString() {
    return String.format(FORMAT_STRING_REPRESENTATION, purpose, systemKey);
  }

}
