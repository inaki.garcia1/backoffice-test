/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *   Nono Carballo <f.carballo@nectus.com>
 *************************************************************************
 */
package com.openbravo.test.integration.erp.gui.procurement.transactions.returntovendor;

import com.openbravo.test.integration.erp.modules.client.application.gui.StandardWindow;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;

/**
 *
 * Class for Return to Vendor Window
 *
 * @author Rafael Queralta
 *
 */
public class ReturnToVendorShipmentWindow extends StandardWindow {

  /** The window title. */
  @SuppressWarnings("hiding")
  public static final String TITLE = "Return to Vendor Shipment";
  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.PROCUREMENT_MANAGEMENT,
      Menu.PROCUREMENT_MANAGEMENT_TRANSACTIONS, Menu.RETURN_TO_VENDOR_SHIPMENT };

  public ReturnToVendorShipmentWindow() {
    super(TITLE, MENU_PATH);
  }

  @Override
  public void load() {
    final ReturnToVendorShipmentHeaderTab headerTab = new ReturnToVendorShipmentHeaderTab();
    addTopLevelTab(headerTab);
    super.load();
  }

}
