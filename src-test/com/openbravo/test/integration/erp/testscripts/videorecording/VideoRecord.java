/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.videorecording;

import static org.monte.media.FormatKeys.EncodingKey;
import static org.monte.media.FormatKeys.FrameRateKey;
import static org.monte.media.FormatKeys.KeyFrameIntervalKey;
import static org.monte.media.FormatKeys.MIME_AVI;
import static org.monte.media.FormatKeys.MediaTypeKey;
import static org.monte.media.FormatKeys.MimeTypeKey;
import static org.monte.media.VideoFormatKeys.CompressorNameKey;
import static org.monte.media.VideoFormatKeys.DepthKey;
import static org.monte.media.VideoFormatKeys.ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE;
import static org.monte.media.VideoFormatKeys.QualityKey;

import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.io.File;

import org.monte.media.Format;
import org.monte.media.FormatKeys.MediaType;
import org.monte.media.math.Rational;

import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Class to manage ScreenRecorder
 *
 * @author Lorenzo Fidalgo
 */
public class VideoRecord {
  public SpecializedScreenRecorder screenRecorder;
  File file = new File("last-results" + File.separator
      + ConfigurationProperties.INSTANCE.getOpenbravoContext() + File.separator + "videorecords");
  String name = "TestRecord";

  public void startRecording() throws Exception {

    // These coordinates & width & height are used if it is wanted to record all the screen(s)
    // Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    // int width = screenSize.width;
    // int height = screenSize.height;
    // Rectangle captureSize = new Rectangle(0, 0, width, height);

    // These coordinates & width & height are used to capture only browser screen. It has been added
    // some pixels (98) in Rectangle instance to avoid to capture address bar. This is static and it
    // is acceptable while screen dimension remains. This is set in SeleniumSinglenton.java class
    // with the following order: ".....setSize(new Dimension(1024, 768));"
    int width = SeleniumSingleton.INSTANCE.manage().window().getSize().width;
    int height = SeleniumSingleton.INSTANCE.manage().window().getSize().height;

    Rectangle captureSize = new Rectangle(
        SeleniumSingleton.INSTANCE.manage().window().getPosition().getX(),
        (SeleniumSingleton.INSTANCE.manage().window().getPosition().getY() + 98), width,
        (height - 98));

    GraphicsConfiguration gc = GraphicsEnvironment.getLocalGraphicsEnvironment()
        .getDefaultScreenDevice()
        .getDefaultConfiguration();

    this.screenRecorder = new SpecializedScreenRecorder(gc, captureSize,
        new Format(MediaTypeKey, MediaType.FILE, MimeTypeKey, MIME_AVI),
        new Format(MediaTypeKey, MediaType.VIDEO, EncodingKey,
            ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE, CompressorNameKey,
            ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE, DepthKey, 24, FrameRateKey, Rational.valueOf(15),
            QualityKey, 1.0f, KeyFrameIntervalKey, 15 * 60),
        new Format(MediaTypeKey, MediaType.VIDEO, EncodingKey, "black", FrameRateKey,
            Rational.valueOf(30)),
        null, file, name);
    this.screenRecorder.start();
  }

  public void stopRecording() throws Exception {
    this.screenRecorder.stop();
  }

  public File getFile() {
    return file;
  }

  public void setFile(File file) {
    this.file = file;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
