/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:15
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.sales.transactions.commissionpayment;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for DetailsData
 *
 * @author plujan
 *
 */
public class DetailsData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the actualQuantity value
     *
     * Description: The actual quantity
     *
     * @param value
     *          The actualQuantity value.
     * @return The builder for this class.
     */
    public Builder actualQuantity(String value) {
      this.dataFields.put("actualQuantity", value);
      return this;
    }

    /**
     * Set the convertedAmount value
     *
     * Description: The monetary sum at which one unit of measure is changed to another.
     *
     * @param value
     *          The convertedAmount value.
     * @return The builder for this class.
     */
    public Builder convertedAmount(String value) {
      this.dataFields.put("convertedAmount", value);
      return this;
    }

    /**
     * Set the actualAmount value
     *
     * Description: The actual amount
     *
     * @param value
     *          The actualAmount value.
     * @return The builder for this class.
     */
    public Builder actualAmount(String value) {
      this.dataFields.put("actualAmount", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the comments value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The comments value.
     * @return The builder for this class.
     */
    public Builder comments(String value) {
      this.dataFields.put("comments", value);
      return this;
    }

    /**
     * Set the invoiceLine value
     *
     * Description: A statement displaying one item or charge in an invoice.
     *
     * @param value
     *          The invoiceLine value.
     * @return The builder for this class.
     */
    public Builder invoiceLine(String value) {
      this.dataFields.put("invoiceLine", value);
      return this;
    }

    /**
     * Set the salesOrderLine value
     *
     * Description: A unique and often automatically generated identifier for a sales order line.
     *
     * @param value
     *          The salesOrderLine value.
     * @return The builder for this class.
     */
    public Builder salesOrderLine(String value) {
      this.dataFields.put("salesOrderLine", value);
      return this;
    }

    /**
     * Set the reference value
     *
     * Description: The data type of this field.
     *
     * @param value
     *          The reference value.
     * @return The builder for this class.
     */
    public Builder reference(String value) {
      this.dataFields.put("reference", value);
      return this;
    }

    /**
     * Set the commissionAmount value
     *
     * Description: Generated Commission Amount
     *
     * @param value
     *          The commissionAmount value.
     * @return The builder for this class.
     */
    public Builder commissionAmount(String value) {
      this.dataFields.put("commissionAmount", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public DetailsData build() {
      return new DetailsData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private DetailsData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
