/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.sales.testsuites.copyfromorder;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;

/**
 * Testing Create From Orders with not product price defined.
 *
 * @author Mark
 *
 */

@RunWith(Parameterized.class)
public class CFSO_ProductPriceNotDefinedInPL extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  // Sales Order Copied from
  SalesOrderHeaderData salesOrderHeaderFromData;
  SalesOrderHeaderData salesOrderHeaderFromVerficationData;
  SalesOrderLinesData salesOrderLinesFromData;
  SalesOrderLinesData salesOrderLinesFromVerificationData;
  SalesOrderHeaderData bookedSalesOrderHeaderFromVerificationData;

  // Sales Order Copied To
  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderLinesData;
  SalesOrderLinesData salesOrderLinesVerificationData;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;

  /**
   * Class constructor.
   *
   */
  public CFSO_ProductPriceNotDefinedInPL(SalesOrderHeaderData salesOrderHeaderFromData,
      SalesOrderHeaderData salesOrderHeaderFromVerficationData,
      SalesOrderLinesData salesOrderLinesFromData,
      SalesOrderLinesData salesOrderLinesFromVerificationData,
      SalesOrderHeaderData bookedSalesOrderHeaderFromVerificationData,
      SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData,
      SalesOrderLinesData salesOrderLinesVerificationData,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData) {
    this.salesOrderHeaderFromData = salesOrderHeaderFromData;
    this.salesOrderHeaderFromVerficationData = salesOrderHeaderFromVerficationData;
    this.salesOrderLinesFromData = salesOrderLinesFromData;
    this.salesOrderLinesFromVerificationData = salesOrderLinesFromVerificationData;
    this.bookedSalesOrderHeaderFromVerificationData = bookedSalesOrderHeaderFromVerificationData;

    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> SalesOrderValues() {
    Object[][] data = new Object[][] { {

        new SalesOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("1 (USA)")
            .paymentTerms("30 days, 5")
            .build(),

        new SalesOrderHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .warehouse("USA warehouse")
            .paymentTerms("30 days, 5")
            .priceList("Customer A")
            .paymentMethod("1 (USA)")
            .build(),

        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("SVA")
            .priceListVersion("Customer A")
            .build()).orderedQuantity("25").tax("Exempt").build(),

        new SalesOrderLinesData.Builder().uOM("Day")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("50.00")
            .build(),

        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("50.00")
            .grandTotalAmount("50.00")
            .currency("EUR")
            .build(),

        new SalesOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer B").build())
            .paymentMethod("1 (USA)")
            .paymentTerms("30 days, 5")
            .build(),

        new SalesOrderHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº23")
            .warehouse("USA warehouse")
            .paymentTerms("30 days, 5")
            .priceList("Customer B")
            .paymentMethod("1 (USA)")
            .build(),

        new SalesOrderLinesData.Builder().lineNo("10")
            .orderedQuantity("25")
            .tax("Exempt 10%")
            .uOM("Day")
            .unitPrice("0.00")
            .listPrice("0.00")
            .lineNetAmount("0.00")
            .build(),

        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("0.00")
            .grandTotalAmount("0.00")
            .currency("EUR")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   */
  @Test
  public void CFSO_ProductPriceNotDefinedInPLTest() {
    logger.info("** Start of test case [CFSO_ProductPriceNotDefinedInPL]. "
        + "Creating lines from orders with product don't have any price"
        + " defined on it price lists **");

    /** Register the Sales order will be copied */
    SalesOrder orderFrom = new SalesOrder(mainPage).open();
    orderFrom.create(salesOrderHeaderFromData);
    orderFrom.assertSaved();
    orderFrom.assertData(salesOrderHeaderFromVerficationData);
    String orderNoFrom = orderFrom.getData("documentNo").toString();

    // Create order lines
    SalesOrder.Lines orderLinesFrom = orderFrom.new Lines(mainPage);
    orderLinesFrom.create(salesOrderLinesFromData);
    orderLinesFrom.assertSaved();
    orderLinesFrom.assertData(salesOrderLinesFromVerificationData);

    // Book the order
    orderFrom.book();
    orderFrom.assertProcessCompletedSuccessfully2();
    orderFrom.assertData(bookedSalesOrderHeaderFromVerificationData);

    /** Register a Sales order and copy from the previously order */
    SalesOrder order = new SalesOrder(mainPage).open();
    order.create(salesOrderHeaderData);
    order.assertSaved();
    order.assertData(salesOrderHeaderVerficationData);

    PickAndExecuteWindow<SalesOrderHeaderData> popup = order.copyFromOrders();
    popup.filter(new SalesOrderHeaderData.Builder().documentNo(orderNoFrom).build());
    List<Map<String, Object>> rows = popup.getSelectedRows();
    assertTrue(rows.size() == 1);
    popup.process();

    SalesOrder.Lines orderLines = order.new Lines(mainPage);
    orderLines.assertCount(1);
    orderLines.selectWithoutFiltering(0);
    orderLines.assertData(salesOrderLinesVerificationData);

    // Book the order
    order.book();
    order.assertProcessCompletedSuccessfully2();
    order.assertData(bookedSalesOrderHeaderVerificationData);

    logger.info("** Start of test case [CFSO_ProductPriceNotDefinedInPL]. "
        + "Creating lines from orders with product don't have any price"
        + " defined on it price lists **");
  }
}
