/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 *   David Miguélez <david.miguelez@openbravo.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.application.conversionrates;

import com.openbravo.test.integration.erp.data.generalsetup.application.currency.ConversionRatesData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;

/**
 * Executes and verifies actions on OpenbravoERP Conversion Rates tab.
 *
 * @author aferraz
 *
 */
public class ConversionRatesTab extends GeneratedTab<ConversionRatesData> {

  /** The tab title. */
  static final String TITLE = "Conversion Rates";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  static final String IDENTIFIER = "198";

  /**
   * Class constructor.
   *
   */
  public ConversionRatesTab() {
    super(TITLE, IDENTIFIER);
  }

}
