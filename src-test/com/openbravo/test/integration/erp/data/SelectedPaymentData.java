/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data;

import java.util.LinkedHashMap;

/**
 *
 * Class for SelectedPaymentData. The DataObject to be used when editing a record that is selected
 * in "AddPayment" window
 *
 * @author lorenzo.fidalgo
 *
 */
public class SelectedPaymentData extends DataObject {

  /**
   * Class builder
   *
   * @author lorenzo.fidalgo
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the amount of the payment
     *
     * @param value
     *          The amount value.
     * @return The builder for this class.
     */
    public Builder amount(String value) {
      this.dataFields.put("amount", value);
      return this;
    }

    /**
     * Set the paymentAmount of the payment
     *
     * @param value
     *          The paymentAmount value.
     * @return The builder for this class.
     */
    public Builder paymentAmount(String value) {
      this.dataFields.put("paymentAmount", value);
      return this;
    }

    /**
     * Set the invoiceNo of the payment
     *
     * @param value
     *          The invoiceNo value.
     * @return The builder for this class.
     */
    public Builder invoiceNo(String value) {
      this.dataFields.put("invoiceNo", value);
      return this;
    }

    /**
     * Set the documentNo of the credit
     *
     * @param value
     *          The documentNo value.
     * @return The builder for this class.
     */
    public Builder documentNo(String value) {
      this.dataFields.put("documentNo", value);
      return this;
    }

    /**
     * Set the Order No
     *
     * @param value
     *          The Order No value.
     * @return The builder for this class.
     */
    public Builder orderNo(String value) {
      this.dataFields.put("salesOrderNo", value);
      return this;
    }

    /**
     * Set the paidOut of the payment
     *
     * @param value
     *          The paidOut value.
     * @return The builder for this class.
     */
    public Builder paidOut(String value) {
      this.dataFields.put("paidOut", value);
      return this;
    }

    /**
     * Set the receivedIn of the payment
     *
     * @param value
     *          The receivedIn value.
     * @return The builder for this class.
     */
    public Builder receivedIn(String value) {
      this.dataFields.put("receivedIn", value);
      return this;
    }

    /**
     * Set the gLItem of the payment
     *
     * @param value
     *          The gLItem value.
     * @return The builder for this class.
     */
    public Builder gLItem(String value) {
      this.dataFields.put("gLItem", value);
      return this;
    }

    /**
     * Set the writeoff of the payment
     *
     * @param value
     *          The writeoff value.
     * @return The builder for this class.
     */
    public Builder writeoff(boolean value) {
      this.dataFields.put("writeoff", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public SelectedPaymentData build() {
      return new SelectedPaymentData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private SelectedPaymentData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
