/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2010-12-28 18:07:30
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.procurement.transactions;

/**
 *
 * @author plujan
 *
 */
public class JournalEntryLineData {

  /* Data fields */
  /** Account Number */
  private String accountNumber;
  /** Name */
  private String name;
  /** Debit */
  private String debit;
  /** Credit */
  private String credit;

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /* Builder fields */
    /** Account Number */
    private String accountNumber = null;
    /** Name */
    private String name = null;
    /** Debit */
    private String debit = null;
    /** Credit */
    private String credit = null;

    /**
     * Set the Account Number value.
     *
     * @param value
     *          The Account Number value.
     * @return The builder for this class.
     */
    public Builder accountNumber(String value) {
      this.accountNumber = value;
      return this;
    }

    /**
     * Set the Name value.
     *
     * @param value
     *          The Name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.name = value;
      return this;
    }

    /**
     * Set the Debit value.
     *
     * @param value
     *          The Debit value.
     * @return The builder for this class.
     */
    public Builder debit(String value) {
      this.debit = value;
      return this;
    }

    /**
     * Set the Credit value.
     *
     * @param value
     *          The Credit value.
     * @return The builder for this class.
     */
    public Builder credit(String value) {
      this.credit = value;
      return this;
    }

    /**
     * Set the fields for this object that are required on the ERP.
     *
     * @param accountNumberField
     *          The Account Number value.
     * @param nameField
     *          The Name value.
     * @param debitField
     *          The Debit value.
     * @param creditField
     *          The Credit value.
     * @return The builder for this class
     */
    public Builder requiredFields(String accountNumberField, String nameField, String debitField,
        String creditField) {
      this.accountNumber = accountNumberField;
      this.name = nameField;
      this.debit = debitField;
      this.credit = creditField;
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public JournalEntryLineData build() {
      return new JournalEntryLineData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private JournalEntryLineData(Builder builder) {
    accountNumber = builder.accountNumber;
    name = builder.name;
    debit = builder.debit;
    credit = builder.credit;

  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public String getName() {
    return name;
  }

  public String getDebit() {
    return debit;
  }

  public String getCredit() {
    return credit;
  }

  @Override
  public String toString() {
    return "JournalEntryLineData [accountNumber=" + accountNumber + ", name=" + name + ", debit="
        + debit + ", credit=" + credit + "]";
  }

}
