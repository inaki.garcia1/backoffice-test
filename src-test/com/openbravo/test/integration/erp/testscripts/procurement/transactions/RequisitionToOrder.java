/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Rafael Queralta Pozo <rafaelcuba81@gmail.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.procurement.transactions;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.openbravo.test.integration.erp.modules.client.application.gui.ClassicFormView;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on the window.
 *
 * @author Pablo Lujan
 */
public class RequisitionToOrder extends ClassicFormView {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The window title. */
  public static final String TITLE = "Requisition To Order";
  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.PROCUREMENT_MANAGEMENT,
      Menu.PROCUREMENT_MANAGEMENT_TRANSACTIONS, Menu.REQUISITIONTOORDER };

  /** Identifier of the lines check box. */
  private static final String CHECK_BOX_LINES = "inpRequisitionLine";
  /** Identifier of the selected lines check box. */
  private static final String CHECK_BOX_SELECTEDLINES = "inpSelectedReq";

  public String purchaseOrderId = null;

  /**
   * Class constructor.
   */
  public RequisitionToOrder() {
    super(TITLE, MENU_PATH);
  }

  public void selectOrders() {
    List<WebElement> lines = SeleniumSingleton.INSTANCE.findElementsByName(CHECK_BOX_LINES);
    if (lines.size() > 0) {
      Sleep.trySleep();
      for (WebElement record : lines) {
        record.click();
      }
      WebElement buttonAdd = SeleniumSingleton.INSTANCE.findElement(By.id("buttonAdd"));
      buttonAdd.click();
    }
  }

  public void selectOrdersForCreatePurchases() {
    List<WebElement> selectedlines = SeleniumSingleton.INSTANCE
        .findElementsByName(CHECK_BOX_SELECTEDLINES);
    if (selectedlines.size() > 0) {
      Sleep.trySleep();
      for (WebElement record : selectedlines) {
        record.click();
      }
      WebElement buttonCreate = SeleniumSingleton.INSTANCE.findElement(By.id("buttonCreate"));
      buttonCreate.click();
      Sleep.trySleep(3000);
      createPurchaseOrders();
    }
  }

  private void createPurchaseOrders() {
    String windowHandlerParent = SeleniumSingleton.INSTANCE.getWindowHandle();
    Set<String> windowHandlers = SeleniumSingleton.INSTANCE.getWindowHandles();
    logger.info("windowHandlers.size() {}", windowHandlers::size);
    for (String winHandler : windowHandlers) {
      SeleniumSingleton.INSTANCE.switchTo().window(winHandler);
    }
    WebElement buttonOK = SeleniumSingleton.INSTANCE.findElement(By.id("buttonOK"));
    buttonOK.click();
    Sleep.trySleep(3000);
    WebElement messageBoxIDMessage = SeleniumSingleton.INSTANCE
        .findElement(By.id("messageBoxIDMessage"));
    assertTrue(messageBoxIDMessage.getText().contains("Purchase Order"));
    purchaseOrderId = messageBoxIDMessage.getText().split(" ")[2].replaceFirst(":", "");
    SeleniumSingleton.INSTANCE.switchTo().window(windowHandlerParent);
  }

}
