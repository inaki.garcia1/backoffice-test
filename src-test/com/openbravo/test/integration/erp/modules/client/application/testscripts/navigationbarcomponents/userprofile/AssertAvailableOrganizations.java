/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.navigationbarcomponents.userprofile;

import org.junit.Test;

import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.UserProfile;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.UserProfile.ProfileTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Test the available organizations for a role.
 *
 * @author elopio
 *
 */
public class AssertAvailableOrganizations extends OpenbravoERPTest {

  /* Data for this test. */
  /** The role to test. */
  private final String role;
  /** The organizations that have to be available for the role. */
  private final String[] availableOrganizations;

  /**
   * Class constructor.
   *
   * @param role
   *          The role to test.
   * @param availableOrganizations
   *          The organizations that have to be available for the role.
   */
  public AssertAvailableOrganizations(String role, String[] availableOrganizations) {
    this.role = role;
    this.availableOrganizations = availableOrganizations;
  }

  /**
   * Test selecting a role and assert the available organizations for that role.
   */
  @Test
  public void onlyTheOrganizationsOfTheRoleShouldBeAvailable() {
    UserProfile componentUserProfile = mainPage.getNavigationBar().getComponentUserProfile();
    ProfileTab profileTab = componentUserProfile.openProfileTab();
    profileTab.fill(new ProfileData.Builder().role(role).build());
    profileTab.assertOrganizationsCount(availableOrganizations.length);
    profileTab.assertOrganizations(availableOrganizations);
  }
}
