/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions1;

import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.accounting.setup.accountingschema.AccountingSchemaData;
import com.openbravo.test.integration.erp.data.financial.accounting.setup.accountingschema.GeneralLedgerData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.setup.generalledgerconfiguration.GeneralLedgerConfiguration;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 30273
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression30273In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /** The General Ledger Configuration data. */
  AccountingSchemaData accountingSchemaData;
  GeneralLedgerData generalLedgerData1;
  GeneralLedgerData generalLedgerData2;
  AccountData accountHeaderData;
  TransactionsData transactionLinesData;
  TransactionsData transactionLinesVerificationData;

  /**
   * Class constructor.
   *
   * @param accountHeaderData
   *          The account Header Data.
   * @param transactionLinesData
   *          The transaction Header Data.
   */
  public APRRegression30273In(AccountingSchemaData accountingSchemaData,
      GeneralLedgerData generalLedgerData1, AccountData accountHeaderData,
      TransactionsData transactionLinesData, TransactionsData transactionLinesVerificationData,
      GeneralLedgerData generalLedgerData2) {
    this.accountingSchemaData = accountingSchemaData;
    this.generalLedgerData1 = generalLedgerData1;
    this.accountHeaderData = accountHeaderData;
    this.transactionLinesData = transactionLinesData;
    this.transactionLinesVerificationData = transactionLinesVerificationData;
    this.generalLedgerData2 = generalLedgerData2;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        /* Parameters for [APRRegression30273In] Create Financial Account Transaction. */
        new AccountingSchemaData.Builder().name("Main US/A/Euro").build(),
        new GeneralLedgerData.Builder().suspenseBalancingUse(false).build(),
        new AccountData.Builder().name("Spain Bank").build(),
        new TransactionsData.Builder().transactionType("BP Deposit")
            .gLItem("Fees")
            .depositAmount("20.00")
            .build(),
        new TransactionsData.Builder().transactionType("BP Deposit")
            .transactionDate(OBDate.CURRENT_DATE)
            .accountingDate(OBDate.CURRENT_DATE)
            .currency("EUR")
            .depositAmount("20.00")
            .paymentAmount("0.00")
            .organization("Spain")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("").build())
            .product(new ProductSelectorData.Builder().name("").build())
            .project("")
            .build(),
        new GeneralLedgerData.Builder().suspenseBalancingUse(true).build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 30273 - Payment In flow
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression30273InTest() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression30273In] Test regression 30273 - Payment In flow. **");

    GeneralLedgerConfiguration.GeneralLedgerConfigurationTab.select(mainPage, accountingSchemaData);
    GeneralLedgerConfiguration.GeneralLedgerConfigurationTab.GeneralAccountsTab.edit(mainPage,
        generalLedgerData1);

    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.Transaction transactions = financialAccount.new Transaction(mainPage);
    transactions.create(transactionLinesData);
    transactions.assertSaved();
    transactions.assertData(transactionLinesVerificationData);
    transactions.process();
    transactions.assertProcessCompletedSuccessfully2();
    transactions.post();
    Sleep.trySleep();
    financialAccount.open();
    Sleep.trySleep();
    assertTrue(transactions.isPosted());

    GeneralLedgerConfiguration.GeneralLedgerConfigurationTab.select(mainPage, accountingSchemaData);
    GeneralLedgerConfiguration.GeneralLedgerConfigurationTab.GeneralAccountsTab.edit(mainPage,
        generalLedgerData2);

    logger.info(
        "** End of test case [APRRegression30273In] Test regression 30273 - Payment In flow. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
