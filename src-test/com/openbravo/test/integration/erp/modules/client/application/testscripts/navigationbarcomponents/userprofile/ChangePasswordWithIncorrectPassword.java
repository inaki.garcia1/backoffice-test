/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Rowinson Gallego <rwn.gallego@gmail.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.navigationbarcomponents.userprofile;

import static org.junit.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.openbravo.test.integration.erp.data.ChangePasswordData;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.UserProfile;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.UserProfile.PasswordTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test the password change from the user profile widget added by Navigation Bar Components module,
 * entering an incorrect password.
 *
 * @author rwngallego
 *
 */
public class ChangePasswordWithIncorrectPassword extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this tests. */
  /** The password data. */
  private final ChangePasswordData passwordData;

  /**
   * Class constructor.
   *
   * @param passwordData
   *          The password data.
   */
  public ChangePasswordWithIncorrectPassword(ChangePasswordData passwordData) {
    this.passwordData = passwordData;
  }

  /**
   * Test that when you change the password but typing incorrectly the current password it fails
   * [NAV0200] Change password. Incorrect password
   */
  @Test
  public void incorrectConfirmationErrorShouldBeShown() {
    UserProfile componentUserProfile = mainPage.getNavigationBar().getComponentUserProfile();
    logger.info("[NAV0200] Change password. Incorrect password");
    PasswordTab passwordTab = componentUserProfile.openPasswordTab();
    passwordTab.changePassword(passwordData);
    Sleep.trySleep();
    assertTrue("Password error should be visible.", passwordTab.isPasswordErrorVisible());
  }
}
