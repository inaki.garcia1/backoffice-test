/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  David Miguélez <david.miguelez@openbravo.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.accounting.testsuites.FAMc_MulticurrencySchema;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;

/**
 * This test case posts a payment and a transaction.
 *
 * @author David Miguélez
 */

@RunWith(Parameterized.class)
public class FAMc_130PurchasePostPAY_TRX extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /* Data for [FAMc_130] Post a Payment and a transaction. */
  /** The purchase invoice header data. */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  /**
   * Expected journal entry lines. Is an array of arrays. Each line has account number, name, debit
   * and credit.
   */
  private String[][] journalEntryLines1;
  private String[][] journalEntryLines2;
  /** The payment out header data. */
  PaymentOutHeaderData paymentOutHeaderData;
  /** Expected journal entry lines for the payment out. */
  private String[][] paymentOutJournalEntryLines1;
  private String[][] paymentOutJournalEntryLines2;
  /** The account header data. */
  AccountData accountHeaderData;
  /** The transaction lines data. */
  TransactionsData transactionsHeaderData;
  /** Expected journal entry lines for the transaction. */
  private String[][] transactionJournalEntryLines1;
  private String[][] transactionJournalEntryLines2;

  /**
   * Class constructor.
   *
   * @param purchaseInvoiceHeaderData
   *          The purchase invoice header data.
   * @param journalEntryLines1
   *          Expected journal entry lines. Is an array of arrays. Each line has account number,
   *          name, debit and credit.
   * @param journalEntryLines2
   *          Expected journal entry lines. Is an array of arrays. Each line has account number,
   *          name, debit and credit.
   * @param paymentOutHeaderData
   *          The payment out header data.
   * @param paymentOutJournalEntryLines1
   *          Expected journal entry lines for the payment out.
   * @param paymentOutJournalEntryLines2
   *          Expected journal entry lines for the payment out.
   * @param accountHeaderData
   *          The account Header Data.
   * @param transactionsHeaderData
   *          The transaction lines data.
   * @param transactionJournalEntryLines1
   *          Expected journal entry lines for the transaction.
   * @param transactionJournalEntryLines2
   *          Expected journal entry lines for the transaction.
   */

  public FAMc_130PurchasePostPAY_TRX(PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      String[][] journalEntryLines1, String[][] journalEntryLines2,
      PaymentOutHeaderData paymentOutHeaderData, String[][] paymentOutJournalEntryLines1,
      String[][] paymentOutJournalEntryLines2, AccountData accountHeaderData,
      TransactionsData transactionsHeaderData, String[][] transactionJournalEntryLines1,
      String[][] transactionJournalEntryLines2) {

    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.journalEntryLines1 = journalEntryLines1;
    this.journalEntryLines2 = journalEntryLines2;
    this.paymentOutHeaderData = paymentOutHeaderData;
    this.paymentOutJournalEntryLines1 = paymentOutJournalEntryLines1;
    this.paymentOutJournalEntryLines2 = paymentOutJournalEntryLines2;
    this.accountHeaderData = accountHeaderData;
    this.transactionsHeaderData = transactionsHeaderData;
    this.transactionJournalEntryLines1 = transactionJournalEntryLines1;
    this.transactionJournalEntryLines2 = transactionJournalEntryLines2;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<Object[]> orderToCashValues() {

    return Arrays.asList(new Object[][] { {
        // Parameters for [FAMc_130] Post a Payment and a transaction.
        new PurchaseInvoiceHeaderData.Builder().documentNo("10000005").build(),
        new String[][] { { "60000", "Compras de mercaderías", "60.00", "" },
            { "40000", "Proveedores (euros)", "", "60.00" } },
        new String[][] { { "51200", "Service costs", "120.00", "" },
            { "21100", "Accounts payable", "", "120.00" } },
        new PaymentOutHeaderData.Builder().documentNo("700005").build(),
        new String[][] { { "40000", "Proveedores (euros)", "60.00", "" },
            { "40100", "Proveedores efectos comerciales a pagar", "", "60.00" } },
        new String[][] { { "55400", "Bank revaluation loss", "120.00", "" },
            { "21100", "Accounts payable", "120.00", "" },
            { "11300", "Bank in transit", "", "240.00" } },
        new AccountData.Builder().name("Accounting Documents DOLLAR").build(),
        new TransactionsData.Builder().documentNo("700005")
            .description("Invoice No.: 10000005.")
            .build(),
        new String[][] { { "40100", "Proveedores efectos comerciales a pagar", "60.00", "" },
            { "66800", "Diferencias negativas de cambio", "20.00", "" },
            { "57200", "Bancos e instituciones de crédito c/c vista euros", "", "80.00" } },
        new String[][] { { "11300", "Bank in transit", "240.00", "" },
            { "11100", "Petty Cash", "", "240.00" } } } });
  }

  /**
   * This test case posts a payment and a transaction.
   */
  @Test
  public void FAMc_130PurchasePostPaymentTransaction() {
    logger.info("** Start of test case [FAMc_130] Post a Payment and a transaction. **");

    final PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.select(purchaseInvoiceHeaderData);
    if (purchaseInvoice.isPosted()) {
      purchaseInvoice.unpost();
      // purchaseInvoice
      // .assertProcessCompletedSuccessfully2("Number of unposted documents = 1, Number of deleted
      // journal entries = 2");
    }
    purchaseInvoice.post();
    mainPage.closeView("Purchase Invoice");

    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines1.length);
    post.assertJournalLines2(journalEntryLines1);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");

    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines2.length);
    post.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    final PaymentOut paymentOut = new PaymentOut(mainPage).open();
    paymentOut.select(paymentOutHeaderData);
    if (paymentOut.isPosted()) {
      paymentOut.unpost();
      // paymentOut
      // .assertProcessCompletedSuccessfully2("Number of unposted documents = 1, Number of deleted
      // journal entries = 2");
    }
    paymentOut.post();
    mainPage.closeView("Payment Out");

    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(paymentOutJournalEntryLines1.length);
    post.assertJournalLines2(paymentOutJournalEntryLines1);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");

    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(paymentOutJournalEntryLines2.length);
    post.assertJournalLines2(paymentOutJournalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    final FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.Transaction transaction = financialAccount.new Transaction(mainPage);
    transaction.select(transactionsHeaderData);
    if (transaction.isPosted()) {
      transaction.unpost();
      // transaction
      // .assertProcessCompletedSuccessfully2("Number of unposted documents = 1, Number of deleted
      // journal entries = 2");
    }
    transaction.post();

    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(transactionJournalEntryLines1.length);
    post.assertJournalLines2(transactionJournalEntryLines1);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");

    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(transactionJournalEntryLines2.length);
    post.assertJournalLines2(transactionJournalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");
    logger.info("** End of test case [FAMc_130] Post a Payment and a transaction. **");
  }

}
