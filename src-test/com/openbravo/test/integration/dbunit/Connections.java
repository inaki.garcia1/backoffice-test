/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.dbunit;

import java.sql.Connection;
import java.sql.SQLException;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.ext.oracle.OracleDataTypeFactory;
import org.dbunit.ext.postgresql.PostgresqlDataTypeFactory;

/**
 * Noninstantiable utility class to handle DBUnit connections.
 *
 * @author elopio
 *
 */
public class Connections {

  // Suppress default constructor for noninstantiability.
  private Connections() {
    throw new AssertionError();
  }

  /**
   * Get the database connection ready to use with DBUnit.
   *
   * @param connection
   *          The SQL connection.
   * @return The database connection ready to use with DBUnit.
   */
  public static IDatabaseConnection getDbUnitConnection(Connection connection)
      throws DatabaseUnitException, SQLException {
    IDatabaseConnection dbUnitConnection = new DatabaseConnection(connection);
    String database = connection.getMetaData().getDatabaseProductName();
    DatabaseConfig config = dbUnitConnection.getConfig();
    if (database.equals("PostgreSQL")) {
      config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new PostgresqlDataTypeFactory());
    } else if (database.equals("Oracle")) {
      config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new OracleDataTypeFactory());
    }
    return dbUnitConnection;
  }
}
