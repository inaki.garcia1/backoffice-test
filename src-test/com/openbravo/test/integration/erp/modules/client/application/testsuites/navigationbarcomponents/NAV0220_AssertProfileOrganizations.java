/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testsuites.navigationbarcomponents;

import java.util.Arrays;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.modules.client.application.testscripts.navigationbarcomponents.userprofile.AssertAvailableOrganizations;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Assert the organizations available on the profile selection widget.
 *
 * @author elopio
 *
 */
@RunWith(Parameterized.class)
public class NAV0220_AssertProfileOrganizations extends AssertAvailableOrganizations {

  /**
   * Class constructor.
   *
   * @param role
   *          The role to test.
   * @param availableOrganizations
   *          The organizations that have to be available for the role.
   */
  public NAV0220_AssertProfileOrganizations(String role, String[] availableOrganizations) {
    super(role, availableOrganizations);
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<Object[]> organizationParameters() {
    Object[][] data = new Object[][] { { "System Administrator - System", new String[] { "*" } },
        { "QA Testing Admin - QA Testing", new String[] { "*", "Main", "Spain", "USA" } } };
    return Arrays.asList(data);
  }
}
