/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.application.heartbeatconfiguration;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for HeartbeatLogData
 *
 * @author plujan
 *
 */
public class HeartbeatLogData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the instanceNumber value
     *
     * Description: Professional Edition Instance Number
     *
     * @param value
     *          The instanceNumber value.
     * @return The builder for this class.
     */
    public Builder instanceNumber(String value) {
      this.dataFields.put("instanceNumber", value);
      return this;
    }

    /**
     * Set the enableCustomQueries value
     *
     * Description: Send Custom Queries to Heartbeat
     *
     * @param value
     *          The enableCustomQueries value.
     * @return The builder for this class.
     */
    public Builder enableCustomQueries(Boolean value) {
      this.dataFields.put("enableCustomQueries", value);
      return this;
    }

    /**
     * Set the rejectedLoginsDueConcUsers value
     *
     * Description: Rejected_Logins_Due_Conc_Users
     *
     * @param value
     *          The rejectedLoginsDueConcUsers value.
     * @return The builder for this class.
     */
    public Builder rejectedLoginsDueConcUsers(String value) {
      this.dataFields.put("rejectedLoginsDueConcUsers", value);
      return this;
    }

    /**
     * Set the instancePurpose value
     *
     * Description: Instance Purpose
     *
     * @param value
     *          The instancePurpose value.
     * @return The builder for this class.
     */
    public Builder instancePurpose(String value) {
      this.dataFields.put("instancePurpose", value);
      return this;
    }

    /**
     * Set the usageAuditEnabled value
     *
     * Description: Is Usage Audit Enbled
     *
     * @param value
     *          The usageAuditEnabled value.
     * @return The builder for this class.
     */
    public Builder usageAuditEnabled(Boolean value) {
      this.dataFields.put("usageAuditEnabled", value);
      return this;
    }

    /**
     * Set the numberOfOrganizations value
     *
     * Description: Total number of organizations in the instance.
     *
     * @param value
     *          The numberOfOrganizations value.
     * @return The builder for this class.
     */
    public Builder numberOfOrganizations(String value) {
      this.dataFields.put("numberOfOrganizations", value);
      return this;
    }

    /**
     * Set the numberOfClients value
     *
     * Description: Total number of clients in the instance.
     *
     * @param value
     *          The numberOfClients value.
     * @return The builder for this class.
     */
    public Builder numberOfClients(String value) {
      this.dataFields.put("numberOfClients", value);
      return this;
    }

    /**
     * Set the maximumConcurrentUsers value
     *
     * Description: Maximum number of concurrent users during last 30 days
     *
     * @param value
     *          The maximumConcurrentUsers value.
     * @return The builder for this class.
     */
    public Builder maximumConcurrentUsers(String value) {
      this.dataFields.put("maximumConcurrentUsers", value);
      return this;
    }

    /**
     * Set the usagePercentage value
     *
     * Description: Percentage of time the instance has been in use (at least one session active)
     * during last 30 days.
     *
     * @param value
     *          The usagePercentage value.
     * @return The builder for this class.
     */
    public Builder usagePercentage(String value) {
      this.dataFields.put("usagePercentage", value);
      return this;
    }

    /**
     * Set the totalLoginsLastMonth value
     *
     * Description: Total number of logins during last 30 days
     *
     * @param value
     *          The totalLoginsLastMonth value.
     * @return The builder for this class.
     */
    public Builder totalLoginsLastMonth(String value) {
      this.dataFields.put("totalLoginsLastMonth", value);
      return this;
    }

    /**
     * Set the concurrentUsersAverage value
     *
     * Description: Average number of concurrent users during last 30 days
     *
     * @param value
     *          The concurrentUsersAverage value.
     * @return The builder for this class.
     */
    public Builder concurrentUsersAverage(String value) {
      this.dataFields.put("concurrentUsersAverage", value);
      return this;
    }

    /**
     * Set the totalLogins value
     *
     * Description: Total number of logins
     *
     * @param value
     *          The totalLogins value.
     * @return The builder for this class.
     */
    public Builder totalLogins(String value) {
      this.dataFields.put("totalLogins", value);
      return this;
    }

    /**
     * Set the lastLogin value
     *
     * Description: Date of last login in the application
     *
     * @param value
     *          The lastLogin value.
     * @return The builder for this class.
     */
    public Builder lastLogin(String value) {
      this.dataFields.put("lastLogin", value);
      return this;
    }

    /**
     * Set the firstLogin value
     *
     * Description: Date of first login in the application
     *
     * @param value
     *          The firstLogin value.
     * @return The builder for this class.
     */
    public Builder firstLogin(String value) {
      this.dataFields.put("firstLogin", value);
      return this;
    }

    /**
     * Set the activationKeyIdentifier value
     *
     * Description: It is an identifier for activation key
     *
     * @param value
     *          The activationKeyIdentifier value.
     * @return The builder for this class.
     */
    public Builder activationKeyIdentifier(String value) {
      this.dataFields.put("activationKeyIdentifier", value);
      return this;
    }

    /**
     * Set the installedModules value
     *
     * Description: List of the modules installed in the instance
     *
     * @param value
     *          The installedModules value.
     * @return The builder for this class.
     */
    public Builder installedModules(String value) {
      this.dataFields.put("installedModules", value);
      return this;
    }

    /**
     * Set the macIdentifier value
     *
     * Description: Mac Address Identifier
     *
     * @param value
     *          The macIdentifier value.
     * @return The builder for this class.
     */
    public Builder macIdentifier(String value) {
      this.dataFields.put("macIdentifier", value);
      return this;
    }

    /**
     * Set the databaseIdentifier value
     *
     * Description: Unique Identifier of the instance's database
     *
     * @param value
     *          The databaseIdentifier value.
     * @return The builder for this class.
     */
    public Builder databaseIdentifier(String value) {
      this.dataFields.put("databaseIdentifier", value);
      return this;
    }

    /**
     * Set the beatType value
     *
     * Description: Type of beat
     *
     * @param value
     *          The beatType value.
     * @return The builder for this class.
     */
    public Builder beatType(String value) {
      this.dataFields.put("beatType", value);
      return this;
    }

    /**
     * Set the codeRevision value
     *
     * Description: The code revision of the installed system
     *
     * @param value
     *          The codeRevision value.
     * @return The builder for this class.
     */
    public Builder codeRevision(String value) {
      this.dataFields.put("codeRevision", value);
      return this;
    }

    /**
     * Set the webServerVersion value
     *
     * Description: The version of Web Server
     *
     * @param value
     *          The webServerVersion value.
     * @return The builder for this class.
     */
    public Builder webServerVersion(String value) {
      this.dataFields.put("webServerVersion", value);
      return this;
    }

    /**
     * Set the numberOfRegisteredUsers value
     *
     * Description: Number of registered users.
     *
     * @param value
     *          The numberOfRegisteredUsers value.
     * @return The builder for this class.
     */
    public Builder numberOfRegisteredUsers(String value) {
      this.dataFields.put("numberOfRegisteredUsers", value);
      return this;
    }

    /**
     * Set the openbravoInstallMode value
     *
     * Description: The method used to install Openbravo.
     *
     * @param value
     *          The openbravoInstallMode value.
     * @return The builder for this class.
     */
    public Builder openbravoInstallMode(String value) {
      this.dataFields.put("openbravoInstallMode", value);
      return this;
    }

    /**
     * Set the openbravoVersion value
     *
     * Description: The version of Openbravo.
     *
     * @param value
     *          The openbravoVersion value.
     * @return The builder for this class.
     */
    public Builder openbravoVersion(String value) {
      this.dataFields.put("openbravoVersion", value);
      return this;
    }

    /**
     * Set the antVersion value
     *
     * Description: The version of Ant used to build the application.
     *
     * @param value
     *          The antVersion value.
     * @return The builder for this class.
     */
    public Builder antVersion(String value) {
      this.dataFields.put("antVersion", value);
      return this;
    }

    /**
     * Set the javaVersion value
     *
     * Description: Java Version
     *
     * @param value
     *          The javaVersion value.
     * @return The builder for this class.
     */
    public Builder javaVersion(String value) {
      this.dataFields.put("javaVersion", value);
      return this;
    }

    /**
     * Set the webServer value
     *
     * Description: The Web Server being used by Openbravo
     *
     * @param value
     *          The webServer value.
     * @return The builder for this class.
     */
    public Builder webServer(String value) {
      this.dataFields.put("webServer", value);
      return this;
    }

    /**
     * Set the servletContainerVersion value
     *
     * Description: The version of Servlet Container Openbravo runs in.
     *
     * @param value
     *          The servletContainerVersion value.
     * @return The builder for this class.
     */
    public Builder servletContainerVersion(String value) {
      this.dataFields.put("servletContainerVersion", value);
      return this;
    }

    /**
     * Set the servletContainer value
     *
     * Description: The type of servlet container Openbravo runs in.
     *
     * @param value
     *          The servletContainer value.
     * @return The builder for this class.
     */
    public Builder servletContainer(String value) {
      this.dataFields.put("servletContainer", value);
      return this;
    }

    /**
     * Set the databaseVersion value
     *
     * Description: The version of database.
     *
     * @param value
     *          The databaseVersion value.
     * @return The builder for this class.
     */
    public Builder databaseVersion(String value) {
      this.dataFields.put("databaseVersion", value);
      return this;
    }

    /**
     * Set the database value
     *
     * Description: The database in use by the system.
     *
     * @param value
     *          The database value.
     * @return The builder for this class.
     */
    public Builder database(String value) {
      this.dataFields.put("database", value);
      return this;
    }

    /**
     * Set the operatingSystemVersion value
     *
     * Description: The version of Operating System.
     *
     * @param value
     *          The operatingSystemVersion value.
     * @return The builder for this class.
     */
    public Builder operatingSystemVersion(String value) {
      this.dataFields.put("operatingSystemVersion", value);
      return this;
    }

    /**
     * Set the operatingSystem value
     *
     * Description: The type of Operating System.
     *
     * @param value
     *          The operatingSystem value.
     * @return The builder for this class.
     */
    public Builder operatingSystem(String value) {
      this.dataFields.put("operatingSystem", value);
      return this;
    }

    /**
     * Set the complexityRate value
     *
     * Description: The rate of complexity within the system.
     *
     * @param value
     *          The complexityRate value.
     * @return The builder for this class.
     */
    public Builder complexityRate(String value) {
      this.dataFields.put("complexityRate", value);
      return this;
    }

    /**
     * Set the activityRate value
     *
     * Description: The rate of activity within the system.
     *
     * @param value
     *          The activityRate value.
     * @return The builder for this class.
     */
    public Builder activityRate(String value) {
      this.dataFields.put("activityRate", value);
      return this;
    }

    /**
     * Set the proxyPort value
     *
     * Description: Proxy port on the proxy server used to access the internet.
     *
     * @param value
     *          The proxyPort value.
     * @return The builder for this class.
     */
    public Builder proxyPort(String value) {
      this.dataFields.put("proxyPort", value);
      return this;
    }

    /**
     * Set the proxyServer value
     *
     * Description: Proxy server used to access the internet.
     *
     * @param value
     *          The proxyServer value.
     * @return The builder for this class.
     */
    public Builder proxyServer(String value) {
      this.dataFields.put("proxyServer", value);
      return this;
    }

    /**
     * Set the proxyRequired value
     *
     * Description: Proxy configuration required to access internet.
     *
     * @param value
     *          The proxyRequired value.
     * @return The builder for this class.
     */
    public Builder proxyRequired(Boolean value) {
      this.dataFields.put("proxyRequired", value);
      return this;
    }

    /**
     * Set the systemIdentifier value
     *
     * Description: Unique ID identying this instance of Openbravo.
     *
     * @param value
     *          The systemIdentifier value.
     * @return The builder for this class.
     */
    public Builder systemIdentifier(String value) {
      this.dataFields.put("systemIdentifier", value);
      return this;
    }

    /**
     * Set the creationDate value
     *
     * Description: The date that this record is completed.
     *
     * @param value
     *          The creationDate value.
     * @return The builder for this class.
     */
    public Builder creationDate(String value) {
      this.dataFields.put("creationDate", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public HeartbeatLogData build() {
      return new HeartbeatLogData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private HeartbeatLogData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
