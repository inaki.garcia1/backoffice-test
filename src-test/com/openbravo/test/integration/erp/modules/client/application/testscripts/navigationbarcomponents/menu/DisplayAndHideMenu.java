/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.navigationbarcomponents.menu;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Test that the menu widget added by Navigation Bar Components module can be properly displayed and
 * hidden.
 *
 * @author elopio
 *
 */
public class DisplayAndHideMenu extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Test the display and hide of the menu.
   */
  @Test
  public void menuShouldBeDisplayedAndHidden() {
    Menu componentMenu = mainPage.getNavigationBar().getComponentMenu();
    logger.info("[NAV0080] Display and hide menu component");
    logger.info("Click Application (Menu) component to open it");
    componentMenu.open();
    assertTrue("Menu should be opened.", componentMenu.isOpen());
    logger.info("Click Application (Menu) component to close it");
    componentMenu.close();
    assertFalse("Menu should be closed.", componentMenu.isOpen());
  }
}
