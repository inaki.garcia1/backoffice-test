/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>,
 *  Inigo Sanchez <inigo.sanchez@openbravo.com>.
 *  Nono Carballo <f.carballo@nectus.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptException;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.InputField;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Executes actions on OpenbravoERP generated tabs build upon SmartClient user interface library.
 *
 * @author elopio
 * @param <T>
 *          The data object of this tab.
 *
 */
public abstract class GeneratedTab<T extends DataObject> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  // TODO is there a better way to do this?
  /** The tab identifier. This should be overridden by the subclass. */
  public static final String IDENTIFIER = "";

  /**
   * Formatting string used to get the button to create a new record in the form from the registry.
   * The parameter is the tab identifier.
   */
  private static final String FORMAT_REGISTRY_KEY_BUTTON_NEW_IN_FORM = "org.openbravo.client.application.toolbar.button.newDoc.%s";
  /**
   * Formatting string used to get the button to create a new record in the grid from the registry.
   * The parameter is the tab identifier.
   */
  private static final String FORMAT_REGISTRY_KEY_BUTTON_NEW_IN_GRID = "org.openbravo.client.application.toolbar.button.newRow.%s";
  /**
   * Formatting string used to get the save button from the registry. The parameter is the tab
   * identifier.
   */
  private static final String FORMAT_REGISTRY_KEY_BUTTON_SAVE = "org.openbravo.client.application.toolbar.button.save.%s";
  /**
   * Formatting string used to get the revert button from the registry. The parameter is the tab
   * identifier.
   */
  private static final String FORMAT_REGISTRY_KEY_BUTTON_REVERT = "org.openbravo.client.application.toolbar.button.undo.%s";
  /**
   * Formatting string used to get the delete button from the registry. The parameter is the tab
   * identifier.
   */
  private static final String FORMAT_REGISTRY_KEY_BUTTON_DELETE = "org.openbravo.client.application.toolbar.button.eliminate.%s";
  /**
   * Formatting string used to get the close button from the registry. The parameter is the tab
   * identifier.
   */
  @SuppressWarnings("unused")
  private static final String FORMAT_REGISTRY_KEY_BUTTON_CLOSE = "org.openbravo.client.application.statusbar.button.close.%s";

  /**
   * Formatting string used to get the refresh button from the registry. The parameter is the tab
   * identifier.
   */
  private static final String FORMAT_REGISTRY_KEY_BUTTON_REFRESH = "org.openbravo.client.application.toolbar.button.refresh.%s";
  /**
   * Formatting string used to get a child tab from the registry. The first parameter is the
   * identifier of the parent tab. The second parameter is the identifier of the child tab.
   */
  private static final String FORMAT_REGISTRY_KEY_CHILD_TAB = "org.openbravo.client.application.ChildTab_%s_%s";

  /** The title of the tab. */
  protected final String title;
  /** The identifier of the tab. */
  protected final String identifier;
  /** The tab level, starting from 0. */
  private final int level;
  /** The parent tab. */
  private GeneratedTab<?> parentTab;
  /** A map of the children tabs of this tab. The key is the tab title. */
  private Map<String, GeneratedTab<? extends DataObject>> childrenTabs;

  /* Tab components */
  protected OBStandardView<T> standardView;
  /** The children tab set. */
  protected StandardWindowTabSet childrenTabSet;

  /* Tool bar components. */
  /** The button to create a new record in the form. */
  private Button buttonNewInForm;
  /** The button to insert a new row in the grid. */
  private Button buttonInsertRow;
  /** The save record button. */
  private Button buttonSave;
  /** The revert changes button. */
  private Button buttonRevert;
  /** The delete record button. */
  private Button buttonDelete;
  /** The refresh button. */
  private Button buttonRefresh;
  /** The close button of the status bar. */
  private Button buttonClose;

  protected final LoadingPopUp loadingPopUp;

  /**
   * Top level tab constructor.
   *
   * @param title
   *          The title of the tab.
   * @param identifier
   *          The identifier of the tab.
   */
  public GeneratedTab(String title, String identifier) {
    this.title = title;
    this.identifier = identifier;
    this.level = 0;
    this.childrenTabs = new LinkedHashMap<String, GeneratedTab<? extends DataObject>>();

    this.standardView = new OBStandardView<T>(identifier);

    this.buttonNewInForm = new Button(TestRegistry
        .getObjectString(String.format(FORMAT_REGISTRY_KEY_BUTTON_NEW_IN_FORM, identifier)));
    this.buttonInsertRow = new Button(TestRegistry
        .getObjectString(String.format(FORMAT_REGISTRY_KEY_BUTTON_NEW_IN_GRID, identifier)));
    this.buttonSave = new Button(
        TestRegistry.getObjectString(String.format(FORMAT_REGISTRY_KEY_BUTTON_SAVE, identifier)));
    this.buttonRevert = new Button(
        TestRegistry.getObjectString(String.format(FORMAT_REGISTRY_KEY_BUTTON_REVERT, identifier)));
    this.buttonDelete = new Button(
        TestRegistry.getObjectString(String.format(FORMAT_REGISTRY_KEY_BUTTON_DELETE, identifier)));
    this.buttonRefresh = new Button(TestRegistry
        .getObjectString(String.format(FORMAT_REGISTRY_KEY_BUTTON_REFRESH, identifier)));

    loadingPopUp = new LoadingPopUp();
  }

  /**
   * Child tab constructor.
   *
   * @param title
   *          The title of the tab.
   * @param identifier
   *          The identifier of the tab.
   * @param level
   *          The level of the tab.
   * @param parentTab
   *          The parent of the tab.
   */
  public GeneratedTab(String title, String identifier, int level, GeneratedTab<?> parentTab) {
    if (level <= 0) {
      throw new IllegalArgumentException(String
          .format("The level of a child tab should be greater than 0. Value passed: '%d'.", level));
    }
    this.title = title;
    this.identifier = identifier;
    this.level = level;
    this.parentTab = parentTab;
    this.childrenTabs = new LinkedHashMap<String, GeneratedTab<? extends DataObject>>();
    // this.childrenTabSet = new StandardWindowTabSet(identifier);

    Boolean waitForLoad = this.level == 0;
    // Only wait for load in case it is a level 0 tab. Child tabs are loaded dynamically once they
    // are set as active views.
    this.standardView = new OBStandardView<T>(identifier, waitForLoad);

    this.buttonNewInForm = new Button(TestRegistry.getObjectString(
        String.format(FORMAT_REGISTRY_KEY_BUTTON_NEW_IN_FORM, identifier)), waitForLoad);
    this.buttonInsertRow = new Button(TestRegistry.getObjectString(
        String.format(FORMAT_REGISTRY_KEY_BUTTON_NEW_IN_GRID, identifier)), waitForLoad);
    this.buttonSave = new Button(
        TestRegistry.getObjectString(String.format(FORMAT_REGISTRY_KEY_BUTTON_SAVE, identifier)),
        waitForLoad);
    this.buttonRevert = new Button(
        TestRegistry.getObjectString(String.format(FORMAT_REGISTRY_KEY_BUTTON_REVERT, identifier)),
        waitForLoad);
    this.buttonDelete = new Button(
        TestRegistry.getObjectString(String.format(FORMAT_REGISTRY_KEY_BUTTON_DELETE, identifier)),
        waitForLoad);
    this.buttonRefresh = new Button(
        TestRegistry.getObjectString(String.format(FORMAT_REGISTRY_KEY_BUTTON_REFRESH, identifier)),
        waitForLoad);

    loadingPopUp = new LoadingPopUp();
  }

  /**
   * Get the title of the tab.
   *
   * @return the title of the tab.
   */
  public String getTitle() {
    return title;
  }

  /**
   * Get the identifier of the tab.
   *
   * @return the identifier of the tab.
   */
  public String getIdentifier() {
    return identifier;
  }

  /**
   * Get the level of the tab.
   *
   * @return the level of the tab.
   */
  public int getLevel() {
    return level;
  }

  /**
   * Get the parent of the tab.
   *
   * @return the parent of the tab.
   */
  public GeneratedTab<?> getParentTab() {
    return parentTab;
  }

  /**
   * Check if this is a top level tab.
   *
   * @return true if this is a top level tab. Otherwise, false.
   */
  public boolean isTopTab() {
    return parentTab == null;
  }

  /**
   * Get the sub tabs of this tab.
   *
   * @return the sub tabs of this tab.
   *
   */
  public Map<String, GeneratedTab<? extends DataObject>> getChildrenTabs() {
    return childrenTabs;
  }

  /**
   * Add a child tab.
   *
   * @param tab
   *          The child tab to add.
   */
  protected void addChildTab(GeneratedTab<? extends DataObject> tab) {
    if (!(tab.getLevel() == getLevel() + 1)) {
      throw new IllegalArgumentException(String.format(
          "The level of the child tab that will be added should be greater than the one of this tab by 1. Level of this tab: '%d'. Level of the child tab: '%d'.",
          getLevel(), tab.getLevel()));
    }
    childrenTabs.put(tab.getIdentifier(), tab);
  }

  /**
   * Get the selected child tab.
   *
   * @return the selected child tab.
   */
  protected GeneratedTab<? extends DataObject> getSelectedChildTab() {
    GeneratedTab<? extends DataObject> selectedTab = null;
    for (final String childIdentifier : childrenTabs.keySet()) {
      final String childRegistryKey = String.format(FORMAT_REGISTRY_KEY_CHILD_TAB, identifier,
          childIdentifier);
      // TODO what GUI component is this?
      final String childObjectString = TestRegistry.getObjectString(childRegistryKey);
      Boolean isSelected;
      try {
        isSelected = (Boolean) SeleniumSingleton.INSTANCE
            .executeScriptWithReturn(childObjectString + ".isSelected()");
      } catch (JavascriptException jse) {
        logger.warn("JavascriptException has been thrown. Sleeping and trying again");
        Sleep.trySleep(10000);
        isSelected = (Boolean) SeleniumSingleton.INSTANCE
            .executeScriptWithReturn(childObjectString + ".isSelected()");
      }
      if (isSelected != null && isSelected.equals(true)) {
        selectedTab = childrenTabs.get(childIdentifier);
        break;
      }
    }
    return selectedTab;
  }

  /**
   * Returns true if this tab is on form view.
   *
   * @return true if the tab is on form view. Otherwise, false.
   */
  public boolean isOnFormView() {
    return standardView.isOnFormView();
  }

  /**
   * Returns true if this tab is on form grid.
   *
   * @return true if the tab is on grid view. Otherwise, false.
   */
  public boolean isOnGridView() {
    return standardView.isOnGridView();
  }

  /**
   * Switch to grid view and wait until the grid has been loaded.
   */
  public void switchToGridView() {
    standardView.switchToGridView();
  }

  /**
   * Get the form component.
   *
   * @return the form component.
   */
  public OBForm getForm() {
    return standardView.getForm();
  }

  /**
   * Get the number of records in the tab.
   *
   * @return The number of records in the tab.
   */
  public int getRecordCount() {
    return standardView.getRecordCount();
  }

  /**
   * Wait for data to be fully loaded.
   */
  public void waitForDataToLoad() {
    if (isOnGridView()) {
      // if we are on grid view, wait till grid is loaded
      standardView.waitForDataToLoad();
    }
  }

  /**
   * Wait for a message to appear.
   */
  public void waitUntilMessageVisible() {
    standardView.waitUntilMessageVisible();
  }

  /**
   * Wait a bit if Info message is visible.
   */
  public void waitIfInfoMessageVisible() {
    standardView.waitIfInfoMessageVisible();
  }

  /**
   * Close the message if the message is visible
   */
  public void closeMessageIfVisible() {
    standardView.closeMessageIfVisible();
  }

  /**
   * Get the data of the selected record in this tab.
   *
   * @return the record data.
   */
  public DataObject getData() {
    return standardView.getData();
  }

  /**
   * Get a data field of the selected record in this tab.
   *
   * @param field
   *          The name of the field to get.
   *
   * @return the value of the field.
   */
  public Object getData(String field) {
    return standardView.getData(field);
  }

  /**
   * Filter the grid.
   *
   * @param dataObject
   *          Data object with the values of the filter.
   */
  public void filter(T dataObject) {
    standardView.filter(dataObject);
  }

  public void pickRecordInFilterDropDown(String columnName, int recordNo) {
    standardView.pickRecordInFilterDropDown(columnName, recordNo);
  }

  public int openFilterDropDown(String columnName) {
    return standardView.openFilterDropDown(columnName);
  }

  /**
   * Clear the filters.
   */
  public void clearFilters() {
    standardView.clearFilters();
  }

  /**
   * Unselect all records.
   */
  public void unselectAll() {
    standardView.unselectAll();
  }

  /**
   * Click on the column header button of the given name
   *
   * @param colName
   *          The name of the column to sort by
   */
  public void clickColumnHeader(String colName) {
    standardView.clickColumnHeader(colName);
  }

  /**
   * Select a record.
   *
   * @param dataObject
   *          Data object with the values of the record to select.
   */
  public void select(T dataObject) {
    standardView.select(dataObject);
  }

  /**
   * Sort Ascending using the context menu of the column header button of the given column in the
   * DataObject.
   *
   * @param dataObject
   *          The dataObject with the name (key) of the column that will be sorted.
   */
  public void sortAscending(T dataObject) {
    standardView.sortAscending(dataObject);
  }

  /**
   * Sort Descending using the context menu of the column header button of the given column in the
   * DataObject.
   *
   * @param dataObject
   *          The dataObject with the name (key) of the column that will be sorted.
   */
  public void sortDescending(T dataObject) {
    standardView.sortDescending(dataObject);
  }

  /**
   * Click the create new in form button and wait for the form to appear.
   */
  public void clickCreateNewInForm() {
    buttonNewInForm.smartClientClick();
    standardView.waitForFormView();
  }

  /**
   * [DEBUG] Click the create new in form button but does not wait for the form to appear.
   */
  public void clickCreateNewInFormNoWait() {
    buttonNewInForm.smartClientClick();
    Sleep.trySleep();
    // standardView.waitForFormView();
  }

  /**
   * Click the insert row button.
   */
  public void clickInsertRow() {
    buttonInsertRow.smartClientClick();
    standardView.waitUntilGridEditFormIsVisible();
  }

  /**
   * Create a record.
   *
   * @param dataObject
   *          Data object with the edited values.
   */
  public void createRecord(T dataObject) {
    createOnForm(dataObject);
  }

  /**
   * [Debug] Create a record.
   *
   * @param dataObject
   *          The required dataObject
   */
  public void createRecordNoWait(T dataObject) {
    createOnFormNoWait(dataObject);
  }

  /**
   * Create a record from the form.
   *
   * @param dataObject
   *          Data object with values of the new record.
   */
  public void createOnForm(T dataObject) {
    logger.trace("Creating a record from the form.");
    // TODO L2 Static sleep added to check if a slow process is there just before clicking in
    // "Create new record"
    Sleep.trySleep();
    clickCreateNewInForm();
    Sleep.trySleep();
    fillForm(dataObject);
    // These are the methods corresponding to two alternatives to force autosave.
    // Manually saving is the default option
    switch (ConfigurationProperties.INSTANCE.getAutosave()) {
      case 1:
        hotkeySaveOnForm();
        break;
      case 2:
        clickCloseOnForm();
        break;
      case 3:
        buttonSave.smartClientClick();
        // TODO L2: Remove the following static sleep once it is stable
        Sleep.trySleep(200);
        standardView.waitUntilSavedOrErrorOnForm();
        break;
      default:
        buttonSave.smartClientClick();
        // TODO L2: Remove the following static sleep once it is stable
        Sleep.trySleep(200);
        standardView.waitUntilSavedOrErrorOnForm();
        break;
    }
  }

  /**
   * Use CTRL + s hotkey to save on form
   */
  public void hotkeySaveOnForm() {
    // Save button is used to keep focus on the context to use the save hotkey
    buttonSave.useHotKeyToSave();
    standardView.waitUntilSavedOrErrorOnForm();
  }

  /**
   * Click on close button to close and autosave form
   */
  public void clickCloseOnForm() {
    buttonClose.smartClientClick();
    // To guarantee page refresh
    Sleep.trySleep(2000);
  }

  public void createOnFormWithoutSaving(T dataObject) {
    logger.debug("Creating a record from the form.");
    clickCreateNewInForm();
    fillForm(dataObject);
    Sleep.trySleep();
  }

  /**
   * [Debug] Create a record from the form.
   *
   * @param dataObject
   *          The required dataObject
   */
  public void createOnFormNoWait(T dataObject) {
    logger.debug("Creating a record from the form.");
    clickCreateNewInFormNoWait();
    fillForm(dataObject);
    buttonSave.smartClientClick();
    standardView.waitUntilSavedOrErrorOnForm();
  }

  /**
   * Fill the form.
   *
   * @param dataObject
   *          Data object with values to fill.
   */
  public void fillForm(T dataObject) {
    standardView.fillForm(dataObject);
  }

  /**
   * Create a record from the grid
   *
   * @param dataObject
   *          Data object with the edited values.
   */
  public void createOnGrid(T dataObject) {
    logger.debug("Creating a record from the grid.");
    clickInsertRow();
    standardView.fillRow(dataObject);
  }

  /**
   * Create a record from the grid and saves the record.
   *
   * @param dataObject
   *          Data object with the edited values.
   */
  public void createOnGridAndSave(T dataObject) {
    createOnGrid(dataObject);

    buttonSave.smartClientClick();
    standardView.waitUntilSavedOrErrorOnGrid();
    Sleep.trySleep();
  }

  /**
   * Delete a record from the grid.
   *
   */
  public void deleteOnGrid() {
    logger.debug("Deleting a record from the grid.");
    buttonDelete.smartClientClick();
    confirmDeletion();
    Sleep.trySleep();
  }

  /**
   * Delete a record from the grid using the shortcut.
   *
   */
  public void deleteOnGridWithShortcut() {
    logger.debug("Deleting a record from the grid using the Delete key.");
    // TODO: This has to be changed to a Keypress of Del key
    // buttonDelete.pressDelete();
    standardView.deleteOnGridWithShortcut();
    // buttonDelete.click();
    confirmDeletion();
    Sleep.trySleep();
  }

  public void confirmDeletion() {
    logger.debug("Confirming the log out warning.");
    final WarnDialog warnDialog = new WarnDialog();
    warnDialog.waitUntilVisible();
    warnDialog.clickYes();
  }

  /**
   * Edit the selected record from the grid.
   *
   * @param dataObject
   *          Data object with the edited values.
   */
  public void editRecord(T dataObject) {
    editOnForm(dataObject);
  }

  /**
   * Edit the selected record from the grid.
   *
   * @param dataObject
   *          Data object with the edited values.
   */
  public void editOnGrid(T dataObject) {
    logger.debug("Editing a record from the grid.");
    standardView.editOnGrid(dataObject);
    buttonSave.smartClientClick();
    standardView.waitUntilSavedOrErrorOnGrid();
    Sleep.trySleep();
  }

  /**
   * Edit the selected record from the grid and then cancel the edition.
   *
   * @param dataObject
   *          Data object with the edited values.
   */
  public void editAndCancelOnGrid(T dataObject) {
    logger.debug("Editing a record from the grid.");
    standardView.editOnGrid(dataObject);
    buttonRevert.smartClientClick();
    // TODO: wait for the cancel message to be shown
    // standardView.waitUntilSavedOrErrorOnGrid();
    Sleep.trySleep();
  }

  /**
   * Edit the selected record from the form.
   *
   * @param dataObject
   *          Data object with the edited values.
   */
  public void editOnForm(T dataObject) {
    logger.debug("Editing a record from the form.");
    standardView.editOnForm(dataObject);
    buttonSave.smartClientClick();
    standardView.waitUntilSavedOrErrorOnForm();
  }

  /**
   * Verify the data of the selected record.
   *
   * @param dataObject
   *          The data object with the expected values.
   */
  public void assertData(T dataObject) {
    standardView.assertData(dataObject);
  }

  /**
   * Verify the number of records in the tab.
   *
   * @param expectedCount
   *          The expected number of records.
   */
  public void assertCount(int expectedCount) {
    standardView.assertCount(expectedCount);
  }

  /**
   * Assert display logic visibility
   *
   * @param visibleDisplayLogic
   *          DataOject with the fields whose visibility has to be asserted.
   */
  public void assertDisplayLogicVisible(DataObject visibleDisplayLogic) {
    standardView.assertDisplayLogicVisible(visibleDisplayLogic);
  }

  /**
   * Assert display logic enable status
   *
   * @param enabledDisplayLogic
   *          DataOject with the fields whose enable status has to be asserted.
   */
  public void assertDisplayLogicEnabled(DataObject enabledDisplayLogic) {
    standardView.assertDisplayLogicEnabled(enabledDisplayLogic);
  }

  /**
   * Assert display logic expanded status
   *
   * @param expandedDisplayLogic
   *          DataOject with the fields whose expanded status has to be asserted.
   */
  public void assertDisplayLogicExpanded(DataObject expandedDisplayLogic) {
    standardView.assertDisplayLogicExpanded(expandedDisplayLogic);
  }

  /**
   * Assert filter value
   *
   * @param fieldName
   *          The name of the field whose filter value will be compared.
   * @param expectedValue
   *          Object with the expected value.
   */
  public void assertFilterValue(String fieldName, Object expectedValue) {
    standardView.assertFilterValue(fieldName, expectedValue);
  }

  /**
   * Open the selected record.
   */
  public void open() {
    standardView.open();
    standardView.waitForFormView();
  }

  /**
   * Close the form and wait for the grid to appear.
   */
  public void closeForm() {
    standardView.closeForm();
    standardView.waitForGridView();
  }

  /**
   * Get the contents of the message displayed in the message bar.
   *
   * @return the contents of the message displayed in the message bar.
   */
  public String getMessageContents() {
    return standardView.getMessageContents();
  }

  /**
   * Assert that the process completed successfully.
   */
  public void assertProcessCompletedSuccessfully() {
    standardView.assertProcessCompletedSuccessfully();
  }

  /**
   * Assert that the process completed successfully.
   *
   * TODO see issue 17291 - https://issues.openbravo.com/view.php?id=17291.
   */
  public void assertProcessCompletedSuccessfully2() {
    standardView.assertProcessCompletedSuccessfully2();
  }

  /**
   * Assert the process completed successfully message.
   *
   * TODO see issue 17291 - https://issues.openbravo.com/view.php?id=17291.
   *
   * @param message
   *          The expected message body.
   */
  public void assertProcessCompletedSuccessfully2(String message) {
    standardView.assertProcessCompletedSuccessfully2(message);
  }

  /**
   * Assert that the process completed successfully.
   *
   * @param message
   *          The expected message body.
   */
  public void assertProcessCompletedSuccessfully(String message) {
    standardView.assertProcessCompletedSuccessfully(message);
  }

  /**
   * Assert the process completed successfully message, using regular expression to match the body.
   *
   * @param message
   *          A regular expression for the expected message body.
   * @return a list with all the capturing groups of the regular expression.
   */
  public List<String> assertProcessCompletedSuccessfullyContentMatch(String message) {
    return standardView.assertProcessCompletedSuccessfullyContentMatch(message);
  }

  /**
   * Assert that the status bar shows the saved message.
   */
  public void assertSaved() {
    standardView.assertSaved();
    assertFalse("The save button should be disabled.", buttonSave.isEnabled());
    // TODO this assertion is not really useful. Should we ask the developers to display a message
    // saying that the record was saved?
  }

  public void assertNotInEditMode() {
    standardView.assertNotInEditMode();
  }

  public void assertInEditMode() {
    assertTrue("Revert and save buttons should be visible",
        buttonRevert.isVisible() && buttonSave.isVisible());
    // standardView.assertInEditMode();
  }

  /**
   * Assert that the tab is in new mode.
   */
  public void assertNewMode() {
    standardView.assertNewMode();
  }

  /**
   * Assert that the error message was shown because some fields contain illegal values.
   */
  public void assertErrorFieldsContainIllegalValues() {
    standardView.assertErrorFieldsContainIllegalValues();
  }

  public void assertSaveButtonIsDisabled() {
    assertTrue("Save button should be disabled", !buttonSave.isEnabled());
  }

  /**
   * Wait until the tab has been selected.
   */
  public void waitUntilSelected() {
    standardView.waitUntilSelected();
  }

  public void deleteMultipleRecordsOneCheckboxOnGrid() {
    standardView.selectAllRecordsOneCheckboxOnGrid();
    buttonDelete.smartClientClick();
    confirmDeletion();
    Sleep.trySleep();
  }

  public void deleteMultipleRecordsMultipleCheckboxesOnGrid(int[] indexesToBeSelected) {
    standardView.selectMultipleRecordsOnGrid(indexesToBeSelected);
    buttonDelete.smartClientClick();
    confirmDeletion();
    Sleep.trySleep();
  }

  public void editAndCancelWithShortcutOnGrid(T editedDataObject) {
    logger.debug("Editing a record from the grid.");
    standardView.editOnGrid(editedDataObject);
    Sleep.trySleep(2000);
    cancelOnGridWithShortcut();
  }

  public void cancelOnGridWithShortcut() {
    standardView.cancelOnGridWithShortcut();
    // TODO: wait for the cancel message to be shown
    // standardView.waitUntilSavedOrErrorOnGrid();
    Sleep.trySleep();
  }

  /**
   * Get all rows in the grid
   */
  @SuppressWarnings("unchecked")
  public List<Map<String, Object>> getRows() {
    return (List<Map<String, Object>>) standardView.getRows();
  }

  /**
   * Get the selected row in the grid
   */
  @SuppressWarnings("unchecked")
  public List<Map<String, Object>> getSelectedRow() {
    return (List<Map<String, Object>>) standardView.getSelectedRow();
  }

  public void startEditionOnGrid(int row) {
    logger.debug("Starting the edition of a record from the grid.");
    standardView.startEditionOnGrid(row);
  }

  public void moveToNextRecordWhileEditingInGrid() {
    standardView.moveToNextRecordWhileEditingInGrid();
  }

  public void cancelOnGrid() {
    buttonRevert.smartClientClick();
    // TODO: wait for the cancel message to be shown
    // standardView.waitUntilSavedOrErrorOnGrid();
    Sleep.trySleep();
  }

  /**
   * Delete a record from the grid using the contextual menu.
   *
   */
  public void deleteOnGridUsingContextualMenu() {
    logger.debug("Deleting a record from the grid using the contextual menu.");
    standardView.deleteOnGridUsingContextualMenu();
    confirmDeletion();
    Sleep.trySleep();
  }

  public void editOnGridUsingContextualMenu(T editedDataObject) {
    logger.debug("Editing a record from the grid using the contextual menu.");
    standardView.editOnGridUsingContextualMenu(editedDataObject);
  }

  public void saveEdition() {
    logger.debug("Saving the record.");
    buttonSave.smartClientClick();
    standardView.waitUntilSavedOrErrorOnGrid();
    Sleep.trySleep();
  }

  /**
   * Select a record without filtering (the record gets the focus).
   *
   * @param row
   *          The row of the record that will be selected.
   */
  public void selectWithoutFiltering(int row) {
    standardView.selectWithoutFiltering(row);
  }

  public void openFirstRecordInFormWithDoubleClick() {
    standardView.openFirstRecordInFormWithDoubleClick();
  }

  public void createOnGridWithContextualMenu(T newDataObject) {
    standardView.createOnGridWithContextualMenu();
    fillRow(newDataObject);
  }

  public void createOnGridWithShortcut(T newDataObject) {
    standardView.createOnGridWithShortcut();
    fillRow(newDataObject);
  }

  public void fillRow(T dataObject) {
    standardView.fillRow(dataObject);
  }

  public InputField getField(String fieldName, boolean onlyAllowTextItems) {
    return standardView.getField(fieldName, onlyAllowTextItems);
  }

  public OBParameterWindow<?> openParameterWindow(String registryKeyButton) {
    Button button = new Button(TestRegistry.getObjectString(registryKeyButton));
    button.click();
    return loadParameterWindow();
  }

  public OBParameterWindow<?> loadParameterWindow() {
    // meant to be abstract, provides default implementation to avoid breaking the api
    return null;
  }

  public void refresh() {
    buttonRefresh.smartClientClick();
    Sleep.trySleep();
  }

  /**
   * Show a column on grid view.
   */
  public void showColumnOnGrid(String fieldName) {
    standardView.showColumnOnGrid(fieldName);
  }

  /**
   * Hide a column on grid view.
   */
  public void hideColumnOnGrid(String fieldName) {
    standardView.hideColumnOnGrid(fieldName);
  }
}
