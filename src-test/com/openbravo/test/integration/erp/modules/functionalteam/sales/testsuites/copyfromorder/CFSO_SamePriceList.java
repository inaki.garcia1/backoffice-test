/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.sales.testsuites.copyfromorder;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;

/**
 * Test Copy From Orders with same price list
 *
 * @author Mark
 *
 */

@RunWith(Parameterized.class)
public class CFSO_SamePriceList extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  // Sales Order Copied from
  SalesOrderHeaderData salesOrderHeaderFromData;
  SalesOrderHeaderData salesOrderHeaderFromVerficationData;
  SalesOrderLinesData salesOrderLinesFromData;
  SalesOrderLinesData salesOrderLinesFromVerificationData;
  SalesOrderHeaderData bookedSalesOrderHeaderFromVerificationData;

  // Sales Order Copied To
  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderLinesData;
  SalesOrderLinesData salesOrderLinesVerificationData;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;

  /**
   * Class constructor.
   *
   */
  public CFSO_SamePriceList(SalesOrderHeaderData salesOrderHeaderFromData,
      SalesOrderHeaderData salesOrderHeaderFromVerficationData,
      SalesOrderLinesData salesOrderLinesFromData,
      SalesOrderLinesData salesOrderLinesFromVerificationData,
      SalesOrderHeaderData bookedSalesOrderHeaderFromVerificationData,
      SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData,
      SalesOrderLinesData salesOrderLinesVerificationData,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData) {
    this.salesOrderHeaderFromData = salesOrderHeaderFromData;
    this.salesOrderHeaderFromVerficationData = salesOrderHeaderFromVerficationData;
    this.salesOrderLinesFromData = salesOrderLinesFromData;
    this.salesOrderLinesFromVerificationData = salesOrderLinesFromVerificationData;
    this.bookedSalesOrderHeaderFromVerificationData = bookedSalesOrderHeaderFromVerificationData;

    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> SalesOrderValues() {
    Object[][] data = new Object[][] { {

        new SalesOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paymentTerms("90 days")
            .invoiceTerms("Immediate")
            .build(),

        new SalesOrderHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .warehouse("USA warehouse")
            .paymentTerms("90 days")
            .priceList("Customer A")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paymentTerms("90 days")
            .invoiceTerms("Immediate")
            .build(),

        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGC")
            .priceListVersion("Customer A")
            .build()).orderedQuantity("10").tax("VAT 10% USA").build(),

        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("20.00")
            .build(),

        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("20.00")
            .grandTotalAmount("22.00")
            .currency("EUR")
            .build(),

        new SalesOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paymentTerms("90 days")
            .invoiceTerms("Immediate")
            .build(),

        new SalesOrderHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .warehouse("USA warehouse")
            .paymentTerms("90 days")
            .priceList("Customer A")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paymentTerms("90 days")
            .invoiceTerms("Immediate")
            .build(),

        new SalesOrderLinesData.Builder().lineNo("10")
            .orderedQuantity("10")
            .tax("Exempt 3%")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("20.00")
            .build(),

        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("20.00")
            .grandTotalAmount("20.00")
            .currency("EUR")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test copy lines from orders with the same price list
   */
  @Test
  public void CFSO_SamePriceTest() {
    logger.info(
        "** Start of test case [CFSO_SamePriceTest]. Creating lines from orders with same price list **");
    /** Register the Sales order will be copied */
    SalesOrder orderFrom = new SalesOrder(mainPage).open();
    orderFrom.create(salesOrderHeaderFromData);
    orderFrom.assertSaved();
    orderFrom.assertData(salesOrderHeaderFromVerficationData);
    String orderNoFrom = orderFrom.getData("documentNo").toString();
    String orderDate = orderFrom.getData("orderDate").toString();

    // Create order lines
    SalesOrder.Lines orderLinesFrom = orderFrom.new Lines(mainPage);
    orderLinesFrom.create(salesOrderLinesFromData);
    orderLinesFrom.assertSaved();
    orderLinesFrom.assertData(salesOrderLinesFromVerificationData);

    // Book the order
    orderFrom.book();
    orderFrom.assertProcessCompletedSuccessfully2();
    orderFrom.assertData(bookedSalesOrderHeaderFromVerificationData);

    /** Register a Sales order and copy from the previously order */
    SalesOrder order = new SalesOrder(mainPage).open();
    order.create(salesOrderHeaderData);
    order.assertSaved();
    order.assertData(salesOrderHeaderVerficationData);

    PickAndExecuteWindow<SalesOrderHeaderData> popup = order.copyFromOrders();
    popup.filter(
        new SalesOrderHeaderData.Builder().documentNo(orderNoFrom).orderDate(orderDate).build());
    List<Map<String, Object>> rows = popup.getSelectedRows();
    assertTrue(rows.size() == 1);
    popup.process();

    SalesOrder.Lines orderLines = order.new Lines(mainPage);
    orderLines.assertCount(1);
    orderLines.selectWithoutFiltering(0);
    orderLines.assertData(salesOrderLinesVerificationData);

    // Book the order
    order.book();
    order.assertProcessCompletedSuccessfully2();
    order.assertData(bookedSalesOrderHeaderVerificationData);

    logger.info(
        "** End of test case [CFSO_SamePriceTest]. Creating lines from orders with same price list **");
  }
}
