/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *   Nono Carballo <f.carballo@nectus.com>
 *************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.aum;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseorder.PurchaseOrderLinesTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseorder.PurchaseOrderWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;

/**
 *
 * Class for Test UOMa060
 *
 * @author Nono Carballo
 *
 */

@RunWith(Parameterized.class)
public class AUMa060CreatePurchaseOrder extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  PurchaseOrderHeaderData headerData;
  PurchaseOrderLinesData lineData;
  PurchaseOrderLinesData lineDataSaved;
  PurchaseOrderLinesData lineDataAum;
  PurchaseOrderLinesData lineDataSavedAum;
  PurchaseOrderLinesData lineDataSelect;

  public AUMa060CreatePurchaseOrder(PurchaseOrderHeaderData headerData,
      PurchaseOrderLinesData lineData, PurchaseOrderLinesData lineDataSaved,
      PurchaseOrderLinesData lineDataAum, PurchaseOrderLinesData lineDataSavedAum,
      PurchaseOrderLinesData lineDataSelect) {
    super();
    this.headerData = headerData;
    this.lineData = lineData;
    this.lineDataSaved = lineDataSaved;
    this.lineDataAum = lineDataAum;
    this.lineDataSavedAum = lineDataSavedAum;
    this.lineDataSelect = lineDataSelect;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  @Parameters
  public static Collection<Object[]> returnValues() {
    Object[][] data = new Object[][] { {
        new PurchaseOrderHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),
        new PurchaseOrderLinesData.Builder()
            .product(new ProductSelectorData.Builder().name("Raw material B").build())
            .operativeQuantity("10")
            .build(),
        new PurchaseOrderLinesData.Builder().orderedQuantity("10").build(),
        new PurchaseOrderLinesData.Builder()
            .product(new ProductSelectorData.Builder().name("Raw material A").build())
            .operativeQuantity("10")
            .build(),
        new PurchaseOrderLinesData.Builder().orderedQuantity("30").build(),
        new PurchaseOrderLinesData.Builder()
            .product(new ProductSelectorData.Builder().name("Raw material B").build())
            .build() } };
    return Arrays.asList(data);
  }

  @Test
  public void UOMa060Test() throws ParseException {

    logger.debug("** Start test case [UOMa060] Create Purchase Order **");

    PurchaseOrder purchaseOrder = new PurchaseOrder(mainPage).open();
    purchaseOrder.create(headerData);
    ;
    purchaseOrder.assertSaved();

    PurchaseOrder.Lines purchaseOrderLine = purchaseOrder.new Lines(mainPage);
    purchaseOrderLine.create(lineData);
    purchaseOrderLine.assertSaved();
    purchaseOrderLine.assertData(lineDataSaved);

    purchaseOrderLine = purchaseOrder.new Lines(mainPage);
    purchaseOrderLine.create(lineDataAum);
    purchaseOrderLine.assertSaved();
    purchaseOrderLine.assertData(lineDataSavedAum);

    PurchaseOrderLinesTab purchaseOrderLineTab = ((PurchaseOrderWindow) mainPage
        .getView(PurchaseOrderWindow.TITLE)).selectPurchaseOrderLinesTab();
    purchaseOrderLineTab.select(lineDataSelect);
    purchaseOrderLineTab.deleteOnGrid();

    purchaseOrder.book();

    logger.debug("** End test case [UOMa060] Create Purchase Order **");
  }

}
