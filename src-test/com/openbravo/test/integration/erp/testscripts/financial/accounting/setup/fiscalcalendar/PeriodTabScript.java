/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.financial.accounting.setup.fiscalcalendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.StaleElementReferenceException;

import com.openbravo.test.integration.erp.data.financial.accounting.setup.fiscalcalendar.PeriodData;
import com.openbravo.test.integration.erp.gui.financial.accounting.setup.fiscalcalendar.FiscalCalendarWindow;
import com.openbravo.test.integration.erp.gui.financial.accounting.setup.fiscalcalendar.PeriodTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;

/**
 * Executes and verifies actions on the Period tab of OpenbravoERP.
 *
 * @author elopio
 *
 */
class PeriodTabScript {

  private static Logger logger = LogManager.getLogger();

  /**
   * Select a record.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param periodData
   *          The record to select.
   * @throws OpenbravoERPTestException
   */
  static void select(MainPage mainPage, PeriodData periodData) throws OpenbravoERPTestException {
    final FiscalCalendarWindow fiscalCalendarWindow = (FiscalCalendarWindow) mainPage
        .getView(FiscalCalendarWindow.TITLE);
    final PeriodTab periodTab = fiscalCalendarWindow.selectPeriodTab();
    try {
      periodTab.select(periodData);
    } catch (StaleElementReferenceException e) {
      logger.warn("StaleElementReferenceException while selecting period, continue anyway");
    }
  }

  /**
   * Verify the number of records.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param count
   *          The expected number of records.
   * @throws OpenbravoERPTestException
   */
  // FIXME: THIS METHOD IS VERIFYING NOTHING!!!
  static void verifyCount(MainPage mainPage, int count) throws OpenbravoERPTestException {
    final FiscalCalendarWindow fiscalCalendarWindow = (FiscalCalendarWindow) mainPage
        .getView(FiscalCalendarWindow.TITLE);
    @SuppressWarnings("unused")
    final PeriodTab periodTab = fiscalCalendarWindow.selectPeriodTab();

    // FIXME: This verification was not working properly
    // // TODO: Check why sometimes is not checking properly the periods.
    // Sleep.trySleep();
    //
    // assertThat("Number of periods", periodTab.getRecordCount(), is(equalTo(count)));
  }

}
