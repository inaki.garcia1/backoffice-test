/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.sales.transactions;

import com.openbravo.test.integration.erp.gui.sales.transactions.createshipmentsfromorders.CreateShipmentsFromOrdersWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the Create Shipments From window.
 *
 * @author elopio
 */
public class CreateShipmentsFromOrders {

  /** The application main page. */
  protected final MainPage mainPage;

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public CreateShipmentsFromOrders(MainPage mainPage) {
    this.mainPage = mainPage;
  }

  /**
   * Open the Create Shipments From window.
   *
   * @return the opened Create Shipments From window.
   */
  public CreateShipmentsFromOrders open() {
    CreateShipmentsFromOrdersWindow window = new CreateShipmentsFromOrdersWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the Create Shipments From tab.
   *
   * @return the Create Shipments From tab GUI object.
   */
  protected CreateShipmentsFromOrdersWindow selectTab() {
    CreateShipmentsFromOrdersWindow window = (CreateShipmentsFromOrdersWindow) mainPage
        .getView(CreateShipmentsFromOrdersWindow.TITLE);
    mainPage.getTabSetMain().selectTab(CreateShipmentsFromOrdersWindow.TITLE);
    window.waitForFrame();
    return window;
  }

  /**
   * Create a shipment from an order.
   *
   * The selected order is the first that appears on the table.
   *
   * @param numbers
   *          The numbers of the orders.
   */
  public void create(String... numbers) {
    selectTab().createShipmentsFromOrders(numbers);
  }

  /**
   * Verify that the process completed successfully
   *
   * @param numbers
   *          The numbers of the orders.
   */
  public void assertProcessCompletedSuccessfully(String... numbers) {
    selectTab().assertProcessCompletedSuccessfully(numbers);
  }

}
