/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testsuites.smoke.masterdata;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.heartbeatconfiguration.HeartbeatConfiguration;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.heartbeatconfiguration.HeartbeatConfigurationTabScript;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.intanceactivation.InstanceActivation;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.modulemanagement.ModuleManagement;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Execute the first part of Setup Instance flow of the smoke test suite.
 *
 * @author Leo Arias
 */
public class ADMba_SetupInstance extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Class constructor.
   */
  public ADMba_SetupInstance() {
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  /**
   * Test the setup instance flow.
   */
  @Test
  public void instanceShouldBeSetUp() {
    logger.info("** Start of test case [ADMb010] Disable Heartbeat. **");
    // TODO There are two possible flows. Right now we are just closing the pop up and following
    // flow 2.
    if (mainPage.isOnNewLayout()) {
      HeartbeatConfigurationTabScript heartbeatConfigurationTabScript = new HeartbeatConfigurationTabScript(
          mainPage).open();
      if (heartbeatConfigurationTabScript.disableHeartbeat()) {
        heartbeatConfigurationTabScript.assertDisabled();
      }
    } else {
      HeartbeatConfiguration.HeartbeatConfigurationTab.disableHeartbeat(mainPage);
      HeartbeatConfiguration.HeartbeatConfigurationTab.verifyDisabled(mainPage);
    }
    // TODO Verify the beat has been sent.
    logger.info("** End of test case [ADMb010] Disable Heartbeat. **");

    logger.info("** Start of test case [ADMb020] Deactivate Instance. **");
    ModuleManagement.InstalledModules.disableModules(mainPage);
    InstanceActivation.deactivateInstance(mainPage);
    logger.info("** End of test case [ADMb020] Deactivate Instance. **");
  }

  /**
   * Logout from Openbravo.
   */
  @Override
  public void logout() {
    mainPage.quit();
    // TODO accept the alert.
    // if (wasDeactivated) {
    // if was active an alert will appear
    // loginPage.acceptAlertOnLogout();
    // }
    // loginPage.verifyLogout();
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
    OpenbravoERPTest.seleniumStarted = false;
    SeleniumSingleton.INSTANCE.quit();
  }
}
