/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_AddPaymentRefactor.APR_AddPaymentRefactor2;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Paying an invoice, adding a gl item transaction type payment and generating credit, all in the
 * same payment.
 *
 * @author Unai Martirena
 */
@RunWith(Parameterized.class)
public class APRSales012 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  private static final String PARAM_ACTUAL_PAYMENT = "actual_payment";
  private static final String PARAM_OVERPYMT_ACTION = "overpayment_action";
  private static final String PARAM_REFERENCE_NO = "reference_no";

  /* Data for this test. */

  /* Data for [APRSales012] Create Sales Invoice. */
  /** The Sales Invoice header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  /** The data to verify the creation of the Sales Invoice header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  /** The data to verify the completion of the sales invoice. */
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  /** The data to verify the creation of the Sales Invoice lines data. */
  SalesInvoiceLinesData salesInvoiceLineData;
  /** The data to verify the creation of the Sales Invoice line. */
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  /** The data to verify the payment in plan. */
  PaymentPlanData paymentInPlanData;

  /* Data for [APRSales009] Select a payment created in APRSales006. */
  PaymentInHeaderData paymentInHeaderData;

  /* Data for [APRSales012] Add Payment. */
  /** The data to verify the data in the add payment pop up. */
  AddPaymentPopUpData addPaymentInVerificationData;
  /** The data to verify the totals data in the add payment pop up. */
  AddPaymentPopUpData addPaymentTotalsVerificationData;
  /** The data to verify the header of the payment. */
  PaymentInHeaderData payedPaymentInHeaderData;

  /**
   * Class constructor.
   *
   * @param salesInvoiceHeaderData
   *          The Sales Invoice header data.
   * @param salesInvoiceHeaderVerificationData
   *          The data to verify the creation of the Sales Invoice header data.
   * @param completedSalesInvoiceHeaderVerificationData
   *          The data to verify the completion of the sales invoice.
   * @param salesInvoiceLineData
   *          The data to verify the creation of the Sales Invoice lines data.
   * @param salesInvoiceLineVerificationData
   *          The data to verify the creation of the Sales Invoice line.
   * @param paymentInPlanData
   *          The data to verify the payment in plan.
   * @param addPaymentInVerificationData
   *          The data to verify the data in the add payment pop up.
   * @param addPaymentTotalsVerificationData
   *          The data to verify the totals data in the add payment pop up.
   * @param payedPaymentInHeaderData
   *          The data to verify the application of the payment.
   */
  public APRSales012(SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData, PaymentPlanData paymentInPlanData,
      PaymentInHeaderData paymentInHeaderData, AddPaymentPopUpData addPaymentInVerificationData,
      AddPaymentPopUpData addPaymentTotalsVerificationData,
      PaymentInHeaderData payedPaymentInHeaderData) {
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData = salesInvoiceLineData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.paymentInHeaderData = paymentInHeaderData;
    this.paymentInPlanData = paymentInPlanData;
    this.addPaymentInVerificationData = addPaymentInVerificationData;
    this.addPaymentTotalsVerificationData = addPaymentTotalsVerificationData;
    this.payedPaymentInHeaderData = payedPaymentInHeaderData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        /* Parameters for [APRSales012] Create Sales Invoice. */
        new SalesInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentTerms("30 days, 5")
            .build(),
        new SalesInvoiceHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentMethod("1 (Spain)")
            .paymentTerms("30 days, 5")
            .build(),
        new SalesInvoiceHeaderData.Builder().totalPaid("0.00")
            .outstandingAmount("207.00")
            .documentStatus("Completed")
            .summedLineAmount("200.00")
            .grandTotalAmount("207.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("FGA")
                .priceListVersionName("Customer A")
                .build())
            .invoicedQuantity("100")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good A").build())
            .invoicedQuantity("100")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT(3)+CHARGE(0.5)")
            .lineNetAmount("200.00")
            .build(),
        new PaymentPlanData.Builder().dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .paymentMethod("1 (Spain)")
            .expected("207.00")
            .received("0.00")
            .outstanding("207.00")
            .build(),
        /* Parameters for [APRSales009] Select Payment. */
        new PaymentInHeaderData.Builder().organization("Spain")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        /* Data for [APRSales012] Add Payment. */
        new AddPaymentPopUpData.Builder().received_from("Customer A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("EUR")
            .expected_payment("0.00")
            .actual_payment("0.00")
            .payment_date(OBDate.CURRENT_DATE)
            .transaction_type("Invoices")
            .build(),
        new AddPaymentPopUpData.Builder().amount_gl_items("1,500.00")
            .amount_inv_ords("207.00")
            .total("1,707.00")
            .difference("293.00")
            .build(),
        new PaymentInHeaderData.Builder().amount("2,000.00")
            .generatedCredit("293.00")
            .status("Payment Received")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test to create a sales order, and a goods receipt and sales invoice from that order.
   *
   * @throws ParseException
   */
  @Test
  public void APRSales012Test() throws ParseException {
    logger.info(
        "** Start of test case [APRSales012] Paying an invoice, adding a gl item transaction type payment and generating credit, all in the same payment. **");
    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    SalesInvoice.Lines salesInvoiceLines = salesInvoice.new Lines(mainPage);
    salesInvoiceLines.create(salesInvoiceLineData);
    salesInvoiceLines.assertSaved();
    salesInvoiceLines.assertData(salesInvoiceLineVerificationData);
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);

    String invoiceNo = salesInvoice.getData("documentNo").toString();

    SalesInvoice.PaymentInPlan paymentInPlan = salesInvoice.new PaymentInPlan(mainPage);
    paymentInPlan.assertCount(1);
    paymentInPlan.assertData(paymentInPlanData);

    PaymentIn paymentIn = new PaymentIn(mainPage).open();
    paymentIn.create(paymentInHeaderData);
    paymentIn.assertSaved();

    AddPaymentProcess addPaymentProcess = paymentIn.addDetailsOpen();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.assertData(addPaymentInVerificationData);
    addPaymentProcess.getOrderInvoiceGrid().filter("invoiceNo", invoiceNo);
    Sleep.trySleep(1000);
    addPaymentProcess.setParameterValue(PARAM_ACTUAL_PAYMENT, "2,000.00");
    addPaymentProcess.getField(PARAM_REFERENCE_NO).focus();
    addPaymentProcess.addGLItem(
        new SelectedPaymentData.Builder().gLItem("Salaries").receivedIn("1,500.00").build());

    addPaymentProcess.getField(PARAM_REFERENCE_NO).focus();
    addPaymentProcess.assertTotalsData(addPaymentTotalsVerificationData);
    addPaymentProcess.setParameterValue(PARAM_OVERPYMT_ACTION, "Refund amount to customer");
    addPaymentProcess.process("Process Received Payment(s)");

    paymentIn.assertRefundedPaymentCreatedSuccessfully();
    paymentIn.assertData(payedPaymentInHeaderData);
    PaymentIn.Lines paymentLines = paymentIn.new Lines(mainPage);
    paymentLines.assertCount(3);

    logger.info(
        "** End of test case [APRSales012] Paying an invoice, adding a gl item transaction type payment and generating credit, all in the same payment. **");
  }
}
