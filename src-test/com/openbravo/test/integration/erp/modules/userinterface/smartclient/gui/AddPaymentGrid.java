/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GridInputFieldFactory;
import com.openbravo.test.integration.util.TestUtils;

/**
 * Executes actions on OpenbravoERP grids built upon SmartClient user interface library.
 *
 * TODO should this class be moved to client.application.gui?
 *
 * @author lorenzo.fidalgo
 *
 */
public class AddPaymentGrid extends Grid {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Class constructor.
   *
   * @param objectString
   *          the string that can be used with SeleniumSingleton.INSTANCE.getEval function to get
   *          the object.
   * @param waitForLoad
   *          Set to true if it is required to wait until it is loaded
   */
  public AddPaymentGrid(String objectString, Boolean waitForLoad) {
    super(objectString, waitForLoad);
  }

  /**
   * Class constructor.
   *
   * @param objectString
   *          the string that can be used with SeleniumSingleton.INSTANCE.getEval function to get
   *          the object.
   */
  public AddPaymentGrid(String objectString) {
    this(objectString, true);
  }

  public void filter(SelectedPaymentData selectedPaymentData) {
    for (final String key : selectedPaymentData.getDataFields().keySet()) {
      String value = (String) selectedPaymentData.getDataFields().get(key);
      logger.trace("Filtering the grid using the value '{}' on the column '{}'.", value, key);
      String fieldSmartClientLocator = String.format(
          FORMAT_LOCATOR_SMART_CLIENT_FORM_ROW_EDITOR_BY_NAME, getFilterLocator(),
          TestUtils.convertFieldSeparator(key, false));
      InputField filterInputField = GridInputFieldFactory.getFilterInputFieldObject(objectString,
          key, fieldSmartClientLocator);
      filterInputField.focus();
      filterInputField.setValue(value);
    }
  }

}
