/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.remittance;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for RemittanceHeaderData
 *
 * @author plujan
 *
 */
public class RemittanceHeaderData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the paymentManagement value
     *
     * Description: Debt Payment Management
     *
     * @param value
     *          The paymentManagement value.
     * @return The builder for this class.
     */
    public Builder paymentManagement(String value) {
      this.dataFields.put("paymentManagement", value);
      return this;
    }

    /**
     * Set the settlement value
     *
     * Description: The process of exchanging or carrying out a payment once a transaction is made.
     *
     * @param value
     *          The settlement value.
     * @return The builder for this class.
     */
    public Builder settlement(String value) {
      this.dataFields.put("settlement", value);
      return this;
    }

    /**
     * Set the dueDate value
     *
     * Description: The date when a specified request must be carried out by.
     *
     * @param value
     *          The dueDate value.
     * @return The builder for this class.
     */
    public Builder dueDate(String value) {
      this.dataFields.put("dueDate", value);
      return this;
    }

    /**
     * Set the remittanceType value
     *
     * Description: Remittance Type
     *
     * @param value
     *          The remittanceType value.
     * @return The builder for this class.
     */
    public Builder remittanceType(String value) {
      this.dataFields.put("remittanceType", value);
      return this;
    }

    /**
     * Set the bankAccount value
     *
     * Description: A monetary account of funds held in a recognized banking institution.
     *
     * @param value
     *          The bankAccount value.
     * @return The builder for this class.
     */
    public Builder bankAccount(String value) {
      this.dataFields.put("bankAccount", value);
      return this;
    }

    /**
     * Set the transactionDate value
     *
     * Description: The date that a specified transaction is entered into the application.
     *
     * @param value
     *          The transactionDate value.
     * @return The builder for this class.
     */
    public Builder transactionDate(String value) {
      this.dataFields.put("transactionDate", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the documentNo value
     *
     * Description: An often automatically generated identifier for all documents.
     *
     * @param value
     *          The documentNo value.
     * @return The builder for this class.
     */
    public Builder documentNo(String value) {
      this.dataFields.put("documentNo", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public RemittanceHeaderData build() {
      return new RemittanceHeaderData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private RemittanceHeaderData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
