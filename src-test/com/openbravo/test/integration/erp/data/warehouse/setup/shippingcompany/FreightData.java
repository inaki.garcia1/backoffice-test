/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-01-25 18:28:20
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.warehouse.setup.shippingcompany;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for FreightData
 *
 * @author plujan
 *
 */
public class FreightData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the price value
     *
     * Description: The cost or value of a good or service.
     *
     * @param value
     *          The price value.
     * @return The builder for this class.
     */
    public Builder price(Boolean value) {
      this.dataFields.put("price", value);
      return this;
    }

    /**
     * Set the maxQuanity value
     *
     * Description: A parameter stating the highest possible quantity of a specified request.
     *
     * @param value
     *          The maxQuanity value.
     * @return The builder for this class.
     */
    public Builder maxQuanity(String value) {
      this.dataFields.put("maxQuanity", value);
      return this;
    }

    /**
     * Set the minQuantity value
     *
     * Description: Qty from
     *
     * @param value
     *          The minQuantity value.
     * @return The builder for this class.
     */
    public Builder minQuantity(String value) {
      this.dataFields.put("minQuantity", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the destinationCountry value
     *
     * Description: The country receiving a shipment
     *
     * @param value
     *          The destinationCountry value.
     * @return The builder for this class.
     */
    public Builder destinationCountry(String value) {
      this.dataFields.put("destinationCountry", value);
      return this;
    }

    /**
     * Set the country value
     *
     * Description: A state or a nation.
     *
     * @param value
     *          The country value.
     * @return The builder for this class.
     */
    public Builder country(String value) {
      this.dataFields.put("country", value);
      return this;
    }

    /**
     * Set the destinationRegion value
     *
     * Description: The state/province inside of a country receiving the shipment.
     *
     * @param value
     *          The destinationRegion value.
     * @return The builder for this class.
     */
    public Builder destinationRegion(String value) {
      this.dataFields.put("destinationRegion", value);
      return this;
    }

    /**
     * Set the validFromDate value
     *
     * Description: A parameter stating the starting time of a specified request.
     *
     * @param value
     *          The validFromDate value.
     * @return The builder for this class.
     */
    public Builder validFromDate(String value) {
      this.dataFields.put("validFromDate", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the freightAmount value
     *
     * Description: The charge amount for a specified shipment.
     *
     * @param value
     *          The freightAmount value.
     * @return The builder for this class.
     */
    public Builder freightAmount(String value) {
      this.dataFields.put("freightAmount", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the shippingCompany value
     *
     * Description: The name of the company making the shipment.
     *
     * @param value
     *          The shippingCompany value.
     * @return The builder for this class.
     */
    public Builder shippingCompany(String value) {
      this.dataFields.put("shippingCompany", value);
      return this;
    }

    /**
     * Set the region value
     *
     * Description: An area of a specific country.
     *
     * @param value
     *          The region value.
     * @return The builder for this class.
     */
    public Builder region(String value) {
      this.dataFields.put("region", value);
      return this;
    }

    /**
     * Set the freightCategory value
     *
     * Description: A classification used to help calculate shipping company freight amounts.
     *
     * @param value
     *          The freightCategory value.
     * @return The builder for this class.
     */
    public Builder freightCategory(String value) {
      this.dataFields.put("freightCategory", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public FreightData build() {
      return new FreightData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private FreightData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
