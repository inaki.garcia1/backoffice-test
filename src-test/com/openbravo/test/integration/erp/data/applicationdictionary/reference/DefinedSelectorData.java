/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.applicationdictionary.reference;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for DefinedSelectorData
 *
 * @author plujan
 *
 */
public class DefinedSelectorData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the suggestiontextmatchstyle value
     *
     * Description: Defines the text matching logic used in the suggestion box.
     *
     * @param value
     *          The suggestiontextmatchstyle value.
     * @return The builder for this class.
     */
    public Builder suggestiontextmatchstyle(String value) {
      this.dataFields.put("suggestiontextmatchstyle", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the displayfieldID value
     *
     * Description: The value of this field will be displayed in the selector text input box.
     *
     * @param value
     *          The displayfieldID value.
     * @return The builder for this class.
     */
    public Builder displayfieldID(String value) {
      this.dataFields.put("displayfieldID", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the valuefieldID value
     *
     * Description: The value of this field will be set in the foreign key column.
     *
     * @param value
     *          The valuefieldID value.
     * @return The builder for this class.
     */
    public Builder valuefieldID(String value) {
      this.dataFields.put("valuefieldID", value);
      return this;
    }

    /**
     * Set the filterExpression value
     *
     * Description: Defines a JavaScript expression that returns a HQL where clause
     *
     * @param value
     *          The filterExpression value.
     * @return The builder for this class.
     */
    public Builder filterExpression(String value) {
      this.dataFields.put("filterExpression", value);
      return this;
    }

    /**
     * Set the reference value
     *
     * Description: The data type of this field.
     *
     * @param value
     *          The reference value.
     * @return The builder for this class.
     */
    public Builder reference(String value) {
      this.dataFields.put("reference", value);
      return this;
    }

    /**
     * Set the popuptextmatchstyle value
     *
     * Description: Defines the text matching logic used in the filters in the popup grid.
     *
     * @param value
     *          The popuptextmatchstyle value.
     * @return The builder for this class.
     */
    public Builder popuptextmatchstyle(String value) {
      this.dataFields.put("popuptextmatchstyle", value);
      return this;
    }

    /**
     * Set the table value
     *
     * Description: A dictionary table used for this tab that points to the database table.
     *
     * @param value
     *          The table value.
     * @return The builder for this class.
     */
    public Builder table(String value) {
      this.dataFields.put("table", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the hQLWhereClause value
     *
     * Description: The HQL where clause is added to the query to retrieve the data which is
     * returned by the datasource.
     *
     * @param value
     *          The hQLWhereClause value.
     * @return The builder for this class.
     */
    public Builder hQLWhereClause(String value) {
      this.dataFields.put("hQLWhereClause", value);
      return this;
    }

    /**
     * Set the column value
     *
     * Description: A link to the database column of the table.
     *
     * @param value
     *          The column value.
     * @return The builder for this class.
     */
    public Builder column(String value) {
      this.dataFields.put("column", value);
      return this;
    }

    /**
     * Set the module value
     *
     * Description: Module
     *
     * @param value
     *          The module value.
     * @return The builder for this class.
     */
    public Builder module(String value) {
      this.dataFields.put("module", value);
      return this;
    }

    /**
     * Set the obclkerTemplateID value
     *
     * Description: The template used to generate the visualization of the component.
     *
     * @param value
     *          The obclkerTemplateID value.
     * @return The builder for this class.
     */
    public Builder obclkerTemplateID(String value) {
      this.dataFields.put("obclkerTemplateID", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the obserdsDatasourceID value
     *
     * Description: The datasource
     *
     * @param value
     *          The obserdsDatasourceID value.
     * @return The builder for this class.
     */
    public Builder obserdsDatasourceID(String value) {
      this.dataFields.put("obserdsDatasourceID", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the helpComment value
     *
     * Description: A comment that adds additional information to help users work with fields.
     *
     * @param value
     *          The helpComment value.
     * @return The builder for this class.
     */
    public Builder helpComment(String value) {
      this.dataFields.put("helpComment", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public DefinedSelectorData build() {
      return new DefinedSelectorData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private DefinedSelectorData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
