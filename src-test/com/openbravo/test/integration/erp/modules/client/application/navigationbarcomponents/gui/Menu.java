/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Martin Taal <martin.taal@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.client.application.gui.ViewLauncher;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verify actions on OpenbravoERP menu added by Navigation Bar module.
 *
 * @author elopio
 *
 */
public class Menu implements ViewLauncher {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Component elements. */
  /** The application menu button. This button opens the menu. */
  private final Button buttonApplicationMenu;
  /** The application menu. */
  private com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Menu menuApplication;

  /* Registry keys. */
  /** Registry key of the menu button. */
  private final String REGISTRY_KEY_BUTTON_APPLICATION_MENU = "org.openbravo.client.application.navigationbarcomponents.ApplicationMenuButton";
  /** Registry key of the application menu. */
  private final String REGISTRY_KEY_MENU_APPLICATION = "org.openbravo.client.application.navigationbarcomponents.ApplicationMenu";

  /* Menu items. */

  /** Application Dictionary menu item. */
  public static final String APPLICATION_DICTIONARY = "Application Dictionary";

  /** Reference menu item. */
  public static final String REFERENCE = "Reference";
  /** Module menu item. */
  public static final String MODULE = "Module";
  /** Windows, tabs and fields item. */
  public static final String WINDOWS_TABS_AND_FIELDS = "Windows, Tabs, and Fields";
  /** Process Definition, tabs and fields item. */
  public static final String PROCESS_DEFINITION = "Process Definition";

  /** General Setup menu item. */
  public static final String GENERAL_SETUP = "General Setup";

  /** Application menu item. */
  public static final String APPLICATION = "Application";
  /** Instance Activation menu item. */
  public static final String INSTANCE_ACTIVATION = "Instance Activation";
  /** Module Management menu item. */
  public static final String MODULE_MANAGEMENT = "Module Management";
  /** Conversion Rates menu item. */
  public static final String CONVERSION_RATES = "Conversion Rates";
  /** Preference menu item. */
  public static final String PREFERENCE = "Preference";
  /** Session Preferences menu item. */
  public static final String SESSION_PREFERENCES = "Session Preferences";
  /** Heartbeat Configuration menu item. */
  public static final String HEARTBEAT_CONFIGURATION = "Heartbeat Configuration";

  /** Client menu item. */
  public static final String CLIENT = "Client";
  /** Initial Client Setup menu item. */
  public static final String INITIAL_CLIENT_SETUP = "Initial Client Setup";
  /** Delete Client menu item. */
  public static final String DELETE_CLIENT = "Delete Client";
  /** Client menu item. */
  public static final String CLIENT_CLIENT = "Client";

  /** Security menu item. */
  public static final String SECURITY = "Security";
  /** User menu item. */
  public static final String USER = "User";
  /** Role menu item. */
  public static final String ROLE = "Role";

  /** Enterprise menu item. */
  public static final String ENTERPRISE = "Enterprise Model";
  /** Initial Organization Setup menu item. */
  public static final String INITIAL_ORGANIZATION_SETUP = "Initial organization setup";
  /** Enterprise Module Management menu item. */
  public static final String ENTERPRISE_MODULE_MANAGEMENT = "Enterprise module management";
  /** Organization menu item. */
  public static final String ORGANIZATION = "Organization";

  /** Process scheduling menu item. */
  public static final String PROCESS_SCHEDULING = "Process Scheduling";
  /** Process request menu item. */
  public static final String PROCESS_REQUEST = "Process Request";

  /** Master data management menu item. */
  public static final String MASTER_DATA_MANAGEMENT = "Master Data Management";

  /** Product menu item. */
  public static final String PRODUCT_CHARACTERISTIC = "Product Characteristic";

  /** Product menu item. */
  public static final String PRODUCT = "Product";

  /** Business Partner menu item. */
  public static final String BUSINESS_PARTNER = "Business Partner";

  /** Business Partner Setup menu item. */
  public static final String BUSINESS_PARTNER_SETUP = "Business Partner Setup";
  /** Business Partner Category menu item. */
  public static final String BUSINESS_PARTNER_CATEGORY = "Business Partner Category";
  /** Invoice Schedule menu item. */
  public static final String INVOICE_SCHEDULE = "Invoice Schedule";
  /** Payment Term menu item. */
  public static final String PAYMENT_TERM = "Payment Term";

  /** Product Setup menu item. */
  public static final String PRODUCT_SETUP = "Product Setup";
  /** Unit of measure menu item. */
  public static final String UNIT_OF_MEASURE = "Unit of Measure";
  /** Product Category menu item. */
  public static final String PRODUCT_CATEGORY = "Product Category";
  /** Attribute Set menu item. */
  public static final String ATTRIBUTE_SET = "Attribute Set";

  /** Pricing menu item. */
  public static final String PRICING = "Pricing";
  /** Price List schema menu item. */
  public static final String PRICE_LIST_SCHEMA = "Price List Schema";
  /** Price List menu item. */
  public static final String PRICE_LIST = "Price List";

  /** Import data menu item. */
  public static final String IMPORT_DATA = "Import Data";
  /** Import loader format menu item. */
  public static final String IMPORT_LOADER_FORMAT = "Import Loader Format";
  /** Import file loader menu item. */
  public static final String IMPORT_FILE_LOADER = "Import File Loader";
  /** Import Products menu item. */
  public static final String IMPORT_PRODUCTS = "Import Products";
  /** Import Business Partner menu item. */
  public static final String IMPORT_BUSINESS_PARTNER = "Import Business Partner";
  /** Import Account menu item. */
  public static final String IMPORT_ACCOUNT = "Import Account";
  /** Import Taxes menu item. */
  public static final String IMPORT_TAXES = "Import Taxes";

  /** Initial Data Load menu item. */
  public static final String INITIAL_DATA_LOAD = "Initial Data Load";
  /** Initial Data Load Setup menu item. */
  public static final String INITIAL_DATA_LOAD_SETUP = "Setup";
  /** Initial Data Load Entity Default Values menu item. */
  public static final String INITIAL_DATA_LOAD_ENTITY_DEFAULT_VALUES = "Entity Default Values";
  /** Initial Data Load Process menu item. */
  public static final String INITIAL_DATA_LOAD_PROCESS = "Process";
  /** Initial Data Load Import menu item. */
  public static final String INITIAL_DATA_LOAD_IMPORT = "Import";

  /** Procurement Management menu item. */
  public static final String PROCUREMENT_MANAGEMENT = "Procurement Management";

  /** Transactions menu item. */
  public static final String PROCUREMENT_MANAGEMENT_TRANSACTIONS = "Transactions";
  /** Requisition menu item. */
  public static final String REQUISITION = "Requisition";
  /** Requisitions To Order menu item. */
  public static final String REQUISITIONTOORDER = "Requisition To Order";
  /** Manage Requisitions menu item. */
  public static final String MANAGE_REQUISITIONS = "Manage Requisitions";
  /** Purchase Order menu item. */
  public static final String PURCHASE_ORDER = "Purchase Order";
  /** Goods Receipt menu item. */
  public static final String GOODS_RECEIPT = "Goods Receipt";
  /** Purchase Invoice menu item. */
  public static final String PURCHASE_INVOICE = "Purchase Invoice";
  /** Purchase Invoice menu item. */
  public static final String LANDED_COST = "Landed Cost";
  /** Return to Vendor menu item. */
  public static final String RETURN_TO_VENDOR = "Return to Vendor";
  /** Return to Vendor Shipment menu item. */
  public static final String RETURN_TO_VENDOR_SHIPMENT = "Return to Vendor Shipment";

  /** Warehouse Management menu item. */
  public static final String WAREHOUSE_MANAGEMENT = "Warehouse Management";

  /** Transactions menu item. */
  public static final String WAREHOUSE_TRANSACTIONS = "Transactions";
  /** Physical Inventory menu item. */
  public static final String PHYSICAL_INVENTORY = "Physical Inventory";
  /** Bill of Materials Production menu item. */
  public static final String BILL_OF_MATERIALS_PRODUCTION = "Bill of Materials Production";
  /** Stock Reservation menu item. */
  public static final String STOCK_RESERVATION = "Stock Reservation";

  /** Setup menu item. */
  public static final String WAREHOUSE_SETUP = "Setup";
  /** Warehouse and Storage Bins menu item. */
  public static final String WAREHOUSE_AND_STORAGE_BINS = "Warehouse and Storage Bins";

  /** Production Management menu item. */
  public static final String PRODUCTION_MANAGEMENT = "Production Management";

  /** Transactions menu item. */
  public static final String PRODUCTION_MANAGEMENT_TRANSACTIONS = "Transactions";
  /** Process Plan menu item. */
  public static final String PROCESS_PLAN = "Process Plan";
  /** Work Requirement menu item. */
  public static final String WORK_REQUIREMENT = "Work Requirement";
  /** Work Effort menu item. */
  public static final String WORK_EFFORT = "Work Effort";

  /** Setup menu item. */
  public static final String PRODUCTION_MANAGEMENT_SETUP = "folderCell2_800189";
  /** Activity menu item. */
  public static final String ACTIVITY = "Activity";
  /** Cost Center menu item. */
  public static final String COST_CENTER = "Cost Center";

  /** Material Requirement Planning menu item. */
  public static final String MATERIAL_REQUIREMENT_PLANNING = "Material Requirement Planning";

  /** Sales Management menu item. */
  public static final String SALES_MANAGEMENT = "Sales Management";

  /** Transactions menu item. */
  public static final String SALES_MANAGEMENT_TRANSACTIONS = "Transactions";
  /** Sales Order menu item. */
  public static final String SALES_ORDER = "Sales Order";
  /** Goods Shipment menu item. */
  public static final String GOODS_SHIPMENT = "Goods Shipment";
  /** Create Shipments from Orders menu item. */
  public static final String CREATE_SHIPMENTS_FROM_ORDERS = "Create Shipments from Orders";
  /** Generate Invoices menu item. */
  public static final String GENERATE_INVOICES = "Generate Invoices";
  /** Sales Invoice menu item. */
  public static final String SALES_INVOICE = "Sales Invoice";
  /** Return From Customer menu item. */
  public static final String RETURN_FROM_CUSTOMER = "Return from Customer";
  /** Return Material Receipt menu item. */
  public static final String RETURN_MATERIAL_RECEIPT = "Return Material Receipt";

  /** Setup menu item. */
  public static final String SALES_MANAGEMENT_SETUP = "Setup";
  /** Commission menu item. */
  public static final String COMMISSION = "Commission";

  /** Project and Service Management menu item. */
  public static final String PROJECT_AND_SERVICE_MANAGEMENT = "Project and Service Management";

  /** Transactions menu item. */
  public static final String PROJECT_AND_SERVICE_MANAGEMENT_TRANSACTIONS = "Transactions";
  /** Multiphase Project menu item. */
  public static final String MULTIPHASE_PROJECT = "Multiphase Project";
  /** Service Project menu item. */
  public static final String SERVICE_PROJECT = "Service Project";
  /** Expense Sheet menu item. */
  public static final String EXPENSE_SHEET = "Expense Sheet";
  /** Invoiceable Expenses menu item. */
  public static final String INVOICEABLE_EXPENSES = "Invoiceable Expenses";
  /** Create Sales Orders from Expenses menu item. */
  public static final String CREATE_SALES_ORDERS_FROM_EXPENSES = "Create Sales Orders from Expenses";
  /** Employee Expenses menu item. */
  public static final String EMPLOYEE_EXPENSES = "Employee Expenses";
  /** Create AP Expenses Invoices menu item. */
  public static final String CREATE_AP_EXPENSE_INVOICES = "Create AP Expense Invoices";

  /** Setup menu item. */
  public static final String PROJECT_AND_SERVICE_MANAEGEMENT_SETUP = "Setup";
  /** Project Type menu item. */
  public static final String PROJECT_TYPE = "Project Type";

  /** Financial Management menu item. */
  public static final String FINANCIAL_MANAGEMENT = "Financial Management";

  /** Receivables and Payables menu item. */
  public static final String RECEIVABLES_AND_PAYABLES = "Receivables and Payables";

  /** Transactions menu item. */
  public static final String RECEIVABLES_AND_PAYABLES_TRANSACTIONS = "Transactions";
  /** Financial Account menu item. */
  public static final String FINANCIAL_ACCOUNT = "Financial Account";
  /** Payment Out menu item. */
  public static final String PAYMENT_OUT = "Payment Out";
  /** Payment Proposal menu item. */
  public static final String PAYMENT_PROPOSAL = "Payment Proposal";
  /** Payment In menu item. */
  public static final String PAYMENT_IN = "Payment In";
  /** Bank Statement menu item. */
  public static final String BANK_STATEMENT = "Bank Statement";
  /** Cash Journal menu item. */
  public static final String CASH_JOURNAL = "Cash Journal";
  /** Remittance menu item. */
  public static final String REMITTANCE = "Remittance";
  /** Settlement menu item. */
  public static final String SETTLEMENT = "Settlement";

  /** Setup menu item. */
  public static final String RECEIVABLES_AND_PAYABLES_SETUP = "Setup";
  /** Payment Method menu item. */
  public static final String PAYMENT_METHOD = "Payment Method";
  /** Bank menu item. */
  public static final String BANK = "Bank";
  /** Cash Book menu item. */
  public static final String CASHBOOK = "Cashbook";
  /** Remittance Type menu item. */
  public static final String REMITTANCE_TYPE = "Remittance Type";

  /** Accounting menu item. */
  public static final String ACCOUNTING = "Accounting";

  /** Transactions menu item. */
  public static final String ACCOUNTING_TRANSACTIONS = "Transactions";
  /** G/L Journal menu item */
  public static final String G_L_JOURNAL = "G/L Journal";
  /** GL Posting by DB menu item. */
  public static final String GL_POSTING_BY_DB_TABLES = "GL Posting by DB Tables";
  /** Reset Accounting menu item. */
  public static final String RESET_ACCOUNTING = "Reset Accounting";
  /** Open/Close Period control menu item. */
  public static final String OPEN_CLOSE_PERIOD_CONTROL = "Open/Close Period Control";

  /** Setup menu item. */
  public static final String ACCOUNTING_SETUP = "Setup";
  /** Fiscal Calendar menu item. */
  public static final String FISCAL_CALENDAR = "Fiscal Calendar";
  /** Accounting tree menu item. */
  public static final String ACCOUNTING_TREE = "Account Tree";
  /** Accounting schema menu item. */
  public static final String ACCOUNTING_SCHEMA = "Accounting Schema";
  /** G/L Item menu item. */
  public static final String G_L_ITEM = "G%2fL Item";
  /** Document Type menu item. */
  public static final String DOCUMENT_TYPE = "Document Type";
  /** Tax Category menu item. */
  public static final String TAX_CATEGORY = "Tax Category";
  /** Tax Rate menu item. */
  public static final String TAX_RATE = "Tax Rate";
  /** Business Partner Tax Category menu item. */
  public static final String BUSINESS_PARTNER_TAX_CATEGORY = "Business Partner Tax Category";
  /** General Ledger Configuration menu item. */
  public static final String GENERAL_LEDGER_CONFIGURATION = "General Ledger Configuration";

  /** Analysis Tools menu item. */
  public static final String ACCOUNTING_ANALYSIS = "Analysis Tools";
  /** General Ledger Journal menu item. */
  public static final String GENERAL_LEDGER_JOURNAL = "General Ledger Journal";

  /** Assets menu item. */
  public static final String ASSETS = "Assets";
  /** Assets menu item. */
  public static final String ASSETS_ASSETS = "Assets";
  /** Asset Category menu item. */
  public static final String ASSET_CATEGORY = "Asset Category";
  /** Amortization menu item. */
  public static final String AMORTIZATIONS = "Amortizations";

  /** Identifier of the Openbravo ERP menu item */
  public static final String OPENBRAVO_ERP = "childel1000001";

  /** Identifier of the Openbravo ERP menu item. */
  public static final String INFORMATION = "childInformation";

  /**
   * Class constructor.
   *
   */
  public Menu() {
    this.buttonApplicationMenu = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_APPLICATION_MENU));
  }

  /**
   * Check if the menu is open.
   *
   * @return true if the menu is open. Otherwise, false.
   */
  public boolean isOpen() {
    if (menuApplication != null) {
      // XXX use this because we used the button to access menu items.
      String menuBodyLocator = "scLocator=" + SeleniumSingleton.INSTANCE.executeScriptWithReturn(
          TestRegistry.getObjectString(REGISTRY_KEY_MENU_APPLICATION) + ".body.getLocator()");
      return SeleniumSingleton.INSTANCE.findElementByScLocator(menuBodyLocator) != null
          && SeleniumSingleton.INSTANCE.findElementByScLocator(menuBodyLocator).isDisplayed();
    } else {
      return false;
    }
  }

  /**
   * Open the menu.
   */
  public void open() {
    // XXX Click the button only if the menu is not open.
    logger.debug("Opening menu.");
    buttonApplicationMenu.showMenuClick();
    // TODO why is the button and not the menu?
    menuApplication = new com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Menu(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_APPLICATION_MENU));
    menuApplication.waitUntilVisible();
  }

  /**
   * Close the menu.
   */
  public void close() {
    logger.debug("Closing the menu.");
    if (isOpen()) {
      buttonApplicationMenu.hideMenuClick();
    } else {
      logger.debug("Menu was already closed.");
    }
  }

  /**
   * Select an item from the menu.
   *
   * @param items
   *          the path of the item to select.
   */
  public void select(String... items) {
    final StringBuilder stringBuilder = new StringBuilder();
    for (String item : items) {
      if (stringBuilder.length() > 0) {
        stringBuilder.append(" || ");
      }
      stringBuilder.append(item);
    }
    logger.info("Go to {} menu.", stringBuilder::toString);
    open();
    menuApplication.select(items);
  }

  /**
   * Launch a view.
   *
   * @param items
   *          the path of the item to select.
   */
  @Override
  public void launch(String... items) {
    select(items);
  }

  /**
   * Pause the execution until the menu is loaded.
   */
  public void waitUntilMenuIsLoaded() {
    logger.debug("Waiting for the menu component to be visisble.");
    buttonApplicationMenu.waitUntilVisible();
  }

  /**
   * Return true if the menu item is present.
   *
   * @param item
   *          The text of the menu item.
   * @return true if the menu item is present.
   */
  public boolean isMenuItemPresent(String item) {
    open();
    return menuApplication.isItemPresent(item);
  }

}
