/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:15
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.projectservice.transactions.serviceproject;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;

/**
 *
 * Class for ProjectLineData
 *
 * @author plujan
 *
 */
public class ProjectLineData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the productDescription value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The productDescription value.
     * @return The builder for this class.
     */
    public Builder productDescription(String value) {
      this.dataFields.put("productDescription", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A identifier for a document which can be used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the identifier value
     *
     * Description: A value specified in many forms.
     *
     * @param value
     *          The identifier value.
     * @return The builder for this class.
     */
    public Builder identifier(String value) {
      this.dataFields.put("identifier", value);
      return this;
    }

    /**
     * Set the tax value
     *
     * Description: The percentage of money requested by the government for this specified product
     * or transaction.
     *
     * @param value
     *          The tax value.
     * @return The builder for this class.
     */
    public Builder tax(String value) {
      this.dataFields.put("tax", value);
      return this;
    }

    /**
     * Set the plannedPrice value
     *
     * Description: Planned price for this project line
     *
     * @param value
     *          The plannedPrice value.
     * @return The builder for this class.
     */
    public Builder plannedPrice(String value) {
      this.dataFields.put("plannedPrice", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the plannedAmount value
     *
     * Description: The monetary sum expected to be involved for a transaction line.
     *
     * @param value
     *          The plannedAmount value.
     * @return The builder for this class.
     */
    public Builder plannedAmount(String value) {
      this.dataFields.put("plannedAmount", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the plannedQuantity value
     *
     * Description: The expected or provisional quantity to be involved for a transaction line.
     *
     * @param value
     *          The plannedQuantity value.
     * @return The builder for this class.
     */
    public Builder plannedQuantity(String value) {
      this.dataFields.put("plannedQuantity", value);
      return this;
    }

    /**
     * Set the project value
     *
     * Description: Identifier of a project defined within the Project Service Management module.
     *
     * @param value
     *          The project value.
     * @return The builder for this class.
     */
    public Builder project(String value) {
      this.dataFields.put("project", value);
      return this;
    }

    /**
     * Set the plannedMargin value
     *
     * Description: The expected or provisional margin to be earned due to this transaction.
     *
     * @param value
     *          The plannedMargin value.
     * @return The builder for this class.
     */
    public Builder plannedMargin(String value) {
      this.dataFields.put("plannedMargin", value);
      return this;
    }

    /**
     * Set the lineNo value
     *
     * Description: A line stating the position of this request in the document.
     *
     * @param value
     *          The lineNo value.
     * @return The builder for this class.
     */
    public Builder lineNo(String value) {
      this.dataFields.put("lineNo", value);
      return this;
    }

    /**
     * Set the plannedPurchasePrice value
     *
     * Description: Planned Purchase Price
     *
     * @param value
     *          The plannedPurchasePrice value.
     * @return The builder for this class.
     */
    public Builder plannedPurchasePrice(String value) {
      this.dataFields.put("plannedPurchasePrice", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ProjectLineData build() {
      return new ProjectLineData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ProjectLineData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
