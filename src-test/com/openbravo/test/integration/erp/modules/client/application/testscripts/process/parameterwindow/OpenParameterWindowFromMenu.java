/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Rowinson Gallego <rwn.gallego@gmail.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.process.parameterwindow;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.openbravo.test.integration.erp.modules.client.application.gui.OBParameterWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Tests to open a parameter window from the menu
 *
 */
public abstract class OpenParameterWindowFromMenu extends OpenbravoERPTest {

  /**
   * Test to insert a new row in the grid.
   */
  @Test
  public void parameterWindowShouldBeOpenedd() {
    OBParameterWindow<?> parameterWindow = getParameterWindow();
    assertTrue("Parameter Window is not visible.", parameterWindow.isVisible());
  }

  /**
   * Open and return the generated parameter window that will be used for the test.
   *
   * @return the generated parameter window that will be used for the test.
   */
  public abstract OBParameterWindow<?> getParameterWindow();
}
