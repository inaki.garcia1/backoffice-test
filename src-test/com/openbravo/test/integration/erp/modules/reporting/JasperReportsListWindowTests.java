/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2020 Openbravo S.L.U.
 * All Rights Reserved.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.reporting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.modules.reporting.windows.ReportsServerReportListingData;
import com.openbravo.test.integration.erp.modules.reporting.windows.ReportsServerReportListingTab;
import com.openbravo.test.integration.erp.modules.reporting.windows.ReportsServerReportListingWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.util.ConfigurationProperties;

public class JasperReportsListWindowTests extends OpenbravoERPTest {

  private static final String REPORT_LIST_TAB_ID = "F6ABA9C38A0A4707B2976537A284D499";
  private static final String REPORT_DESCRIPTION = "";
  private static final String REPORT_LABEL = "AdHocViewDefinedInJasper";

  public JasperReportsListWindowTests() {
    super();
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  @Test
  public void openJasperReportsTab() {
    ReportsServerReportListingTab tab = getTab();
    tab.assertCount(1);

    ReportsServerReportListingData row = new ReportsServerReportListingData.Builder()
        .label(REPORT_LABEL)
        .description(REPORT_DESCRIPTION)
        .build();
    tab.assertData(row);

    List<WebElement> iframesBefore = getIframes();
    tab.openReport();
    List<WebElement> iframesAfter = getIframes();
    iframesAfter.removeAll(iframesBefore);

    assertEquals("The report should have opened in a new iframe", 1, iframesAfter.size());

    SeleniumSingleton.INSTANCE.switchTo().frame(iframesAfter.get(0));

    assertNotNull("Report iframe should contain a reportContainer element",
        SeleniumSingleton.INSTANCE.findElements(By.id("reportContainer")));
  }

  private List<WebElement> getIframes() {
    return SeleniumSingleton.INSTANCE.findElements(By.tagName("iframe"));
  }

  public ReportsServerReportListingTab getTab() {
    ReportsServerReportListingWindow reportList = new ReportsServerReportListingWindow();
    mainPage.openView(reportList);
    return (ReportsServerReportListingTab) reportList.selectTab(REPORT_LIST_TAB_ID);
  }

}
