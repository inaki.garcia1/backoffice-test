/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentproposal;

import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions on Select Expected Payments pop up.
 *
 * @author elopio
 *
 */
public class SelectExpectedPaymentsPopUp extends OBClassicPopUp {

  /* Components identifiers. */
  /** The identifier of the process button. */
  private static final String IDENTIFIER_BUTTON_PROCESS = "buttonProcess";

  /**
   * Formatting string used to select an order by its number. The parameter is the order number that
   * will be selected.
   */
  private static final String FORMAT_XPATH_SELECT_ORDER_BY_NUMBER = "//div[@id = 'client2']//td[@class = 'DataGrid_Body_Cell' and text() = '%s']/..//input[@type = 'checkbox']";

  /**
   * Class constructor.
   *
   */
  public SelectExpectedPaymentsPopUp() {
    super(TestRegistry.getObjectString(
        "org.openbravo.classicpopup./org.openbravo.advpaymentmngt.ad_actionbutton/SelectExpectedPayments.html"));
  }

  /**
   * Select the pop up frame.
   */
  @Override
  protected void selectFrame() {
    SeleniumSingleton.INSTANCE.switchTo().frame("process");
  }

  /**
   * Select the PopUp.
   *
   * This function waits until the process button is present and visible.
   *
   */
  public void selectPopUp() {
    selectPopUp(IDENTIFIER_BUTTON_PROCESS);
  }

  /**
   * Select orders.
   *
   * @param numbers
   *          The order numbers that will be selected.
   */
  private void selectOrders(String... numbers) {
    for (String number : numbers) {
      String orderXpath = String.format(FORMAT_XPATH_SELECT_ORDER_BY_NUMBER, number);
      if (!SeleniumSingleton.INSTANCE.findElementByXPath(orderXpath).isSelected()) {
        SeleniumSingleton.INSTANCE.findElementByXPath(orderXpath).click();
      }
    }
  }

  /**
   * Process orders.
   *
   * @param orderNumbers
   *          The order numbers that will be selected.
   */
  public void process(String... orderNumbers) {
    selectPopUp();
    selectOrders(orderNumbers);
    SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_BUTTON_PROCESS).click();
  }
}
