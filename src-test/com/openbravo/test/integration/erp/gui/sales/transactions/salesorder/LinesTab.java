/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.sales.transactions.salesorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.ManageReservationData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on OpenbravoERP Lines tab.
 *
 * @author elopio
 *
 */
public class LinesTab extends GeneratedTab<SalesOrderLinesData> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The tab title. */
  static final String TITLE = "Lines";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "187";
  /** The tab level. */
  private static final int LEVEL = 1;

  /* Registry keys. */
  /** Registry key of the reservation button. */
  private static final String REGISTRY_KEY_BUTTON_RESERVATION = "org.openbravo.client.application.toolbar.button.manageReservation.187";
  /** Registry key of the Pick and Edit Lines Pop Up */
  private static final String REGISTRY_KEY_POPUP_PICKANDEDIT = "org.openbravo.client.application.process.pickandexecute.popup";

  /* Toolbar buttons. */
  /** The reservation button. */
  private static Button buttonReservation;

  /**
   * Class constructor.
   *
   * @param parentTab
   *          The parent of the tab.
   */
  public LinesTab(GeneratedTab<?> parentTab) {
    super(TITLE, IDENTIFIER, LEVEL, parentTab);
  }

  /**
   * Manage stock reservation.
   */
  public void manageReservation() {
    manageReservation(null, null, false);
  }

  /**
   * Manage stock reservation.
   */
  public void manageReservation(ManageReservationData data) {
    manageReservation(null, data, false);
  }

  /**
   * Manage stock reservation.
   */
  public void manageReservation(ManageReservationData filterData, ManageReservationData data) {
    manageReservation(filterData, data, false);
  }

  /**
   * Manage stock reservation.
   */
  public void manageReservationWithError(ManageReservationData data) {
    manageReservation(null, data, true);
  }

  /**
   * Manage stock reservation.
   */
  public void manageReservation(ManageReservationData filterData, ManageReservationData data,
      boolean error) {
    logger.debug("Manage Reservation");
    if (buttonReservation == null) {
      buttonReservation = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_RESERVATION));
    }
    buttonReservation.click();
    Sleep.trySleep();

    PickAndExecuteWindow<ManageReservationData> popUp = new PickAndExecuteWindow<ManageReservationData>(
        TestRegistry.getObjectString(REGISTRY_KEY_POPUP_PICKANDEDIT));
    if (filterData != null) {
      popUp.filter(filterData);
    }
    if (filterData != null && data != null) {
      popUp.focus();
    }
    if (data != null) {
      popUp.edit(data);
    }
    popUp.process();
    // TODO L2: Change the following static sleep for a smart Sleep
    Sleep.trySleep();
    if (error) {
      SeleniumSingleton.INSTANCE.executeScript("isc.Dialog.Warn.okClick()");
      popUp.cancel();
    }
  }

}
