/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *   Nono Carballo <f.carballo@nectus.com>
 *************************************************************************
 */
package com.openbravo.test.integration.erp.testscripts.applicationdictionary.processdefinition;

import com.openbravo.test.integration.erp.data.applicationdictionary.processdefinition.ParameterData;
import com.openbravo.test.integration.erp.data.applicationdictionary.processdefinition.ProcessDefinitionHeaderData;
import com.openbravo.test.integration.erp.gui.applicationdictionary.processdefinition.ParameterTab;
import com.openbravo.test.integration.erp.gui.applicationdictionary.processdefinition.ProcessDefinitionHeaderTab;
import com.openbravo.test.integration.erp.gui.applicationdictionary.processdefinition.ProcessDefinitionWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;

/**
 *
 * Class for Process Definition Controller
 *
 * @author Nono Carballo
 *
 */
public class ProcessDefinition
    extends SmokeTabScript<ProcessDefinitionHeaderData, ProcessDefinitionHeaderTab>
    implements SmokeWindowScript<ProcessDefinitionWindow> {

  public ProcessDefinition(MainPage mainPage) {
    super(mainPage);
  }

  @Override
  public SmokeWindowScript<ProcessDefinitionWindow> open() {
    final ProcessDefinitionWindow window = new ProcessDefinitionWindow();
    mainPage.openView(window);
    return this;
  }

  @Override
  protected ProcessDefinitionHeaderTab selectTab() {
    final ProcessDefinitionWindow window = (ProcessDefinitionWindow) mainPage
        .getView(ProcessDefinitionWindow.TITLE);
    mainPage.getTabSetMain().selectTab(ProcessDefinitionWindow.TITLE);
    final ProcessDefinitionHeaderTab tab = (ProcessDefinitionHeaderTab) window.getTopLevelTab();
    tab.waitUntilSelected();
    return tab;
  }

  public class Parameter extends SmokeTabScript<ParameterData, ParameterTab> {

    public Parameter(MainPage mainPage) {
      super(mainPage);
    }

    @Override
    protected ParameterTab selectTab() {
      final ProcessDefinitionWindow window = (ProcessDefinitionWindow) mainPage
          .getView(ProcessDefinitionWindow.TITLE);
      final ParameterTab tab = (ParameterTab) window.selectTab(ParameterTab.IDENTIFIER);
      return tab;
    }

  }

}
