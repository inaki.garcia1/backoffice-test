/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentin;

import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions on OpenbravoERP Execute Payment In pop up.
 *
 * @author aferraz
 *
 */
public class ExecutePaymentInPopUp extends OBClassicPopUp {

  protected static final String IDENTIFIER_EXECUTE_PAYMENT_COMBO_BOX_CHECK_NUMBER = "textFF8080812C2ABFC6012C2B652A4D00DF";

  /**
   * Class constructor.
   *
   */
  public ExecutePaymentInPopUp() {
    super(TestRegistry.getObjectString(
        "org.openbravo.classicpopup./org.openbravo.advpaymentmngt.ad_actionbutton/ExecutePayments.html"));
  }

  /**
   * Select the pop up frame.
   */
  @Override
  protected void selectFrame() {
    SeleniumSingleton.INSTANCE.switchTo().frame("process");
  }

  /**
   * Process an action.
   */
  public void process() {
    selectPopUpWithOKButton();
    clickOK();
  }

  /**
   * Process an action.
   */
  public void process(String number) {
    selectPopUpWithOKButton();
    SeleniumSingleton.INSTANCE.findElementByName(IDENTIFIER_EXECUTE_PAYMENT_COMBO_BOX_CHECK_NUMBER)
        .sendKeys(number);
    clickOK();
  }

}
