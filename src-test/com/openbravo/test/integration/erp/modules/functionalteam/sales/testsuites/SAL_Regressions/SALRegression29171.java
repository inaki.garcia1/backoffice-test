/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.sales.testsuites.SAL_Regressions;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.TimeoutException;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.masterdata.product.PriceData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.masterdata.product.Product;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;

/**
 * Test regression 29171
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class SALRegression29171 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  ProductData productData;
  PriceData productPriceData;
  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderLinesData;
  SalesOrderLinesData salesOrderLinesVerificationData;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;

  /**
   * Class constructor.
   *
   */
  public SALRegression29171(ProductData productData, PriceData productPriceData,
      SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData, SalesOrderLinesData salesOrderLinesData,
      SalesOrderLinesData salesOrderLinesVerificationData,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData) {
    this.productData = productData;
    this.productPriceData = productPriceData;
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesData = salesOrderLinesData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new ProductData.Builder().organization("Spain")
            .uOM("Unit")
            .productCategory("Finished Goods")
            .taxCategory("VAT 10%")
            .purchase(false)
            .sale(true)
            .productType("Item")
            .stocked(true)
            .build(),
        new PriceData.Builder().priceListVersion("Customer A")
            .standardPrice("0.00")
            .listPrice("5.00")
            .build(),

        new SalesOrderHeaderData.Builder().transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .warehouse("Spain warehouse")
            .build(),
        new SalesOrderHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .invoiceAddress(".Pamplona, Street Customer center nº1")
            .warehouse("Spain warehouse")
            .priceList("Customer A")
            .paymentMethod("1 (Spain)")
            .paymentTerms("30 days, 5")
            .invoiceTerms("Customer Schedule After Delivery")
            .build(),
        new SalesOrderLinesData.Builder().orderedQuantity("1").build(),
        new SalesOrderLinesData.Builder().uOM("Unit")
            .unitPrice("0.00")
            .listPrice("5.00")
            .tax("VAT 10%")
            .lineNetAmount("0.00")
            .build(),
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("0.00")
            .grandTotalAmount("0.00")
            .currency("EUR")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 29171
   *
   * @throws ParseException
   */
  @Test
  public void SALRegression29171Test() throws ParseException {
    logger.info("** Start of test case [SALRegression29171] Test regression 29171. **");

    String productName = "Test29171Sales-";
    int num = Product.ProductTab.getRecordCount(mainPage,
        new ProductData.Builder().name(productName).build());
    productName = productName + String.valueOf(num);

    productData.addDataField("searchKey", productName);
    productData.addDataField("name", productName);
    Product.ProductTab.create(mainPage, productData);
    Product.Price.create(mainPage, productPriceData);

    SalesOrder salesOrder = new SalesOrder(mainPage).open();
    salesOrder.create(salesOrderHeaderData);
    salesOrder.assertSaved();
    salesOrder.assertData(salesOrderHeaderVerficationData);
    SalesOrder.Lines salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLinesData.addDataField("product",
        new ProductSelectorData.Builder().searchKey(productName)
            .priceListVersion("Customer A")
            .build());
    try {
      salesOrderLines.create(salesOrderLinesData);
    } catch (TimeoutException tme) {
      // TODO Auto-generated catch block
      logger.fatal(
          "CAUTION: TimeoutException has been thrown in this specific area of this tests. Check if AUOM if set. Maybe the context is not right due to AUOM was not deactivated properly.");
      Assert.fail();
    }
    salesOrderLines.assertSaved();
    salesOrderLines.assertData(salesOrderLinesVerificationData);
    salesOrder.book();
    salesOrder.assertProcessCompletedSuccessfully2();
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);

    logger.info("** End of test case [SALRegression29171] Test regression 29171. **");
  }
}
