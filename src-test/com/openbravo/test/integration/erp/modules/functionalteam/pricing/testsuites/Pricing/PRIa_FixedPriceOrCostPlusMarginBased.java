/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Lujan <plu@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.pricing.testsuites.Pricing;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelist.PriceListData;
import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelist.PriceListVersionData;
import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelist.ProductPriceData;
import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelistschema.PriceListSchemaHeaderData;
import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelistschema.PriceListSchemaLinesData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.masterdata.pricing.pricelist.PriceList;
import com.openbravo.test.integration.erp.testscripts.masterdata.pricing.pricelistschema.PriceListSchema;

/**
 * Execute the Setup User and Role flow of the smoke test suite.
 *
 * @author Alberto Santos
 */
@RunWith(Parameterized.class)
public class PRIa_FixedPriceOrCostPlusMarginBased extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  private PriceListSchemaHeaderData priceListSchemaHeaderStandardData;
  private PriceListSchemaLinesData priceListSchemaLinesLaptopData;
  private PriceListSchemaLinesData priceListSchemaLinesSoccerBallData;

  private PriceListData priceListData;
  private PriceListVersionData priceListVersionTestData;
  private PriceListVersionData priceListVersionTestDateData;
  private ProductPriceData productPriceLaptopData;
  private ProductPriceData productPriceSoccerBallData;
  private ProductPriceData productPriceLaptopDataSelect;
  private ProductPriceData productPriceSoccerBallDataSelect;

  /**
   * Class constructor.
   */
  public PRIa_FixedPriceOrCostPlusMarginBased(
      PriceListSchemaHeaderData priceListSchemaHeaderStandardData,
      PriceListSchemaLinesData priceListSchemaLinesLaptopData,
      PriceListSchemaLinesData priceListSchemaLinesSoccerBallData, PriceListData priceListData,
      PriceListVersionData priceListVersionTestData,
      PriceListVersionData priceListVersionTestDateData, ProductPriceData productPriceLaptopData,
      ProductPriceData productPriceSoccerBallData, ProductPriceData productPriceLaptopDataSelect,
      ProductPriceData productPriceSoccerBallDataSelect) {

    this.priceListSchemaHeaderStandardData = priceListSchemaHeaderStandardData;
    this.priceListSchemaLinesLaptopData = priceListSchemaLinesLaptopData;
    this.priceListSchemaLinesSoccerBallData = priceListSchemaLinesSoccerBallData;
    this.priceListData = priceListData;
    this.priceListVersionTestData = priceListVersionTestData;
    this.priceListVersionTestDateData = priceListVersionTestDateData;
    this.productPriceLaptopData = productPriceLaptopData;
    this.productPriceSoccerBallData = productPriceSoccerBallData;
    this.productPriceLaptopDataSelect = productPriceLaptopDataSelect;
    this.productPriceSoccerBallDataSelect = productPriceSoccerBallDataSelect;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   * @throws IOException
   *
   */
  @Parameters
  public static Collection<Object[]> setupProductValues() throws IOException {
    Calendar date = Calendar.getInstance();
    int day = date.get(Calendar.DAY_OF_MONTH);
    int month = date.get(Calendar.MONTH) + 1;
    int year = date.get(Calendar.YEAR);
    String currentDate = day + "-" + month + "-" + year;
    return Arrays.asList(new Object[][] { {

        // Create Pricelist Schema Standard
        new PriceListSchemaHeaderData.Builder().name("Standard").build(),
        new PriceListSchemaLinesData.Builder().productCategory("Distribution Goods")
            .product(new ProductSelectorData.Builder().name("Laptop").build())
            .baseListPrice("Fixed Price or Cost plus Margin Based")
            .fixedListPrice("15.00")
            .listPriceMargin("15")
            .standardBasePrice("Fixed Price or Cost plus Margin Based")
            .fixedStandardPrice("0.00")
            .unitPriceMargin("0")
            .build(),
        new PriceListSchemaLinesData.Builder().productCategory("Distribution Goods")
            .product(new ProductSelectorData.Builder().name("Soccer Ball").build())
            .baseListPrice("Fixed Price or Cost plus Margin Based")
            .fixedListPrice("10.00")
            .listPriceMargin("10")
            .standardBasePrice("Fixed Price or Cost plus Margin Based")
            .fixedStandardPrice("0.00")
            .unitPriceMargin("0")
            .build(),

        // Create Pricelist
        new PriceListData.Builder().name("test")
            .salesPriceList(true)
            .currency("EUR")
            .costBasedPriceList(true)
            .build(),
        new PriceListVersionData.Builder().name("test").priceListSchema("Standard").build(),
        new PriceListVersionData.Builder().name("test " + currentDate)
            .priceListSchema("Standard")
            .basePriceListVersion("test")
            .build(),
        new ProductPriceData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("Laptop").build())
            .standardPrice("10.00")
            .listPrice("15.00")
            .build(),
        new ProductPriceData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("Soccer Ball").build())
            .standardPrice("10.00")
            .listPrice("11.00")
            .build(),
        new ProductPriceData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("Laptop").build())
            .build(),
        new ProductPriceData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("Soccer Ball").build())
            .build() } });
  }

  /**
   * Test the setup client and organization flow.
   *
   * @throws IOException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void FixedOrCostPlusMarginPrice() throws IOException, OpenbravoERPTestException {
    logger.info("** Start of test case [PRIa]Create pricelist schema **");
    PriceListSchema.Header.select(mainPage, priceListSchemaHeaderStandardData);
    PriceListSchema.Lines.create(mainPage, priceListSchemaLinesLaptopData);
    PriceListSchema.Lines.create(mainPage, priceListSchemaLinesSoccerBallData);
    logger.info("** End of test case [PRIa]Create pricelist schema **");

    logger.info("** Start of test case [PRIa] Create Pricelist **");
    PriceList.PriceListTab.create(mainPage, priceListData);
    PriceList.PriceListVersionTab.create(mainPage, priceListVersionTestData);
    PriceList.PriceListVersionTab.processCreatePricelist(mainPage);
    PriceList.PriceListVersionTab.create(mainPage, priceListVersionTestDateData);
    PriceList.PriceListVersionTab.processCreatePricelist(mainPage);
    PriceList.ProductPriceTab.select(mainPage, productPriceLaptopDataSelect);
    PriceList.ProductPriceTab.verify(mainPage, productPriceLaptopData);
    PriceList.ProductPriceTab.select(mainPage, productPriceSoccerBallDataSelect);
    PriceList.ProductPriceTab.verify(mainPage, productPriceSoccerBallData);
    logger.info("** End of test case [PRIa] Create Pricelist **");
  }
}
