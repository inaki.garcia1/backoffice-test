/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.application.sessionpreferences;

import com.openbravo.test.integration.erp.gui.general.application.sessionpreferences.SessionPreferencesWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on the Session Preferences window.
 *
 * @author elopio
 */
public class SessionPreferences {

  /** The application main page. */
  protected final MainPage mainPage;

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public SessionPreferences(MainPage mainPage) {
    this.mainPage = mainPage;
  }

  /**
   * Open the Session Preferences window.
   *
   * @return this test scrip.
   */
  public SessionPreferences open() {
    SessionPreferencesWindow window = new SessionPreferencesWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the Session Preferences tab.
   *
   * @return the Session Preferences tab GUI object.
   */
  protected SessionPreferencesWindow selectTab() {
    SessionPreferencesWindow window = (SessionPreferencesWindow) mainPage
        .getView(SessionPreferencesWindow.TITLE);
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    mainPage.getTabSetMain().selectTab(SessionPreferencesWindow.TITLE);
    window.waitForFrame();
    return window;
  }

  /**
   * Check the show accounting tabs preference
   */
  public void checkShowAccountingTabs() {
    selectTab().checkShowAccountingTabs();
  }

}
