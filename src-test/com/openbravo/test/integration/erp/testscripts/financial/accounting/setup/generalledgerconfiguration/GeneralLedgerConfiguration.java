/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.financial.accounting.setup.generalledgerconfiguration;

import com.openbravo.test.integration.erp.data.financial.accounting.setup.accountingschema.AccountingSchemaData;
import com.openbravo.test.integration.erp.data.financial.accounting.setup.accountingschema.GeneralLedgerData;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the window.
 *
 * @author aferraz
 */
public class GeneralLedgerConfiguration {

  /**
   * Executes and verifies actions on the tab.
   *
   * @author aferraz
   *
   */
  public static class GeneralLedgerConfigurationTab {

    /**
     * Creates the record.
     *
     * @param mainPage
     *          The application main page.
     * @param data
     *
     */
    public static void create(MainPage mainPage, AccountingSchemaData data) {
      if (mainPage.isOnNewLayout()) {
        GeneralLedgerConfigurationTabScript.create(mainPage, data);
      } else {
        GeneralLedgerConfigurationTabClassicScript.create(mainPage, data);
      }
    }

    /**
     * Select the record.
     *
     * @param mainPage
     *          The application main page.
     * @param data
     *
     */
    public static void select(MainPage mainPage, AccountingSchemaData data) {
      if (mainPage.isOnNewLayout()) {
        GeneralLedgerConfigurationTabScript.select(mainPage, data);
      } else {
        GeneralLedgerConfigurationTabClassicScript.select(mainPage, data);
      }
    }

    /**
     * Executes and verifies actions on the General Accounts tab of OpenbravoERP.
     *
     * @author aferraz
     *
     */
    public static class GeneralAccountsTab {

      /**
       * Create a general account record.
       *
       * @param mainPage
       *          The application main page.
       * @param data
       *          The data of the general ledger that will be created.
       */
      public static void create(MainPage mainPage, GeneralLedgerData data) {
        if (mainPage.isOnNewLayout()) {
          GeneralAccountsTabScript.create(mainPage, data);
        } else {
          GeneralAccountsTabClassicScript.create(mainPage, data);
        }
      }

      /**
       * Edit a general account record.
       *
       * @param mainPage
       *          The application main page.
       * @param data
       *          The data of the general ledger that will be edited.
       */
      public static void edit(MainPage mainPage, GeneralLedgerData data) {
        if (mainPage.isOnNewLayout()) {
          GeneralAccountsTabScript.edit(mainPage, data);
        } else {
          GeneralAccountsTabClassicScript.edit(mainPage, data);
        }
      }
    }

  }

}
