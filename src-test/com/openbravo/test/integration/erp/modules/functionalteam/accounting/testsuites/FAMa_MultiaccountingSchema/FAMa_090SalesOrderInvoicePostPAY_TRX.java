/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  David Miguélez <david.miguelez@openbravo.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.accounting.testsuites.FAMa_MultiaccountingSchema;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;

/**
 * This test case post a payment and transaction. You create an order and you collect it partially.
 * Then you invoice it. So you have two payment, one for the order and another one for the invoice.
 *
 * @author David Miguélez
 */

@RunWith(Parameterized.class)
public class FAMa_090SalesOrderInvoicePostPAY_TRX extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /* Data for [FAMa_090] Post a Payment and a Transaction for a Sales Order and a Sales Invoice. */
  /** The sales order header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  /**
   * Expected journal entry lines. Is an array of arrays. Each line has account number, name, debit
   * and credit.
   */
  private String[][] journalEntryLines;
  /** The first payment in header data. */
  PaymentInHeaderData paymentInOneHeaderData;
  /** Expected journal entry lines for the first payment in. */
  private String[][] paymentInOneJournalEntryLines;
  /** The second payment in header data. */
  PaymentInHeaderData paymentInTwoHeaderData;
  /** Expected journal entry lines for the second payment in. */
  private String[][] paymentInTwoJournalEntryLines;
  /** The account header data. */
  AccountData accountHeaderData;
  /** The first transaction lines data. */
  TransactionsData transactionsOneHeaderData;
  /** Expected journal entry lines for the first transaction. */
  private String[][] transactionOneJournalEntryLines;
  /** The second transaction lines data. */
  TransactionsData transactionstwoHeaderData;
  /** Expected journal entry lines for the second transaction. */
  private String[][] transactionTwoJournalEntryLines;

  private String[][] journalEntryLines2;

  private String[][] paymentInOneJournalEntryLines2;

  private String[][] paymentInTwoJournalEntryLines2;

  private String[][] transactionOneJournalEntryLines2;

  private String[][] transactionTwoJournalEntryLines2;

  /**
   * Class constructor.
   *
   * @param salesInvoiceHeaderData
   *          The sales invoice header data.
   * @param journalEntryLines
   *          Expected journal entry lines. Is an array of arrays. Each line has account number,
   *          name, debit and credit.
   * @param journalEntryLines2
   *          Expected journal entry lines. Is an array of arrays. Each line has account number,
   *          name, debit and credit.
   * @param paymentInOneHeaderData
   *          The first payment in header data.
   * @param paymentInOneJournalEntryLines
   *          Expected journal entry lines for the first payment in.
   * @param paymentInOneJournalEntryLines2
   *          Expected journal entry lines for the first payment in.
   * @param paymentInTwoHeaderData
   *          The second payment in header data.
   * @param paymentInTwoJournalEntryLines
   *          Expected journal entry lines for the second payment in.
   * @param paymentInTwoJournalEntryLines2
   *          Expected journal entry lines for the second payment in.
   * @param accountHeaderData
   *          The account Header Data.
   * @param transactionsOneHeaderData
   *          The first transaction lines data.
   * @param transactionOneJournalEntryLines
   *          Expected journal entry lines for the first transaction.
   * @param transactionstwoHeaderData
   *          The second transaction lines data.
   * @param transactionTwoJournalEntryLines
   *          Expected journal entry lines for the second transaction.
   * @param transactionTwoJournalEntryLines2
   *          Expected journal entry lines for the second transaction.
   */

  public FAMa_090SalesOrderInvoicePostPAY_TRX(SalesInvoiceHeaderData salesInvoiceHeaderData,
      String[][] journalEntryLines, String[][] journalEntryLines2,
      PaymentInHeaderData paymentInOneHeaderData, String[][] paymentInOneJournalEntryLines,
      String[][] paymentInOneJournalEntryLines2, PaymentInHeaderData paymentInTwoHeaderData,
      String[][] paymentInTwoJournalEntryLines, String[][] paymentInTwoJournalEntryLines2,
      AccountData accountHeaderData, TransactionsData transactionsOneHeaderData,
      String[][] transactionOneJournalEntryLines, String[][] transactionOneJournalEntryLines2,
      TransactionsData transactionstwoHeaderData, String[][] transactionTwoJournalEntryLines,
      String[][] transactionTwoJournalEntryLines2) {

    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.journalEntryLines = journalEntryLines;
    this.journalEntryLines2 = journalEntryLines2;
    this.paymentInOneHeaderData = paymentInOneHeaderData;
    this.paymentInOneJournalEntryLines = paymentInOneJournalEntryLines;
    this.paymentInOneJournalEntryLines2 = paymentInOneJournalEntryLines2;
    this.paymentInTwoHeaderData = paymentInTwoHeaderData;
    this.paymentInTwoJournalEntryLines = paymentInTwoJournalEntryLines;
    this.paymentInTwoJournalEntryLines2 = paymentInTwoJournalEntryLines2;
    this.accountHeaderData = accountHeaderData;
    this.transactionsOneHeaderData = transactionsOneHeaderData;
    this.transactionOneJournalEntryLines = transactionOneJournalEntryLines;
    this.transactionOneJournalEntryLines2 = transactionOneJournalEntryLines2;
    this.transactionstwoHeaderData = transactionstwoHeaderData;
    this.transactionTwoJournalEntryLines = transactionTwoJournalEntryLines;
    this.transactionTwoJournalEntryLines2 = transactionTwoJournalEntryLines2;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<Object[]> orderToCashValues() {

    return Arrays.asList(new Object[][] { {
        // Parameters for [FAMa_090] Post a Payment and a Transaction for a Sales Order and a
        // Sales Invoice.
        new SalesInvoiceHeaderData.Builder().documentNo("I/22").build(),
        new String[][] { { "43000", "Clientes (euros)", "15.00", "" },
            { "43800", "438. Anticipos de clientes", "15.00", "" },
            { "70000", "Ventas de mercaderías", "", "30.00" }, },
        new String[][] { { "11400", "Accounts receivable", "60.00", "" },
            { "55400", "Bank revaluation loss", "30.00", "" },
            { "2450", "Customer Prepayments", "30.00", "" }, { "41100", "Sales", "", "120.00" } },
        new PaymentInHeaderData.Builder().documentNo("400023").build(),
        new String[][] { { "43100", "Efectos comerciales en cartera", "15.00", "" },
            { "43000", "Clientes (euros)", "", "15.00" } },
        new String[][] { { "11300", "Bank in transit", "60.00", "" },
            { "11400", "Accounts receivable", "", "60.00" }, },
        new PaymentInHeaderData.Builder().documentNo("400022").build(),
        new String[][] { { "43100", "Efectos comerciales en cartera", "15.00", "" },
            { "43800", "438. Anticipos de clientes", "", "15.00" } },
        new String[][] { { "11300", "Bank in transit", "30.00", "" },
            { "2450", "Customer Prepayments", "", "30.00" } },
        new AccountData.Builder().name("Accounting Documents EURO").build(),
        new TransactionsData.Builder().documentNo("400023").build(),
        new String[][] {
            { "57200", "Bancos e instituciones de crédito c/c vista euros", "15.00", "" },
            { "43100", "Efectos comerciales en cartera", "", "15.00" } },
        new String[][] { { "11100", "Petty Cash", "45.00", "" },
            { "55400", "Bank revaluation loss", "15.00", "" },
            { "11300", "Bank in transit", "", "60.00" } },
        new TransactionsData.Builder().documentNo("400022").build(),
        new String[][] {
            { "57200", "Bancos e instituciones de crédito c/c vista euros", "15.00", "" },
            { "43100", "Efectos comerciales en cartera", "", "15.00" } },
        new String[][] { { "11100", "Petty Cash", "45.00", "" },
            { "11300", "Bank in transit", "", "30.00" },
            { "45200", "Bank revaluation gain", "", "15.00" } } } });
  }

  /**
   * This test case post a payment and transaction. You create an order and you collect it
   * partially. Then you invoice it. So you have two payment, one for the order and another one for
   * the invoice.
   */
  @Test
  public void FAMa_090OrderAndInvoicePostPaymentAndTransaction() {
    logger.info(
        "** Start of test case [FAMa_090] Post a Payment and a Transaction for a Sales Order and a Sales Invoice. **");

    final SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.select(salesInvoiceHeaderData);
    if (salesInvoice.isPosted()) {
      salesInvoice.unpost();
      salesInvoice.assertProcessCompletedSuccessfully2(
          "Number of unposted documents = 1, Number of deleted journal entries = 7");
    }
    salesInvoice.post();
    mainPage.closeView("Sales Invoice");

    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines.length);
    post.assertJournalLines2(journalEntryLines);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");

    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines2.length);
    post.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    final PaymentIn paymentInOne = new PaymentIn(mainPage).open();
    paymentInOne.select(paymentInOneHeaderData);
    if (paymentInOne.isPosted()) {
      paymentInOne.unpost();
      paymentInOne.assertProcessCompletedSuccessfully2(
          "Number of unposted documents = 1, Number of deleted journal entries = 4");
    }
    paymentInOne.post();

    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(paymentInOneJournalEntryLines.length);
    post.assertJournalLines2(paymentInOneJournalEntryLines);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");

    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(paymentInOneJournalEntryLines2.length);
    post.assertJournalLines2(paymentInOneJournalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    final PaymentIn paymentInTwo = new PaymentIn(mainPage).open();
    paymentInTwo.select(paymentInTwoHeaderData);
    if (paymentInTwo.isPosted()) {
      paymentInTwo.unpost();
      // paymentInTwo
      // .assertProcessCompletedSuccessfully2("Number of unposted documents = 1, Number of deleted
      // journal entries = 2");
    }
    paymentInTwo.post();

    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(paymentInTwoJournalEntryLines.length);
    post.assertJournalLines2(paymentInTwoJournalEntryLines);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");

    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(paymentInTwoJournalEntryLines2.length);
    post.assertJournalLines2(paymentInTwoJournalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    final FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.Transaction transactionOne = financialAccount.new Transaction(mainPage);
    transactionOne.select(transactionsOneHeaderData);
    if (transactionOne.isPosted()) {
      transactionOne.unpost();
      // transactionOne
      // .assertProcessCompletedSuccessfully2("Number of unposted documents = 1, Number of deleted
      // journal entries = 2");
    }
    transactionOne.post();

    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(transactionOneJournalEntryLines.length);
    post.assertJournalLines2(transactionOneJournalEntryLines);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");

    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(transactionOneJournalEntryLines2.length);
    post.assertJournalLines2(transactionOneJournalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    FinancialAccount.Transaction transactionTwo = financialAccount.new Transaction(mainPage);
    transactionTwo.select(transactionstwoHeaderData);
    if (transactionTwo.isPosted()) {
      transactionTwo.unpost();
      // transactionTwo
      // .assertProcessCompletedSuccessfully2("Number of unposted documents = 1, Number of deleted
      // journal entries = 2");
    }
    transactionTwo.post();

    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(transactionTwoJournalEntryLines.length);
    post.assertJournalLines2(transactionTwoJournalEntryLines);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");

    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(transactionTwoJournalEntryLines2.length);
    post.assertJournalLines2(transactionTwoJournalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    logger.info(
        "** End of test case [FAMa_090] Post a Payment and a Transaction for a Sales Order and a Sales Invoice. **");
  }

}
