/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.client.client;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for EmailConfigurationData
 *
 * @author plujan
 *
 */
public class EmailConfigurationData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the smtpServerSenderAddress value
     *
     * Description: Address used to send the emails
     *
     * @param value
     *          The smtpServerSenderAddress value.
     * @return The builder for this class.
     */
    public Builder smtpServerSenderAddress(String value) {
      this.dataFields.put("smtpServerSenderAddress", value);
      return this;
    }

    /**
     * Set the smtpServerPassword value
     *
     * Description: Smtp Server Password
     *
     * @param value
     *          The smtpServerPassword value.
     * @return The builder for this class.
     */
    public Builder smtpServerPassword(String value) {
      this.dataFields.put("smtpServerPassword", value);
      return this;
    }

    /**
     * Set the smtpServerAccount value
     *
     * Description: Smtp Server Account
     *
     * @param value
     *          The smtpServerAccount value.
     * @return The builder for this class.
     */
    public Builder smtpServerAccount(String value) {
      this.dataFields.put("smtpServerAccount", value);
      return this;
    }

    /**
     * Set the smtpServer value
     *
     * Description: Smtp Server
     *
     * @param value
     *          The smtpServer value.
     * @return The builder for this class.
     */
    public Builder smtpServer(String value) {
      this.dataFields.put("smtpServer", value);
      return this;
    }

    /**
     * Set the sMTPAuthentification value
     *
     * Description: Your mail server requires Authentification
     *
     * @param value
     *          The sMTPAuthentification value.
     * @return The builder for this class.
     */
    public Builder sMTPAuthentification(Boolean value) {
      this.dataFields.put("sMTPAuthentification", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public EmailConfigurationData build() {
      return new EmailConfigurationData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private EmailConfigurationData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
