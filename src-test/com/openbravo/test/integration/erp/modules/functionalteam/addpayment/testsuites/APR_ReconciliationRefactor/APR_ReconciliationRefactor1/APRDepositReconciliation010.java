/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_ReconciliationRefactor.APR_ReconciliationRefactor1;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddTransactionData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.MatchStatementData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.bankstatement.BankStatementHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.BankStatementLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddTransactionProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementGrid;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Match Automatically a Transaction and a Bank Statement Line
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRDepositReconciliation010 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /** The Purchase Invoice header data. */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  /** The data to verify the creation of the Purchase Invoice header data. */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  /** The data to verify the creation of the Purchase Invoice lines data. */
  PurchaseInvoiceLinesData purchaseInvoiceLineData;
  /** The data to verify the creation of the Purchase Invoice line. */
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData;
  /** The data to verify the completion of the purchase invoice. */
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;

  /** The account header data. */
  AccountData accountHeaderData;
  /** The bank statement data. */
  BankStatementHeaderData bankStatementHeaderData;
  BankStatementLinesData bankStatementLinesData;
  MatchStatementData matchStatementData;
  MatchStatementData matchStatementData2;
  MatchStatementData matchStatementData3;
  AddPaymentPopUpData addPaymentTotalsVerificationData;
  AddTransactionData addTransactionVerificationData;
  MatchStatementData matchStatementData4;
  MatchStatementData matchStatementData5;

  /**
   * Class constructor.
   *
   * @param accountHeaderData
   *          The account Header Data.
   * @param bankStatementHeaderData
   *          The bank statement Header Data.
   * @param bankStatementLinesData
   *          The bank statement lines Data.
   */
  public APRDepositReconciliation010(PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineData,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      AccountData accountHeaderData, BankStatementHeaderData bankStatementHeaderData,
      BankStatementLinesData bankStatementLinesData, MatchStatementData matchStatementData,
      MatchStatementData matchStatementData2, MatchStatementData matchStatementData3,
      AddPaymentPopUpData addPaymentTotalsVerificationData,
      AddTransactionData addTransactionVerificationData, MatchStatementData matchStatementData4,
      MatchStatementData matchStatementData5) {
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineData = purchaseInvoiceLineData;
    this.purchaseInvoiceLineVerificationData = purchaseInvoiceLineVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.accountHeaderData = accountHeaderData;
    this.bankStatementHeaderData = bankStatementHeaderData;
    this.bankStatementLinesData = bankStatementLinesData;
    this.matchStatementData = matchStatementData;
    this.matchStatementData2 = matchStatementData2;
    this.matchStatementData3 = matchStatementData3;
    this.addPaymentTotalsVerificationData = addPaymentTotalsVerificationData;
    this.addTransactionVerificationData = addTransactionVerificationData;
    this.matchStatementData4 = matchStatementData4;
    this.matchStatementData5 = matchStatementData5;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> APRDepositReconciliation010Values() {
    Object[][] data = new Object[][] { {
        /* Parameters for [APRDepositReconciliation010Test] */
        new PurchaseInvoiceHeaderData.Builder().transactionDocument("AP Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),
        new PurchaseInvoiceHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .priceList("Purchase")
            .paymentMethod("1 (Spain)")
            .paymentTerms("90 days")
            .build(),
        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("RMA")
                .priceListVersionName("Purchase")
                .build())
            .invoicedQuantity("25")
            .tax("VAT 10%")
            .build(),
        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Raw material A").build())
            .invoicedQuantity("25")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("50.00")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().totalPaid("0.00")
            .outstandingAmount("55.00")
            .documentStatus("Completed")
            .summedLineAmount("50.00")
            .grandTotalAmount("55.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),
        new AccountData.Builder().name("Spain Bank").build(),
        new BankStatementHeaderData.Builder().name("TestReconciliationD010").build(),
        new BankStatementLinesData.Builder().referenceNo("D010")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .dramount("100.00")
            .build(),
        new MatchStatementData.Builder().bankStatementType("P")
            .sender("Vendor A")
            .referenceNo("D010")
            .glitem("")
            .amount("-100")
            .affinity("")
            .matchedDocument("")
            .matchingType("")
            .businessPartner("")
            .transactionAmount("")
            .transactionGLItem("")
            .build(),
        new MatchStatementData.Builder().bankStatementType("P")
            .sender("Vendor A")
            .referenceNo("D010")
            .glitem("")
            .amount("-155")
            .affinity("AD")
            .matchedDocument("T")
            .matchingType("AD")
            .businessPartner("Vendor A")
            .transactionAmount("-155")
            .transactionGLItem("Salaries")
            .build(),
        new MatchStatementData.Builder().bankStatementType("D")
            .sender("Vendor A")
            .referenceNo("D010")
            .glitem("")
            .amount("55")
            .affinity("")
            .matchedDocument("")
            .matchingType("")
            .businessPartner("")
            .transactionAmount("")
            .transactionGLItem("")
            .build(),
        new AddPaymentPopUpData.Builder().trxtype("Paid Out")
            .received_from("Vendor A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("EUR")
            .actual_payment("55.00")
            .expected_payment("55.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("55.00")
            .total("55.00")
            .difference("0.00")
            .build(),
        new AddTransactionData.Builder().trxtype("BP Deposit")
            .currency("EUR")
            .depositamt("0.00")
            .withdrawalamt("55.00")
            .organization("Spain")
            .build(),
        new MatchStatementData.Builder().bankStatementType("P")
            .sender("Vendor A")
            .referenceNo("D010")
            .glitem("")
            .amount("-55")
            .affinity("AD")
            .matchedDocument("T")
            .matchingType("AD")
            .businessPartner("Vendor A")
            .transactionAmount("-55")
            .transactionGLItem("")
            .build(),
        new MatchStatementData.Builder().bankStatementType("D")
            .sender("Vendor A")
            .referenceNo("D010")
            .glitem("")
            .amount("110")
            .affinity("")
            .matchedDocument("")
            .matchingType("")
            .businessPartner("")
            .transactionAmount("")
            .transactionGLItem("")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Match Automatically a Transaction and a Bank Statement Line
   *
   * @throws ParseException
   */
  @Test
  public void APRDepositReconciliation010Test() throws ParseException {
    logger.info("** Start of test case [APRDepositReconciliation010Test]");

    // Create a Purchase Invoice
    PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    String invoiceNo = purchaseInvoice.getData("documentNo").toString();
    PurchaseInvoice.Lines purchaseInvoiceLines = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLines.create(purchaseInvoiceLineData);
    purchaseInvoiceLines.assertSaved();
    purchaseInvoiceLines.assertData(purchaseInvoiceLineVerificationData);
    Sleep.trySleep();
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData);

    // Select Financial Account
    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);

    // Get the date of the latest Bank Statement
    FinancialAccount.BankStatements bankStatement = financialAccount.new BankStatements(mainPage);
    String date;
    if (bankStatement.getRecordCount() == 0) {
      date = OBDate.CURRENT_DATE;
    } else {
      bankStatement.selectWithoutFiltering(0);
      date = OBDate.addDaysToDate((String) bankStatement.getData("transactionDate"), 1);
    }

    // Create Bank Statement
    bankStatementHeaderData.addDataField("transactionDate", date);
    bankStatementHeaderData.addDataField("importdate", date);
    bankStatement.create(bankStatementHeaderData);
    bankStatement.assertSaved();
    FinancialAccount.BankStatements.BankStatementLines bankStatementLines = bankStatement.new BankStatementLines(
        mainPage);
    bankStatementLinesData.addDataField("transactionDate", date);
    bankStatementLines.create(bankStatementLinesData);
    bankStatementLines.assertSaved();
    bankStatement.process();
    bankStatement.assertProcessCompletedSuccessfully2();

    // Open Match Statement Grid
    @SuppressWarnings("rawtypes")
    MatchStatementProcess matchStatementProcess = financialAccount.openMatchStatement(false);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementData.addDataField("banklineDate", date);
    matchStatementData.addDataField("trxDate", "");
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData);

    // Add new Transaction
    AddTransactionProcess addTransactionProcess1 = ((MatchStatementGrid) matchStatementProcess
        .getMatchStatementGrid()).clickAdd(0);
    addTransactionProcess1.setParameterValue("c_glitem_id", "Salaries");
    addTransactionProcess1.setParameterValue("withdrawalamt", "155.00");
    addTransactionProcess1.process(true);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("amount", "-155");
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid().filter("businessPartner", "Vendor A");
    matchStatementData2.addDataField("banklineDate", date);
    matchStatementData2.addDataField("trxDate", date);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData2);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("amount", "55");
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid().filter("businessPartner", "Vendor A");
    matchStatementData3.addDataField("banklineDate", date);
    matchStatementData3.addDataField("trxDate", "");
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData3);

    // Add new Transaction
    AddTransactionProcess addTransactionProcess2 = ((MatchStatementGrid) matchStatementProcess
        .getMatchStatementGrid()).clickAdd(0);
    AddPaymentProcess addPaymentProcess = addTransactionProcess2.openAddPayment();
    addPaymentProcess.setParameterValue("trxtype", "Paid Out");
    addPaymentProcess.setParameterValue("received_from",
        new BusinessPartnerSelectorData.Builder().name("Vendor A").build());
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.process("Invoices", invoiceNo, "Process Made Payment(s)",
        addPaymentTotalsVerificationData);
    String description = "Invoice No.: " + invoiceNo + "\n";
    addTransactionVerificationData.addDataField("description", description);
    addTransactionProcess2.assertData(addTransactionVerificationData);
    addTransactionProcess2.process(true);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("amount", "-55");
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid().filter("businessPartner", "Vendor A");
    matchStatementData4.addDataField("banklineDate", date);
    matchStatementData4.addDataField("trxDate", date);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData4);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("amount", "110");
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid().filter("businessPartner", "Vendor A");
    matchStatementData5.addDataField("banklineDate", date);
    matchStatementData5.addDataField("trxDate", "");
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData5);

    // Process Match Statement
    matchStatementProcess.process();
    financialAccount.assertProcessCompletedSuccessfully2();

    logger.info("** End of test case [APRDepositReconciliation010Test]");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
