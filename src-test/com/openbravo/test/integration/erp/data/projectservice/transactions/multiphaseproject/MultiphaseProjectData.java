/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.projectservice.transactions.multiphaseproject;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;

/**
 *
 * Class for MultiphaseProjectData
 *
 * @author plujan
 *
 */
public class MultiphaseProjectData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the summaryLevel value
     *
     * Description: A means of grouping fields in order to view or hide additional information.
     *
     * @param value
     *          The summaryLevel value.
     * @return The builder for this class.
     */
    public Builder summaryLevel(Boolean value) {
      this.dataFields.put("summaryLevel", value);
      return this;
    }

    /**
     * Set the searchKey value
     *
     * Description: A fast method for finding a particular record.
     *
     * @param value
     *          The searchKey value.
     * @return The builder for this class.
     */
    public Builder searchKey(String value) {
      this.dataFields.put("searchKey", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the legallyBindingContract value
     *
     * Description: Is this document a (legal) commitment?
     *
     * @param value
     *          The legallyBindingContract value.
     * @return The builder for this class.
     */
    public Builder legallyBindingContract(Boolean value) {
      this.dataFields.put("legallyBindingContract", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the contractAmount value
     *
     * Description: The maximum legal monetary price a project may be billed for.
     *
     * @param value
     *          The contractAmount value.
     * @return The builder for this class.
     */
    public Builder contractAmount(String value) {
      this.dataFields.put("contractAmount", value);
      return this;
    }

    /**
     * Set the salesRepresentative value
     *
     * Description: The person in charge of a document.
     *
     * @param value
     *          The salesRepresentative value.
     * @return The builder for this class.
     */
    public Builder salesRepresentative(String value) {
      this.dataFields.put("salesRepresentative", value);
      return this;
    }

    /**
     * Set the contractDate value
     *
     * Description: The planned end data is when a contract is registered into the application.
     *
     * @param value
     *          The contractDate value.
     * @return The builder for this class.
     */
    public Builder contractDate(String value) {
      this.dataFields.put("contractDate", value);
      return this;
    }

    /**
     * Set the endingDate value
     *
     * Description: The date that a task process or action is to be completed or delivered by.
     *
     * @param value
     *          The endingDate value.
     * @return The builder for this class.
     */
    public Builder endingDate(String value) {
      this.dataFields.put("endingDate", value);
      return this;
    }

    /**
     * Set the comments value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The comments value.
     * @return The builder for this class.
     */
    public Builder comments(String value) {
      this.dataFields.put("comments", value);
      return this;
    }

    /**
     * Set the orderReference value
     *
     * Description: A reference or document order number as listed in business partner application.
     *
     * @param value
     *          The orderReference value.
     * @return The builder for this class.
     */
    public Builder orderReference(String value) {
      this.dataFields.put("orderReference", value);
      return this;
    }

    /**
     * Set the paymentTerms value
     *
     * Description: The setup and timing defined to complete a specified payment.
     *
     * @param value
     *          The paymentTerms value.
     * @return The builder for this class.
     */
    public Builder paymentTerms(String value) {
      this.dataFields.put("paymentTerms", value);
      return this;
    }

    /**
     * Set the userContact value
     *
     * Description: An acquaintance to reach for information related to the business partner.
     *
     * @param value
     *          The userContact value.
     * @return The builder for this class.
     */
    public Builder userContact(String value) {
      this.dataFields.put("userContact", value);
      return this;
    }

    /**
     * Set the partnerAddress value
     *
     * Description: The location of the selected business partner.
     *
     * @param value
     *          The partnerAddress value.
     * @return The builder for this class.
     */
    public Builder partnerAddress(String value) {
      this.dataFields.put("partnerAddress", value);
      return this;
    }

    /**
     * Set the priceCeiling value
     *
     * Description: An indication that the highest possible contract amount and quantity are being
     * charged (may depend on government regulations.
     *
     * @param value
     *          The priceCeiling value.
     * @return The builder for this class.
     */
    public Builder priceCeiling(Boolean value) {
      this.dataFields.put("priceCeiling", value);
      return this;
    }

    /**
     * Set the projectPhase value
     *
     * Description: One section or part of a project which is potentially made up of one or many
     * tasks.
     *
     * @param value
     *          The projectPhase value.
     * @return The builder for this class.
     */
    public Builder projectPhase(String value) {
      this.dataFields.put("projectPhase", value);
      return this;
    }

    /**
     * Set the projectType value
     *
     * Description: Project Type is a template of a common project.
     *
     * @param value
     *          The projectType value.
     * @return The builder for this class.
     */
    public Builder projectType(String value) {
      this.dataFields.put("projectType", value);
      return this;
    }

    /**
     * Set the contractQuantity value
     *
     * Description: The maximum legal quantity for a project.
     *
     * @param value
     *          The contractQuantity value.
     * @return The builder for this class.
     */
    public Builder contractQuantity(String value) {
      this.dataFields.put("contractQuantity", value);
      return this;
    }

    /**
     * Set the warehouse value
     *
     * Description: The location where products arrive to or are sent from.
     *
     * @param value
     *          The warehouse value.
     * @return The builder for this class.
     */
    public Builder warehouse(String value) {
      this.dataFields.put("warehouse", value);
      return this;
    }

    /**
     * Set the personInCharge value
     *
     * Description: An employee that is responsible for the project.
     *
     * @param value
     *          The personInCharge value.
     * @return The builder for this class.
     */
    public Builder personInCharge(String value) {
      this.dataFields.put("personInCharge", value);
      return this;
    }

    /**
     * Set the serviceRevenue value
     *
     * Description: Service Revenue
     *
     * @param value
     *          The serviceRevenue value.
     * @return The builder for this class.
     */
    public Builder serviceRevenue(String value) {
      this.dataFields.put("serviceRevenue", value);
      return this;
    }

    /**
     * Set the serviceCost value
     *
     * Description: Service Cost
     *
     * @param value
     *          The serviceCost value.
     * @return The builder for this class.
     */
    public Builder serviceCost(String value) {
      this.dataFields.put("serviceCost", value);
      return this;
    }

    /**
     * Set the reinvoicedExpenses value
     *
     * Description: Reinvoiced Expenses
     *
     * @param value
     *          The reinvoicedExpenses value.
     * @return The builder for this class.
     */
    public Builder reinvoicedExpenses(String value) {
      this.dataFields.put("reinvoicedExpenses", value);
      return this;
    }

    /**
     * Set the plannedExpenses value
     *
     * Description: Planned Expenses
     *
     * @param value
     *          The plannedExpenses value.
     * @return The builder for this class.
     */
    public Builder plannedExpenses(String value) {
      this.dataFields.put("plannedExpenses", value);
      return this;
    }

    /**
     * Set the serviceMargin value
     *
     * Description: Service Margin %
     *
     * @param value
     *          The serviceMargin value.
     * @return The builder for this class.
     */
    public Builder serviceMargin(String value) {
      this.dataFields.put("serviceMargin", value);
      return this;
    }

    /**
     * Set the expensesMargin value
     *
     * Description: Expenses Margin %
     *
     * @param value
     *          The expensesMargin value.
     * @return The builder for this class.
     */
    public Builder expensesMargin(String value) {
      this.dataFields.put("expensesMargin", value);
      return this;
    }

    /**
     * Set the projectStatus value
     *
     * Description: Indicates the current status of the project.
     *
     * @param value
     *          The projectStatus value.
     * @return The builder for this class.
     */
    public Builder projectStatus(String value) {
      this.dataFields.put("projectStatus", value);
      return this;
    }

    /**
     * Set the priceList value
     *
     * Description: A catalog of selected items with prices defined generally or for a specific
     * partner.
     *
     * @param value
     *          The priceList value.
     * @return The builder for this class.
     */
    public Builder priceList(String value) {
      this.dataFields.put("priceList", value);
      return this;
    }

    /**
     * Set the formOfPayment value
     *
     * Description: The method used for payment of this transaction.
     *
     * @param value
     *          The formOfPayment value.
     * @return The builder for this class.
     */
    public Builder formOfPayment(String value) {
      this.dataFields.put("formOfPayment", value);
      return this;
    }

    /**
     * Set the startingDate value
     *
     * Description: A parameter stating when a specified request will begin.
     *
     * @param value
     *          The startingDate value.
     * @return The builder for this class.
     */
    public Builder startingDate(String value) {
      this.dataFields.put("startingDate", value);
      return this;
    }

    /**
     * Set the servicesProvidedCost value
     *
     * Description: Services Provided Cost
     *
     * @param value
     *          The servicesProvidedCost value.
     * @return The builder for this class.
     */
    public Builder servicesProvidedCost(String value) {
      this.dataFields.put("servicesProvidedCost", value);
      return this;
    }

    /**
     * Set the outsourcedCost value
     *
     * Description: Outsourced Cost
     *
     * @param value
     *          The outsourcedCost value.
     * @return The builder for this class.
     */
    public Builder outsourcedCost(String value) {
      this.dataFields.put("outsourcedCost", value);
      return this;
    }

    /**
     * Set the paymentMethod value
     *
     * Description: It is the method by which payment is expected to be made or received.
     *
     * @param value
     *          The paymentMethod value.
     * @return The builder for this class.
     */
    public Builder paymentMethod(String value) {
      this.dataFields.put("paymentMethod", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public MultiphaseProjectData build() {
      return new MultiphaseProjectData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private MultiphaseProjectData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
