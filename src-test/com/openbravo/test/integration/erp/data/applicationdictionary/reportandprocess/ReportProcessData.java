/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.applicationdictionary.reportandprocess;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ReportProcessData
 *
 * @author plujan
 *
 */
public class ReportProcessData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the uIPattern value
     *
     * Description: Defines the UI Pattern
     *
     * @param value
     *          The uIPattern value.
     * @return The builder for this class.
     */
    public Builder uIPattern(String value) {
      this.dataFields.put("uIPattern", value);
      return this;
    }

    /**
     * Set the module value
     *
     * Description: Module
     *
     * @param value
     *          The module value.
     * @return The builder for this class.
     */
    public Builder module(String value) {
      this.dataFields.put("module", value);
      return this;
    }

    /**
     * Set the serviceSource value
     *
     * Description: Indicates where is located the external service. It can be an URL or a path.
     *
     * @param value
     *          The serviceSource value.
     * @return The builder for this class.
     */
    public Builder serviceSource(String value) {
      this.dataFields.put("serviceSource", value);
      return this;
    }

    /**
     * Set the serviceType value
     *
     * Description: Indicates the type of the external service
     *
     * @param value
     *          The serviceType value.
     * @return The builder for this class.
     */
    public Builder serviceType(String value) {
      this.dataFields.put("serviceType", value);
      return this;
    }

    /**
     * Set the externalService value
     *
     * Description: Sets the process as a external service that is loadable from the application
     * menu.
     *
     * @param value
     *          The externalService value.
     * @return The builder for this class.
     */
    public Builder externalService(Boolean value) {
      this.dataFields.put("externalService", value);
      return this;
    }

    /**
     * Set the jasperReport value
     *
     * Description: Jasper Report
     *
     * @param value
     *          The jasperReport value.
     * @return The builder for this class.
     */
    public Builder jasperReport(Boolean value) {
      this.dataFields.put("jasperReport", value);
      return this;
    }

    /**
     * Set the jRTemplateName value
     *
     * Description: JR Template name
     *
     * @param value
     *          The jRTemplateName value.
     * @return The builder for this class.
     */
    public Builder jRTemplateName(String value) {
      this.dataFields.put("jRTemplateName", value);
      return this;
    }

    /**
     * Set the background value
     *
     * Description: Is background process
     *
     * @param value
     *          The background value.
     * @return The builder for this class.
     */
    public Builder background(Boolean value) {
      this.dataFields.put("background", value);
      return this;
    }

    /**
     * Set the dataAccessLevel value
     *
     * Description: Indicates what type of data (in terms of AD_CLIENT and AD_ORG data columns) can
     * be entered or viewed.
     *
     * @param value
     *          The dataAccessLevel value.
     * @return The builder for this class.
     */
    public Builder dataAccessLevel(String value) {
      this.dataFields.put("dataAccessLevel", value);
      return this;
    }

    /**
     * Set the javaClassName value
     *
     * Description: x not implemented
     *
     * @param value
     *          The javaClassName value.
     * @return The builder for this class.
     */
    public Builder javaClassName(String value) {
      this.dataFields.put("javaClassName", value);
      return this;
    }

    /**
     * Set the directPrint value
     *
     * Description: Print without dialog
     *
     * @param value
     *          The directPrint value.
     * @return The builder for this class.
     */
    public Builder directPrint(Boolean value) {
      this.dataFields.put("directPrint", value);
      return this;
    }

    /**
     * Set the searchKey value
     *
     * Description: A fast method for finding a particular record.
     *
     * @param value
     *          The searchKey value.
     * @return The builder for this class.
     */
    public Builder searchKey(String value) {
      this.dataFields.put("searchKey", value);
      return this;
    }

    /**
     * Set the report value
     *
     * Description: An indication whether something is a document or a report which summarizes
     * information.
     *
     * @param value
     *          The report value.
     * @return The builder for this class.
     */
    public Builder report(Boolean value) {
      this.dataFields.put("report", value);
      return this;
    }

    /**
     * Set the procedure value
     *
     * Description: Name of the Database Procedure
     *
     * @param value
     *          The procedure value.
     * @return The builder for this class.
     */
    public Builder procedure(String value) {
      this.dataFields.put("procedure", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the helpComment value
     *
     * Description: A comment that adds additional information to help users work with fields.
     *
     * @param value
     *          The helpComment value.
     * @return The builder for this class.
     */
    public Builder helpComment(String value) {
      this.dataFields.put("helpComment", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ReportProcessData build() {
      return new ReportProcessData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ReportProcessData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
