/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testsuites.standardview.grid;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.HeaderTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.SalesOrderWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.util.ConfigurationProperties;

@RunWith(Parameterized.class)
public class GFI001_DateFilterPersistsAfterPressingKey extends OpenbravoERPTest {
  private static final String ENTERED_DATE_FILTER = "141215";
  private static final String ENTERED_DOC_NO_FILTER = "1";
  private static final String ENTERED_DATE_FILTER_FORMATTED = "14-12-2015";
  /** The data object to be used in the filtering. */
  private final DataObject dataObjectForFiltering;

  /**
   * Class constructor.
   *
   * @param dataObjectForFiltering
   *          The data object to be used in the filtering
   */
  public GFI001_DateFilterPersistsAfterPressingKey(DataObject dataObjectForFiltering) {
    this.dataObjectForFiltering = dataObjectForFiltering;
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<DataObject[]> editRecordValues() {
    DataObject[][] data = new DataObject[][] {
        { new SalesOrderHeaderData.Builder().orderDate(ENTERED_DATE_FILTER)
            .documentNo(ENTERED_DOC_NO_FILTER)
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test to check that a date filter is not cleared after pressing enter/tab keys.
   */
  @Test
  public void dateFilterNotCleared() {
    GeneratedTab<DataObject> tab = getSalesOrderHeader();
    tab.clearFilters();
    tab.filter(dataObjectForFiltering);
    tab.assertFilterValue("orderDate", ENTERED_DATE_FILTER_FORMATTED);
  }

  /**
   * Open and return the Sales Order header tab that will be used for the test.
   *
   * @return the generated tab that will be used for the test.
   */
  @SuppressWarnings("unchecked")
  public GeneratedTab<DataObject> getSalesOrderHeader() {
    SalesOrderWindow salesOrderWindow = new SalesOrderWindow();
    mainPage.openView(salesOrderWindow);
    GeneratedTab<DataObject> salesOrderHeaderTab = (GeneratedTab<DataObject>) salesOrderWindow
        .selectTab(HeaderTab.IDENTIFIER);
    return salesOrderHeaderTab;
  }
}
