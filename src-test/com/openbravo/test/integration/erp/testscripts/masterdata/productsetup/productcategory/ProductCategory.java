/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.masterdata.productsetup.productcategory;

import com.openbravo.test.integration.erp.data.masterdata.productsetup.productcategory.AccountingData;
import com.openbravo.test.integration.erp.data.masterdata.productsetup.productcategory.ProductCategoryData;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the window.
 *
 * @author Pablo Lujan
 */
public class ProductCategory {

  /**
   * Executes and verifies actions on the tab.
   *
   * @author plujan
   *
   */
  public static class ProductCategoryTab {

    /**
     * Creates the record.
     *
     * @param mainPage
     *          The application main page.
     * @param productCategoryData
     *
     */
    public static void create(MainPage mainPage, ProductCategoryData productCategoryData) {
      if (mainPage.isOnNewLayout()) {
        ProductCategoryScript.create(mainPage, productCategoryData);
      } else {
        ProductCategoryClassicScript.create(mainPage, productCategoryData);
      }
    }
  }

  /**
   * Executes and verifies actions on the tab.
   *
   * @author plujan
   *
   */
  public static class AccountingTab {

    /**
     * Verify the record was properly created.
     *
     * @param mainPage
     *          The application main page.
     * @param accountingData
     *          .
     */
    public static void verify(MainPage mainPage, AccountingData accountingData) {
      AccountingScript.verify(mainPage, accountingData);
    }
  }
}
