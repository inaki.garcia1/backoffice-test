/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.data.selectors;

import java.util.LinkedHashMap;

/**
 * Class for Shipment/Receipt Line Selector data.
 *
 * @author plujan
 *
 */
public class ShipmentReceiptLineSelectorData extends SelectorDataObject {

  /**
   * Class builder.
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the warehouse value.
     *
     * @param value
     *          The warehouse value.
     * @return The builder for this class.
     */
    public Builder warehouse(String value) {
      this.dataFields.put("shipmentReceipt$warehouse", value);
      return this;
    }

    /**
     * Set the documentNo value.
     *
     * @param value
     *          The documentNo value.
     * @return The builder for this class.
     */
    public Builder documentNo(String value) {
      this.dataFields.put("shipmentReceipt$documentNo", value);
      return this;
    }

    /**
     * Set the businessPartner value.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(String value) {
      this.dataFields.put("shipmentReceipt$businessPartner", value);
      return this;
    }

    /**
     * Set the movementDate value.
     *
     * @param value
     *          The movementDate value.
     * @return The builder for this class.
     */
    public Builder movementDate(String value) {
      this.dataFields.put("shipmentReceipt$movementDate", value);
      return this;
    }

    /**
     * Set the product value.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(String value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the movementQuantity value.
     *
     * @param value
     *          The movementQuantity value.
     * @return The builder for this class.
     */
    public Builder movementQuantity(String value) {
      this.dataFields.put("movementQuantity", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ShipmentReceiptLineSelectorData build() {
      return new ShipmentReceiptLineSelectorData(this);
    }
  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ShipmentReceiptLineSelectorData(Builder builder) {
    dataFields = builder.dataFields;
  }

  /**
   * Get the search key of the business partner selector data object.
   *
   * @return the search key of the business partner selector data object.
   */
  @Override
  public String getSearchKey() {
    return (String) dataFields.get("shipmentReceipt$documentNo");
  }

  /**
   * Get the display value of the selector data object.
   *
   * @return the value displayed by the selector data object.
   */
  @Override
  public String getDisplayValue() {
    return (String) dataFields.get("shipmentReceipt$businessPartner");
  }

}
