/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.masterdata.product.classic;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.data.masterdata.product.ProductData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Product tab.
 *
 * @author elopio
 *
 */
public class ProductTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Product";
  /** The tab level. */
  private static final int LEVEL = 0;

  /* GUI components. */
  /** Identifier of the tab. */
  public static final String TAB = "tabname180";

  /** Identifier of the search key text field. */
  private static final String TEXT_FIELD_SEARCH_KEY = "Value";
  /** Identifier of the name text field. */
  private static final String TEXT_FIELD_NAME = "Name";
  /** Identifier of the category combo box. */
  private static final String COMBO_BOX_CATEGORY = "reportM_Product_Category_ID_S";
  /** Identifier of the unit of measure combo box. */
  private static final String COMBO_BOX_UNIT_OF_MEASURE = "reportC_UOM_ID_S";
  /** Identifier of the product type combo box. */
  private static final String COMBO_BOX_PRODUCT_TYPE = "reportProductType_S";
  /** Identifier of the stocked check box. */
  @SuppressWarnings("unused")
  private static final String CHECK_BOX_STOCKED = "IsStocked";
  /** Identifier of the production check box. */
  @SuppressWarnings("unused")
  private static final String CHECK_BOX_PRODUCTION = "Production";
  /** Identifier of the bill of materials check box. */
  @SuppressWarnings("unused")
  private static final String CHECK_BOX_BILL_OF_MATERIALS = "IsBOM";
  /** Identifier of the purchase check box. */
  private static final String CHECK_BOX_PURCHASE = "IsPurchased";
  /** Identifier of the sale check box. */
  private static final String CHECK_BOX_SALE = "IsSold";
  /** Identifier of the tax category combo box. */
  private static final String COMBO_BOX_TAX_CATEGORY = "reportC_TaxCategory_ID_S";
  /** Identifier of the attribute set combo box. */
  private static final String COMBO_BOX_ATTRIBUTE_SET = "reportM_AttributeSet_ID_S";

  /**
   * Class constructor
   *
   */
  public ProductTab() {
    super(TITLE, LEVEL, TAB);
    addSubTab(new PriceTab());
  }

  /**
   * Create a Product.
   *
   * @param data
   *          The data of the Product that will be created.
   */
  public void create(ProductData data) {
    clickCreateNewRecord();
    if (data.getDataField("searchKey") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_SEARCH_KEY)
          .sendKeys(data.getDataField("searchKey").toString());
    }
    if (data.getDataField("name") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_NAME)
          .sendKeys(data.getDataField("name").toString());
    }
    if (data.getDataField("taxCategory") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_TAX_CATEGORY))
          .selectByVisibleText(data.getDataField("taxCategory").toString());
    }
    if (data.getDataField("productCategory") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_CATEGORY))
          .selectByVisibleText(data.getDataField("productCategory").toString());
    }
    if (data.getDataField("uOM") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_UNIT_OF_MEASURE))
          .selectByVisibleText(data.getDataField("uOM").toString());
    }
    if (data.getDataField("productType") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_PRODUCT_TYPE))
          .selectByVisibleText(data.getDataField("productType").toString());
    }
    if (data.getDataField("purchase") != null) {
      WebElement checkBoxPurchase = SeleniumSingleton.INSTANCE.findElementById(CHECK_BOX_PURCHASE);
      if (!data.getDataField("purchase").equals(checkBoxPurchase.isSelected())) {
        checkBoxPurchase.click();
      }
    }
    if (data.getDataField("sale") != null) {
      WebElement checkBoxSale = SeleniumSingleton.INSTANCE.findElementById(CHECK_BOX_SALE);
      if (!data.getDataField("sale").equals(checkBoxSale.isSelected())) {
        checkBoxSale.click();
      }
    }
    if (data.getDataField("attributeSet") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_ATTRIBUTE_SET))
          .selectByVisibleText(data.getDataField("attributeSet").toString());
    }
    clickSave();
  }
}
