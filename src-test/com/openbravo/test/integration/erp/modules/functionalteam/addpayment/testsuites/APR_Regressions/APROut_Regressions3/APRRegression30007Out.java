/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions3;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddTransactionData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.MatchStatementData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.bankstatement.BankStatementHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.BankStatementLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddTransactionProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementGrid;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 30007
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression30007Out extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PurchaseOrderHeaderData purchaseOrderHeaderData;
  PurchaseOrderHeaderData purchaseOrderHeaderVerficationData;
  PurchaseOrderLinesData purchaseOrderLinesData;
  PurchaseOrderLinesData purchaseOrderLinesVerificationData;
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData;
  AccountData accountHeaderData;
  BankStatementHeaderData bankStatementHeaderData;
  BankStatementLinesData bankStatementLinesData;
  MatchStatementData matchStatementData;
  AddTransactionData addTransactionVerificationData;
  AddPaymentPopUpData addPaymentTotalsVerificationData;
  MatchStatementData matchStatementData2;
  PaymentPlanData orderPaymentOutPlanData;
  PaymentDetailsData orderPaymentOutDetailsData;

  /**
   * Class constructor.
   *
   */
  public APRRegression30007Out(PurchaseOrderHeaderData purchaseOrderHeaderData,
      PurchaseOrderHeaderData purchaseOrderHeaderVerficationData,
      PurchaseOrderLinesData purchaseOrderLinesData,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData,
      AccountData accountHeaderData, BankStatementHeaderData bankStatementHeaderData,
      BankStatementLinesData bankStatementLinesData, MatchStatementData matchStatementData,
      AddTransactionData addTransactionVerificationData,
      AddPaymentPopUpData addPaymentTotalsVerificationData, MatchStatementData matchStatementData2,
      PaymentPlanData orderPaymentOutPlanData, PaymentDetailsData orderPaymentOutDetailsData) {
    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    this.purchaseOrderHeaderVerficationData = purchaseOrderHeaderVerficationData;
    this.purchaseOrderLinesData = purchaseOrderLinesData;
    this.purchaseOrderLinesVerificationData = purchaseOrderLinesVerificationData;
    this.bookedPurchaseOrderHeaderVerificationData = bookedPurchaseOrderHeaderVerificationData;
    this.accountHeaderData = accountHeaderData;
    this.bankStatementHeaderData = bankStatementHeaderData;
    this.bankStatementLinesData = bankStatementLinesData;
    this.matchStatementData = matchStatementData;
    this.addTransactionVerificationData = addTransactionVerificationData;
    this.addPaymentTotalsVerificationData = addPaymentTotalsVerificationData;
    this.matchStatementData2 = matchStatementData2;
    this.orderPaymentOutPlanData = orderPaymentOutPlanData;
    this.orderPaymentOutDetailsData = orderPaymentOutDetailsData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> purchaseOrderValues() {
    Object[][] data = new Object[][] { {
        new PurchaseOrderHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor Madrid").build())
            .paymentMethod("1 (Spain)")
            .priceList("Purchase")
            .build(),
        new PurchaseOrderHeaderData.Builder().organization("Spain")
            .transactionDocument("Purchase Order")
            .partnerAddress(".Madrid, Enorme")
            .warehouse("Spain warehouse")
            .paymentTerms("90 days")
            .build(),
        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMA").priceListVersion("Purchase").build())
            .orderedQuantity("500")
            .build(),
        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("1,000.00")
            .build(),
        new PurchaseOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("1,000.00")
            .grandTotalAmount("1,100.00")
            .currency("EUR")
            .build(),

        new AccountData.Builder().name("Spain Bank").build(),
        new BankStatementHeaderData.Builder().name("APRRegression30007Out").build(),
        new BankStatementLinesData.Builder().referenceNo("30007Out")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor Madrid").build())
            .dramount("1,100.00")
            .build(),

        new MatchStatementData.Builder().bankStatementType("P")
            .sender("Vendor Madrid")
            .referenceNo("30007Out")
            .glitem("")
            .amount("-1100")
            .affinity("")
            .matchedDocument("")
            .matchingType("")
            .transactionDate("")
            .businessPartner("")
            .transactionAmount("")
            .transactionGLItem("")
            .build(),
        new AddTransactionData.Builder().trxtype("BP Withdrawal")
            .currency("EUR")
            .depositamt("0.00")
            .withdrawalamt("1,100.00")
            .organization("Spain")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor Madrid").build())
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Vendor Madrid")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("EUR")
            .actual_payment("1,100.00")
            .expected_payment("1,100.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("1,100.00")
            .total("1,100.00")
            .difference("0.00")
            .build(),
        new MatchStatementData.Builder().bankStatementType("P")
            .sender("Vendor Madrid")
            .referenceNo("30007Out")
            .glitem("")
            .amount("-1100")
            .affinity("AD")
            .matchedDocument("T")
            .matchingType("AD")
            .businessPartner("Vendor Madrid")
            .transactionAmount("-1100")
            .transactionGLItem("")
            .build(),

        new PaymentPlanData.Builder().dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("1 (Spain)")
            .expected("1,100.00")
            .paid("1,100.00")
            .outstanding("0.00")
            .numberOfPayments("1")
            .currency("EUR")
            .build(),
        new PaymentDetailsData.Builder().dueDate(OBDate.CURRENT_DATE)
            .expected("1,100.00")
            .paid("1,100.00")
            .writeoff("0.00")
            .status("Payment Cleared")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 30007 - Payment Out flow
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression30007OutTest() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression30007Out] Test regression 30007 - Payment Out flow. **");

    PurchaseOrder purchaseOrder = new PurchaseOrder(mainPage).open();
    purchaseOrder.create(purchaseOrderHeaderData);
    purchaseOrder.assertSaved();
    purchaseOrder.assertData(purchaseOrderHeaderVerficationData);
    String orderNo = purchaseOrder.getData("documentNo").toString();
    PurchaseOrder.Lines purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.create(purchaseOrderLinesData);
    purchaseOrderLines.assertSaved();
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData);
    purchaseOrder.book();
    purchaseOrder.assertProcessCompletedSuccessfully2();
    purchaseOrder.assertData(bookedPurchaseOrderHeaderVerificationData);

    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);

    FinancialAccount.BankStatements bankStatement = financialAccount.new BankStatements(mainPage);
    String date;
    if (bankStatement.getRecordCount() == 0) {
      date = OBDate.CURRENT_DATE;
    } else {
      bankStatement.selectWithoutFiltering(0);
      date = OBDate.addDaysToDate((String) bankStatement.getData("transactionDate"), 1);
    }
    bankStatementHeaderData.addDataField("transactionDate", date);
    bankStatementHeaderData.addDataField("importdate", date);
    bankStatement.create(bankStatementHeaderData);
    bankStatement.assertSaved();

    FinancialAccount.BankStatements.BankStatementLines bankStatementLines = bankStatement.new BankStatementLines(
        mainPage);
    bankStatementLinesData.addDataField("transactionDate", date);
    bankStatementLines.create(bankStatementLinesData);
    bankStatementLines.assertSaved();
    bankStatement.process();
    bankStatement.assertProcessCompletedSuccessfully2();

    @SuppressWarnings("rawtypes")
    MatchStatementProcess matchStatementProcess = financialAccount.openMatchStatement(false);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementData.addDataField("banklineDate", date);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData);

    AddTransactionProcess addTransactionProcess = ((MatchStatementGrid) matchStatementProcess
        .getMatchStatementGrid()).clickAdd(0);
    addTransactionProcess.assertData(addTransactionVerificationData);
    AddPaymentProcess addPaymentProcess = addTransactionProcess.openAddPayment();
    addPaymentProcess.setParameterValue("received_from",
        new BusinessPartnerSelectorData.Builder().name("Vendor Madrid").build());
    addPaymentProcess.setParameterValue("fin_paymentmethod_id", "1 (Spain)");
    addPaymentProcess.process("Orders", orderNo, "Process Made Payment(s)",
        addPaymentTotalsVerificationData);
    String orderDescription = "Order No.: " + orderNo + "\n";
    addTransactionVerificationData.addDataField("trxdate", date);
    addTransactionVerificationData.addDataField("dateacct", date);
    addTransactionVerificationData.addDataField("description", orderDescription);
    addTransactionProcess.assertData(addTransactionVerificationData);
    addTransactionProcess.process();

    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementData2.addDataField("banklineDate", date);
    matchStatementData2.addDataField("trxDate", date);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData2);
    matchStatementProcess.process();
    financialAccount.assertProcessCompletedSuccessfully2();

    purchaseOrder.open();
    Sleep.trySleep(15000);
    purchaseOrder.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    PurchaseOrder.PaymentOutPlan orderPaymentOutPlan = purchaseOrder.new PaymentOutPlan(mainPage);
    orderPaymentOutPlan.assertCount(1);
    orderPaymentOutPlanData.addDataField("lastPayment", date);
    orderPaymentOutPlan.assertData(orderPaymentOutPlanData);
    PurchaseOrder.PaymentOutPlan.PaymentOutDetails orderPaymentOutDetails = orderPaymentOutPlan.new PaymentOutDetails(
        mainPage);
    orderPaymentOutDetails.assertCount(1);
    orderPaymentOutDetailsData.addDataField("paymentDate", date);
    orderPaymentOutDetails.assertData(orderPaymentOutDetailsData);

    logger.info(
        "** End of test case [APRRegression30007Out] Test regression 30007 - Payment Out flow. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
