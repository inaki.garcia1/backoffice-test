/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014 Openbravo S.L.U.
 * All Rights Reserved.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.process.parameterwindow;

import org.junit.Test;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Test to open a parameter window with a process button
 *
 * @author AugustoMauch
 *
 */
public abstract class CheckDefaultParameterValues extends OpenbravoERPTest {

  public static final String REGISTRY_KEY_BUTTON_TEST_PARAMETER_WINDOW = "org.openbravo.client.application.toolbar.button.obpw3Paramwindow.133";

  /**
   * Class constructor.
   *
   *
   * /** Test to insert a new row in the grid.
   */
  @Test
  public void defaultValuesShouldBeProperlySet() {
    GeneratedTab<DataObject> tab = getTab();
    tab.selectWithoutFiltering(0);
    // OBParameterWindow parameterWindow = tab
    // .openParameterWindow(REGISTRY_KEY_BUTTON_TEST_PARAMETER_WINDOW);

    // // check primitive default value
    // String paramValue = (String) parameterWindow.getParameterValue("primitiveDefaultValue");
    // assertTrue("Invalid primitive default value", "primitiveDefaultValue".equals(paramValue));
    //
    // // check subordinate (from param) default value
    // paramValue = (String) parameterWindow.getParameterValue("subordinateDefaultValue");
    // assertTrue("Invalid subordinate (from param) default value",
    // "primitiveDefaultValue".equals(paramValue));
    //
    // // check subordinate (from selected record) default value
    // paramValue = (String) parameterWindow.getParameterValue("numWithCallout");
    // assertTrue("Invalid subordinate (from selected record) default value",
    // "2".equals(paramValue));
  }

  /**
   * Open and return the generated tab that will be used for the test.
   *
   * @return the generated tab that will be used for the test.
   */
  public abstract GeneratedTab<DataObject> getTab();
}
