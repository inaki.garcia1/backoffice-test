/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.accounting.transactions.gljournal;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.AccountSelectorData;

/**
 *
 * Class for GLJournalLinesData
 *
 * @author plujan
 *
 */
public class GLJournalLinesData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the accountingCombination value
     *
     * Description: An identification code comprised of an individual account number and additional
     * dimensions such organization product and business partner.
     *
     * @param value
     *          The accountingCombination value.
     * @return The builder for this class.
     */
    public Builder accountingCombination(AccountSelectorData value) {
      this.dataFields.put("accountingCombination", value);
      return this;
    }

    /**
     * Set the accountingDate value
     *
     * Description: The date this transaction is recorded for in the general ledger.
     *
     * @param value
     *          The accountingDate value.
     * @return The builder for this class.
     */
    public Builder accountingDate(String value) {
      this.dataFields.put("accountingDate", value);
      return this;
    }

    /**
     * Set the currencyRateType value
     *
     * Description: A distinct currency rate characteristic used for processes.
     *
     * @param value
     *          The currencyRateType value.
     * @return The builder for this class.
     */
    public Builder currencyRateType(String value) {
      this.dataFields.put("currencyRateType", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the rate value
     *
     * Description: The percentage to be multiplied by the source to arrive at the tax or exchange
     * amount.
     *
     * @param value
     *          The rate value.
     * @return The builder for this class.
     */
    public Builder rate(String value) {
      this.dataFields.put("rate", value);
      return this;
    }

    /**
     * Set the credit value
     *
     * Description: The amount credited to an account converted to the organization default
     * currency.
     *
     * @param value
     *          The credit value.
     * @return The builder for this class.
     */
    public Builder credit(String value) {
      this.dataFields.put("credit", value);
      return this;
    }

    /**
     * Set the debit value
     *
     * Description: The amount debited to an account converted to the organization default currency.
     *
     * @param value
     *          The debit value.
     * @return The builder for this class.
     */
    public Builder debit(String value) {
      this.dataFields.put("debit", value);
      return this;
    }

    /**
     * Set the foreignCurrencyCredit value
     *
     * Description: The amount credited from the account given in provider currency.
     *
     * @param value
     *          The foreignCurrencyCredit value.
     * @return The builder for this class.
     */
    public Builder foreignCurrencyCredit(String value) {
      this.dataFields.put("foreignCurrencyCredit", value);
      return this;
    }

    /**
     * Set the foreignCurrencyDebit value
     *
     * Description: The amount debited from the account given in provider currency.
     *
     * @param value
     *          The foreignCurrencyDebit value.
     * @return The builder for this class.
     */
    public Builder foreignCurrencyDebit(String value) {
      this.dataFields.put("foreignCurrencyDebit", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the lineNo value
     *
     * Description: A line stating the position of this request in the document.
     *
     * @param value
     *          The lineNo value.
     * @return The builder for this class.
     */
    public Builder lineNo(String value) {
      this.dataFields.put("lineNo", value);
      return this;
    }

    /**
     * Set the journalEntry value
     *
     * Description: One transaction with a debit and credit and entered into the general ledger.
     *
     * @param value
     *          The journalEntry value.
     * @return The builder for this class.
     */
    public Builder journalEntry(String value) {
      this.dataFields.put("journalEntry", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public GLJournalLinesData build() {
      return new GLJournalLinesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private GLJournalLinesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
