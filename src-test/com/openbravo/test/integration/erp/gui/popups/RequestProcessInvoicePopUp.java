/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.popups;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions on the Sales Order Request Process pop up.
 *
 * @author elopio
 *
 */
public class RequestProcessInvoicePopUp extends RequestProcessPopUp {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Class constructor.
   *
   * @param registryKey
   *          The pop up key in the Openbravo Test Registry array.
   */
  public RequestProcessInvoicePopUp(String registryKey) {
    super(registryKey);
  }

  /**
   * Select the pop up frame.
   */
  @Override
  protected void selectFrame() {
    SeleniumSingleton.INSTANCE.switchTo().frame("process");
    try {
      logger.debug("Switching to frame mainframe.");
      SeleniumSingleton.INSTANCE.switchTo().frame("mainframe");
      logger.debug("Done switching.");
    } catch (Exception e) {
      logger.debug("No such frame 'mainframe' called from extended RequestProcess.");
      // Continue
    }
  }

}
