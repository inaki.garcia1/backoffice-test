/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.enterprise.enterprisemodulemanagement;

import java.util.ArrayList;
import java.util.List;

import com.openbravo.test.integration.erp.data.generalsetup.ModuleReferenceData;

/**
 * Class for Enterprise Module Management data.
 *
 * @author elopio
 *
 */
public class EnterpriseModuleManagementData {

  /** Formatting string used to get a string representation of this object. */
  private static final String FORMAT_STRING_REPRESENTATION = "[Enterprise Module Management Data:\n"
      + "organization=%s,\n" + "reference data=%s]";

  /* Data for this test */
  /** The organization to manage. */
  private String organization;
  /** The reference data that will be added to the organization. */
  private List<ModuleReferenceData> referenceDataList;

  /**
   * Class builder.
   *
   * @author elopio
   *
   */
  public static class Builder {

    /** The organization to manage. */
    private String organization = null;
    /** The reference data that will be added to the organization. */
    private List<ModuleReferenceData> referenceDataList = null;

    /**
     * Set the organization value.
     *
     * @param value
     *          The organization to manage.
     * @return the builder for this class.
     */
    public Builder organization(String value) {
      this.organization = value;
      return this;
    }

    /**
     * Set the reference data value.
     *
     * @param value
     *          The reference data that will be added to the organization.
     * @return the builder for this class.
     */
    public Builder referenceDataList(List<ModuleReferenceData> value) {
      this.referenceDataList = value;
      return this;
    }

    /**
     * Add a reference data value to the list.
     *
     * @param value
     *          The reference data value.
     * @return the builder for this class.
     */
    public Builder addReferenceData(ModuleReferenceData value) {
      if (this.referenceDataList == null) {
        this.referenceDataList = new ArrayList<ModuleReferenceData>();
      }
      this.referenceDataList.add(value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return the data object.
     */
    public EnterpriseModuleManagementData build() {
      return new EnterpriseModuleManagementData(this);
    }
  }

  /**
   * Class constructor.
   *
   * @param builder
   *          The object builder.
   */
  private EnterpriseModuleManagementData(Builder builder) {
    organization = builder.organization;
    referenceDataList = builder.referenceDataList;
  }

  /**
   * Get the organization to manage.
   *
   * @return the organization to manage.
   */
  public String getOrganization() {
    return organization;
  }

  /**
   * Get the reference data that will be added to the organization.
   *
   * @return the reference data that will be added to the organization.
   */
  public List<ModuleReferenceData> getReferenceDataList() {
    return referenceDataList;
  }

  /**
   * Get the string representation of this data object.
   *
   * @return the string representation.
   */
  @Override
  public String toString() {
    return String.format(FORMAT_STRING_REPRESENTATION, organization, referenceDataList.toString());
  }

}
