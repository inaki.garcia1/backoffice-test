/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Rowinson Gallego <rwn.gallego@gmail.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.navigationbarcomponents.userprofile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Test the profile information change from the user profile widget added by Navigation Bar
 * Components module.
 *
 * @author rwngallego
 *
 */
public class ChangeProfileInformation extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The new profile. */
  private final ProfileData profile;

  /**
   * Class constructor.
   *
   * @param profile
   *          The new profile.
   */
  public ChangeProfileInformation(ProfileData profile) {
    this.profile = profile;
  }

  /**
   * Test profile information change.
   */
  @Test
  public void profileInformationShouldBeChanged() {
    if (profile.getClient() == null && profile.getOrganization() == null
        && profile.getRole() != null && profile.getWarehouse() == null
        && profile.getLanguage() == null) {
      logger.info("[NAV0040a] Change user profile. Change role");
    }
    if (profile.getClient() == null && profile.getOrganization() != null
        && profile.getRole() != null && profile.getWarehouse() == null
        && profile.getLanguage() == null) {
      logger.info("[NAV0040b] Change user profile. Change role and org");
    }
    if (profile.getClient() == null && profile.getOrganization() == null
        && profile.getRole() != null && profile.getWarehouse() != null
        && profile.getLanguage() == null) {
      logger.info("[NAV0040c] Change the user profile. Change the role and the warehouse");
    }
    mainPage.changeProfile(profile);
    mainPage.assertProfile(profile);
    mainPage.closeProfile();
  }
}
