/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testsuites.navigationbarcomponents;

import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.SuiteThatStopsIfFailure;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Navigation Bar Components test suite.
 *
 * @author elopio
 *
 */
@RunWith(SuiteThatStopsIfFailure.class)
@Suite.SuiteClasses({ NAV0010_LaunchViewFromMenuWidget.class,
    NAV0020_LaunchViewFromQuickLaunchWidget.class,
    NAV0030_LaunchViewFromCreateNewRecordWidget.class, NAV0040_ChangeProfileInformation.class,
    NAV0060_DisplayAndHideQuickLaunch.class, NAV0070_DisplayAndHideCreateNew.class,
    NAV0080_DisplayAndHideMenu.class, NAV0090_OpenAlertsFromNavigationBar.class,
    NAV0100_OpenHelpWidgetWithNoWindowsOpen.class, NAV0110_OpenHelpWindow.class,
    NAV0140_CheckYourCompanyLogo.class, NAV0150_DisplayAndHideUserProfileTabs.class,
    NAV0160_ChangeDefaultProfile.class, NAV0170_ChangeProfileWithDefaultUnchecked.class,
    NAV0180_CancelProfileChanges.class, NAV0190_ChangePasswordWithIncorrectConfirmation.class,
    NAV0200_ChangePasswordWithIncorrectPassword.class, NAV0210_CancelPasswordChange.class,
    NAV0050_ChangePassword.class })
public class NavigationBarComponentsSuite {
  // No content is required, this is just the definition of a test suite.
  @AfterClass
  public static void tearDown() {
    SeleniumSingleton.INSTANCE.quit();
    OpenbravoERPTest.seleniumStarted = false;
    OpenbravoERPTest.forceLoginRequired();
  }
}
