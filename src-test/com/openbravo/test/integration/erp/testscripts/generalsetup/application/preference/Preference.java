/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.application.preference;

import com.openbravo.test.integration.erp.data.generalsetup.application.preference.PreferenceData;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the Preference window.
 *
 * @author aferraz
 */
public class Preference {

  /**
   * Executes and verifies actions on the Preference tab.
   *
   * @author aferraz
   *
   */
  public static class PreferenceTab {

    /**
     * Creates the record.
     *
     * @param mainPage
     *          The application main page.
     * @param preferenceData
     *
     */
    public static void create(MainPage mainPage, PreferenceData preferenceData) {
      if (mainPage.isOnNewLayout()) {
        PreferenceScript.create(mainPage, preferenceData);
      }
    }

    /**
     * Deletes the record.
     *
     * @param mainPage
     *          The application main page.
     * @param preferenceData
     *
     */
    public static void delete(MainPage mainPage, PreferenceData preferenceData) {
      if (mainPage.isOnNewLayout()) {
        PreferenceScript.delete(mainPage, preferenceData);
      }
    }

    /**
     * Assert the number of records is expectedCount
     *
     * @param mainPage
     *          The application main page.
     * @param preferenceData
     *          The data of the record that will be created.
     * @param expectedCount
     *          The expected number of records.
     */
    public static void assertNumRecords(MainPage mainPage, PreferenceData preferenceData,
        int expectedCount) {
      if (mainPage.isOnNewLayout()) {
        PreferenceScript.assertNumRecords(mainPage, preferenceData, expectedCount);
      }
    }

    /**
     * Filter the records on grid.
     *
     * @param mainPage
     *          The application main page.
     * @param preferenceData
     *          The data of the record that will be filtered.
     *
     */
    public static void filter(MainPage mainPage, PreferenceData preferenceData) {
      if (mainPage.isOnNewLayout()) {
        PreferenceScript.filter(mainPage, preferenceData);
      }
    }

    /**
     * Get the number of records on grid.
     *
     * @param mainPage
     *          The application main page.
     *
     */
    public static int getRecordCount(MainPage mainPage) {
      return PreferenceScript.getRecordCount(mainPage);
    }
  }
}
