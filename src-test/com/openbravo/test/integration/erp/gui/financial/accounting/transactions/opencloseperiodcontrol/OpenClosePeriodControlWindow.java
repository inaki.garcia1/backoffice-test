/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.accounting.transactions.opencloseperiodcontrol;

import com.openbravo.test.integration.erp.modules.client.application.gui.StandardWindow;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;

/**
 * Executes and verify actions on the Open/Close Period Control standard window of OpenbravoERP.
 *
 * @author elopio
 *
 */
public class OpenClosePeriodControlWindow extends StandardWindow {

  /** The window title. */
  @SuppressWarnings("hiding")
  private static final String TITLE = "Open/Close Period Control";
  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.FINANCIAL_MANAGEMENT,
      Menu.ACCOUNTING, Menu.ACCOUNTING_SETUP, Menu.OPEN_CLOSE_PERIOD_CONTROL };

  /**
   * Class constructor.
   *
   */
  public OpenClosePeriodControlWindow() {
    super(TITLE, MENU_PATH);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void load() {
    OpenClosePeriodControlTab openClosePeriodControlTab = new OpenClosePeriodControlTab();
    addTopLevelTab(openClosePeriodControlTab);
    super.load();
  }

  /**
   * Select and return the Calendar tab.
   *
   * @return the Calendar tab.
   */
  public OpenClosePeriodControlTab selectOpenClosePeriodControlTab() {
    return (OpenClosePeriodControlTab) selectTab(OpenClosePeriodControlTab.IDENTIFIER);
  }

}
