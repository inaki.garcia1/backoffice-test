/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import com.openbravo.test.integration.selenium.Attribute;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes actions on OpenbravoERP check boxes built upon SmartClient user interface library.
 *
 * @author elopio
 *
 */
public class CheckBox extends FormItem implements InputField {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Formatting string to locate the check box. The parameter is the form locator. */
  private final static String FORMAT_LOCATOR_SMART_CLIENT_CHECKBOX = "%s/textbox";
  /** Formatting string to get the value of the check box. The parameter is the object string. */
  private final static String FORMAT_SMART_CLIENT_GET_VALUE = "%s.getValue()";

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   */
  public CheckBox(String objectString) {
    super(objectString);
  }

  /**
   * Class constructor.
   *
   * This constructor will just instantiate the check box, but after this the check box item will
   * not be ready to be used. It has to be initialized using the function setSmartClientLocator in
   * order to be used with Selenium.
   *
   * This is an alternate way to access the SmartClient object, and should be used only if there is
   * no way to get the object string. Always prefer to call the other constructor with the object
   * string as a parameter, because it will set the SmartClient Locator. With this method the
   * actions that require to call SmartClient functions and attributes directly will not be
   * available.
   */
  public CheckBox() {
  }

  /**
   * Get the SmartClient locator of the check box. This is the full path, including the form locator
   * and the item locator.
   *
   * @return the SmartClient locator of the check box.
   */
  @Override
  protected String getSmartClientLocator() {
    if (smartClientLocator == null) {
      String formItemLocator = super.getSmartClientLocator();
      return String.format(FORMAT_LOCATOR_SMART_CLIENT_CHECKBOX, formItemLocator);
    } else {
      return smartClientLocator;
    }
  }

  // FIXME this things should be defined on a toggle button class, and check boxes and radio
  // buttons should inherit from that class.

  /**
   * Get whether the check box is checked or not.
   *
   * @return true if the check box is checked. Otherwise false.
   */
  public boolean isChecked() {
    if (objectString != null) {
      Boolean checkBoxValue = (Boolean) SeleniumSingleton.INSTANCE
          .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_VALUE, objectString));
      if (checkBoxValue == null) {
        checkBoxValue = false;
      }
      return checkBoxValue;
    } else {
      // XXX this is fragile.
      // TODO does this work only with grid check boxes?
      // TODO L00: Remove following static sleep when it is required no more
      Sleep.trySleep();
      return getAttribute(Attribute.CLASS).contains("Selected");
    }
  }

  /**
   * Check the check box.
   */
  public void check() {
    logger.trace("Checking check box {}.", this::getInformationForLog);
    setValue(true);
  }

  /**
   * Uncheck the check box.
   */
  public void uncheck() {
    logger.trace("Unchecking check box {}.", this::getInformationForLog);
    setValue(false);
  }

  /**
   * Set the value of the check box.
   *
   * @param value
   *          The new value of the check box.
   */
  public void setValue(Boolean value) {
    if (value != isChecked()) {
      try {
        SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()).click();
      } catch (StaleElementReferenceException sere) {
        logger.warn("StaleElementReferenceException setting value in CheckBox, retry it again");
        SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()).click();
      } catch (ElementNotVisibleException enve2) {
        // This is a workaround made when updating from FF19 & Selenium2.33 to FF33 & Selenium2.44
        // Test failed since the scroll did not automatically
        // put
        // the field visible.
        // So the solution is to catch the exception and force the scroll to get the field into
        // view.
        // Note: Probably this focusing could have been moved to be the standard behavior, but I
        // prefer to keep it as an exception since it is like this for us.
        // Note 2: I do not understand why the update failed, but I've tested other FF and Selenium
        // combinations and as low as FF24 and Selenium2.37 the issue can be reproduced.
        logger.warn(
            "Interaction with field [{}] is not possible since it is not visibile, forcing the scroll.",
            getSmartClientLocator());
        WebElement obj = SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator());
        SeleniumSingleton.INSTANCE.executeScript("arguments[0].scrollIntoView(false);", obj);
        SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
            .sendKeys(Keys.END);
      } catch (WebDriverException wde2) {
        // This is a workaround made when updating to FF52.1.2esr & Selenium3.3.1 & Geckodriver.
        // Before Selenium 3.3.1, this exception was treated like an ElementNotVisibleException, but
        // now it seems to be removed and now it is throwing a WebDriverException
        // Test failed since the scroll did not automatically
        // put
        // the field visible.
        // So the solution is to catch the exception and force the scroll to get the field into
        // view.
        // Note: Probably this focusing could have been moved to be the standard behavior, but I
        // prefer to keep it as an exception since it is like this for us.
        // Note 2: I do not understand why the update failed, but I've tested other FF and Selenium
        // combinations and as low as FF24 and Selenium2.37 the issue can be reproduced.
        logger.warn(
            "Interaction with field [{}] is not possible since it is not visibile, forcing the scroll.",
            getSmartClientLocator());
        WebElement obj = SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator());
        SeleniumSingleton.INSTANCE.executeScript("arguments[0].scrollIntoView(false);", obj);
        SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
            .sendKeys(Keys.END);
      }
    }
    // XXX this was added because the payment term check box wasn't toggled the first time.
    if (value != isChecked()) {
      logger.warn("The click on the check box didn't work. Trying again.");
      // retry.
      SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()).click();
    }
    // XXX this was added because the sales price list check box wasn't toggled sometimes.
    if (value != isChecked()) {
      logger.warn(
          "The click on the check box didn't work. Changing it's value directly with SmartClient.");
      smartClientSetValue(value.toString());
      Sleep.trySleep();
    }
  }

  /**
   * Get the value of the check box.
   *
   * @return true if the check box is checked. Otherwise, false.
   */
  @Override
  public Object getValue() {
    return isChecked();
  }

  /**
   * Set the value of a check box.
   *
   * @param value
   *          The value to set.
   */
  @Override
  public void setValue(Object value) {
    if (value instanceof Boolean) {
      setValue((Boolean) value);
    } else {
      throw new IllegalArgumentException("Valid values for check boxes are only Booleans.");
    }
  }
}
