/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *      Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.sales.transactions.createshipmentsfromorders;

import static org.junit.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.openbravo.test.integration.erp.gui.MessageBox;
import com.openbravo.test.integration.erp.modules.client.application.gui.ClassicProcessView;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Execute and verify actions on the create shipments from orders view.
 *
 * @author elopio
 *
 */
public class CreateShipmentsFromOrdersWindow extends ClassicProcessView {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The window title. */
  public static final String TITLE = "Create Shipments from Orders";

  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.SALES_MANAGEMENT,
      Menu.SALES_MANAGEMENT_TRANSACTIONS, Menu.CREATE_SHIPMENTS_FROM_ORDERS };

  /**
   * Formatting string used to locate the checkbox of an order, with its number.
   */
  private static final String FORMAT_CHECK_BOX_ORDER = "//a[@class='Link']/span[text()='%s']/../../..//input";
  /** Identifier of the Process button */
  private static final String BUTTON_PROCESS = "buttonProcess";
  /** Regular expression used to verify that a shipment was created. */
  private static final String REGULAR_EXPRESSION_SHIPMENT_CREATED = "Order No. %s Shipment No. .*: Shipment completed successfully.";

  /**
   * Class constructor.
   */
  public CreateShipmentsFromOrdersWindow() {
    super(TITLE, MENU_PATH);
  }

  /**
   * Create a shipment from an order The selected order is the first that appears on the table
   *
   * @param numbers
   *          The numbers of the orders.
   */
  public void createShipmentsFromOrders(String... numbers) {
    for (String number : numbers) {
      // TODO L 3: Check sleeps in this method. Removable? Maybe removing them induces 'random'
      // error
      Sleep.trySleep();
      // Scroll to that record to avoid errors induced for old classic windows
      WebElement element = SeleniumSingleton.INSTANCE
          .findElementByXPath(String.format(FORMAT_CHECK_BOX_ORDER, number));
      ((JavascriptExecutor) SeleniumSingleton.INSTANCE)
          .executeScript("arguments[0].scrollIntoView(true);", element);
      // Click in the element (the specific checkbox)
      element.click();
      Sleep.trySleep();
      logger
          .debug("Element " + String.format(FORMAT_CHECK_BOX_ORDER, number) + " has been checked");
    }
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_PROCESS).click();
  }

  /**
   * Verify that the process completed successfully
   *
   * @param numbers
   *          The numbers of the orders.
   */
  public void assertProcessCompletedSuccessfully(String... numbers) {
    // TODO L1: Remove static sleep. Assert execute before it has finished loading
    Sleep.trySleep(1000);
    final MessageBox messageBox = new MessageBox();
    messageBox.verifyTitle(MessageBox.TITLE_PROCESS_COMPLETED_SUCCESSFULLY);
    // form the message regular expression.
    StringBuilder messageBuilder = new StringBuilder();
    if (numbers.length >= 1) {
      String messageLine = String.format(REGULAR_EXPRESSION_SHIPMENT_CREATED, numbers[0]);
      messageBuilder.append(messageLine);
    }
    for (int index = 1; index <= numbers.length - 1; index++) {
      messageBuilder.append("\n\n");
      String messageLine = String.format(REGULAR_EXPRESSION_SHIPMENT_CREATED, numbers[index]);
      messageBuilder.append(messageLine);
    }
    String messageBodyRegExp = messageBuilder.toString();
    final String text = messageBox.getText();
    assertTrue(String.format("'%s' should match with '%s'.", text, messageBodyRegExp),
        text.matches(messageBodyRegExp));
  }
}
