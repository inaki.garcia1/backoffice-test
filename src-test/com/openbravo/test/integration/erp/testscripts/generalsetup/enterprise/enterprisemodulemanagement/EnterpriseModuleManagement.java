/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.enterprise.enterprisemodulemanagement;

import com.openbravo.test.integration.erp.data.generalsetup.enterprise.enterprisemodulemanagement.EnterpriseModuleManagementData;
import com.openbravo.test.integration.erp.gui.general.enterprise.enterprisemodulemanagement.EnterpriseModuleManagementForm;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the Instance Activation form of OpenbravoERP.
 *
 * @author elopio
 */
public class EnterpriseModuleManagement {

  /**
   * Apply modules to an organization.
   *
   * @param mainPage
   *          The application main page.
   * @param enterpriseModuleManagementData
   *          The enterprise module management data.
   */
  public static void applyModules(MainPage mainPage,
      EnterpriseModuleManagementData enterpriseModuleManagementData) {
    EnterpriseModuleManagementForm enterpriseModuleManagementForm = new EnterpriseModuleManagementForm();
    mainPage.openView(enterpriseModuleManagementForm);

    enterpriseModuleManagementForm.applyModules(enterpriseModuleManagementData);
    enterpriseModuleManagementForm.verifyProcessCompletedSuccessfully2();
  }

}
