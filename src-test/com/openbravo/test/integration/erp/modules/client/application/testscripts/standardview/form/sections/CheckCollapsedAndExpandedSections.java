/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.standardview.form.sections;

import org.junit.Test;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Test the collapsed and expanded sections of a form.
 *
 * @author elopio
 */
public abstract class CheckCollapsedAndExpandedSections extends OpenbravoERPTest {

  /* Data for this test. */
  /** An array with the names of the sections that should be expanded. */
  private final String[] expandedSections;
  /** An array with the names of the sections that should be collapsed. */
  private final String[] collapsedSections;

  /**
   * Class constructor.
   *
   * @param expandedSections
   *          An array with the names of the sections that should be expanded.
   * @param collapsedSections
   *          An array with the names of the sections that should be collapsed
   */
  public CheckCollapsedAndExpandedSections(String[] expandedSections, String[] collapsedSections) {
    this.expandedSections = expandedSections;
    this.collapsedSections = collapsedSections;
  }

  /**
   * Test the display of sections in the form.
   */
  @Test
  public void sectionsShouldBeDisplayedAsExpected() {
    GeneratedTab<DataObject> tab = getTab();
    tab.clickCreateNewInForm();
    for (String expandedSection : expandedSections) {
      tab.getForm().assertSectionExpanded(expandedSection);
    }
    for (String collapsedSection : collapsedSections) {
      tab.getForm().assertSectionCollapsed(collapsedSection);
    }
  }

  /**
   * Open and return the generated tab that will be used for the test.
   *
   * @return the generated tab that will be used for the test.
   */
  public abstract GeneratedTab<DataObject> getTab();

}
