/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *      Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.security.user.classic;

import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.data.generalsetup.security.user.UserData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP User tab.
 *
 * @author elopio
 *
 */
public class UserTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "User";
  /** The tab level. */
  private static final int LEVEL = 0;

  /* GUI components. */
  /** Identifier of the User tab. */
  public static final String TAB = "tabname118";

  /** Number of the name column. */
  public static final int COLUMN_NAME = 2;

  /** Identifier of the organization combo box. */
  private static final String COMBO_BOX_ORGANIZATION = "reportAD_Org_ID_S";
  /** Identifier of the first name text field. */
  private static final String TEXT_FIELD_FIRST_NAME = "Firstname";
  /** Identifier of the name text field. */
  private static final String TEXT_FIELD_NAME = "Name";
  /** Identifier of the user name text field. */
  private static final String TEXT_FIELD_USERNAME = "UserName";

  /** Identifier of the password button. */
  private static final String BUTTON_PASSWORD = "buttonPassword";

  /**
   * Class constructor
   *
   */
  public UserTab() {
    super(TITLE, LEVEL, TAB);
    addSubTab(new UserRolesTab());
  }

  /**
   * Create an user Assumes that the User menu item is selected.
   *
   * @param userData
   *          The data of the user to create.
   */
  public void create(UserData userData) throws OpenbravoERPTestException {
    clickCreateNewRecord();
    if (userData.getDataField("organization") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_ORGANIZATION))
          .selectByVisibleText(userData.getDataField("organization").toString());
    }
    if (userData.getDataField("firstName") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_FIRST_NAME)
          .sendKeys(userData.getDataField("firstName").toString());
    }
    if (userData.getDataField("name") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_NAME)
          .sendKeys(userData.getDataField("name").toString());
      // Workaround to execute the callout.
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_USERNAME).click();
    }
    if (userData.getDataField("username") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_USERNAME)
          .sendKeys(userData.getDataField("username").toString());
    }
    clickSave();
    // TODO verifications should be moved outside this class.
    verifySave();
    if (userData.getDataField("password") != null) {
      setPassword(userData.getDataField("password").toString());
    }
    // an additional waiting time is required, as Sleep.trySleep(1000);
  }

  /**
   * Set the password to a user.
   *
   * @param password
   *          password that will be set.
   */
  private void setPassword(String password) throws OpenbravoERPTestException {
    switchToFormView();
    String windowHandle = SeleniumSingleton.INSTANCE.getWindowHandle();
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_PASSWORD).click();
    final PasswordPopUp passwordPopUp = new PasswordPopUp();
    passwordPopUp.setPassword(password);

    // select the main window
    SeleniumSingleton.INSTANCE.switchTo().window(windowHandle);
    waitForFrame();
  }
}
