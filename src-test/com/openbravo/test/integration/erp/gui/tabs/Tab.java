/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Lujan <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Arun kumar <arun.kumar@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.tabs;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.selenium.Attribute;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Class that represents a Tab on OpenbravoERP.
 *
 * @author elopio
 *
 */
public abstract class Tab {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Identifier of the application frame. */
  public static final String FRAME_APPLICATION = "appFrame";

  /** The tab title. */
  private final String title;
  /** The tab level, starting from 0. */
  private final int level;
  /** The identifier of the tab. */
  private final String tab;
  /** A map of the subtabs of this tab. The key is the tab title. */
  private Map<String, Tab> subtabs;

  /** Identifier of the status icon */
  private static final String ICON_STATUS = "TabStatusIcon";
  /** Class of the status icon when it's normal (i.e., not processing). */
  private static final String CLASS_ICON_STATUS_NORMAL = "tabTitle_elements_image_normal_icon";
  // TODO: 2009-10-13-PLU: If this identifier is not used should be removed
  /** Class of the status icon when it's processing. */
  @SuppressWarnings("unused")
  private static final String CLAS_ICON_STATUS_PROCESSING = "tabTitle_elements_image_processing_icon";

  /**
   * Class constructor.
   *
   * @param title
   *          The title of the tab.
   * @param level
   *          The level of the tab.
   * @param tab
   *          The identifier of the tab.
   */
  public Tab(String title, int level, String tab) {
    this.title = title;
    this.level = level;
    this.tab = tab;
    this.subtabs = new LinkedHashMap<String, Tab>();
  }

  /**
   * Get the title of the tab.
   *
   * @return the title of the tab.
   */
  public String getTitle() {
    return title;
  }

  /**
   * Get the level of the tab.
   *
   * @return the level of the tab.
   */
  public int getLevel() {
    return level;
  }

  /**
   * Get the subtabs of this tab.
   *
   * @return the subtabs of this tab.
   *
   */
  public Map<String, Tab> getSubtabs() {
    return subtabs;
  }

  /**
   * Add a subtab.
   *
   * @param tab
   *          The subtab to add.
   */
  protected void addSubTab(Tab subTab) {
    subtabs.put(subTab.getTitle(), subTab);
  }

  /**
   * Select the window frame.
   */
  public void waitForFrame() {
    logger.trace("Waiting for the classic frame to load.");
    SeleniumSingleton.WAIT.until(wd -> {
      SeleniumSingleton.INSTANCE.switchTo().defaultContent();

      // Looking in all the top frames those one containing a frame name appFrame. This is much
      // faster directly in JavaSript than switching frames through Selenium.

      // @formatter:off
      Long idx = (Long)SeleniumSingleton.INSTANCE.executeScriptWithReturn(
            "(function (frameName) {\n"
          + "  for (let i = 0; i < frames.length; i++) {\n"
          + "    try {\n"
          + "      let f = frames[i]\n"
          + "      if (f && f[frameName]) {\n"
          + "        return i\n"
          + "      }\n"
          + "    } catch {}\n"
          + "  }\n"
          + "  return -1\n"
          + "})('" + FRAME_APPLICATION + "')");
      // @formatter:on

      logger.trace("Classic frame index {}", idx);

      if (idx.equals(-1L)) {
        logger.trace("Frame not found in JS");
        return false;
      }

      try {
        SeleniumSingleton.INSTANCE.switchTo().frame(idx.intValue());
        SeleniumSingleton.INSTANCE.switchTo().frame(FRAME_APPLICATION);
        return true;
      } catch (NoSuchFrameException exception) {
        return false;
      }
    });
  }

  /**
   * Open the tab.
   */
  public void open() {
    try {
      SeleniumSingleton.INSTANCE.findElementById(tab).click();
    } catch (StaleElementReferenceException sere) {
      logger.warn(
          "StaleElementReferenceException has been thrown. Sleeping and trying again to click");
      Sleep.trySleep(6000);
      SeleniumSingleton.INSTANCE.findElementById(tab).click();
    }
    waitForDataToLoad();
  }

  /**
   * Wait until the tab is visible on the tabset.
   */
  public void waitUntilTabIsVisible() {
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting for tab to load.");
        try {
          WebElement tabElement = SeleniumSingleton.INSTANCE.findElementById(tab);
          return tabElement.isDisplayed();
        } catch (NoSuchElementException exception) {
          return false;
        }
      }
    });
  }

  /**
   * Wait for all data requests to be completed.
   */
  public void waitForDataToLoad() {
    // XXX a static sleep to avoid possible complications.
    Sleep.trySleep(3000);
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting for the status icon.");
        try {
          SeleniumSingleton.INSTANCE.findElementById(ICON_STATUS);
        } catch (NoSuchElementException exception) {
          return false;
        } catch (StaleElementReferenceException sere) {
          return false;
        }
        return true;
      }
    });
    // wait for the status icon to indicate that all data is loaded
    Sleep
        .execSingletonActionWithFWaitAndCond(SeleniumSingleton.INSTANCE.findElementById(ICON_STATUS)
            .getAttribute(Attribute.CLASS.getName())
            .equals(CLASS_ICON_STATUS_NORMAL), StaleElementReferenceException.class, 10000);
  }

  /** Waits while processing message is shown in tab. */
  public void waitWhileProcessing() {
    Sleep.setFluentWait(100, 5_000)
        .until(wd -> "Main_Status_Processing_Container_hidden"
            .equals(SeleniumSingleton.INSTANCE.findElementById("Processing_Container")
                .getAttribute(Attribute.CLASS.getName())));
  }
}
