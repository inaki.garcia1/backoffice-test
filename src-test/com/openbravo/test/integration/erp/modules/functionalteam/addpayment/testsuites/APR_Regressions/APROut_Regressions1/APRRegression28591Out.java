/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions1;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 28591
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression28591Out extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineData;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData2;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData2;
  PurchaseInvoiceLinesData purchaseInvoiceLineData2;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData2;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData2;
  PaymentOutHeaderData paymentOutHeaderData;
  PaymentOutHeaderData paymentOutHeaderVerificationData;
  AddPaymentPopUpData addPaymentVerificationData;
  PaymentOutHeaderData paymentOutHeaderVerificationData2;
  PaymentOutLinesData paymentOutLinesVerificationData;
  PaymentOutLinesData paymentOutLinesVerificationData2;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData3;
  PaymentPlanData invoicePaymentOutPlanData;
  PaymentDetailsData invoicePaymentOutDetailsData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData4;
  PaymentPlanData invoicePaymentOutPlanData2;
  PaymentDetailsData invoicePaymentOutDetailsData2;
  PaymentOutHeaderData paymentOutHeaderVerificationData3;
  PaymentOutLinesData paymentOutLinesVerificationData3;
  PaymentOutLinesData paymentOutLinesVerificationData4;
  PaymentPlanData invoicePaymentOutPlanData3;
  PaymentDetailsData invoicePaymentOutDetailsData3;
  PaymentDetailsData invoicePaymentOutDetailsData4;
  AddPaymentPopUpData addPaymentVerificationData2;
  PaymentPlanData invoicePaymentOutPlanData4;
  PaymentDetailsData invoicePaymentOutDetailsData5;
  PaymentPlanData invoicePaymentOutPlanData5;
  PaymentDetailsData invoicePaymentOutDetailsData6;
  PaymentDetailsData invoicePaymentOutDetailsData7;
  AddPaymentPopUpData addPaymentVerificationData3;
  PaymentPlanData invoicePaymentOutPlanData6;
  PaymentDetailsData invoicePaymentOutDetailsData8;

  /**
   * Class constructor.
   *
   */
  public APRRegression28591Out(PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineData,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData2,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData2,
      PurchaseInvoiceLinesData purchaseInvoiceLineData2,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData2,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData2,
      PaymentOutHeaderData paymentOutHeaderData,
      PaymentOutHeaderData paymentOutHeaderVerificationData,
      AddPaymentPopUpData addPaymentVerificationData,
      PaymentOutHeaderData paymentOutHeaderVerificationData2,
      PaymentOutLinesData paymentOutLinesVerificationData,
      PaymentOutLinesData paymentOutLinesVerificationData2,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData3,
      PaymentPlanData invoicePaymentOutPlanData, PaymentDetailsData invoicePaymentOutDetailsData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData4,
      PaymentPlanData invoicePaymentOutPlanData2, PaymentDetailsData invoicePaymentOutDetailsData2,
      PaymentOutHeaderData paymentOutHeaderVerificationData3,
      PaymentOutLinesData paymentOutLinesVerificationData3,
      PaymentOutLinesData paymentOutLinesVerificationData4,
      PaymentPlanData invoicePaymentOutPlanData3, PaymentDetailsData invoicePaymentOutDetailsData3,
      PaymentDetailsData invoicePaymentOutDetailsData4,
      AddPaymentPopUpData addPaymentVerificationData2, PaymentPlanData invoicePaymentOutPlanData4,
      PaymentDetailsData invoicePaymentOutDetailsData5, PaymentPlanData invoicePaymentOutPlanData5,
      PaymentDetailsData invoicePaymentOutDetailsData6,
      PaymentDetailsData invoicePaymentOutDetailsData7,
      AddPaymentPopUpData addPaymentVerificationData3, PaymentPlanData invoicePaymentOutPlanData6,
      PaymentDetailsData invoicePaymentOutDetailsData8) {
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineData = purchaseInvoiceLineData;
    this.purchaseInvoiceLineVerificationData = purchaseInvoiceLineVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceHeaderData2 = purchaseInvoiceHeaderData2;
    this.purchaseInvoiceHeaderVerificationData2 = purchaseInvoiceHeaderVerificationData2;
    this.purchaseInvoiceLineData2 = purchaseInvoiceLineData2;
    this.purchaseInvoiceLineVerificationData2 = purchaseInvoiceLineVerificationData2;
    this.completedPurchaseInvoiceHeaderVerificationData2 = completedPurchaseInvoiceHeaderVerificationData2;
    this.paymentOutHeaderData = paymentOutHeaderData;
    this.paymentOutHeaderVerificationData = paymentOutHeaderVerificationData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.paymentOutHeaderVerificationData2 = paymentOutHeaderVerificationData2;
    this.paymentOutLinesVerificationData = paymentOutLinesVerificationData;
    this.paymentOutLinesVerificationData2 = paymentOutLinesVerificationData2;
    this.completedPurchaseInvoiceHeaderVerificationData3 = completedPurchaseInvoiceHeaderVerificationData3;
    this.invoicePaymentOutPlanData = invoicePaymentOutPlanData;
    this.invoicePaymentOutDetailsData = invoicePaymentOutDetailsData;
    this.completedPurchaseInvoiceHeaderVerificationData4 = completedPurchaseInvoiceHeaderVerificationData4;
    this.invoicePaymentOutPlanData2 = invoicePaymentOutPlanData2;
    this.invoicePaymentOutDetailsData2 = invoicePaymentOutDetailsData2;
    this.paymentOutHeaderVerificationData3 = paymentOutHeaderVerificationData3;
    this.paymentOutLinesVerificationData3 = paymentOutLinesVerificationData3;
    this.paymentOutLinesVerificationData4 = paymentOutLinesVerificationData4;
    this.invoicePaymentOutPlanData3 = invoicePaymentOutPlanData3;
    this.invoicePaymentOutDetailsData3 = invoicePaymentOutDetailsData3;
    this.invoicePaymentOutDetailsData4 = invoicePaymentOutDetailsData4;
    this.addPaymentVerificationData2 = addPaymentVerificationData2;
    this.invoicePaymentOutPlanData4 = invoicePaymentOutPlanData4;
    this.invoicePaymentOutDetailsData5 = invoicePaymentOutDetailsData5;
    this.invoicePaymentOutPlanData5 = invoicePaymentOutPlanData5;
    this.invoicePaymentOutDetailsData6 = invoicePaymentOutDetailsData6;
    this.invoicePaymentOutDetailsData7 = invoicePaymentOutDetailsData7;
    this.addPaymentVerificationData3 = addPaymentVerificationData3;
    this.invoicePaymentOutPlanData6 = invoicePaymentOutPlanData6;
    this.invoicePaymentOutDetailsData8 = invoicePaymentOutDetailsData8;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> purchaseOrderValues() {
    Object[][] data = new Object[][] { {
        new PurchaseInvoiceHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),
        new PurchaseInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AP Invoice")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .paymentTerms("90 days")
            .paymentMethod("1 (Spain)")
            .priceList("Purchase")
            .build(),
        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Raw material A")
                .priceListVersionName("Purchase")
                .build())
            .invoicedQuantity("10")
            .build(),
        new PurchaseInvoiceLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("20.00")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("22.00")
            .summedLineAmount("20.00")
            .currency("EUR")
            .paymentComplete(false)
            .totalPaid("0.00")
            .outstandingAmount("22.00")
            .dueAmount("0.00")
            .build(),

        new PurchaseInvoiceHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),
        new PurchaseInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AP Invoice")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .paymentTerms("90 days")
            .paymentMethod("1 (Spain)")
            .priceList("Purchase")
            .build(),
        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Raw material B")
                .priceListVersionName("Purchase")
                .build())
            .invoicedQuantity("20")
            .build(),
        new PurchaseInvoiceLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("40.00")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("44.00")
            .summedLineAmount("40.00")
            .currency("EUR")
            .paymentComplete(false)
            .totalPaid("0.00")
            .outstandingAmount("44.00")
            .dueAmount("0.00")
            .build(),

        new PaymentOutHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),
        new PaymentOutHeaderData.Builder().organization("Spain")
            .documentType("AP Payment")
            .paymentMethod("1 (Spain)")
            .account("Spain Cashbook - EUR")
            .currency("EUR")
            .generatedCredit("0.00")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Vendor A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Cashbook - EUR")
            .c_currency_id("EUR")
            .actual_payment("66.00")
            .expected_payment("66.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("66.00")
            .total("66.00")
            .difference("0.00")
            .build(),
        new PaymentOutHeaderData.Builder().status("Withdrawn not Cleared")
            .amount("66.00")
            .usedCredit("0.00")
            .build(),
        new PaymentOutLinesData.Builder().dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .invoiceAmount("22.00")
            .expected("22.00")
            .paid("22.00")
            .orderno("")
            .glitemname("")
            .build(),
        new PaymentOutLinesData.Builder().dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .invoiceAmount("44.00")
            .expected("44.00")
            .paid("44.00")
            .orderno("")
            .glitemname("")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("22.00")
            .summedLineAmount("20.00")
            .currency("EUR")
            .paymentComplete(true)
            .build(),
        new PaymentPlanData.Builder().expectedDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("1 (Spain)")
            .expected("22.00")
            .paid("22.00")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),
        new PaymentDetailsData.Builder().paid("22.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("1 (Spain)")
            .amount("22.00")
            .account("Spain Cashbook - EUR")
            .status("Withdrawn not Cleared")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("44.00")
            .summedLineAmount("40.00")
            .currency("EUR")
            .paymentComplete(true)
            .build(),
        new PaymentPlanData.Builder().expectedDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("1 (Spain)")
            .expected("44.00")
            .paid("44.00")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),
        new PaymentDetailsData.Builder().paid("44.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("1 (Spain)")
            .amount("44.00")
            .account("Spain Cashbook - EUR")
            .status("Withdrawn not Cleared")
            .build(),

        new PaymentOutHeaderData.Builder().status("Payment Made")
            .amount("-66.00")
            .usedCredit("0.00")
            .organization("Spain")
            .documentType("AP Payment")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("1 (Spain)")
            .generatedCredit("0.00")
            .account("Spain Cashbook - EUR")
            .currency("EUR")
            .build(),
        new PaymentOutLinesData.Builder().dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .invoiceAmount("22.00")
            .expected("22.00")
            .paid("-22.00")
            .orderno("")
            .glitemname("")
            .build(),
        new PaymentOutLinesData.Builder().dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .invoiceAmount("44.00")
            .expected("44.00")
            .paid("-44.00")
            .orderno("")
            .glitemname("")
            .build(),

        new PaymentPlanData.Builder().expectedDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("1 (Spain)")
            .expected("22.00")
            .paid("0.00")
            .outstanding("22.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("2")
            .build(),
        new PaymentDetailsData.Builder().paid("22.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("1 (Spain)")
            .amount("22.00")
            .account("Spain Cashbook - EUR")
            .status("Withdrawn not Cleared")
            .build(),
        new PaymentDetailsData.Builder().paid("-22.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("1 (Spain)")
            .amount("22.00")
            .account("Spain Cashbook - EUR")
            .status("Payment Made")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Vendor A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Cashbook - EUR")
            .c_currency_id("EUR")
            .actual_payment("22.00")
            .expected_payment("22.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("22.00")
            .total("22.00")
            .difference("0.00")
            .build(),
        new PaymentPlanData.Builder().expectedDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("1 (Spain)")
            .expected("22.00")
            .paid("22.00")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("3")
            .build(),
        new PaymentDetailsData.Builder().paid("22.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("1 (Spain)")
            .amount("22.00")
            .account("Spain Cashbook - EUR")
            .status("Withdrawn not Cleared")
            .build(),

        new PaymentPlanData.Builder().expectedDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("1 (Spain)")
            .expected("44.00")
            .paid("0.00")
            .outstanding("44.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("2")
            .build(),
        new PaymentDetailsData.Builder().paid("44.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("1 (Spain)")
            .amount("44.00")
            .account("Spain Cashbook - EUR")
            .status("Withdrawn not Cleared")
            .build(),
        new PaymentDetailsData.Builder().paid("-44.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("1 (Spain)")
            .amount("44.00")
            .account("Spain Cashbook - EUR")
            .status("Payment Made")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Vendor A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Cashbook - EUR")
            .c_currency_id("EUR")
            .actual_payment("44.00")
            .expected_payment("44.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("44.00")
            .total("44.00")
            .difference("0.00")
            .build(),
        new PaymentPlanData.Builder().expectedDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("1 (Spain)")
            .expected("44.00")
            .paid("44.00")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("3")
            .build(),
        new PaymentDetailsData.Builder().paid("44.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("1 (Spain)")
            .amount("44.00")
            .account("Spain Cashbook - EUR")
            .status("Withdrawn not Cleared")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 28591 - Payment Out flow
   *
   * Payment from Order
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression28591OutTest() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression28591Out] Test regression 28591 - Payment Out flow. **");

    PurchaseInvoice purchaseInvoice1 = new PurchaseInvoice(mainPage).open();
    purchaseInvoice1.create(purchaseInvoiceHeaderData);
    purchaseInvoice1.assertSaved();
    purchaseInvoice1.assertData(purchaseInvoiceHeaderVerificationData);
    String invoiceNo1 = purchaseInvoice1.getData("documentNo").toString();
    PurchaseInvoice.Lines purchaseInvoiceLines1 = purchaseInvoice1.new Lines(mainPage);
    purchaseInvoiceLines1.create(purchaseInvoiceLineData);
    purchaseInvoiceLines1.assertSaved();
    purchaseInvoiceLines1.assertData(purchaseInvoiceLineVerificationData);
    purchaseInvoice1.complete();
    purchaseInvoice1.assertProcessCompletedSuccessfully2();
    purchaseInvoice1.assertData(completedPurchaseInvoiceHeaderVerificationData);

    PurchaseInvoice purchaseInvoice2 = new PurchaseInvoice(mainPage).open();
    purchaseInvoice2.create(purchaseInvoiceHeaderData2);
    purchaseInvoice2.assertSaved();
    purchaseInvoice2.assertData(purchaseInvoiceHeaderVerificationData2);
    String invoiceNo2 = purchaseInvoice2.getData("documentNo").toString();
    PurchaseInvoice.Lines purchaseInvoiceLines2 = purchaseInvoice2.new Lines(mainPage);
    purchaseInvoiceLines2.create(purchaseInvoiceLineData2);
    purchaseInvoiceLines2.assertSaved();
    purchaseInvoiceLines2.assertData(purchaseInvoiceLineVerificationData2);
    purchaseInvoice2.complete();
    purchaseInvoice2.assertProcessCompletedSuccessfully2();
    purchaseInvoice2.assertData(completedPurchaseInvoiceHeaderVerificationData2);

    PaymentOut paymentOut1 = new PaymentOut(mainPage).open();
    paymentOut1.create(paymentOutHeaderData);
    paymentOut1.assertSaved();
    paymentOut1.assertData(paymentOutHeaderVerificationData);
    String invoiceNo = invoiceNo1 + " or " + invoiceNo2;
    AddPaymentProcess addPaymentProcess = paymentOut1.addDetailsOpen();
    String payment1No = addPaymentProcess.getParameterValue("payment_documentno").toString();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.getOrderInvoiceGrid().unselectAll();
    addPaymentProcess.getOrderInvoiceGrid().filter("invoiceNo", invoiceNo);
    Sleep.trySleep(1000);
    addPaymentProcess.getOrderInvoiceGrid().selectRecord(0);
    Sleep.trySleep(1000);
    addPaymentProcess.getOrderInvoiceGrid().selectRecord(1);
    Sleep.trySleep(1000);
    addPaymentProcess.getField("reference_no").focus();
    addPaymentProcess.process("Process Made Payment(s) and Withdrawal", addPaymentVerificationData);
    paymentOut1.assertPaymentCreatedSuccessfully();
    paymentOut1.assertData(paymentOutHeaderVerificationData2);
    PaymentOut.Lines paymentOutLines1 = paymentOut1.new Lines(mainPage);
    paymentOutLines1.assertCount(2);
    paymentOutLines1.select(new PaymentOutLinesData.Builder().invoiceno(invoiceNo1).build());
    paymentOutLines1.assertData((PaymentOutLinesData) paymentOutLinesVerificationData
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo1));
    paymentOutLines1.select(new PaymentOutLinesData.Builder().invoiceno(invoiceNo2).build());
    paymentOutLines1.assertData((PaymentOutLinesData) paymentOutLinesVerificationData2
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo2));

    purchaseInvoice1.open();
    Sleep.trySleep(20000);
    purchaseInvoice1.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoiceNo1).build());
    purchaseInvoice1.assertData(completedPurchaseInvoiceHeaderVerificationData3);
    PurchaseInvoice.PaymentOutPlan invoicePaymentOutPlan1 = purchaseInvoice1.new PaymentOutPlan(
        mainPage);
    invoicePaymentOutPlan1.assertCount(1);
    invoicePaymentOutPlan1.assertData(invoicePaymentOutPlanData);
    PurchaseInvoice.PaymentOutPlan.PaymentOutDetails invoicePaymentOutDetails1 = invoicePaymentOutPlan1.new PaymentOutDetails(
        mainPage);
    invoicePaymentOutDetails1.assertCount(1);
    invoicePaymentOutDetails1.assertData(invoicePaymentOutDetailsData);

    purchaseInvoice2.open();
    Sleep.trySleep(20000);
    purchaseInvoice2.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoiceNo2).build());
    purchaseInvoice2.assertData(completedPurchaseInvoiceHeaderVerificationData4);
    PurchaseInvoice.PaymentOutPlan invoicePaymentOutPlan2 = purchaseInvoice1.new PaymentOutPlan(
        mainPage);
    invoicePaymentOutPlan2.assertCount(1);
    invoicePaymentOutPlan2.assertData(invoicePaymentOutPlanData2);
    PurchaseInvoice.PaymentOutPlan.PaymentOutDetails invoicePaymentOutDetails2 = invoicePaymentOutPlan2.new PaymentOutDetails(
        mainPage);
    invoicePaymentOutDetails2.assertCount(1);
    invoicePaymentOutDetails2.assertData(invoicePaymentOutDetailsData2);

    paymentOut1.open();
    Sleep.trySleep(20000);
    paymentOut1.select(new PaymentOutHeaderData.Builder().documentNo(payment1No).build());
    paymentOut1.reverse("Reverse Payment", OBDate.getSystemDate());
    paymentOut1.assertProcessCompletedSuccessfully2();

    String payment2No = "*R*" + String.valueOf(Integer.valueOf(payment1No) + 1);
    PaymentOut paymentOut2 = new PaymentOut(mainPage).open();
    paymentOut2.getTab().refresh();
    paymentOut2.select(new PaymentOutHeaderData.Builder().documentNo(payment2No).build());//
    paymentOut2.assertSaved();
    paymentOut2.assertData(paymentOutHeaderVerificationData3);
    PaymentOut.Lines paymentOutLines2 = paymentOut1.new Lines(mainPage);
    paymentOutLines2.assertCount(2);
    paymentOutLines2.select(new PaymentOutLinesData.Builder().invoiceno(invoiceNo1).build());
    paymentOutLines2.assertData((PaymentOutLinesData) paymentOutLinesVerificationData3
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo1));
    paymentOutLines2.select(new PaymentOutLinesData.Builder().invoiceno(invoiceNo2).build());
    paymentOutLines2.assertData((PaymentOutLinesData) paymentOutLinesVerificationData4
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo2));

    purchaseInvoice1.open();
    Sleep.trySleep(20000);
    purchaseInvoice1.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoiceNo1).build());
    purchaseInvoice1.assertData(completedPurchaseInvoiceHeaderVerificationData);
    invoicePaymentOutPlan1 = purchaseInvoice1.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan1.assertCount(1);
    invoicePaymentOutPlan1.assertData(invoicePaymentOutPlanData3);
    invoicePaymentOutDetails1 = invoicePaymentOutPlan1.new PaymentOutDetails(mainPage);
    invoicePaymentOutDetails1.filter(new PaymentDetailsData.Builder().documentNo("").build());
    Sleep.trySleep(1000);
    invoicePaymentOutDetails1.assertCount(2);
    invoicePaymentOutDetails1
        .select(new PaymentDetailsData.Builder().documentNo(payment1No).build());
    invoicePaymentOutDetails1.assertData(invoicePaymentOutDetailsData3);
    invoicePaymentOutDetails1
        .select(new PaymentDetailsData.Builder().documentNo(payment2No).build());
    invoicePaymentOutDetails1.assertData(invoicePaymentOutDetailsData4);

    AddPaymentProcess addPaymentProcess1 = purchaseInvoice1.openAddPayment();
    String payment3No = addPaymentProcess1.getParameterValue("payment_documentno").toString();
    payment3No = payment3No.substring(1, payment3No.length() - 1);
    addPaymentProcess1.assertData(addPaymentVerificationData2);
    addPaymentProcess1.process("Process Made Payment(s) and Withdrawal");
    purchaseInvoice1.assertPaymentCreatedSuccessfully();
    purchaseInvoice1.assertData(completedPurchaseInvoiceHeaderVerificationData3);
    invoicePaymentOutPlan1 = purchaseInvoice1.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan1.assertCount(1);
    invoicePaymentOutPlan1.assertData(invoicePaymentOutPlanData4);
    invoicePaymentOutDetails1 = invoicePaymentOutPlan1.new PaymentOutDetails(mainPage);
    invoicePaymentOutDetails1.filter(new PaymentDetailsData.Builder().documentNo("").build());
    Sleep.trySleep(1000);
    invoicePaymentOutDetails1.assertCount(3);
    invoicePaymentOutDetails1
        .select(new PaymentDetailsData.Builder().documentNo(payment1No).build());
    invoicePaymentOutDetails1.assertData(invoicePaymentOutDetailsData3);
    invoicePaymentOutDetails1
        .select(new PaymentDetailsData.Builder().documentNo(payment2No).build());
    invoicePaymentOutDetails1.assertData(invoicePaymentOutDetailsData4);
    invoicePaymentOutDetails1
        .select(new PaymentDetailsData.Builder().documentNo(payment3No).build());
    invoicePaymentOutDetails1.assertData(invoicePaymentOutDetailsData5);

    purchaseInvoice2.open();
    Sleep.trySleep(20000);
    purchaseInvoice2.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoiceNo2).build());
    purchaseInvoice2.assertData(completedPurchaseInvoiceHeaderVerificationData2);
    invoicePaymentOutPlan2 = purchaseInvoice2.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan2.assertCount(1);
    invoicePaymentOutPlan2.assertData(invoicePaymentOutPlanData5);
    invoicePaymentOutDetails2 = invoicePaymentOutPlan2.new PaymentOutDetails(mainPage);
    invoicePaymentOutDetails2.filter(new PaymentDetailsData.Builder().documentNo("").build());
    Sleep.trySleep(1000);
    invoicePaymentOutDetails2.assertCount(2);
    invoicePaymentOutDetails2
        .select(new PaymentDetailsData.Builder().documentNo(payment1No).build());
    invoicePaymentOutDetails2.assertData(invoicePaymentOutDetailsData6);
    invoicePaymentOutDetails2
        .select(new PaymentDetailsData.Builder().documentNo(payment2No).build());
    invoicePaymentOutDetails2.assertData(invoicePaymentOutDetailsData7);

    AddPaymentProcess addPaymentProcess2 = purchaseInvoice2.openAddPayment();
    String payment4No = addPaymentProcess2.getParameterValue("payment_documentno").toString();
    payment4No = payment4No.substring(1, payment4No.length() - 1);
    addPaymentProcess2.assertData(addPaymentVerificationData3);
    addPaymentProcess2.process("Process Made Payment(s) and Withdrawal");
    purchaseInvoice2.assertPaymentCreatedSuccessfully();
    purchaseInvoice2.assertData(completedPurchaseInvoiceHeaderVerificationData4);
    invoicePaymentOutPlan2 = purchaseInvoice2.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan2.assertCount(1);
    invoicePaymentOutPlan2.assertData(invoicePaymentOutPlanData6);
    invoicePaymentOutDetails2 = invoicePaymentOutPlan2.new PaymentOutDetails(mainPage);
    invoicePaymentOutDetails2.filter(new PaymentDetailsData.Builder().documentNo("").build());
    Sleep.trySleep(1000);
    invoicePaymentOutDetails2.assertCount(3);
    invoicePaymentOutDetails2
        .select(new PaymentDetailsData.Builder().documentNo(payment1No).build());
    invoicePaymentOutDetails2.assertData(invoicePaymentOutDetailsData6);
    invoicePaymentOutDetails2
        .select(new PaymentDetailsData.Builder().documentNo(payment2No).build());
    invoicePaymentOutDetails2.assertData(invoicePaymentOutDetailsData7);
    invoicePaymentOutDetails2
        .select(new PaymentDetailsData.Builder().documentNo(payment4No).build());
    invoicePaymentOutDetails2.assertData(invoicePaymentOutDetailsData8);

    logger.info(
        "** End of test case [APRRegression28591Out] Test regression 28591 - Payment Out flow. **");
  }
}
