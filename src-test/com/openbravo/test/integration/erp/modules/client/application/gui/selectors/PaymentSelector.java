/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 *   Nono Carballo <nonofce@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui.selectors;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.selectors.PaymentSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.AddPaymentSelectorField;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.AddPaymentSelectorItem;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.selenium.Sleep;

/**
 *
 * Executes actions on OpenbravoERP organization selector.
 *
 * @author lorenzo.fidalgo.
 *
 */
public class PaymentSelector extends AddPaymentSelectorField {

  /**
   * Class constructor.
   *
   * @param addPaymentSelectorItem
   *          The selector item of this selector.
   */
  public PaymentSelector(AddPaymentSelectorItem addPaymentSelectorItem) {
    super(addPaymentSelectorItem);
  }

  @Override
  public void setValue(Object value) {

    /* Constants to assert */
    /* Groups to assert */
    final String FIELD_GROUP_GLITEM = "7B6B5F5475634E35A85CF7023165E50B";
    final String FIELD_GROUP_ORDER = "0C672A3B7CDF416F9522DF3FA5AE4022";
    final String FIELD_GROUP_CREDIT = "CB265F2D7ACF439F9FB5EFBFA0B50363";
    final String FIELD_GROUP_TOTALS = "BFFF70E721654110AD5BACF3D4216D3A";
    /* Fields to assert */
    final String PARAM_PYMT_DOCUMENTNO = "payment_documentno";
    final String PARAM_BP_FROM = "received_from";
    final String PARAM_REFERENCENO = "reference_no";
    final String PARAM_PYMT_METHOD = "fin_paymentmethod_id";
    final String PARAM_CURRENCY = "c_currency_id";
    final String PARAM_CURRENCYTO = "c_currency_to_id";
    final String PARAM_ACTUAL_PAYMENT = "actual_payment";
    final String PARAM_EXPECTED_PAYMENT = "expected_payment";
    final String PARAM_FIN_ACCT = "fin_financial_account_id";
    final String PARAM_CONV_AMT = "converted_amount";
    final String PARAM_CONV_RATE = "conversion_rate";
    final String PARAM_TRX_TYPE = "transaction_type";
    final String PARAM_AMTGLITEMS = "amount_gl_items";
    final String PARAM_ORDER_INV = "amount_inv_ords";
    final String PARAM_TOTAL = "total";
    final String PARAM_DIFFERENCE = "difference";
    final String PARAM_DOCACTION = "document_action";
    final String PARAM_OVERPYMT_ACTION = "overpayment_action";

    final String visibleFields[] = { PARAM_PYMT_DOCUMENTNO, PARAM_BP_FROM, PARAM_REFERENCENO,
        PARAM_PYMT_METHOD, PARAM_EXPECTED_PAYMENT, PARAM_FIN_ACCT, PARAM_CURRENCY, PARAM_TRX_TYPE,
        PARAM_AMTGLITEMS, PARAM_ORDER_INV, PARAM_TOTAL, PARAM_DIFFERENCE, PARAM_DOCACTION, };
    final String enabledFields[] = { PARAM_PYMT_DOCUMENTNO, PARAM_BP_FROM, PARAM_REFERENCENO,
        PARAM_PYMT_METHOD, PARAM_CONV_AMT, PARAM_CONV_RATE, PARAM_TRX_TYPE, PARAM_DOCACTION,
        PARAM_OVERPYMT_ACTION };
    final String notVisibleFields[] = { PARAM_CURRENCYTO, PARAM_CONV_AMT, PARAM_CONV_RATE,
        PARAM_OVERPYMT_ACTION };
    final String notEnabledFields[] = { PARAM_CURRENCYTO, PARAM_EXPECTED_PAYMENT, PARAM_FIN_ACCT,
        PARAM_CURRENCY, PARAM_AMTGLITEMS, PARAM_ORDER_INV, PARAM_TOTAL, PARAM_DIFFERENCE };

    /* End constants to assert */

    PaymentSelectorData paymentSelectorData = (PaymentSelectorData) value;
    if (paymentSelectorData.getDataField("documentNo") != null) {
      super.setValue(value);
    } else {
      FinancialAccount.Transaction transactions = (FinancialAccount.Transaction) paymentSelectorData
          .getDataField("transactions");
      PaymentSelector paymentSelector = (PaymentSelector) transactions.getTab()
          .getForm()
          .getField("finPayment");

      paymentSelector.openPopUpToProcess();
      AddPaymentProcess addPaymentProcess = new AddPaymentProcess(
          (GeneratedTab<?>) paymentSelectorData.getDataField("currentTab"));

      for (int i = 0; i < 20
          && !addPaymentProcess.getForm().getSection("trxtype").isVisible(); i++) {
        Sleep.trySleep(500);
      }
      addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();

      /* Elemental FINe asserts */
      /* Section asserts */
      assertTrue("FIELD_GROUP_ORDER is not visible",
          addPaymentProcess.getForm().getSection(FIELD_GROUP_ORDER).isVisible());
      assertTrue("FIELD_GROUP_ORDER is not expanded",
          addPaymentProcess.getForm().getSection(FIELD_GROUP_ORDER).isExpanded());
      assertTrue("FIELD_GROUP_GLITEM is not visible",
          addPaymentProcess.getForm().getSection(FIELD_GROUP_GLITEM).isVisible());
      assertFalse("FIELD_GROUP_GLITEM is expanded, it shouldn't",
          addPaymentProcess.getForm().getSection(FIELD_GROUP_GLITEM).isExpanded());
      assertFalse("FIELD_GROUP_CREDIT is visible",
          addPaymentProcess.getForm().getSection(FIELD_GROUP_CREDIT).isVisible());
      assertTrue("FIELD_GROUP_CREDIT is not expanded",
          addPaymentProcess.getForm().getSection(FIELD_GROUP_CREDIT).isExpanded());
      assertTrue("FIELD_GROUP_TOTALS is not visible",
          addPaymentProcess.getForm().getSection(FIELD_GROUP_TOTALS).isVisible());
      assertTrue("FIELD_GROUP_TOTALS is not expanded",
          addPaymentProcess.getForm().getSection(FIELD_GROUP_TOTALS).isExpanded());
      /* End section asserts */
      /* Field asserts */

      for (final String key : visibleFields) {
        assertTrue(key + " is not visible",
            addPaymentProcess.getForm().getSection(key).isVisible());
      }
      for (final String key : enabledFields) {
        assertTrue(key + " is not enabled",
            addPaymentProcess.getForm().getSection(key).isEnabled());
      }
      BigDecimal actualPayment = new BigDecimal(
          ((String) addPaymentProcess.getForm().getData("actual_payment")).replace(",", ""));
      for (final String key : notVisibleFields) {
        /* Skips PARAM_OVERPYMT_ACTION due to it is visible when actual_payment <> 0.00 */
        if (key.equals(PARAM_OVERPYMT_ACTION) && actualPayment.compareTo(BigDecimal.ZERO) == 1) {
          continue;
        }
        assertFalse(key + " is visible. It shouldn't",
            addPaymentProcess.getForm().getSection(key).isVisible());
      }
      for (final String key : notEnabledFields) {
        assertFalse(key + " is enabled. It should't",
            addPaymentProcess.getForm().getSection(key).isEnabled());
      }
      Sleep.trySleep(1200);
      addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
      Sleep.trySleep(500);
      if (addPaymentProcess.getForm().getField("trxtype").getValue().equals("Received In")) {
        assertTrue(addPaymentProcess.getField(PARAM_ACTUAL_PAYMENT).isVisible());
        assertTrue(addPaymentProcess.getField(PARAM_ACTUAL_PAYMENT).isEnabled());
      } else {
        assertTrue(addPaymentProcess.getField(PARAM_ACTUAL_PAYMENT).isVisible());
        assertFalse(addPaymentProcess.getField(PARAM_ACTUAL_PAYMENT).isEnabled());
      }

      /**
       * Asserts the initial values of Payment
       */
      if (paymentSelectorData.getDataField("initialPayment") != null) {
        addPaymentProcess
            .assertData((AddPaymentPopUpData) paymentSelectorData.getDataField("initialPayment"));
      }

      /* End elemental FINe asserts */

      Sleep.trySleep(800);
      // Set paymentMethod field as paymentMethod 1
      if (paymentSelectorData.getDataField("addEditPayment") != null) {
        AddPaymentPopUpData APDToEditPaymentMethod = (AddPaymentPopUpData) paymentSelectorData
            .getDataField("addEditPayment");
        if ((APDToEditPaymentMethod).getDataFields().get("fin_paymentmethod_id") != null) {
          addPaymentProcess.setParameterValue("fin_paymentmethod_id",
              (APDToEditPaymentMethod).getDataFields().get("fin_paymentmethod_id"));
        }
      }
      addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
      addPaymentProcess.assertExistRecordsInOrderInvoice();
      addPaymentProcess.getOrderInvoiceGrid().clearFilters();
      Sleep.trySleep(500);
      addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
      if (actualPayment.compareTo(BigDecimal.ZERO) == 1) {
        addPaymentProcess.getOrderInvoiceGrid().unselectAll();
      }
      addPaymentProcess.getOrderInvoiceGrid()
          .filter("invoiceNo", paymentSelectorData.getDataField("invoiceNo").toString());

      /* Field asserts & addEditPayment */
      if (paymentSelectorData.getDataField("addEditPayment") != null) {
        AddPaymentPopUpData editAPD = (AddPaymentPopUpData) paymentSelectorData
            .getDataField("addEditPayment");
        for (final String key : (editAPD).getDataFields().keySet()) {
          assertTrue(addPaymentProcess.getField(key) + " field is not visible.",
              addPaymentProcess.getField(key).isVisible());
          assertTrue(addPaymentProcess.getField(key) + " field is not enabled.",
              addPaymentProcess.getField(key).isEnabled());
          addPaymentProcess.setParameterValue(key, (editAPD).getDataFields().get(key));
        }
      }
      /* End field asserts */

      for (int i = 0; i < 20 && addPaymentProcess.NumberOfRowsInOrderInvoiceGrid() != 1; i++) {
        Sleep.trySleep(500);
      }
      addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
      if (paymentSelectorData.getDataField("addEditPayment") != null) {
        addPaymentProcess.assertRecordsNumberInOrderInvoice(1);
      }
      addPaymentProcess.editOrderInvoiceRecord(0,
          new SelectedPaymentData.Builder()
              .amount(paymentSelectorData.getDataField("amount").toString())
              .build());
      addPaymentProcess.getField("reference_no").focus();
      addPaymentProcess
          .assertTotalsData((AddPaymentPopUpData) paymentSelectorData.getDataField("addPayment"));
      /* Section asserts */
      assertTrue("FIELD_GROUP_ORDER is not visible",
          addPaymentProcess.getForm().getSection(FIELD_GROUP_ORDER).isVisible());
      assertTrue("FIELD_GROUP_ORDER is not expanded",
          addPaymentProcess.getForm().getSection(FIELD_GROUP_ORDER).isExpanded());
      assertTrue("FIELD_GROUP_GLITEM is not visible",
          addPaymentProcess.getForm().getSection(FIELD_GROUP_GLITEM).isVisible());
      assertFalse("FIELD_GROUP_GLITEM is expanded, it shouldn't",
          addPaymentProcess.getForm().getSection(FIELD_GROUP_GLITEM).isExpanded());
      assertFalse("FIELD_GROUP_CREDIT is visible",
          addPaymentProcess.getForm().getSection(FIELD_GROUP_CREDIT).isVisible());
      assertTrue("FIELD_GROUP_CREDIT is not expanded",
          addPaymentProcess.getForm().getSection(FIELD_GROUP_CREDIT).isExpanded());
      assertTrue("FIELD_GROUP_TOTALS is not visible",
          addPaymentProcess.getForm().getSection(FIELD_GROUP_TOTALS).isVisible());
      assertTrue("FIELD_GROUP_TOTALS is not expanded",
          addPaymentProcess.getForm().getSection(FIELD_GROUP_TOTALS).isExpanded());
      /* End section asserts */

      if (paymentSelectorData.getDataField("addEditPayment") != null
          && ((AddPaymentPopUpData) paymentSelectorData.getDataField("addEditPayment"))
              .getDataFields()
              .get("actual_payment") != null) {
        addPaymentProcess.setParameterValue("actual_payment",
            ((AddPaymentPopUpData) paymentSelectorData.getDataField("addEditPayment"))
                .getDataFields()
                .get("actual_payment"));
      }

      addPaymentProcess.getField("reference_no").focus();
      Sleep.trySleep(400);
      addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
      addPaymentProcess.assertExistRecordsInOrderInvoice();
      addPaymentProcess
          .assertTotalsData((AddPaymentPopUpData) paymentSelectorData.getDataField("addPayment"));

      /**
       * Asserts the final values of Payment
       */
      if (paymentSelectorData.getDataField("finalPayment") != null) {
        addPaymentProcess
            .assertData((AddPaymentPopUpData) paymentSelectorData.getDataField("finalPayment"));
      }

      if (addPaymentProcess.getForm().getField("trxtype").getValue().equals("Received In")) {
        addPaymentProcess.process("Process Received Payment(s)");
      } else {
        addPaymentProcess.process("Process Made Payment(s)");
      }
    }
    for (int i = 0; i < 20 && (this.isEmpty()); i++) {
      Sleep.trySleep(150);
    }

  }
}
