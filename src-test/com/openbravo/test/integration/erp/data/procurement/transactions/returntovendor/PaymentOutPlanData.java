/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016-2017 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * Contributor(s):
 *************************************************************************
 */
package com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 * Payment Out Plan Data
 *
 * @author nonofce
 *
 */
public class PaymentOutPlanData extends DataObject {

  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the expected value
     *
     * Description: Expected Amount value
     *
     * @param value
     *          The expected value.
     * @return The builder for this class.
     */
    public Builder expected(String value) {
      this.dataFields.put("expected", value);
      return this;
    }

    /**
     * Set the numberOfPayments value
     *
     * Description: Number of Payments
     *
     * @param value
     *          The numberOfPayments value.
     * @return The builder for this class.
     */
    public Builder numberOfPayments(String value) {
      this.dataFields.put("numberOfPayments", value);
      return this;
    }

    /**
     * Set the paidAmount value
     *
     * Description: Paid Amount
     *
     * @param value
     *          The paid value.
     * @return The builder for this class.
     */
    public Builder paid(String value) {
      this.dataFields.put("received", value);
      return this;
    }

    /**
     * Set the outstandingAmount value
     *
     * Description: Outstanding Amount
     *
     * @param value
     *          The outstanding value.
     * @return The builder for this class.
     */
    public Builder outstanding(String value) {
      this.dataFields.put("outstanding", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PaymentOutPlanData build() {
      return new PaymentOutPlanData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PaymentOutPlanData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
