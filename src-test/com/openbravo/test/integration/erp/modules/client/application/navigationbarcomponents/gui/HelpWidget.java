/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Rowinson Gallego <rwn.gallego@gmail.com>,
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriverException;

import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes actions on OpenbravoERP help widget built upon SmartClient user interface library.
 *
 * @author rwngallego
 *
 */
public class HelpWidget {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Registry keys. */
  /** Registry key of the help widget */
  private static final String REGISTRY_KEY_BUTTON_HELP = "org.openbravo.client.application.HelpAboutWidget";
  /** Registry key of the help link */
  private static final String REGISTRY_KEY_LINK_HELP = "org.openbravo.client.application.HelpAbout.HelpLink";
  /** Identifier of the about link */
  private static final String REGISTRY_KEY_LINK_ABOUT = "org.openbravo.client.application.HelpAbout.AboutLink";

  /* Component elements. */
  /** The help button. */
  private Button buttonHelp;
  /** The help link. */
  private Button linkHelp;
  /** The about link. */
  private Button linkAbout;

  /**
   * Class constructor.
   *
   */
  public HelpWidget() {
    buttonHelp = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_HELP));
  }

  /**
   * Check if the widget is open.
   *
   * @return true if the widget is open. Otherwise, false.
   */
  public boolean isOpen() {
    // XXX it would be better to use the parent component.
    Boolean isNotNull = linkAbout != null;
    Boolean isPresent = false;
    Boolean isVisible = false;
    if (isNotNull) {
      logger.debug("About link is not null");
      isPresent = linkAbout.isPresent();
      if (isPresent) {
        logger.debug("About link is present");
      }
      isVisible = linkAbout.isVisible();
      if (isVisible) {
        logger.debug("About link is visible");
      }
    }
    logger.debug("Return: {} , {} , {}", isNotNull, isPresent, isVisible);
    return isNotNull && isPresent && isVisible;
  }

  /**
   * Open the help widget by clicking it and create its internals components.
   */
  public void open() {
    if (!isOpen()) {
      logger.info("Opening Help widget.");
      buttonHelp.smartClientClick();
      linkAbout = new Button(TestRegistry.getObjectString(REGISTRY_KEY_LINK_ABOUT));
      try {
        linkHelp = new Button(TestRegistry.getObjectString(REGISTRY_KEY_LINK_HELP));
      } catch (Exception e) {
        // In case the help link was not shown a timeout will happen
        linkHelp = null;
      }
    }
  }

  /**
   * Open the about pop up.
   */
  public void openAbout() {
    logger.info("Open About view.");
    try {
      Sleep.smartWaitButtonVisible(REGISTRY_KEY_LINK_ABOUT, 200);
    } catch (WebDriverException wde) {
      Sleep.trySleep(1000);
    }
    linkAbout.smartClientClick();
  }

  /**
   * Open the help pop up.
   */
  public void openHelp() {
    logger.info("Open Help view.");
    try {
      Sleep.smartWaitButtonVisible(REGISTRY_KEY_LINK_HELP, 200);
    } catch (WebDriverException wde) {
      Sleep.trySleep(1000);
    }
    linkHelp.smartClientClick();
  }

  /**
   * Close the widget.
   */
  public void close() {
    if (isOpen()) {
      logger.info("Closing Help widget.");
      try {
        Sleep.smartWaitButtonVisible(REGISTRY_KEY_BUTTON_HELP, 200);
      } catch (WebDriverException wde) {
        Sleep.trySleep(1300);
      }
      buttonHelp.smartClientClick();
      linkAbout = null;
      linkHelp = null;
    }
  }

  /**
   * Check if the help link is visible.
   *
   * @return true if the help link is visible. Otherwise, false.
   */
  public boolean isHelpLinkVisbile() {
    /*
     * // There is a weird behavior with the help link. It always says that is visible. But, if it
     * is // not visible and you try to get its SmartClient locator, then it will throw an
     * exception. try { // Do any action on the link to get the SmartClient locator. String text =
     * linkHelp.getTitle(); logger.debug("Current Help link text is: " + text); if
     * (text.equals("Help")) { return true; } } catch (WebDriverException exception) { if
     * (exception.getMessage().startsWith("_23 is null") || exception.getMessage().startsWith(
     * "TypeError: _23 is null") || exception.getMessage().startsWith("null")) { logger.debug(
     * "Help link is not visible."); return false; } else if (exception.getMessage().startsWith(
     * "_6 is null") || exception.getMessage().startsWith("TypeError: _6 is null") ||
     * exception.getMessage().startsWith("null")) { logger.debug("Help link is not visible.");
     * return false; } else { // Unknown exception. throw exception; } } return linkHelp.isVisible()
     * && linkHelp.getTitle().equals("Help");
     */
    return linkHelp != null;
  }

  /**
   * Verify that the help link element is visible.
   */
  public void verifyHelpLinkVisible() {
    assertTrue("The help link is not present in the help widget.",
        linkHelp != null && isHelpLinkVisbile());
  }

  /**
   * Verify that the help link element is not visible.
   */
  public void verifyHelpLinkNotVisible() {
    assertFalse("The help link is visible in the help widget.", linkHelp != null);
  }

  /**
   * Verify that the about link element is visible.
   */
  public void verifyAboutLinkVisible() {
    assertTrue("The about link is no present in the help widget",
        linkAbout != null && linkAbout.isVisible());
  }

  /**
   * Verify that the help widget is open.
   */
  public void verifyOpen() {
    assertTrue("The help widget is closed.", isOpen());
  }

  /**
   * Verify that the help widget is closed.
   */
  public void verifyClosed() {
    assertFalse("The help widget is open.", isOpen());
  }

}
