/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo LujÃ¡n <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Verifies messages displayed on OpenbravoERP.
 *
 * @author elopio
 *
 */
public class MessageBox {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Empty title. */
  public static final String TITLE_EMPTY = "";

  /** Title displayed when a process was completed successfully. */
  public static final String TITLE_PROCESS_COMPLETED_SUCCESSFULLY = "Process completed successfully";

  /** Title displayed when a process ended with a warning. */
  public static final String TITLE_PROCESS_WARNING = "Warning";

  /** Title displayed when a process ended with an error. */
  public static final String TITLE_PROCESS_ERROR = "Error:";

  /** Empty message. */
  public static final String MESSAGE_EMPTY = "";

  /** Identifier of the message box title element. */
  public static String MESSAGE_BOX_TITLE_ID = "messageBoxIDTitle";

  /** Identifier of the message box message element. */
  public static String MESSAGE_BOX_MESSAGE_ID = "messageBoxIDMessage";

  /**
   * Class constructor.
   *
   */
  public MessageBox() {
    waitForMessage();
  }

  /**
   * Wait until the message exists.
   */
  public void waitForMessage() {
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting for message to appear.");
        try {
          SeleniumSingleton.INSTANCE.findElementById(MESSAGE_BOX_TITLE_ID);
          SeleniumSingleton.INSTANCE.findElementById(MESSAGE_BOX_MESSAGE_ID);
        } catch (final NoSuchElementException exception) {
          return false;
        }
        return true;
      }
    });
  }

  /**
   * Wait until the message is visible.
   */
  public void waitForMessageVisible() {
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting until message is visible.");
        return SeleniumSingleton.INSTANCE.findElementById(MESSAGE_BOX_TITLE_ID).isDisplayed()
            && SeleniumSingleton.INSTANCE.findElementById(MESSAGE_BOX_MESSAGE_ID).isDisplayed();
      }
    });
  }

  /**
   * Returns true if the message box is visible.
   *
   * @return true if the message box is visible, otherwise false.
   */
  public boolean isVisible() {
    boolean result = false;
    try {
      result = SeleniumSingleton.INSTANCE.findElementById(MESSAGE_BOX_TITLE_ID).isDisplayed()
          && SeleniumSingleton.INSTANCE.findElementById(MESSAGE_BOX_MESSAGE_ID).isDisplayed();
    } catch (StaleElementReferenceException e) {
      logger.warn("StaleElementReferenceException: {}", e::getMessage);
      result = SeleniumSingleton.INSTANCE.findElementById(MESSAGE_BOX_TITLE_ID).isDisplayed()
          && SeleniumSingleton.INSTANCE.findElementById(MESSAGE_BOX_MESSAGE_ID).isDisplayed();
    }
    return result;
  }

  /**
   * Returns true if is a warning message.
   *
   * @return true if the title shown the title of a warning message, otherwise false.
   */
  public boolean isWarning() {
    return SeleniumSingleton.INSTANCE.findElementById(MESSAGE_BOX_TITLE_ID)
        .getText()
        .equals(TITLE_PROCESS_WARNING);
  }

  /**
   * Returns true if is an error message.
   *
   * @return true if the title shown the title of an error message, otherwise false.
   */
  public boolean isError() {
    return SeleniumSingleton.INSTANCE.findElementById(MESSAGE_BOX_TITLE_ID)
        .getText()
        .equals(TITLE_PROCESS_ERROR);
  }

  /**
   * Get the text of the message.
   *
   * @return the text of the message.
   */
  public String getText() {
    return SeleniumSingleton.INSTANCE.findElementById(MESSAGE_BOX_MESSAGE_ID).getText();
  }

  /**
   * Verify title and message of a message box.
   *
   * @param title
   *          the expected title.
   * @param message
   *          the expected message.
   */
  public void verify(String title, String message) {
    verifyMessage(message);
    verifyTitle(title);
  }

  /**
   * Verify title of a message box.
   *
   * @param title
   *          the expected title.
   */
  public void verifyTitle(String title) {
    assertThat(SeleniumSingleton.INSTANCE.findElementById(MESSAGE_BOX_TITLE_ID).getText(),
        is(equalTo(title)));
  }

  /**
   * Verify message of a message box.
   *
   * @param message
   *          the expected message.
   */
  public void verifyMessage(String message) {
    assertThat(getText().replaceAll("\\s", ""), is(equalTo(message.replaceAll("\\s", ""))));
  }

  /**
   * Verify the process completed successfully message.
   *
   * @param message
   *          The message displayed in the box.
   */
  public void verifyProcessCompletedSuccessfullyMessage(String message) {
    verify(TITLE_PROCESS_COMPLETED_SUCCESSFULLY, message);
  }

  /**
   * Verify that an error message is shown.
   *
   * @param message
   *          The message to verify.
   */
  public void verifyError(String message) {
    verify(MessageBox.TITLE_EMPTY, message);
  }
}
