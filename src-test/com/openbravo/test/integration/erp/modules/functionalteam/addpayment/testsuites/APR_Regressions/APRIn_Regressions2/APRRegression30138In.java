/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions2;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test regression 30138
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression30138In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PaymentInHeaderData paymentInHeaderData;
  PaymentInHeaderData paymentInHeaderVerificationData;
  AddPaymentPopUpData addPaymentVerificationData;
  PaymentInHeaderData paymentInHeaderVerificationData2;
  PaymentInLinesData paymentInLinesVerificationData;
  PaymentInLinesData paymentInLinesVerificationData2;
  PaymentInLinesData paymentInLinesVerificationData3;
  PaymentInLinesData paymentInLinesVerificationData4;

  /**
   * Class constructor.
   *
   */
  public APRRegression30138In(PaymentInHeaderData paymentInHeaderData,
      PaymentInHeaderData paymentInHeaderVerificationData,
      AddPaymentPopUpData addPaymentVerificationData,
      PaymentInHeaderData paymentInHeaderVerificationData2,
      PaymentInLinesData paymentInLinesVerificationData,
      PaymentInLinesData paymentInLinesVerificationData2,
      PaymentInLinesData paymentInLinesVerificationData3,
      PaymentInLinesData paymentInLinesVerificationData4) {
    this.paymentInHeaderData = paymentInHeaderData;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.paymentInHeaderVerificationData2 = paymentInHeaderVerificationData2;
    this.paymentInLinesVerificationData = paymentInLinesVerificationData;
    this.paymentInLinesVerificationData2 = paymentInLinesVerificationData2;
    this.paymentInLinesVerificationData3 = paymentInLinesVerificationData3;
    this.paymentInLinesVerificationData4 = paymentInLinesVerificationData4;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new PaymentInHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .amount("10.00")
            .build(),
        new PaymentInHeaderData.Builder().organization("Spain")
            .documentType("AR Receipt")
            .paymentMethod("1 (Spain)")
            .account("Spain Bank - EUR")
            .currency("EUR")
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Customer A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("EUR")
            .actual_payment("10.00")
            .expected_payment("0.00")
            .amount_gl_items("-1.00")
            .amount_inv_ords("0.00")
            .total("-1.00")
            .difference("11.00")
            .build(),

        new PaymentInHeaderData.Builder().status("Deposited not Cleared")
            .generatedCredit("11.00")
            .usedCredit("0.00")
            .build(),
        new PaymentInLinesData.Builder().received("-1.00").build(),
        new PaymentInLinesData.Builder().dueDate("")
            .invoiceAmount("")
            .expected("")
            .received("-1.00")
            .orderno("")
            .invoiceno("")
            .glitemname("Fees")
            .build(),
        new PaymentInLinesData.Builder().received("11.00").build(),
        new PaymentInLinesData.Builder().dueDate("")
            .invoiceAmount("")
            .expected("")
            .received("11.00")
            .orderno("")
            .invoiceno("")
            .glitemname("")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 30138 - Payment In flow
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression30138InTest() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression30138In] Test regression 30138 - Payment In flow. **");

    PaymentIn paymentIn = new PaymentIn(mainPage).open();
    paymentIn.create(paymentInHeaderData);
    paymentIn.assertSaved();
    paymentIn.assertData(paymentInHeaderVerificationData);

    AddPaymentProcess addPaymentProcess = paymentIn.addDetailsOpen();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.getOrderInvoiceGrid().unselectAll();
    Sleep.trySleep(1000);
    addPaymentProcess
        .addGLItem(new SelectedPaymentData.Builder().gLItem("Fees").paidOut("1.00").build());
    addPaymentProcess.getField("reference_no").focus();
    addPaymentProcess.process("Process Received Payment(s) and Deposit",
        "Leave the credit to be used later", addPaymentVerificationData);

    paymentIn.assertPaymentCreatedSuccessfully();
    paymentIn.assertData(paymentInHeaderVerificationData2);
    PaymentIn.Lines paymentInLines = paymentIn.new Lines(mainPage);
    paymentInLines.assertCount(2);
    // TODO L: Check the following static sleep. This test was failing without it.
    Sleep.trySleep();
    paymentInLines.select(paymentInLinesVerificationData);
    paymentInLines.assertData(paymentInLinesVerificationData2);
    paymentInLines.cancelOnGrid();
    paymentInLines.getTab().clearFilters();
    paymentInLines.getTab().unselectAll();
    Sleep.trySleep(1000);
    paymentInLines.select(paymentInLinesVerificationData3);
    paymentInLines.assertData(paymentInLinesVerificationData4);

    logger.info(
        "** End of test case [APRRegression30138In] Test regression 30138 - Payment In flow. **");
  }

  @AfterClass
  public static void tearDown() {
    SeleniumSingleton.INSTANCE.quit();
    OpenbravoERPTest.seleniumStarted = false;
    OpenbravoERPTest.forceLoginRequired();
  }
}
