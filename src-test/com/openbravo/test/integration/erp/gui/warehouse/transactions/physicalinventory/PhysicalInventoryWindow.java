/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.warehouse.transactions.physicalinventory;

import com.openbravo.test.integration.erp.modules.client.application.gui.StandardWindow;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;

/**
 * Executes and verify actions on the Physical Inventory standard window of OpenbravoERP.
 *
 * @author elopio
 *
 */
public class PhysicalInventoryWindow extends StandardWindow {

  /** The window title. */
  @SuppressWarnings("hiding")
  public static final String TITLE = "Physical Inventory";
  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.WAREHOUSE_MANAGEMENT,
      Menu.WAREHOUSE_TRANSACTIONS, Menu.PHYSICAL_INVENTORY };

  /**
   * Class constructor.
   *
   */
  public PhysicalInventoryWindow() {
    super(TITLE, MENU_PATH);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void load() {
    HeaderTab headerTab = new HeaderTab();
    addTopLevelTab(headerTab);
  }

  /**
   * Select and return the Lines tab.
   *
   * @return the Lines tab.
   */
  public LinesTab selectLinesTab() {
    return (LinesTab) selectTab(LinesTab.IDENTIFIER);
  }

}
