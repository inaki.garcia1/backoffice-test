#!/bin/bash
clear && clear && clear
echo "Executing setupPiSmoke"
echo "The following files will be created: OpenbravoERPTest.properties, log4j2.xml, .classpath."
echo


echo "Creating files from templates"
echo "Choose the desired option:"
echo " + [0] Overwrite existing files."
echo " + [1] Do NOT overwrite existing files."
echo -n "Chosen option: "
read overwriteOption
echo

if  [ "$overwriteOption" = "0" ]; then
cp OpenbravoERPTest.properties.template OpenbravoERPTest.properties && echo "OpenbravoERPTest.properties created" && cp log4j2.xml.template log4j2.xml && echo "log4j2.xml created" && cd .. && cp .classpath.template .classpath && echo ".classpath created" && echo && cd config
  #exit 1
elif [ "$overwriteOption" = "1" ]; then
cp -n OpenbravoERPTest.properties.template OpenbravoERPTest.properties && echo "OpenbravoERPTest.properties created if file didn't exist" && cp -n log4j2.xml.template log4j2.xml && echo "log4j2.xml created if file didn't exist" && cd .. && cp -n .classpath.template .classpath && echo ".classpath created if file didn't exist" && echo && cd config
  #exit 1
else
	echo "Wrong option. No processes executed."
fi

#cp OpenbravoERPTest.properties.template OpenbravoERPTest.properties && echo "OpenbravoERPTest.properties created" && cp log4j2.xml.template log4j2.xml && echo "log4j2.xml created" && cd .. && cp .classpath.template .classpath && echo ".classpath created" && echo && cd config

echo "Setup finished."
