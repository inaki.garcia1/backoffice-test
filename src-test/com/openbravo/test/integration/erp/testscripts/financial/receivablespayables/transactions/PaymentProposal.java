/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentproposal.PaymentProposalHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentproposal.PaymentProposalLinesData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentproposal.PaymentProposalHeaderTab;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentproposal.PaymentProposalLinesTab;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentproposal.PaymentProposalWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;

/**
 * Executes and verifies actions on the Payment Proposal window.
 *
 * @author elopio
 */
public class PaymentProposal
    extends SmokeTabScript<PaymentProposalHeaderData, PaymentProposalHeaderTab>
    implements SmokeWindowScript<PaymentProposalWindow> {

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public PaymentProposal(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Open the Payment Proposal window.
   *
   * @return the opened this script class.
   */
  @Override
  public PaymentProposal open() {
    PaymentProposalWindow window = new PaymentProposalWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the Header tab.
   *
   * Before this, the Payment Proposal window must be opened.
   *
   * @return the Header tab GUI object.
   */
  @Override
  protected PaymentProposalHeaderTab selectTab() {
    PaymentProposalWindow window = (PaymentProposalWindow) mainPage
        .getView(PaymentProposalWindow.TITLE);
    mainPage.getTabSetMain().selectTab(PaymentProposalWindow.TITLE);
    PaymentProposalHeaderTab tab = (PaymentProposalHeaderTab) window.getTopLevelTab();
    return tab;
  }

  /**
   * Select expected payments.
   *
   * @param orderNumbers
   *          The order numbers that will be selected.
   */
  public void selectExpectedPayments(String... orderNumbers) {
    PaymentProposalHeaderTab tab = selectTab();
    tab.selectExpectedPayments(orderNumbers);
  }

  /**
   * Select all expected payments.
   */
  public void selectAllExpectedPayments() {
    PaymentProposalHeaderTab tab = selectTab();
    tab.selectAllExpectedPayments();
  }

  /**
   * Unselect all expected payments.
   */
  public void unselectAllExpectedPayments() {
    PaymentProposalHeaderTab tab = selectTab();
    tab.unselectAllExpectedPayments();
  }

  /**
   * Generate payments.
   */
  public void generatePayments() {
    PaymentProposalHeaderTab tab = selectTab();
    tab.generatePayments();
  }

  /**
   * Executes and verifies actions on the Payment Proposal Lines tab.
   *
   * @author elopio
   *
   */
  public class Lines extends SmokeTabScript<PaymentProposalLinesData, PaymentProposalLinesTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Lines(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Lines tab.
     *
     * Before this, the Payment Proposal window must be opened and this tab has to be accessible
     * from the currently selected one.
     *
     * @return the Lines tab GUI object.
     */
    @Override
    protected PaymentProposalLinesTab selectTab() {
      PaymentProposalWindow window = (PaymentProposalWindow) mainPage
          .getView(PaymentProposalWindow.TITLE);
      PaymentProposalLinesTab tab = (PaymentProposalLinesTab) window
          .selectTab(PaymentProposalLinesTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }
  }

}
