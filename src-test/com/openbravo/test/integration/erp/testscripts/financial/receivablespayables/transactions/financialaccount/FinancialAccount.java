/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.financialaccount;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountingConfigurationData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;

/**
 * Executes and verifies actions on the window.
 *
 * @author Pablo Lujan
 */
public class FinancialAccount {

  /**
   * Executes and verifies actions on the tab.
   *
   * @author plujan
   *
   */
  public static class AccountTab {

    /**
     * Select a record.
     *
     * @param mainPage
     *          The application main page.
     * @param data
     *          The data of the record to select.
     * @throws OpenbravoERPTestException
     */
    public static void select(MainPage mainPage, AccountData data)
        throws OpenbravoERPTestException {
      if (mainPage.isOnNewLayout()) {
        AccountScript.select(mainPage, data);
      } else {
        AccountClassicScript.select(mainPage, data);
      }
    }

    /**
     * Creates the record.
     *
     * @param mainPage
     *          The application main page.
     * @param financialAccountData
     *
     */
    public static void create(MainPage mainPage, AccountData financialAccountData) {
      if (mainPage.isOnNewLayout()) {
        AccountScript.create(mainPage, financialAccountData);
      } else {
        AccountClassicScript.create(mainPage, financialAccountData);
      }
    }

    public static void edit(MainPage mainPage, AccountData financialAccountData) {
      throw new UnsupportedOperationException();

    }
  }

  /**
   * Executes and verifies actions on the tab.
   *
   * @author plujan
   *
   */
  public static class Accounting {

    /**
     * Verify the record was properly created.
     *
     * @param mainPage
     *          The application main page.
     * @param accountingData
     *          .
     */
    public static void verify(MainPage mainPage, AccountingConfigurationData accountingData) {
      if (mainPage.isOnNewLayout()) {
        AccountingScript.verify(mainPage, accountingData);
      } else {
        AccountingClassicScript.verify(mainPage, accountingData);
      }
    }

    /**
     * Edit the record .
     *
     * @param mainPage
     *          The application main page.
     * @param accountingData
     *          .
     */
    public static void edit(MainPage mainPage, AccountingConfigurationData accountingData) {
      throw new UnsupportedOperationException();
    }
  }

  /**
   * Executes and verifies actions on the tab.
   *
   * @author plujan
   *
   */
  public static class PaymentMethod {

    /**
     * Creates the record.
     *
     * @param mainPage
     *          The application main page.
     * @param data
     *
     */
    public static void create(MainPage mainPage, PaymentMethodData data) {
      if (mainPage.isOnNewLayout()) {
        PaymentMethodScript.create(mainPage, data);
      } else {
        PaymentMethodClassicScript.create(mainPage, data);
      }
    }

    /**
     * Verify the record was properly created.
     *
     * @param mainPage
     *          The application main page.
     * @param paymentMethodData
     *
     */
    public static void verify(MainPage mainPage, PaymentMethodData paymentMethodData) {
      if (mainPage.isOnNewLayout()) {
        PaymentMethodScript.verify(mainPage, paymentMethodData);
      } else {
        PaymentMethodClassicScript.verify(mainPage, paymentMethodData);
      }
    }
  }
}
