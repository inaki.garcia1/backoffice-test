/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui;

import static org.junit.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Canvas;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes actions on OpenbravoERP message bar built upon SmartClient user interface library.
 *
 * @author elopio
 *
 */
public class OBStatusBar extends Canvas {

  /**
   * Formatting string used to get the left status bar. The parameter is the object string.
   */
  private static final String FORMAT_SMART_CLIENT_GET_LEFT_STATUS_BAR = "%s.leftStatusBar";
  /**
   * Formatting string used to get the title and value of a field in the status bar. First parameter
   * is the STATUS_BAR Second parameter is the index (between 0 and LENGTH)
   */
  private static final String FORMAT_SMART_CLIENT_GET_TITLE = "%s.members[1].members[%s]._title";
  private static final String FORMAT_SMART_CLIENT_GET_VALUE = "%s.members[1].members[%s]._value";
  /**
   * Formatting string used to get the left status bar. The parameter is STATUS_BAR.
   */
  private static final String FORMAT_SMART_CLIENT_STATUSBAR_LENGTH = "%s.members[1].members.length";
  /** The message displayed when the form is in new mode. */
  private static final String MESSAGE_NEW = "New";
  /** The message displayed when the form is in edit mode. */
  private static final String MESSAGE_EDITING = "Editing";
  /** The message displayed when the form was saved. */
  private static final String MESSAGE_SAVED = "Saved";

  /* Components */
  /** The left status bar, where the message and status are shown. */
  private final Canvas leftStatusBar;

  /** The number of fields in the status bar */
  // private final String statusBarLength;

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  // TODO add the buttons.

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   *
   * @param waitForLoad
   *          Boolean to determine if the system should wait the object be ready to continue.
   */
  public OBStatusBar(String objectString, Boolean waitForLoad) {
    super(objectString, waitForLoad);
    leftStatusBar = new Canvas(String.format(FORMAT_SMART_CLIENT_GET_LEFT_STATUS_BAR, objectString),
        waitForLoad);
  }

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   *
   */
  public OBStatusBar(String objectString) {
    this(objectString, true);
  }

  /**
   * Assert that the message bar is visible.
   */
  private void assertVisible() {
    assertTrue("Status Bar is not visible.", isVisible());
  }

  /**
   * Assert that the message bar is not visible.
   */
  @SuppressWarnings("unused")
  private void assertNotVisible() {
    assertTrue("Status Bar is visible.", isVisible());
  }

  // var m =
  // window.OB.TestRegistry.registry['org.openbravo.client.application.ViewForm_AF4090093CFF1431E040007F010048A5'].view.statusBar.leftStatusBar.members[1].members;
  //
  // for(z = 0; z < m.length; z++) { if(m[z]._title) console.log(m[z]._title + ":" + m[z]._value); }

  // "window.OB.TestRegistry.registry['org.openbravo.client.application.ViewForm_263'].view.statusBar.leftStatusBar.members[1].members.length"
  // window.OB.TestRegistry.registry['org.openbravo.client.application.ViewForm_AF4090093CFF1431E040007F010048A5'].view.statusBar.leftStatusBar.members[1].members[i]._title;
  // window.OB.TestRegistry.registry['org.openbravo.client.application.ViewForm_AF4090093CFF1431E040007F010048A5'].view.statusBar.leftStatusBar.members[1].members[i].value;

  /**
   * Get the message displayed in the status bar.
   *
   * @return the message displayed in the status bar.
   */
  public String getMessage() {
    return leftStatusBar.getText().trim();
  }

  /**
   * Asserts that a Title and a Value are in the status bar.
   *
   * @param title
   *          The value of the title as a String
   * @param value
   *          The value that has to be asserted
   */
  public void assertField(String title, String value) {
    Boolean result = false;

    // TODO L: Check if the following workaround is possible to be removed (Issue 33264)
    avoidReservationStatusIssue(title);
    avoidAmountIssue(title);
    try {
      String barLocator = String.format(FORMAT_SMART_CLIENT_GET_LEFT_STATUS_BAR, objectString);
      logger.info("[assertField] Locator string: {}",
          String.format(FORMAT_SMART_CLIENT_STATUSBAR_LENGTH, barLocator));
      Long statusBarLength = (Long) SeleniumSingleton.INSTANCE
          .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_STATUSBAR_LENGTH,
              String.format(FORMAT_SMART_CLIENT_GET_LEFT_STATUS_BAR, objectString)));

      Integer max = Integer.valueOf(statusBarLength.toString());
      String currentTitle;
      String currentValue;
      for (int i = 0; i < max; i++) {
        currentTitle = (String) SeleniumSingleton.INSTANCE
            .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_TITLE, barLocator, i));
        if (currentTitle != null) {
          if (currentTitle.equals(title)) {
            currentValue = (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
                String.format(FORMAT_SMART_CLIENT_GET_VALUE, barLocator, i));
            result = result || currentValue.equals(value);
            if (!currentValue.equals(value)) {
              logger.error("The title: {} expects '{}' but '{}' was got instead ", title, value,
                  currentValue);
            }
          }
        }
      }
    } catch (NumberFormatException e) {
      logger.error("An error has occurred in OBStatusBar.assertField");
      e.printStackTrace();
    }
    assertTrue(result);
  }

  /**
   * Check that the displayed message matches the expected one.
   *
   * @param message
   *          The expected message.
   * @return true if the displayed message matches the expected one. Otherwise, false.
   */
  public boolean checkMessage(String message) {
    return getMessage().startsWith(message);
  }

  /**
   * Verify the message displayed in the status bar.
   *
   * @param message
   *          The expected message.
   */
  private void assertMessage(String message) {
    assertTrue(getMessage().startsWith(message));
  }

  /**
   * Assert that the status bar shows the new message.
   */
  public void assertNew() {
    assertVisible();
    assertMessage(MESSAGE_NEW);
    // TODO verify the icon.
  }

  /**
   * Assert that the status bar shows the editing message.
   */
  public void assertEditing() {
    assertVisible();
    assertMessage(MESSAGE_EDITING);
    // TODO verify the icon.
  }

  /**
   * Check if the saved message is displayed.
   *
   * @return true if the saved message is displayed. Otherwise, false.
   */
  public boolean isSavedMessage() {
    return checkMessage(MESSAGE_SAVED);
  }

  /**
   * Assert that the status bar shows the saved message.
   */
  public void assertSaved() {
    // Assert moved to assertNotInEditionMode
    // assertVisible();
    assertMessage(MESSAGE_SAVED);
    // TODO verify the icon.
  }

  public void assertNotInEditMode() {
    assertVisible();
  }

  /**
   * Sleeps when Reservation Status is going to be asserted due to issue 33264
   *
   * @param title
   *          The value of the title of the field that is going to be asserted as a String
   */
  private void avoidReservationStatusIssue(String title) {
    if (title.equals("Reservation Status")) {
      Sleep.trySleepWithMessage(8000,
          "Sleeping to avoid performance issues related with the OBStatusBar not correctly loaded");
    }
  }

  private void avoidAmountIssue(String title) {
    if (title.equals("Amount")) {
      Sleep.trySleepWithMessage(15000,
          "Sleeping to avoid performance issues related with the OBStatusBar not correctly loaded");
    }
  }

  /**
   * Assert the fields displayed in the status bar.
   *
   * @param fieldsPattern
   *          The regular expression pattern of the fields displayed in the status bar.
   */
  public void assertFields(String fieldsPattern) {
    String displayedMessage = getMessage();
    assertTrue(
        String.format("Displayed message '%s' should match '%s'.", displayedMessage, fieldsPattern),
        displayedMessage.matches("((New|Saved|Editing)  \\|  )?" + fieldsPattern));
  }
}
