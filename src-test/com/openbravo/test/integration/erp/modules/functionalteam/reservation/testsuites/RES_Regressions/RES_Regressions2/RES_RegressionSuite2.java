/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.reservation.testsuites.RES_Regressions.RES_Regressions2;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.openbravo.test.integration.erp.modules.functionalteam.reservation.testsuites.RES_Regressions.RES_Regressions1.DisableStockPreference;
import com.openbravo.test.integration.erp.modules.functionalteam.reservation.testsuites.RES_Regressions.RES_Regressions1.EnableStockPreference;
import com.openbravo.test.integration.erp.testscripts.SuiteThatStopsIfFailure;

@RunWith(SuiteThatStopsIfFailure.class)
@Suite.SuiteClasses({ EnableStockPreference.class, RESRegression31799.class,
    DisableStockPreference.class })
// Tests temporally commented to check the rest of the tests are working properly
// RESRegression28907R27848R29003.class,
public class RES_RegressionSuite2 {
  // No content is required, this is just the definition of a test suite.

}
