/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Rowinson Gallego <rwn.gallego@gmail.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.navigationbarcomponents.userprofile;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.UserProfile;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Test the display and hide of the UserProfile tab set, the Password tab and the Profile tab from
 * the user profile widget added by Navigation Bar Components module.
 *
 * @author rwngallego
 *
 */
public class DisplayAndHideUserProfileTabs extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Display the user profile component, open the password tab and finally hide the component
   * [NAV0150] Display and hide user profile
   */
  @Test
  public void userProfileTabsShouldBeDisplayAndHidden() {
    UserProfile componentUserProfile = mainPage.getNavigationBar().getComponentUserProfile();
    componentUserProfile.open();
    logger.info("[NAV0150] Display and hide user profile");
    logger.info("Display user profile");
    assertTrue("User profile widget was not opened.", componentUserProfile.isOpen());
    assertTrue("Profile tab was not selected.", componentUserProfile.isProfileTabOpen());

    logger.info("Open password tab");
    componentUserProfile.openPasswordTab();
    assertFalse("Profile tab still selected.", componentUserProfile.isProfileTabOpen());
    assertTrue("Password tab was not selected.", componentUserProfile.isPasswordTabOpen());

    componentUserProfile.close();
    assertFalse("User profile widget was still opened.", componentUserProfile.isOpen());
  }
}
