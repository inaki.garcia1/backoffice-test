/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.accounting.transactions.opencloseperiodcontrol;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for OpenClosePeriodControlData
 *
 * @author plujan
 *
 */
public class OpenClosePeriodControlData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the periodNo value
     *
     * Description: Until Period Number
     *
     * @param value
     *          The periodNo value.
     * @return The builder for this class.
     */
    public Builder periodNo(String value) {
      this.dataFields.put("periodNo", value);
      return this;
    }

    /**
     * Set the periodAction value
     *
     * Description: Action taken for this period
     *
     * @param value
     *          The periodAction value.
     * @return The builder for this class.
     */
    public Builder periodAction(String value) {
      this.dataFields.put("periodAction", value);
      return this;
    }

    /**
     * Set the cascade value
     *
     * Description: If it is selected the process will also open or close the documents of all of
     * the organizations that depend on the selected organization.
     *
     * @param value
     *          The cascade value.
     * @return The builder for this class.
     */
    public Builder cascade(Boolean value) {
      this.dataFields.put("cascade", value);
      return this;
    }

    /**
     * Set the documentCategory value
     *
     * Description: A classification of document types that are shown and processed in the same
     * window.
     *
     * @param value
     *          The documentCategory value.
     * @return The builder for this class.
     */
    public Builder documentCategory(String value) {
      this.dataFields.put("documentCategory", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the year value
     *
     * Description: Calendar Year
     *
     * @param value
     *          The year value.
     * @return The builder for this class.
     */
    public Builder year(String value) {
      this.dataFields.put("year", value);
      return this;
    }

    /**
     * Set the calendar value
     *
     * Description: A table showing the days of the week for each month of the year.
     *
     * @param value
     *          The calendar value.
     * @return The builder for this class.
     */
    public Builder calendar(String value) {
      this.dataFields.put("calendar", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the Starting date value
     *
     * Description: Starting date
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder startingDate(String value) {
      this.dataFields.put("startingDate", value);
      return this;
    }

    /**
     * Set the Ending date value
     *
     * Description: Ending date
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder endingDate(String value) {
      this.dataFields.put("endingDate", value);
      return this;
    }

    /**
     * Set the status value
     *
     * Description: Current status of the period
     *
     * @param value
     *          The status value.
     * @return The builder for this class.
     */
    public Builder status(String value) {
      this.dataFields.put("status", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public OpenClosePeriodControlData build() {
      return new OpenClosePeriodControlData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private OpenClosePeriodControlData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
