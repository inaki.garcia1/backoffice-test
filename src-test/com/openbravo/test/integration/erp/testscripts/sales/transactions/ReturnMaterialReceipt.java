/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Andy Armaignac <collazoandy4@gmail.com>,
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.sales.transactions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.sales.transactions.returnmaterialreceipt.ReturnMaterialReceiptHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.returnmaterialreceipt.ReturnMaterialReceiptLinesData;
import com.openbravo.test.integration.erp.data.selectors.ReturnMaterialReceiptLineSelectorData;
import com.openbravo.test.integration.erp.gui.sales.transactions.returnmaterialreceipt.LinesTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.returnmaterialreceipt.ReturnMaterialReceiptHeaderTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.returnmaterialreceipt.ReturnMaterialReceiptWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;

/**
 * Executes and verifies actions on the Return Material Receipt.
 *
 * @author collazoandy4
 */
public class ReturnMaterialReceipt
    extends SmokeTabScript<ReturnMaterialReceiptHeaderData, ReturnMaterialReceiptHeaderTab>
    implements SmokeWindowScript<ReturnMaterialReceiptWindow> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public ReturnMaterialReceipt(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Open the Return Material Receipt window.
   *
   * @return this Return Material Receipt script.
   */
  @Override
  public ReturnMaterialReceipt open() {
    ReturnMaterialReceiptWindow window = new ReturnMaterialReceiptWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the Header tab.
   *
   * Before this, the Return Material Receipt window must be opened.
   *
   * @return the Header tab GUI object.
   */
  @Override
  protected ReturnMaterialReceiptHeaderTab selectTab() {
    ReturnMaterialReceiptWindow window = (ReturnMaterialReceiptWindow) mainPage
        .getView(ReturnMaterialReceiptWindow.TITLE);
    mainPage.getTabSetMain().selectTab(ReturnMaterialReceiptWindow.TITLE);
    ReturnMaterialReceiptHeaderTab tab = (ReturnMaterialReceiptHeaderTab) window.getTopLevelTab();
    tab.waitUntilSelected();
    return tab;
  }

  /**
   * Complete the return material receipt.
   *
   */
  public void complete() {
    logger.info("Complete return material receipt.");
    ReturnMaterialReceiptHeaderTab tab = selectTab();
    tab.complete();
  }

  public PickAndExecuteWindow<ReturnMaterialReceiptLineSelectorData> openPickAndEditLines() {
    return selectTab().openPickAndEditLines();
  }

  /**
   * Executes and verifies actions on the Return Material Receipt Lines tab.
   *
   * @author collazoandy4
   *
   */
  public class Lines extends SmokeTabScript<ReturnMaterialReceiptLinesData, LinesTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Lines(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Lines tab.
     *
     * Before this, the ReturnMaterialReceiptWindow must be opened and this tab has to be accessible
     * from the currently selected one.
     *
     * @return the Lines tab GUI object.
     */
    @Override
    protected LinesTab selectTab() {
      ReturnMaterialReceiptWindow window = (ReturnMaterialReceiptWindow) mainPage
          .getView(ReturnMaterialReceiptWindow.TITLE);
      LinesTab tab = (LinesTab) window.selectTab(LinesTab.IDENTIFIER);
      return tab;
    }

  }
}
