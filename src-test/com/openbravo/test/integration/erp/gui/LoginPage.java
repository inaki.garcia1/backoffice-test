/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.openbravo.test.integration.erp.common.TimeoutConstants;
import com.openbravo.test.integration.erp.data.ChangeExpiredPasswordData;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Executes and verifies actions on OpenbravoERP Login page.
 *
 * @author elopio
 *
 */
public class LoginPage implements Page {

  /** Identifier of the username text field. */
  public static final String TEXT_FIELD_USERNAME = "user";
  /** Identifier of the password text field. */
  public static final String TEXT_FIELD_PASSWORD = "password";
  public static final String TEXT_FIELD_NEW_PASSWORD = "user";
  public static final String TEXT_FIELD_CONFIRM_PASSWORD = "password";
  /** Identifier of the ok button. */
  public static final String BUTTON_OK = "buttonOK";
  /** Identifier of the error message title. */
  private static final String IDENTIFIER_ERROR_MESSAGE_TITLE = "errorMsgTitle";
  /** Identifier of the error message content. */
  private static final String IDENTIFIER_ERROR_MESSAGE_CONTENT = "errorMsgContent";
  /** Failed error message title. */
  private static final String MESSAGE_TITLE_INVALID_USER_NAME_OR_PASSWORD = "Invalid user name or password.";
  /** Failed error message content. */
  private static final String MESSAGE_CONTENT_PLEASE_TRY_AGAIN = "Please try again.";

  private static final String MESSAGE_EXPIRED_PASSWORD = "Your password has expired. Please provide a new one.";
  private static final String MESSAGE_WRITE_NEW_PASSWORD = "Write a new password";
  private static final String TITLE_WEAK_PASSWORD = "Weak password. Please provide a stronger one";
  private static final String MESSAGE_INVALID_PASSWORD = "Passwords must have at least 8 characters and contain"
      + " at least three of the following: uppercase letters, lowercase letters, numbers and symbols.";

  /** Relative URL of the login screen. */
  public static String LOGIN_URL = "/security/Login";
  public static boolean upgraded;
  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Open login page.
   */
  public void open() {
    final String baseUrl = ConfigurationProperties.INSTANCE.getOpenbravoBaseURL();
    final String context = ConfigurationProperties.INSTANCE.getOpenbravoContext();
    // TODO is this still required?
    // SeleniumSingleton.INSTANCE.executeScriptWithReturn("window.opener=null");
    // TODO Selenium2
    // SeleniumSingleton.INSTANCE.setTimeout("30000");
    logger.debug("Open the web page {}.", context + LOGIN_URL);
    try {
      SeleniumSingleton.INSTANCE.get(baseUrl + context + LOGIN_URL);
    } catch (final RuntimeException e) {
      logger.warn("The web page took more than 30 seconds to open. Trying again.");
      // TODO Selenium2
      // SeleniumSingleton.INSTANCE.setTimeout(OPEN_PAGE_TIMEOUT);
      SeleniumSingleton.INSTANCE.get(context + LOGIN_URL);
    }
  }

  /**
   * Wait for the username and password text fields to appear.
   */
  public void waitForFieldsToAppear() {
    waitForFieldsToAppear(TimeoutConstants.PAGE_LOAD_TIMEOUT);
  }

  /**
   * Wait for the username and password text fields to appear.
   *
   * @param timeout
   *          Timeout in milliseconds after which a failure will be reported.
   */
  public void waitForFieldsToAppear(long timeout) {
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    Wait<WebDriver> loginWait = new WebDriverWait(SeleniumSingleton.INSTANCE, timeout);
    loginWait.until(wd -> {
      logger.debug("Waiting for Login page to load.");
      try {
        SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_USERNAME);
        SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_PASSWORD);
        upgraded = true;
      } catch (final NoSuchElementException exception) {
        try {
          // to be removed: look for the appFrame if the login page still has it
          WebElement frame = SeleniumSingleton.INSTANCE.findElementByName("appFrame");
          SeleniumSingleton.INSTANCE.switchTo().frame(frame);
          SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_USERNAME);
          SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_PASSWORD);
        } catch (final NoSuchElementException ignored) {
          return false;
        }
      }
      return true;
    });
  }

  /**
   * Perform log in on Openbravo ERP.
   *
   * @param logInData
   *          Data to log in.
   */
  public void logIn(LogInData logInData) {
    logger.info("Log in.");
    waitForFieldsToAppearWithRetry();
    // XXX I hate alerts!
    // TODO Selenium2
    // if (SeleniumSingleton.INSTANCE.isAlertPresent()) {
    // SeleniumSingleton.INSTANCE.getAlert();
    // }
    logger.debug("Logging in with user name '{}' and password '{}'.", logInData.getUserName(),
        logInData.getPassword());
    if (logInData.getUserName() != null) {
      logger.debug("Entering user name '{}'.", logInData::getUserName);
      clickAndInputField(TEXT_FIELD_USERNAME, logInData.getUserName());
    }
    if (logInData.getPassword() != null) {
      logger.debug("Entering password '{}'.", logInData::getPassword);
      clickAndInputField(TEXT_FIELD_PASSWORD, logInData.getPassword());
    }
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_OK).click();
  }

  /**
   * Verify that the login was not successful.
   */
  public void verifyWrongLogin() {
    verifyLoginError(MESSAGE_TITLE_INVALID_USER_NAME_OR_PASSWORD, MESSAGE_CONTENT_PLEASE_TRY_AGAIN);
  }

  public void verifyPasswordExpired() {
    verifyLoginError(MESSAGE_EXPIRED_PASSWORD, MESSAGE_WRITE_NEW_PASSWORD);
  }

  public void verifyInvalidPassword() {
    verifyLoginError(TITLE_WEAK_PASSWORD, MESSAGE_INVALID_PASSWORD);
  }

  /**
   * This is required when logging out after (de)activating.
   */
  public void acceptAlertOnLogout() {
    final Alert alert = SeleniumSingleton.INSTANCE.switchTo().alert();
    alert.dismiss();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
  }

  /**
   * Verify that the logout succeeded.
   */
  public void verifyLogout() {
    logger.debug("Verifying log out.");
    try {
      waitForFieldsToAppear();
    } catch (Throwable t) {
      logger.warn("Verify logout timed out. Refresh the page to try one more time.");
      SeleniumSingleton.INSTANCE.navigate().refresh();
      waitForFieldsToAppear();
    }
    // TODO Selenium2
    // FIXME this will close any alert if present. It is necessary because selenium not always sees
    // the alerts, so if we wait for the alert to appear, sometimes it will fail. A better way to
    // handle alerts is required, waiting for them only when they are expected to appear.
    // if (SeleniumSingleton.INSTANCE.isAlertPresent()) {
    // String alertText = SeleniumSingleton.INSTANCE.getAlert();
    // logger.info("An alert was present: {}.", alertText);
    // }
  }

  public void changeExpiredPassword(ChangeExpiredPasswordData passwordData) {
    logger.info("Update expired password.");
    waitForFieldsToAppearWithRetry();

    logger.debug("Setting new password as '{}' and confirmation '{}'.",
        passwordData::getNewPassword, passwordData::getConfirmPassword);
    if (passwordData.getNewPassword() != null) {
      logger.debug("Entering new password '{}'.", passwordData::getNewPassword);
      clickAndInputField(TEXT_FIELD_NEW_PASSWORD, passwordData.getNewPassword());
    }
    if (passwordData.getConfirmPassword() != null) {
      logger.debug(
          String.format("Entering confirmation password '%s'.", passwordData.getConfirmPassword()));
      clickAndInputField(TEXT_FIELD_CONFIRM_PASSWORD, passwordData.getConfirmPassword());
    }

    // FIXME: Adding sleep for password change in login screen to work properly
    Sleep.trySleep(1000);
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_OK).click();
  }

  private void verifyLoginError(String errorTitle, String errorContent) {
    logger.debug("Verifying a failed login.");

    LoginErrorMessageAppears loginErrorMessageAppears = new LoginErrorMessageAppears();
    SeleniumSingleton.WAIT.until(loginErrorMessageAppears);

    String titleText = (loginErrorMessageAppears.getErrorMessageTitle() != null)
        ? loginErrorMessageAppears.getErrorMessageTitle().getText()
        : "";
    String contentText = (loginErrorMessageAppears.getErrorMessageContent() != null)
        ? loginErrorMessageAppears.getErrorMessageContent().getText()
        : "";

    assertThat(titleText, is(equalTo(errorTitle)));
    assertThat(contentText, is(equalTo(errorContent)));
  }

  private void clickAndInputField(String fieldName, String value) {
    WebElement element = SeleniumSingleton.INSTANCE.findElementById(fieldName);

    element.clear();
    element.click();
    // TODO: <chromedriver> specific workaround to sendKeys in this case: send user or pass via JS
    // call
    if (value.matches(".*[56/].*") && SeleniumSingleton.runsChrome()) {
      SeleniumSingleton.INSTANCE.executeScriptWithReturn(
          String.format("document.getElementById('%s').value = '%s'", fieldName, value));
    } else {
      element.sendKeys(value);
    }
  }

  private void waitForFieldsToAppearWithRetry() {
    try {
      waitForFieldsToAppear();
    } catch (Throwable throwable) {
      logger.warn("Load log in page timed out. Refresh the page to try one more time.");
      SeleniumSingleton.INSTANCE.navigate().refresh();
      waitForFieldsToAppear();
    }
  }

  private class LoginErrorMessageAppears implements ExpectedCondition<Boolean> {

    private WebElement errorMessageTitle;
    private WebElement errorMessageContent;

    @Override
    public Boolean apply(WebDriver webDriver) {
      logger.debug("Waiting for login error message to appear.");
      try {
        errorMessageTitle = SeleniumSingleton.INSTANCE
            .findElementById(IDENTIFIER_ERROR_MESSAGE_TITLE);
        errorMessageContent = SeleniumSingleton.INSTANCE
            .findElementById(IDENTIFIER_ERROR_MESSAGE_CONTENT);
        return errorMessageTitle.isDisplayed() && errorMessageContent.isDisplayed();
      } catch (final NoSuchElementException exception) {
        return false;
      }
    }

    public WebElement getErrorMessageTitle() {
      return errorMessageTitle;
    }

    public WebElement getErrorMessageContent() {
      return errorMessageContent;
    }
  }
}
