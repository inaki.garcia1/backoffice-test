/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.application.heartbeatconfiguration;

import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the Heartbeat Configuration classic window of OpenbravoERP.
 *
 * @author Pablo Lujan
 */
public class HeartbeatConfiguration {

  /**
   * Executes and verifies actions on the Heartbeat Configuration tab of OpenbravoERP.
   *
   * @author elopio
   *
   */
  public static class HeartbeatConfigurationTab {

    /**
     * Enable the heartbeat.
     *
     * @param mainPage
     *          The application main page.
     */
    public static void enableHeartbeat(MainPage mainPage) {
      HeartbeatConfigurationTabClassicScript.enableHeartbeat(mainPage);
    }

    /**
     * Deactivate the hearbeat.
     *
     * @param mainPage
     *          The application main page.
     */
    public static void disableHeartbeat(MainPage mainPage) {
      HeartbeatConfigurationTabClassicScript.disableHeartbeat(mainPage);
    }

    public static void verifyDisabled(MainPage mainPage) {
      HeartbeatConfigurationTabClassicScript.verifyDisabled(mainPage);
    }

    public static void verifyEnabled(MainPage mainPage) {
      // Not implemented
      // HeartbeatConfigurationTabScriptClassic.verifyEnabled(mainPage);
    }

  }

}
