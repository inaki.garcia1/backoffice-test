/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.standardview.grid;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Test the grid filters.
 *
 * @author igarcia
 *
 */
public abstract class SortRecords extends OpenbravoERPTest {

  private final DataObject dataObjectExpectedFirstSorting;
  private final DataObject dataObjectExpectedSecondSorting;

  /**
   * Class constructor.
   *
   * @param dataObjectExpectedFirstSorting
   *          Expected result for first line in grid view on first sorting
   * @param dataObjectExpectedSecondSorting
   *          Expected result for first line in grid view on second sorting
   */
  public SortRecords(DataObject dataObjectExpectedFirstSorting,
      DataObject dataObjectExpectedSecondSorting) {
    this.dataObjectExpectedFirstSorting = dataObjectExpectedFirstSorting;
    this.dataObjectExpectedSecondSorting = dataObjectExpectedSecondSorting;
  }

  /**
   * Test grid sorting.
   */
  @Test
  public void sortRecords() {
    GeneratedTab<DataObject> tab = getTab();
    Boolean compare = dataObjectExpectedFirstSorting
        .getNextFieldName() == dataObjectExpectedSecondSorting.getNextFieldName();
    // Checking that the test parameters are correct (column header names of data objects match)
    assertTrue("Column headers don't match", compare);
    tab.clickColumnHeader(dataObjectExpectedFirstSorting.getNextFieldName());
    tab.selectWithoutFiltering(0);
    tab.assertData(dataObjectExpectedFirstSorting);
    tab.closeForm();
    tab.clickColumnHeader(dataObjectExpectedSecondSorting.getNextFieldName());
    tab.selectWithoutFiltering(0);
    tab.assertData(dataObjectExpectedSecondSorting);
    tab.closeForm();
  }

  /**
   * Open and return the generated tab that will be used for the test.
   *
   * @return the generated tab that will be used for the test.
   */
  public abstract GeneratedTab<DataObject> getTab();
}
