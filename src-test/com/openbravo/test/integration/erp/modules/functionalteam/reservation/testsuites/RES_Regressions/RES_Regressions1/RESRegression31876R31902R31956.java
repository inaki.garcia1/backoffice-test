/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.reservation.testsuites.RES_Regressions.RES_Regressions1;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.masterdata.product.PriceData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.warehouse.transactions.stockreservation.ReservationData;
import com.openbravo.test.integration.erp.data.warehouse.transactions.stockreservation.StockData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.masterdata.product.Product;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.GoodsShipment;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;
import com.openbravo.test.integration.erp.testscripts.warehouse.transactions.stockreservation.StockReservation;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test regression 31876 - 31902 - 31956
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class RESRegression31876R31902R31956 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  ProductData productData;
  PriceData productPriceData;
  GoodsReceiptHeaderData goodsReceiptHeaderData;
  GoodsReceiptHeaderData goodsReceiptHeaderVerificationData;
  GoodsReceiptLinesData goodsReceiptLinesData1;
  GoodsReceiptLinesData goodsReceiptLineVerificationData1;
  GoodsReceiptLinesData goodsReceiptLinesData2;
  GoodsReceiptLinesData goodsReceiptLineVerificationData2;
  GoodsReceiptLinesData goodsReceiptLinesData3;
  GoodsReceiptLinesData goodsReceiptLineVerificationData3;
  GoodsReceiptLinesData goodsReceiptLinesData4;
  GoodsReceiptLinesData goodsReceiptLineVerificationData4;
  GoodsReceiptLinesData goodsReceiptLinesData5;
  GoodsReceiptLinesData goodsReceiptLineVerificationData5;
  GoodsReceiptLinesData goodsReceiptLinesData6;
  GoodsReceiptLinesData goodsReceiptLineVerificationData6;
  GoodsReceiptHeaderData completedGoodsReceiptHeaderVerificationData;
  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderLinesData1;
  SalesOrderLinesData salesOrderLinesVerificationData1;
  SalesOrderLinesData salesOrderLinesData2;
  SalesOrderLinesData salesOrderLinesVerificationData2;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;
  ReservationData reservationVerificationData1;
  StockData stockVerificationData1;
  StockData stockVerificationData2;
  StockData stockVerificationData3;
  ReservationData reservationVerificationData2;
  StockData stockVerificationData4;
  StockData stockVerificationData5;
  StockData stockVerificationData6;
  GoodsShipmentHeaderData goodsShipmentHeaderData;
  GoodsShipmentHeaderData goodsShipmentHeaderVerificationData;
  GoodsShipmentLinesData goodsShipmentLinesData1;
  GoodsShipmentLinesData goodsShipmentLineVerificationData1;
  GoodsShipmentLinesData goodsShipmentLinesData2;
  GoodsShipmentLinesData goodsShipmentLineVerificationData2;
  GoodsShipmentLinesData goodsShipmentLinesData3;
  GoodsShipmentLinesData goodsShipmentLineVerificationData3;
  GoodsShipmentLinesData goodsShipmentLinesData4;
  GoodsShipmentLinesData goodsShipmentLineVerificationData4;
  GoodsShipmentLinesData goodsShipmentLinesData5;
  GoodsShipmentLinesData goodsShipmentLineVerificationData5;
  GoodsShipmentLinesData goodsShipmentLinesData6;
  GoodsShipmentLinesData goodsShipmentLineVerificationData6;
  GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData;
  ReservationData reservationVerificationData3;
  StockData stockVerificationData7;
  StockData stockVerificationData8;
  StockData stockVerificationData9;
  ReservationData reservationVerificationData4;
  StockData stockVerificationData10;
  StockData stockVerificationData11;
  StockData stockVerificationData12;
  GoodsShipmentLinesData goodsShipmentLinesData7;
  GoodsShipmentLinesData goodsShipmentLineVerificationData7;
  GoodsShipmentLinesData goodsShipmentLinesData8;
  GoodsShipmentLinesData goodsShipmentLineVerificationData8;
  GoodsShipmentLinesData goodsShipmentLinesData9;
  GoodsShipmentLinesData goodsShipmentLineVerificationData9;
  GoodsShipmentLinesData goodsShipmentLinesData10;
  GoodsShipmentLinesData goodsShipmentLineVerificationData10;
  ReservationData reservationVerificationData5;
  StockData stockVerificationData13;
  StockData stockVerificationData14;
  StockData stockVerificationData15;
  ReservationData reservationVerificationData6;
  StockData stockVerificationData16;
  StockData stockVerificationData17;
  StockData stockVerificationData18;
  GoodsShipmentLinesData goodsShipmentLineVerificationData11;
  GoodsShipmentLinesData goodsShipmentLineVerificationData12;
  ReservationData reservationVerificationData7;
  StockData stockVerificationData19;
  StockData stockVerificationData20;
  StockData stockVerificationData21;
  ReservationData reservationVerificationData8;
  StockData stockVerificationData22;
  StockData stockVerificationData23;
  StockData stockVerificationData24;
  GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData2;
  ReservationData reservationData;
  ReservationData reservationVerificationData9;
  StockData stockVerificationData25;
  ReservationData reservationVerificationData10;
  StockData stockVerificationData26;
  ReservationData reservationData2;
  ReservationData reservationVerificationData11;
  StockData stockVerificationData27;
  ReservationData reservationVerificationData12;
  StockData stockVerificationData28;
  ReservationData reservationVerificationData13;
  StockData stockVerificationData29;
  StockData stockVerificationData30;
  ReservationData reservationVerificationData14;
  StockData stockData1;
  StockData stockVerificationData31;
  StockData stockData2;
  StockData stockVerificationData32;
  ReservationData reservationVerificationData15;
  StockData stockVerificationData33;
  StockData stockVerificationData34;
  ReservationData reservationVerificationData16;
  StockData stockVerificationData35;
  StockData stockVerificationData36;
  GoodsShipmentLinesData goodsShipmentLinesData11;
  GoodsShipmentLinesData goodsShipmentLineVerificationData13;
  GoodsShipmentLinesData goodsShipmentLinesData12;
  GoodsShipmentLinesData goodsShipmentLineVerificationData14;
  GoodsShipmentLinesData goodsShipmentLinesData13;
  GoodsShipmentLinesData goodsShipmentLineVerificationData15;
  GoodsShipmentLinesData goodsShipmentLinesData14;
  GoodsShipmentLinesData goodsShipmentLineVerificationData16;
  GoodsShipmentLinesData goodsShipmentLinesData15;
  GoodsShipmentLinesData goodsShipmentLineVerificationData17;
  GoodsShipmentLinesData goodsShipmentLinesData16;
  GoodsShipmentLinesData goodsShipmentLineVerificationData18;
  ReservationData reservationData3;
  ReservationData reservationData4;

  /**
   * Class constructor.
   *
   */
  public RESRegression31876R31902R31956(ProductData productData, PriceData productPriceData,
      GoodsReceiptHeaderData goodsReceiptHeaderData,
      GoodsReceiptHeaderData goodsReceiptHeaderVerificationData,
      GoodsReceiptLinesData goodsReceiptLinesData1,
      GoodsReceiptLinesData goodsReceiptLineVerificationData1,
      GoodsReceiptLinesData goodsReceiptLinesData2,
      GoodsReceiptLinesData goodsReceiptLineVerificationData2,
      GoodsReceiptLinesData goodsReceiptLinesData3,
      GoodsReceiptLinesData goodsReceiptLineVerificationData3,
      GoodsReceiptLinesData goodsReceiptLinesData4,
      GoodsReceiptLinesData goodsReceiptLineVerificationData4,
      GoodsReceiptLinesData goodsReceiptLinesData5,
      GoodsReceiptLinesData goodsReceiptLineVerificationData5,
      GoodsReceiptLinesData goodsReceiptLinesData6,
      GoodsReceiptLinesData goodsReceiptLineVerificationData6,
      GoodsReceiptHeaderData completedGoodsReceiptHeaderVerificationData,
      SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData,
      SalesOrderLinesData salesOrderLinesData1,
      SalesOrderLinesData salesOrderLinesVerificationData1,
      SalesOrderLinesData salesOrderLinesData2,
      SalesOrderLinesData salesOrderLinesVerificationData2,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData,
      ReservationData reservationVerificationData1, StockData stockVerificationData1,
      StockData stockVerificationData2, StockData stockVerificationData3,
      ReservationData reservationVerificationData2, StockData stockVerificationData4,
      StockData stockVerificationData5, StockData stockVerificationData6,
      GoodsShipmentHeaderData goodsShipmentHeaderData,
      GoodsShipmentHeaderData goodsShipmentHeaderVerificationData,
      GoodsShipmentLinesData goodsShipmentLinesData1,
      GoodsShipmentLinesData goodsShipmentLineVerificationData1,
      GoodsShipmentLinesData goodsShipmentLinesData2,
      GoodsShipmentLinesData goodsShipmentLineVerificationData2,
      GoodsShipmentLinesData goodsShipmentLinesData3,
      GoodsShipmentLinesData goodsShipmentLineVerificationData3,
      GoodsShipmentLinesData goodsShipmentLinesData4,
      GoodsShipmentLinesData goodsShipmentLineVerificationData4,
      GoodsShipmentLinesData goodsShipmentLinesData5,
      GoodsShipmentLinesData goodsShipmentLineVerificationData5,
      GoodsShipmentLinesData goodsShipmentLinesData6,
      GoodsShipmentLinesData goodsShipmentLineVerificationData6,
      GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData,
      ReservationData reservationVerificationData3, StockData stockVerificationData7,
      StockData stockVerificationData8, StockData stockVerificationData9,
      ReservationData reservationVerificationData4, StockData stockVerificationData10,
      StockData stockVerificationData11, StockData stockVerificationData12,
      GoodsShipmentLinesData goodsShipmentLinesData7,
      GoodsShipmentLinesData goodsShipmentLineVerificationData7,
      GoodsShipmentLinesData goodsShipmentLinesData8,
      GoodsShipmentLinesData goodsShipmentLineVerificationData8,
      GoodsShipmentLinesData goodsShipmentLinesData9,
      GoodsShipmentLinesData goodsShipmentLineVerificationData9,
      GoodsShipmentLinesData goodsShipmentLinesData10,
      GoodsShipmentLinesData goodsShipmentLineVerificationData10,
      ReservationData reservationVerificationData5, StockData stockVerificationData13,
      StockData stockVerificationData14, StockData stockVerificationData15,
      ReservationData reservationVerificationData6, StockData stockVerificationData16,
      StockData stockVerificationData17, StockData stockVerificationData18,
      GoodsShipmentLinesData goodsShipmentLineVerificationData11,
      GoodsShipmentLinesData goodsShipmentLineVerificationData12,
      ReservationData reservationVerificationData7, StockData stockVerificationData19,
      StockData stockVerificationData20, StockData stockVerificationData21,
      ReservationData reservationVerificationData8, StockData stockVerificationData22,
      StockData stockVerificationData23, StockData stockVerificationData24,
      GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData2,
      ReservationData reservationData, ReservationData reservationVerificationData9,
      StockData stockVerificationData25, ReservationData reservationVerificationData10,
      StockData stockVerificationData26, ReservationData reservationData2,
      ReservationData reservationVerificationData11, StockData stockVerificationData27,
      ReservationData reservationVerificationData12, StockData stockVerificationData28,
      ReservationData reservationVerificationData13, StockData stockVerificationData29,
      StockData stockVerificationData30, ReservationData reservationVerificationData14,
      StockData stockData1, StockData stockVerificationData31, StockData stockData2,
      StockData stockVerificationData32, ReservationData reservationVerificationData15,
      StockData stockVerificationData33, StockData stockVerificationData34,
      ReservationData reservationVerificationData16, StockData stockVerificationData35,
      StockData stockVerificationData36, GoodsShipmentLinesData goodsShipmentLinesData11,
      GoodsShipmentLinesData goodsShipmentLineVerificationData13,
      GoodsShipmentLinesData goodsShipmentLinesData12,
      GoodsShipmentLinesData goodsShipmentLineVerificationData14,
      GoodsShipmentLinesData goodsShipmentLinesData13,
      GoodsShipmentLinesData goodsShipmentLineVerificationData15,
      GoodsShipmentLinesData goodsShipmentLinesData14,
      GoodsShipmentLinesData goodsShipmentLineVerificationData16,
      GoodsShipmentLinesData goodsShipmentLinesData15,
      GoodsShipmentLinesData goodsShipmentLineVerificationData17,
      GoodsShipmentLinesData goodsShipmentLinesData16,
      GoodsShipmentLinesData goodsShipmentLineVerificationData18, ReservationData reservationData3,
      ReservationData reservationData4) {
    this.productData = productData;
    this.productPriceData = productPriceData;
    this.goodsReceiptHeaderData = goodsReceiptHeaderData;
    this.goodsReceiptHeaderVerificationData = goodsReceiptHeaderVerificationData;
    this.goodsReceiptLinesData1 = goodsReceiptLinesData1;
    this.goodsReceiptLineVerificationData1 = goodsReceiptLineVerificationData1;
    this.goodsReceiptLinesData2 = goodsReceiptLinesData2;
    this.goodsReceiptLineVerificationData2 = goodsReceiptLineVerificationData2;
    this.goodsReceiptLinesData3 = goodsReceiptLinesData3;
    this.goodsReceiptLineVerificationData3 = goodsReceiptLineVerificationData3;
    this.goodsReceiptLinesData4 = goodsReceiptLinesData4;
    this.goodsReceiptLineVerificationData4 = goodsReceiptLineVerificationData4;
    this.goodsReceiptLinesData5 = goodsReceiptLinesData5;
    this.goodsReceiptLineVerificationData5 = goodsReceiptLineVerificationData5;
    this.goodsReceiptLinesData6 = goodsReceiptLinesData6;
    this.goodsReceiptLineVerificationData6 = goodsReceiptLineVerificationData6;
    this.completedGoodsReceiptHeaderVerificationData = completedGoodsReceiptHeaderVerificationData;
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesData1 = salesOrderLinesData1;
    this.salesOrderLinesVerificationData1 = salesOrderLinesVerificationData1;
    this.salesOrderLinesData2 = salesOrderLinesData2;
    this.salesOrderLinesVerificationData2 = salesOrderLinesVerificationData2;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    this.reservationVerificationData1 = reservationVerificationData1;
    this.stockVerificationData1 = stockVerificationData1;
    this.stockVerificationData2 = stockVerificationData2;
    this.stockVerificationData3 = stockVerificationData3;
    this.reservationVerificationData2 = reservationVerificationData2;
    this.stockVerificationData4 = stockVerificationData4;
    this.stockVerificationData5 = stockVerificationData5;
    this.stockVerificationData6 = stockVerificationData6;
    this.goodsShipmentHeaderData = goodsShipmentHeaderData;
    this.goodsShipmentHeaderVerificationData = goodsShipmentHeaderVerificationData;
    this.goodsShipmentLinesData1 = goodsShipmentLinesData1;
    this.goodsShipmentLineVerificationData1 = goodsShipmentLineVerificationData1;
    this.goodsShipmentLinesData2 = goodsShipmentLinesData2;
    this.goodsShipmentLineVerificationData2 = goodsShipmentLineVerificationData2;
    this.goodsShipmentLinesData3 = goodsShipmentLinesData3;
    this.goodsShipmentLineVerificationData3 = goodsShipmentLineVerificationData3;
    this.goodsShipmentLinesData4 = goodsShipmentLinesData4;
    this.goodsShipmentLineVerificationData4 = goodsShipmentLineVerificationData4;
    this.goodsShipmentLinesData5 = goodsShipmentLinesData5;
    this.goodsShipmentLineVerificationData5 = goodsShipmentLineVerificationData5;
    this.goodsShipmentLinesData6 = goodsShipmentLinesData6;
    this.goodsShipmentLineVerificationData6 = goodsShipmentLineVerificationData6;
    this.completedGoodsShipmentHeaderVerificationData = completedGoodsShipmentHeaderVerificationData;
    this.reservationVerificationData3 = reservationVerificationData3;
    this.stockVerificationData7 = stockVerificationData7;
    this.stockVerificationData8 = stockVerificationData8;
    this.stockVerificationData9 = stockVerificationData9;
    this.reservationVerificationData4 = reservationVerificationData4;
    this.stockVerificationData10 = stockVerificationData10;
    this.stockVerificationData11 = stockVerificationData11;
    this.stockVerificationData12 = stockVerificationData12;
    this.goodsShipmentLinesData7 = goodsShipmentLinesData7;
    this.goodsShipmentLineVerificationData7 = goodsShipmentLineVerificationData7;
    this.goodsShipmentLinesData8 = goodsShipmentLinesData8;
    this.goodsShipmentLineVerificationData8 = goodsShipmentLineVerificationData8;
    this.goodsShipmentLinesData9 = goodsShipmentLinesData9;
    this.goodsShipmentLineVerificationData9 = goodsShipmentLineVerificationData9;
    this.goodsShipmentLinesData10 = goodsShipmentLinesData10;
    this.goodsShipmentLineVerificationData10 = goodsShipmentLineVerificationData10;
    this.reservationVerificationData5 = reservationVerificationData5;
    this.stockVerificationData13 = stockVerificationData13;
    this.stockVerificationData14 = stockVerificationData14;
    this.stockVerificationData15 = stockVerificationData15;
    this.reservationVerificationData6 = reservationVerificationData6;
    this.stockVerificationData16 = stockVerificationData16;
    this.stockVerificationData17 = stockVerificationData17;
    this.stockVerificationData18 = stockVerificationData18;
    this.goodsShipmentLineVerificationData11 = goodsShipmentLineVerificationData11;
    this.goodsShipmentLineVerificationData12 = goodsShipmentLineVerificationData12;
    this.reservationVerificationData7 = reservationVerificationData7;
    this.stockVerificationData19 = stockVerificationData19;
    this.stockVerificationData20 = stockVerificationData20;
    this.stockVerificationData21 = stockVerificationData21;
    this.reservationVerificationData8 = reservationVerificationData8;
    this.stockVerificationData22 = stockVerificationData22;
    this.stockVerificationData23 = stockVerificationData23;
    this.stockVerificationData24 = stockVerificationData24;
    this.completedGoodsShipmentHeaderVerificationData2 = completedGoodsShipmentHeaderVerificationData2;
    this.reservationData = reservationData;
    this.reservationVerificationData9 = reservationVerificationData9;
    this.stockVerificationData25 = stockVerificationData25;
    this.reservationVerificationData10 = reservationVerificationData10;
    this.stockVerificationData26 = stockVerificationData26;
    this.reservationData2 = reservationData2;
    this.reservationVerificationData11 = reservationVerificationData11;
    this.stockVerificationData27 = stockVerificationData27;
    this.reservationVerificationData12 = reservationVerificationData12;
    this.stockVerificationData28 = stockVerificationData28;
    this.reservationVerificationData13 = reservationVerificationData13;
    this.stockVerificationData29 = stockVerificationData29;
    this.stockVerificationData30 = stockVerificationData30;
    this.reservationVerificationData14 = reservationVerificationData14;
    this.stockData1 = stockData1;
    this.stockVerificationData31 = stockVerificationData31;
    this.stockData2 = stockData2;
    this.stockVerificationData32 = stockVerificationData32;
    this.reservationVerificationData15 = reservationVerificationData15;
    this.stockVerificationData33 = stockVerificationData33;
    this.stockVerificationData34 = stockVerificationData34;
    this.reservationVerificationData16 = reservationVerificationData16;
    this.stockVerificationData35 = stockVerificationData35;
    this.stockVerificationData36 = stockVerificationData36;
    this.goodsShipmentLinesData11 = goodsShipmentLinesData11;
    this.goodsShipmentLineVerificationData13 = goodsShipmentLineVerificationData13;
    this.goodsShipmentLinesData12 = goodsShipmentLinesData12;
    this.goodsShipmentLineVerificationData14 = goodsShipmentLineVerificationData14;
    this.goodsShipmentLinesData13 = goodsShipmentLinesData13;
    this.goodsShipmentLineVerificationData15 = goodsShipmentLineVerificationData15;
    this.goodsShipmentLinesData14 = goodsShipmentLinesData14;
    this.goodsShipmentLineVerificationData16 = goodsShipmentLineVerificationData16;
    this.goodsShipmentLinesData15 = goodsShipmentLinesData15;
    this.goodsShipmentLineVerificationData17 = goodsShipmentLineVerificationData17;
    this.goodsShipmentLinesData16 = goodsShipmentLinesData16;
    this.goodsShipmentLineVerificationData18 = goodsShipmentLineVerificationData18;
    this.reservationData3 = reservationData3;
    this.reservationData4 = reservationData4;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new ProductData.Builder().organization("Spain")
            .uOM("Unit")
            .productCategory("Finished Goods")
            .taxCategory("VAT 10%")
            .purchase(false)
            .sale(true)
            .productType("Item")
            .stocked(true)
            .build(),
        new PriceData.Builder().priceListVersion("Customer A")
            .standardPrice("2.00")
            .listPrice("2.00")
            .build(),

        new GoodsReceiptHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),
        new GoodsReceiptHeaderData.Builder().organization("Spain")
            .documentType("MM Receipt")
            .warehouse("Spain warehouse")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .build(),
        new GoodsReceiptLinesData.Builder().storageBin("L01").build(),
        new GoodsReceiptLinesData.Builder().lineNo("10").uOM("Unit").organization("Spain").build(),
        new GoodsReceiptLinesData.Builder().storageBin("L02").build(),
        new GoodsReceiptLinesData.Builder().lineNo("20").uOM("Unit").organization("Spain").build(),
        new GoodsReceiptLinesData.Builder().storageBin("L03").build(),
        new GoodsReceiptLinesData.Builder().lineNo("30").uOM("Unit").organization("Spain").build(),
        new GoodsReceiptLinesData.Builder().storageBin("L01").build(),
        new GoodsReceiptLinesData.Builder().lineNo("40").uOM("Unit").organization("Spain").build(),
        new GoodsReceiptLinesData.Builder().storageBin("L02").build(),
        new GoodsReceiptLinesData.Builder().lineNo("50").uOM("Unit").organization("Spain").build(),
        new GoodsReceiptLinesData.Builder().storageBin("L03").build(),
        new GoodsReceiptLinesData.Builder().lineNo("60").uOM("Unit").organization("Spain").build(),
        new GoodsReceiptHeaderData.Builder().documentStatus("Completed").build(),

        new SalesOrderHeaderData.Builder().transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new SalesOrderHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentMethod("1 (Spain)")
            .paymentTerms("30 days, 5")
            .warehouse("Spain warehouse")
            .invoiceTerms("Customer Schedule After Delivery")
            .invoiceAddress(".Pamplona, Street Customer center nº1")
            .build(),
        new SalesOrderLinesData.Builder().orderedQuantity("90").build(),
        new SalesOrderLinesData.Builder().uOM("Unit")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("180.00")
            .build(),
        new SalesOrderLinesData.Builder().orderedQuantity("60").build(),
        new SalesOrderLinesData.Builder().uOM("Unit")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("120.00")
            .build(),
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("300.00")
            .grandTotalAmount("330.00")
            .currency("EUR")
            .build(),

        new ReservationData.Builder().organization("Spain")
            .quantity("90")
            .reservedQuantity("90")
            .releasedQuantity("0")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Completed")
            .build(),
        new StockData.Builder().storageBin("L01")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("15")
            .allocated(false)
            .build(),
        new StockData.Builder().storageBin("L02")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("30")
            .allocated(false)
            .build(),
        new StockData.Builder().storageBin("L03")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("45")
            .allocated(false)
            .build(),
        new ReservationData.Builder().organization("Spain")
            .quantity("60")
            .reservedQuantity("60")
            .releasedQuantity("0")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Completed")
            .build(),
        new StockData.Builder().storageBin("L01")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("10")
            .allocated(false)
            .build(),
        new StockData.Builder().storageBin("L02")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("20")
            .allocated(false)
            .build(),
        new StockData.Builder().storageBin("L03")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("30")
            .allocated(false)
            .build(),

        new GoodsShipmentHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new GoodsShipmentHeaderData.Builder().documentType("MM Shipment")
            .warehouse("Spain warehouse")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .build(),
        new GoodsShipmentLinesData.Builder().movementQuantity("15").build(),
        new GoodsShipmentLinesData.Builder().lineNo("10")
            .movementQuantity("15")
            .uOM("Unit")
            .storageBin("L01")
            .organization("Spain")
            .build(),
        new GoodsShipmentLinesData.Builder().movementQuantity("10").build(),
        new GoodsShipmentLinesData.Builder().lineNo("20")
            .movementQuantity("10")
            .uOM("Unit")
            .storageBin("L01")
            .organization("Spain")
            .build(),
        new GoodsShipmentLinesData.Builder().movementQuantity("25").build(),
        new GoodsShipmentLinesData.Builder().lineNo("30")
            .movementQuantity("25")
            .uOM("Unit")
            .storageBin("L02")
            .organization("Spain")
            .build(),
        new GoodsShipmentLinesData.Builder().movementQuantity("15").build(),
        new GoodsShipmentLinesData.Builder().lineNo("40")
            .movementQuantity("15")
            .uOM("Unit")
            .storageBin("L02")
            .organization("Spain")
            .build(),
        new GoodsShipmentLinesData.Builder().movementQuantity("30").build(),
        new GoodsShipmentLinesData.Builder().lineNo("50")
            .movementQuantity("30")
            .uOM("Unit")
            .storageBin("L03")
            .organization("Spain")
            .build(),
        new GoodsShipmentLinesData.Builder().movementQuantity("20").build(),
        new GoodsShipmentLinesData.Builder().lineNo("60")
            .movementQuantity("20")
            .uOM("Unit")
            .storageBin("L03")
            .organization("Spain")
            .build(),
        new GoodsShipmentHeaderData.Builder().documentStatus("Completed").build(),

        new ReservationData.Builder().organization("Spain")
            .quantity("90")
            .reservedQuantity("90")
            .releasedQuantity("70")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Completed")
            .build(),
        new StockData.Builder().storageBin("L01")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("15")
            .releasedQuantity("15")
            .allocated(false)
            .build(),
        new StockData.Builder().storageBin("L02")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("30")
            .releasedQuantity("25")
            .allocated(false)
            .build(),
        new StockData.Builder().storageBin("L03")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("45")
            .releasedQuantity("30")
            .allocated(false)
            .build(),
        new ReservationData.Builder().organization("Spain")
            .quantity("60")
            .reservedQuantity("60")
            .releasedQuantity("45")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Completed")
            .build(),
        new StockData.Builder().storageBin("L01")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("10")
            .releasedQuantity("10")
            .allocated(false)
            .build(),
        new StockData.Builder().storageBin("L02")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("20")
            .releasedQuantity("15")
            .allocated(false)
            .build(),
        new StockData.Builder().storageBin("L03")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("30")
            .releasedQuantity("20")
            .allocated(false)
            .build(),

        new GoodsShipmentLinesData.Builder().movementQuantity("5").build(),
        new GoodsShipmentLinesData.Builder().lineNo("10")
            .movementQuantity("5")
            .uOM("Unit")
            .storageBin("L02")
            .organization("Spain")
            .build(),
        new GoodsShipmentLinesData.Builder().movementQuantity("5").build(),
        new GoodsShipmentLinesData.Builder().lineNo("20")
            .movementQuantity("5")
            .uOM("Unit")
            .storageBin("L02")
            .organization("Spain")
            .build(),
        new GoodsShipmentLinesData.Builder().movementQuantity("10").build(),
        new GoodsShipmentLinesData.Builder().lineNo("30")
            .movementQuantity("10")
            .uOM("Unit")
            .storageBin("L03")
            .organization("Spain")
            .build(),
        new GoodsShipmentLinesData.Builder().movementQuantity("5").build(),
        new GoodsShipmentLinesData.Builder().lineNo("40")
            .movementQuantity("5")
            .uOM("Unit")
            .storageBin("L03")
            .organization("Spain")
            .build(),

        new ReservationData.Builder().organization("Spain")
            .quantity("90")
            .reservedQuantity("90")
            .releasedQuantity("85")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Completed")
            .build(),
        new StockData.Builder().storageBin("L01")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("15")
            .releasedQuantity("15")
            .allocated(false)
            .build(),
        new StockData.Builder().storageBin("L02")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("30")
            .releasedQuantity("30")
            .allocated(false)
            .build(),
        new StockData.Builder().storageBin("L03")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("45")
            .releasedQuantity("40")
            .allocated(false)
            .build(),
        new ReservationData.Builder().organization("Spain")
            .quantity("60")
            .reservedQuantity("60")
            .releasedQuantity("55")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Completed")
            .build(),
        new StockData.Builder().storageBin("L01")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("10")
            .releasedQuantity("10")
            .allocated(false)
            .build(),
        new StockData.Builder().storageBin("L02")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("20")
            .releasedQuantity("20")
            .allocated(false)
            .build(),
        new StockData.Builder().storageBin("L03")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("30")
            .releasedQuantity("25")
            .allocated(false)
            .build(),

        new GoodsShipmentLinesData.Builder().lineNo("10")
            .movementQuantity("5")
            .uOM("Unit")
            .storageBin("L03")
            .organization("Spain")
            .build(),
        new GoodsShipmentLinesData.Builder().lineNo("20")
            .movementQuantity("5")
            .uOM("Unit")
            .storageBin("L03")
            .organization("Spain")
            .build(),

        new ReservationData.Builder().organization("Spain")
            .quantity("90")
            .reservedQuantity("90")
            .releasedQuantity("90")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Closed")
            .build(),
        new StockData.Builder().storageBin("L01")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("15")
            .releasedQuantity("15")
            .allocated(false)
            .build(),
        new StockData.Builder().storageBin("L02")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("30")
            .releasedQuantity("30")
            .allocated(false)
            .build(),
        new StockData.Builder().storageBin("L03")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("45")
            .releasedQuantity("45")
            .allocated(false)
            .build(),
        new ReservationData.Builder().organization("Spain")
            .quantity("60")
            .reservedQuantity("60")
            .releasedQuantity("60")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Closed")
            .build(),
        new StockData.Builder().storageBin("L01")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("10")
            .releasedQuantity("10")
            .allocated(false)
            .build(),
        new StockData.Builder().storageBin("L02")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("20")
            .releasedQuantity("20")
            .allocated(false)
            .build(),
        new StockData.Builder().storageBin("L03")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("30")
            .releasedQuantity("30")
            .allocated(false)
            .build(),

        new GoodsShipmentHeaderData.Builder().documentStatus("Voided").build(),

        new ReservationData.Builder().status("Completed").build(),
        new ReservationData.Builder().organization("Spain")
            .quantity("90")
            .reservedQuantity("5")
            .releasedQuantity("0")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Completed")
            .build(),
        new StockData.Builder().storageBin("L03")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("5")
            .allocated(false)
            .build(),
        new ReservationData.Builder().organization("Spain")
            .quantity("60")
            .reservedQuantity("5")
            .releasedQuantity("0")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Completed")
            .build(),
        new StockData.Builder().storageBin("L03")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("5")
            .allocated(false)
            .build(),

        new ReservationData.Builder().reservedQuantity("5").build(),
        new ReservationData.Builder().organization("Spain")
            .quantity("90")
            .reservedQuantity("5")
            .releasedQuantity("5")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Closed")
            .build(),
        new StockData.Builder().storageBin("L03")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("5")
            .releasedQuantity("5")
            .allocated(false)
            .build(),
        new ReservationData.Builder().organization("Spain")
            .quantity("60")
            .reservedQuantity("5")
            .releasedQuantity("5")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Closed")
            .build(),
        new StockData.Builder().storageBin("L03")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("5")
            .releasedQuantity("5")
            .allocated(false)
            .build(),

        new ReservationData.Builder().organization("Spain")
            .quantity("90")
            .reservedQuantity("15")
            .releasedQuantity("0")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Completed")
            .build(),
        new StockData.Builder().storageBin("L02")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("5")
            .allocated(false)
            .build(),
        new StockData.Builder().storageBin("L03")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("10")
            .allocated(false)
            .build(),
        new ReservationData.Builder().organization("Spain")
            .quantity("60")
            .reservedQuantity("10")
            .releasedQuantity("0")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Completed")
            .build(),
        new StockData.Builder().storageBin("L02").build(),
        new StockData.Builder().storageBin("L02")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("5")
            .allocated(false)
            .build(),
        new StockData.Builder().storageBin("L03").build(),
        new StockData.Builder().storageBin("L03")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("5")
            .allocated(false)
            .build(),

        new ReservationData.Builder().organization("Spain")
            .quantity("90")
            .reservedQuantity("20")
            .releasedQuantity("0")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Completed")
            .build(),
        new StockData.Builder().storageBin("L02")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("5")
            .allocated(false)
            .build(),
        new StockData.Builder().storageBin("L03")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("15")
            .allocated(false)
            .build(),
        new ReservationData.Builder().organization("Spain")
            .quantity("60")
            .reservedQuantity("15")
            .releasedQuantity("0")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Completed")
            .build(),
        new StockData.Builder().storageBin("L02")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("5")
            .allocated(false)
            .build(),
        new StockData.Builder().storageBin("L03")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("10")
            .allocated(false)
            .build(),

        new GoodsShipmentLinesData.Builder().movementQuantity("15").build(),
        new GoodsShipmentLinesData.Builder().lineNo("10")
            .movementQuantity("15")
            .uOM("Unit")
            .storageBin("L01")
            .organization("Spain")
            .build(),
        new GoodsShipmentLinesData.Builder().movementQuantity("10").build(),
        new GoodsShipmentLinesData.Builder().lineNo("20")
            .movementQuantity("10")
            .uOM("Unit")
            .storageBin("L01")
            .organization("Spain")
            .build(),
        new GoodsShipmentLinesData.Builder().movementQuantity("30").build(),
        new GoodsShipmentLinesData.Builder().lineNo("30")
            .movementQuantity("30")
            .uOM("Unit")
            .storageBin("L02")
            .organization("Spain")
            .build(),
        new GoodsShipmentLinesData.Builder().movementQuantity("20").build(),
        new GoodsShipmentLinesData.Builder().lineNo("40")
            .movementQuantity("20")
            .uOM("Unit")
            .storageBin("L02")
            .organization("Spain")
            .build(),
        new GoodsShipmentLinesData.Builder().movementQuantity("45").build(),
        new GoodsShipmentLinesData.Builder().lineNo("50")
            .movementQuantity("45")
            .uOM("Unit")
            .storageBin("L03")
            .organization("Spain")
            .build(),
        new GoodsShipmentLinesData.Builder().movementQuantity("30").build(),
        new GoodsShipmentLinesData.Builder().lineNo("60")
            .movementQuantity("30")
            .uOM("Unit")
            .storageBin("L03")
            .organization("Spain")
            .build(),

        new ReservationData.Builder().reservedQuantity("90").build(),
        new ReservationData.Builder().reservedQuantity("60").build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 31876 - 31902 - 31956
   *
   * @throws ParseException
   */
  @Test
  public void RESRegression31876R31902R31956Test() throws ParseException {
    logger.info(
        "** Start of test case [RESRegression31876R31902R31956] Test regression 31876 - 31902 - 31956. **");

    String productName1 = "Test31876A-";
    int num1 = Product.ProductTab.getRecordCount(mainPage,
        new ProductData.Builder().name(productName1).build());
    productName1 = productName1 + String.valueOf(num1);
    ProductData productData1 = productData;
    productData1.addDataField("searchKey", productName1);
    productData1.addDataField("name", productName1);
    Product.ProductTab.create(mainPage, productData1);
    Product.Price.create(mainPage, productPriceData);

    String productName2 = "Test31876B-";
    int num2 = Product.ProductTab.getRecordCount(mainPage,
        new ProductData.Builder().name(productName2).build());
    productName2 = productName2 + String.valueOf(num2);
    ProductData productData2 = productData;
    productData2.addDataField("searchKey", productName2);
    productData2.addDataField("name", productName2);
    Product.ProductTab.create(mainPage, productData2);
    Product.Price.create(mainPage, productPriceData);

    GoodsReceipt goodsReceipt = new GoodsReceipt(mainPage).open();
    goodsReceipt.create(goodsReceiptHeaderData);
    goodsReceipt.assertSaved();
    goodsReceipt.assertData(goodsReceiptHeaderVerificationData);
    GoodsReceipt.Lines goodsReceiptLines1 = goodsReceipt.new Lines(mainPage);
    goodsReceiptLinesData1.addDataField("product",
        new ProductCompleteSelectorData.Builder().name(productName1).build());
    goodsReceiptLinesData1.addDataField("movementQuantity", "15");
    goodsReceiptLines1.create(goodsReceiptLinesData1);
    goodsReceiptLines1.assertSaved();
    goodsReceiptLines1.assertData(goodsReceiptLineVerificationData1);
    GoodsReceipt.Lines goodsReceiptLines2 = goodsReceipt.new Lines(mainPage);
    goodsReceiptLinesData2.addDataField("product",
        new ProductCompleteSelectorData.Builder().name(productName1).build());
    goodsReceiptLinesData2.addDataField("movementQuantity", "30");
    goodsReceiptLines2.create(goodsReceiptLinesData2);
    goodsReceiptLines2.assertSaved();
    goodsReceiptLines2.assertData(goodsReceiptLineVerificationData2);
    GoodsReceipt.Lines goodsReceiptLines3 = goodsReceipt.new Lines(mainPage);
    goodsReceiptLinesData3.addDataField("product",
        new ProductCompleteSelectorData.Builder().name(productName1).build());
    goodsReceiptLinesData3.addDataField("movementQuantity", "45");
    goodsReceiptLines3.create(goodsReceiptLinesData3);
    goodsReceiptLines3.assertSaved();
    goodsReceiptLines3.assertData(goodsReceiptLineVerificationData3);
    GoodsReceipt.Lines goodsReceiptLines4 = goodsReceipt.new Lines(mainPage);
    goodsReceiptLinesData4.addDataField("product",
        new ProductCompleteSelectorData.Builder().name(productName2).build());
    goodsReceiptLinesData4.addDataField("movementQuantity", "10");
    goodsReceiptLines4.create(goodsReceiptLinesData4);
    goodsReceiptLines4.assertSaved();
    goodsReceiptLines4.assertData(goodsReceiptLineVerificationData4);
    GoodsReceipt.Lines goodsReceiptLines5 = goodsReceipt.new Lines(mainPage);
    goodsReceiptLinesData5.addDataField("product",
        new ProductCompleteSelectorData.Builder().name(productName2).build());
    goodsReceiptLinesData5.addDataField("movementQuantity", "20");
    goodsReceiptLines5.create(goodsReceiptLinesData5);
    goodsReceiptLines5.assertSaved();
    goodsReceiptLines5.assertData(goodsReceiptLineVerificationData5);
    GoodsReceipt.Lines goodsReceiptLines6 = goodsReceipt.new Lines(mainPage);
    goodsReceiptLinesData6.addDataField("product",
        new ProductCompleteSelectorData.Builder().name(productName2).build());
    goodsReceiptLinesData6.addDataField("movementQuantity", "30");
    goodsReceiptLines6.create(goodsReceiptLinesData6);
    goodsReceiptLines6.assertSaved();
    goodsReceiptLines6.assertData(goodsReceiptLineVerificationData6);
    goodsReceipt.complete();
    goodsReceipt.assertProcessCompletedSuccessfully2();
    goodsReceipt.assertData(completedGoodsReceiptHeaderVerificationData);

    SalesOrder salesOrder = new SalesOrder(mainPage).open();
    salesOrder.create(salesOrderHeaderData);
    salesOrder.assertSaved();
    salesOrder.assertData(salesOrderHeaderVerficationData);
    String orderNo = salesOrder.getData("documentNo").toString();
    SalesOrder.Lines salesOrderLines1 = salesOrder.new Lines(mainPage);
    salesOrderLinesData1.addDataField("product",
        new ProductCompleteSelectorData.Builder().name(productName1).build());
    salesOrderLines1.create(salesOrderLinesData1);
    salesOrderLines1.assertSaved();
    salesOrderLines1.assertData(salesOrderLinesVerificationData1);
    String orderLine1Identifier = String.format(" - %s - %s", salesOrderLines1.getData("lineNo"),
        salesOrderLines1.getData("lineNetAmount"));
    SalesOrder.Lines salesOrderLines2 = salesOrder.new Lines(mainPage);
    salesOrderLinesData2.addDataField("product",
        new ProductCompleteSelectorData.Builder().name(productName2).build());
    salesOrderLines2.create(salesOrderLinesData2);
    salesOrderLines2.assertSaved();
    salesOrderLines2.assertData(salesOrderLinesVerificationData2);
    String orderLine2Identifier = String.format(" - %s - %s", salesOrderLines2.getData("lineNo"),
        salesOrderLines2.getData("lineNetAmount"));
    salesOrder.book();
    salesOrder.assertProcessCompletedSuccessfully2();
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);
    String orderIdentifier = String.format("%s - %s - %s", orderNo, salesOrder.getData("orderDate"),
        bookedSalesOrderHeaderVerificationData.getDataField("grandTotalAmount"));
    orderLine1Identifier = orderIdentifier + orderLine1Identifier;
    orderLine2Identifier = orderIdentifier + orderLine2Identifier;

    salesOrderLines1.select(salesOrderLinesData1);
    salesOrderLines1.openSelectedRecord();
    salesOrderLines1.manageReservation();
    // TODO L1: Remove the following static sleep
    Sleep.trySleep(5000);
    salesOrderLines2.select(salesOrderLinesData2);
    salesOrderLines1.openSelectedRecord();
    salesOrderLines2.manageReservation();
    // TODO L: Remove the following static sleep
    Sleep.trySleep(5000);
    StockReservation stockReservation1 = new StockReservation(mainPage).open();
    // TODO L: Remove the following static sleep
    Sleep.trySleep(5000);
    stockReservation1
        .select(new ReservationData.Builder().salesOrderLine(orderLine1Identifier).build());
    // TODO L: Remove the following static sleep
    Sleep.trySleep(5000);
    stockReservation1.assertData(
        (ReservationData) reservationVerificationData1.addDataField("product", productName1)
            .addDataField("salesOrderLine", orderLine1Identifier));
    // TODO L: Remove the following static sleep
    Sleep.trySleep(5000);
    StockReservation.Stock stock1 = stockReservation1.new Stock(mainPage);
    stock1.assertCount(3);
    stock1.selectWithoutFiltering(0);
    stock1.assertData(stockVerificationData1);
    stock1.selectWithoutFiltering(1);
    stock1.assertData(stockVerificationData2);
    stock1.selectWithoutFiltering(2);
    stock1.assertData(stockVerificationData3);
    StockReservation stockReservation2 = new StockReservation(mainPage).open();
    stockReservation2
        .select(new ReservationData.Builder().salesOrderLine(orderLine2Identifier).build());
    stockReservation2.assertData(
        (ReservationData) reservationVerificationData2.addDataField("product", productName2)
            .addDataField("salesOrderLine", orderLine2Identifier));
    StockReservation.Stock stock2 = stockReservation2.new Stock(mainPage);
    stock2.assertCount(3);
    stock2.selectWithoutFiltering(0);
    stock2.assertData(stockVerificationData4);
    stock2.selectWithoutFiltering(1);
    stock2.assertData(stockVerificationData5);
    stock2.selectWithoutFiltering(2);
    stock2.assertData(stockVerificationData6);

    GoodsShipment goodsShipment1 = new GoodsShipment(mainPage).open();
    goodsShipment1.create(goodsShipmentHeaderData);
    goodsShipment1.assertSaved();
    goodsShipment1.assertData(goodsShipmentHeaderVerificationData);
    goodsShipment1.createLinesFrom("L01", orderIdentifier);
    goodsShipment1.assertProcessCompletedSuccessfully();
    goodsShipment1.createLinesFrom("L02", orderIdentifier);
    goodsShipment1.assertProcessCompletedSuccessfully();
    goodsShipment1.createLinesFrom("L03", orderIdentifier);
    goodsShipment1.assertProcessCompletedSuccessfully();
    String shipment1No = goodsShipment1.getData("documentNo").toString();
    GoodsShipment.Lines goodsShipmentLines1 = goodsShipment1.new Lines(mainPage);
    goodsShipmentLines1.assertCount(6);
    goodsShipmentLines1.selectWithoutFiltering(0);
    goodsShipmentLines1.edit(goodsShipmentLinesData1);
    goodsShipmentLines1.assertData((GoodsShipmentLinesData) goodsShipmentLineVerificationData1
        .addDataField("product", productName1)
        .addDataField("salesOrderLine", orderLine1Identifier));
    goodsShipmentLines1.selectWithoutFiltering(1);
    goodsShipmentLines1.edit(goodsShipmentLinesData2);
    goodsShipmentLines1.assertData((GoodsShipmentLinesData) goodsShipmentLineVerificationData2
        .addDataField("product", productName2)
        .addDataField("salesOrderLine", orderLine2Identifier));
    goodsShipmentLines1.selectWithoutFiltering(2);
    goodsShipmentLines1.edit(goodsShipmentLinesData3);
    goodsShipmentLines1.assertData((GoodsShipmentLinesData) goodsShipmentLineVerificationData3
        .addDataField("product", productName1)
        .addDataField("salesOrderLine", orderLine1Identifier));
    goodsShipmentLines1.selectWithoutFiltering(3);
    goodsShipmentLines1.edit(goodsShipmentLinesData4);
    goodsShipmentLines1.assertData((GoodsShipmentLinesData) goodsShipmentLineVerificationData4
        .addDataField("product", productName2)
        .addDataField("salesOrderLine", orderLine2Identifier));
    goodsShipmentLines1.selectWithoutFiltering(4);
    goodsShipmentLines1.edit(goodsShipmentLinesData5);
    goodsShipmentLines1.assertData((GoodsShipmentLinesData) goodsShipmentLineVerificationData5
        .addDataField("product", productName1)
        .addDataField("salesOrderLine", orderLine1Identifier));
    goodsShipmentLines1.selectWithoutFiltering(5);
    goodsShipmentLines1.edit(goodsShipmentLinesData6);
    goodsShipmentLines1.assertData((GoodsShipmentLinesData) goodsShipmentLineVerificationData6
        .addDataField("product", productName2)
        .addDataField("salesOrderLine", orderLine2Identifier));
    goodsShipment1.complete();
    goodsShipment1.assertProcessCompletedSuccessfully2();
    goodsShipment1.assertData((GoodsShipmentHeaderData) completedGoodsShipmentHeaderVerificationData
        .addDataField("salesOrder", orderIdentifier));

    stockReservation1 = new StockReservation(mainPage).open();
    stockReservation1
        .select(new ReservationData.Builder().salesOrderLine(orderLine1Identifier).build());
    stockReservation1.assertData(
        (ReservationData) reservationVerificationData3.addDataField("product", productName1)
            .addDataField("salesOrderLine", orderLine1Identifier));
    stock1 = stockReservation1.new Stock(mainPage);
    stock1.assertCount(3);
    stock1.selectWithoutFiltering(0);
    stock1.assertData(stockVerificationData7);
    stock1.selectWithoutFiltering(1);
    stock1.assertData(stockVerificationData8);
    stock1.selectWithoutFiltering(2);
    stock1.assertData(stockVerificationData9);
    stockReservation2 = new StockReservation(mainPage).open();
    stockReservation2
        .select(new ReservationData.Builder().salesOrderLine(orderLine2Identifier).build());
    stockReservation2.assertData(
        (ReservationData) reservationVerificationData4.addDataField("product", productName2)
            .addDataField("salesOrderLine", orderLine2Identifier));
    stock2 = stockReservation2.new Stock(mainPage);
    stock2.assertCount(3);
    stock2.selectWithoutFiltering(0);
    stock2.assertData(stockVerificationData10);
    stock2.selectWithoutFiltering(1);
    stock2.assertData(stockVerificationData11);
    stock2.selectWithoutFiltering(2);
    stock2.assertData(stockVerificationData12);

    GoodsShipment goodsShipment2 = new GoodsShipment(mainPage).open();
    goodsShipment2.create(goodsShipmentHeaderData);
    goodsShipment2.assertSaved();
    goodsShipment2.assertData(goodsShipmentHeaderVerificationData);
    goodsShipment2.createLinesFrom("L02", orderIdentifier);
    goodsShipment2.assertProcessCompletedSuccessfully();
    goodsShipment2.createLinesFrom("L03", orderIdentifier);
    goodsShipment2.assertProcessCompletedSuccessfully();
    String shipment2No = goodsShipment2.getData("documentNo").toString();
    GoodsShipment.Lines goodsShipmentLines2 = goodsShipment2.new Lines(mainPage);
    goodsShipmentLines2.assertCount(4);
    goodsShipmentLines2.selectWithoutFiltering(0);
    goodsShipmentLines2.edit(goodsShipmentLinesData7);
    goodsShipmentLines2.assertData((GoodsShipmentLinesData) goodsShipmentLineVerificationData7
        .addDataField("product", productName1)
        .addDataField("salesOrderLine", orderLine1Identifier));
    goodsShipmentLines2.selectWithoutFiltering(1);
    goodsShipmentLines2.edit(goodsShipmentLinesData8);
    goodsShipmentLines2.assertData((GoodsShipmentLinesData) goodsShipmentLineVerificationData8
        .addDataField("product", productName2)
        .addDataField("salesOrderLine", orderLine2Identifier));
    goodsShipmentLines2.selectWithoutFiltering(2);
    goodsShipmentLines2.edit(goodsShipmentLinesData9);
    goodsShipmentLines2.assertData((GoodsShipmentLinesData) goodsShipmentLineVerificationData9
        .addDataField("product", productName1)
        .addDataField("salesOrderLine", orderLine1Identifier));
    goodsShipmentLines2.selectWithoutFiltering(3);
    goodsShipmentLines2.edit(goodsShipmentLinesData10);
    goodsShipmentLines2.assertData((GoodsShipmentLinesData) goodsShipmentLineVerificationData10
        .addDataField("product", productName2)
        .addDataField("salesOrderLine", orderLine2Identifier));
    goodsShipment2.complete();
    goodsShipment2.assertProcessCompletedSuccessfully2();
    goodsShipment2.assertData((GoodsShipmentHeaderData) completedGoodsShipmentHeaderVerificationData
        .addDataField("salesOrder", orderIdentifier));

    stockReservation1 = new StockReservation(mainPage).open();
    stockReservation1
        .select(new ReservationData.Builder().salesOrderLine(orderLine1Identifier).build());
    stockReservation1.assertData(
        (ReservationData) reservationVerificationData5.addDataField("product", productName1)
            .addDataField("salesOrderLine", orderLine1Identifier));
    stock1 = stockReservation1.new Stock(mainPage);
    stock1.assertCount(3);
    stock1.selectWithoutFiltering(0);
    stock1.assertData(stockVerificationData13);
    stock1.selectWithoutFiltering(1);
    stock1.assertData(stockVerificationData14);
    stock1.selectWithoutFiltering(2);
    stock1.assertData(stockVerificationData15);
    stockReservation2 = new StockReservation(mainPage).open();
    stockReservation2
        .select(new ReservationData.Builder().salesOrderLine(orderLine2Identifier).build());
    stockReservation2.assertData(
        (ReservationData) reservationVerificationData6.addDataField("product", productName2)
            .addDataField("salesOrderLine", orderLine2Identifier));
    stock2 = stockReservation2.new Stock(mainPage);
    stock2.assertCount(3);
    stock2.selectWithoutFiltering(0);
    stock2.assertData(stockVerificationData16);
    stock2.selectWithoutFiltering(1);
    stock2.assertData(stockVerificationData17);
    stock2.selectWithoutFiltering(2);
    stock2.assertData(stockVerificationData18);

    GoodsShipment goodsShipment3 = new GoodsShipment(mainPage).open();
    goodsShipment3.create(goodsShipmentHeaderData);
    goodsShipment3.assertSaved();
    goodsShipment3.assertData(goodsShipmentHeaderVerificationData);
    goodsShipment3.createLinesFrom("L03", orderIdentifier);
    goodsShipment3.assertProcessCompletedSuccessfully();
    String shipment3No = goodsShipment3.getData("documentNo").toString();
    GoodsShipment.Lines goodsShipmentLines3 = goodsShipment3.new Lines(mainPage);
    goodsShipmentLines3.assertCount(2);
    goodsShipmentLines3.selectWithoutFiltering(0);
    goodsShipmentLines3.assertData((GoodsShipmentLinesData) goodsShipmentLineVerificationData11
        .addDataField("product", productName1)
        .addDataField("salesOrderLine", orderLine1Identifier));
    goodsShipmentLines3.selectWithoutFiltering(1);
    goodsShipmentLines3.assertData((GoodsShipmentLinesData) goodsShipmentLineVerificationData12
        .addDataField("product", productName2)
        .addDataField("salesOrderLine", orderLine2Identifier));
    goodsShipment3.complete();
    goodsShipment3.assertProcessCompletedSuccessfully2();
    goodsShipment3.assertData((GoodsShipmentHeaderData) completedGoodsShipmentHeaderVerificationData
        .addDataField("salesOrder", orderIdentifier));

    stockReservation1 = new StockReservation(mainPage).open();
    stockReservation1
        .select(new ReservationData.Builder().salesOrderLine(orderLine1Identifier).build());
    stockReservation1.assertData(
        (ReservationData) reservationVerificationData7.addDataField("product", productName1)
            .addDataField("salesOrderLine", orderLine1Identifier));
    stock1 = stockReservation1.new Stock(mainPage);
    stock1.assertCount(3);
    stock1.selectWithoutFiltering(0);
    stock1.assertData(stockVerificationData19);
    stock1.selectWithoutFiltering(1);
    stock1.assertData(stockVerificationData20);
    stock1.selectWithoutFiltering(2);
    stock1.assertData(stockVerificationData21);
    stockReservation2 = new StockReservation(mainPage).open();
    stockReservation2
        .select(new ReservationData.Builder().salesOrderLine(orderLine2Identifier).build());
    stockReservation2.assertData(
        (ReservationData) reservationVerificationData8.addDataField("product", productName2)
            .addDataField("salesOrderLine", orderLine2Identifier));
    stock2 = stockReservation2.new Stock(mainPage);
    stock2.assertCount(3);
    stock2.selectWithoutFiltering(0);
    stock2.assertData(stockVerificationData22);
    stock2.selectWithoutFiltering(1);
    stock2.assertData(stockVerificationData23);
    stock2.selectWithoutFiltering(2);
    stock2.assertData(stockVerificationData24);

    goodsShipment3 = new GoodsShipment(mainPage).open();
    goodsShipment3.select(new GoodsShipmentHeaderData.Builder().documentNo(shipment3No).build());
    goodsShipment3.close();
    goodsShipment3.assertProcessCompletedSuccessfully2();
    goodsShipment3.assertData(completedGoodsShipmentHeaderVerificationData2);

    stockReservation1 = new StockReservation(mainPage).open();
    stockReservation1.select(
        (ReservationData) reservationData.addDataField("salesOrderLine", orderLine1Identifier));
    stockReservation1.assertData(
        (ReservationData) reservationVerificationData9.addDataField("product", productName1)
            .addDataField("salesOrderLine", orderLine1Identifier));
    stock1 = stockReservation1.new Stock(mainPage);
    stock1.assertCount(1);
    stock1.selectWithoutFiltering(0);
    stock1.assertData(stockVerificationData25);
    stockReservation2 = new StockReservation(mainPage).open();
    stockReservation2.select(
        (ReservationData) reservationData.addDataField("salesOrderLine", orderLine2Identifier)
            .addDataField("rESStatus", "Completed")
            .addDataField("warehouse", ""));
    // TODO L: Remove the following static sleep
    Sleep.trySleep(5000);
    stockReservation2.assertData(
        (ReservationData) reservationVerificationData10.addDataField("product", productName2)
            .addDataField("salesOrderLine", orderLine2Identifier));
    stock2 = stockReservation2.new Stock(mainPage);
    stock2.assertCount(1);
    stock2.selectWithoutFiltering(0);
    stock2.assertData(stockVerificationData26);

    GoodsShipment goodsShipment4 = new GoodsShipment(mainPage).open();
    goodsShipment4.create(goodsShipmentHeaderData);
    goodsShipment4.assertSaved();
    goodsShipment4.assertData(goodsShipmentHeaderVerificationData);
    goodsShipment4.createLinesFrom("L03", orderIdentifier);
    goodsShipment4.assertProcessCompletedSuccessfully();
    String shipment4No = goodsShipment4.getData("documentNo").toString();
    GoodsShipment.Lines goodsShipmentLines4 = goodsShipment4.new Lines(mainPage);
    goodsShipmentLines4.assertCount(2);
    goodsShipmentLines4.selectWithoutFiltering(0);
    goodsShipmentLines4.assertData((GoodsShipmentLinesData) goodsShipmentLineVerificationData11
        .addDataField("product", productName1)
        .addDataField("salesOrderLine", orderLine1Identifier));
    goodsShipmentLines4.selectWithoutFiltering(1);
    goodsShipmentLines4.assertData((GoodsShipmentLinesData) goodsShipmentLineVerificationData12
        .addDataField("product", productName2)
        .addDataField("salesOrderLine", orderLine2Identifier));
    goodsShipment4.complete();
    goodsShipment4.assertProcessCompletedSuccessfully2();
    goodsShipment4.assertData((GoodsShipmentHeaderData) completedGoodsShipmentHeaderVerificationData
        .addDataField("salesOrder", orderIdentifier));

    stockReservation1 = new StockReservation(mainPage).open();
    stockReservation1.select(
        (ReservationData) reservationData2.addDataField("salesOrderLine", orderLine1Identifier));
    stockReservation1.assertData(
        (ReservationData) reservationVerificationData11.addDataField("product", productName1)
            .addDataField("salesOrderLine", orderLine1Identifier));
    stock1 = stockReservation1.new Stock(mainPage);
    stock1.assertCount(1);
    stock1.selectWithoutFiltering(0);
    stock1.assertData(stockVerificationData27);
    stockReservation2 = new StockReservation(mainPage).open();
    stockReservation2.select(
        (ReservationData) reservationData2.addDataField("salesOrderLine", orderLine2Identifier));
    stockReservation2.assertData(
        (ReservationData) reservationVerificationData12.addDataField("product", productName2)
            .addDataField("salesOrderLine", orderLine2Identifier));
    stock2 = stockReservation2.new Stock(mainPage);
    stock2.assertCount(1);
    stock2.selectWithoutFiltering(0);
    stock2.assertData(stockVerificationData28);

    goodsShipment2 = new GoodsShipment(mainPage).open();
    goodsShipment2.select(new GoodsShipmentHeaderData.Builder().documentNo(shipment2No).build());
    goodsShipment2.close();
    goodsShipment2.assertProcessCompletedSuccessfully2();
    goodsShipment2.assertData(completedGoodsShipmentHeaderVerificationData2);

    stockReservation1 = new StockReservation(mainPage).open();
    stockReservation1.select(
        (ReservationData) reservationData.addDataField("salesOrderLine", orderLine1Identifier)
            .addDataField("warehouse", ""));
    stockReservation1.assertData(
        (ReservationData) reservationVerificationData13.addDataField("product", productName1)
            .addDataField("salesOrderLine", orderLine1Identifier));
    stock1 = stockReservation1.new Stock(mainPage);
    stock1.assertCount(2);
    stock1.selectWithoutFiltering(0);
    stock1.assertData(stockVerificationData29);
    stock1.selectWithoutFiltering(1);
    stock1.assertData(stockVerificationData30);
    stockReservation2 = new StockReservation(mainPage).open();
    // TODO L: Remove the following static sleep
    Sleep.trySleep(5000);
    stockReservation2.select(
        (ReservationData) reservationData.addDataField("salesOrderLine", orderLine2Identifier)
            .addDataField("rESStatus", "Completed")
            .addDataField("warehouse", ""));
    // TODO L: Remove the following static sleep
    Sleep.trySleep(5000);
    stockReservation2.assertData(
        (ReservationData) reservationVerificationData14.addDataField("product", productName2)
            .addDataField("salesOrderLine", orderLine2Identifier));
    // TODO L: Remove the following static sleep
    Sleep.trySleep(5000);
    stock2 = stockReservation2.new Stock(mainPage);
    stock2.assertCount(2);
    stock2.select(stockData1);
    stock2.assertData(stockVerificationData31);
    stock2.cancelOnGrid();
    stock2.getTab().clearFilters();
    stock2.getTab().unselectAll();
    stock2.select(stockData2);
    stock2.assertData(stockVerificationData32);

    goodsShipment4 = new GoodsShipment(mainPage).open();
    goodsShipment4.select(new GoodsShipmentHeaderData.Builder().documentNo(shipment4No).build());
    goodsShipment4.close();
    goodsShipment4.assertProcessCompletedSuccessfully2();
    goodsShipment4.assertData(completedGoodsShipmentHeaderVerificationData2);

    stockReservation1 = new StockReservation(mainPage).open();
    stockReservation1.select(
        (ReservationData) reservationData.addDataField("salesOrderLine", orderLine1Identifier)
            .addDataField("warehouse", ""));
    stockReservation1.assertData(
        (ReservationData) reservationVerificationData15.addDataField("product", productName1)
            .addDataField("salesOrderLine", orderLine1Identifier));
    stock1 = stockReservation1.new Stock(mainPage);
    stock1.assertCount(2);
    stock1.selectWithoutFiltering(0);
    stock1.assertData(stockVerificationData33);
    stock1.selectWithoutFiltering(1);
    stock1.assertData(stockVerificationData34);
    stockReservation2 = new StockReservation(mainPage).open();
    stockReservation2.select(
        (ReservationData) reservationData.addDataField("salesOrderLine", orderLine2Identifier)
            .addDataField("rESStatus", "Completed")
            .addDataField("warehouse", ""));
    // TODO L: Remove the following static sleep
    Sleep.trySleep(5000);
    stockReservation2.assertData(
        (ReservationData) reservationVerificationData16.addDataField("product", productName2)
            .addDataField("salesOrderLine", orderLine2Identifier));
    stock2 = stockReservation2.new Stock(mainPage);
    stock2.assertCount(2);
    stock2.selectWithoutFiltering(0);
    stock2.assertData(stockVerificationData35);
    stock2.selectWithoutFiltering(1);
    stock2.assertData(stockVerificationData36);

    goodsShipment1 = new GoodsShipment(mainPage).open();
    goodsShipment1.select(new GoodsShipmentHeaderData.Builder().documentNo(shipment1No).build());
    goodsShipment1.close();
    goodsShipment1.assertProcessCompletedSuccessfully2();
    goodsShipment1.assertData(completedGoodsShipmentHeaderVerificationData2);

    stockReservation1 = new StockReservation(mainPage).open();
    stockReservation1.select(
        (ReservationData) reservationData.addDataField("salesOrderLine", orderLine1Identifier)
            .addDataField("rESStatus", "Completed")
            .addDataField("warehouse", ""));
    stockReservation1.assertData(
        (ReservationData) reservationVerificationData1.addDataField("product", productName1)
            .addDataField("salesOrderLine", orderLine1Identifier));
    stock1 = stockReservation1.new Stock(mainPage);
    stock1.assertCount(3);
    stock1.selectWithoutFiltering(0);
    stock1.assertData(stockVerificationData1);
    stock1.selectWithoutFiltering(1);
    stock1.assertData(stockVerificationData2);
    stock1.selectWithoutFiltering(2);
    stock1.assertData(stockVerificationData3);
    stockReservation2 = new StockReservation(mainPage).open();
    stockReservation2.select(
        (ReservationData) reservationData.addDataField("salesOrderLine", orderLine2Identifier)
            .addDataField("rESStatus", "Completed")
            .addDataField("warehouse", ""));
    // TODO L: Remove the following static sleep
    Sleep.trySleep(5000);
    stockReservation2.assertData(
        (ReservationData) reservationVerificationData2.addDataField("product", productName2)
            .addDataField("salesOrderLine", orderLine2Identifier));
    stock2 = stockReservation2.new Stock(mainPage);
    stock2.assertCount(3);
    stock2.selectWithoutFiltering(0);
    stock2.assertData(stockVerificationData4);
    stock2.selectWithoutFiltering(1);
    stock2.assertData(stockVerificationData5);
    stock2.selectWithoutFiltering(2);
    stock2.assertData(stockVerificationData6);

    GoodsShipment goodsShipment5 = new GoodsShipment(mainPage).open();
    goodsShipment5.create(goodsShipmentHeaderData);
    goodsShipment5.assertSaved();
    goodsShipment5.assertData(goodsShipmentHeaderVerificationData);
    goodsShipment5.createLinesFrom("L01", orderIdentifier);
    goodsShipment5.assertProcessCompletedSuccessfully();
    goodsShipment5.createLinesFrom("L02", orderIdentifier);
    goodsShipment5.assertProcessCompletedSuccessfully();
    goodsShipment5.createLinesFrom("L03", orderIdentifier);
    goodsShipment5.assertProcessCompletedSuccessfully();
    String shipment5No = goodsShipment5.getData("documentNo").toString();
    GoodsShipment.Lines goodsShipmentLines5 = goodsShipment5.new Lines(mainPage);
    goodsShipmentLines5.assertCount(6);
    goodsShipmentLines5.selectWithoutFiltering(0);
    goodsShipmentLines5.edit(goodsShipmentLinesData11);
    goodsShipmentLines5.assertData((GoodsShipmentLinesData) goodsShipmentLineVerificationData13
        .addDataField("product", productName1)
        .addDataField("salesOrderLine", orderLine1Identifier));
    goodsShipmentLines5.selectWithoutFiltering(1);
    goodsShipmentLines5.edit(goodsShipmentLinesData12);
    goodsShipmentLines5.assertData((GoodsShipmentLinesData) goodsShipmentLineVerificationData14
        .addDataField("product", productName2)
        .addDataField("salesOrderLine", orderLine2Identifier));
    goodsShipmentLines5.selectWithoutFiltering(2);
    goodsShipmentLines5.edit(goodsShipmentLinesData13);
    goodsShipmentLines5.assertData((GoodsShipmentLinesData) goodsShipmentLineVerificationData15
        .addDataField("product", productName1)
        .addDataField("salesOrderLine", orderLine1Identifier));
    goodsShipmentLines5.selectWithoutFiltering(3);
    goodsShipmentLines5.edit(goodsShipmentLinesData14);
    goodsShipmentLines5.assertData((GoodsShipmentLinesData) goodsShipmentLineVerificationData16
        .addDataField("product", productName2)
        .addDataField("salesOrderLine", orderLine2Identifier));
    goodsShipmentLines5.selectWithoutFiltering(4);
    goodsShipmentLines5.edit(goodsShipmentLinesData15);
    goodsShipmentLines5.assertData((GoodsShipmentLinesData) goodsShipmentLineVerificationData17
        .addDataField("product", productName1)
        .addDataField("salesOrderLine", orderLine1Identifier));
    goodsShipmentLines5.selectWithoutFiltering(5);
    goodsShipmentLines5.edit(goodsShipmentLinesData16);
    goodsShipmentLines5.assertData((GoodsShipmentLinesData) goodsShipmentLineVerificationData18
        .addDataField("product", productName2)
        .addDataField("salesOrderLine", orderLine2Identifier));
    goodsShipment5.complete();
    goodsShipment5.assertProcessCompletedSuccessfully2();
    goodsShipment5.assertData((GoodsShipmentHeaderData) completedGoodsShipmentHeaderVerificationData
        .addDataField("salesOrder", orderIdentifier));

    stockReservation1 = new StockReservation(mainPage).open();
    stockReservation1.select(
        (ReservationData) reservationData3.addDataField("salesOrderLine", orderLine1Identifier));
    stockReservation1.assertCount(2);
    stockReservation1.selectWithoutFiltering(0);
    stockReservation1.assertData(
        (ReservationData) reservationVerificationData7.addDataField("product", productName1)
            .addDataField("salesOrderLine", orderLine1Identifier));
    stock1 = stockReservation1.new Stock(mainPage);
    stock1.assertCount(3);
    stock1.selectWithoutFiltering(0);
    stock1.assertData(stockVerificationData19);
    stock1.selectWithoutFiltering(1);
    stock1.assertData(stockVerificationData20);
    stock1.selectWithoutFiltering(2);
    stock1.assertData(stockVerificationData21);
    stockReservation1.selectWithoutFiltering(1);
    stockReservation1.assertData(
        (ReservationData) reservationVerificationData7.addDataField("product", productName1)
            .addDataField("salesOrderLine", orderLine1Identifier));
    stock1 = stockReservation1.new Stock(mainPage);
    stock1.assertCount(3);
    stock1.selectWithoutFiltering(0);
    stock1.assertData(stockVerificationData19);
    stock1.selectWithoutFiltering(1);
    stock1.assertData(stockVerificationData20);
    stock1.selectWithoutFiltering(2);
    stock1.assertData(stockVerificationData21);
    stockReservation2 = new StockReservation(mainPage).open();
    stockReservation2.select(
        (ReservationData) reservationData4.addDataField("salesOrderLine", orderLine2Identifier));
    stockReservation2.assertCount(2);
    stockReservation2.selectWithoutFiltering(0);
    stockReservation2.assertData(
        (ReservationData) reservationVerificationData8.addDataField("product", productName2)
            .addDataField("salesOrderLine", orderLine2Identifier));
    stock2 = stockReservation2.new Stock(mainPage);
    stock2.assertCount(3);
    stock2.selectWithoutFiltering(0);
    stock2.assertData(stockVerificationData22);
    stock2.selectWithoutFiltering(1);
    stock2.assertData(stockVerificationData23);
    stock2.selectWithoutFiltering(2);
    stock2.assertData(stockVerificationData24);
    stockReservation2.selectWithoutFiltering(1);
    stockReservation2.assertData(
        (ReservationData) reservationVerificationData8.addDataField("product", productName2)
            .addDataField("salesOrderLine", orderLine2Identifier));
    stock2 = stockReservation2.new Stock(mainPage);
    stock2.assertCount(3);
    stock2.selectWithoutFiltering(0);
    stock2.assertData(stockVerificationData22);
    stock2.selectWithoutFiltering(1);
    stock2.assertData(stockVerificationData23);
    stock2.selectWithoutFiltering(2);
    stock2.assertData(stockVerificationData24);

    goodsShipment5 = new GoodsShipment(mainPage).open();
    goodsShipment5.select(new GoodsShipmentHeaderData.Builder().documentNo(shipment5No).build());
    goodsShipment5.close();
    goodsShipment5.assertProcessCompletedSuccessfully2();
    goodsShipment5.assertData(completedGoodsShipmentHeaderVerificationData2);

    stockReservation1 = new StockReservation(mainPage).open();
    stockReservation1.select(
        (ReservationData) reservationData.addDataField("salesOrderLine", orderLine1Identifier)
            .addDataField("warehouse", ""));
    stockReservation1.assertData(
        (ReservationData) reservationVerificationData1.addDataField("product", productName1)
            .addDataField("salesOrderLine", orderLine1Identifier));
    stock1 = stockReservation1.new Stock(mainPage);
    stock1.assertCount(3);
    stock1.selectWithoutFiltering(0);
    stock1.assertData(stockVerificationData1);
    stock1.selectWithoutFiltering(1);
    stock1.assertData(stockVerificationData2);
    stock1.selectWithoutFiltering(2);
    stock1.assertData(stockVerificationData3);
    stockReservation2 = new StockReservation(mainPage).open();
    // TODO L: Remove the following static sleep
    Sleep.trySleep(5000);
    stockReservation2.select(
        (ReservationData) reservationData.addDataField("salesOrderLine", orderLine2Identifier)
            .addDataField("rESStatus", "Completed")
            .addDataField("warehouse", ""));
    // TODO L: Remove the following static sleep
    Sleep.trySleep(5000);
    stockReservation2.assertData(
        (ReservationData) reservationVerificationData2.addDataField("product", productName2)
            .addDataField("salesOrderLine", orderLine2Identifier));
    // TODO L: Remove the following static sleep
    Sleep.trySleep(5000);
    stock2 = stockReservation2.new Stock(mainPage);
    stock2.assertCount(3);
    stock2.selectWithoutFiltering(0);
    stock2.assertData(stockVerificationData4);
    stock2.selectWithoutFiltering(1);
    stock2.assertData(stockVerificationData5);
    stock2.selectWithoutFiltering(2);
    stock2.assertData(stockVerificationData6);

    logger.info(
        "** End of test case [RESRegression31876R31902R31956] Test regression 31876 - 31902 - 31956. **");
  }
}
