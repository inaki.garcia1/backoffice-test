/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.financialaccount;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes actions on Add Payment Out pop up.
 *
 * @author elopio
 *
 */
public class AddPaymentOutPopUp extends OBClassicPopUp {

  /** Component identifiers. */
  /** The identifier of the to be paid to text item. */
  private static final String IDENTIFIER_TEXT_FIELD_TO_BE_PAID_TO = "isc_9";
  /** Identifier of the payment method combo box. */
  private static final String IDENTIFIER_COMBO_BOX_PAYMENT_METHOD = "inpPaymentMethod";
  /** The identifier of the process button. */
  private static final String IDENTIFIER_BUTTON_PROCESS = "buttonProcess";

  /**
   * Formatting string of the xpath to locate a check box in the table, using the purchase invoice
   * number. The parameter is the purchase invoice number.
   */
  private static final String FORMAT_XPATH_CHECK_BOX_BY_PURCHASE_INVOICE_NUMBER = "//div[@id = 'client2']//td[@class = 'DataGrid_Body_Cell' and text() = '%s']/..//input[@type = 'checkbox']";

  /**
   * Class constructor. *
   */
  public AddPaymentOutPopUp() {
    super(TestRegistry.getObjectString(
        "org.openbravo.classicpopup./org.openbravo.advpaymentmngt.ad_actionbutton/AddTransaction.html"));
  }

  /**
   * Select the pop up frame.
   */
  @Override
  protected void selectFrame() {
    SeleniumSingleton.INSTANCE.switchTo().frame("ADDPAYMENT");
  }

  /**
   * Select the PopUp.
   *
   * This function waits until the process button is present and visible.
   *
   */
  public void selectPopUp() {
    selectPopUp(IDENTIFIER_BUTTON_PROCESS);
  }

  /**
   * Add payment out.
   *
   * @param toBePaidTo
   *          The business partner to be paid.
   * @param paymentMethod
   *          The payment method.
   * @param purchaseInvoiceNumber
   *          The number of the purchase invoice.
   * @param action
   *          The action to execute on the document.
   */
  public void addPaymentOut(String toBePaidTo, String paymentMethod, String purchaseInvoiceNumber,
      String action) {
    selectPopUp();
    // FIXME: Adds and extra trySleep before clicking the button
    // to prevent false positives
    Sleep.trySleep();
    SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_TEXT_FIELD_TO_BE_PAID_TO)
        .sendKeys(toBePaidTo);
    // FIXME: Adds and extra trySleep after writing the text
    // to prevent false positives
    Sleep.trySleep();
    // FIXME: Adds and a tab key press and a sleep
    // to prevent false positives
    SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_TEXT_FIELD_TO_BE_PAID_TO)
        .sendKeys(Keys.TAB);
    Sleep.trySleep();

    new Select(SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_COMBO_BOX_PAYMENT_METHOD))
        .selectByVisibleText(paymentMethod);
    // FIXME: Adds and extra trySleep before clicking the button
    // to prevent false positives
    Sleep.trySleep();
    SeleniumSingleton.INSTANCE
        .findElementByXPath(
            String.format(FORMAT_XPATH_CHECK_BOX_BY_PURCHASE_INVOICE_NUMBER, purchaseInvoiceNumber))
        .click();
    // FIXME: Adds and extra trySleep before clicking the button
    // to prevent false positives
    Sleep.trySleep();
    SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_BUTTON_PROCESS).click();
    Sleep.trySleep();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    selectPopUp(BUTTON_OK);
    new Select(SeleniumSingleton.INSTANCE.findElementById("paramActionDocument"))
        .selectByVisibleText(action);
    clickOK();
  }

}
