/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.application.menu;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for MenuData
 *
 * @author plujan
 *
 */
public class MenuData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the eMObuiappView value
     *
     * Description: The view implementation which is shown in the tab of a multi-document-interface
     *
     * @param value
     *          The eMObuiappView value.
     * @return The builder for this class.
     */
    public Builder eMObuiappView(String value) {
      this.dataFields.put("eMObuiappView", value);
      return this;
    }

    /**
     * Set the module value
     *
     * Description: Module
     *
     * @param value
     *          The module value.
     * @return The builder for this class.
     */
    public Builder module(String value) {
      this.dataFields.put("module", value);
      return this;
    }

    /**
     * Set the uRL value
     *
     * Description: An address which can be accessed via internet.
     *
     * @param value
     *          The uRL value.
     * @return The builder for this class.
     */
    public Builder uRL(String value) {
      this.dataFields.put("uRL", value);
      return this;
    }

    /**
     * Set the specialForm value
     *
     * Description: The name of the form being edited.
     *
     * @param value
     *          The specialForm value.
     * @return The builder for this class.
     */
    public Builder specialForm(String value) {
      this.dataFields.put("specialForm", value);
      return this;
    }

    /**
     * Set the process value
     *
     * Description: A series of actions carried out in sequential order.
     *
     * @param value
     *          The process value.
     * @return The builder for this class.
     */
    public Builder process(String value) {
      this.dataFields.put("process", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the summaryLevel value
     *
     * Description: A means of grouping fields in order to view or hide additional information.
     *
     * @param value
     *          The summaryLevel value.
     * @return The builder for this class.
     */
    public Builder summaryLevel(Boolean value) {
      this.dataFields.put("summaryLevel", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the oSTask value
     *
     * Description: The name of an operating task.
     *
     * @param value
     *          The oSTask value.
     * @return The builder for this class.
     */
    public Builder oSTask(String value) {
      this.dataFields.put("oSTask", value);
      return this;
    }

    /**
     * Set the workflow value
     *
     * Description: A set of procedures used to show the path to complete a specified task.
     *
     * @param value
     *          The workflow value.
     * @return The builder for this class.
     */
    public Builder workflow(String value) {
      this.dataFields.put("workflow", value);
      return this;
    }

    /**
     * Set the window value
     *
     * Description: A work area which can be used to create view edit and process a record.
     *
     * @param value
     *          The window value.
     * @return The builder for this class.
     */
    public Builder window(String value) {
      this.dataFields.put("window", value);
      return this;
    }

    /**
     * Set the action value
     *
     * Description: A drop down list box indicating the next step to take.
     *
     * @param value
     *          The action value.
     * @return The builder for this class.
     */
    public Builder action(String value) {
      this.dataFields.put("action", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public MenuData build() {
      return new MenuData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private MenuData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
