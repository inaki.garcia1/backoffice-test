/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.applicationdictionary.windowstabsandfields;

import com.openbravo.test.integration.erp.data.applicationdictionary.windowstabsandfields.TabTranslationData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;

/**
 * Executes and verifies actions on OpenbravoERP Tab Translation tab.
 *
 * @author elopio
 *
 */
public class TabTranslationTab extends GeneratedTab<TabTranslationData> {

  /** The tab title. */
  static final String TITLE = "Tab Translation";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  static final String IDENTIFIER = "116";
  /** The tab level. */
  private static final int LEVEL = 2;

  /**
   * Class constructor.
   *
   * @param parentTab
   *          The parent of the tab.
   */
  public TabTranslationTab(GeneratedTab<?> parentTab) {
    super(TITLE, IDENTIFIER, LEVEL, parentTab);
  }
}
