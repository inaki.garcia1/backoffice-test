/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Lujan <plu@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testsuites.smoke.masterdata;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.common.TimeoutConstants;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.generalsetup.ModuleReferenceData;
import com.openbravo.test.integration.erp.data.generalsetup.enterprise.enterprisemodulemanagement.EnterpriseModuleManagementData;
import com.openbravo.test.integration.erp.gui.LoginPage;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.modules.initialdataload.data.masterdata.setup.entitydefaultvalues.EntitiesData;
import com.openbravo.test.integration.erp.modules.initialdataload.data.masterdata.setup.entitydefaultvalues.FieldsData;
import com.openbravo.test.integration.erp.modules.initialdataload.testscripts.masterdata.setup.EntityDefaultValues;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.modulemanagement.ModuleManagement;
import com.openbravo.test.integration.erp.testscripts.generalsetup.enterprise.enterprisemodulemanagement.EnterpriseModuleManagement;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Execute the Setup Initial Data Load flow of the smoke test suite.
 *
 * @author Pablo Lujan
 */
@RunWith(Parameterized.class)
public class ADMha_SetupInitialDataLoad extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  ModuleReferenceData moduleReferenceIDLData;

  EnterpriseModuleManagementData enterpriseModuleManagementData;

  EntitiesData entitiesData;
  FieldsData[][] fieldsData;

  /**
   * Class constructor.
   */
  public ADMha_SetupInitialDataLoad(ModuleReferenceData moduleReferenceIDLData,
      EnterpriseModuleManagementData enterpriseModuleManagementData, EntitiesData entitiesData,
      FieldsData[][] fieldsData) {
    this.moduleReferenceIDLData = moduleReferenceIDLData;
    this.enterpriseModuleManagementData = enterpriseModuleManagementData;
    this.entitiesData = entitiesData;
    this.fieldsData = fieldsData;

    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   * @throws IOException
   *
   */
  @Parameters
  public static Collection<Object[]> setupProductValues() throws IOException {
    return Arrays.asList(new Object[][] { {

        // Module to install IDL
        new ModuleReferenceData.Builder().name("Initial Data Load")
            .version("3.0.501")
            .language("English (USA)")
            .build(),

        // Apply modules
        new EnterpriseModuleManagementData.Builder().organization("*")
            .addReferenceData(new ModuleReferenceData.Builder().name("Initial Data Load")
                .version("3.0.501")
                .language("English (USA)")
                .build())
            .build(),

        // Assign Entities
        new EntitiesData.Builder().name("Product").build(),
        new FieldsData[][] {
            { new FieldsData.Builder().name("PriceListVersionPurchase").build(),
                new FieldsData.Builder().defaultValue("Purchase").build() },
            { new FieldsData.Builder().name("PriceListVersionSale").build(),
                new FieldsData.Builder().defaultValue("Sales").build() },
            { new FieldsData.Builder().name("PriceListSchema").build(),
                new FieldsData.Builder().defaultValue("Standard").build() },
            { new FieldsData.Builder().name("PriceListPurchase").build(),
                new FieldsData.Builder().defaultValue("Purchase").build() },
            { new FieldsData.Builder().name("PriceListSale").build(),
                new FieldsData.Builder().defaultValue("Sales").build() } } } });
  }

  /**
   * Test the setup client and organization flow.
   *
   * @throws IOException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void initialDataLoadShouldBeSetUp() throws IOException, OpenbravoERPTestException {
    logger.info("** Start of test case [ADMha010] Install Initial Data Load module **");
    boolean isRebuildRequired = false;

    // TODO this module should be installed from obx.
    if (ModuleManagement.AddModules.installModuleFromCentralRepository(mainPage,
        moduleReferenceIDLData)) {
      isRebuildRequired = true;
    }
    LoginPage loginPage = new LoginPage();
    if (isRebuildRequired) {
      ModuleManagement.InstalledModules.rebuildSystemAndRestartServletContainer(mainPage);
      loginPage.waitForFieldsToAppear(TimeoutConstants.REBUILD_TIMEOUT);
    } else {
      mainPage.quit();
    }
    logger.info("** End of test case [ADMha010] Install Initial Data Load module **");

    loginPage = new LoginPage();

    LogInData clientAdministratorLogInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getClientAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getClientAdministratorPassword())
        .build();
    loginPage.logIn(clientAdministratorLogInData);

    mainPage = new MainPage();
    mainPage.assertLogin(clientAdministratorLogInData.getUserName());

    logger.info("** Start of test case [ADMha020] Apply Initial Data Load datasets **");
    EnterpriseModuleManagement.applyModules(mainPage, enterpriseModuleManagementData);
    logger.info("** End of test case [ADMha020] Apply Initial Data Load datasets **");

    logger.info("** Start of test case [ADMha030] Assign Entity Default Values **");
    EntityDefaultValues.Entities.select(mainPage, entitiesData);
    for (FieldsData[] fieldData : fieldsData) {
      // TODO use two separate parameters, one for the original object and one for the edited.
      EntityDefaultValues.Fields.select(mainPage, fieldData[0]);
      EntityDefaultValues.Fields.edit(mainPage, fieldData[1]);
    }
    logger.info("** End of test case [ADMha030] Assign Entity Default Values **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
    OpenbravoERPTest.seleniumStarted = false;
    SeleniumSingleton.INSTANCE.quit();
  }
}
