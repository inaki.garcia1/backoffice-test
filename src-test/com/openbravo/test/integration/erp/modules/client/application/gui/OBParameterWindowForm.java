/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014 Openbravo S.L.U.
 * All Rights Reserved.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.SelectorDataObject;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Canvas;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.DateItem;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.HiddenItem;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.InputField;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.SectionItem;
import com.openbravo.test.integration.selenium.NotImplementedException;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.util.OBDate;
import com.openbravo.test.integration.util.TestUtils;

/**
 * Executes actions on OpenbravoERP parameter window forms built upon SmartClient user interface
 * library.
 *
 */
public class OBParameterWindowForm extends Canvas {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Formatting string used to get the form from the registry. The parameter is the tab identifier.
   */
  private static final String FORMAT_REGISTRY_KEY_FORM = "org.openbravo.client.application.ParameterWindow_Form_%s";

  /**
   * Formatting string used to get a field by its name or index. The first parameter is object
   * string. The second parameter can be the field name or the field index.
   */
  private static final String FORMAT_SMART_CLIENT_GET_FIELD = "%s.getField(%s)";
  /**
   * Formatting string used to get the number of fields in the form. The parameter is the object
   * string.
   */
  private static final String FORMAT_SMART_CLIENT_GET_FIELDS_COUNT = "%s.getFields().size()";
  /**
   * Formatting string used to get the name of a field. The first parameter is object string. The
   * second parameter is the field index.
   */
  private static final String FORMAT_SMART_CLIENT_GET_FIELD_NAME = FORMAT_SMART_CLIENT_GET_FIELD
      + ".name";
  /**
   * Formatting string used to get the section that contains a field. The first parameter is the
   * object string. The second parameter can be the field name or the field index.
   */
  private static final String FORMAT_SMART_CLIENT_GET_SECTION = FORMAT_SMART_CLIENT_GET_FIELD
      + ".section";

  /**
   * Class constructor.
   *
   * @param processId
   *          The process id.
   *
   * @param waitForLoad
   *          Boolean to determine if the system should wait the object be ready to continue.
   */
  public OBParameterWindowForm(String processId, Boolean waitForLoad) {
    super(TestRegistry.getObjectString(String.format(FORMAT_REGISTRY_KEY_FORM, processId)),
        waitForLoad);
  }

  /**
   * Class constructor.
   *
   * @param tabIdentifier
   *          The tab identifier.
   */
  public OBParameterWindowForm(String tabIdentifier) {
    this(tabIdentifier, true);
  }

  /**
   * Get a field object.
   *
   * @param fieldName
   *          The name of the field.
   *
   * @return the field object.
   */
  public InputField getField(String fieldName) {
    return FormInputFieldFactory.getInputFieldObject(String.format(FORMAT_SMART_CLIENT_GET_FIELD,
        objectString, TestUtils.convertFieldSeparator(fieldName)));
  }

  /**
   * Get the number of fields in the form.
   *
   * @return the number of fields in the form.
   */
  private Long getFieldsCount() {
    return (Long) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_FIELDS_COUNT, objectString));
  }

  /**
   * Get the name of a field.
   *
   * @param index
   *          The index of the field.
   * @return the name of the field.
   */
  private String getFieldName(long index) {
    return (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
        String.format(FORMAT_SMART_CLIENT_GET_FIELD_NAME, objectString, index));
  }

  /**
   * Get the class of a field.
   *
   * @param fieldName
   *          The name of the field.
   * @return the class of the field.
   */
  private String getFieldClass(String fieldName) {
    return (String) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_FIELD + ".Class",
            objectString, TestUtils.convertFieldSeparator(fieldName)));
  }

  /**
   * Get the section of a field.
   *
   * @param fieldName
   *          The name of the field.
   * @return The section that contains that field. Or null if it is not contained in a section.
   */
  public SectionItem getFieldSection(String fieldName) {
    String sectionObjectString = String.format(FORMAT_SMART_CLIENT_GET_SECTION, objectString,
        TestUtils.convertFieldSeparator(fieldName));
    if ((Boolean) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(sectionObjectString + " == null")) {
      return null;
    } else {
      return new SectionItem(sectionObjectString);
    }
  }

  /**
   * Get a section object.
   *
   * @param sectionName
   *          The name of the section.
   * @return the section object.
   */
  public SectionItem getSection(String sectionName) {
    // TODO assert that the name corresponds to a section.
    return new SectionItem(String.format(FORMAT_SMART_CLIENT_GET_FIELD, objectString,
        TestUtils.convertFieldSeparator(sectionName)));
  }

  /**
   * Get the data of the selected record in this view.
   *
   * @return the record data.
   */
  public DataObject getData() {
    long fieldsCount = getFieldsCount();
    DataObject data = new DataObject();
    for (long index = 0; index < fieldsCount; index++) {
      String fieldName = getFieldName(index);
      String fieldClass = getFieldClass(fieldName);
      if (fieldClass.equals("OBAuditSectionItem") || fieldClass.equals("OBNoteSectionItem")
          || fieldClass.equals("OBLinkedItemSectionItem")
          || fieldClass.equals("OBAttachmentsSectionItem") || fieldClass.equals("OBImageItem")
          || fieldName.equals("quotation") || fieldName.equals("costcenter")) {
        break;
      }
      if (!fieldClass.equals(ClassNamesConstants.CLASS_SECTION_ITEM)
          && !fieldClass.equals(ClassNamesConstants.CLASS_HIDDEN_ITEM)
          && !fieldClass.equals(ClassNamesConstants.CLASS_SPACER_ITEM)) {
        SectionItem sectionItem = getFieldSection(fieldName);
        if (sectionItem != null) {
          sectionItem.expand();
        }
        InputField inputField;
        try {
          inputField = FormInputFieldFactory
              .getInputFieldObject(String.format(FORMAT_SMART_CLIENT_GET_FIELD, objectString,
                  TestUtils.convertFieldSeparator(fieldName)));
          data.addDataField(fieldName, inputField.getValue());
        } catch (NotImplementedException e) {
          logger.error("Field name is '{}' - Field class is '{}'.", fieldName, fieldClass);
          logger.error(e::getMessage);
        }
      }
    }
    return data;
  }

  /**
   * Get a data field of the selected record in this tab.
   *
   * @param field
   *          The name of the field to get.
   *
   * @return the value of the field.
   */
  public Object getData(String field) {
    SectionItem sectionItem = getFieldSection(field);
    if (sectionItem != null) {
      sectionItem.expand();
    }
    final InputField inputField = FormInputFieldFactory.getInputFieldObject(String.format(
        FORMAT_SMART_CLIENT_GET_FIELD, objectString, TestUtils.convertFieldSeparator(field)));
    return inputField.getValue();
  }

  /**
   * Fill a field.
   *
   * @param fieldName
   *          The name of the field.
   * @param newValue
   *          The new value to enter on the field.
   */
  public void fill(String fieldName, Object newValue) {
    logger.debug("Filling the field '{}' with the value '{}'.", fieldName, newValue);
    SectionItem sectionItem = getFieldSection(fieldName);
    if (sectionItem != null) {
      sectionItem.expand();
      sectionItem.waitUntilExpanded();
    }
    InputField inputField;
    try {
      inputField = getField(fieldName);
    } catch (Exception e) {
      logger.warn(e::getMessage);
      inputField = getField(fieldName);
    }
    inputField.waitUntilEnabled();
    inputField.setValue(newValue);
    inputField.waitUntilEnabled();
  }

  /**
   * Assert a field.
   *
   * @param fieldName
   *          The name of the field.
   * @param expectedValue
   *          The expected value of the field.
   */
  public void assertField(String fieldName, Object expectedValue) {
    SectionItem sectionItem = getFieldSection(fieldName);
    if (sectionItem != null) {
      sectionItem.expand();
    }
    final InputField inputField = getField(fieldName);
    if (inputField instanceof HiddenItem) {
      logger.info("Field '{}' is a hidden item and will not be checked in the form.", fieldName);
    } else {
      Object expectedDisplayValue;
      if (expectedValue instanceof SelectorDataObject) {
        logger.trace("Expected value is instance of SelectorDataObject");
        expectedDisplayValue = ((SelectorDataObject) expectedValue).getDisplayValue();
      } else if (expectedValue instanceof DataObject) {
        expectedDisplayValue = expectedValue.toString();
      } else {
        expectedDisplayValue = expectedValue;
      }
      if (inputField instanceof DateItem) {
        expectedDisplayValue = OBDate.getDateFromTag(expectedDisplayValue);
      }
      assertThat(fieldName, inputField.getValue(), is(equalTo(expectedDisplayValue)));
    }
  }

  /**
   * Assert that a section is expanded.
   *
   * @param name
   *          The name of the section.
   *
   */
  public void assertSectionExpanded(String name) {
    final SectionItem sectionItem = getSection(name);
    assertTrue("Section item is not expanded.", sectionItem.isExpanded());
  }

  /**
   * Assert that a section is collapsed.
   *
   * @param name
   *          The name of the section.
   *
   */
  public void assertSectionCollapsed(String name) {
    final SectionItem sectionItem = getSection(name);
    assertFalse("Section item is not collapsed.", sectionItem.isExpanded());
  }
}
