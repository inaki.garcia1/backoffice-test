/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014 Openbravo S.L.U.
 * All Rights Reserved.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 * This represents the menu item of a parameter window
 *
 */
public abstract class GeneratedParameterWindowMenuItem<T extends DataObject> extends View {

  /** The id of the Defined Process. This should be overridden by the subclass. */
  public static final String PROCESS_ID = "";

  /** The title of the tab. */
  protected final String title;
  /** The identifier of the tab. */
  protected final String identifier;

  /**
   * Top level tab constructor.
   *
   * @param title
   *          The title of the tab.
   * @param identifier
   *          The identifier of the tab.
   * @param menuPath
   *          The menu path where the parameter window is located
   */
  public GeneratedParameterWindowMenuItem(String title, String identifier, String[] menuPath) {
    super(title, menuPath);
    this.title = title;
    this.identifier = identifier;
  }

  /**
   * Get the title of the tab.
   *
   * @return the title of the tab.
   */
  @Override
  public String getTitle() {
    return title;
  }

  /**
   * Get the identifier of the tab.
   *
   * @return the identifier of the tab.
   */
  public String getIdentifier() {
    return identifier;
  }

  public abstract OBParameterWindow<?> loadParameterWindow();
}
