/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *      Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.masterdata.pricing.pricelist.classic;

import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelist.PriceListVersionData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicButtonPopUp;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Price List Version tab.
 *
 * @author elopio
 *
 */
public class PriceListVersionTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Price List Version";
  /** The tab level. */
  private static final int LEVEL = 1;
  /** Identifier of the Price List Version tab. */
  public static final String TAB = "tabname238";

  /** Identifier of the name text field. */
  private static final String TEXT_FIELD_NAME = "Name";
  /** Identifier of the price list schema combo box. */
  private static final String COMBO_BOX_PRICE_LIST_SCHEMA = "reportM_DiscountSchema_ID_S";
  /** Identifier of the base version combo box. */
  private static final String COMBO_BOX_BASE_VERSION = "reportM_Pricelist_Version_Base_ID_S";
  /** Identifier of the valid from date calendar. */
  private static final String CALENDAR_VALID_FROM_DATE = "ValidFrom";
  /** Identifier of the create price list version button. */
  @SuppressWarnings("unused")
  private static final String BUTTON_CREATE_PRICE_LIST_VERSION = "M_Pricelist_Version_Generate_linkBTN";
  /** Identifier of the create price list button. */
  private static final String BUTTON_CREATE_PRICE_LIST = "ProcCreate_linkBTN";

  /**
   * Class constructor
   *
   */
  public PriceListVersionTab() {
    super(TITLE, LEVEL, TAB);
  }

  /**
   * Create a Price List Version.
   *
   * @param data
   *          The data of the price list version.
   */
  public void create(PriceListVersionData data) {
    clickCreateNewRecord();
    if (data.getDataField("name") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_NAME)
          .sendKeys(data.getDataField("name").toString());
    }
    if (data.getDataField("priceListSchema") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_PRICE_LIST_SCHEMA))
          .selectByVisibleText(data.getDataField("priceListSchema").toString());
    }
    if (data.getDataField("basePriceListVersion") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_BASE_VERSION))
          .selectByVisibleText(data.getDataField("basePriceListVersion").toString());
    }
    if (data.getDataField("validFromDate") != null) {
      SeleniumSingleton.INSTANCE.findElementById(CALENDAR_VALID_FROM_DATE)
          .sendKeys(data.getDataField("validFromDate").toString());
    }
    clickSave();
  }

  /**
   * Process the transaction
   */
  @Override
  public void process() throws OpenbravoERPTestException {
    switchToFormView();
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_CREATE_PRICE_LIST).click();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    final OBClassicButtonPopUp postPopUp = new OBClassicButtonPopUp() {
      /**
       * Select the pop up frame.
       */
      @Override
      protected void selectFrame() {
        SeleniumSingleton.INSTANCE.switchTo().frame("BUTTON");
        SeleniumSingleton.INSTANCE.switchTo().frame("mainframe");
      }
    };
    postPopUp.selectPopUpWithOKButton();
    postPopUp.clickOK();
    waitForFrame();
  }
}
