/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 *************************************************************************
 */

package com.openbravo.test.integration.erp.gui.sales.transactions.returnmaterialreceipt;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.sales.transactions.returnmaterialreceipt.ReturnMaterialReceiptHeaderData;
import com.openbravo.test.integration.erp.data.selectors.ReturnMaterialReceiptLineSelectorData;
import com.openbravo.test.integration.erp.gui.popups.RequestProcessPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on OpenbravoERP Return Material Receipt Header tab.
 *
 * @author collazoandy4
 *
 */
public class ReturnMaterialReceiptHeaderTab extends GeneratedTab<ReturnMaterialReceiptHeaderData> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The tab title. */
  static final String TITLE = "Header";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "30576C6ABD12419F9D19D497216FC9B8";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /** Registry key of the complete button. */
  private static final String REGISTRY_KEY_BUTTON_COMPLETE = "org.openbravo.client.application.toolbar.button.documentAction.30576C6ABD12419F9D19D497216FC9B8";
  /** Registry key of the post button. */
  @SuppressWarnings("unused")
  private static final String REGISTRY_KEY_BUTTON_POST = "org.openbravo.client.application.toolbar.button.posted.30576C6ABD12419F9D19D497216FC9B8";
  /** Registry key of the Pick and Edit Lines Button */
  private static final String REGISTRY_KEY_BUTTON_PICKANDEDIT = "org.openbravo.client.application.toolbar.button.receiveMaterials.30576C6ABD12419F9D19D497216FC9B8";
  /** Registry key of the Pick and Edit Lines Pop Up */
  private static final String REGISTRY_KEY_POPUP_PICKANDEDIT = "org.openbravo.client.application.process.pickandexecute.popup";

  /** Identifier of the delete accounting entry checkbox */
  protected static final String IDENTIFIER_CHECKBOX_DELETE = "inpEliminar";

  /* Toolbar buttons. */
  /** The complete button. */
  private static Button buttonComplete;
  private static Button buttonPickAndEdit;

  /**
   * Class constructor.
   *
   */
  public ReturnMaterialReceiptHeaderTab() {
    super(TITLE, IDENTIFIER);
    addChildTab(new LinesTab(this));
    buttonComplete = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_COMPLETE));
    buttonPickAndEdit = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_PICKANDEDIT));
  }

  /**
   * Process the order.
   *
   * @param action
   *          The action to process.
   */
  private void process(String action) {
    logger.debug("Process return material receipt header with action '{}'.", action);
    // XXX workaround issue https://issues.openbravo.com/view.php?id=17409
    buttonComplete.smartClientClick();
    RequestProcessPopUp popUp = new RequestProcessPopUp(
        "org.openbravo.classicpopup./ReturnMaterialReceipt/Header_Edition.html");
    popUp.process(action);
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }

  /**
   * Complete the order.
   *
   */
  public void complete() {
    process("Complete");
    // TODO L1: Check after stabilization if following sleeps is still required
    Sleep.trySleep();
  }

  public PickAndExecuteWindow<ReturnMaterialReceiptLineSelectorData> openPickAndEditLines() {
    buttonPickAndEdit.click();
    PickAndExecuteWindow<ReturnMaterialReceiptLineSelectorData> popup = new PickAndExecuteWindow<ReturnMaterialReceiptLineSelectorData>(
        TestRegistry.getObjectString(REGISTRY_KEY_POPUP_PICKANDEDIT));
    return popup;
  }
}
