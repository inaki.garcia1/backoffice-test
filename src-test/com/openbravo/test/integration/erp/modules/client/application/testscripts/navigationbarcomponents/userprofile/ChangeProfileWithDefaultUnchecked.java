/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Rowinson Gallego <rwn.gallego@gmail.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.navigationbarcomponents.userprofile;

import java.security.InvalidParameterException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.gui.LoginPage;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.UserProfile;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Test the profile information change with the save as default option unchecked from the user
 * profile widget added by Navigation Bar Components module.
 *
 * @author rwngallego
 *
 */
public class ChangeProfileWithDefaultUnchecked extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The new profile. */
  private final ProfileData profile;

  /**
   * Class constructor.
   *
   * @param profile
   *          The new profile.
   */
  public ChangeProfileWithDefaultUnchecked(ProfileData profile) {
    if (profile.isDefault()) {
      throw new InvalidParameterException("Default value of profile data should not be set.");
    }
    this.profile = profile;
  }

  /**
   * [NAV0170] Change profile. Not marked as default In this test case you change the user profile
   * but without marking the default value. Once you change the info you verify that those values
   * persist. Finally you logout and since you didn't mark the changes as default you should see the
   * same values as they were before changed
   */
  @Test
  public void defaultProfileShouldNotBeChanged() {
    UserProfile componentUserProfile = mainPage.getNavigationBar().getComponentUserProfile();
    ProfileData defaultProfileData = componentUserProfile.getProfile();
    logger.info("[NAV0170] Change profile. Not marked as default");
    mainPage.changeProfile(profile);
    // We verify that the changes persist after clicking apply
    mainPage.assertProfile(profile);

    mainPage.quit();

    LoginPage loginPage = new LoginPage();
    loginPage.logIn(logInData);

    mainPage = new MainPage();
    mainPage.assertLogin(logInData.getUserName());

    mainPage.assertProfile(defaultProfileData);
    mainPage.closeProfile();
  }
}
