/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions3;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 29532
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression29532Out extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PurchaseOrderHeaderData purchaseOrderHeaderData;
  PurchaseOrderHeaderData purchaseOrderHeaderVerficationData;
  PurchaseOrderLinesData purchaseOrderLinesData;
  PurchaseOrderLinesData purchaseOrderLinesVerificationData;
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData;
  PaymentOutHeaderData paymentOutHeaderData;
  PaymentOutHeaderData paymentOutHeaderVerificationData;
  AddPaymentPopUpData addPaymentTotalsVerificationData;
  PaymentOutLinesData paymentOutLinesVerificationData;
  PaymentOutHeaderData paymentOutHeaderData2;
  PaymentOutHeaderData paymentOutHeaderVerificationData2;
  AddPaymentPopUpData addPaymentTotalsVerificationData2;
  PaymentOutLinesData paymentOutLinesVerificationData2;

  /**
   * Class constructor.
   *
   */
  public APRRegression29532Out(PurchaseOrderHeaderData purchaseOrderHeaderData,
      PurchaseOrderHeaderData purchaseOrderHeaderVerficationData,
      PurchaseOrderLinesData purchaseOrderLinesData,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData,
      PaymentOutHeaderData paymentOutHeaderData,
      PaymentOutHeaderData paymentOutHeaderVerificationData,
      AddPaymentPopUpData addPaymentTotalsVerificationData,
      PaymentOutLinesData paymentOutLinesVerificationData,
      PaymentOutHeaderData paymentOutHeaderData2,
      PaymentOutHeaderData paymentOutHeaderVerificationData2,
      AddPaymentPopUpData addPaymentTotalsVerificationData2,
      PaymentOutLinesData paymentOutLinesVerificationData2) {
    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    this.purchaseOrderHeaderVerficationData = purchaseOrderHeaderVerficationData;
    this.purchaseOrderLinesData = purchaseOrderLinesData;
    this.purchaseOrderLinesVerificationData = purchaseOrderLinesVerificationData;
    this.bookedPurchaseOrderHeaderVerificationData = bookedPurchaseOrderHeaderVerificationData;
    this.paymentOutHeaderData = paymentOutHeaderData;
    this.paymentOutHeaderVerificationData = paymentOutHeaderVerificationData;
    this.addPaymentTotalsVerificationData = addPaymentTotalsVerificationData;
    this.paymentOutLinesVerificationData = paymentOutLinesVerificationData;
    this.paymentOutHeaderData2 = paymentOutHeaderData2;
    this.paymentOutHeaderVerificationData2 = paymentOutHeaderVerificationData2;
    this.addPaymentTotalsVerificationData2 = addPaymentTotalsVerificationData2;
    this.paymentOutLinesVerificationData2 = paymentOutLinesVerificationData2;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> purchaseOrderValues() {
    Object[][] data = new Object[][] { {
        new PurchaseOrderHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("7.2 (Spain)")
            .build(),
        new PurchaseOrderHeaderData.Builder().organization("Spain")
            .transactionDocument("Purchase Order")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("Spain warehouse")
            .priceList("Purchase")
            .paymentTerms("90 days")
            .build(),
        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMA").priceListVersion("Purchase").build())
            .orderedQuantity("1")
            .build(),
        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("2.00")
            .build(),
        new PurchaseOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("2.00")
            .grandTotalAmount("2.20")
            .currency("EUR")
            .build(),

        new PaymentOutHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("7.2 (Spain)")
            .build(),
        new PaymentOutHeaderData.Builder().organization("Spain")
            .documentType("AP Payment")
            .account("Spain Bank - EUR")
            .currency("EUR")
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Vendor A")
            .fin_paymentmethod_id("7.2 (Spain)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("EUR")
            .actual_payment("1.00")
            .expected_payment("2.20")
            .amount_gl_items("0.00")
            .amount_inv_ords("1.00")
            .total("1.00")
            .difference("0.00")
            .build(),

        new PaymentOutLinesData.Builder().dueDate(OBDate.CURRENT_DATE)
            .invoiceAmount("2.20")
            .expected("2.20")
            .paid("1.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PaymentOutHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("7.2 (Spain)")
            .build(),
        new PaymentOutHeaderData.Builder().organization("Spain")
            .documentType("AP Payment")
            .account("Spain Bank - EUR")
            .currency("EUR")
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Vendor A")
            .fin_paymentmethod_id("7.2 (Spain)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("EUR")
            .actual_payment("1.20")
            .expected_payment("1.20")
            .amount_gl_items("0.00")
            .amount_inv_ords("1.20")
            .total("1.20")
            .difference("0.00")
            .build(),

        new PaymentOutLinesData.Builder().dueDate(OBDate.CURRENT_DATE)
            .invoiceAmount("2.20")
            .expected("2.20")
            .paid("1.20")
            .invoiceno("")
            .glitemname("")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 29532 - Payment Out flow
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression29532OutTest() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression29532Out] Test regression 29532 - Payment Out flow. **");

    PurchaseOrder purchaseOrder = new PurchaseOrder(mainPage).open();
    purchaseOrder.create(purchaseOrderHeaderData);
    purchaseOrder.assertSaved();
    purchaseOrder.assertData(purchaseOrderHeaderVerficationData);
    String orderNo = purchaseOrder.getData("documentNo").toString();
    PurchaseOrder.Lines purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.create(purchaseOrderLinesData);
    purchaseOrderLines.assertSaved();
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData);
    purchaseOrder.book();
    purchaseOrder.assertProcessCompletedSuccessfully2();
    purchaseOrder.assertData(bookedPurchaseOrderHeaderVerificationData);

    PaymentOut paymentOut1 = new PaymentOut(mainPage).open();
    paymentOut1.create(paymentOutHeaderData);
    paymentOut1.assertSaved();
    paymentOut1.assertData(paymentOutHeaderVerificationData);

    AddPaymentProcess addPaymentProcess1 = paymentOut1.addDetailsOpen();
    addPaymentProcess1.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess1.setParameterValue("transaction_type", "Orders");
    addPaymentProcess1.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess1.getOrderInvoiceGrid().unselectAll();
    addPaymentProcess1.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess1.getOrderInvoiceGrid().filter("salesOrderNo", orderNo);
    Sleep.trySleep();
    addPaymentProcess1.editOrderInvoiceRecord(0,
        new SelectedPaymentData.Builder().amount("1.00").build());
    addPaymentProcess1.getField("reference_no").focus();
    addPaymentProcess1.process("Process Made Payment(s) and Withdrawal",
        addPaymentTotalsVerificationData);
    paymentOut1.assertPaymentCreatedSuccessfully();

    PaymentOut.Lines paymentOutLines1 = paymentOut1.new Lines(mainPage);
    paymentOutLines1.assertCount(1);
    paymentOutLines1.select(new PaymentOutLinesData.Builder().orderno(orderNo).build());
    paymentOutLines1.assertData((PaymentOutLinesData) paymentOutLinesVerificationData
        .addDataField("orderPaymentSchedule$order$documentNo", orderNo));

    PaymentOut paymentOut2 = new PaymentOut(mainPage).open();
    paymentOut2.create(paymentOutHeaderData2);
    paymentOut2.assertSaved();
    paymentOut2.assertData(paymentOutHeaderVerificationData2);

    AddPaymentProcess addPaymentProcess2 = paymentOut2.addDetailsOpen();
    addPaymentProcess2.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess2.setParameterValue("transaction_type", "Orders");
    addPaymentProcess2.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess2.getOrderInvoiceGrid().unselectAll();
    addPaymentProcess2.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess2.getOrderInvoiceGrid().filter("salesOrderNo", orderNo);
    Sleep.trySleep();
    addPaymentProcess2.editOrderInvoiceRecord(0,
        new SelectedPaymentData.Builder().amount("1.20").build());
    addPaymentProcess2.getField("reference_no").focus();
    addPaymentProcess2.process("Process Made Payment(s) and Withdrawal",
        addPaymentTotalsVerificationData2);
    paymentOut2.assertPaymentCreatedSuccessfully();

    PaymentOut.Lines paymentOutLines2 = paymentOut2.new Lines(mainPage);
    paymentOutLines2.assertCount(1);
    paymentOutLines2.select(new PaymentOutLinesData.Builder().orderno(orderNo).build());
    paymentOutLines2.assertData((PaymentOutLinesData) paymentOutLinesVerificationData2
        .addDataField("orderPaymentSchedule$order$documentNo", orderNo));

    logger.info(
        "** End of test case [APRRegression29532Out] Test regression 29532 - Payment Out flow. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
