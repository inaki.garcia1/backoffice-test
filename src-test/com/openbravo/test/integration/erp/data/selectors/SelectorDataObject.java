/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.data.selectors;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 * Parent class for a selector data object.
 *
 * @author elopio
 *
 */
public abstract class SelectorDataObject extends DataObject {

  /**
   * Get the search key of the selector data object.
   *
   * @return the search key of the selector data object.
   */
  public abstract String getSearchKey();

  /**
   * Get the display value of the selector data object.
   *
   * @return the value displayed by the selector data object.
   */
  public abstract String getDisplayValue();
}
