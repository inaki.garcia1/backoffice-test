/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Rafael Queralta <rafaelcuba81@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.sales.testsuites.SAL_Regressions;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.generalsetup.enterprise.organization.OrganizationData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.LocationSelectorData;
import com.openbravo.test.integration.erp.data.warehouse.setup.warehouseandstoragebins.StorageBinData;
import com.openbravo.test.integration.erp.data.warehouse.setup.warehouseandstoragebins.WarehouseData;
import com.openbravo.test.integration.erp.gui.warehouse.setup.warehouseandstoragebins.WarehouseAndStorageBinsWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.generalsetup.enterprise.Organization;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.GoodsShipment;
import com.openbravo.test.integration.erp.testscripts.warehouse.setup.warehouse.Warehouse;

/**
 * Test regression 33382
 *
 * @author rqueralta
 */
@RunWith(Parameterized.class)
public class SALRegression33382 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  GoodsShipmentHeaderData goodsReceiptHeaderData;
  GoodsShipmentLinesData goodsReceiptLinesData;
  GoodsShipmentLinesData goodsReceiptLinesVerificationData;

  WarehouseData warehouseData;
  StorageBinData storageBinData;
  GoodsShipmentHeaderData goodsReceiptHeaderData2;
  GoodsShipmentLinesData goodsReceiptLinesData2;
  GoodsShipmentLinesData goodsReceiptLinesVerificationData2;

  /**
   * Class constructor.
   *
   */
  public SALRegression33382(GoodsShipmentHeaderData goodsReceiptHeaderData,
      GoodsShipmentLinesData goodsReceiptLinesData,
      GoodsShipmentLinesData goodsReceiptLinesVerificationData, WarehouseData warehouseData,
      StorageBinData storageBinData, GoodsShipmentHeaderData goodsReceiptHeaderData2,
      GoodsShipmentLinesData goodsReceiptLinesData2,
      GoodsShipmentLinesData goodsReceiptLinesVerificationData2) {
    this.goodsReceiptHeaderData = goodsReceiptHeaderData;
    this.goodsReceiptLinesData = goodsReceiptLinesData;
    this.goodsReceiptLinesVerificationData = goodsReceiptLinesVerificationData;
    this.warehouseData = warehouseData;
    this.storageBinData = storageBinData;
    this.goodsReceiptHeaderData2 = goodsReceiptHeaderData2;
    this.goodsReceiptLinesData2 = goodsReceiptLinesData2;
    this.goodsReceiptLinesVerificationData2 = goodsReceiptLinesVerificationData2;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new GoodsShipmentHeaderData.Builder().organization("Spain")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .documentType("MM Shipment")
            .warehouse("Spain East warehouse")
            .build(),
        new GoodsShipmentLinesData.Builder().build(),
        new GoodsShipmentLinesData.Builder().storageBin("M01").build(),
        // Warehouse and StorageBins
        new WarehouseData.Builder().organization("Spain")
            .searchKey("TestV")
            .name("TestV")
            .locationAddress(new LocationSelectorData.Builder().firstLine("P.I.")
                .secondLine("SLAN")
                .postalCode("31012")
                .city("Pamplona")
                .country("Spain")
                .build())
            .build(),
        new StorageBinData.Builder().searchKey("NewTestBinV")
            .rowX("1")
            .stackY("1")
            .levelZ("1")
            .defaultStorage(true)
            .build(),
        // Goods Receipt
        new GoodsShipmentHeaderData.Builder().organization("Spain")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .documentType("MM Shipment")
            .warehouse("TestV")
            .build(),
        new GoodsShipmentLinesData.Builder().movementQuantity("10").build(),
        new GoodsShipmentLinesData.Builder().storageBin("NewTestBinV").build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 33382
   *
   * @throws ParseException
   */
  @Test
  public void SALRegression33382Test() throws ParseException {
    logger.info("** Start of test case [SALRegression33382] Test regression 33382. **");

    GoodsShipment goodsReceipt = new GoodsShipment(mainPage).open();
    goodsReceipt.create(goodsReceiptHeaderData);
    goodsReceipt.assertSaved();

    GoodsShipment.Lines goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.create(goodsReceiptLinesData);
    goodsReceiptLines.assertData(goodsReceiptLinesVerificationData);

    WarehouseAndStorageBinsWindow warehouseAndStorageBinsWindow = new WarehouseAndStorageBinsWindow();
    mainPage.openView(warehouseAndStorageBinsWindow);
    warehouseAndStorageBinsWindow.selectWarehouseTab()
        .filter(new WarehouseData.Builder().organization("Spain")
            .searchKey("TestV")
            .name("TestV")
            .build());
    if (warehouseAndStorageBinsWindow.selectWarehouseTab().getRecordCount() == 0) {
      mainPage.closeAllViews();
      Warehouse.WarehouseTab.create(mainPage, warehouseData);
      Warehouse.StorageBinTab.create(mainPage, storageBinData);
    }

    Organization organization = new Organization(mainPage).open();
    organization.select(new OrganizationData.Builder().searchKey("Spain").build());
    Organization.Warehouse warehouse = organization.new Warehouse(mainPage);
    warehouse.create(new WarehouseData.Builder().warehouse("TestV").build());

    goodsReceipt = new GoodsShipment(mainPage).open();
    goodsReceipt.create(goodsReceiptHeaderData2);
    goodsReceipt.assertSaved();

    goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.create(goodsReceiptLinesData2);
    goodsReceiptLines.assertData(goodsReceiptLinesVerificationData2);

    organization = new Organization(mainPage).open();
    organization.select(new OrganizationData.Builder().searchKey("Spain").build());
    warehouse = organization.new Warehouse(mainPage);
    warehouse.select(new WarehouseData.Builder().warehouse("TestV").build());
    warehouse.deleteOnGrid();

    logger.info("** End of test case [SALRegression28226] Test regression 33382. **");
  }
}
