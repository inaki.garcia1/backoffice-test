/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions2;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartner.BusinessPartnerData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartner.CustomerData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.masterdata.businesspartner.BusinessPartner;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test regression 30641 - 29942
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression30641In29942In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  BusinessPartnerData businessPartnerData;
  CustomerData customerData;
  AccountData accountHeaderData;
  PaymentMethodData paymentMethodData;
  PaymentMethodData paymentMethodData2;
  TransactionsData transactionLinesData;
  AddPaymentPopUpData addPaymentVerificationData;
  AddPaymentPopUpData addPaymentVerificationData2;
  AddPaymentPopUpData addPaymentVerificationData3;
  PaymentMethodData paymentMethodData3;
  CustomerData customerData2;

  /**
   * Class constructor.
   *
   */
  public APRRegression30641In29942In(BusinessPartnerData businessPartnerData,
      CustomerData customerData, AccountData accountHeaderData, PaymentMethodData paymentMethodData,
      PaymentMethodData paymentMethodData2, TransactionsData transactionLinesData,
      AddPaymentPopUpData addPaymentVerificationData,
      AddPaymentPopUpData addPaymentVerificationData2,
      AddPaymentPopUpData addPaymentVerificationData3, PaymentMethodData paymentMethodData3,
      CustomerData customerData2) {
    this.businessPartnerData = businessPartnerData;
    this.customerData = customerData;
    this.accountHeaderData = accountHeaderData;
    this.paymentMethodData = paymentMethodData;
    this.paymentMethodData2 = paymentMethodData2;
    this.transactionLinesData = transactionLinesData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.addPaymentVerificationData2 = addPaymentVerificationData2;
    this.addPaymentVerificationData3 = addPaymentVerificationData3;
    this.paymentMethodData3 = paymentMethodData3;
    this.customerData2 = customerData2;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] {
        { new BusinessPartnerData.Builder().name("Customer Barcelona").build(),
            new CustomerData.Builder().paymentMethod("").account("").build(),

            new AccountData.Builder().name("Spain Bank").build(),
            new PaymentMethodData.Builder().paymentMethod("1 (Spain)").build(),
            new PaymentMethodData.Builder().pmdefault(true).build(),

            new TransactionsData.Builder().transactionType("BP Deposit").build(),
            new AddPaymentPopUpData.Builder().trxtype("Received In")
                .received_from("")
                .fin_paymentmethod_id("1 (Spain)")
                .fin_financial_account_id("Spain Bank - EUR")
                .c_currency_id("EUR")
                .actual_payment("0.00")
                .expected_payment("0.00")
                .amount_gl_items("0.00")
                .amount_inv_ords("0.00")
                .total("0.00")
                .difference("0.00")
                .build(),
            new AddPaymentPopUpData.Builder().trxtype("Received In")
                .received_from("Customer B")
                .fin_paymentmethod_id("4 (Spain)")
                .fin_financial_account_id("Spain Bank - EUR")
                .c_currency_id("EUR")
                .actual_payment("0.00")
                .expected_payment("0.00")
                .amount_gl_items("0.00")
                .amount_inv_ords("0.00")
                .total("0.00")
                .difference("0.00")
                .build(),
            new AddPaymentPopUpData.Builder().trxtype("Received In")
                .received_from("Customer Barcelona")
                .fin_paymentmethod_id("4 (Spain)")
                .fin_financial_account_id("Spain Bank - EUR")
                .c_currency_id("EUR")
                .actual_payment("0.00")
                .expected_payment("0.00")
                .amount_gl_items("0.00")
                .amount_inv_ords("0.00")
                .total("0.00")
                .difference("0.00")
                .build(),

            new PaymentMethodData.Builder().pmdefault(false).build(),

            new CustomerData.Builder().paymentMethod("6 (Spain)").account("Spain Bank").build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 30641 - 29942 - Payment In flow
   *
   * @throws ParseException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void APRRegression30641In29942InTest() throws ParseException, OpenbravoERPTestException {
    logger.info(
        "** Start of test case [APRRegression30641In29942In] Test regression 30641 - 29942 - Payment In flow. **");

    BusinessPartner.BusinessPartnerTab.select(mainPage, businessPartnerData);
    BusinessPartner.Customer.edit(mainPage, customerData);
    BusinessPartner.BusinessPartnerTab.close(mainPage);

    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.PaymentMethod paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodData2);
    FinancialAccount.Transaction transactions = financialAccount.new Transaction(mainPage);
    transactions.createWithoutSaving(transactionLinesData);

    AddPaymentProcess addPaymentProcess = transactions.openAddPayment();
    addPaymentProcess.assertData(addPaymentVerificationData);
    addPaymentProcess.setParameterValue("received_from",
        new BusinessPartnerSelectorData.Builder().name("Customer B").build());
    addPaymentProcess.assertData(addPaymentVerificationData2);
    addPaymentProcess.setParameterValue("received_from",
        new BusinessPartnerSelectorData.Builder().name("Customer Barcelona").build());
    addPaymentProcess.assertData(addPaymentVerificationData3);
    addPaymentProcess.processWithError("Process Received Payment(s)",
        AddPaymentProcess.REGEXP_ERROR_MESSAGE_PAYMENT_AMOUNT_IS_ZERO);
    addPaymentProcess.close();
    transactions.cancelOnGrid();
    transactions.closeForm();

    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodData3);

    BusinessPartner.BusinessPartnerTab.open(mainPage);
    Sleep.trySleep(15000);
    BusinessPartner.BusinessPartnerTab.select(mainPage, businessPartnerData);
    BusinessPartner.Customer.edit(mainPage, customerData2);
    BusinessPartner.BusinessPartnerTab.close(mainPage);

    logger.info(
        "** End of test case [APRRegression30641In29942In] Test regression 30641 - 29942 - Payment In flow. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
