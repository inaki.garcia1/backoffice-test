/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.sales.testsuites.SAL_Regressions;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelist.PriceListData;
import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelist.PriceListVersionData;
import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelist.ProductPriceData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.TaxData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.masterdata.pricing.pricelist.PriceListTab;
import com.openbravo.test.integration.erp.gui.masterdata.pricing.pricelist.PriceListVersionTab;
import com.openbravo.test.integration.erp.gui.masterdata.pricing.pricelist.PriceListWindow;
import com.openbravo.test.integration.erp.gui.masterdata.pricing.pricelist.ProductPriceTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder.Lines;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder.Tax;

/**
 * Test Regression 32428
 *
 * @author nonofce
 *
 */

@RunWith(Parameterized.class)
public class SALRegression32428 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  PriceListData priceList;
  PriceListVersionData priceListVersion;
  ProductPriceData productPrice;
  SalesOrderHeaderData salesOrder;
  SalesOrderLinesData orderLines;

  public SALRegression32428(PriceListData priceList, PriceListVersionData priceListVersion,
      ProductPriceData productPrice, SalesOrderHeaderData salesOrder,
      SalesOrderLinesData orderLines) {
    super();
    this.priceList = priceList;
    this.priceListVersion = priceListVersion;
    this.productPrice = productPrice;
    this.salesOrder = salesOrder;
    this.orderLines = orderLines;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> values() {
    Object[][] data = new Object[][] { {

        new PriceListData.Builder().name("Test Price List Sales")
            .salesPriceList(true)
            .priceIncludesTax(true)
            .build(),

        new PriceListVersionData.Builder().name("Test Price List Sales Version").build(),

        new ProductPriceData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("costingProduct1").build())
            .listPrice("10")
            .standardPrice("10")
            .build(),

        new SalesOrderHeaderData.Builder().transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .priceList("Test Price List Sales")
            .build(),

        new SalesOrderLinesData.Builder()
            .product(new ProductSelectorData.Builder().searchKey("costingProduct1")
                .priceListVersion("Test Price List Sales Version")
                .build())
            .orderedQuantity("10")
            .tax("VAT 10%")
            .build()

        } };

    return Arrays.asList(data);
  }

  /**
   * Test regression 32428
   *
   * @throws ParseException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void SALRegression32428Test() throws ParseException, OpenbravoERPTestException {
    logger.info(
        "** Start of test case [SAL32428] Wrong Tax Amount and Total Gross Amount in orders created by the Copy Record. **");

    PriceListWindow priceListWindow = new PriceListWindow();
    mainPage.openView(priceListWindow);
    PriceListTab priceListTab = priceListWindow.selectPriceListTab();
    priceListTab.filter(new PriceListData.Builder().name("Test Price List Sales").build());
    if (priceListTab.getRecordCount() == 0) {
      priceListTab.clearFilters();
      priceListTab.createRecord(priceList);
      PriceListVersionTab priceListVersionTab = priceListWindow.selectPriceListVersionTab();
      priceListVersionTab.createRecord(priceListVersion);
      priceListVersionTab.assertSaved();
      priceListVersionTab.process();
      ProductPriceTab productPriceTab = priceListWindow.selectProductPriceTab();
      productPriceTab.createRecord(productPrice);
    }

    SalesOrder sales = new SalesOrder(mainPage).open();
    sales.create(salesOrder);
    sales.assertSaved();

    Lines lines = sales.new Lines(mainPage);
    lines.create(orderLines);
    lines.assertSaved();

    sales.assertData(new SalesOrderHeaderData.Builder().grandTotalAmount("100.00")
        .summedLineAmount("90.91")
        .build());

    Tax tax = sales.new Tax(mainPage);
    tax.selectWithoutFiltering(0);
    tax.assertData(new TaxData.Builder().taxAmount("9.09").taxableAmount("90.91").build());

    sales.book();

    sales.copyRecord();

    sales.assertData(new SalesOrderHeaderData.Builder().grandTotalAmount("100.00")
        .summedLineAmount("90.91")
        .build());
    tax.selectWithoutFiltering(0);
    tax.assertData(new TaxData.Builder().taxAmount("9.09").taxableAmount("90.91").build());

    sales.book();

    sales.assertData(new SalesOrderHeaderData.Builder().grandTotalAmount("100.00")
        .summedLineAmount("90.91")
        .build());
    tax.selectWithoutFiltering(0);
    tax.assertData(new TaxData.Builder().taxAmount("9.09").taxableAmount("90.91").build());

    logger.info(
        "** End of test case [SAL32428] Wrong Tax Amount and Total Gross Amount in orders created by the Copy Record. **");
  }
}
