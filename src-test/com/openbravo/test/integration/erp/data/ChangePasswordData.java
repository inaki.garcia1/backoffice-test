/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.data;

/**
 * Class for password data.
 *
 * @author elopio
 *
 */
public class ChangePasswordData {

  /** Formatting string used to get a string representation of this object. */
  private static final String FORMAT_STRING_REPRESENTATION = "[Password data: current password=%s, new password=%s, new password confirmation=%s]";

  /* Data fields. */
  /** The current password. */
  private final String currentPassword;
  /** The new password. */
  private final String newPassword;
  /** The new password confirmation. */
  private final String newPasswordConfirmation;

  /**
   * Class builder.
   *
   * @author elopio
   *
   */
  public static class Builder {
    /** The current password. */
    private String currentPassword = null;
    /** The new password. */
    private String newPassword = null;
    /** The new password confirmation. */
    private String newPasswordConfirmation = null;

    /**
     * Set the current password.
     *
     * @param value
     *          The current password.
     * @return the password builder.
     */
    public Builder currentPassword(String value) {
      this.currentPassword = value;
      return this;
    }

    /**
     * Set the new password.
     *
     * @param value
     *          The new password.
     * @return the password builder.
     */
    public Builder newPassword(String value) {
      this.newPassword = value;
      return this;
    }

    /**
     * Set the new password confirmation.
     *
     * @param value
     *          The new password confirmation.
     * @return the password builder.
     */
    public Builder newPasswordConfirmation(String value) {
      this.newPasswordConfirmation = value;
      return this;
    }

    /**
     * Build the profile object.
     *
     * @return the password object.
     */
    public ChangePasswordData build() {
      return new ChangePasswordData(this);
    }

  }

  /**
   * Class constructor.
   *
   * @param builder
   *          The object builder.
   */
  private ChangePasswordData(Builder builder) {
    currentPassword = builder.currentPassword;
    newPassword = builder.newPassword;
    newPasswordConfirmation = builder.newPasswordConfirmation;
  }

  /**
   * Get the current password.
   *
   * @return the current password.
   */
  public String getCurrentPassword() {
    return currentPassword;
  }

  /**
   * Get the new password.
   *
   * @return the new password.
   */
  public String getNewPassword() {
    return newPassword;
  }

  /**
   * Get the new password confirmation.
   *
   * @return the new password confirmation.
   */
  public String getNewPasswordConfirmation() {
    return newPasswordConfirmation;
  }

  /**
   * Get the string representation of this password data object.
   *
   * @return the string representation.
   */
  @Override
  public String toString() {
    return String.format(FORMAT_STRING_REPRESENTATION, currentPassword, newPassword,
        newPasswordConfirmation);
  }

}
