/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 * 	Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.selenium;

/**
 * Class used to access Radio buttons and check boxes with Selenium
 *
 * @author elopio
 *
 */
public class Checkables {

  /** Format string used to locate a radio button with xpath */
  private static final String FORMAT_LOCATE_CHECKABLE = "//input[@name='%s' and @value='%s']";

  /**
   * Check a radio button or check box
   *
   * @param radioButtonName
   *          The name of the radio button
   * @param value
   *          The value to check
   */
  public static void check(String radioButtonName, String value) {
    if (!SeleniumSingleton.INSTANCE
        .findElementByXPath(String.format(FORMAT_LOCATE_CHECKABLE, radioButtonName, value))
        .isSelected()) {
      SeleniumSingleton.INSTANCE
          .findElementByXPath(String.format(FORMAT_LOCATE_CHECKABLE, radioButtonName, value))
          .click();
    }
  }
}
