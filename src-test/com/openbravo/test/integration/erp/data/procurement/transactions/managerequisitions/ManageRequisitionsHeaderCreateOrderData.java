/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2015 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2010-12-28 18:04:24
 * Contributor(s):
 *  Pablo Lujan <pablo.lujan@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.procurement.transactions.managerequisitions;

/**
 *
 * @author plujan
 *
 */
public class ManageRequisitionsHeaderCreateOrderData {

  /* Data fields */
  private String requisitionNumber;
  private String orderDate;
  private String organization;
  private String warehouse;
  private String documentStatus;
  private String quantity;

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /* Builder fields */
    private String requisitionNumber = null;
    private String orderDate = null;
    private String organization = null;
    private String warehouse = null;
    private String documentStatus = null;
    private String quantity = null;

    /**
     * @param value
     *          The requisitionNumber value
     * @return The builder for this class.
     */
    public Builder requisitionNumber(String value) {
      this.requisitionNumber = value;
      return this;
    }

    /**
     * @param value
     *          The orderDate value
     * @return The builder for this class.
     */
    public Builder orderDate(String value) {
      this.orderDate = value;
      return this;
    }

    /**
     * @param value
     *          The organization value
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.organization = value;
      return this;
    }

    /**
     * @param value
     *          The warehouse value
     * @return The builder for this class.
     */
    public Builder warehouse(String value) {
      this.warehouse = value;
      return this;
    }

    /**
     * Set the Document Status value.
     *
     * @param value
     *          The Document Status value.
     * @return The builder for this class.
     */
    public Builder documentStatus(String value) {
      this.documentStatus = value;
      return this;
    }

    /**
     * @param value
     *          The quantity value
     * @return The builder for this class
     */
    public Builder quantity(String value) {
      this.quantity = value;
      return this;
    }

    /**
     * Set the fields for this object that are required on the ERP.
     *
     * @param requisitionNumberField
     *          The requisitionNumber value
     * @param orderDateField
     *          The orderDate value
     * @param organizationField
     *          The organization value
     * @param warehouseField
     *          The warehouse value
     * @param documentStatusField
     *          The documentStatus value
     * @param quantityField
     *          The quantity value
     *
     * @return The builder for this class
     */
    public Builder requiredFields(String requisitionNumberField, String orderDateField,
        String organizationField, String warehouseField, String documentStatusField,
        String quantityField) {
      this.requisitionNumber = requisitionNumberField;
      this.orderDate = orderDateField;
      this.organization = organizationField;
      this.warehouse = warehouseField;
      this.documentStatus = documentStatusField;
      this.quantity = quantityField;
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ManageRequisitionsHeaderCreateOrderData build() {
      return new ManageRequisitionsHeaderCreateOrderData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ManageRequisitionsHeaderCreateOrderData(Builder builder) {
    requisitionNumber = builder.requisitionNumber;
    orderDate = builder.orderDate;
    organization = builder.organization;
    warehouse = builder.warehouse;
    documentStatus = builder.documentStatus;
    quantity = builder.quantity;

  }

  /**
   * @return The requisitionNumber value
   */
  public String getRequisitionNumber() {
    return requisitionNumber;
  }

  /**
   * @return The orderDate value
   */
  public String getOrderDate() {
    return orderDate;
  }

  /**
   * @return The organization value
   */
  public String getOrganization() {
    return organization;
  }

  /**
   * @return The warehouse value
   */
  public String getWarehouse() {
    return warehouse;
  }

  /**
   * @return The documentStatus value
   */
  public String getDocumentStatus() {
    return documentStatus;
  }

  /**
   * @return The quantity value
   */
  public String getQuantity() {
    return quantity;
  }

  @Override
  public String toString() {
    return "ManageRequisitionsHeaderCreateSalesOrderData [requisitionNumber=" + requisitionNumber
        + ", orderDate=" + orderDate + ", organization=" + organization + ", warehouse=" + warehouse
        + ", documentStatus=" + documentStatus + ", quantity=" + quantity + "]";
  }

}
