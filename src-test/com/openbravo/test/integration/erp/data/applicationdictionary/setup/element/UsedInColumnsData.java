/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.applicationdictionary.setup.element;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for UsedInColumnsData
 *
 * @author plujan
 *
 */
public class UsedInColumnsData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the dBColumnName value
     *
     * Description: The name of a column within the database.
     *
     * @param value
     *          The dBColumnName value.
     * @return The builder for this class.
     */
    public Builder dBColumnName(String value) {
      this.dataFields.put("dBColumnName", value);
      return this;
    }

    /**
     * Set the table value
     *
     * Description: A dictionary table used for this tab that points to the database table.
     *
     * @param value
     *          The table value.
     * @return The builder for this class.
     */
    public Builder table(String value) {
      this.dataFields.put("table", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public UsedInColumnsData build() {
      return new UsedInColumnsData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private UsedInColumnsData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
