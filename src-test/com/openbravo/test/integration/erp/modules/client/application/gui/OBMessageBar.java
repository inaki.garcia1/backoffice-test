/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.TimeoutException;

import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Canvas;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes actions on OpenbravoERP message bar built upon SmartClient user interface library.
 *
 * @author elopio
 *
 */
public class OBMessageBar extends Canvas {

  /**
   * Formatting string used to verify the displayed message. The first parameter is the message
   * title. The second parameter is the message body.
   */
  private static final String FORMAT_MESSAGE = "<b>%s</b><br/>%s";
  /** Title of the error messages. */
  private static final String MESSAGE_TITLE_ERROR = "Error";
  /** Title of the error messages. */
  private static final String MESSAGE_TITLE_WARNING = "Warning";
  /** Title of the info messages. */
  private static final String MESSAGE_TITLE_INFO = "Info";
  /** Title of the process completed successfully message. */
  private static final String MESSAGE_TITLE_SUCCESS = "Success";
  /** Title of the process completed successfully message. */
  private static final String MESSAGE_BODY_PROCESS_COMPLETED_SUCCESSFULLY = "Process completed successfully";
  /** Title of the success message. */
  private static final String MESSAGE_TITLE_PROCESS_EXECUTION_SUCCEEDED = "Success";

  /**
   * Formatting string used to get the contents of the message bar. The parameter is the object
   * string.
   */
  // TODO can we use the SeleniumSingleton.INSTANCE.getText to verify this?
  private static final String FORMAT_SMART_CLIENT_GET_CONTENTS = "%s.text.contents";
  /**
   * Formatting string used to get the close icon object. The parameter is the object string.
   */
  private static final String FORMAT_SMART_CLIENT_GET_CLOSE_ICON = "%s.closeIcon";

  /* Components */
  /** The close button. */
  private final Button buttonClose;

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   * @param waitForLoad
   *          Boolean to determine if the system should wait the object be ready to continue.
   */
  public OBMessageBar(String objectString, Boolean waitForLoad) {
    super(objectString, waitForLoad);
    buttonClose = new Button(String.format(FORMAT_SMART_CLIENT_GET_CLOSE_ICON, objectString),
        waitForLoad);
  }

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   */
  public OBMessageBar(String objectString) {
    this(objectString, true);
  }

  /**
   * Assert that the message bar is visible.
   */
  public void assertVisible() {
    assertTrue("Message Bar is not visible.", isVisible());
  }

  /**
   * Assert that the message bar is not visible.
   */
  public void assertNotVisible() {
    assertFalse("Message Bar is visible.", isVisible());
  }

  /**
   * Get the contents of the message bar.
   *
   * @return the contents of the message bar.
   */
  public String getContents() {
    return (String) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_CONTENTS, objectString));
  }

  /**
   * Assert the contents of the message bar.
   *
   * @param title
   *          The expected title of the message bar.
   * @param body
   *          The expected body of the message bar.
   */
  private void assertContents(String title, String body) {
    assertThat(getContents(), is(equalTo(String.format(FORMAT_MESSAGE, title, body))));
  }

  /**
   * Assert that the title and body of the message bar matches the regular expressions.
   *
   * @param title
   *          A regular expression for the expected title of the message bar.
   * @param body
   *          A regular expression for the expected body of the message bar.
   *
   * @return a list with all the capturing groups of the regular expression.
   */
  private List<String> assertContentsMatch(String title, String body) {
    String messageContents = getContents();
    String expectedMatch = String.format(FORMAT_MESSAGE, title, body);
    Pattern pattern = Pattern.compile(expectedMatch);
    Matcher matcher = pattern.matcher(messageContents);
    assertTrue(String.format("'%s' should match with '%s'", messageContents, expectedMatch),
        matcher.matches());
    ArrayList<String> capturingGroups = new ArrayList<String>();
    for (int index = 0; index <= matcher.groupCount(); index++) {
      capturingGroups.add(matcher.group(index));
    }
    return capturingGroups;
  }

  /**
   * Check if the error message is visible.
   *
   * @return true if the error message is visible. Otherwise, false.
   */
  public boolean isErrorMessageVisible() {
    return isVisible()
        && getContents().matches(String.format(FORMAT_MESSAGE, MESSAGE_TITLE_ERROR, ".*"));
  }

  /**
   * Check if the warning message is visible.
   *
   * @return true if the warning message is visible. Otherwise, false.
   */
  public boolean isWarningMessageVisible() {
    return isVisible()
        && getContents().matches(String.format(FORMAT_MESSAGE, MESSAGE_TITLE_WARNING, ".*"));
  }

  /**
   * Check if the info message is visible.
   *
   * @return true if the info message is visible. Otherwise, false.
   */
  public boolean isInfoMessageVisible() {
    return isVisible()
        && getContents().matches(String.format(FORMAT_MESSAGE, MESSAGE_TITLE_INFO, ".*"));
  }

  /**
   * Check if the info message is visible, and, if it is visible, it Sleeps milliseconds until it is
   * not visible.
   *
   * @param milliseconds
   *          The number of milliseconds it will sleep each iteration if the info message is
   *          visible.
   * @param maxIterations
   *          The number of maximum allowed iterations.
   */
  public void waitIfInfoMessageVisible(int milliseconds, int maxIterations) {
    for (int i = 0; i < (maxIterations - 1) && isInfoMessageVisible(); i++) {
      Sleep.trySleepWithMessage(milliseconds,
          "Info message has been found. Sleeping " + milliseconds + " milliseconds.");
    }
  }

  /**
   * Assert an error message.
   *
   * @param message
   *          The expected body of the message displayed.
   */
  public void assertErrorMessage(String message) {
    assertVisible();
    assertContents(MESSAGE_TITLE_ERROR, message);
    // TODO verify the icon.
  }

  /**
   * Assert an info message.
   *
   * @param message
   *          The expected body of the message displayed.
   */
  public void assertInfoMessage(String message) {
    assertVisible();
    assertContents(MESSAGE_TITLE_INFO, message);
    // TODO verify the icon.
  }

  /**
   * Assert a warning message.
   *
   * @param message
   *          The expected body of the message displayed.
   */
  public void assertWarningMessage(String message) {
    assertVisible();
    assertContents(MESSAGE_TITLE_WARNING, message);
    // TODO verify the icon.
  }

  /**
   * Assert the process completed successfully message.
   */
  public void assertProcessCompletedSuccessfully() {
    assertProcessCompletedSuccessfully(MESSAGE_BODY_PROCESS_COMPLETED_SUCCESSFULLY);
  }

  /**
   * Assert the process completed successfully message.
   *
   * TODO see issue 17291 - https://issues.openbravo.com/view.php?id=17291.
   */
  public void assertProcessCompletedSuccessfully2() {
    // TODO I0: Adding explicit wait period to ensure that the field to be asserted is
    // Added due to Gecko insertion
    try {
      Sleep.setExplicitWait(10000)
          .until(wd -> getContents().equals("<b>Process completed successfully</b>"));
    } catch (TimeoutException te) {
      // Continue when condition isn't met, too
    }
    assertThat(getContents(), is(equalTo("<b>Process completed successfully</b>")));
  }

  /**
   * Assert the process completed successfully message.
   *
   * TODO see issue 17291 - https://issues.openbravo.com/view.php?id=17291.
   *
   * @param message
   *          The expected message body.
   */
  public void assertProcessCompletedSuccessfully2(String message) {
    assertVisible();
    assertContents("Process completed successfully", message);
  }

  /**
   * Assert the process completed successfully message.
   *
   * @param message
   *          The expected message body.
   */
  public void assertProcessCompletedSuccessfully(String message) {
    assertVisible();
    assertContents(MESSAGE_TITLE_SUCCESS, message);
    // TODO verify the icon.
  }

  /**
   * Assert the process completed successfully message, using regular expression to match the body.
   *
   * @param message
   *          A regular expression for the expected message body.
   * @return A list with all the capturing groups of the regular expression.
   */
  public List<String> assertProcessCompletedSuccessfullyContentMatch(String message) {
    assertVisible();
    // TODO see issue 17291 - https://issues.openbravo.com/view.php?id=17291
    return assertContentsMatch(MESSAGE_BODY_PROCESS_COMPLETED_SUCCESSFULLY, message);
    // TODO verify the icon.
  }

  /**
   * Assert the process execution succeded message, using regular expression to match the body.
   *
   * @param message
   *          A regular expression for the expected message body.
   * @return A list with all the capturing groups of the regular expression.
   */
  public List<String> assertProcessExecutionSuceededContentMatch(String message) {
    assertVisible();
    return assertContentsMatch(MESSAGE_TITLE_PROCESS_EXECUTION_SUCCEEDED, message);
    // TODO verify the icon.
  }

  /**
   * Close the message bar.
   */
  public void close() {
    // XXX we had to use the SmartClient click directly because with the selenium click, the buttons
    // bar disappears and the message bar doesn't.
    buttonClose.smartClientClick();
    waitUntilNotVisible();
  }
}
