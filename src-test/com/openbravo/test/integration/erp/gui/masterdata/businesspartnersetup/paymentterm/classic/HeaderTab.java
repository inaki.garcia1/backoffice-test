/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.masterdata.businesspartnersetup.paymentterm.classic;

import org.openqa.selenium.WebElement;

import com.openbravo.test.integration.erp.data.masterdata.businesspartnersetup.paymentterm.PaymentTermHeaderData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Payment Term Header tab.
 *
 * @author elopio
 *
 */
public class HeaderTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Header";
  /** The tab level. */
  private static final int LEVEL = 0;

  /* GUI components. */
  /** Identifier of the Header tab. */
  public static final String TAB = "tabname184";

  /** Identifier of the search key text field */
  private static final String TEXT_FIELD_SEARCH_KEY = "Value";
  /** Identifier of the name text field */
  private static final String TEXT_FIELD_NAME = "Name";
  /** Identifier of the is due fixed check box */
  private static final String CHECK_BOX_IS_DUE_FIXED = "IsDueFixed";
  /** Identifier of the fix month day text field */
  private static final String TEXT_FIELD_FIX_MONTH_DAY = "FixMonthDay";
  /** Identifier of the fix month offset text field */
  private static final String TEXT_FIELD_FIX_MONTH_OFFSET = "FixMonthOffset";

  /**
   * Class constructor
   *
   */
  public HeaderTab() {
    super(TITLE, LEVEL, TAB);
  }

  /**
   * Create a new Payment Term header.
   *
   * @param data
   *          The data of the Payment Term header that will be created.
   */
  public void create(PaymentTermHeaderData data) {
    clickCreateNewRecord();
    if (data.getDataField("searchKey") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_SEARCH_KEY)
          .sendKeys(data.getDataField("searchKey").toString());
    }
    if (data.getDataField("name") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_NAME)
          .sendKeys(data.getDataField("name").toString());
    }
    if (data.getDataField("fixedDueDate") != null) {
      WebElement checkBoxFixedDueDate = SeleniumSingleton.INSTANCE
          .findElementById(CHECK_BOX_IS_DUE_FIXED);
      if (!data.getDataField("fixedDueDate").equals(checkBoxFixedDueDate.isSelected())) {
        checkBoxFixedDueDate.click();
      }
    }
    if (data.getDataField("maturityDate1") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_FIX_MONTH_DAY)
          .sendKeys(data.getDataField("maturityDate1").toString());
    }
    if (data.getDataField("offsetMonthDue") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_FIX_MONTH_OFFSET)
          .sendKeys(data.getDataField("offsetMonthDue").toString());
    }
    clickSave();
  }
}
