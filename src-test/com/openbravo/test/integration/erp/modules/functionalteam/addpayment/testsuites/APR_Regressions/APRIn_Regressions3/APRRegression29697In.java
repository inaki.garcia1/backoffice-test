/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions3;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddTransactionData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.MatchStatementData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.bankstatement.BankStatementHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.BankStatementLinesData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddTransactionProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementGrid;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 29697
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression29697In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  AccountData accountHeaderData;
  BankStatementHeaderData bankStatementHeaderData;
  BankStatementLinesData bankStatementLinesData;
  MatchStatementData matchStatementData;
  AddTransactionData addTransactionVerificationData;
  MatchStatementData matchStatementData2;
  TransactionsData transactionData;
  TransactionsData transactionVerificationData;

  /**
   * Class constructor.
   *
   */
  public APRRegression29697In(AccountData accountHeaderData,
      BankStatementHeaderData bankStatementHeaderData,
      BankStatementLinesData bankStatementLinesData, MatchStatementData matchStatementData,
      AddTransactionData addTransactionVerificationData, MatchStatementData matchStatementData2,
      TransactionsData transactionData, TransactionsData transactionVerificationData) {
    this.accountHeaderData = accountHeaderData;
    this.bankStatementHeaderData = bankStatementHeaderData;
    this.bankStatementLinesData = bankStatementLinesData;
    this.matchStatementData = matchStatementData;
    this.addTransactionVerificationData = addTransactionVerificationData;
    this.matchStatementData2 = matchStatementData2;
    this.transactionData = transactionData;
    this.transactionVerificationData = transactionVerificationData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { { new AccountData.Builder().name("Spain Bank").build(),
        new BankStatementHeaderData.Builder().name("APRRegression29697In").build(),
        new BankStatementLinesData.Builder().referenceNo("29697In")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .gLItem("GL Item 1")
            .cramount("200.00")
            .build(),

        new MatchStatementData.Builder().bankStatementType("D")
            .sender("Customer A")
            .referenceNo("29697In")
            .glitem("GL Item 1")
            .amount("200")
            .affinity("")
            .matchedDocument("")
            .matchingType("")
            .transactionDate("")
            .businessPartner("")
            .transactionAmount("")
            .transactionGLItem("")
            .build(),
        new AddTransactionData.Builder().trxtype("BP Deposit")
            .gLItem("GL Item 1")
            .description("")
            .currency("EUR")
            .depositamt("200.00")
            .withdrawalamt("0.00")
            .organization("Spain")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new MatchStatementData.Builder().bankStatementType("D")
            .sender("Customer A")
            .referenceNo("29697In")
            .glitem("GL Item 1")
            .amount("200")
            .affinity("AD")
            .matchedDocument("T")
            .matchingType("AD")
            .businessPartner("Customer A")
            .transactionAmount("200")
            .transactionGLItem("GL Item 1")
            .build(),

        new TransactionsData.Builder().transactionType("BP Deposit")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .gLItem("GL Item 1")
            .description("GL Item: GL Item 1")
            .depositAmount("200.00")
            .paymentAmount("0.00")
            .currency("EUR")
            .status("Payment Cleared")
            .foreignAmount("")
            .foreignCurrency("")
            .build(),
        new TransactionsData.Builder().transactionType("BP Deposit")
            .gLItem("GL Item 1")
            .description("GL Item: GL Item 1")
            .currency("EUR")
            .depositAmount("200.00")
            .paymentAmount("0.00")
            .organization("Spain")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 29697 - Payment In flow
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression29697InTest() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression29697In] Test regression 29697 - Payment In flow. **");

    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);

    FinancialAccount.BankStatements bankStatement = financialAccount.new BankStatements(mainPage);
    String date;
    if (bankStatement.getRecordCount() == 0) {
      date = OBDate.CURRENT_DATE;
    } else {
      bankStatement.selectWithoutFiltering(0);
      date = OBDate.addDaysToDate((String) bankStatement.getData("transactionDate"), 1);
    }
    bankStatementHeaderData.addDataField("transactionDate", date);
    bankStatementHeaderData.addDataField("importdate", date);
    bankStatement.create(bankStatementHeaderData);
    bankStatement.assertSaved();

    FinancialAccount.BankStatements.BankStatementLines bankStatementLines = bankStatement.new BankStatementLines(
        mainPage);
    bankStatementLinesData.addDataField("transactionDate", date);
    bankStatementLines.create(bankStatementLinesData);
    bankStatementLines.assertSaved();
    bankStatement.process();
    bankStatement.assertProcessCompletedSuccessfully2();

    @SuppressWarnings("rawtypes")
    MatchStatementProcess matchStatementProcess = financialAccount.openMatchStatement(false);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementData.addDataField("banklineDate", date);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData);

    AddTransactionProcess addTransactionProcess = ((MatchStatementGrid) matchStatementProcess
        .getMatchStatementGrid()).clickAdd(0);
    addTransactionVerificationData.addDataField("trxdate", date);
    addTransactionVerificationData.addDataField("dateacct", date);
    addTransactionProcess.assertData(addTransactionVerificationData);
    addTransactionProcess.process();

    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementData2.addDataField("banklineDate", date);
    matchStatementData2.addDataField("trxDate", date);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData2);
    matchStatementProcess.process();
    financialAccount.assertProcessCompletedSuccessfully2();

    FinancialAccount.Transaction transaction = financialAccount.new Transaction(mainPage);
    transactionData.addDataField("transactionDate", date);
    transaction.select(transactionData);
    transactionVerificationData.addDataField("transactionDate", date);
    transactionVerificationData.addDataField("dateAcct", date);
    transaction.assertData(transactionVerificationData);

    logger.info(
        "** End of test case [APRRegression29697In] Test regression 29697 - Payment In flow. **");
  }

  @AfterClass
  public static void tearDown() {
    SeleniumSingleton.INSTANCE.quit();
    OpenbravoERPTest.seleniumStarted = false;
    OpenbravoERPTest.forceLoginRequired();
  }
}
