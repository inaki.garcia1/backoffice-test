/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017-2020 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.sales.testsuites.SAL_Regressions;

import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test regression 29963
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class SALRegression29963 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineData;
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  AddPaymentPopUpData addPaymentVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData2;

  /**
   * Class constructor.
   *
   */
  public SALRegression29963(SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      AddPaymentPopUpData addPaymentVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData2) {
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData = salesInvoiceLineData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.completedSalesInvoiceHeaderVerificationData2 = completedSalesInvoiceHeaderVerificationData2;
    logInData = new LogInData.Builder().userName("Openbravo").password("openbravo").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new SalesInvoiceHeaderData.Builder().businessPartner(
            new BusinessPartnerSelectorData.Builder().name("Moon Light Restaurants, Co.").build())
            .build(),
        new SalesInvoiceHeaderData.Builder().organization("F&B US East Coast")
            .transactionDocument("AR Invoice")
            .partnerAddress("1135 Maple Ave, Los Angeles, CA 90015")
            .paymentTerms("30 days")
            .paymentMethod("Wire Transfer")
            .priceList("General Sales")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Plain Water")
                .priceListVersionName("General Sales")
                .build())
            .invoicedQuantity("18")
            .build(),
        new SalesInvoiceLinesData.Builder().uOM("Liter")
            .unitPrice("1.53")
            .listPrice("1.53")
            .tax("NY Sales Tax Exempt")
            .lineNetAmount("27.54")
            .build(),
        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("27.54")
            .summedLineAmount("27.54")
            .currency("USD")
            .paymentComplete(false)
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Moon Light Restaurants, Co.")
            .fin_paymentmethod_id("Wire Transfer")
            .fin_financial_account_id("Bank - Account 1 - USD")
            .c_currency_id("USD")
            .actual_payment("27.54")
            .expected_payment("27.54")
            .amount_gl_items("0.00")
            .amount_inv_ords("27.54")
            .total("27.54")
            .difference("0.00")
            .build(),

        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("27.54")
            .summedLineAmount("27.54")
            .currency("USD")
            .paymentComplete(true)
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 29963 - Sales flow
   *
   * @throws ParseException
   */
  @Test
  public void SALRegression29963Test() throws ParseException {
    logger
        .info("** Start of test case [SALRegression29963] Test regression 29963 - Sales flow. **");

    mainPage.getNavigationBar()
        .changeProfile(
            new ProfileData.Builder().role("F&B US, Inc. - Finance - F&B International Group")
                .build());
    Sleep.smartWaitButtonEnabled("UINAVBA_RecentLaunchList_BUTTON", 200);
    Sleep.trySleep(10000);

    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    String invoiceNo = salesInvoice.getData("documentNo").toString();
    SalesInvoice.Lines salesInvoiceLines = salesInvoice.new Lines(mainPage);
    salesInvoiceLines.create(salesInvoiceLineData);
    salesInvoiceLines.assertSaved();
    salesInvoiceLines.assertData(salesInvoiceLineVerificationData);
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);

    salesInvoice.post();
    salesInvoice.open();
    assertTrue(salesInvoice.isPosted());

    AddPaymentProcess addPaymentProcess = salesInvoice.openAddPayment();
    addPaymentProcess.process("Invoices", invoiceNo, "Process Received Payment(s) and Deposit",
        addPaymentVerificationData);
    salesInvoice.assertPaymentCreatedSuccessfully();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData2);

    logger.info("** End of test case [SALRegression29963] Test regression 29963 - Sales flow. **");
  }

  @AfterClass
  public static void tearDown() {
    SeleniumSingleton.INSTANCE.quit();
    OpenbravoERPTest.seleniumStarted = false;
    OpenbravoERPTest.forceLoginRequired();
  }
}
