/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014 Openbravo S.L.U.
 * All Rights Reserved.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_AddPaymentRefactor.APR_AddPaymentRefactor1;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

@RunWith(Parameterized.class)
public class APRPurchase004_005 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PaymentOutHeaderData paymentOutHeaderData;
  AddPaymentPopUpData addPaymentTotalsVerificationData;
  PaymentOutHeaderData paymentOutHeaderData2;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceLinesData purchaseInvoiceLinesData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;
  PaymentPlanData purchaseInvoicePaymentOutPlanData;
  AddPaymentPopUpData addPaymentTotalsVerificationData2;
  PaymentOutHeaderData paymentOutHeaderData3;

  public APRPurchase004_005(PaymentOutHeaderData paymentOutHeaderData,
      AddPaymentPopUpData addPaymentTotalsVerificationData,
      PaymentOutHeaderData paymentOutHeaderData2,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceLinesData purchaseInvoiceLinesData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      PaymentPlanData purchaseInvoicePaymentOutPlanData,
      AddPaymentPopUpData addPaymentTotalsVerificationData2,
      PaymentOutHeaderData paymentOutHeaderData3) {

    this.paymentOutHeaderData = paymentOutHeaderData;
    this.addPaymentTotalsVerificationData = addPaymentTotalsVerificationData;
    this.paymentOutHeaderData2 = paymentOutHeaderData2;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceLinesData = purchaseInvoiceLinesData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.purchaseInvoicePaymentOutPlanData = purchaseInvoicePaymentOutPlanData;
    this.addPaymentTotalsVerificationData2 = addPaymentTotalsVerificationData2;
    this.paymentOutHeaderData3 = paymentOutHeaderData3;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  @Parameters
  public static Collection<Object[]> PaymentOutDisplaysValues() {

    return Arrays.asList(new Object[][] { {
        new PaymentOutHeaderData.Builder().organization("Spain")
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .generatedCredit("2000.00")
            .build(),
        new AddPaymentPopUpData.Builder().amount_gl_items("0.00")
            .amount_inv_ords("0.00")
            .total("0.00")
            .difference("2,000.00")
            .build(),
        new PaymentOutHeaderData.Builder().organization("Spain")
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .build(),
        new PurchaseInvoiceHeaderData.Builder().transactionDocument("AP Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .build(),
        new PurchaseInvoiceLinesData.Builder().invoicedQuantity("10")
            .product(new ProductSimpleSelectorData.Builder().searchKey("RMA")
                .priceListVersionName("Purchase")
                .build())
            .build(),
        new PurchaseInvoiceHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .invoiceDate(OBDate.CURRENT_DATE)
            .accountingDate(OBDate.CURRENT_DATE)
            .priceList("Purchase")
            .paymentMethod("1 (Spain)")
            .paymentTerms("90 days")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().totalPaid("0.00")
            .outstandingAmount("22.00")
            .daysTillDue("90")
            .dueAmount("0.00")
            .summedLineAmount("20.00")
            .grandTotalAmount("22.00")
            .documentStatus("Completed")
            .currency("EUR")
            .paymentComplete(false)
            .build(),
        new PaymentPlanData.Builder().dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("1 (Spain)")
            .expected("22.00")
            .paid("0.00")
            .outstanding("22.00")
            .lastPaymentDate("")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),
        new AddPaymentPopUpData.Builder().amount_gl_items("0.00")
            .amount_inv_ords("22.00")
            .total("22.00")
            .difference("0.00")
            .build(),
        new PaymentOutHeaderData.Builder().organization("USA")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor France").build())
            .paymentMethod("Acc-6 (Transactions)")
            .account("Accounting Documents EURO - EUR")
            .currency("USD")
            .build() } });
  }

  /**
   * Test the creation of a purchase order and an invoice.
   */
  @Test
  public void executeAPRPurchase004_005() {
    logger.info("** Start of test case [APRPurchase004] **");
    // Test APRPurchase004
    PaymentOut paymentOut = new PaymentOut(mainPage).open();
    paymentOut.create(paymentOutHeaderData);
    paymentOut.assertSaved();
    String documentnocredit = paymentOut.getData("documentNo").toString();
    AddPaymentProcess addPaymentProcess = paymentOut.addDetailsOpen();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.assertTotalsData(addPaymentTotalsVerificationData);
    addPaymentProcess.process("Process Made Payment(s)");
    logger.info("** End of test case [APRPurchase004] **");
    // Test APRPurchase005
    logger.info("** Start of test case [APRPurchase005] **");
    // Create Purchase Invoice
    final PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();

    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    String invoiceNo = purchaseInvoice.getData("documentNo").toString();
    PurchaseInvoice.Lines purchaseInvoiceLines = purchaseInvoice.new Lines(mainPage);

    purchaseInvoiceLines.create(purchaseInvoiceLinesData);
    purchaseInvoiceLines.assertCount(1);
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();

    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData);
    PurchaseInvoice.PaymentOutPlan purchaseInvoicePaymentOutPlan = purchaseInvoice.new PaymentOutPlan(
        mainPage);
    purchaseInvoicePaymentOutPlan.assertCount(1);
    purchaseInvoicePaymentOutPlan.assertData(purchaseInvoicePaymentOutPlanData);
    PurchaseInvoice.PaymentOutPlan.PaymentOutDetails purchaseInvoicePaymentOutDetails = purchaseInvoicePaymentOutPlan.new PaymentOutDetails(
        mainPage);
    purchaseInvoicePaymentOutDetails.assertCount(0);
    purchaseInvoicePaymentOutPlan.assertCount(1);
    purchaseInvoicePaymentOutPlan.assertData(purchaseInvoicePaymentOutPlanData);

    PaymentOut paymentOut2 = new PaymentOut(mainPage).open();
    paymentOut2.create(paymentOutHeaderData2);
    AddPaymentProcess addPaymentProcess2 = paymentOut2.addDetailsOpen();
    addPaymentProcess2.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess2.getOrderInvoiceGrid().filter("invoiceNo", invoiceNo);
    Sleep.trySleep(2000);
    addPaymentProcess2.getOrderInvoiceGrid().selectRowByContents("invoiceNo", invoiceNo);
    addPaymentProcess2.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess2.scrollToBottom();
    addPaymentProcess2.getCreditToUseGrid().waitForDataToLoad();
    addPaymentProcess2.getCreditToUseGrid().filter("documentNo", documentnocredit);
    Sleep.trySleep(2000);
    addPaymentProcess2.getCreditToUseGrid().selectRowByContents("documentNo", documentnocredit);

    Sleep.trySleep(3500);
    addPaymentProcess2.editCreditToUseRecord(0,
        new SelectedPaymentData.Builder().paymentAmount("22.00").build());
    Sleep.trySleep(2000);
    addPaymentProcess2.getCreditToUseGrid().waitForDataToLoad();
    addPaymentProcess2.pressTab();
    addPaymentProcess2.assertTotalsData(addPaymentTotalsVerificationData2);
    addPaymentProcess2.process("Process Made Payment(s)");
    paymentOut2.assertPaymentCreatedSuccessfully();

    PaymentOut paymentOut3 = new PaymentOut(mainPage).open();
    paymentOut3.create(paymentOutHeaderData3);
    AddPaymentProcess addPaymentProcess3 = paymentOut3.addDetailsOpen();
    addPaymentProcess3.getOrderInvoiceGrid().waitForDataToLoad();

    addPaymentProcess3.close();

    logger.info("** End of test case [APRPurchase005] **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
