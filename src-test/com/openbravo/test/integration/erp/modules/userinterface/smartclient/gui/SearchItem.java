/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui;

import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions on OpenbravoERP search items built upon SmartClient user interface library.
 *
 * @author elopio
 *
 */
public class SearchItem extends FormItem {

  /**
   * Formatting string to locate the search button on the search item. The parameter is the locator
   * of the search item.
   */
  private final static String FORMAT_LOCATOR_SMART_CLIENT_PICKER = "%s/[icon='picker']";
  /**
   * Formatting string to get the target entity of the search item. The parameter is the object
   * string.
   */
  private final static String FORMAT_SMART_CLIENT_GET_TARGET_ENTITY = "%s.targetEntity";

  /* Components */
  /** The text box that shows the currently selected value. It may be editable. */
  private final TextItem textBoxValue;
  /** The button that opens the selector pop up. */
  private final Button buttonSearch;

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   */
  public SearchItem(String objectString) {
    super(objectString);
    this.textBoxValue = new TextItem(objectString);
    this.buttonSearch = new Button();
    this.buttonSearch.setSmartClientLocator(getPickerLocator());
  }

  /**
   * Get the SmartClient locator of the picker button component.
   *
   * @return the SmartClient locator of the picker button component.
   */
  private String getPickerLocator() {
    return String.format(FORMAT_LOCATOR_SMART_CLIENT_PICKER, getSmartClientLocator());
  }

  /**
   * Open the search pop up.
   */
  public void openSearchPopUp() {
    buttonSearch.click();
  }

  /**
   * Get the target entity of the search item.
   *
   * @return the target entity.
   */
  public String getTargetEntity() {
    return (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
        String.format(FORMAT_SMART_CLIENT_GET_TARGET_ENTITY, objectString));
  }

  /**
   * Get the value of the item.
   *
   * @return the value of the item.
   */
  public String getValue() {
    return textBoxValue.getValue();
  }

}
