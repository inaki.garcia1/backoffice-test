/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.data.selectors;

/**
 * Class for location data.
 *
 * @author elopio
 *
 */
public class LocationSelectorData {

  /** Formatting string used to get a string representation of this object. */
  private static final String FORMAT_STRING_REPRESENTATION = "[Location data:\n"
      + "first line=%s,\n" + "second line=%s\n" + "postal code=%s,\n" + "city=%s,\n"
      + "contry=%s,\n" + "region=%s]";

  /* Data fields. */
  /** The first line in address. */
  private String firstLine;
  /** The second line in address. */
  private String secondLine;
  /** The postal code in address. */
  private String postalCode;
  /** The name of the city. */
  private String city;
  /** The name of the country. */
  private String country;
  /** The name of the region. */
  private String region;

  /**
   * Class builder.
   *
   * @author elopio
   *
   */
  public static class Builder {

    /** The first line in address. */
    private String firstLine;
    /** The second line in address. */
    private String secondLine;
    /** The postal code in address. */
    private String postalCode;
    /** The name of the city. */
    private String city;
    /** The name of the country. */
    private String country;
    /** The name of the region. */
    private String region;

    /**
     * Set the address' first line.
     *
     * @param value
     *          The first line in address.
     * @return the builder for this class.
     */
    public Builder firstLine(String value) {
      this.firstLine = value;
      return this;
    }

    /**
     * Set the address' second line.
     *
     * @param value
     *          The second line in address.
     * @return the builder for this class.
     */
    public Builder secondLine(String value) {
      this.secondLine = value;
      return this;
    }

    /**
     * Set the address'postal code.
     *
     * @param value
     *          The postal code in address.
     * @return the builder for this class.
     */
    public Builder postalCode(String value) {
      this.postalCode = value;
      return this;
    }

    /**
     * Set the name of the city.
     *
     * @param value
     *          The name of the city.
     * @return the builder for this class.
     */
    public Builder city(String value) {
      this.city = value;
      return this;
    }

    /**
     * Set the name of the country.
     *
     * @param value
     *          The name of the country.
     * @return the builder for this class.
     */
    public Builder country(String value) {
      this.country = value;
      return this;
    }

    /**
     * Set the name of the region.
     *
     * @param value
     *          The name of the region.
     * @return the builder for this class.
     */
    public Builder region(String value) {
      this.region = value;
      return this;
    }

    /**
     * Build the data object.
     *
     * @return the data object.
     */
    public LocationSelectorData build() {
      return new LocationSelectorData(this);
    }
  }

  /**
   * Class constructor.
   *
   * @param builder
   *          The object builder.
   */
  private LocationSelectorData(Builder builder) {
    firstLine = builder.firstLine;
    secondLine = builder.secondLine;
    postalCode = builder.postalCode;
    city = builder.city;
    country = builder.country;
    region = builder.region;
  }

  /**
   * Get the first line of the address.
   *
   * @return the first line of the address.
   */
  public String getFirstLine() {
    return firstLine;
  }

  /**
   * Get the second line of the address.
   *
   * @return the second line of the address.
   */
  public String getSecondLine() {
    return secondLine;
  }

  /**
   * Get the postal code of the address.
   *
   * @return the postal code of the address.
   */
  public String getPostalCode() {
    return postalCode;
  }

  /**
   * Get the name of the city.
   *
   * @return the name of the city.
   */
  public String getCity() {
    return city;
  }

  /**
   * Get the name of the country.
   *
   * @return the name of the country.
   */
  public String getCountry() {
    return country;
  }

  /**
   * Get the name of the region.
   *
   * @return the name of the region.
   */
  public String getRegion() {
    return region;
  }

  /**
   * Get the string representation of this data object.
   *
   * @return the string representation.
   */
  @Override
  public String toString() {
    return String.format(FORMAT_STRING_REPRESENTATION, firstLine, secondLine, postalCode, city,
        country, region);
  }
}
