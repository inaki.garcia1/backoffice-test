/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.accounting.setup.taxrate;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for TaxZoneData
 *
 * @author plujan
 *
 */
public class TaxZoneData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the destinationRegion value
     *
     * Description: The state/province inside of a country receiving the shipment.
     *
     * @param value
     *          The destinationRegion value.
     * @return The builder for this class.
     */
    public Builder destinationRegion(String value) {
      this.dataFields.put("destinationRegion", value);
      return this;
    }

    /**
     * Set the destinationCountry value
     *
     * Description: The country receiving a shipment
     *
     * @param value
     *          The destinationCountry value.
     * @return The builder for this class.
     */
    public Builder destinationCountry(String value) {
      this.dataFields.put("destinationCountry", value);
      return this;
    }

    /**
     * Set the fromRegion value
     *
     * Description: From Region
     *
     * @param value
     *          The fromRegion value.
     * @return The builder for this class.
     */
    public Builder fromRegion(String value) {
      this.dataFields.put("fromRegion", value);
      return this;
    }

    /**
     * Set the fromCountry value
     *
     * Description: From Country
     *
     * @param value
     *          The fromCountry value.
     * @return The builder for this class.
     */
    public Builder fromCountry(String value) {
      this.dataFields.put("fromCountry", value);
      return this;
    }

    /**
     * Set the tax value
     *
     * Description: The percentage of money requested by the government for this specified product
     * or transaction.
     *
     * @param value
     *          The tax value.
     * @return The builder for this class.
     */
    public Builder tax(String value) {
      this.dataFields.put("tax", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public TaxZoneData build() {
      return new TaxZoneData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private TaxZoneData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
