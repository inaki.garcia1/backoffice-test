/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.workspace.widget;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ParameterData
 *
 * @author plujan
 *
 */
public class ParameterData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the evaluateFixedValue value
     *
     * Description: Evaluate Fixed Value
     *
     * @param value
     *          The evaluateFixedValue value.
     * @return The builder for this class.
     */
    public Builder evaluateFixedValue(Boolean value) {
      this.dataFields.put("evaluateFixedValue", value);
      return this;
    }

    /**
     * Set the sequenceNumber value
     *
     * Description: The order of records in a specified document.
     *
     * @param value
     *          The sequenceNumber value.
     * @return The builder for this class.
     */
    public Builder sequenceNumber(String value) {
      this.dataFields.put("sequenceNumber", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the mandatory value
     *
     * Description: An indication noting that completing in a field is required to proceed.
     *
     * @param value
     *          The mandatory value.
     * @return The builder for this class.
     */
    public Builder mandatory(Boolean value) {
      this.dataFields.put("mandatory", value);
      return this;
    }

    /**
     * Set the fixed value
     *
     * Description: A means of locking the header tab so it will not be affected if a specified
     * process is run again.
     *
     * @param value
     *          The fixed value.
     * @return The builder for this class.
     */
    public Builder fixed(Boolean value) {
      this.dataFields.put("fixed", value);
      return this;
    }

    /**
     * Set the centralMaintenance value
     *
     * Description: A flag indicating that this label is managed in a central repository.
     *
     * @param value
     *          The centralMaintenance value.
     * @return The builder for this class.
     */
    public Builder centralMaintenance(Boolean value) {
      this.dataFields.put("centralMaintenance", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the helpComment value
     *
     * Description: A comment that adds additional information to help users work with fields.
     *
     * @param value
     *          The helpComment value.
     * @return The builder for this class.
     */
    public Builder helpComment(String value) {
      this.dataFields.put("helpComment", value);
      return this;
    }

    /**
     * Set the fixedValue value
     *
     * Description: Fixed Value
     *
     * @param value
     *          The fixedValue value.
     * @return The builder for this class.
     */
    public Builder fixedValue(String value) {
      this.dataFields.put("fixedValue", value);
      return this;
    }

    /**
     * Set the length value
     *
     * Description: An indication of the column length as defined in the database.
     *
     * @param value
     *          The length value.
     * @return The builder for this class.
     */
    public Builder length(String value) {
      this.dataFields.put("length", value);
      return this;
    }

    /**
     * Set the eMObkmoWidgetClassID value
     *
     * Description: EM_Obkmo_Widget_Class_ID
     *
     * @param value
     *          The eMObkmoWidgetClassID value.
     * @return The builder for this class.
     */
    public Builder eMObkmoWidgetClassID(String value) {
      this.dataFields.put("eMObkmoWidgetClassID", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the defaultValue value
     *
     * Description: The first non-null value in a set of values. It is used as a default value for a
     * field when creating a record.
     *
     * @param value
     *          The defaultValue value.
     * @return The builder for this class.
     */
    public Builder defaultValue(String value) {
      this.dataFields.put("defaultValue", value);
      return this;
    }

    /**
     * Set the dBColumnName value
     *
     * Description: The name of a column within the database.
     *
     * @param value
     *          The dBColumnName value.
     * @return The builder for this class.
     */
    public Builder dBColumnName(String value) {
      this.dataFields.put("dBColumnName", value);
      return this;
    }

    /**
     * Set the referenceSearchKey value
     *
     * Description: The exact reference specification for a list or a table.
     *
     * @param value
     *          The referenceSearchKey value.
     * @return The builder for this class.
     */
    public Builder referenceSearchKey(String value) {
      this.dataFields.put("referenceSearchKey", value);
      return this;
    }

    /**
     * Set the reference value
     *
     * Description: The data type of this field.
     *
     * @param value
     *          The reference value.
     * @return The builder for this class.
     */
    public Builder reference(String value) {
      this.dataFields.put("reference", value);
      return this;
    }

    /**
     * Set the module value
     *
     * Description: Module
     *
     * @param value
     *          The module value.
     * @return The builder for this class.
     */
    public Builder module(String value) {
      this.dataFields.put("module", value);
      return this;
    }

    /**
     * Set the applicationElement value
     *
     * Description: An element that consolidates help descriptions and terms for a database column
     * and allows for a central maintenance.
     *
     * @param value
     *          The applicationElement value.
     * @return The builder for this class.
     */
    public Builder applicationElement(String value) {
      this.dataFields.put("applicationElement", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ParameterData build() {
      return new ParameterData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ParameterData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
