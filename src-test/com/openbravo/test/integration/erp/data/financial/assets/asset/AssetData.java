/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.assets.asset;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.LocationSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;

/**
 *
 * Class for AssetData
 *
 * @author plujan
 *
 */
public class AssetData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the lotName value
     *
     * Description: A group of identical or similar items organized and placed into inventory under
     * one number.
     *
     * @param value
     *          The lotName value.
     * @return The builder for this class.
     */
    public Builder lotName(String value) {
      this.dataFields.put("lotName", value);
      return this;
    }

    /**
     * Set the serialNo value
     *
     * Description: An attribute used as a unique identifier for a product.
     *
     * @param value
     *          The serialNo value.
     * @return The builder for this class.
     */
    public Builder serialNo(String value) {
      this.dataFields.put("serialNo", value);
      return this;
    }

    /**
     * Set the versionNo value
     *
     * Description: Version Number
     *
     * @param value
     *          The versionNo value.
     * @return The builder for this class.
     */
    public Builder versionNo(String value) {
      this.dataFields.put("versionNo", value);
      return this;
    }

    /**
     * Set the fullyDepreciated value
     *
     * Description: The asset is fully depreciated
     *
     * @param value
     *          The fullyDepreciated value.
     * @return The builder for this class.
     */
    public Builder fullyDepreciated(Boolean value) {
      this.dataFields.put("fullyDepreciated", value);
      return this;
    }

    /**
     * Set the expirationDate value
     *
     * Description: The date upon which an item is guaranteed to be of good quality.
     *
     * @param value
     *          The expirationDate value.
     * @return The builder for this class.
     */
    public Builder expirationDate(String value) {
      this.dataFields.put("expirationDate", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the helpComment value
     *
     * Description: A comment that adds additional information to help users work with fields.
     *
     * @param value
     *          The helpComment value.
     * @return The builder for this class.
     */
    public Builder helpComment(String value) {
      this.dataFields.put("helpComment", value);
      return this;
    }

    /**
     * Set the lifeUse value
     *
     * Description: Units of use until the asset is not usable anymore
     *
     * @param value
     *          The lifeUse value.
     * @return The builder for this class.
     */
    public Builder lifeUse(String value) {
      this.dataFields.put("lifeUse", value);
      return this;
    }

    /**
     * Set the depreciate value
     *
     * Description: x not implemented
     *
     * @param value
     *          The depreciate value.
     * @return The builder for this class.
     */
    public Builder depreciate(Boolean value) {
      this.dataFields.put("depreciate", value);
      return this;
    }

    /**
     * Set the locationComment value
     *
     * Description: Additional comments or remarks concerning the location
     *
     * @param value
     *          The locationComment value.
     * @return The builder for this class.
     */
    public Builder locationComment(String value) {
      this.dataFields.put("locationComment", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the owned value
     *
     * Description: The asset is owned by the organization
     *
     * @param value
     *          The owned value.
     * @return The builder for this class.
     */
    public Builder owned(Boolean value) {
      this.dataFields.put("owned", value);
      return this;
    }

    /**
     * Set the searchKey value
     *
     * Description: A fast method for finding a particular record.
     *
     * @param value
     *          The searchKey value.
     * @return The builder for this class.
     */
    public Builder searchKey(String value) {
      this.dataFields.put("searchKey", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the assetCategory value
     *
     * Description: A classification of assets based on similar characteristics.
     *
     * @param value
     *          The assetCategory value.
     * @return The builder for this class.
     */
    public Builder assetCategory(String value) {
      this.dataFields.put("assetCategory", value);
      return this;
    }

    /**
     * Set the locationAddress value
     *
     * Description: A specific place or residence.
     *
     * @param value
     *          The locationAddress value.
     * @return The builder for this class.
     */
    public Builder locationAddress(LocationSelectorData value) {
      this.dataFields.put("locationAddress", value);
      return this;
    }

    /**
     * Set the assetDisposalDate value
     *
     * Description: Asset disposal date
     *
     * @param value
     *          The assetDisposalDate value.
     * @return The builder for this class.
     */
    public Builder assetDisposalDate(String value) {
      this.dataFields.put("assetDisposalDate", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the usableLifeYears value
     *
     * Description: Years of the usable life of the asset
     *
     * @param value
     *          The usableLifeYears value.
     * @return The builder for this class.
     */
    public Builder usableLifeYears(String value) {
      this.dataFields.put("usableLifeYears", value);
      return this;
    }

    /**
     * Set the partnerAddress value
     *
     * Description: The location of the selected business partner.
     *
     * @param value
     *          The partnerAddress value.
     * @return The builder for this class.
     */
    public Builder partnerAddress(String value) {
      this.dataFields.put("partnerAddress", value);
      return this;
    }

    /**
     * Set the useUnits value
     *
     * Description: Currently used units of the assets
     *
     * @param value
     *          The useUnits value.
     * @return The builder for this class.
     */
    public Builder useUnits(String value) {
      this.dataFields.put("useUnits", value);
      return this;
    }

    /**
     * Set the inServiceDate value
     *
     * Description: Date when Asset was put into service
     *
     * @param value
     *          The inServiceDate value.
     * @return The builder for this class.
     */
    public Builder inServiceDate(String value) {
      this.dataFields.put("inServiceDate", value);
      return this;
    }

    /**
     * Set the usableLifeMonths value
     *
     * Description: Months of the usable life of the asset
     *
     * @param value
     *          The usableLifeMonths value.
     * @return The builder for this class.
     */
    public Builder usableLifeMonths(String value) {
      this.dataFields.put("usableLifeMonths", value);
      return this;
    }

    /**
     * Set the assetDepreciationDate value
     *
     * Description: Date of last depreciation
     *
     * @param value
     *          The assetDepreciationDate value.
     * @return The builder for this class.
     */
    public Builder assetDepreciationDate(String value) {
      this.dataFields.put("assetDepreciationDate", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the inPossession value
     *
     * Description: The asset is in the possession of the organization
     *
     * @param value
     *          The inPossession value.
     * @return The builder for this class.
     */
    public Builder inPossession(Boolean value) {
      this.dataFields.put("inPossession", value);
      return this;
    }

    /**
     * Set the storageBin value
     *
     * Description: A set of coordinates (x y z) which help locate an item in a warehouse.
     *
     * @param value
     *          The storageBin value.
     * @return The builder for this class.
     */
    public Builder storageBin(String value) {
      this.dataFields.put("storageBin", value);
      return this;
    }

    /**
     * Set the disposed value
     *
     * Description: The asset is disposed
     *
     * @param value
     *          The disposed value.
     * @return The builder for this class.
     */
    public Builder disposed(Boolean value) {
      this.dataFields.put("disposed", value);
      return this;
    }

    /**
     * Set the userContact value
     *
     * Description: An acquaintance to reach for information related to the business partner.
     *
     * @param value
     *          The userContact value.
     * @return The builder for this class.
     */
    public Builder userContact(String value) {
      this.dataFields.put("userContact", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public AssetData build() {
      return new AssetData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private AssetData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
