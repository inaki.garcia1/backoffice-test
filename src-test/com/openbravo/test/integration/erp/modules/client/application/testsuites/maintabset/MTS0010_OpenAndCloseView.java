/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Rowinson Gallego <rwn.gallego@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testsuites.maintabset;

import java.util.Arrays;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;
import com.openbravo.test.integration.erp.modules.client.application.testscripts.maintabset.OpenAndCloseView;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Test the selection of a recently selected menu item using the Navigation Bar Components module.
 *
 * @author Rowinson Gallego
 */
@RunWith(Parameterized.class)
public class MTS0010_OpenAndCloseView extends OpenAndCloseView {

  /**
   * Class constructor.
   *
   * @param items
   *          the path of the item to select.
   */
  public MTS0010_OpenAndCloseView(String[] items) {
    super(items);
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<Object[]> menuItemValues() {
    return Arrays.asList(new Object[][] { {
        new String[] { Menu.MASTER_DATA_MANAGEMENT, Menu.PRODUCT_SETUP, Menu.UNIT_OF_MEASURE } } });
  }

}
