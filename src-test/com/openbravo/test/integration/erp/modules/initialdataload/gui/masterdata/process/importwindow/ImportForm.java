/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.initialdataload.gui.masterdata.process.importwindow;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.gui.MessageBox;
import com.openbravo.test.integration.erp.modules.client.application.gui.ClassicFormView;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;
import com.openbravo.test.integration.erp.modules.initialdataload.data.masterdata.process.importwindow.ImportData;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verify actions on the Import form window of OpenbravoERP.
 *
 * @author elopio
 *
 */
public class ImportForm extends ClassicFormView {

  /** The window title. */
  private static final String TITLE = "Import";
  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.MASTER_DATA_MANAGEMENT,
      Menu.INITIAL_DATA_LOAD, Menu.INITIAL_DATA_LOAD_PROCESS, Menu.INITIAL_DATA_LOAD_IMPORT };

  /** Identifier of the file input field. */
  private static final String FILE_INPUT_FILE = "inpFile";
  /** Identifier of the entity combo box. */
  private static final String COMBO_BOX_ENTITY = "inpidlentityId";
  /** Identifier of the validate button. */
  private static final String BUTTON_OK = "buttonOK";
  /** Identifier of the process button. */
  private static final String BUTTON_PROCESS = "buttonProcess";

  // TODO file a bug. The title should be "Process completed successfully".
  /** Title displayed when a process was completed successfully. */
  public static final String TITLE_PROCESS_COMPLETED_SUCCESSFULLY = "Success";
  /**
   * Formatting string used to verify the message displayed after the validation process.
   */
  private static final String FORMAT_VALIDATION_MESSAGE = "Total of rows read: %dNumber of rows validated: %dNumber of row rejected: %d";
  /**
   * Formatting string used to verify the message displayed after the import process.
   */
  private static final String FORMAT_IMPORT_MESSAGE = "Total of rows read: %dNumber of rows processed: %dNumber of row rejected: %d";

  /**
   * Class constructor.
   *
   */
  public ImportForm() {
    super(TITLE, MENU_PATH);
  }

  /**
   * Verify that the entities combo box contains some entities.
   *
   * @param expectedEntities
   *          An array of entities that should be in the combo box.
   */
  public void verifyEntities(String[] expectedEntities) {
    final List<WebElement> entityWebElements = SeleniumSingleton.INSTANCE
        .findElementById(COMBO_BOX_ENTITY)
        .findElements(By.tagName("option"));
    final List<String> entityNames = new ArrayList<String>();
    for (WebElement entityWebElement : entityWebElements) {
      entityNames.add(entityWebElement.getText());
    }
    for (String entity : expectedEntities) {
      assertTrue(String.format("Entity %s not present.", entity), entityNames.contains(entity));
    }
  }

  /**
   * Fill the form.
   *
   * @param data
   *          The data of the import process.
   */
  public void fill(ImportData data) {
    if (data.getFile() != null) {
      SeleniumSingleton.INSTANCE.findElementByName(FILE_INPUT_FILE).sendKeys(data.getFile());
    }
    if (data.getEntity() != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_ENTITY))
          .selectByVisibleText(data.getEntity());
    }
  }

  /**
   * Validate a file.
   *
   * @param data
   *          The data for the import process.
   */
  public void validateFile(ImportData data) {
    fill(data);
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_OK).click();
  }

  /**
   * Verify the successful message displayed after validation.
   *
   * @param totalRows
   *          The number of rows in the file.
   * @param rowsValidated
   *          The number of rows validated.
   * @param rowsRejected
   *          The number of rows rejected.
   */
  public void verifyValidationProcessCompletedSuccessfully(int totalRows, int rowsValidated,
      int rowsRejected) {
    final MessageBox messageBox = new MessageBox();
    messageBox.verify(TITLE_PROCESS_COMPLETED_SUCCESSFULLY,
        String.format(FORMAT_VALIDATION_MESSAGE, totalRows, rowsValidated, rowsRejected));
  }

  /**
   * Process a file.
   *
   * @param data
   *          The data for the import process.
   */
  public void processFile(ImportData data) {
    fill(data);
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_PROCESS).click();
  }

  /**
   * Verify the successful message displayed after import process.
   *
   * @param totalRows
   *          The number of rows in the file.
   * @param rowsProcessed
   *          The number of rows processed.
   * @param rowsRejected
   *          The number of rows rejected.
   */
  public void verifyImportProcessCompletedSuccessfully(int totalRows, int rowsProcessed,
      int rowsRejected) {
    final MessageBox messageBox = new MessageBox();
    messageBox.verify(TITLE_PROCESS_COMPLETED_SUCCESSFULLY,
        String.format(FORMAT_IMPORT_MESSAGE, totalRows, rowsProcessed, rowsRejected));
  }
}
