/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>
 *  Iñaki Garcia <inaki.garcia@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes actions on OpenbravoERP select items built upon SmartClient user interface library.
 *
 * @author elopio
 *
 */
public class SelectItem extends FormItem implements InputField {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Formatting string to locate the picker button on the combo box. The parameter is the locator of
   * the combo box.
   */
  private final static String FORMAT_LOCATOR_SMART_CLIENT_PICKER = "%s/[icon='picker']";
  /**
   * Formatting string used to locate the list on the combo box. The parameter is the string that
   * can be used with selenium.getEval function to get the object.
   */
  private static final String FORMAT_SMART_CLIENT_GET_PICK_LIST = "%s.pickList";
  /**
   * Formatting string used to locate the list on the combo box. The parameter is the locator of the
   * combo box.
   */
  private static final String FORMAT_LOCATOR_SMART_CLIENT_LIST = "%s/pickList";
  /**
   * Formatting string used to get the display value of the SmartClient object. The parameter is the
   * object string.
   */
  private static final String FORMAT_SMART_CLIENT_GET_DISPLAY_VALUE = "%s.getDisplayValue()";

  /* Components */
  /** The text box that shows the currently selected value. It may be editable. */
  private TextItem textBoxValue;
  /** The button that opens the list of possible values. */
  private Button buttonPicker;
  /** The list box that shows possible values. */
  private ListItem listBoxValues;

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   */
  public SelectItem(String objectString) {
    super(objectString);
    this.textBoxValue = new TextItem(objectString);
    this.buttonPicker = new Button();
    this.buttonPicker.setSmartClientLocator(getPickerLocator());
  }

  /**
   * Class constructor.
   *
   * This constructor will just instantiate the select item, but after this the select item will not
   * be ready to be used. It has to be initialized using the function setSmartClientLocator in order
   * to be used with Selenium.
   *
   * This is an alternate way to access the SmartClient object, and should be used only if there is
   * no way to get the object string. Always prefer to call the other constructor with the object
   * string as a parameter, because it will set the SmartClient Locator. With this method the
   * actions that require to call SmartClient functions and attributes directly will not be
   * available.
   */
  public SelectItem() {
  }

  /**
   * Set the SmartClient Locator of the select item.
   *
   * @param smartClientLocator
   *          The SmartClient locator of the select item.
   */
  @Override
  public void setSmartClientLocator(String smartClientLocator) {
    super.setSmartClientLocator(smartClientLocator);
    this.textBoxValue = new TextItem();
    this.textBoxValue.setSmartClientLocator(smartClientLocator);
    this.buttonPicker = new Button();
    this.buttonPicker.setSmartClientLocator(getPickerLocator());
  }

  /**
   * Return true if the select item is present.
   *
   * @return true if the select item is present. Otherwise, false.
   */
  @Override
  public boolean isPresent() {
    return textBoxValue.isPresent();
  }

  /**
   * Return true if the select item is visible.
   *
   * @return true if the select item is visible. Otherwise, false.
   */
  @Override
  public boolean isVisible() {
    return textBoxValue.isVisible();
  }

  /**
   * Get the SmartClient locator of the picker button component.
   *
   * @return the SmartClient locator of the picker button component.
   */
  private String getPickerLocator() {
    return String.format(FORMAT_LOCATOR_SMART_CLIENT_PICKER, getSmartClientLocator());
  }

  /**
   * Get the SmartClient locator of the list.
   *
   * @return the SmartClient locator of the list.
   */
  private String getListLocator() {
    return String.format(FORMAT_LOCATOR_SMART_CLIENT_LIST, getSmartClientLocator());
  }

  /**
   * Get the text selected on the select item.
   *
   * @return the text selected on the select item.
   */
  public String getSelectedText() {
    if (objectString != null) {
      // It is safer to get the display value of the SmartClient object.
      return (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
          String.format(FORMAT_SMART_CLIENT_GET_DISPLAY_VALUE, objectString));
    } else {
      return textBoxValue.getText();
    }
  }

  /**
   * Click the select item.
   */
  @Override
  public void click() {
    textBoxValue.click();
  }

  /**
   * Enter a text on the select item.
   *
   * @param text
   *          The text to enter.
   */
  // TODO not all select items can be edited this way. Move this function to a
  // new class that inherits from this one, or that implements an editable
  // interface, or just leave it here.
  public void enterText(String text) {
    textBoxValue.enterText(text);
  }

  /**
   * Press the enter key on the select item.
   */
  @Override
  public void pressEnter() {
    textBoxValue.pressEnter();
  }

  /**
   * Initialize the list box with the values.
   */
  private void initializeListBox() {
    if (objectString == null) {
      listBoxValues = new ListItem();
      listBoxValues.setSmartClientLocator(getListLocator());
    } else {
      listBoxValues = new ListItem(String.format(FORMAT_SMART_CLIENT_GET_PICK_LIST, objectString));
    }
  }

  /**
   * Open the list box with the options.
   */
  private void openListBox() {
    logger.trace("Opening the items list.");
    // The field might have the list box with the options opened. If this is the case, the following
    // click will just close the box. Click it in order to make sure it's closed and that the next
    // click will work.
    click();
    // TODO L1: A Sleep.trySleep(5000); has been removed from the following line, if it is stable,
    // remove this comment line
    if (listBoxValues == null) {
      focus();
      // FIXME: <chromedriver> Skipping the click on the "Profile" tab of the role selection menu,
      // but not for the rest of the pickers
      // This is due to a bug with the listBox WebElement being shown as visible when executing the
      // test with Chrome (isVisible() returns true)
      // See issue ...
      // Remove following IF after updating FF to 60 + Se 3.13 if errors are thrown
      if ((objectString != null && !objectString.contains("UserProfile"))
          || (objectString == null)) {
        buttonPicker.click();
      }
      // FIXME it might take a while to load the items. How to wait for the
      // list to be fully loaded?
      Sleep.trySleep();
      initializeListBox();
      // XXX The field might have the list box with the options opened. If this is the case, the
      // previous click will just have closed the box but is still required because when the list is
      // opened it doesn't show all the values. So, if at this point the list box is not visible,
      // try to open it again. See issue 18075 - https://issues.openbravo.com/view.php?id=18075
      if (!listBoxValues.isVisible()) {
        buttonPicker.click();
        Sleep.setFluentWait(500, 30000, ElementNotInteractableException.class)
            .until(new ExpectedCondition<Boolean>() {
              @Override
              public Boolean apply(WebDriver webDriver) {
                buttonPicker.click();
                return listBoxValues.isVisible();
              }
            });
      }
    } else {
      logger.trace("The list box was already opened.");
    }
  }

  /**
   * Close the list box with the options.
   */
  private void closeListBox() {
    logger.trace("Closing the items list.");
    if (listBoxValues != null) {
      focus();
      buttonPicker.click();
      listBoxValues = null;
    } else {
      logger.trace("The list box was already closed.");
    }
  }

  /**
   * Get the list box with the options.
   *
   * @return the list box item.
   */
  public ListItem getListItem() {
    // TODO is the list box open?
    initializeListBox();
    return listBoxValues;
  }

  /**
   * Get the number of items in the list.
   *
   * @return the number of items in the list.
   */
  public int getItemCount() {
    openListBox();
    int itemCount = listBoxValues.getItemCount();
    closeListBox();
    return itemCount;
  }

  /**
   * Select an item on the select item.
   *
   * @param item
   *          The text of the item to select.
   */
  public void selectItem(String item) {
    logger.trace("Selecting the item '{}'", item);
    if (!getSelectedText().equals(item)) {
      openListBox();
      listBoxValues.selectItem(item);
      listBoxValues = null;
      closeListBox();
    } else {
      logger.trace("The item was already selected.");
    }
  }

  /**
   * Select an item on the select item.
   *
   * @param item
   *          The text of the item to select.
   * @param fieldName
   *          The name of the field.
   */
  public void selectItem(String item, String fieldName) {
    openListBox();
    listBoxValues.selectItem(item, fieldName);
    listBoxValues = null;
    closeListBox();
  }

  /**
   * Select an item on the select item box.
   *
   * @param item
   *          The text of the item to select.
   */
  @SuppressWarnings("unlikely-arg-type")
  public void selectItem(DataObject item) {
    logger.trace("Selecting the item '{}'", item);
    if (!getSelectedText().equals(item)) {
      openListBox();
      listBoxValues.selectItem(item);
      listBoxValues = null;
      closeListBox();
    } else {
      logger.trace("The item was already selected.");
    }
  }

  /**
   * Set the selected item.
   *
   * @param value
   *          The value to set.
   */
  @Override
  public void setValue(Object value) {
    // TODO DataObjects are also valid values, but they call the selectItem function directly.
    if (value instanceof String) {
      selectItem((String) value);
    } else {
      throw new IllegalArgumentException("Valid values for select items are only Strings.");
    }
  }

  /**
   * Get the selected item.
   */
  @Override
  public String getValue() {
    return getSelectedText();
  }
}
