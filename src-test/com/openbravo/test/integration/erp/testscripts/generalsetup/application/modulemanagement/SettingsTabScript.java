/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.application.modulemanagement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.gui.general.application.modulemanagement.ModuleManagementWindow;
import com.openbravo.test.integration.erp.gui.general.application.modulemanagement.SettingsFormTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the Settings form tab of OpenbravoERP.
 *
 * @author elopio
 */
public class SettingsTabScript {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Set the status for the scan and search
   *
   * @param mainPage
   *          The application main page.
   * @param scanStatus
   *          The status that will be set for the scan.
   * @param searchStatus
   *          The status that will be set for the search.
   * @return true if at least one of the status was changed. False if both statuses were already the
   *         requested.
   */
  static boolean setStatus(MainPage mainPage, String scanStatus, String searchStatus) {
    logger.info("Set scan status to {} and search status to {}.", scanStatus, searchStatus);
    ModuleManagementWindow moduleManagementWindow = new ModuleManagementWindow();
    mainPage.openView(moduleManagementWindow);
    SettingsFormTab settingsFormTab = moduleManagementWindow.selectSettingsTab();
    boolean statusChanged = settingsFormTab.setStatus(scanStatus, searchStatus);
    if (statusChanged) {
      settingsFormTab.clickSave();
      settingsFormTab.asssertSettingsSavedSuccessfully();
    }
    return statusChanged;
  }

  /**
   * Set the scan status.
   *
   * @param mainPage
   *          The application main page.
   * @param status
   *          The status that will be set.
   * @return true if the status was changed. False if the status was already the requested.
   */
  static boolean setScanStatus(MainPage mainPage, String status) {
    logger.info("Set scan status to {}.", status);
    ModuleManagementWindow moduleManagementWindow = new ModuleManagementWindow();
    mainPage.openView(moduleManagementWindow);
    SettingsFormTab settingsFormTab = moduleManagementWindow.selectSettingsTab();
    boolean statusChanged = settingsFormTab.setScanStatus(status);
    if (statusChanged) {
      settingsFormTab.clickSave();
      settingsFormTab.asssertSettingsSavedSuccessfully();
    }
    return statusChanged;
  }

  /**
   * Set the search status.
   *
   * @param mainPage
   *          The application main page.
   * @param status
   *          The status that will be set.
   * @return true if the status was changed. False if the status was already the requested.
   */
  static boolean setSearchStatus(MainPage mainPage, String status) {
    logger.info("Set search status to {}.", status);
    ModuleManagementWindow moduleManagementWindow = new ModuleManagementWindow();
    mainPage.openView(moduleManagementWindow);
    SettingsFormTab settingsFormTab = moduleManagementWindow.selectSettingsTab();
    boolean statusChanged = settingsFormTab.setSearchStatus(status);
    if (statusChanged) {
      settingsFormTab.clickSave();
      settingsFormTab.asssertSettingsSavedSuccessfully();
    }
    return statusChanged;
  }

}
