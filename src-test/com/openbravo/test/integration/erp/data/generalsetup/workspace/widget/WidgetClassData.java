/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.workspace.widget;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for WidgetClassData
 *
 * @author plujan
 *
 */
public class WidgetClassData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the dataAccessLevel value
     *
     * Description: Indicates what type of data (in terms of AD_CLIENT and AD_ORG data columns) can
     * be entered or viewed.
     *
     * @param value
     *          The dataAccessLevel value.
     * @return The builder for this class.
     */
    public Builder dataAccessLevel(String value) {
      this.dataFields.put("dataAccessLevel", value);
      return this;
    }

    /**
     * Set the widgetSuperclass value
     *
     * Description: Widget Superclass
     *
     * @param value
     *          The widgetSuperclass value.
     * @return The builder for this class.
     */
    public Builder widgetSuperclass(String value) {
      this.dataFields.put("widgetSuperclass", value);
      return this;
    }

    /**
     * Set the superclass value
     *
     * Description: Superclass
     *
     * @param value
     *          The superclass value.
     * @return The builder for this class.
     */
    public Builder superclass(Boolean value) {
      this.dataFields.put("superclass", value);
      return this;
    }

    /**
     * Set the allowAnonymousAccess value
     *
     * Description: Allow_Anonymous_Access
     *
     * @param value
     *          The allowAnonymousAccess value.
     * @return The builder for this class.
     */
    public Builder allowAnonymousAccess(Boolean value) {
      this.dataFields.put("allowAnonymousAccess", value);
      return this;
    }

    /**
     * Set the widgetTitle value
     *
     * Description: Widget Title
     *
     * @param value
     *          The widgetTitle value.
     * @return The builder for this class.
     */
    public Builder widgetTitle(String value) {
      this.dataFields.put("widgetTitle", value);
      return this;
    }

    /**
     * Set the javaClass value
     *
     * Description: Java Class implementing the widget
     *
     * @param value
     *          The javaClass value.
     * @return The builder for this class.
     */
    public Builder javaClass(String value) {
      this.dataFields.put("javaClass", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the height value
     *
     * Description: Height in pixels the widget will be represented with.
     *
     * @param value
     *          The height value.
     * @return The builder for this class.
     */
    public Builder height(String value) {
      this.dataFields.put("height", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the module value
     *
     * Description: Module
     *
     * @param value
     *          The module value.
     * @return The builder for this class.
     */
    public Builder module(String value) {
      this.dataFields.put("module", value);
      return this;
    }

    /**
     * Set the canMaximize value
     *
     * Description: If the widget has the hability to maximize
     *
     * @param value
     *          The canMaximize value.
     * @return The builder for this class.
     */
    public Builder canMaximize(Boolean value) {
      this.dataFields.put("canMaximize", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public WidgetClassData build() {
      return new WidgetClassData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private WidgetClassData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
