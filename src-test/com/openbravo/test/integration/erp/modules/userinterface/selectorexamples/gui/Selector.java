/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */
package com.openbravo.test.integration.erp.modules.userinterface.selectorexamples.gui;

import com.openbravo.test.integration.selenium.Attribute;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions on OpenbravoERP Selector. This element is added by the Selector module.
 *
 * @author elopio
 *
 */
public class Selector {

  /** String format used to locate the text field of a selector. */
  private static final String FORMAT_TEXT_FIELD_LOCATOR = "//td[@id='%s']//input";

  /** Identifier of the selector. */
  private String identifier;

  /**
   * Class constructor.
   *
   * webDriver variable is the Selenium object that will be used to execute actions on the
   * application.
   *
   * @param identifier
   *          The identifier of the selector.
   */
  public Selector(String identifier) {
    this.identifier = identifier;
  }

  /**
   * Type a text in the text field of a selector.
   *
   * @param text
   *          The text to type.
   */
  public void type(String text) {
    final String textFieldLocator = String.format(FORMAT_TEXT_FIELD_LOCATOR, identifier);
    SeleniumSingleton.INSTANCE.findElementByXPath(textFieldLocator).sendKeys(text);
  }

  /**
   * Get the value of a selector text field. This is the text displayed inside the input.
   *
   * @return the value of a selector text field.
   */
  public String getValue() {
    final String textFieldLocator = String.format(FORMAT_TEXT_FIELD_LOCATOR, identifier);
    return SeleniumSingleton.INSTANCE.findElementByXPath(textFieldLocator)
        .getAttribute(Attribute.VALUE.getName());
  }

}
