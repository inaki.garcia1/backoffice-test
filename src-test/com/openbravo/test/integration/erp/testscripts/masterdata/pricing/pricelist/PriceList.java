/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 *   Nono Carballo <nonofce@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.masterdata.pricing.pricelist;

import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelist.PriceListData;
import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelist.PriceListVersionData;
import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelist.ProductPriceData;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;

/**
 * Executes and verifies actions on the Price List window.
 *
 * @author Pablo Lujan
 */
public class PriceList {

  /**
   * Executes and verifies actions on the Price List tab.
   *
   * @author plujan
   *
   */
  public static class PriceListTab {

    /**
     * Creates the record.
     *
     * @param mainPage
     *          The application main page.
     * @param priceListData
     *
     */
    public static void create(MainPage mainPage, PriceListData priceListData) {
      if (mainPage.isOnNewLayout()) {
        PriceListScript.create(mainPage, priceListData);
      } else {
        PriceListClassicScript.create(mainPage, priceListData);
      }
    }
  }

  /**
   * Executes and verifies actions on the tab.
   *
   * @author plujan
   *
   */
  public static class PriceListVersionTab {

    /**
     * Creates the record.
     *
     * @param mainPage
     *          The application main page.
     * @param priceListVersionData
     *
     */
    public static void create(MainPage mainPage, PriceListVersionData priceListVersionData) {
      if (mainPage.isOnNewLayout()) {
        PriceListVersionScript.create(mainPage, priceListVersionData);
      } else {
        PriceListVersionClassicScript.create(mainPage, priceListVersionData);
      }
    }

    /**
     * Execute the process that creates the Price List.
     *
     * @param mainPage
     *          The application main page.
     * @throws OpenbravoERPTestException
     */
    public static void processCreatePricelist(MainPage mainPage) throws OpenbravoERPTestException {
      if (mainPage.isOnNewLayout()) {
        PriceListVersionScript.processCreatePricelist(mainPage);
      } else {
        PriceListVersionClassicScript.processCreatePricelist(mainPage);
      }
    }

    /**
     * Execute the process that creates the Price List.
     *
     * @param mainPage
     *          The application main page.
     * @throws OpenbravoERPTestException
     */
    public static void select(MainPage mainPage, PriceListVersionData priceListVersionData)
        throws OpenbravoERPTestException {
      if (mainPage.isOnNewLayout()) {
        PriceListVersionScript.select(mainPage, priceListVersionData);
      } else {
        PriceListVersionClassicScript.select(mainPage, priceListVersionData);
      }
    }
  }

  /**
   * Executes and verifies actions on the tab.
   *
   * @author plujan
   *
   */
  public static class ProductPriceTab {

    public static void verify(MainPage mainPage, ProductPriceData productPriceData) {
      if (mainPage.isOnNewLayout()) {
        ProductPriceScript.verify(mainPage, productPriceData);
      } else {
        ProductPriceClassicScript.verify(mainPage, productPriceData);
      }
    }

    public static void select(MainPage mainPage, ProductPriceData productPriceData)
        throws OpenbravoERPTestException {
      if (mainPage.isOnNewLayout()) {
        ProductPriceScript.select(mainPage, productPriceData);
      } else {
        ProductPriceClassicScript.select(mainPage, productPriceData);
      }
    }

    public static void create(MainPage mainPage, ProductPriceData productPriceData) {
      if (mainPage.isOnNewLayout()) {
        ProductPriceScript.create(mainPage, productPriceData);
      } else {

      }
    }
  }
}
