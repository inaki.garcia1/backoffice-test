/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentrun;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ParametersData
 *
 * @author plujan
 *
 */
public class ParametersData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the paymentExecutionProcessParameter value
     *
     * Description: Payment Execution Process Parameter
     *
     * @param value
     *          The paymentExecutionProcessParameter value.
     * @return The builder for this class.
     */
    public Builder paymentExecutionProcessParameter(String value) {
      this.dataFields.put("paymentExecutionProcessParameter", value);
      return this;
    }

    /**
     * Set the valueOfTheCheck value
     *
     * Description: Value of the Check
     *
     * @param value
     *          The valueOfTheCheck value.
     * @return The builder for this class.
     */
    public Builder valueOfTheCheck(Boolean value) {
      this.dataFields.put("valueOfTheCheck", value);
      return this;
    }

    /**
     * Set the paymentRun value
     *
     * Description: Payment Run
     *
     * @param value
     *          The paymentRun value.
     * @return The builder for this class.
     */
    public Builder paymentRun(String value) {
      this.dataFields.put("paymentRun", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the valueOfTheTextParameter value
     *
     * Description: Value of the Text Parameter
     *
     * @param value
     *          The valueOfTheTextParameter value.
     * @return The builder for this class.
     */
    public Builder valueOfTheTextParameter(String value) {
      this.dataFields.put("valueOfTheTextParameter", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ParametersData build() {
      return new ParametersData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ParametersData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
