/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016-2018 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.aum;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.procurement.AUMPROa_PurchaseOrder_GoodsReceipt_PurchaseInvoice;
import com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.procurement.AUMPROb_PurchaseOrder_PaymentOut_PurchaseInvoice;
import com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.procurement.AUMPROc_PurchaseInvoice;
import com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.procurement.AUMPROd_RequisitionToOrder;
import com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.procurement.AUMPROd_Requisition_PurchaseOrder;
import com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.procurement.AUMPROe_PurchaseOrder_CopyLines;
import com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.returntovendor.AUMRTVa000CreateReturnableProductWithAUM;
import com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.returntovendor.AUMRTVa010CreateEditDeleteLine;
import com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.returntovendor.AUMRTVa020ReturnedQyt;
import com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.returntovendor.AUMRTVa030FilteringPickEditLineButton;
import com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.returntovendor.AUMRTVa040LoadingPELines;
import com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.returntovendor.AUMRTVa050ReturnedQtyInOthers;
import com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.returntovendor.AUMRTVb010CreateAReturnToVendorDocument;
import com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.returntovendor.AUMRTVb020CreateEditDeleteLine;
import com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.returntovendor.AUMRTVb030ValidationsPickEditLinesButton;
import com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.returntovendor.AUMRTVc010InvoiceAReturnToVendorShipment;
import com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.returntovendor.AUMRTVd010CreateReversedInvoiceFromReturnToVendorShipment;
import com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.sales.AUMSALa_OrderToInvoice;
import com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.sales.AUMSALb_AddSalesInvoicePayment;
import com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.sales.AUMSALc_GenerateInvoice;
import com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.sales.AUMSALd_ReturnMaterial;
import com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.sales.AUMSALe_CloseSalesOrder;
import com.openbravo.test.integration.erp.testscripts.SuiteThatStopsIfFailure;

/**
 *
 * Test Suite for UOM Management Enhance test cases
 *
 * @author Nono Carballo
 *
 */
@RunWith(SuiteThatStopsIfFailure.class)
@Suite.SuiteClasses({ AUMa010EnablePreference.class, AUMa030AddAUMProduct.class,
    AUMa050CreateSalesOrder.class, AUMa060CreatePurchaseOrder.class,
    AUMPROa_PurchaseOrder_GoodsReceipt_PurchaseInvoice.class,
    AUMPROb_PurchaseOrder_PaymentOut_PurchaseInvoice.class, AUMPROc_PurchaseInvoice.class,
    AUMPROd_Requisition_PurchaseOrder.class, AUMPROd_RequisitionToOrder.class,
    AUMPROe_PurchaseOrder_CopyLines.class, AUMRTVa000CreateReturnableProductWithAUM.class,
    AUMRTVa010CreateEditDeleteLine.class, AUMRTVa020ReturnedQyt.class,
    AUMRTVa030FilteringPickEditLineButton.class, AUMRTVa040LoadingPELines.class,
    AUMRTVa050ReturnedQtyInOthers.class, AUMRTVb010CreateAReturnToVendorDocument.class,
    AUMRTVb020CreateEditDeleteLine.class, AUMRTVb030ValidationsPickEditLinesButton.class,
    AUMRTVc010InvoiceAReturnToVendorShipment.class,
    AUMRTVd010CreateReversedInvoiceFromReturnToVendorShipment.class, AUMSALa_OrderToInvoice.class,
    AUMSALb_AddSalesInvoicePayment.class, AUMSALc_GenerateInvoice.class,
    AUMSALd_ReturnMaterial.class, AUMSALe_CloseSalesOrder.class, AUMa040RemoveAUMProduct.class,
    AUMa020DisablePreference.class })
public class AUMTestSuite {

}
