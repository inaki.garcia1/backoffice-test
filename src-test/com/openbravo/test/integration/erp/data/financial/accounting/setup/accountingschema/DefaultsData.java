/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.accounting.setup.accountingschema;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for DefaultsData
 *
 * @author plujan
 *
 */
public class DefaultsData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the accountingSchema value
     *
     * Description: The structure used in accounting including costing methods currencies and the
     * calendar.
     *
     * @param value
     *          The accountingSchema value.
     * @return The builder for this class.
     */
    public Builder accountingSchema(String value) {
      this.dataFields.put("accountingSchema", value);
      return this;
    }

    /**
     * Set the customerPrepayment value
     *
     * Description: x not implemented
     *
     * @param value
     *          The customerPrepayment value.
     * @return The builder for this class.
     */
    public Builder customerPrepayment(String value) {
      this.dataFields.put("customerPrepayment", value);
      return this;
    }

    /**
     * Set the customerReceivablesNo value
     *
     * Description: An account number in the general ledger to mark transactions with a business
     * partner.
     *
     * @param value
     *          The customerReceivablesNo value.
     * @return The builder for this class.
     */
    public Builder customerReceivablesNo(String value) {
      this.dataFields.put("customerReceivablesNo", value);
      return this;
    }

    /**
     * Set the fixedAsset value
     *
     * Description: A tangible asset used for business without expectation of sale in the coming
     * fiscal year.
     *
     * @param value
     *          The fixedAsset value.
     * @return The builder for this class.
     */
    public Builder fixedAsset(String value) {
      this.dataFields.put("fixedAsset", value);
      return this;
    }

    /**
     * Set the productCOGS value
     *
     * Description: The cost of goods sold for a specified product.
     *
     * @param value
     *          The productCOGS value.
     * @return The builder for this class.
     */
    public Builder productCOGS(String value) {
      this.dataFields.put("productCOGS", value);
      return this;
    }

    /**
     * Set the productExpense value
     *
     * Description: An account number in the general ledger to mark transactions with this item.
     *
     * @param value
     *          The productExpense value.
     * @return The builder for this class.
     */
    public Builder productExpense(String value) {
      this.dataFields.put("productExpense", value);
      return this;
    }

    /**
     * Set the productRevenue value
     *
     * Description: An account number in the general ledger to mark transactions with this product.
     *
     * @param value
     *          The productRevenue value.
     * @return The builder for this class.
     */
    public Builder productRevenue(String value) {
      this.dataFields.put("productRevenue", value);
      return this;
    }

    /**
     * Set the vendorLiability value
     *
     * Description: An account number in the general ledger used to mark transactions with this
     * business partner.
     *
     * @param value
     *          The vendorLiability value.
     * @return The builder for this class.
     */
    public Builder vendorLiability(String value) {
      this.dataFields.put("vendorLiability", value);
      return this;
    }

    /**
     * Set the vendorPrepayment value
     *
     * Description: An account number in the general ledger used to mark transactions with this
     * business partner.
     *
     * @param value
     *          The vendorPrepayment value.
     * @return The builder for this class.
     */
    public Builder vendorPrepayment(String value) {
      this.dataFields.put("vendorPrepayment", value);
      return this;
    }

    /**
     * Set the warehouseDifferences value
     *
     * Description: Warehouse Differences Account
     *
     * @param value
     *          The warehouseDifferences value.
     * @return The builder for this class.
     */
    public Builder warehouseDifferences(String value) {
      this.dataFields.put("warehouseDifferences", value);
      return this;
    }

    /**
     * Set the warehouseInventory value
     *
     * Description: Warehouse Inventory Asset Account
     *
     * @param value
     *          The warehouseInventory value.
     * @return The builder for this class.
     */
    public Builder warehouseInventory(String value) {
      this.dataFields.put("warehouseInventory", value);
      return this;
    }

    /**
     * Set the nonInvoicedReceipts value
     *
     * Description: Account for not-invoiced Material Receipts
     *
     * @param value
     *          The nonInvoicedReceipts value.
     * @return The builder for this class.
     */
    public Builder nonInvoicedReceipts(String value) {
      this.dataFields.put("nonInvoicedReceipts", value);
      return this;
    }

    /**
     * Set the inventoryRevaluation value
     *
     * Description: Account for Inventory Revaluation
     *
     * @param value
     *          The inventoryRevaluation value.
     * @return The builder for this class.
     */
    public Builder inventoryRevaluation(String value) {
      this.dataFields.put("inventoryRevaluation", value);
      return this;
    }

    /**
     * Set the writeOff value
     *
     * Description: Account for Receivables write-off
     *
     * @param value
     *          The writeOff value.
     * @return The builder for this class.
     */
    public Builder writeOff(String value) {
      this.dataFields.put("writeOff", value);
      return this;
    }

    /**
     * Set the workInProgress value
     *
     * Description: An account used in production projects until it becomes defined as a finished
     * good.
     *
     * @param value
     *          The workInProgress value.
     * @return The builder for this class.
     */
    public Builder workInProgress(String value) {
      this.dataFields.put("workInProgress", value);
      return this;
    }

    /**
     * Set the taxDue value
     *
     * Description: Account for Tax you have to pay
     *
     * @param value
     *          The taxDue value.
     * @return The builder for this class.
     */
    public Builder taxDue(String value) {
      this.dataFields.put("taxDue", value);
      return this;
    }

    /**
     * Set the taxCredit value
     *
     * Description: Account for Tax you can reclaim
     *
     * @param value
     *          The taxCredit value.
     * @return The builder for this class.
     */
    public Builder taxCredit(String value) {
      this.dataFields.put("taxCredit", value);
      return this;
    }

    /**
     * Set the bankInTransit value
     *
     * Description: Bank In Transit Account
     *
     * @param value
     *          The bankInTransit value.
     * @return The builder for this class.
     */
    public Builder bankInTransit(String value) {
      this.dataFields.put("bankInTransit", value);
      return this;
    }

    /**
     * Set the bankAsset value
     *
     * Description: Bank Asset Account
     *
     * @param value
     *          The bankAsset value.
     * @return The builder for this class.
     */
    public Builder bankAsset(String value) {
      this.dataFields.put("bankAsset", value);
      return this;
    }

    /**
     * Set the bankExpense value
     *
     * Description: Bank Expense Account
     *
     * @param value
     *          The bankExpense value.
     * @return The builder for this class.
     */
    public Builder bankExpense(String value) {
      this.dataFields.put("bankExpense", value);
      return this;
    }

    /**
     * Set the bankRevaluationGain value
     *
     * Description: Bank Revaluation Gain Account
     *
     * @param value
     *          The bankRevaluationGain value.
     * @return The builder for this class.
     */
    public Builder bankRevaluationGain(String value) {
      this.dataFields.put("bankRevaluationGain", value);
      return this;
    }

    /**
     * Set the bankRevaluationLoss value
     *
     * Description: Bank Revaluation Loss Account
     *
     * @param value
     *          The bankRevaluationLoss value.
     * @return The builder for this class.
     */
    public Builder bankRevaluationLoss(String value) {
      this.dataFields.put("bankRevaluationLoss", value);
      return this;
    }

    /**
     * Set the chargeExpense value
     *
     * Description: Charge Expense Account
     *
     * @param value
     *          The chargeExpense value.
     * @return The builder for this class.
     */
    public Builder chargeExpense(String value) {
      this.dataFields.put("chargeExpense", value);
      return this;
    }

    /**
     * Set the cashBookAsset value
     *
     * Description: Cash Book Asset Account
     *
     * @param value
     *          The cashBookAsset value.
     * @return The builder for this class.
     */
    public Builder cashBookAsset(String value) {
      this.dataFields.put("cashBookAsset", value);
      return this;
    }

    /**
     * Set the cashBookDifferences value
     *
     * Description: Cash Book Differences Account
     *
     * @param value
     *          The cashBookDifferences value.
     * @return The builder for this class.
     */
    public Builder cashBookDifferences(String value) {
      this.dataFields.put("cashBookDifferences", value);
      return this;
    }

    /**
     * Set the cashBookExpense value
     *
     * Description: Cash Book Expense Account
     *
     * @param value
     *          The cashBookExpense value.
     * @return The builder for this class.
     */
    public Builder cashBookExpense(String value) {
      this.dataFields.put("cashBookExpense", value);
      return this;
    }

    /**
     * Set the cashBookReceipt value
     *
     * Description: Cash Book Receipts Account
     *
     * @param value
     *          The cashBookReceipt value.
     * @return The builder for this class.
     */
    public Builder cashBookReceipt(String value) {
      this.dataFields.put("cashBookReceipt", value);
      return this;
    }

    /**
     * Set the invoicePriceVariance value
     *
     * Description: The Invoice Price Variance is used reflects the difference between the current
     * Costs and the Invoice Price.
     *
     * @param value
     *          The invoicePriceVariance value.
     * @return The builder for this class.
     */
    public Builder invoicePriceVariance(String value) {
      this.dataFields.put("invoicePriceVariance", value);
      return this;
    }

    /**
     * Set the cashTransfer value
     *
     * Description: Cash Transfer Clearing Account
     *
     * @param value
     *          The cashTransfer value.
     * @return The builder for this class.
     */
    public Builder cashTransfer(String value) {
      this.dataFields.put("cashTransfer", value);
      return this;
    }

    /**
     * Set the accumulatedDepreciation value
     *
     * Description: Accumulated Depreciation
     *
     * @param value
     *          The accumulatedDepreciation value.
     * @return The builder for this class.
     */
    public Builder accumulatedDepreciation(String value) {
      this.dataFields.put("accumulatedDepreciation", value);
      return this;
    }

    /**
     * Set the depreciation value
     *
     * Description: Depreciation account
     *
     * @param value
     *          The depreciation value.
     * @return The builder for this class.
     */
    public Builder depreciation(String value) {
      this.dataFields.put("depreciation", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public DefaultsData build() {
      return new DefaultsData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private DefaultsData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
