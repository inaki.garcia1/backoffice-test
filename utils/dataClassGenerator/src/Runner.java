import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class Runner {
  static String BASE_DIRECTORY = "c:\\temp";

  /**
   * @param args
   */
  public static void main(String[] args) {
    // String className="AccountingConfigurationData";
    final ArrayList<String> files = getXmlFilesWithPath(BASE_DIRECTORY);

    for (final String fileName : files) {
      // final String fileName = "c:\\tmp\\" + className + ".xml";
      // System.out.println(file);
      final DataClassGeneratorV2 dc = new DataClassGeneratorV2(fileName);
      final String file = dc.generate();
      final String outputFile = changeExtension(fileName, "java");
      generateOutput(file, outputFile);
    }
  }

  private static ArrayList<String> getXmlFiles(String dirName) {
    final ArrayList<String> result = new ArrayList<String>();
    final File dir = new File(dirName);
    final String[] children = dir.list();
    if (children == null) {
      // Either dir does not exist or is not a directory
    } else {
      for (int i = 0; i < children.length; i++) {
        // Get filename of file or directory
        final String filename = children[i];
        if (children[i].endsWith(".xml")) {
          result.add(children[i]);
        }
      }
    }
    return result;
  }

  private static ArrayList<String> getXmlFilesWithPath(String dirName) {
    final ArrayList<String> result = new ArrayList<String>();
    final File dir = new File(dirName);
    if (dir.isDirectory()) {
      final String[] children = dir.list();
      if (children == null) {
        // Either dir does not exist or is not a directory
      } else {
        for (int i = 0; i < children.length; i++) {
          // Get filename of file or directory
          final String fileName = children[i];
          final File subdir = new File(dirName + "\\" + fileName);
          if (subdir.isDirectory()) {
            result.addAll(getXmlFilesWithPath(dirName + "\\" + fileName));
          } else if (children[i].endsWith(".xml")) {
            result.add(dirName + "\\" + children[i]);
          }
        }
      }
    }
    return result;
  }

  private static String changeExtension(String file, String ext) {
    String result;
    if (file.contains(".")) {
      result = file.substring(0, file.lastIndexOf(".") + 1) + ext;
    } else {
      result = file + "." + ext;
    }
    return result;
  }

  private static void generateOutput(String file, String outputFile) {
    BufferedOutputStream bo = null;
    try {
      bo = new BufferedOutputStream(new FileOutputStream(outputFile));
      bo.write(file.getBytes());
    } catch (final FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (final IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } finally {
      // Close the BufferedOutputStream
      try {
        if (bo != null) {
          bo.flush();
          bo.close();
        }
      } catch (final IOException ex) {
        ex.printStackTrace();
      }
    }

  }
}
