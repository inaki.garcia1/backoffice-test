/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 * Nono Carballo <f.carballo@nectus.com>.
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.applicationdictionary.windowstabsandfields;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for FieldData
 *
 * @author plujan
 *
 */
public class FieldData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the eMObuiselOutfieldID value
     *
     * Description: Identifies the 'out field' from which get a value on a row selection
     *
     * @param value
     *          The eMObuiselOutfieldID value.
     * @return The builder for this class.
     */
    public Builder eMObuiselOutfieldID(String value) {
      this.dataFields.put("eMObuiselOutfieldID", value);
      return this;
    }

    /**
     * Set the startnewline value
     *
     * Description: Startnewline
     *
     * @param value
     *          The startnewline value.
     * @return The builder for this class.
     */
    public Builder startnewline(Boolean value) {
      this.dataFields.put("startnewline", value);
      return this;
    }

    /**
     * Set the gridPosition value
     *
     * Description: Grid position
     *
     * @param value
     *          The gridPosition value.
     * @return The builder for this class.
     */
    public Builder gridPosition(String value) {
      this.dataFields.put("gridPosition", value);
      return this;
    }

    /**
     * Set the module value
     *
     * Description: Module
     *
     * @param value
     *          The module value.
     * @return The builder for this class.
     */
    public Builder module(String value) {
      this.dataFields.put("module", value);
      return this;
    }

    /**
     * Set the startinoddcolumn value
     *
     * Description: Startinoddcolumn
     *
     * @param value
     *          The startinoddcolumn value.
     * @return The builder for this class.
     */
    public Builder startinoddcolumn(Boolean value) {
      this.dataFields.put("startinoddcolumn", value);
      return this;
    }

    /**
     * Set the isFirstFocusedField value
     *
     * Description: Is First Focused Field
     *
     * @param value
     *          The isFirstFocusedField value.
     * @return The builder for this class.
     */
    public Builder isFirstFocusedField(Boolean value) {
      this.dataFields.put("isFirstFocusedField", value);
      return this;
    }

    /**
     * Set the showInGridView value
     *
     * Description: Show in Grid View
     *
     * @param value
     *          The showInGridView value.
     * @return The builder for this class.
     */
    public Builder showInGridView(Boolean value) {
      this.dataFields.put("showInGridView", value);
      return this;
    }

    /**
     * Set the fieldGroup value
     *
     * Description: A classification of similar fields.
     *
     * @param value
     *          The fieldGroup value.
     * @return The builder for this class.
     */
    public Builder fieldGroup(String value) {
      this.dataFields.put("fieldGroup", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the centralMaintenance value
     *
     * Description: A flag indicating that this label is managed in a central repository.
     *
     * @param value
     *          The centralMaintenance value.
     * @return The builder for this class.
     */
    public Builder centralMaintenance(Boolean value) {
      this.dataFields.put("centralMaintenance", value);
      return this;
    }

    /**
     * Set the readOnly value
     *
     * Description: An object which may only be viewed not edited.
     *
     * @param value
     *          The readOnly value.
     * @return The builder for this class.
     */
    public Builder readOnly(Boolean value) {
      this.dataFields.put("readOnly", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the displayEncription value
     *
     * Description: An indication noting if the input box of a field will present full text or just
     * asterisks.
     *
     * @param value
     *          The displayEncription value.
     * @return The builder for this class.
     */
    public Builder displayEncription(Boolean value) {
      this.dataFields.put("displayEncription", value);
      return this;
    }

    /**
     * Set the displayOnSameLine value
     *
     * Description: An indication that the field will display on the same line as the previous one.
     *
     * @param value
     *          The displayOnSameLine value.
     * @return The builder for this class.
     */
    public Builder displayOnSameLine(Boolean value) {
      this.dataFields.put("displayOnSameLine", value);
      return this;
    }

    /**
     * Set the recordSortNo value
     *
     * Description: A means of sorting and ordering records in a window.
     *
     * @param value
     *          The recordSortNo value.
     * @return The builder for this class.
     */
    public Builder recordSortNo(String value) {
      this.dataFields.put("recordSortNo", value);
      return this;
    }

    /**
     * Set the sequenceNumber value
     *
     * Description: The order of records in a specified document.
     *
     * @param value
     *          The sequenceNumber value.
     * @return The builder for this class.
     */
    public Builder sequenceNumber(String value) {
      this.dataFields.put("sequenceNumber", value);
      return this;
    }

    /**
     * Set the displayedLength value
     *
     * Description: The number of characters that made be added to a specified field.
     *
     * @param value
     *          The displayedLength value.
     * @return The builder for this class.
     */
    public Builder displayedLength(String value) {
      this.dataFields.put("displayedLength", value);
      return this;
    }

    /**
     * Set the displayLogic value
     *
     * Description: A specification of statements which when evaluated as false cause the field to
     * appear hidden.
     *
     * @param value
     *          The displayLogic value.
     * @return The builder for this class.
     */
    public Builder displayLogic(String value) {
      this.dataFields.put("displayLogic", value);
      return this;
    }

    /**
     * Set the displayed value
     *
     * Description: Determines if this field is displayed
     *
     * @param value
     *          The displayed value.
     * @return The builder for this class.
     */
    public Builder displayed(Boolean value) {
      this.dataFields.put("displayed", value);
      return this;
    }

    /**
     * Set the column value
     *
     * Description: A link to the database column of the table.
     *
     * @param value
     *          The column value.
     * @return The builder for this class.
     */
    public Builder column(String value) {
      this.dataFields.put("column", value);
      return this;
    }

    /**
     * Set the helpComment value
     *
     * Description: A comment that adds additional information to help users work with fields.
     *
     * @param value
     *          The helpComment value.
     * @return The builder for this class.
     */
    public Builder helpComment(String value) {
      this.dataFields.put("helpComment", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the tab value
     *
     * Description: An indication that a tab is displayed within a window.
     *
     * @param value
     *          The tab value.
     * @return The builder for this class.
     */
    public Builder tab(String value) {
      this.dataFields.put("tab", value);
      return this;
    }

    /**
     * Set the shownInStatusBar value
     *
     * Description: An indication that a field is shown shown in the Status Bar.
     *
     * @param value
     *          The shownInStatusBar value.
     * @return The builder for this class.
     */
    public Builder shownInStatusBar(Boolean value) {
      this.dataFields.put("shownInStatusBar", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public FieldData build() {
      return new FieldData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private FieldData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
