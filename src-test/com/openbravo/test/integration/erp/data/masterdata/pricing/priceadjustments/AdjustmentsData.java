/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.pricing.priceadjustments;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for AdjustmentsData
 *
 * @author plujan
 *
 */
public class AdjustmentsData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the includePriceLists value
     *
     * Description: Pricelist Selection Mode
     *
     * @param value
     *          The includePriceLists value.
     * @return The builder for this class.
     */
    public Builder includePriceLists(String value) {
      this.dataFields.put("includePriceLists", value);
      return this;
    }

    /**
     * Set the maxQuantity value
     *
     * Description: Quantity to
     *
     * @param value
     *          The maxQuantity value.
     * @return The builder for this class.
     */
    public Builder maxQuantity(String value) {
      this.dataFields.put("maxQuantity", value);
      return this;
    }

    /**
     * Set the minQuantity value
     *
     * Description: Quantity From
     *
     * @param value
     *          The minQuantity value.
     * @return The builder for this class.
     */
    public Builder minQuantity(String value) {
      this.dataFields.put("minQuantity", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the includedProductCategories value
     *
     * Description: Product Category Selection Mode
     *
     * @param value
     *          The includedProductCategories value.
     * @return The builder for this class.
     */
    public Builder includedProductCategories(String value) {
      this.dataFields.put("includedProductCategories", value);
      return this;
    }

    /**
     * Set the includedProducts value
     *
     * Description: Product Selection Mode
     *
     * @param value
     *          The includedProducts value.
     * @return The builder for this class.
     */
    public Builder includedProducts(String value) {
      this.dataFields.put("includedProducts", value);
      return this;
    }

    /**
     * Set the includedBPCategories value
     *
     * Description: BP Group Selection Mode
     *
     * @param value
     *          The includedBPCategories value.
     * @return The builder for this class.
     */
    public Builder includedBPCategories(String value) {
      this.dataFields.put("includedBPCategories", value);
      return this;
    }

    /**
     * Set the includedBusinessPartners value
     *
     * Description: Business Partner Selection Mode
     *
     * @param value
     *          The includedBusinessPartners value.
     * @return The builder for this class.
     */
    public Builder includedBusinessPartners(String value) {
      this.dataFields.put("includedBusinessPartners", value);
      return this;
    }

    /**
     * Set the endingDate value
     *
     * Description: A parameter stating the ending time range related for a specified request query
     * etc.
     *
     * @param value
     *          The endingDate value.
     * @return The builder for this class.
     */
    public Builder endingDate(String value) {
      this.dataFields.put("endingDate", value);
      return this;
    }

    /**
     * Set the startingDate value
     *
     * Description: A parameter stating the starting time range related to a specified request.
     *
     * @param value
     *          The startingDate value.
     * @return The builder for this class.
     */
    public Builder startingDate(String value) {
      this.dataFields.put("startingDate", value);
      return this;
    }

    /**
     * Set the fixedPrice value
     *
     * Description: A price that does not change when price adjustments are made.
     *
     * @param value
     *          The fixedPrice value.
     * @return The builder for this class.
     */
    public Builder fixedPrice(String value) {
      this.dataFields.put("fixedPrice", value);
      return this;
    }

    /**
     * Set the discount value
     *
     * Description: The proportional discount given to an item without respect to any previously
     * defined discounts.
     *
     * @param value
     *          The discount value.
     * @return The builder for this class.
     */
    public Builder discount(String value) {
      this.dataFields.put("discount", value);
      return this;
    }

    /**
     * Set the discountAmount value
     *
     * Description: Fixed discount amount
     *
     * @param value
     *          The discountAmount value.
     * @return The builder for this class.
     */
    public Builder discountAmount(String value) {
      this.dataFields.put("discountAmount", value);
      return this;
    }

    /**
     * Set the priority value
     *
     * Description: A defined level of importance or precedence.
     *
     * @param value
     *          The priority value.
     * @return The builder for this class.
     */
    public Builder priority(String value) {
      this.dataFields.put("priority", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public AdjustmentsData build() {
      return new AdjustmentsData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private AdjustmentsData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
