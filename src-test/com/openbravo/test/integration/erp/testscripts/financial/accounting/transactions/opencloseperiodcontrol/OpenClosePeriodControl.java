/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.financial.accounting.transactions.opencloseperiodcontrol;

import com.openbravo.test.integration.erp.data.financial.accounting.transactions.opencloseperiodcontrol.OpenClosePeriodControlData;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;

/**
 * Executes and verifies actions on the Open/Close Period Control classic window of OpenbravoERP.
 *
 * @author elopio
 */
public class OpenClosePeriodControl {

  /**
   * Executes and verifies actions on the Open/Close Period Control tab of OpenbravoERP.
   *
   * @author elopio
   *
   */
  public static class OpenClosePeriodControlTab {

    /**
     * Create a calendar record.
     *
     * @param mainPage
     *          The application main page.
     * @param openClosePeriodControlData
     *          The data of the period control that will be created.
     */
    public static void create(MainPage mainPage,
        OpenClosePeriodControlData openClosePeriodControlData) {
      if (mainPage.isOnNewLayout()) {
        OpenClosePeriodControlTabScript.create(mainPage, openClosePeriodControlData);
      } else {
        OpenClosePeriodControlTabClassicScript.create(mainPage, openClosePeriodControlData);
      }
    }

    /**
     * Open or close all period documents.
     *
     * @param mainPage
     *          The application main page.
     * @throws OpenbravoERPTestException
     */
    public static void openCloseAll(MainPage mainPage) throws OpenbravoERPTestException {
      if (mainPage.isOnNewLayout()) {
        OpenClosePeriodControlTabScript.openCloseAll(mainPage);
      } else {
        OpenClosePeriodControlTabClassicScript.openCloseAll(mainPage);
      }
    }

    /**
     * Open or close all periods.
     *
     * @param mainPage
     *          The application main page.
     * @throws OpenbravoERPTestException
     */
    public static void openAllPeriods(MainPage mainPage) throws OpenbravoERPTestException {
      if (mainPage.isOnNewLayout()) {
        OpenClosePeriodControlTabScript.openAllPeriods(mainPage);
      } else {
        OpenClosePeriodControlTabClassicScript.openCloseAll(mainPage);
      }
    }
  }
}
