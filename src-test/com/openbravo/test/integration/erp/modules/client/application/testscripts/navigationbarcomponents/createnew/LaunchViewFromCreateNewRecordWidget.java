/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.navigationbarcomponents.createnew;

import static org.junit.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.StandardWindow;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.CreateNew;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Test the launch of a view from the create new widget added by Navigation Bar Components module.
 *
 * @author elopio
 *
 */
public class LaunchViewFromCreateNewRecordWidget extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The standard window that will be opened on create new mode. */
  private final StandardWindow standardWindow;

  /**
   * Class constructor.
   *
   * @param standardWindow
   *          The standard window that will be opened on create new mode.
   */
  public LaunchViewFromCreateNewRecordWidget(StandardWindow standardWindow) {
    this.standardWindow = standardWindow;
  }

  /**
   * Test create new view entering its name in the text field. [NAV0030a] Create new record in form
   * view. Typing the name
   */
  @Ignore("Issue with seleniumserver in linux. Works in Windows")
  @Test
  public void viewShouldOpenEnteringNameInTextField() {
    logger.info("[NAV0030a] Create new record in form view. Typing the name");
    final CreateNew componentCreateNew = mainPage.getNavigationBar().getComponentCreateNew();
    componentCreateNew.setMode(CreateNew.Mode.TEXT_FIELD);
    mainPage.openView(standardWindow, componentCreateNew);
    assertFormViewAndNewMode();
  }

  /**
   * Test create new view selecting its name from the combo box. [NAV0030b] Create new record in
   * form view. Selecting from combo box
   */
  @Test
  public void viewShouldOpenSelectingNameFromComboBox() {
    logger
        .info(String.format("[NAV0030b] Create new record in form view. Selecting from combo box"));
    CreateNew componentCreateNew = mainPage.getNavigationBar().getComponentCreateNew();
    componentCreateNew.setMode(CreateNew.Mode.COMBO_BOX);
    mainPage.openView(standardWindow, componentCreateNew);
    assertFormViewAndNewMode();
    logger.info(
        String.format("[NAV0030c] Create new record in form view. Selecting from recen items"));
    componentCreateNew = mainPage.getNavigationBar().getComponentCreateNew();
    logger.info("Create new record selecting from combo box to have a recent item");
    componentCreateNew.setMode(CreateNew.Mode.COMBO_BOX);
    mainPage.openView(standardWindow, componentCreateNew);

    mainPage.closeAllViews();

    logger.info("Create new record in form view. Selecting from recent items");
    componentCreateNew.setMode(CreateNew.Mode.RECENT_ITEMS);
    mainPage.openView(standardWindow, componentCreateNew);
    assertFormViewAndNewMode();
  }

  /**
   * Assert that the top tab of the window opened is in form view and in new mode.
   */
  private void assertFormViewAndNewMode() {
    GeneratedTab<? extends DataObject> tab = standardWindow.getTopLevelTab();
    assertTrue("Top level tab should be in form view.", tab.isOnFormView());
    tab.assertNewMode();
  }
}
