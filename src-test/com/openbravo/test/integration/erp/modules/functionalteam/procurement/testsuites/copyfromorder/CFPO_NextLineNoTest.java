/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.procurement.testsuites.copyfromorder;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;

/**
 * Testing Line Number generation in Copy From Orders
 *
 * @author Mark
 *
 */

@RunWith(Parameterized.class)
public class CFPO_NextLineNoTest extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  // Purchase Order Copied from
  PurchaseOrderHeaderData purchaseOrderHeaderFromData;
  PurchaseOrderHeaderData purchaseOrderHeaderFromVerficationData;
  PurchaseOrderLinesData purchaseOrderLinesFromData;
  PurchaseOrderLinesData purchaseOrderLinesFromVerificationData;
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderFromVerificationData;

  // Purchase Order Copied To
  PurchaseOrderHeaderData purchaseOrderHeaderData;
  PurchaseOrderHeaderData purchaseOrderHeaderVerficationData;
  PurchaseOrderLinesData purchaseOrderFirstLineData;
  PurchaseOrderLinesData purchaseOrderFirstLineVerificationData;
  PurchaseOrderLinesData purchaseOrderCopiedLineVerificationData;
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData;

  /**
   * Class constructor.
   *
   */
  public CFPO_NextLineNoTest(PurchaseOrderHeaderData purchaseOrderHeaderFromData,
      PurchaseOrderHeaderData purchaseOrderHeaderFromVerficationData,
      PurchaseOrderLinesData purchaseOrderLinesFromData,
      PurchaseOrderLinesData purchaseOrderLinesFromVerificationData,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderFromVerificationData,
      PurchaseOrderHeaderData purchaseOrderHeaderData,
      PurchaseOrderHeaderData purchaseOrderHeaderVerficationData,
      PurchaseOrderLinesData purchaseOrderFirstLineData,
      PurchaseOrderLinesData purchaseOrderFirstLineVerificationData,
      PurchaseOrderLinesData purchaseOrderCopiedLineVerificationData,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData) {
    this.purchaseOrderHeaderFromData = purchaseOrderHeaderFromData;
    this.purchaseOrderHeaderFromVerficationData = purchaseOrderHeaderFromVerficationData;
    this.purchaseOrderLinesFromData = purchaseOrderLinesFromData;
    this.purchaseOrderLinesFromVerificationData = purchaseOrderLinesFromVerificationData;
    this.bookedPurchaseOrderHeaderFromVerificationData = bookedPurchaseOrderHeaderFromVerificationData;

    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    this.purchaseOrderHeaderVerficationData = purchaseOrderHeaderVerficationData;
    this.purchaseOrderFirstLineData = purchaseOrderFirstLineData;
    this.purchaseOrderFirstLineVerificationData = purchaseOrderFirstLineVerificationData;
    this.purchaseOrderCopiedLineVerificationData = purchaseOrderCopiedLineVerificationData;
    this.bookedPurchaseOrderHeaderVerificationData = bookedPurchaseOrderHeaderVerificationData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> PurchaseOrderValues() {
    Object[][] data = new Object[][] { {

        new PurchaseOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Purchase Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("1 (USA)")
            .paymentTerms("90 days")
            .build(),

        new PurchaseOrderHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("USA warehouse")
            .paymentTerms("90 days")
            .priceList("Purchase")
            .paymentMethod("1 (USA)")
            .build(),

        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMA").priceListVersion("Purchase").build())
            .orderedQuantity("10")
            .tax("Exempt 10%")
            .build(),

        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("20.00")
            .build(),

        new PurchaseOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("20.00")
            .grandTotalAmount("20.00")
            .currency("EUR")
            .build(),

        new PurchaseOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Purchase Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("1 (USA)")
            .paymentTerms("90 days")
            .build(),

        new PurchaseOrderHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("USA warehouse")
            .paymentTerms("90 days")
            .priceList("Purchase")
            .paymentMethod("1 (USA)")
            .build(),

        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMB").priceListVersion("Purchase").build())
            .orderedQuantity("10")
            .tax("Exempt 10%")
            .build(),

        new PurchaseOrderLinesData.Builder().lineNo("10")
            .orderedQuantity("10")
            .tax("Exempt 10%")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("20.00")
            .build(),

        new PurchaseOrderLinesData.Builder().lineNo("20")
            .orderedQuantity("10")
            .tax("Exempt 10%")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("20.00")
            .build(),

        new PurchaseOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("40.00")
            .grandTotalAmount("40.00")
            .currency("EUR")
            .build() } };
    return Arrays.asList(data);
  }

  @Test
  public void CFPO_NextLineNo() {
    logger.info(
        "** Start of test case [CFPO_NextLineNo]. Checks the line no generation is correct **");

    /** Register the Sales order will be copied */
    PurchaseOrder orderFrom = new PurchaseOrder(mainPage).open();
    orderFrom.create(purchaseOrderHeaderFromData);
    orderFrom.assertSaved();
    orderFrom.assertData(purchaseOrderHeaderFromVerficationData);
    String orderNoFrom = orderFrom.getData("documentNo").toString();

    // Create order lines
    PurchaseOrder.Lines orderLinesFrom = orderFrom.new Lines(mainPage);
    orderLinesFrom.create(purchaseOrderLinesFromData);
    orderLinesFrom.assertSaved();
    orderLinesFrom.assertData(purchaseOrderLinesFromVerificationData);

    // Book the order
    orderFrom.book();
    orderFrom.assertProcessCompletedSuccessfully2();
    orderFrom.assertData(bookedPurchaseOrderHeaderFromVerificationData);

    /** Register a Sales order and copy from the previously order */
    PurchaseOrder order = new PurchaseOrder(mainPage).open();
    order.create(purchaseOrderHeaderData);
    order.assertSaved();
    order.assertData(purchaseOrderHeaderVerficationData);

    // Create a line
    PurchaseOrder.Lines orderLines = order.new Lines(mainPage);
    orderLines.create(purchaseOrderFirstLineData);
    orderLines.assertSaved();
    orderLines.assertData(purchaseOrderFirstLineVerificationData);

    PickAndExecuteWindow<PurchaseOrderHeaderData> popup = order.copyFromOrders();
    popup.filter(new PurchaseOrderHeaderData.Builder().documentNo(orderNoFrom).build());
    List<Map<String, Object>> rows = popup.getSelectedRows();
    assertTrue(rows.size() == 1);
    popup.process();

    orderLines.assertCount(2);
    orderLines.selectWithoutFiltering(1);
    orderLines.assertData(purchaseOrderCopiedLineVerificationData);

    // Book the order
    order.book();
    order.assertProcessCompletedSuccessfully2();
    order.assertData(bookedPurchaseOrderHeaderVerificationData);

    logger.info(
        "** Start of test case [CFPO_NextLineNo]. Checks the line no generation is correct **");
  }
}
