/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016-2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions4;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddTransactionData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.MatchStatementData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.bankstatement.BankStatementHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.BankStatementLinesData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddTransactionProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementGrid;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 32268
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression32268Out extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineData;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;
  PaymentPlanData purchaseInvoicePaymentPlanData;
  AccountData accountHeaderData;
  BankStatementHeaderData bankStatementHeaderData;
  BankStatementHeaderData bankStatementHeaderVerificationData;
  BankStatementLinesData bankStatementLinesData;
  BankStatementLinesData bankStatementLinesVerificationData;
  MatchStatementData matchStatementData;
  AddTransactionData addTransactionVerificationData;
  AddPaymentPopUpData addPaymentTotalsVerificationData;
  MatchStatementData matchStatementData2;
  TransactionsData transactionVerificationData;
  PaymentOutHeaderData paymentOutHeaderVerificationData;
  PaymentOutLinesData paymentOutLinesVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData2;
  PaymentPlanData purchaseInvoicePaymentPlanData2;
  PaymentDetailsData purchaseInvoicePaymentDetailsData;
  MatchStatementData matchStatementData3;
  TransactionsData transactionVerificationData2;
  PaymentOutHeaderData paymentOutHeaderVerificationData2;
  PaymentDetailsData purchaseInvoicePaymentDetailsData2;

  /**
   * Class constructor.
   *
   */
  public APRRegression32268Out(PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineData,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      PaymentPlanData purchaseInvoicePaymentPlanData, AccountData accountHeaderData,
      BankStatementHeaderData bankStatementHeaderData,
      BankStatementHeaderData bankStatementHeaderVerificationData,
      BankStatementLinesData bankStatementLinesData,
      BankStatementLinesData bankStatementLinesVerificationData,
      MatchStatementData matchStatementData, AddTransactionData addTransactionVerificationData,
      AddPaymentPopUpData addPaymentTotalsVerificationData, MatchStatementData matchStatementData2,
      TransactionsData transactionVerificationData,
      PaymentOutHeaderData paymentOutHeaderVerificationData,
      PaymentOutLinesData paymentOutLinesVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData2,
      PaymentPlanData purchaseInvoicePaymentPlanData2,
      PaymentDetailsData purchaseInvoicePaymentDetailsData, MatchStatementData matchStatementData3,
      TransactionsData transactionVerificationData2,
      PaymentOutHeaderData paymentOutHeaderVerificationData2,
      PaymentDetailsData purchaseInvoicePaymentDetailsData2) {
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineData = purchaseInvoiceLineData;
    this.purchaseInvoiceLineVerificationData = purchaseInvoiceLineVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.purchaseInvoicePaymentPlanData = purchaseInvoicePaymentPlanData;
    this.accountHeaderData = accountHeaderData;
    this.bankStatementHeaderData = bankStatementHeaderData;
    this.bankStatementHeaderVerificationData = bankStatementHeaderVerificationData;
    this.bankStatementLinesData = bankStatementLinesData;
    this.bankStatementLinesVerificationData = bankStatementLinesVerificationData;
    this.matchStatementData = matchStatementData;
    this.addTransactionVerificationData = addTransactionVerificationData;
    this.addPaymentTotalsVerificationData = addPaymentTotalsVerificationData;
    this.matchStatementData2 = matchStatementData2;
    this.transactionVerificationData = transactionVerificationData;
    this.paymentOutHeaderVerificationData = paymentOutHeaderVerificationData;
    this.paymentOutLinesVerificationData = paymentOutLinesVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData2 = completedPurchaseInvoiceHeaderVerificationData2;
    this.purchaseInvoicePaymentPlanData2 = purchaseInvoicePaymentPlanData2;
    this.purchaseInvoicePaymentDetailsData = purchaseInvoicePaymentDetailsData;
    this.matchStatementData3 = matchStatementData3;
    this.transactionVerificationData2 = transactionVerificationData2;
    this.paymentOutHeaderVerificationData2 = paymentOutHeaderVerificationData2;
    this.purchaseInvoicePaymentDetailsData2 = purchaseInvoicePaymentDetailsData2;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> purchaseOrderValues() {
    Object[][] data = new Object[][] { {
        new PurchaseInvoiceHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),
        new PurchaseInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AP Invoice")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .paymentTerms("90 days")
            .paymentMethod("1 (Spain)")
            .priceList("Purchase")
            .build(),
        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("RMA")
                .priceListVersionName("Purchase")
                .build())
            .invoicedQuantity("50")
            .build(),
        new PurchaseInvoiceLinesData.Builder().lineNo("10")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("100.00")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("110.00")
            .summedLineAmount("100.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),
        new PaymentPlanData.Builder().dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .expectedDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("1 (Spain)")
            .expected("110.00")
            .paid("0.00")
            .outstanding("110.00")
            .currency("EUR")
            .lastPaymentDate("")
            .daysOverdue("0")
            .numberOfPayments("0")
            .build(),

        new AccountData.Builder().name("Spain Bank").build(),
        new BankStatementHeaderData.Builder().name("APRRegression32268Out").build(),
        new BankStatementHeaderData.Builder().documentType("Bank Statement File")
            .active(true)
            .fileName("")
            .notes("")
            .build(),
        new BankStatementLinesData.Builder().referenceNo("32268Out")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .dramount("110.00")
            .build(),
        new BankStatementLinesData.Builder().active(true)
            .lineNo("10")
            .bpartnername("")
            .gLItem("")
            .dramount("110.00")
            .cramount("0.00")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .financialAccountTransaction("&nbsp;")
            .matchingtype("")
            .description("")
            .build(),

        new MatchStatementData.Builder().bankStatementType("P")
            .sender("Vendor A")
            .bpartnername("")
            .referenceNo("32268Out")
            .glitem("")
            .description("")
            .amount("-110")
            .affinity("")
            .matchedDocument("")
            .matchingType("")
            .businessPartner("")
            .transactionAmount("")
            .transactionGLItem("")
            .trxDescription("")
            .build(),
        new AddTransactionData.Builder().trxtype("BP Withdrawal")
            .gLItem("")
            .description("")
            .currency("EUR")
            .depositamt("0.00")
            .withdrawalamt("110.00")
            .organization("Spain")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .project("")
            .product("")
            .build(),
        new AddPaymentPopUpData.Builder().trxtype("Paid Out")
            .reference_no("")
            .received_from("Vendor A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Bank - EUR")
            .bslamount("-110.00")
            .c_currency_id("EUR")
            .actual_payment("110.00")
            .expected_payment("110.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("110.00")
            .total("110.00")
            .difference("0.00")
            .build(),
        new MatchStatementData.Builder().bankStatementType("P")
            .sender("Vendor A")
            .bpartnername("")
            .referenceNo("32268Out")
            .glitem("")
            .description("")
            .amount("-110")
            .affinity("AD")
            .matchedDocument("T")
            .matchingType("AD")
            .businessPartner("Vendor A")
            .transactionAmount("-110")
            .transactionGLItem("")
            .build(),

        new TransactionsData.Builder().transactionType("BP Withdrawal")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .gLItem("")
            .description("")
            .depositAmount("0.00")
            .paymentAmount("110.00")
            .currency("EUR")
            .status("Withdrawn not Cleared")
            .organization("Spain")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .product(new ProductSelectorData.Builder().name("").build())
            .project("")
            .build(),

        new PaymentOutHeaderData.Builder().status("Withdrawn not Cleared")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .organization("Spain")
            .documentType("AP Payment")
            .referenceNo("")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("1 (Spain)")
            .amount("110.00")
            .account("Spain Bank - EUR")
            .currency("EUR")
            .build(),
        new PaymentOutLinesData.Builder().dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .invoiceAmount("110.00")
            .expected("110.00")
            .paid("110.00")
            .glitemname("")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("110.00")
            .summedLineAmount("100.00")
            .currency("EUR")
            .paymentComplete(true)
            .build(),
        new PaymentPlanData.Builder().dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .expectedDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("1 (Spain)")
            .expected("110.00")
            .paid("110.00")
            .outstanding("0.00")
            .currency("EUR")
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),
        new PaymentDetailsData.Builder().paid("110.00")
            .writeoff("0.00")
            .paymentMethod("1 (Spain)")
            .amount("110.00")
            .account("Spain Bank - EUR")
            .status("Withdrawn not Cleared")
            .build(),

        new MatchStatementData.Builder().bankStatementType("P")
            .sender("Vendor A")
            .bpartnername("")
            .referenceNo("32268Out")
            .glitem("")
            .description("")
            .amount("-110")
            .affinity("AP")
            .matchedDocument("T")
            .matchingType("AP")
            .businessPartner("Vendor A")
            .transactionAmount("-110")
            .transactionGLItem("")
            .build(),

        new TransactionsData.Builder().transactionType("BP Withdrawal")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .gLItem("")
            .description("")
            .depositAmount("0.00")
            .paymentAmount("110.00")
            .currency("EUR")
            .status("Payment Cleared")
            .organization("Spain")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .product(new ProductSelectorData.Builder().name("").build())
            .project("")
            .build(),

        new PaymentOutHeaderData.Builder().status("Payment Cleared")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .organization("Spain")
            .documentType("AP Payment")
            .referenceNo("")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("1 (Spain)")
            .amount("110.00")
            .account("Spain Bank - EUR")
            .currency("EUR")
            .build(),

        new PaymentDetailsData.Builder().paid("110.00")
            .writeoff("0.00")
            .paymentMethod("1 (Spain)")
            .amount("110.00")
            .account("Spain Bank - EUR")
            .status("Payment Cleared")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 32268 - Payment Out flow
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression32268OutTest() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression32268Out] Test regression 32268 - Payment Out flow. **");

    PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    String invoiceNo = purchaseInvoice.getData("documentNo").toString();
    PurchaseInvoice.Lines purchaseInvoiceLine = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLine.create(purchaseInvoiceLineData);
    purchaseInvoiceLine.assertSaved();
    purchaseInvoiceLine.assertData(purchaseInvoiceLineVerificationData);
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData);
    PurchaseInvoice.PaymentOutPlan purchaseInvoicePaymentPlan = purchaseInvoice.new PaymentOutPlan(
        mainPage);
    purchaseInvoicePaymentPlan.assertCount(1);
    purchaseInvoicePaymentPlan.assertData(purchaseInvoicePaymentPlanData);
    PurchaseInvoice.PaymentOutPlan.PaymentOutDetails purchaseInvoicePaymentDetails = purchaseInvoicePaymentPlan.new PaymentOutDetails(
        mainPage);
    purchaseInvoicePaymentDetails.assertCount(0);

    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.BankStatements bankStatement = financialAccount.new BankStatements(mainPage);
    String date;
    if (bankStatement.getRecordCount() == 0) {
      date = OBDate.CURRENT_DATE;
    } else {
      bankStatement.selectWithoutFiltering(0);
      date = OBDate.addDaysToDate((String) bankStatement.getData("transactionDate"), 1);
    }
    bankStatementHeaderData.addDataField("transactionDate", date);
    bankStatementHeaderData.addDataField("importdate", date);
    bankStatement.create(bankStatementHeaderData);
    bankStatement.assertSaved();
    bankStatement.assertData(bankStatementHeaderVerificationData);
    FinancialAccount.BankStatements.BankStatementLines bankStatementLines = bankStatement.new BankStatementLines(
        mainPage);
    bankStatementLinesData.addDataField("transactionDate", date);
    bankStatementLines.create(bankStatementLinesData);
    bankStatementLines.assertSaved();
    bankStatementLines.assertData(bankStatementLinesVerificationData);
    bankStatement.process();
    bankStatement.assertProcessCompletedSuccessfully2();

    @SuppressWarnings("rawtypes")
    MatchStatementProcess matchStatementProcess = financialAccount.openMatchStatement(false);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementData.addDataField("banklineDate", date);
    matchStatementData.addDataField("trxDate", "");
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData);

    AddTransactionProcess addTransactionProcess = ((MatchStatementGrid) matchStatementProcess
        .getMatchStatementGrid()).clickAdd(0);
    addTransactionProcess.assertData(addTransactionVerificationData);
    AddPaymentProcess addPaymentProcess = addTransactionProcess.openAddPayment();
    String paymentNo = addPaymentProcess.getParameterValue("payment_documentno").toString();
    paymentNo = paymentNo.substring(1, paymentNo.length() - 1);
    addPaymentProcess.setParameterValue("fin_paymentmethod_id", "1 (Spain)");
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.getOrderInvoiceGrid().unselectAll();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.getOrderInvoiceGrid().filter("invoiceNo", invoiceNo);
    Sleep.trySleep();
    addPaymentProcess.editOrderInvoiceRecord(0,
        new SelectedPaymentData.Builder().amount("110.00").build());
    addPaymentProcess.getField("reference_no").focus();
    addPaymentProcess.process("Process Made Payment(s)", addPaymentTotalsVerificationData);
    String invoiceDescription = "Invoice No.: " + invoiceNo + "\n";
    addTransactionVerificationData.addDataField("trxdate", date);
    addTransactionVerificationData.addDataField("dateacct", date);
    addTransactionVerificationData.addDataField("description", invoiceDescription);
    addTransactionProcess.assertData(addTransactionVerificationData);
    addTransactionProcess.process();

    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementData2.addDataField("banklineDate", date);
    matchStatementData2.addDataField("trxDate", date);
    matchStatementData2.addDataField("fatDescription", invoiceDescription);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData2);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).clickClear(0);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData);
    matchStatementProcess.process();
    financialAccount.assertProcessCompletedSuccessfully2();

    FinancialAccount.Transaction transaction = financialAccount.new Transaction(mainPage);
    transaction.select(new TransactionsData.Builder().documentNo(paymentNo).build());
    transactionVerificationData.addDataField("transactionDate", date);
    transactionVerificationData.addDataField("dateAcct", date);
    transactionVerificationData.addDataField("description", invoiceDescription);
    transaction.assertData(transactionVerificationData);

    PaymentOut paymentOut = new PaymentOut(mainPage).open();
    paymentOut.select(new PaymentOutHeaderData.Builder().documentNo(paymentNo).build());
    paymentOutHeaderVerificationData.addDataField("paymentDate", date);
    paymentOutHeaderVerificationData.addDataField("documentNo", paymentNo);
    paymentOutHeaderVerificationData.addDataField("description", invoiceDescription);
    paymentOut.assertData(paymentOutHeaderVerificationData);
    PaymentOut.Lines paymentOutLines = paymentOut.new Lines(mainPage);
    paymentOutLines.assertCount(1);
    paymentOutLinesVerificationData.addDataField("invoicePaymentSchedule$invoice$documentNo",
        invoiceNo);
    paymentOutLines.assertData(paymentOutLinesVerificationData);

    purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoiceNo).build());
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData2);
    purchaseInvoicePaymentPlan = purchaseInvoice.new PaymentOutPlan(mainPage);
    purchaseInvoicePaymentPlan.assertCount(1);
    purchaseInvoicePaymentPlanData2.addDataField("lastPaymentDate", date);
    purchaseInvoicePaymentPlan.assertData(purchaseInvoicePaymentPlanData2);
    purchaseInvoicePaymentDetails = purchaseInvoicePaymentPlan.new PaymentOutDetails(mainPage);
    purchaseInvoicePaymentDetails.assertCount(1);
    purchaseInvoicePaymentDetailsData.addDataField("paymentDetails$finPayment$paymentDate", date);
    purchaseInvoicePaymentDetailsData.addDataField("paymentDetails$finPayment$documentNo",
        paymentNo);
    purchaseInvoicePaymentDetails.assertData(purchaseInvoicePaymentDetailsData);

    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    matchStatementProcess = financialAccount.openMatchStatement(true);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementData3.addDataField("banklineDate", date);
    matchStatementData3.addDataField("trxDate", date);
    matchStatementData3.addDataField("fatDescription", invoiceDescription);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData3);
    matchStatementProcess.process();
    financialAccount.assertProcessCompletedSuccessfully2();

    transaction = financialAccount.new Transaction(mainPage);
    transaction.select(new TransactionsData.Builder().documentNo(paymentNo).build());
    transactionVerificationData2.addDataField("transactionDate", date);
    transactionVerificationData2.addDataField("dateAcct", date);
    transactionVerificationData2.addDataField("description", invoiceDescription);
    transaction.assertData(transactionVerificationData2);

    paymentOut = new PaymentOut(mainPage).open();
    paymentOut.select(new PaymentOutHeaderData.Builder().documentNo(paymentNo).build());
    paymentOutHeaderVerificationData2.addDataField("paymentDate", date);
    paymentOutHeaderVerificationData2.addDataField("documentNo", paymentNo);
    paymentOutHeaderVerificationData2.addDataField("description", invoiceDescription);
    paymentOut.assertData(paymentOutHeaderVerificationData2);
    paymentOutLines = paymentOut.new Lines(mainPage);
    paymentOutLines.assertCount(1);
    paymentOutLines.assertData(paymentOutLinesVerificationData);

    purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoiceNo).build());
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData2);
    purchaseInvoicePaymentPlan = purchaseInvoice.new PaymentOutPlan(mainPage);
    purchaseInvoicePaymentPlan.assertCount(1);
    purchaseInvoicePaymentPlan.assertData(purchaseInvoicePaymentPlanData2);
    purchaseInvoicePaymentDetails = purchaseInvoicePaymentPlan.new PaymentOutDetails(mainPage);
    purchaseInvoicePaymentDetails.assertCount(1);
    purchaseInvoicePaymentDetailsData2.addDataField("paymentDetails$finPayment$paymentDate", date);
    purchaseInvoicePaymentDetailsData2.addDataField("paymentDetails$finPayment$documentNo",
        paymentNo);
    purchaseInvoicePaymentDetails.assertData(purchaseInvoicePaymentDetailsData2);

    logger.info(
        "** End of test case [APRRegression32268Out] Test regression 32268 - Payment Out flow. **");
  }
}
