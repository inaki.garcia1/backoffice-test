/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.projectservice.setup.resource;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ResourceProductData
 *
 * @author plujan
 *
 */
public class ResourceProductData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the descriptionURL value
     *
     * Description: An address for the product description which can be accessed via internet.
     *
     * @param value
     *          The descriptionURL value.
     * @return The builder for this class.
     */
    public Builder descriptionURL(String value) {
      this.dataFields.put("descriptionURL", value);
      return this;
    }

    /**
     * Set the imageURL value
     *
     * Description: An address for the product image which can be accessed via internet.
     *
     * @param value
     *          The imageURL value.
     * @return The builder for this class.
     */
    public Builder imageURL(String value) {
      this.dataFields.put("imageURL", value);
      return this;
    }

    /**
     * Set the productType value
     *
     * Description: An important classification used to determine the accounting and management of a
     * product.
     *
     * @param value
     *          The productType value.
     * @return The builder for this class.
     */
    public Builder productType(String value) {
      this.dataFields.put("productType", value);
      return this;
    }

    /**
     * Set the salesRepresentative value
     *
     * Description: The person in charge of a document.
     *
     * @param value
     *          The salesRepresentative value.
     * @return The builder for this class.
     */
    public Builder salesRepresentative(String value) {
      this.dataFields.put("salesRepresentative", value);
      return this;
    }

    /**
     * Set the classification value
     *
     * Description: x not implemented
     *
     * @param value
     *          The classification value.
     * @return The builder for this class.
     */
    public Builder classification(String value) {
      this.dataFields.put("classification", value);
      return this;
    }

    /**
     * Set the helpComment value
     *
     * Description: A comment that adds additional information to help users work with fields.
     *
     * @param value
     *          The helpComment value.
     * @return The builder for this class.
     */
    public Builder helpComment(String value) {
      this.dataFields.put("helpComment", value);
      return this;
    }

    /**
     * Set the comments value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The comments value.
     * @return The builder for this class.
     */
    public Builder comments(String value) {
      this.dataFields.put("comments", value);
      return this;
    }

    /**
     * Set the discontinuedBy value
     *
     * Description: The name of the person who discontinues an item.
     *
     * @param value
     *          The discontinuedBy value.
     * @return The builder for this class.
     */
    public Builder discontinuedBy(String value) {
      this.dataFields.put("discontinuedBy", value);
      return this;
    }

    /**
     * Set the discontinued value
     *
     * Description: A statement mentioning that this product will no longer be available on the
     * market.
     *
     * @param value
     *          The discontinued value.
     * @return The builder for this class.
     */
    public Builder discontinued(Boolean value) {
      this.dataFields.put("discontinued", value);
      return this;
    }

    /**
     * Set the sKU value
     *
     * Description: A "stock keeping unit" used to track items sold to business partners.
     *
     * @param value
     *          The sKU value.
     * @return The builder for this class.
     */
    public Builder sKU(String value) {
      this.dataFields.put("sKU", value);
      return this;
    }

    /**
     * Set the uPCEAN value
     *
     * Description: A bar code with a number to identify a product.
     *
     * @param value
     *          The uPCEAN value.
     * @return The builder for this class.
     */
    public Builder uPCEAN(String value) {
      this.dataFields.put("uPCEAN", value);
      return this;
    }

    /**
     * Set the taxCategory value
     *
     * Description: A classification of tax options based on similar characteristics or attributes.
     *
     * @param value
     *          The taxCategory value.
     * @return The builder for this class.
     */
    public Builder taxCategory(String value) {
      this.dataFields.put("taxCategory", value);
      return this;
    }

    /**
     * Set the productCategory value
     *
     * Description: A classification of items based on similar characteristics or attributes.
     *
     * @param value
     *          The productCategory value.
     * @return The builder for this class.
     */
    public Builder productCategory(String value) {
      this.dataFields.put("productCategory", value);
      return this;
    }

    /**
     * Set the searchKey value
     *
     * Description: A fast method for finding a particular record.
     *
     * @param value
     *          The searchKey value.
     * @return The builder for this class.
     */
    public Builder searchKey(String value) {
      this.dataFields.put("searchKey", value);
      return this;
    }

    /**
     * Set the sale value
     *
     * Description: An indication that an item may be sold by a business partner.
     *
     * @param value
     *          The sale value.
     * @return The builder for this class.
     */
    public Builder sale(Boolean value) {
      this.dataFields.put("sale", value);
      return this;
    }

    /**
     * Set the purchase value
     *
     * Description: An indication that an item may be purchased by a business partner.
     *
     * @param value
     *          The purchase value.
     * @return The builder for this class.
     */
    public Builder purchase(Boolean value) {
      this.dataFields.put("purchase", value);
      return this;
    }

    /**
     * Set the uOM value
     *
     * Description: A non monetary unit of measure.
     *
     * @param value
     *          The uOM value.
     * @return The builder for this class.
     */
    public Builder uOM(String value) {
      this.dataFields.put("uOM", value);
      return this;
    }

    /**
     * Set the summaryLevel value
     *
     * Description: A means of grouping fields in order to view or hide additional information.
     *
     * @param value
     *          The summaryLevel value.
     * @return The builder for this class.
     */
    public Builder summaryLevel(Boolean value) {
      this.dataFields.put("summaryLevel", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ResourceProductData build() {
      return new ResourceProductData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ResourceProductData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
