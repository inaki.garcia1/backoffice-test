/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.setup.paymentmethod;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for PaymentMethodData
 *
 * @author plujan
 *
 */
public class PaymentMethodData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the automaticDeposit value
     *
     * Description: Automatic Deposit
     *
     * @param value
     *          The automaticDeposit value.
     * @return The builder for this class.
     */
    public Builder automaticDeposit(Boolean value) {
      this.dataFields.put("automaticDeposit", value);
      return this;
    }

    /**
     * Set the automaticWithdrawn value
     *
     * Description: Automatic Withdrawn
     *
     * @param value
     *          The automaticWithdrawn value.
     * @return The builder for this class.
     */
    public Builder automaticWithdrawn(Boolean value) {
      this.dataFields.put("automaticWithdrawn", value);
      return this;
    }

    /**
     * Set the payinDeferred value
     *
     * Description: Payin_Deferred
     *
     * @param value
     *          The payinDeferred value.
     * @return The builder for this class.
     */
    public Builder payinDeferred(Boolean value) {
      this.dataFields.put("payinDeferred", value);
      return this;
    }

    /**
     * Set the oUTUponClearingUse value
     *
     * Description: Account to be used upon reconciliation
     *
     * @param value
     *          The oUTUponClearingUse value.
     * @return The builder for this class.
     */
    public Builder oUTUponClearingUse(String value) {
      this.dataFields.put("oUTUponClearingUse", value);
      return this;
    }

    /**
     * Set the payinExecutionType value
     *
     * Description: Payin_Execution_Type
     *
     * @param value
     *          The payinExecutionType value.
     * @return The builder for this class.
     */
    public Builder payinExecutionType(String value) {
      this.dataFields.put("payinExecutionType", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the automaticPayment value
     *
     * Description: Automatic Payment
     *
     * @param value
     *          The automaticPayment value.
     * @return The builder for this class.
     */
    public Builder automaticPayment(Boolean value) {
      this.dataFields.put("automaticPayment", value);
      return this;
    }

    /**
     * Set the payoutExecutionProcessID value
     *
     * Description: Payout_Execution_Process_ID
     *
     * @param value
     *          The payoutExecutionProcessID value.
     * @return The builder for this class.
     */
    public Builder payoutExecutionProcess(String value) {
      this.dataFields.put("payoutExecutionProcess", value);
      return this;
    }

    /**
     * Set the uponPaymentUse value
     *
     * Description: Account used upon payment
     *
     * @param value
     *          The uponPaymentUse value.
     * @return The builder for this class.
     */
    public Builder uponPaymentUse(String value) {
      this.dataFields.put("uponPaymentUse", value);
      return this;
    }

    /**
     * Set the payoutAllow value
     *
     * Description: Payout_Allow
     *
     * @param value
     *          The payoutAllow value.
     * @return The builder for this class.
     */
    public Builder payoutAllow(Boolean value) {
      this.dataFields.put("payoutAllow", value);
      return this;
    }

    /**
     * Set the automaticReceipt value
     *
     * Description: Automatic Receipt
     *
     * @param value
     *          The automaticReceipt value.
     * @return The builder for this class.
     */
    public Builder automaticReceipt(Boolean value) {
      this.dataFields.put("automaticReceipt", value);
      return this;
    }

    /**
     * Set the payinAllow value
     *
     * Description: Payin_Allow
     *
     * @param value
     *          The payinAllow value.
     * @return The builder for this class.
     */
    public Builder payinAllow(Boolean value) {
      this.dataFields.put("payinAllow", value);
      return this;
    }

    /**
     * Set the uponDepositUse value
     *
     * Description: Account used upon deposit
     *
     * @param value
     *          The uponDepositUse value.
     * @return The builder for this class.
     */
    public Builder uponDepositUse(String value) {
      this.dataFields.put("uponDepositUse", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the uponReceiptUse value
     *
     * Description: Account used upon receipt
     *
     * @param value
     *          The uponReceiptUse value.
     * @return The builder for this class.
     */
    public Builder uponReceiptUse(String value) {
      this.dataFields.put("uponReceiptUse", value);
      return this;
    }

    /**
     * Set the payoutExecutionType value
     *
     * Description: Payout_Execution_Type
     *
     * @param value
     *          The payoutExecutionType value.
     * @return The builder for this class.
     */
    public Builder payoutExecutionType(String value) {
      this.dataFields.put("payoutExecutionType", value);
      return this;
    }

    /**
     * Set the payinExecutionProcessID value
     *
     * Description: Payin_Execution_Process_ID
     *
     * @param value
     *          The payinExecutionProcessID value.
     * @return The builder for this class.
     */
    public Builder payinExecutionProcess(String value) {
      this.dataFields.put("payinExecutionProcess", value);
      return this;
    }

    /**
     * Set the uponWithdrawalUse value
     *
     * Description: Account used upon withdrawal
     *
     * @param value
     *          The uponWithdrawalUse value.
     * @return The builder for this class.
     */
    public Builder uponWithdrawalUse(String value) {
      this.dataFields.put("uponWithdrawalUse", value);
      return this;
    }

    /**
     * Set the iNUponClearingUse value
     *
     * Description: Account used upon reconciliation
     *
     * @param value
     *          The iNUponClearingUse value.
     * @return The builder for this class.
     */
    public Builder iNUponClearingUse(String value) {
      this.dataFields.put("iNUponClearingUse", value);
      return this;
    }

    /**
     * Set the payoutDeferred value
     *
     * Description: Payout_Deferred
     *
     * @param value
     *          The payoutDeferred value.
     * @return The builder for this class.
     */
    public Builder payoutDeferred(Boolean value) {
      this.dataFields.put("payoutDeferred", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the payinIsMulticurrency value
     *
     * Description: A flag indicating whether this Payment Method receive payments in multiple
     * currencies
     *
     * @param value
     *          The payinIsMulticurrency value.
     * @return The builder for this class.
     */
    public Builder payinIsMulticurrency(Boolean value) {
      this.dataFields.put("payinIsMulticurrency", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PaymentMethodData build() {
      return new PaymentMethodData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PaymentMethodData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
