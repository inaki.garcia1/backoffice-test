/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui.searchitems;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.gui.popups.AttributeSetPopUpSc;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.SearchItem;
import com.openbravo.test.integration.selenium.NotImplementedException;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions on OpenbravoERP attribute search item.
 *
 * @author elopio
 *
 */
public class AttributeSearchItem extends OBSearchItem {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Components. */
  /** The attribute selector pop up. */
  private AttributeSetPopUpSc popUp;

  /**
   * Class constructor.
   *
   * @param searchItem
   *          The search item of this selector.
   */
  public AttributeSearchItem(SearchItem searchItem) {
    super(searchItem);
  }

  /**
   * Set the attribute search item value.
   *
   * @param value
   *          The value to set.
   */
  @Override
  public void setValue(Object value) {
    logger.debug("Setting the value '{}' to the attribute search item'.", value);
    if (value instanceof String) {
      searchItem.openSearchPopUp();
      popUp = new AttributeSetPopUpSc();
      popUp.setAttribute((String) value);
      popUp = null;
      SeleniumSingleton.INSTANCE.switchTo().window("");
    } else {
      throw new IllegalArgumentException(
          "Valid values for location selectors are only of class LocationSelectorData.");
    }
  }

  /**
   * Get the title of the item.
   */
  @Override
  public String getTitle() {
    // TODO
    throw new NotImplementedException();
  }

}
