/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.productsetup.attributeset;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for AttributeSetData
 *
 * @author plujan
 *
 */
public class AttributeSetData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the guaranteedDays value
     *
     * Description: Number of days the product is guaranteed or available
     *
     * @param value
     *          The guaranteedDays value.
     * @return The builder for this class.
     */
    public Builder guaranteedDays(String value) {
      this.dataFields.put("guaranteedDays", value);
      return this;
    }

    /**
     * Set the requireAtLeastOneValue value
     *
     * Description: At least one attribute set value will be required in transactions.
     *
     * @param value
     *          The requireAtLeastOneValue value.
     * @return The builder for this class.
     */
    public Builder requireAtLeastOneValue(Boolean value) {
      this.dataFields.put("requireAtLeastOneValue", value);
      return this;
    }

    /**
     * Set the lot value
     *
     * Description: A group of identical or similar items organized and placed into inventory under
     * one number.
     *
     * @param value
     *          The lot value.
     * @return The builder for this class.
     */
    public Builder lot(Boolean value) {
      this.dataFields.put("lot", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the serialNo value
     *
     * Description: An attribute used as a unique identifier for a product.
     *
     * @param value
     *          The serialNo value.
     * @return The builder for this class.
     */
    public Builder serialNo(Boolean value) {
      this.dataFields.put("serialNo", value);
      return this;
    }

    /**
     * Set the expirationDate value
     *
     * Description: The date upon which an item is guaranteed to be of good quality.
     *
     * @param value
     *          The expirationDate value.
     * @return The builder for this class.
     */
    public Builder expirationDate(Boolean value) {
      this.dataFields.put("expirationDate", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public AttributeSetData build() {
      return new AttributeSetData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private AttributeSetData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
