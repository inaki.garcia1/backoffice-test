/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Lujan <plu@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testsuites.smoke.masterdata;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.common.TestFiles;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.accounting.setup.taxcategory.TaxCategoryData;
import com.openbravo.test.integration.erp.data.financial.accounting.setup.taxrate.TaxData;
import com.openbravo.test.integration.erp.modules.initialdataload.data.masterdata.process.importwindow.ImportData;
import com.openbravo.test.integration.erp.modules.initialdataload.testscripts.masterdata.process.importwindow.Import;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.setup.taxcategory.TaxCategory;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.setup.taxrate.TaxRate;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Execute the Setup Initial Data Load flow of the smoke test suite.
 *
 * @author Pablo Lujan
 */
@RunWith(Parameterized.class)
public class ADMhb_LoadProductsData extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  ImportData importProductsData;
  ImportData importPricelistData;

  TaxCategoryData taxCategoryData;

  TaxData taxData;

  ImportData importQuantityData;

  /**
   * Class constructor.
   */
  public ADMhb_LoadProductsData(ImportData importProductsData, ImportData importPricelistData,
      TaxCategoryData taxCategoryData, TaxData taxData, ImportData importQuantityData) {
    this.importProductsData = importProductsData;
    this.importPricelistData = importPricelistData;
    this.taxCategoryData = taxCategoryData;
    this.taxData = taxData;
    this.importQuantityData = importQuantityData;

    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getClientAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getClientAdministratorPassword())
        .build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   * @throws IOException
   *
   */
  @Parameters
  public static Collection<Object[]> setupProductValues() throws IOException {
    return Arrays.asList(new Object[][] { {

        // Products
        new ImportData.Builder().entity("Product")
            .file(TestFiles.getFullFilePath("Products.csv"))
            .build(),

        // Pricelist
        new ImportData.Builder().entity("Price List")
            .file(TestFiles.getFullFilePath("PriceLists.csv"))
            .build(),

        // Edit Tax Category
        new TaxCategoryData.Builder().name("VAT 10%").defaultValue(true).build(),

        // Create Tax Rate
        new TaxData.Builder().name("VAT 10%")
            .description("VAT 10%")
            .taxCategory("VAT 10%")
            .summaryLevel(true)
            .salesPurchaseType("Both")/* .rate("10") */
            .country("Spain")
            .destinationCountry("Spain")
            .validFromDate("01-01-2000")
            .build(),

        // Quantity On hand
        new ImportData.Builder().entity("Stock")
            .file(TestFiles.getFullFilePath("OnHandQuantity.csv"))
            .build()

        } });
  }

  /**
   * Test the setup client and organization flow.
   *
   * @throws IOException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void productsDataShouldBeLoaded() throws IOException, OpenbravoERPTestException {
    logger.info("** Start of test case [ADMhb010] Import products **");
    Import.validateFile(mainPage, importProductsData);
    // TODO add the values to the parameters.
    Import.verifyValidationProcessCompletedSuccessfully(mainPage, 11, 11, 0);
    Import.processFile(mainPage, importProductsData);
    Import.verifyImportProcessCompletedSuccessfully(mainPage, 11, 11, 0);
    logger.info("** End of test case [ADMhb010] Import products **");

    logger.info("** Start of test case [ADMhb020] Import Pricelist **");
    Import.validateFile(mainPage, importPricelistData);
    // TODO see issue 15375 - https://issues.openbravo.com/view.php?id=15375
    // TODO add the values to the parameters.
    Import.verifyValidationProcessCompletedSuccessfully(mainPage, 10, 10, 0);
    Import.processFile(mainPage, importPricelistData);
    Import.verifyImportProcessCompletedSuccessfully(mainPage, 10, 10, 0);
    logger.info("** End of test case [ADMhb020] Import Pricelist **");

    logger.info("** Start of test case [ADMhb030] Edit Tax Categories **");
    TaxCategory.TaxCategoryTab.select(mainPage,
        new TaxCategoryData.Builder().name((String) taxCategoryData.getDataField("name")).build());
    TaxCategory.TaxCategoryTab.edit(mainPage, taxCategoryData);
    logger.info("** End of test case [ADMhb030] Edit Tax Categories **");

    logger.info("** Start of test case [ADMhb040] Create Tax rate **");
    TaxRate.Tax.create(mainPage, taxData);
    logger.info("** End of test case [ADMhb040] Create Tax rate **");

    logger.info("** Start of test case [ADMhb050] Import quantity on hand **");
    Import.validateFile(mainPage, importQuantityData);
    Import.verifyValidationProcessCompletedSuccessfully(mainPage, 9, 9, 0);
    Import.processFile(mainPage, importQuantityData);
    Import.verifyImportProcessCompletedSuccessfully(mainPage, 9, 9, 0);
    logger.info("** End of test case [ADMhb050] Import quantity on hand **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
    OpenbravoERPTest.seleniumStarted = false;
    SeleniumSingleton.INSTANCE.quit();
  }
}
