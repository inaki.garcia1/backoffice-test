/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.openbravo.test.integration.erp.testsuites.smoke.masterdata;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.applicationdictionary.module.ModuleData;
import com.openbravo.test.integration.erp.testscripts.applicationdictionary.module.Module;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Test that once a instance its set as production, any attempt to mark a module as in development
 * should fail
 *
 * @author jarmendariz
 */
@RunWith(Parameterized.class)
public class ADMib_ProductionInstanceCannotHaveDevModules extends ADMi_ActivateInstanceBaseTest {

  public ADMib_ProductionInstanceCannotHaveDevModules(LogInData logInData) {
    this.logInData = logInData;
  }

  @Parameters
  public static Collection<LogInData[]> parameters() {
    return Arrays.asList(new LogInData[][] { { new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build() } });
  }

  @Test
  public void testSavingAModuleAsInDevelopmentInProductionShouldFail() {
    changeRoleToSystemAdmin();
    setupInstanceAsProduction();

    verifyCoreModuleCannotBeSetAsInDevelopment();
    setupInstanceAsTesting();
  }

  private void verifyCoreModuleCannotBeSetAsInDevelopment() {
    Module module = new Module(mainPage).open();
    module.select(new ModuleData.Builder().name("Core").build());
    module.edit(new ModuleData.Builder().inDevelopment(Boolean.TRUE).build());

    module.assertErrorMessage(
        "Saving failed. Cannot have modules in &quot;In Development&quot; status in a Production instance");
  }

}
