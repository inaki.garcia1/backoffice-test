/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Lujan <plu@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testsuites.smoke.masterdata;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.accounting.setup.taxcategory.TaxCategoryData;
import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelist.PriceListData;
import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelist.PriceListVersionData;
import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelistschema.PriceListSchemaHeaderData;
import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelistschema.PriceListSchemaLinesData;
import com.openbravo.test.integration.erp.data.masterdata.product.PriceData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductData;
import com.openbravo.test.integration.erp.data.masterdata.productsetup.attributeset.AttributeSetData;
import com.openbravo.test.integration.erp.data.masterdata.productsetup.productcategory.ProductCategoryData;
import com.openbravo.test.integration.erp.data.masterdata.productsetup.unitofmeasure.UnitOfMeasureData;
import com.openbravo.test.integration.erp.data.selectors.LocationSelectorData;
import com.openbravo.test.integration.erp.data.warehouse.setup.warehouseandstoragebins.StorageBinData;
import com.openbravo.test.integration.erp.data.warehouse.setup.warehouseandstoragebins.WarehouseData;
import com.openbravo.test.integration.erp.data.warehouse.transactions.physicalinventory.CreateInventoryCountListData;
import com.openbravo.test.integration.erp.data.warehouse.transactions.physicalinventory.PhysicalInventoryHeaderData;
import com.openbravo.test.integration.erp.data.warehouse.transactions.physicalinventory.PhysicalInventoryLinesData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.setup.taxcategory.TaxCategory;
import com.openbravo.test.integration.erp.testscripts.masterdata.pricing.pricelist.PriceList;
import com.openbravo.test.integration.erp.testscripts.masterdata.pricing.pricelistschema.PriceListSchema;
import com.openbravo.test.integration.erp.testscripts.masterdata.product.Product;
import com.openbravo.test.integration.erp.testscripts.masterdata.productsetup.attritbuteset.AttributeSet;
import com.openbravo.test.integration.erp.testscripts.masterdata.productsetup.productcategory.ProductCategory;
import com.openbravo.test.integration.erp.testscripts.masterdata.productsetup.unitofmeasure.UnitOfMeasure;
import com.openbravo.test.integration.erp.testscripts.warehouse.setup.warehouse.Warehouse;
import com.openbravo.test.integration.erp.testscripts.warehouse.transactions.physicalinventory.PhysicalInventory;
import com.openbravo.test.integration.erp.testscripts.warehouse.transactions.physicalinventory.PhysicalInventoryHeader;
import com.openbravo.test.integration.erp.testscripts.warehouse.transactions.physicalinventory.PhysicalInventoryLines;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Execute the Setup User and Role flow of the smoke test suite.
 *
 * @author Pablo Lujan
 */
@RunWith(Parameterized.class)
public class ADMe_SetupProduct extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  private PriceListSchemaHeaderData priceListSchemaHeaderStandardData;
  private PriceListSchemaLinesData priceListSchemaLinesStandardData;
  private PriceListSchemaHeaderData priceListSchemaHeaderDiscountData;
  private PriceListSchemaLinesData priceListSchemaLinesDiscountData;

  private PriceListData priceListData;
  private PriceListVersionData priceListVersionData;

  private AttributeSetData attributeSetSerialNumberData;
  private AttributeSetData attributeSetLotData;

  private ProductCategoryData productCategoryData;
  private UnitOfMeasureData unitOfMeasureData;

  private TaxCategoryData taxCategoryData;

  private ProductData productData;
  private PriceData priceData;

  private PriceListData priceListDiscountData;
  private PriceListVersionData priceListVersionDiscountData;
  private WarehouseData warehouseData;
  private StorageBinData storageBinData;

  private PhysicalInventoryHeaderData physicalInventoryHeaderData;
  private CreateInventoryCountListData createInventoryCountListData;
  private PhysicalInventoryLinesData physicalInventoryLinesData;

  /**
   * Class constructor.
   */
  public ADMe_SetupProduct(PriceListSchemaHeaderData priceListSchemaHeaderStandardData,
      PriceListSchemaLinesData priceListSchemaLinesStandardData,
      PriceListSchemaHeaderData priceListSchemaHeaderDiscountData,
      PriceListSchemaLinesData priceListSchemaLinesDiscountData, PriceListData priceListData,
      PriceListVersionData priceListVersionData, AttributeSetData attributeSetSerialNumberData,
      AttributeSetData attributeSetLotData, ProductCategoryData productCategoryData,
      /* AccountingData accountingData, */UnitOfMeasureData unitOfMeasureData,
      TaxCategoryData taxCategoryData, ProductData productData, PriceData priceData,
      PriceListData priceListDiscountData, PriceListVersionData priceListVersionDiscountData,
      /* ProductPriceData productPriceDiscountData, */WarehouseData warehouseData,
      StorageBinData storageBinData, PhysicalInventoryHeaderData physicalInventoryHeaderData,
      CreateInventoryCountListData createInventoryCountListData,
      PhysicalInventoryLinesData physicalInventoryLinesData) {

    this.priceListSchemaHeaderStandardData = priceListSchemaHeaderStandardData;
    this.priceListSchemaLinesStandardData = priceListSchemaLinesStandardData;
    this.priceListSchemaHeaderDiscountData = priceListSchemaHeaderDiscountData;
    this.priceListSchemaLinesDiscountData = priceListSchemaLinesDiscountData;
    this.priceListData = priceListData;
    this.priceListVersionData = priceListVersionData;
    this.attributeSetSerialNumberData = attributeSetSerialNumberData;
    this.attributeSetLotData = attributeSetLotData;
    this.productCategoryData = productCategoryData;
    // this.accountingData = accountingData;
    this.unitOfMeasureData = unitOfMeasureData;
    this.taxCategoryData = taxCategoryData;
    this.productData = productData;
    this.priceData = priceData;
    this.priceListDiscountData = priceListDiscountData;
    this.priceListVersionDiscountData = priceListVersionDiscountData;
    // this.productPriceDiscountData = productPriceDiscountData;
    this.warehouseData = warehouseData;
    this.storageBinData = storageBinData;
    this.physicalInventoryHeaderData = physicalInventoryHeaderData;
    this.createInventoryCountListData = createInventoryCountListData;
    this.physicalInventoryLinesData = physicalInventoryLinesData;

    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getClientAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getClientAdministratorPassword())
        .build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   * @throws IOException
   *
   */
  @Parameters
  public static Collection<Object[]> setupProductValues() throws IOException {
    return Arrays.asList(new Object[][] { {

        // Create Pricelist Schema Standard
        new PriceListSchemaHeaderData.Builder().name("Standard").build(),
        new PriceListSchemaLinesData.Builder() // No data to insert
            .build(),
        // Create Pricelist Schema with discount
        new PriceListSchemaHeaderData.Builder().name("Spain_pricelist_schema").build(),
        new PriceListSchemaLinesData.Builder().listPriceDiscount("10")
            .standardPriceDiscount("10")
            .build(),

        // Create Pricelist
        new PriceListData.Builder().name("Based_pricelist")
            .salesPriceList(true)
            .currency("EUR")
            .build(),
        new PriceListVersionData.Builder().name("Wholesale_pricelist")
            .priceListSchema("Standard")
            .validFromDate("01-01-2000")
            .build(),

        // Create Attribute Set - Serial
        new AttributeSetData.Builder().name("Serial number").lot(false).serialNo(true).build(),
        // Create Attribute Set - Lot
        new AttributeSetData.Builder().name("Lot").lot(true).serialNo(false).build(),

        // Create Product Category
        new ProductCategoryData.Builder().searchKey("Pr_Met").name("Precious metals").build(),
        // TODO
        // new AccountingData.Builder().fixedAsset(new
        // AccountSelectorData()"Spain - 35000").productExpense("Spain - 60000")
        // .productCOGS("Spain - 99900").productRevenue("Spain - 70000").build(),

        // Create Unit of Measure
        new UnitOfMeasureData.Builder().eDICode("Ca")
            .symbol("Ca")
            .name("Carat")
            .standardPrecision("2")
            .costingPrecision("2")
            .build(),

        // Create Tax Category
        new TaxCategoryData.Builder().name("VAT 3%").build(),

        // Create a Product
        new ProductData.Builder().searchKey("Platinum")
            .name("Platinum")
            .productCategory("Precious metals")
            .uOM("Carat")
            .productType("Item")
            .purchase(true)
            .sale(true)
            .taxCategory("VAT 3%")
            .attributeSet("Lot")
            .build(),
        new PriceData.Builder().priceListVersion("Wholesale_pricelist")
            .listPrice("2")
            .standardPrice("2")
            .build(),

        // Create a Discount Price List
        new PriceListData.Builder().name("Discount_pricelist")
            .salesPriceList(true)
            .currency("EUR")
            .build(),
        new PriceListVersionData.Builder().name("Discount_pricelist")
            .priceListSchema("Spain_pricelist_schema")
            .basePriceListVersion("Wholesale_pricelist")
            .validFromDate("01-01-2000")
            .build(),

        // TODO
        // new
        // ProductPriceData.Builder().product("Platinum").listPrice("1.8").standardPrice("1.8").build(),

        // Create Warehouse
        new WarehouseData.Builder().organization("Spain")
            .searchKey("Standard")
            .name("Standard")
            .locationAddress(new LocationSelectorData.Builder().firstLine("P.I. Landaben")
                .secondLine("Calle J. Edif. SLAN")
                .postalCode("31012")
                .city("Pamplona")
                .country("Spain")
                .region("NAVARRA")
                .build())
            .build(),
        new StorageBinData.Builder().searchKey("Standard")
            .rowX("1")
            .stackY("1")
            .levelZ("1")
            .build(),

        // Create Physical Inventory Header data.
        new PhysicalInventoryHeaderData.Builder().name("Testing Physical Inventory").build(),
        new CreateInventoryCountListData.Builder().storagebin("Standard")
            .inventoryQuantity("0")
            .build(),

        // Create Physical Inventory Header data.
        new PhysicalInventoryLinesData.Builder().quantityCount("20000")
            .attributeSetValue("101")
            .build(), } });
  }

  /**
   * Test the setup client and organization flow.
   *
   * @throws IOException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void productShouldBeSetUp() throws IOException, OpenbravoERPTestException {
    logger.info("** Start of test case [ADMe010]Create pricelist schema **");

    PriceListSchema.Header.create(mainPage, priceListSchemaHeaderStandardData);
    PriceListSchema.Lines.create(mainPage, priceListSchemaLinesStandardData);
    PriceListSchema.Header.create(mainPage, priceListSchemaHeaderDiscountData);
    PriceListSchema.Lines.create(mainPage, priceListSchemaLinesDiscountData);

    logger.info("** End of test case [ADMe010]Create pricelist schema **");

    logger.info("** Start of test case [ADMe020] Create Pricelist **");
    PriceList.PriceListTab.create(mainPage, priceListData);
    PriceList.PriceListVersionTab.create(mainPage, priceListVersionData);
    logger.info("** End of test case [ADMe020] Create Pricelist **");

    logger.info("** Start of test case [ADMe030] Create Attribute set **");
    AttributeSet.AttributeSetTab.create(mainPage, attributeSetSerialNumberData);
    AttributeSet.AttributeSetTab.create(mainPage, attributeSetLotData);
    logger.info("** End of test case [ADMe030] Create Attribute set **");

    logger.info("** Start of test case [ADMe040] Create Product category **");
    ProductCategory.ProductCategoryTab.create(mainPage, productCategoryData);
    // TODO Verify the accounting tab.
    // ProductCategory.AccountingTab.verify(mainPage, accountingData);
    logger.info("** End of test case [ADMe040] Create Product category **");

    logger.info("** Start of test case [ADMe050] Create UOM **");
    UnitOfMeasure.UnitOfMeasureTab.create(mainPage, unitOfMeasureData);
    logger.info("** End of test case [ADMe050] Create UOM **");

    logger.info("** Start of test case [ADMe060]Create tax category **");
    TaxCategory.TaxCategoryTab.create(mainPage, taxCategoryData);
    logger.info("** End of test case [ADMe060]Create tax category **");

    logger.info("** Start of test case [ADMe070] Create a Product **");
    Product.ProductTab.create(mainPage, productData);
    Product.Price.create(mainPage, priceData);
    logger.info("** End of test case [ADMe070] Create a Product **");

    logger.info("** Start of test case [ADMe080]Create a discount Pricelist **");
    PriceList.PriceListTab.create(mainPage, priceListDiscountData);
    PriceList.PriceListVersionTab.create(mainPage, priceListVersionDiscountData);
    PriceList.PriceListVersionTab.processCreatePricelist(mainPage);
    // TODO Verify that 10% discount is applied in both List price & Standard price for all the
    // finished goods.
    // PriceList.ProductPrice.verify(mainPage, productPriceDiscountData);
    logger.info("** End of test case [ADMe080]Create a discount Pricelist **");

    logger.info("** Start of test case [ADMe090] Create Warehouse **");
    Warehouse.WarehouseTab.create(mainPage, warehouseData);
    Warehouse.StorageBinTab.create(mainPage, storageBinData);
    logger.info("** End of test case [ADMe090] Create Warehouse **");

    logger.info("** Start of test case [ADMe100] Create Physical inventory **");
    if (mainPage.isOnNewLayout()) {
      PhysicalInventoryHeader physicalInventory = new PhysicalInventoryHeader(mainPage).open();
      physicalInventory.create(physicalInventoryHeaderData);
      physicalInventory.createInventoryCountList(createInventoryCountListData, 1, 0);
      PhysicalInventoryLines physicalInventoryLines = new PhysicalInventoryLines(mainPage);
      if (physicalInventoryLines.getRecordCount() == 0) {
        logger.warn("No inventory lines were found. Refreshing to try again.");
        SeleniumSingleton.INSTANCE.navigate().refresh();
        mainPage.waitForDataToLoad();
        physicalInventory.open();
      }
      // TODO select the product.
      physicalInventoryLines.edit(physicalInventoryLinesData);
      physicalInventory.processInventoryCount();
    } else {
      PhysicalInventory.Header.create(mainPage, physicalInventoryHeaderData);
      PhysicalInventory.Header.createInventoryCountList(mainPage, createInventoryCountListData, 1,
          0);
      // TODO select the product.
      PhysicalInventory.Lines.edit(mainPage, physicalInventoryLinesData);
      PhysicalInventory.Header.processInventoryCount(mainPage);
    }
    // TODO Verify that the stock value for the Product Platinum is 20000.
    logger.info("** End of test case [ADMe100] Create Physical inventory **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
    OpenbravoERPTest.seleniumStarted = false;
    SeleniumSingleton.INSTANCE.quit();
  }
}
