/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Pablo Lujan <pablo.lujan@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.application.preference;

import com.openbravo.test.integration.erp.data.generalsetup.application.preference.PreferenceData;
import com.openbravo.test.integration.erp.gui.general.application.preference.PreferenceTab;
import com.openbravo.test.integration.erp.gui.general.application.preference.PreferenceWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the Preference tab of OpenbravoERP.
 *
 * @author aferraz
 *
 */
class PreferenceScript {

  /**
   * Create a record.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param data
   *          The data of the record that will be created.
   */
  static void create(MainPage mainPage, PreferenceData data) {
    PreferenceWindow preferenceWindow = new PreferenceWindow();
    mainPage.openView(preferenceWindow);
    PreferenceTab preferenceTab = preferenceWindow.selectPreferenceTab();
    preferenceTab.createRecord(data);
    preferenceTab.assertSaved();
  }

  /**
   * Deletes a record.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param data
   *          The data of the record that will be created.
   */
  static void delete(MainPage mainPage, PreferenceData data) {
    PreferenceWindow preferenceWindow = new PreferenceWindow();
    mainPage.openView(preferenceWindow);
    PreferenceTab preferenceTab = preferenceWindow.selectPreferenceTab();
    preferenceTab.select(data);
    preferenceTab.deleteOnGrid();
  }

  /**
   * Assert the number of records is expectedCount
   *
   * @param mainPage
   *          The application main page.
   * @param data
   *          The data of the record that will be created.
   * @param expectedCount
   *          The expected number of records.
   */
  static void assertNumRecords(MainPage mainPage, PreferenceData data, int expectedCount) {
    PreferenceWindow preferenceWindow = new PreferenceWindow();
    mainPage.openView(preferenceWindow);
    PreferenceTab preferenceTab = preferenceWindow.selectPreferenceTab();
    preferenceTab.assertCount(expectedCount);
  }

  /**
   * Filter the grid
   *
   * @param mainPage
   *          The application main page.
   * @param data
   *          The data of the record that will be filtered.
   */
  static void filter(MainPage mainPage, PreferenceData data) {
    PreferenceWindow preferenceWindow = new PreferenceWindow();
    mainPage.openView(preferenceWindow);
    PreferenceTab preferenceTab = preferenceWindow.selectPreferenceTab();
    preferenceTab.filter(data);
  }

  /**
   * Get the number of records on grid.
   *
   * @param mainPage
   *          The application main page.
   */
  static int getRecordCount(MainPage mainPage) {
    PreferenceWindow preferenceWindow = new PreferenceWindow();
    mainPage.openView(preferenceWindow);
    PreferenceTab preferenceTab = preferenceWindow.selectPreferenceTab();
    return preferenceTab.getRecordCount();
  }
}
