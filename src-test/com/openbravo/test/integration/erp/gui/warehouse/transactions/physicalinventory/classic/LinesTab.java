/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.warehouse.transactions.physicalinventory.classic;

import com.openbravo.test.integration.erp.data.warehouse.transactions.physicalinventory.PhysicalInventoryLinesData;
import com.openbravo.test.integration.erp.gui.popups.AttributeSetPopUp;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Physical Inventory Lines tab.
 *
 * @author elopio
 *
 */
public class LinesTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Lines";
  /** The tab level. */
  private static final int LEVEL = 1;

  /** Identifier of the Lines tab */
  public static final String TAB = "tabname256";

  /** Identifier of the Quantity Count text field */
  private static final String TEXT_FIELD_QUANTITY_COUNT = "QtyCount";

  /**
   * Class constructor
   *
   */
  public LinesTab() {
    super(TITLE, LEVEL, TAB);
  }

  /**
   * Edit the line of the physical inventory.
   *
   * @param data
   *          The data of the Line to create.
   */
  public void edit(PhysicalInventoryLinesData data) throws OpenbravoERPTestException {
    switchToFormView();
    if (data.getDataField("attributeSetValue") != null) {
      String windowHandle = SeleniumSingleton.INSTANCE.getWindowHandle();
      SeleniumSingleton.INSTANCE.findElementById(AttributeSetPopUp.BUTTON_ATTRIBUTE_SET).click();
      final AttributeSetPopUp attributeSetScreen = new AttributeSetPopUp();
      attributeSetScreen.setAttribute(data.getDataField("attributeSetValue").toString());
      SeleniumSingleton.INSTANCE.switchTo().window(windowHandle);
      waitForFrame();
    }
    if (data.getDataField("quantityCount") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_QUANTITY_COUNT)
          .sendKeys(data.getDataField("quantityCount").toString());
    }
    clickSave();
  }
}
