package com.openbravo.test.integration.erp.gui.procurement.transactions.returntovendor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorShipmentHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorShipmentLineData;
import com.openbravo.test.integration.erp.gui.popups.RequestProcessPopUp;
/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *   Nono Carballo <f.carballo@nectus.com>
 *************************************************************************
 */
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 *
 * Class for Return to Vendor Header Tab
 *
 * @author Rafael Queralta
 *
 */
public class ReturnToVendorShipmentHeaderTab
    extends GeneratedTab<ReturnToVendorShipmentHeaderData> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The tab title. */
  static final String TITLE = "Header";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "728DBD16A1F14A4D82335E37BA433E33";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /* Registry keys. */
  /** Registry key of the complete button. */
  private static final String REGISTRY_KEY_BUTTON_COMPLETE = "org.openbravo.client.application.toolbar.button.documentAction.728DBD16A1F14A4D82335E37BA433E33";
  /** Registry key of the request process pop up. */
  private static final String REGISTRY_KEY_POP_UP_REQUEST_PROCESS = "org.openbravo.classicpopup./ReturntoVendorShipment/Header_Edition.html";
  /** Registry key of the Pick and Edit Lines Button */
  private static final String REGISTRY_KEY_BUTTON_PICKANDEDIT = "org.openbravo.client.application.toolbar.button.sendMaterials.728DBD16A1F14A4D82335E37BA433E33";
  /** Registry key of the Pick and Edit Lines Pop Up */
  private static final String REGISTRY_KEY_POPUP_PICKANDEDIT = "org.openbravo.client.application.process.pickandexecute.popup";

  /* Toolbar buttons. */
  private static Button buttonComplete;
  private static Button buttonPickAndEdit;

  public ReturnToVendorShipmentHeaderTab() {
    super(TITLE, IDENTIFIER);
    buttonComplete = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_COMPLETE));
    buttonPickAndEdit = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_PICKANDEDIT));
    addChildTab(new ReturnToVendorShipmentLinesTab(this));
    logger.debug("Creating Return to Vendor Header Tab");
  }

  private void process(String action) {
    logger.debug("Process return to vendor order header with action '{}'.", action);
    buttonComplete.click();
    RequestProcessPopUp popUp = new RequestProcessPopUp(REGISTRY_KEY_POP_UP_REQUEST_PROCESS);
    popUp.process(action);
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }

  public void complete() {
    process("Complete");
    // TODO L1: Check after stabilization if following sleeps is still required
    Sleep.trySleep();
  }

  public PickAndExecuteWindow<ReturnToVendorShipmentLineData> openPickAndEditLines() {
    buttonPickAndEdit.click();
    PickAndExecuteWindow<ReturnToVendorShipmentLineData> popUp = new PickAndExecuteWindow<ReturnToVendorShipmentLineData>(
        TestRegistry.getObjectString(REGISTRY_KEY_POPUP_PICKANDEDIT));
    return popUp;
  }

}
