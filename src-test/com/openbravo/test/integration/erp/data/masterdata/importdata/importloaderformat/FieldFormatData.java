/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.importdata.importloaderformat;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for FieldFormatData
 *
 * @author plujan
 *
 */
public class FieldFormatData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the calloutFunction value
     *
     * Description: A series of actions that occur when data is modified.
     *
     * @param value
     *          The calloutFunction value.
     * @return The builder for this class.
     */
    public Builder calloutFunction(String value) {
      this.dataFields.put("calloutFunction", value);
      return this;
    }

    /**
     * Set the constantValue value
     *
     * Description: Constant value
     *
     * @param value
     *          The constantValue value.
     * @return The builder for this class.
     */
    public Builder constantValue(String value) {
      this.dataFields.put("constantValue", value);
      return this;
    }

    /**
     * Set the column value
     *
     * Description: A link to the database column of the table.
     *
     * @param value
     *          The column value.
     * @return The builder for this class.
     */
    public Builder column(String value) {
      this.dataFields.put("column", value);
      return this;
    }

    /**
     * Set the divideBy100 value
     *
     * Description: Divide number by 100 to get correct amount
     *
     * @param value
     *          The divideBy100 value.
     * @return The builder for this class.
     */
    public Builder divideBy100(Boolean value) {
      this.dataFields.put("divideBy100", value);
      return this;
    }

    /**
     * Set the decimalPoint value
     *
     * Description: Decimal Point in the data file
     *
     * @param value
     *          The decimalPoint value.
     * @return The builder for this class.
     */
    public Builder decimalPoint(String value) {
      this.dataFields.put("decimalPoint", value);
      return this;
    }

    /**
     * Set the dataFormat value
     *
     * Description: Format String in Java Notation e.g. ddMMyy
     *
     * @param value
     *          The dataFormat value.
     * @return The builder for this class.
     */
    public Builder dataFormat(String value) {
      this.dataFields.put("dataFormat", value);
      return this;
    }

    /**
     * Set the dataType value
     *
     * Description: Type of data
     *
     * @param value
     *          The dataType value.
     * @return The builder for this class.
     */
    public Builder dataType(String value) {
      this.dataFields.put("dataType", value);
      return this;
    }

    /**
     * Set the endNo value
     *
     * Description: End No
     *
     * @param value
     *          The endNo value.
     * @return The builder for this class.
     */
    public Builder endNo(String value) {
      this.dataFields.put("endNo", value);
      return this;
    }

    /**
     * Set the startingNo value
     *
     * Description: The first number that will be used in a standard or control sequence.
     *
     * @param value
     *          The startingNo value.
     * @return The builder for this class.
     */
    public Builder startingNo(String value) {
      this.dataFields.put("startingNo", value);
      return this;
    }

    /**
     * Set the sequenceNumber value
     *
     * Description: The order of records in a specified document.
     *
     * @param value
     *          The sequenceNumber value.
     * @return The builder for this class.
     */
    public Builder sequenceNumber(String value) {
      this.dataFields.put("sequenceNumber", value);
      return this;
    }

    /**
     * Set the importFormat value
     *
     * Description: Import Format
     *
     * @param value
     *          The importFormat value.
     * @return The builder for this class.
     */
    public Builder importFormat(String value) {
      this.dataFields.put("importFormat", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public FieldFormatData build() {
      return new FieldFormatData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private FieldFormatData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
