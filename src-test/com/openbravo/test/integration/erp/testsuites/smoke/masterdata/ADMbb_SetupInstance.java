/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testsuites.smoke.masterdata;

import static com.openbravo.test.integration.erp.data.generalsetup.application.instanceactivation.Purposes.TESTING;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.common.TestFiles;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.generalsetup.application.instanceactivation.ActivateOnlineData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.intanceactivation.InstanceActivation;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Execute the second part of Setup Instance flow of the smoke test suite.
 *
 * @author Leo Arias
 */
@RunWith(Parameterized.class)
public class ADMbb_SetupInstance extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  /** Data to activate the instance online. */
  private ActivateOnlineData activateOnlineData;

  /**
   * Class constructor.
   *
   * @param activateOnlineData
   *          Data to activate the instance online.
   */
  public ADMbb_SetupInstance(ActivateOnlineData activateOnlineData) {
    this.activateOnlineData = activateOnlineData;
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   * @throws IOException
   *
   */
  @Parameters
  public static Collection<Object[]> setupInstanceValues() throws IOException {
    return Arrays
        .asList(
            new Object[][] { { new ActivateOnlineData.Builder()
                .requiredFields(TESTING.purpose(),
                    FileUtils.readFileToString(
                        new File(TestFiles.getRelativeFilePath("SystemKey.txt")), (String) null))
                .build() } });
  }

  /**
   * Test the setup instance flow.
   */
  @Test
  public void instanceShouldBeSetUp() {
    mainPage.verifyCommunityLogo();
    logger.info("** End of test case [ADMb020] Deactivate Instance. **");

    logger.info("** Start of test case [ADMb030] Activate Instance. **");
    InstanceActivation.activateInstanceOnline(mainPage, activateOnlineData);
    // TODO Verify the value in the field Instance Purpose.

  }

  /**
   * Logout from Openbravo.
   */
  @Override
  public void logout() {
    // TODO Selenium2
    // mainPage.quit();
    // if (wasActivated) {
    // if was inactive an alert will appear
    // loginPage.acceptAlertOnLogout();
    // }
    // loginPage.verifyLogout();
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
    OpenbravoERPTest.seleniumStarted = false;
    SeleniumSingleton.INSTANCE.quit();
  }

}
