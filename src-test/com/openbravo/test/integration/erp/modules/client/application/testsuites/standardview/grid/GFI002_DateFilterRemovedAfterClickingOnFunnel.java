/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testsuites.standardview.grid;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.WebElement;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.util.ConfigurationProperties;

@RunWith(Parameterized.class)
public class GFI002_DateFilterRemovedAfterClickingOnFunnel extends OpenbravoERPTest {

  private final AccountData accountHeaderData;
  private final TransactionsData transactionLinesData;

  /**
   * Class constructor.
   */
  public GFI002_DateFilterRemovedAfterClickingOnFunnel(AccountData accountHeaderData,
      TransactionsData transactionLinesData) {
    this.accountHeaderData = accountHeaderData;
    this.transactionLinesData = transactionLinesData;
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> editRecordValues() {
    Object[][] data = new Object[][] { { new AccountData.Builder().name("Spain Bank").build(),
        new TransactionsData.Builder().transactionType("BP Deposit").build() } };
    return Arrays.asList(data);
  }

  /**
   * Test to check that a date filter is not cleared after pressing enter/tab keys.
   */
  @Test
  public void dateFilterIsCleared() {
    mainPage.getNavigationBar()
        .changeProfile(new ProfileData.Builder().role("QA Testing Admin - QA Testing")
            .client("QA Testing")
            .organization("*")
            .warehouse("Spain East warehouse")
            .build());
    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.Transaction transactions = financialAccount.new Transaction(mainPage);
    transactions.createWithoutSaving(transactionLinesData);
    String value = (String) transactions.getSelectorFieldFilterClearedValue("paymentDate");
    assertThat(value, is(equalTo(""))); // The filter value must be cleared
    // Temporal changeset to stabilize code
    WebElement popupCloseButton = SeleniumSingleton.INSTANCE
        .findElementByCssSelector("td.OBFormButton");
    popupCloseButton.click();
    Button buttonCancel = new Button(TestRegistry.getObjectString(
        "org.openbravo.client.application.toolbar.button.undo.23691259D1BD4496BCC5F32645BCA4B9"));
    if (buttonCancel.isEnabled()) {
      buttonCancel.click();
    }
  }
}
