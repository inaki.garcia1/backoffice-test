/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 *  David Miguélez <david.miguelez@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Canvas;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes actions on OpenbravoERP classic pop ups built upon SmartClient user interface library.
 *
 * @author elopio
 *
 */
public class OBClassicPopUp extends Canvas {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Identifier of a specific frame. */
  public static final String FRAME_PROCESS = "process";
  /** Identifier of a specific frame. */
  public static final String FRAME_MAINFRAME = "mainframe";
  private int nRetries = 0;

  /**
   * Format string used to get the close button. The parameter is the string that can be used with
   * SeleniumSingleton.INSTANCE.getEval function to get the object.
   */
  private static final String FORMAT_SMART_CLIENT_GET_CLOSE_BUTTON = "%s.closeButton";

  /** Identifier of the ok button */
  protected static final String BUTTON_OK = "buttonOK";
  /** Identifier of the close button */
  protected static final String BUTTON_CLOSE = "ButtonClose";
  /** Identifier of the MessageBoxTitle */
  protected static final String IDENTIFIER_MESSAGE_BOX_TITLE = "messageBoxIDTitle";

  /* Pop up components. */
  /** The close button. */
  protected Button buttonClose;

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   */
  public OBClassicPopUp(String objectString) {
    super(objectString);
    logger.debug("Opening PopUp: {}", objectString);
    buttonClose = new Button(String.format(FORMAT_SMART_CLIENT_GET_CLOSE_BUTTON, objectString));
  }

  /**
   * Close the pop up.
   */
  public void close() {
    logger.debug("Closing the pop up.");
    buttonClose.click();
  }

  /**
   * Select the window frame.
   */
  public void waitForFrame() {
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.trace("Waiting for the classic frame to load.");
        try {
          Long framesCount;
          framesCount = (Long) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
              "document.getElementsByName('OBClassicPopup_iframe').length");
          logger.debug("Frames count is: {}.", framesCount);
          if (framesCount == 1) {
            logger.debug("Switch to frame.");
            SeleniumSingleton.INSTANCE.switchTo().frame("OBClassicPopup_iframe");
            logger.debug("Frame switched.");
          } else {
            Boolean result = selectedFrameHasNoName();
            logger.debug("Frame with no name loaded, result= {}", result);
            return result;
          }
          selectFrame();
          return true;
        } catch (NoSuchFrameException e) {
          logger.debug("No such frame: ", e::getStackTrace);
          SeleniumSingleton.INSTANCE.switchTo().defaultContent();
          return false;
        } catch (Exception ex) {
          logger.debug("Other exception: {}", ex::toString);
          return false;
        }
      }
    });
  }

  /**
   * @return true if it is possible to switch to "process" and "mainframe" frames. Otherwise, return
   *         false.
   */
  // XXX: This method selectedFrameHasNoName() could be refactored. Moreover, all switchTo() frames
  // infrastructure could be enhanced. But it would imply remove this legacy code and recreate
  // everything from scratch.
  public Boolean selectedFrameHasNoName() {
    logger.trace("Waiting for the classic frame with no name to load.");
    nRetries++;
    Long idx = locateIndexFrameForProcessMainFrame();
    if (idx.equals(-1L)) {
      logger.trace("Frame not found in JS");
      logger.warn(
          "Number of times that frames[i]['process']['mainframe'] has been looked for in this method: "
              + nRetries);
      // TODO L00: Decrease the limit of nRetries allowed
      if (nRetries >= 4) {
        return true;
      }
    }
    // Once we know index (i value), we can continue with the flow
    try {
      SeleniumSingleton.INSTANCE.switchTo().frame(idx.intValue());
      selectFrame();
      return true;
    } catch (NoSuchFrameException exception) {
      logger.debug("SelectFrame for frame {} is not possible.", idx.intValue());
    }
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    logger.debug("Returning false at index {}.", idx.intValue());
    return false;
  }

  /**
   * Select the pop up frame.
   */
  protected void selectFrame() {
    try {
      SeleniumSingleton.INSTANCE.switchTo().frame("process");
      logger.debug("Inside selectFrame() just after switchTo().frame(\"process\").");
      SeleniumSingleton.INSTANCE.switchTo().frame("mainframe");
      logger.debug("Inside selectFrame() just after switchTo().frame(\"mainframe\").");
    } catch (Exception e) {
      logger.debug("Inside selectFrame() the exception {} has been captured. Just continue.",
          e.getClass()::getSimpleName);

    }
  }

  /**
   * Checking in all frames, seeking for frames[i]['process']['mainframe'] Once it has been found,
   * it is used [i] as index
   */
  private Long locateIndexFrameForProcessMainFrame() {
    // @formatter:off
    Long idx = (Long)SeleniumSingleton.INSTANCE.executeScriptWithReturn(
          "(function (frameName) {\n"
        + "  for (let i = 0; i < frames.length; i++) {\n"
        + "    try {\n"
        + "      let f = frames[i]\n"
        + "      if (f && f[frameName]['" + FRAME_MAINFRAME + "']) {\n"
        + "        return i\n"
        + "      }\n"
        + "    } catch {}\n"
        + "  }\n"
        + "  return -1\n"
        + "})('" + FRAME_PROCESS + "')");
    // @formatter:on
    logger.trace("Classic frame index {}", idx);
    return idx;
  }

  /**
   * Select a PopUp with a web element. This function waits until the element is present and
   * visible.
   *
   * @param webElementId
   *          The ID of the PopUp that is wanted to be selected
   */
  public void selectPopUp(final String webElementId) {
    logger.debug("Selecting the pop up.");
    waitForFrame();
    Sleep.setExplicitWait(90000).until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting for the pop up to be loaded.");
        try {
          final WebElement webElement = SeleniumSingleton.INSTANCE.findElementById(webElementId);
          return webElement.isDisplayed();
        } catch (final NoSuchElementException exception) {
          return false;
        }
      }
    });
  }

  /**
   * Verifies that all the elements that should appear in the window are present and visible.
   *
   * @param webElementName
   *          An array of strings containing the names of the web elements to verify
   */
  public void verifyElementsAreVisible(final String[] webElementName) {
    // FIXME: Adds a trySleep to prevent false positives
    Sleep.trySleep();
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting for all the elements in the window to load.");
        try {
          Boolean expectedCondition = true;
          for (int i = 0; i < webElementName.length; i++) {
            logger.trace("Checking for element {}", webElementName[i]);
            WebElement webElement = SeleniumSingleton.INSTANCE.findElementByName(webElementName[i]);
            logger.trace("Checked as {} for element {}", webElement.isDisplayed(),
                webElementName[i]);
            expectedCondition = expectedCondition && webElement.isDisplayed();
          }
          return expectedCondition;
        } catch (final NoSuchElementException exception) {
          return false;
        }
      }
    });
  }

  /**
   * Select a Pop Up with ok button. This function waits until the OK button is present and visible.
   */
  public void selectPopUpWithOKButton() {
    selectPopUp(BUTTON_OK);
  }

  /**
   * Click OK button.
   */
  public void clickOK() {
    logger.debug("Clicking the OK button.");
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_OK).click();
    logger.trace("OK button has been clicked.");
  }

  /**
   * Click close button.
   */
  public void clickClose() {
    logger.debug("Clicking the Close button.");
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_CLOSE).click();
    Sleep.trySleep();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
  }

  /**
   * Assert the process has been completed succesfully
   */
  public void assertProcessCompletedSuccessfully() {
    selectPopUp(IDENTIFIER_MESSAGE_BOX_TITLE);
    assertThat(SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_MESSAGE_BOX_TITLE).getText(),
        is(equalTo("Process completed successfully")));
  }
}
