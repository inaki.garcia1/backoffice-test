/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Lorenzo Fidalgo<lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.financialaccount;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;

import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

public class ReconciliationPopUp extends OBClassicPopUp {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Component identifiers. */
  /** The identifier of the end balance text field. */
  private static final String IDENTIFIER_TEXT_FIELD_END_BALANCE = "paramEndBalance";
  /** Identifier of the hide transactions after statement date check box. */
  private static final String IDENTIFIER_CHECK_BOX_HIDE_TRANSACTIONS_AFTER_STATEMENT_DATE = "paramAfterDate";
  /** Identifier of the clear all transactions check box. */
  private static final String IDENTIFIER_CHECK_BOX_CLEAR_ALL = "paramClearAll";

  /** The identifier of the process button. */
  private static final String IDENTIFIER_BUTTON_PROCESS = "buttonProcess";

  /**
   * Formatting string of the xpath to locate a checkbox in the table, using the description. The
   * parameter is the document description.
   */
  private static final String FORMAT_XPATH_CHECK_BOX_BY_DOCUMENT_DESCRIPTION = "//div[@id = 'client2']//td[@class = 'DataGrid_Body_Cell' and contains(text(), '%s')]/..//input[@type = 'checkbox']";

  /**
   * Class constructor. *
   */
  public ReconciliationPopUp() {
    super(TestRegistry.getObjectString(
        "org.openbravo.classicpopup./org.openbravo.advpaymentmngt.ad_actionbutton/Reconciliation.html"));
  }

  /**
   * Select the pop up frame.
   */
  @Override
  protected void selectFrame() {
    SeleniumSingleton.INSTANCE.switchTo().frame("process");
  }

  /**
   * Select the PopUp.
   *
   * This function waits until the process button is present and visible.
   *
   */
  public void selectPopUp() {
    selectPopUp(IDENTIFIER_BUTTON_PROCESS);
  }

  /**
   * Process a reconciliation.
   *
   * @param endingBalance
   *          The reconciliation ending balance.
   * @param hideTransactionsAfterStatementDate
   *          Indicates if the transactions after statement date should be hidden.
   * @param description
   *          The description of the transaction to clear.
   */
  public void reconcile(String endingBalance, boolean hideTransactionsAfterStatementDate,
      String description) {
    selectPopUp();
    double updatedBalance;
    logger.debug("Beginning Balance: {}",
        SeleniumSingleton.INSTANCE.findElementByName("inpBeginBalance").getAttribute("value"));
    logger.debug("Ending Balance: {}", endingBalance);
    updatedBalance = java.lang.Double.parseDouble(
        SeleniumSingleton.INSTANCE.findElementByName("inpBeginBalance").getAttribute("value"));
    updatedBalance = updatedBalance + java.lang.Double.parseDouble(endingBalance);
    logger.debug("Updated Balance: {}", updatedBalance);

    SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_TEXT_FIELD_END_BALANCE)
        .sendKeys(Keys.END);
    // FIXME: This solution should be improved by finding proper while condition
    // while
    // (SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_TEXT_FIELD_END_BALANCE).getAttribute(Attribute.VALUE.getName())!="")
    // {
    for (int i = 0; i < 10; i++) { // it will fail if content is longer than 10 chars
      SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_TEXT_FIELD_END_BALANCE)
          .sendKeys(Keys.BACK_SPACE);
    }
    // FIXME: <chromedriver> implementing sendKeys alternative/workaround
    if (!SeleniumSingleton.runsChrome()) {
      SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_TEXT_FIELD_END_BALANCE)
          .sendKeys(String.valueOf(updatedBalance));
    } else {
      String scrToExec = String.format("document.getElementById('%s')",
          IDENTIFIER_TEXT_FIELD_END_BALANCE);
      if (!String.valueOf(updatedBalance).matches(".*[56/].*")) {
        logger.debug("[chromedriver] Sending text with the Selenium API sendKeys(), default way");
        SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_TEXT_FIELD_END_BALANCE)
            .sendKeys(String.valueOf(updatedBalance));
      } else {
        logger.debug(
            "[chromedriver] The text to be sent contains characters which could cause undesirable actions when parsed by sendKeys() method in Chrome");
        logger
            .debug("[chromedriver] Attempting to send text in the alternate way for ChromeDriver");

        SeleniumSingleton.INSTANCE.executeScriptWithReturn(
            scrToExec + ".value = '" + String.valueOf(updatedBalance) + "';");
      }
    }
    logger.debug(
        "Sleeping to let the application to click 'Hide transactions after statement date' correctly");
    Sleep.trySleep(4000);
    if (SeleniumSingleton.INSTANCE
        .findElementById(IDENTIFIER_CHECK_BOX_HIDE_TRANSACTIONS_AFTER_STATEMENT_DATE)
        .isSelected() != hideTransactionsAfterStatementDate) {
      SeleniumSingleton.INSTANCE
          .findElementById(IDENTIFIER_CHECK_BOX_HIDE_TRANSACTIONS_AFTER_STATEMENT_DATE)
          .click();
      Sleep.trySleep();
    }
    // Clicks twice the clear all button to deselect all the values
    SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_CHECK_BOX_CLEAR_ALL).click();
    Sleep.trySleep();
    SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_CHECK_BOX_CLEAR_ALL).click();
    Sleep.trySleep();

    String transactionXpath = String.format(FORMAT_XPATH_CHECK_BOX_BY_DOCUMENT_DESCRIPTION,
        description);
    SeleniumSingleton.INSTANCE.findElementByXPath(transactionXpath).click();
    Sleep.trySleep();
    logger.debug("Clicking reconcile button");
    SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_BUTTON_PROCESS).click();
  }
}
