/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui;

/**
 * Helper class for handling the test registry, that's an array with references to SmartClient GUI
 * objects.
 *
 * @author elopio
 *
 */
public class TestRegistry {

  /**
   * Formatting string to use the Openbravo components registry to get a SmartClient GUI object. The
   * parameter is the registry key of the of the object in the test registry array.
   */
  private final static String FORMAT_REGISTRY_OBJECT_STRING = "dom=window.OB.TestRegistry.registry['%s']";

  /**
   * Get the object string of a SmartClient GUI component.
   *
   * The object string is a JavaScript expression that can be evaluated to get a reference to the
   * SmartClient GUI object.
   *
   * @param registryKey
   *          The key of the object in the test registry array.
   * @return the object string of the SmartClient GUI component identified by the key.
   */
  public static String getObjectString(String registryKey) {
    return String.format(FORMAT_REGISTRY_OBJECT_STRING, registryKey);
  }

}
