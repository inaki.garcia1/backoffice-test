/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.image.PixelGrabber;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Executes actions on OpenbravoERP images built upon SmartClient user interface library.
 *
 * @author elopio
 *
 */
public class Image extends Canvas {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Formatting string used to get the src of the image. The parameter is the object string. */
  private static final String FORMAT_SMART_CLIENT_GET_SRC = "%s.src";

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   */
  public Image(String objectString) {
    super(objectString);
  }

  /**
   * Get the source of the image.
   *
   * @return The source of the image.
   */
  public String getImageSource() {
    final String result = ConfigurationProperties.INSTANCE.getOpenbravoBaseURL()
        + SeleniumSingleton.INSTANCE
            .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_SRC, objectString));
    return result;
  }

  /**
   * Checks if current image matches the file.
   *
   * @param pathToFile
   *          The path with the image to compare.
   */
  public void verifyImage(String pathToFile) {
    try {
      assertTrue("The image at " + getImageSource() + " should match the image at " + pathToFile,
          equals(pathToFile));
    } catch (Exception exception) {
      exception.printStackTrace();
      fail(exception.getMessage());
    }
  }

  /**
   * Return true if current image matches the file.
   *
   * @param pathToFile
   *          The path with the image to compare.
   * @return true if the image is equal in a byte to byte basis with the file.
   */
  public Boolean equals(String pathToFile) {
    java.awt.Image imageWeb = null;
    java.awt.Image imageFile = null;

    // Read from a file
    // Example pathToFile="C:\\selenium\\prototype\\testfiles\\ShowImageLogo.png"
    logger.trace("Reading image file at {}", pathToFile);
    final File file = new File(pathToFile);
    try {
      imageFile = ImageIO.read(file);
    } catch (IOException exception) {
      fail(exception.getMessage());
    }

    // Read from a URL
    URL url = null;
    try {
      url = new URL(this.getImageSource());
    } catch (MalformedURLException exception) {
      fail(exception.getMessage());
    }
    try {
      imageWeb = ImageIO.read(url);
    } catch (IOException exception) {
      fail(exception.getMessage());
    }

    final PixelGrabber grab1 = new PixelGrabber(imageWeb, 0, 0, -1, -1, false);
    final PixelGrabber grab2 = new PixelGrabber(imageFile, 0, 0, -1, -1, false);

    if (grab1.getPixels() instanceof byte[]) {
      if (!(grab2.getPixels() instanceof byte[])) {
        return false;
      } else {
        byte[] data1 = null;
        try {
          if (grab1.grabPixels()) {
            final int width = grab1.getWidth();
            final int height = grab1.getHeight();
            data1 = new byte[width * height];

            data1 = (byte[]) grab1.getPixels();
          }
        } catch (InterruptedException exception) {
          fail(exception.getMessage());
        }

        byte[] data2 = null;
        try {
          if (grab2.grabPixels()) {
            final int width = grab2.getWidth();
            final int height = grab2.getHeight();
            data2 = new byte[width * height];
            data2 = (byte[]) grab2.getPixels();
          }
        } catch (InterruptedException exception) {
          fail(exception.getMessage());
        }
        return java.util.Arrays.equals(data1, data2);
      }
    } else {
      if (grab2.getPixels() instanceof byte[]) {
        return false;
      } else {
        int[] data1 = null;
        try {
          if (grab1.grabPixels()) {
            final int width = grab1.getWidth();
            final int height = grab1.getHeight();
            data1 = new int[width * height];
            data1 = (int[]) grab1.getPixels();
          }
        } catch (InterruptedException exception) {
          fail(exception.getMessage());
        }

        int[] data2 = null;
        try {
          if (grab2.grabPixels()) {
            final int width = grab2.getWidth();
            final int height = grab2.getHeight();
            data2 = new int[width * height];
            data2 = (int[]) grab2.getPixels();
          }
        } catch (InterruptedException exception) {
          fail(exception.getMessage());
        }
        return java.util.Arrays.equals(data1, data2);
      }
    }

  }
}
