/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *      Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.accounting.transactions.opencloseperiodcontrol;

import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;

import com.openbravo.test.integration.erp.data.financial.accounting.transactions.opencloseperiodcontrol.OpenClosePeriodControlData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.OpenPeriodsPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on OpenbravoERP Fiscal Calendar tab.
 *
 * @author elopio
 *
 */
public class OpenClosePeriodControlTab extends GeneratedTab<OpenClosePeriodControlData> {

  /** The tab title. */
  static final String TITLE = "Open/Close Period Control";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  static final String IDENTIFIER = "E429CB8F5CDF4D8395B76D553E72A2D0";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /**
   * Class constructor
   *
   */
  public OpenClosePeriodControlTab() {
    super(TITLE, IDENTIFIER);
  }

  /**
   * Open or close all period documents.
   */
  public void openCloseAll() throws OpenbravoERPTestException {
    // Button buttonCreatePeriods = new Button(
    // TestRegistry
    // .getObjectString("org.openbravo.client.application.toolbar.button.processed.4DE17FC05C3843C4959166BB67967486"));
    standardView.clearFilters();
    Sleep.trySleep(1000);
    standardView.selectAllRecordsOneCheckboxOnGrid();
    Button buttonCreatePeriods = new Button(TestRegistry.getObjectString(
        "org.openbravo.client.application.toolbar.button.openClose.E429CB8F5CDF4D8395B76D553E72A2D0"));

    buttonCreatePeriods.click();

    OpenPeriodsPopUp createPeriods = new OpenPeriodsPopUp(
        TestRegistry.getObjectString("org.openbravo.client.application.OpenClosePeriod.popup"));
    createPeriods.selectPopUp();
    try {
      createPeriods.select("Open Period");
    } catch (StaleElementReferenceException see) {
      Sleep.trySleep();
      createPeriods.select("Open Period");
    }
    createPeriods.clickOK();
    SeleniumSingleton.INSTANCE.switchTo().window("");
    waitUntilMessageVisible();
  }

  /**
   * Open all period records. All of them are selected
   */
  public void openAllPeriods() throws OpenbravoERPTestException {
    // All periods
    standardView.clearFilters();
    standardView.filter(new OpenClosePeriodControlData.Builder()
        .status("All Closed or All Never Opened or All Opened or All Permanently Closed or Mixed")
        .startingDate("Since 01-01-2001")
        .endingDate("Before 06-02-2119")
        .build());
    SeleniumSingleton.INSTANCE.switchTo().activeElement().sendKeys(Keys.TAB);
    standardView.selectAllRecordsOneCheckboxOnGrid();
    Button buttonCreatePeriods = new Button(TestRegistry.getObjectString(
        "org.openbravo.client.application.toolbar.button.openClose.E429CB8F5CDF4D8395B76D553E72A2D0"));
    Sleep.trySleep();
    buttonCreatePeriods.click();
    OpenPeriodsPopUp createPeriods = new OpenPeriodsPopUp(
        TestRegistry.getObjectString("org.openbravo.client.application.OpenClosePeriod.popup"));
    createPeriods.selectPopUp();
    try {
      createPeriods.select("Open Period");
    } catch (StaleElementReferenceException see) {
      Sleep.trySleep();
      createPeriods.select("Open Period");
    }
    createPeriods.clickOK();
    SeleniumSingleton.INSTANCE.switchTo().window("");
    waitUntilMessageVisible();
  }

}
