/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.enterprise;

import com.openbravo.test.integration.erp.data.generalsetup.enterprise.organization.InformationData;
import com.openbravo.test.integration.erp.data.generalsetup.enterprise.organization.OrganizationData;
import com.openbravo.test.integration.erp.data.warehouse.setup.warehouseandstoragebins.WarehouseData;
import com.openbravo.test.integration.erp.gui.general.enterprise.organization.InformationTab;
import com.openbravo.test.integration.erp.gui.general.enterprise.organization.OrganizationTab;
import com.openbravo.test.integration.erp.gui.general.enterprise.organization.OrganizationWindow;
import com.openbravo.test.integration.erp.gui.general.enterprise.organization.WarehouseTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;

/**
 * Executes and verifies actions on the Organization window.
 *
 * @author elopio
 */
public class Organization extends SmokeTabScript<OrganizationData, OrganizationTab>
    implements SmokeWindowScript<OrganizationWindow> {

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public Organization(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Open the Organization window.
   *
   * @return the opened Sales Order window.
   */
  @Override
  public Organization open() {
    OrganizationWindow window = new OrganizationWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the Organization tab.
   *
   * Before this, the Organization window must be opened.
   *
   * @return the Header tab GUI object.
   */
  @Override
  protected OrganizationTab selectTab() {
    OrganizationWindow window = (OrganizationWindow) mainPage.getView(OrganizationWindow.TITLE);
    mainPage.getTabSetMain().selectTab(OrganizationWindow.TITLE);
    OrganizationTab tab = (OrganizationTab) window.getTopLevelTab();
    return tab;
  }

  /**
   * Set the organization as ready.
   */
  public void setAsReady() {
    OrganizationTab tab = selectTab();
    tab.setAsReady();
  }

  /**
   * Executes and verifies actions on the Organization Information tab.
   *
   * @author aferraz
   *
   */
  public class Information extends SmokeTabScript<InformationData, InformationTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Information(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Information tab.
     *
     * Before this, the Information window must be opened.
     *
     * @return the Information tab GUI object.
     */
    @Override
    protected InformationTab selectTab() {
      OrganizationWindow window = (OrganizationWindow) mainPage.getView(OrganizationWindow.TITLE);
      InformationTab tab = (InformationTab) window.selectTab(InformationTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }

  }

  /**
   * Executes and verifies actions on the Organization Warehouse tab.
   *
   * @author rqueralta
   *
   */
  public class Warehouse extends SmokeTabScript<WarehouseData, WarehouseTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Warehouse(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Warehouse tab.
     *
     * Before this, the Warehouse window must be opened.
     *
     * @return the Warehouse tab GUI object.
     */
    @Override
    protected WarehouseTab selectTab() {
      OrganizationWindow window = (OrganizationWindow) mainPage.getView(OrganizationWindow.TITLE);
      WarehouseTab tab = (WarehouseTab) window.selectTab(WarehouseTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }

    public void deleteOnGrid() {
      WarehouseTab tab = selectTab();
      tab.deleteOnGrid();
    }

  }

}
