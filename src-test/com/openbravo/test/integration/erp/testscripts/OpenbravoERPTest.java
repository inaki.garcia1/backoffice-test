/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.MethodRule;
import org.junit.rules.TestName;
import org.junit.rules.TestWatchman;
import org.junit.runners.model.FrameworkMethod;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.ScreenshotException;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.gui.LoginPage;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.videorecording.VideoRecord;
import com.openbravo.test.integration.selenium.AlertHandler;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Test the Openbravo ERP.
 *
 * @author elopio
 *
 */
public class OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The path to the properties file. */

  /**
   * Data to the application login. Default values are set to "test.user" and "test.password"
   * properties on the configuration file.
   */
  protected LogInData logInData = new LogInData.Builder()
      .userName(ConfigurationProperties.INSTANCE.getUserName())
      .password(ConfigurationProperties.INSTANCE.getPassword())
      .build();

  private static String previouslyLoggedUsername;

  /** The main page of the application. */
  public static MainPage mainPage;

  public static boolean seleniumStarted;
  private boolean invalidCredentials = false;

  /**
   * Used to force the login when it is considered at the beginning of the tests in the startTest
   * method.
   */
  private static boolean forceLogin;

  /** VIDEO_RECORD is got from OpenbravoERPTest.properties file (test.videorecord property) */
  static char VIDEO_RECORD = ConfigurationProperties.INSTANCE.getVideoRecord();

  /**
   * Rule to made available the name of the test.
   */
  @Rule
  public TestName name = new TestName();

  /**
   * Rule to intercept different sections of code execution.
   *
   * When the test is about to start, start selenium session. When the test has finished, close
   * selenium session. In case of failure, take a screenshot.
   */
  // TestWatchman is now deprecated. It should be updated to TestWatcher
  // When code is updated to TestWatcher, it will be possible to use getClassName() to know the
  // class name instead of the method where @Test is invoked
  // http://junit.org/junit4/javadoc/latest/org/junit/runner/Description.html#getClassName()
  @Rule
  public MethodRule watchman = new TestWatchman() {
    // Initialization to be able to record video
    VideoRecord videoRecord = new VideoRecord();

    /**
     * Prepare the browser for the execution.
     */
    @Override
    public void starting(FrameworkMethod method) {
      logger.info("**  Starting @Test: {}  **", method.getName());
      if (!seleniumStarted) {
        try {
          logger.info("Selenium server will be started");
          SeleniumSingleton.start();
        } catch (final WebDriverException exception) {
          logger.trace(exception::getMessage);
          logger.info("Waiting for retrying selenium.start.");
          Sleep.trySleep(120000);
          logger.info("Retrying selenium.start by catching the exception.");
          SeleniumSingleton.start();
        }
        seleniumStarted = true;
      }
      // If video record preference is set to "t", we set video record name and file directory
      // and, after that, video recording process is started.
      if (VIDEO_RECORD == 't') {
        try {
          videoRecordSettingPath(method, videoRecord);
          videoRecord.startRecording();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }

    /**
     * Invoked when a test method succeeds
     */
    @Override
    public void succeeded(FrameworkMethod method) {
      closePopups();
      loginIfLoginPageOpen();
      if (mainPage != null && seleniumStarted != false && forceLogin != true) {
        try {
          mainPage.closeAllViews();
        } catch (WebDriverException wde) {
          logger.error("** It was not possible to closeAllViews() **");
          wde.printStackTrace();
        }
      }
      if (VIDEO_RECORD == 't') {
        // And remove that video, due to test finished successfully and therefore it is not
        // necessary
        stopAndDeleteVideo(videoRecord);
      }
    }

    /**
     * Take the screenshot.
     */
    @Override
    public void failed(Throwable e, FrameworkMethod method) {
      logger.error("An error occurred in {}: {}", method.getName(), e.getMessage(), e);
      try {
        if (VIDEO_RECORD == 't') {
          logger.info("Stop recording and keep the video record due to unsuccessful execution.");
          videoRecord.stopRecording();
        }
        takeAndSaveScreenshot(e);
      } catch (final Throwable t) {
        logger.error("Error on the failure logging: {}", t::getMessage);
      }
      if (seleniumStarted) {
        logger.error("** The test has finished with 'Failed' status. Closing Selenium Instance **");
        mainPage = null;
        seleniumStarted = false;
        SeleniumSingleton.INSTANCE.quit();
      }

    }

    /**
     * Close the browser and selenium session.
     */
    @Override
    public void finished(FrameworkMethod method) {
      // try {
      // logout();
      // } catch (Throwable throwable) {
      // logger.error("Error while logging out: {}", throwable.getMessage());
      // }
      if (mainPage != null) {
        mainPage.clearStoredViews();
      }
      try {
        // SeleniumSingleton.INSTANCE.quit();
      } catch (Throwable exception) {
        logger.error("Error closing selenium: {}", exception::getMessage);
      }
      logger.info("**  End of test execution @Test: {}  **", method::getName);
    }
  };

  /**
   * Login to Openbravo.
   */
  @Before
  public void startTest() {
    boolean loginRequired = mainPage == null
        || !previouslyLoggedUsername.equals(logInData.getUserName()) || forceLogin;
    if (loginRequired) {
      login();
      forceLogin = false;
    } else {
      logger.debug("Already logged in. No login required.");
    }
  }

  public void login() {
    SeleniumSingleton.toggleImplicitWait();
    logoutIfLoggedIn();
    final LoginPage loginPage = new LoginPage();
    loginPage.open();
    loginPage.logIn(logInData);
    validCredentialsVerification();
    if (!invalidCredentials) {
      testEnvironmentVerification();
    }
    mainPage = new MainPage();
    mainPage.assertLogin(logInData.getUserName());
    previouslyLoggedUsername = logInData.getUserName();
    SeleniumSingleton.toggleImplicitWait();
  }

  /**
   * Logout from Openbravo.
   */
  public void logout() {
    mainPage.quit();
    final LoginPage loginPage = new LoginPage();
    loginPage.verifyLogout();
  }

  /**
   * It is checked if there is a PopUp with its close button with id 'ButtonClose' and if it found,
   * the PopUp is closed.
   */
  public static void closePopups() {
    if ((Boolean) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn("document.getElementById('ButtonClose') != null")) {
      try {
        WebElement popupCloseButton = SeleniumSingleton.INSTANCE.findElementById("ButtonClose");
        popupCloseButton.click();
      } catch (NoSuchElementException nsee) {
        logger.info("The order \"findElementById('ButtonClose')\" has not found that element");
      }
    }
    // The following commented methods can be modified to use them when certain popup appears at the
    // end of a test case and it is required to close it
    /*
     * else if ((Boolean) SeleniumSingleton.INSTANCE
     * .executeScriptWithReturn("document.getElementsByClassName('OBPopup')[0] != undefined")) { try
     * { WebElement popupCloseButton = SeleniumSingleton.INSTANCE
     * .findElementByCssSelector("td.OBFormButton"); popupCloseButton.click(); } catch
     * (NoSuchElementException nsee) {
     * logger.info("The order \"findElementById('ButtonClose')\" has not found that element"); } //
     * It is possible that some record process has not finished due to some "withOutSaving" method
     * // has been executed. So cancel button has to be clicked. Button buttonCancel = new Button(
     * TestRegistry .getObjectString(
     * "org.openbravo.client.application.toolbar.button.undo.23691259D1BD4496BCC5F32645BCA4B9")); if
     * (buttonCancel.isEnabled()) { buttonCancel.click(); }
     *
     * }
     */
    else {
      // It is supposed PurposePopup can only appear in cases where the other PopUp does not appear
      if ((Boolean) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
          "document.getElementsByClassName('OBPopupBody')[0] != undefined")) {
        mainPage.closeInstancePurposePopUp();
      }
    }
  }

  /**
   * It is checked if LoginPage is open. If so, then login();
   */
  private void loginIfLoginPageOpen() {
    try {
      // to be removed: look for the user filed inside the appFrame if the login page still has it
      String openedLoginPageCheck;
      if (!LoginPage.upgraded) {
        openedLoginPageCheck = "window.frames['appFrame'].document.getElementById('user') != 'undefined' && window.frames['appFrame'].document.getElementById('user') != null";
      } else {
        openedLoginPageCheck = "document.getElementById('user') != 'undefined' && document.getElementById('user') != null";
      }
      if ((Boolean) SeleniumSingleton.INSTANCE.executeScriptWithReturn(openedLoginPageCheck)) {
        logger.debug("It is open LoginPage. Loggin in...");
        login();
      }
    } catch (WebDriverException wde) {
      logger.debug("It is not open LoginPage");
    }
  }

  /**
   * It is checked if logged in Openbravo searching Logout button. If so, then logout();
   */
  private void logoutIfLoggedIn() {
    try {
      if ((Boolean) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
          "window.OB.TestRegistry.registry['org.openbravo.client.application.navigationbarcomponents.QuitButton'] != 'undefined' && window.OB.TestRegistry.registry['org.openbravo.client.application.navigationbarcomponents.QuitButton'] != null")) {
        logout();
      }
    } catch (WebDriverException wde) {
      logger.debug("It is not required to logout. Not logged in");
    }
  }

  /**
   * Here we check if OB.Application.testEnvironment is true (test.environment=true property)
   */
  public void testEnvironmentVerification() {
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    Sleep.smartWaitExecuteScript("document.getElementsByClassName('OBNavBarTextButton')[0] != null",
        1750);
    boolean testEnvironmentActivated = false;
    try {
      testEnvironmentActivated = (Boolean) SeleniumSingleton.INSTANCE
          .executeScriptWithReturn("OB.Application.testEnvironment");
    } catch (WebDriverException wde) {
      if (wde.getMessage().contains("OB is not defined")) {
        logger.error("OB is not defined");
        logger.error(
            "If OB has not been found, it could be a performance issue due to ERP has took too much time to log in.");
      }
    }
    assertTrue(
        "Check credentials were properly introduced. If that is ok, it seems'test.environment' setting from Openbravo.properties file is not set as 'true'. Activate it to get tests working properly",
        testEnvironmentActivated);
  }

  /**
   * Here we check if introduced credentials are valid. In case they are invalid, warn it.
   */
  public void validCredentialsVerification() {
    try {
      String credentialsText = SeleniumSingleton.INSTANCE.findElementById("errorMsgTitle")
          .getAttribute("textContent");
      if (credentialsText.equals("Invalid user name or password.")) {
        invalidCredentials = true;
        logger.error("Credentials are not correct: INVALID USER NAME OR PASSWORD.");
      } else {
        invalidCredentials = false;
        logger.debug("Credentials are correct");
      }
    } catch (NoSuchElementException nsee) {
      if (nsee.toString()
          .contains(
              "Unable to locate element: {\"method\":\"id\",\"selector\":\"errorMsgTitle\"}")) {
        invalidCredentials = false;
        logger.debug("Credentials are valid");
      }
    } catch (StaleElementReferenceException sere) {
      if (sere.toString().contains("Element not found in the cache")) {
        invalidCredentials = false;
        logger.debug("Credentials are valid");
      }

    }
  }

  /**
   * Set the path of the test video file
   *
   * @param method
   *          Represents a method on a test class to be invoked at the appropriate point in test
   *          execution. These methods are usually marked with an annotation (such
   *          as @Test, @Before, @After, @BeforeClass, @AfterClass, etc.)
   * @param videoRecord
   *          The videoRecord object that will record the test
   */
  private void videoRecordSettingPath(FrameworkMethod method, VideoRecord videoRecord) {
    int indexLastDot = method.getMethod().toString().lastIndexOf(".");
    int indexPenultimateDot = method.getMethod().toString().lastIndexOf(".", indexLastDot - 1);
    videoRecord.setName(method.getMethod().toString().substring(indexPenultimateDot + 1));
    videoRecord.setFile(new File(
        "last-results" + File.separator + ConfigurationProperties.INSTANCE.getOpenbravoContext()
            + File.separator + "videoRecords"));
  }

  /**
   * videoRecord will be stopped and that file will be deleted
   *
   * @param videoRecord
   *          The videoRecord object that will record the test
   */
  private void stopAndDeleteVideo(VideoRecord videoRecord) {
    try {
      logger.info("Stop video recording and delete created record due to successful execution.");
      videoRecord.stopRecording();
      DeleteTestVideoFile(videoRecord);
    } catch (Throwable exception) {
      logger.error("Error closing selenium: {}", exception::getMessage);
    }
  }

  /**
   * Delete the file where the test video is being recorded
   *
   * @param videoRecord
   *          The videoRecord object that will record the test.
   */
  private void DeleteTestVideoFile(VideoRecord videoRecord) {
    Path testRecordPath = Paths.get(videoRecord.screenRecorder.getsSRecordFile().getPath());
    try {
      Files.delete(testRecordPath);
    } catch (IOException e) {
      logger.error("Error deleting video record: {}", e::getMessage);
    }
  }

  /**
   * Take the screenshot of the test failure and save it (write it) in the certain path
   *
   * @param e
   *          The possible exception
   */
  private void takeAndSaveScreenshot(Throwable e) {
    // Take a screenshot.
    Throwable cause = e.getCause();
    String screenShot;
    if (cause instanceof ScreenshotException) {
      screenShot = ((ScreenshotException) cause).getBase64EncodedScreenshot();
    } else {
      screenShot = ((TakesScreenshot) SeleniumSingleton.INSTANCE)
          .getScreenshotAs(OutputType.BASE64);
    }
    logger.info("Save the screenshot.");
    File screenshotPath = new File(
        "last-results" + File.separator + ConfigurationProperties.INSTANCE.getOpenbravoContext()
            + File.separator + "screenshots" + File.separator + name.getMethodName() + ".png");
    // If the directory of the file does not exist, create it.
    if (!screenshotPath.getParentFile().exists()) {
      screenshotPath.getParentFile().mkdirs();
    }
    try {
      FileOutputStream fileOutputStream = new FileOutputStream(screenshotPath);
      byte[] imgBytes = Base64.getDecoder().decode(screenShot);
      fileOutputStream.write(imgBytes);
      fileOutputStream.close();
    } catch (IOException ioe) {
      logger.error("There have been errors saving the screenshot of the test failure.");
    }
  }

  /**
   * This method checks if the alert warning about the browser is not supported has appeared and, if
   * so, that alert is closed.
   *
   * This method can be used as a workaround needed while using not supported firefox
   */
  @SuppressWarnings("unused")
  private void closeBrowserNotSupportedAlert() {
    if (AlertHandler.isAlertPresent()) {
      if (AlertHandler.getAlert().contains("Your browser is not officially supported.")) {
        AlertHandler.acceptAlert();
      }
    }
  }

  // This method can be used to set the right loggedUsername when the userName is different (has
  // been changed) from the LogInData in the collection parameters
  public static void setPreviouslyLoggedUsername(String loggedUsername) {
    previouslyLoggedUsername = loggedUsername;
  }

  /**
   * foceLogin variable is set to true. Used to force the login when it is considered at the
   * beginning of the tests in the startTest() method.
   */
  public static void forceLoginRequired() {
    forceLogin = true;
  }
}
