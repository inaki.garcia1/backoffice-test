/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.sales.transactions.goodsshipment;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentHeaderData;
import com.openbravo.test.integration.erp.gui.popups.RequestProcessInvoicePopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on OpenbravoERP Header tab.
 *
 * @author elopio
 *
 */
public class GoodsShipmentHeaderTab extends GeneratedTab<GoodsShipmentHeaderData> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The tab title. */
  static final String TITLE = "Header";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "257";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /** Registry key of the complete button. */
  private static final String REGISTRY_KEY_BUTTON_COMPLETE = "org.openbravo.client.application.toolbar.button.processGoodsJava.257";
  /** Registry key of the create lines from button. */
  private static final String REGISTRY_KEY_BUTTON_CREATE_LINES_FROM = "org.openbravo.client.application.toolbar.button.createLinesFrom.257";

  /* Toolbar buttons. */
  /** The complete button. */
  private static Button buttonComplete;
  /** The create lines from button. */
  private static Button buttonCreateLinesFrom;

  /**
   * Class constructor.
   *
   */
  public GoodsShipmentHeaderTab() {
    super(TITLE, IDENTIFIER);
    addChildTab(new GoodsShipmentLinesTab(this));
    buttonComplete = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_COMPLETE));
    buttonCreateLinesFrom = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_CREATE_LINES_FROM));
  }

  /**
   * Create goods shipment lines from order.
   *
   * @param warehouseAlias
   *          The alias of the warehouse.
   * @param order
   *          The order data. This is a string with the form: number - date - amount.
   */
  public void createLinesFrom(String warehouseAlias, String order) {
    createLinesFrom(warehouseAlias, order, 0);
  }

  /**
   * Create goods shipment lines from order.
   *
   * @param warehouseAlias
   *          The alias of the warehouse.
   * @param order
   *          The order data. This is a string with the form: number - date - amount.
   * @param lineNo
   *          The order line number to select or 0 to select all
   */
  public void createLinesFrom(String warehouseAlias, String order, int lineNo) {
    logger.debug("Creating lines from order '{}'.", order);
    buttonCreateLinesFrom.click();
    CreateLinesFromPopUp popUp = new CreateLinesFromPopUp();
    popUp.createLinesFrom(warehouseAlias, order, lineNo);
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
    waitIfInfoMessageVisible();
  }

  /**
   * Process the shipment.
   *
   * @param action
   *          The action to process.
   */
  private void process(String action) {
    logger.debug("Process goods shipment header with action '{}'.", action);
    buttonComplete.smartClientClick();
    RequestProcessInvoicePopUp popUp = new RequestProcessInvoicePopUp(
        "org.openbravo.classicpopup./org.openbravo.erpCommon.ad_actionButton/ProcessGoods.html");
    popUp.process(action);
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
    waitIfInfoMessageVisible();
  }

  /**
   * Complete the shipment.
   *
   */
  public void complete() {
    process("Complete");
    // TODO L1: Check after stabilization if following sleeps is still required
    Sleep.trySleep();
  }

  /**
   * Complete the shipment.
   *
   */
  public void close() {
    process("Void");
  }
}
