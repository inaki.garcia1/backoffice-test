/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ReconciliationsData
 *
 * @author plujan
 *
 */
public class ReconciliationsData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the unrecNo value
     *
     * Description: Unrec_No
     *
     * @param value
     *          The unrecNo value.
     * @return The builder for this class.
     */
    public Builder unrecNo(String value) {
      this.dataFields.put("unrecNo", value);
      return this;
    }

    /**
     * Set the unrecAmt value
     *
     * Description: Unrec_Amt
     *
     * @param value
     *          The unrecAmt value.
     * @return The builder for this class.
     */
    public Builder unrecAmt(String value) {
      this.dataFields.put("unrecAmt", value);
      return this;
    }

    /**
     * Set the transactionDate value
     *
     * Description: The date the transaction is carried out and registered in the cash journal.
     *
     * @param value
     *          The transactionDate value.
     * @return The builder for this class.
     */
    public Builder transactionDate(String value) {
      this.dataFields.put("transactionDate", value);
      return this;
    }

    /**
     * Set the startingBalance value
     *
     * Description: Starting Balance
     *
     * @param value
     *          The startingBalance value.
     * @return The builder for this class.
     */
    public Builder startingBalance(String value) {
      this.dataFields.put("startingBalance", value);
      return this;
    }

    /**
     * Set the paymentNo value
     *
     * Description: Payment_No
     *
     * @param value
     *          The paymentNo value.
     * @return The builder for this class.
     */
    public Builder paymentNo(String value) {
      this.dataFields.put("paymentNo", value);
      return this;
    }

    /**
     * Set the paymentAmt value
     *
     * Description: Payment_Amt
     *
     * @param value
     *          The paymentAmt value.
     * @return The builder for this class.
     */
    public Builder paymentAmt(String value) {
      this.dataFields.put("paymentAmt", value);
      return this;
    }

    /**
     * Set the itemNo value
     *
     * Description: Item_No
     *
     * @param value
     *          The itemNo value.
     * @return The builder for this class.
     */
    public Builder itemNo(String value) {
      this.dataFields.put("itemNo", value);
      return this;
    }

    /**
     * Set the itemAmt value
     *
     * Description: Item_Amt
     *
     * @param value
     *          The itemAmt value.
     * @return The builder for this class.
     */
    public Builder itemAmt(String value) {
      this.dataFields.put("itemAmt", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the account value
     *
     * Description: Financial account used to deposit / withdrawal money such as bank accounts or
     * petty cash
     *
     * @param value
     *          The account value.
     * @return The builder for this class.
     */
    public Builder account(String value) {
      this.dataFields.put("account", value);
      return this;
    }

    /**
     * Set the endingBalance value
     *
     * Description: Ending or closing balance
     *
     * @param value
     *          The endingBalance value.
     * @return The builder for this class.
     */
    public Builder endingBalance(String value) {
      this.dataFields.put("endingBalance", value);
      return this;
    }

    /**
     * Set the documentNo value
     *
     * Description: An often automatically generated identifier for all documents.
     *
     * @param value
     *          The documentNo value.
     * @return The builder for this class.
     */
    public Builder documentNo(String value) {
      this.dataFields.put("documentNo", value);
      return this;
    }

    /**
     * Set the documentStatus value
     *
     * Description: The Document Status indicates the status of a document at this time.
     *
     * @param value
     *          The documentStatus value.
     * @return The builder for this class.
     */
    public Builder documentStatus(String value) {
      this.dataFields.put("documentStatus", value);
      return this;
    }

    /**
     * Set the depositNo value
     *
     * Description: Deposit_No
     *
     * @param value
     *          The depositNo value.
     * @return The builder for this class.
     */
    public Builder depositNo(String value) {
      this.dataFields.put("depositNo", value);
      return this;
    }

    /**
     * Set the depositAmt value
     *
     * Description: Deposit_Amt
     *
     * @param value
     *          The depositAmt value.
     * @return The builder for this class.
     */
    public Builder depositAmt(String value) {
      this.dataFields.put("depositAmt", value);
      return this;
    }

    /**
     * Set the endingDate value
     *
     * Description: A parameter stating the ending time range related for a specified request query
     * etc.
     *
     * @param value
     *          The endingDate value.
     * @return The builder for this class.
     */
    public Builder endingDate(String value) {
      this.dataFields.put("endingDate", value);
      return this;
    }

    /**
     * Set the documentType value
     *
     * Description: A value defining what sequence and process setup are used to handle this
     * document.
     *
     * @param value
     *          The documentType value.
     * @return The builder for this class.
     */
    public Builder documentType(String value) {
      this.dataFields.put("documentType", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ReconciliationsData build() {
      return new ReconciliationsData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ReconciliationsData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
