/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.manualsettlement;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for BalancePaymentData
 *
 * @author plujan
 *
 */
public class BalancePaymentData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the gLItem value
     *
     * Description: An alias for the Account Combination which can be commonly used in daily
     * operations.
     *
     * @param value
     *          The gLItem value.
     * @return The builder for this class.
     */
    public Builder gLItem(String value) {
      this.dataFields.put("gLItem", value);
      return this;
    }

    /**
     * Set the creditAmount value
     *
     * Description: Credit amount
     *
     * @param value
     *          The creditAmount value.
     * @return The builder for this class.
     */
    public Builder creditAmount(String value) {
      this.dataFields.put("creditAmount", value);
      return this;
    }

    /**
     * Set the debitAmount value
     *
     * Description: Debit amount
     *
     * @param value
     *          The debitAmount value.
     * @return The builder for this class.
     */
    public Builder debitAmount(String value) {
      this.dataFields.put("debitAmount", value);
      return this;
    }

    /**
     * Set the payment value
     *
     * Description: A obligation to pay or a right to collect for a specified item or service.
     *
     * @param value
     *          The payment value.
     * @return The builder for this class.
     */
    public Builder payment(String value) {
      this.dataFields.put("payment", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public BalancePaymentData build() {
      return new BalancePaymentData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private BalancePaymentData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
