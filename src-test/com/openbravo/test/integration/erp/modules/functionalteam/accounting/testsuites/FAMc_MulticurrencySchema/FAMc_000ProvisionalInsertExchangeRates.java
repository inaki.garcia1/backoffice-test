/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  David Miguélez <david.miguelez@openbravo.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.accounting.testsuites.FAMc_MulticurrencySchema;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sharedtabs.ExchangeRatesData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount.Transaction;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;

/**
 * This test case is provisional while an issue is being resolved. Insert data into Exchange Rates
 * Tabs.
 *
 * @author David Miguélez
 */

@RunWith(Parameterized.class)
public class FAMc_000ProvisionalInsertExchangeRates extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /* Data for [FAMc_000] Insert data into Exchange Rates Tabs. */
  /* Data for the Invoices */
  /** The sales invoice header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  /** The first sales invoice exchange rates data. */
  ExchangeRatesData salesInvoiceExchangeRatesData;
  /** The second sales invoice header data. */
  SalesInvoiceHeaderData secondSalesInvoiceHeaderData;
  /** The second sales invoice exchange rates data. */
  ExchangeRatesData secondSalesInvoiceExchangeRatesData;
  /** The third sales invoice header data. */
  SalesInvoiceHeaderData thirdSalesInvoiceHeaderData;
  /** The third sales invoice exchange rates data. */
  ExchangeRatesData thirdSalesInvoiceExchangeRatesData;
  /** The fourth sales invoice header data. */
  SalesInvoiceHeaderData fourthSalesInvoiceHeaderData;
  /** The fourth sales invoice exchange rates data. */
  ExchangeRatesData fourthSalesInvoiceExchangeRatesData;
  /** The purchase invoice header data. */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  /** The purchase invoice exchange rates data. */
  ExchangeRatesData purchaseInvoiceExchangeRatesData;

  /* Data for the Payments */
  /** The paymentIn header data. */
  PaymentInHeaderData paymentInHeaderData;
  /** The paymentIn exchange rates data. */
  ExchangeRatesData paymentInExchangeRatesData;
  /** The second paymentIn header data. */
  PaymentInHeaderData secondPpaymentInHeaderData;
  /** The second paymentIn exchange rates data. */
  ExchangeRatesData secodPaymentInExchangeRatesData;
  /** The third paymentIn header data. */
  PaymentInHeaderData thirdPpaymentInHeaderData;
  /** The third paymentIn exchange rates data. */
  ExchangeRatesData thirdPaymentInExchangeRatesData;
  /** The fourth paymentIn header data. */
  PaymentInHeaderData fourthPpaymentInHeaderData;
  /** The fourth paymentIn exchange rates data. */
  ExchangeRatesData fourthPaymentInExchangeRatesData;
  /** The fifth paymentIn header data. */
  PaymentInHeaderData fifthPpaymentInHeaderData;
  /** The fifth paymentIn exchange rates data. */
  ExchangeRatesData fifthPaymentInExchangeRatesData;
  /** The sixth paymentIn header data. */
  PaymentInHeaderData sixthPpaymentInHeaderData;
  /** The sixth paymentIn exchange rates data. */
  ExchangeRatesData sixthPaymentInExchangeRatesData;
  /** The PaymentOut header data. */
  PaymentOutHeaderData paymentOutHeaderData;
  /** The PaymenOut exchange rates data. */
  ExchangeRatesData paymentOutExchangeRatesData;
  /** The second PaymentOut header data. */
  PaymentOutHeaderData secondPaymentOutHeaderData;
  /** The second PaymenOut exchange rates data. */
  ExchangeRatesData secondPaymentOutExchangeRatesData;
  /** The third PaymentOut header data. */
  PaymentOutHeaderData thirdPaymentOutHeaderData;
  /** The third PaymenOut exchange rates data. */
  ExchangeRatesData thirdPaymentOutExchangeRatesData;
  /** The fourth PaymentOut header data. */
  PaymentOutHeaderData fourthPaymentOutHeaderData;
  /** The fourth PaymenOut exchange rates data. */
  ExchangeRatesData fourthPaymentOutExchangeRatesData;

  /* Data for the Transactions */
  /** The account header data. */
  AccountData accountHeaderData;
  /** The first transaction header data. */
  TransactionsData transactionLinesData;
  /** The first transaction exchange rates data. */
  ExchangeRatesData transactionExchangeRatesData;
  /** The second transaction header data. */
  TransactionsData secondTransactionLinesData;
  /** The second transaction exchange rates data. */
  ExchangeRatesData secondTransactionExchangeRatesData;
  /** The third transaction header data. */
  TransactionsData thirdTransactionLinesData;
  /** The third transaction exchange rates data. */
  ExchangeRatesData thirdTransactionExchangeRatesData;
  /** The fourth transaction header data. */
  TransactionsData fourthTransactionLinesData;
  /** The fourth transaction exchange rates data. */
  ExchangeRatesData fourthTransactionExchangeRatesData;
  /** The fifth transaction header data. */
  TransactionsData fifthTransactionLinesData;
  /** The fifth transaction exchange rates data. */
  ExchangeRatesData fifthTransactionExchangeRatesData;
  /** The sixth transaction header data. */
  TransactionsData sixthTransactionLinesData;
  /** The sixth transaction exchange rates data. */
  ExchangeRatesData sixthTransactionExchangeRatesData;
  /** The seventh transaction header data. */
  TransactionsData seventhTransactionLinesData;
  /** The seventh transaction exchange rates data. */
  ExchangeRatesData seventhTransactionExchangeRatesData;
  /** The eighth transaction header data. */
  TransactionsData eighthTransactionLinesData;
  /** The eighth transaction exchange rates data. */
  ExchangeRatesData eighthTransactionExchangeRatesData;
  /** The ninth transaction header data. */
  TransactionsData ninthTransactionLinesData;
  /** The ninth transaction exchange rates data. */
  ExchangeRatesData ninthTransactionExchangeRatesData;

  /**
   * Class constructor.
   *
   * Invoices:
   *
   * @param salesInvoiceHeaderData
   *          The sales invoice header data.
   * @param salesInvoiceExchangeRatesData
   *          The first sales invoice exchange rates data.
   * @param secondSalesInvoiceHeaderData
   *          The second sales invoice header data.
   * @param secondSalesInvoiceExchangeRatesData
   *          The second sales invoice exchange rates data.
   * @param thirdSalesInvoiceHeaderData
   *          The third sales invoice header data.
   * @param thirdSalesInvoiceExchangeRatesData
   *          The third sales invoice exchange rates data.
   * @param fourthSalesInvoiceHeaderData
   *          The fourth sales invoice header data.
   * @param fourthSalesInvoiceExchangeRatesData
   *          The fourth sales invoice exchange rates data.
   * @param purchaseInvoiceHeaderData
   *          The purchase invoice header data.
   * @param purchaseInvoiceExchangeRatesData
   *          The purchase invoice exchange rates data.
   *
   * @param paymentInHeaderData
   *          The paymentIn header data.
   * @param paymentInExchangeRatesData
   *          The paymentIn exchange rates data.
   * @param secondPpaymentInHeaderData
   *          The second paymentIn header data.
   * @param secodPaymentInExchangeRatesData
   *          The second paymentIn exchange rates data.
   * @param thirdPpaymentInHeaderData
   *          The third paymentIn header data.
   * @param thirdPaymentInExchangeRatesData
   *          The third paymentIn exchange rates data.
   * @param fourthPpaymentInHeaderData
   *          The fourth paymentIn header data.
   * @param fourthPaymentInExchangeRatesData
   *          The fourth paymentIn exchange rates data.
   * @param fifthPpaymentInHeaderData
   *          The fifth paymentIn header data.
   * @param fifthPaymentInExchangeRatesData
   *          The fifth paymentIn exchange rates data.
   * @param sixthPpaymentInHeaderData
   *          The sixth paymentIn header data.
   * @param sixthPaymentInExchangeRatesData
   *          The sixth paymentIn exchange rates data.
   * @param paymentOutHeaderData
   *          The PaymentOut header data.
   * @param paymentOutExchangeRatesData
   *          The PaymenOut exchange rates data.
   * @param secondPaymentOutHeaderData
   *          The second PaymentOut header data.
   * @param secondPaymentOutExchangeRatesData
   *          The second PaymenOut exchange rates data.
   * @param thirdPaymentOutHeaderData
   *          The third PaymentOut header data.
   * @param thirdPaymentOutExchangeRatesData
   *          The third PaymenOut exchange rates data.
   * @param fourthPaymentOutHeaderData
   *          The fourth PaymentOut header data.
   * @param accountHeaderData
   *          The account Header Data.
   * @param transactionLinesData
   *          The transaction Header Data.
   * @param transactionExchangeRatesData
   *          The first transaction exchange rates data.
   * @param secondTransactionLinesData
   *          The second transaction header data.
   * @param secondTransactionExchangeRatesData
   *          The second transaction exchange rates data.
   * @param thirdTransactionLinesData
   *          The third transaction header data.
   * @param thirdTransactionExchangeRatesData
   *          The third transaction exchange rates data.
   * @param fourthTransactionLinesData
   *          The fourth transaction header data.
   * @param fourthTransactionExchangeRatesData
   *          The fourth transaction exchange rates data.
   * @param fifthTransactionLinesData
   *          The fifth transaction header data.
   * @param fifthTransactionExchangeRatesData
   *          The fifth transaction exchange rates data.
   * @param sixthTransactionLinesData
   *          The sixth transaction header data.
   * @param sixthTransactionExchangeRatesData
   *          The sixth transaction exchange rates data.
   * @param seventhTransactionLinesData
   *          The seventh transaction header data.
   * @param seventhTransactionExchangeRatesData
   *          The seventh transaction exchange rates data.
   * @param eighthTransactionLinesData
   *          The eighth transaction header data.
   * @param eighthTransactionExchangeRatesData
   *          The eighth transaction exchange rates data.
   * @param ninthTransactionLinesData
   *          The ninth transaction header data.
   * @param ninthTransactionExchangeRatesData
   *          The ninth transaction exchange rates data.
   */

  public FAMc_000ProvisionalInsertExchangeRates(SalesInvoiceHeaderData salesInvoiceHeaderData,
      ExchangeRatesData salesInvoiceExchangeRatesData,
      SalesInvoiceHeaderData secondSalesInvoiceHeaderData,
      ExchangeRatesData secondSalesInvoiceExchangeRatesData,
      SalesInvoiceHeaderData thirdSalesInvoiceHeaderData,
      ExchangeRatesData thirdSalesInvoiceExchangeRatesData,
      SalesInvoiceHeaderData fourthSalesInvoiceHeaderData,
      ExchangeRatesData fourthSalesInvoiceExchangeRatesData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      ExchangeRatesData purchaseInvoiceExchangeRatesData, PaymentInHeaderData paymentInHeaderData,
      ExchangeRatesData paymentInExchangeRatesData, PaymentInHeaderData secondPpaymentInHeaderData,
      ExchangeRatesData secodPaymentInExchangeRatesData,
      PaymentInHeaderData thirdPpaymentInHeaderData,
      ExchangeRatesData thirdPaymentInExchangeRatesData,
      PaymentInHeaderData fourthPpaymentInHeaderData,
      ExchangeRatesData fourthPaymentInExchangeRatesData,
      PaymentInHeaderData fifthPpaymentInHeaderData,
      ExchangeRatesData fifthPaymentInExchangeRatesData,
      PaymentInHeaderData sixthPpaymentInHeaderData,
      ExchangeRatesData sixthPaymentInExchangeRatesData, PaymentOutHeaderData paymentOutHeaderData,
      ExchangeRatesData paymentOutExchangeRatesData,
      PaymentOutHeaderData secondPaymentOutHeaderData,
      ExchangeRatesData secondPaymentOutExchangeRatesData,
      PaymentOutHeaderData thirdPaymentOutHeaderData,
      ExchangeRatesData thirdPaymentOutExchangeRatesData,
      PaymentOutHeaderData fourthPaymentOutHeaderData,
      ExchangeRatesData fourthPaymentOutExchangeRatesData, AccountData accountHeaderData,
      TransactionsData transactionLinesData, ExchangeRatesData transactionExchangeRatesData,
      TransactionsData secondTransactionLinesData,
      ExchangeRatesData secondTransactionExchangeRatesData,
      TransactionsData thirdTransactionLinesData,
      ExchangeRatesData thirdTransactionExchangeRatesData,
      TransactionsData fourthTransactionLinesData,
      ExchangeRatesData fourthTransactionExchangeRatesData,
      TransactionsData fifthTransactionLinesData,
      ExchangeRatesData fifthTransactionExchangeRatesData,
      TransactionsData sixthTransactionLinesData,
      ExchangeRatesData sixthTransactionExchangeRatesData,
      TransactionsData seventhTransactionLinesData,
      ExchangeRatesData seventhTransactionExchangeRatesData,
      TransactionsData eighthTransactionLinesData,
      ExchangeRatesData eighthTransactionExchangeRatesData,
      TransactionsData ninthTransactionLinesData,
      ExchangeRatesData ninthTransactionExchangeRatesData) {

    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceExchangeRatesData = salesInvoiceExchangeRatesData;
    this.secondSalesInvoiceHeaderData = secondSalesInvoiceHeaderData;
    this.secondSalesInvoiceExchangeRatesData = secondSalesInvoiceExchangeRatesData;
    this.thirdSalesInvoiceHeaderData = thirdSalesInvoiceHeaderData;
    this.thirdSalesInvoiceExchangeRatesData = thirdSalesInvoiceExchangeRatesData;
    this.fourthSalesInvoiceHeaderData = fourthSalesInvoiceHeaderData;
    this.fourthSalesInvoiceExchangeRatesData = fourthSalesInvoiceExchangeRatesData;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceExchangeRatesData = purchaseInvoiceExchangeRatesData;

    this.paymentInHeaderData = paymentInHeaderData;
    this.paymentInExchangeRatesData = paymentInExchangeRatesData;
    this.secondPpaymentInHeaderData = secondPpaymentInHeaderData;
    this.secodPaymentInExchangeRatesData = secodPaymentInExchangeRatesData;
    this.thirdPpaymentInHeaderData = thirdPpaymentInHeaderData;
    this.thirdPaymentInExchangeRatesData = thirdPaymentInExchangeRatesData;
    this.fourthPpaymentInHeaderData = fourthPpaymentInHeaderData;
    this.fourthPaymentInExchangeRatesData = fourthPaymentInExchangeRatesData;
    this.fifthPpaymentInHeaderData = fifthPpaymentInHeaderData;
    this.fifthPaymentInExchangeRatesData = fifthPaymentInExchangeRatesData;
    this.sixthPpaymentInHeaderData = sixthPpaymentInHeaderData;
    this.sixthPaymentInExchangeRatesData = sixthPaymentInExchangeRatesData;
    this.paymentOutHeaderData = paymentOutHeaderData;
    this.paymentOutExchangeRatesData = paymentOutExchangeRatesData;
    this.secondPaymentOutHeaderData = secondPaymentOutHeaderData;
    this.secondPaymentOutExchangeRatesData = secondPaymentOutExchangeRatesData;
    this.thirdPaymentOutHeaderData = thirdPaymentOutHeaderData;
    this.thirdPaymentOutExchangeRatesData = thirdPaymentOutExchangeRatesData;
    this.fourthPaymentOutHeaderData = fourthPaymentOutHeaderData;
    this.fourthPaymentOutExchangeRatesData = fourthPaymentOutExchangeRatesData;

    this.accountHeaderData = accountHeaderData;
    this.transactionLinesData = transactionLinesData;
    this.transactionExchangeRatesData = transactionExchangeRatesData;
    this.secondTransactionLinesData = secondTransactionLinesData;
    this.secondTransactionExchangeRatesData = secondTransactionExchangeRatesData;
    this.thirdTransactionLinesData = thirdTransactionLinesData;
    this.thirdTransactionExchangeRatesData = thirdTransactionExchangeRatesData;
    this.fourthTransactionLinesData = fourthTransactionLinesData;
    this.fourthTransactionExchangeRatesData = fourthTransactionExchangeRatesData;
    this.fifthTransactionLinesData = fifthTransactionLinesData;
    this.fifthTransactionExchangeRatesData = fifthTransactionExchangeRatesData;
    this.sixthTransactionLinesData = sixthTransactionLinesData;
    this.sixthTransactionExchangeRatesData = sixthTransactionExchangeRatesData;
    this.seventhTransactionLinesData = seventhTransactionLinesData;
    this.seventhTransactionExchangeRatesData = seventhTransactionExchangeRatesData;
    this.eighthTransactionLinesData = eighthTransactionLinesData;
    this.eighthTransactionExchangeRatesData = eighthTransactionExchangeRatesData;
    this.ninthTransactionLinesData = ninthTransactionLinesData;
    this.ninthTransactionExchangeRatesData = ninthTransactionExchangeRatesData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<Object[]> orderToCashValues() {

    return Arrays.asList(new Object[][] { {
        // Parameters for [FAMc_000] Insert data into Exchange Rates Tabs.
        // Data for the Invoices
        new SalesInvoiceHeaderData.Builder().documentNo("I/12").build(),
        new ExchangeRatesData.Builder().toCurrency("USD").rate("1.5").build(),
        new SalesInvoiceHeaderData.Builder().documentNo("I/16").build(),
        new ExchangeRatesData.Builder().toCurrency("USD").rate("1.5").build(),
        new SalesInvoiceHeaderData.Builder().documentNo("I/19").build(),
        new ExchangeRatesData.Builder().toCurrency("USD").rate("1.25").build(),
        new SalesInvoiceHeaderData.Builder().documentNo("I/18").build(),
        new ExchangeRatesData.Builder().toCurrency("USD").rate("1.5").build(),
        new PurchaseInvoiceHeaderData.Builder().documentNo("10000007").build(),
        new ExchangeRatesData.Builder().toCurrency("USD").rate("0.75").build(),

        // Data for the Payments
        new PaymentInHeaderData.Builder().documentNo("400010").build(),
        new ExchangeRatesData.Builder().toCurrency("USD").rate("5").build(),
        new PaymentInHeaderData.Builder().documentNo("400011").build(),
        new ExchangeRatesData.Builder().toCurrency("USD").rate("2.5").build(),
        new PaymentInHeaderData.Builder().documentNo("400012").build(),
        new ExchangeRatesData.Builder().toCurrency("USD").rate("3.5").build(),
        new PaymentInHeaderData.Builder().documentNo("400013").build(),
        new ExchangeRatesData.Builder().toCurrency("USD").rate("2").build(),
        new PaymentInHeaderData.Builder().documentNo("400021").build(),
        new ExchangeRatesData.Builder().toCurrency("USD").rate("3.5").build(),
        new PaymentInHeaderData.Builder().documentNo("400025").build(),
        new ExchangeRatesData.Builder().toCurrency("USD").rate("3.5").build(),
        new PaymentOutHeaderData.Builder().documentNo("700006").build(),
        new ExchangeRatesData.Builder().toCurrency("USD").rate("1.25").build(),
        new PaymentOutHeaderData.Builder().documentNo("700007").build(),
        new ExchangeRatesData.Builder().toCurrency("USD").rate("0.8").build(),
        new PaymentOutHeaderData.Builder().documentNo("700008").build(),
        new ExchangeRatesData.Builder().toCurrency("USD").rate("0.5").build(),
        new PaymentOutHeaderData.Builder().documentNo("700009").build(),
        new ExchangeRatesData.Builder().toCurrency("USD").rate("2.1").build(),

        // Data for the Transactions
        new AccountData.Builder().name("Accounting Documents DOLLAR").build(),
        new TransactionsData.Builder().documentNo("400009")
            .description("Invoice No.: I/7.")
            .build(),
        new ExchangeRatesData.Builder().toCurrency("EUR").rate("0.25").build(),
        new TransactionsData.Builder().documentNo("400010")
            .description("Invoice No.: I/8.")
            .build(),
        new ExchangeRatesData.Builder().toCurrency("EUR").rate("0.2").build(),
        new TransactionsData.Builder().documentNo("400012")
            .description("Invoice No.: I/10.")
            .build(),
        new ExchangeRatesData.Builder().toCurrency("EUR").rate("0.285714").build(),
        new TransactionsData.Builder().documentNo("400013")
            .description("Invoice No.: I/11.")
            .build(),
        new ExchangeRatesData.Builder().toCurrency("EUR").rate("0.5").build(),
        new TransactionsData.Builder().documentNo("400014")
            .description("Invoice No.: I/12.")
            .build(),
        new ExchangeRatesData.Builder().toCurrency("EUR").rate("0.235294").build(),
        new TransactionsData.Builder().documentNo("400018")
            .description("Invoice No.: I/16.")
            .build(),
        new ExchangeRatesData.Builder().toCurrency("EUR").rate("0.25").build(),
        new TransactionsData.Builder().documentNo("400019")
            .description("Invoice No.: I/17, I/18.")
            .build(),
        new ExchangeRatesData.Builder().toCurrency("EUR").rate("0.25").build(),
        new TransactionsData.Builder().documentNo("700006")
            .description("Invoice No.: 10000006.")
            .build(),
        new ExchangeRatesData.Builder().toCurrency("EUR").rate("0.8").build(),
        new TransactionsData.Builder().documentNo("700009")
            .description("Invoice No.: 10000009.")
            .build(),
        new ExchangeRatesData.Builder().toCurrency("EUR").rate("0.47619").build() } });
  }

  /**
   * This test case is provisional while an issue is being resolved. Insert data into Exchange Rates
   * Tabs.
   */
  @Test
  public void FAMc_000ProvisionalExchangeRatesTabs() {
    logger.info("** Start of test case [FAMc_000] Insert data into Exchange Rates Tabs. **");

    /* Test cases for the Invoices */
    final SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.select(salesInvoiceHeaderData);
    SalesInvoice.ExchangeRates salesInvoiceExchangeRates = salesInvoice.new ExchangeRates(mainPage);
    if (salesInvoiceExchangeRates.getRecordCount() == 0) {
      salesInvoiceExchangeRates.create(salesInvoiceExchangeRatesData);
    }
    salesInvoice.select(secondSalesInvoiceHeaderData);
    salesInvoiceExchangeRates = salesInvoice.new ExchangeRates(mainPage);
    if (salesInvoiceExchangeRates.getRecordCount() == 0) {
      salesInvoiceExchangeRates.create(secondSalesInvoiceExchangeRatesData);
    }
    salesInvoice.select(thirdSalesInvoiceHeaderData);
    salesInvoiceExchangeRates = salesInvoice.new ExchangeRates(mainPage);
    if (salesInvoiceExchangeRates.getRecordCount() == 0) {
      salesInvoiceExchangeRates.create(thirdSalesInvoiceExchangeRatesData);
    }
    salesInvoice.select(fourthSalesInvoiceHeaderData);
    salesInvoiceExchangeRates = salesInvoice.new ExchangeRates(mainPage);
    if (salesInvoiceExchangeRates.getRecordCount() == 0) {
      salesInvoiceExchangeRates.create(fourthSalesInvoiceExchangeRatesData);
    }
    final PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.select(purchaseInvoiceHeaderData);
    PurchaseInvoice.ExchangeRates purchaseInvoiceExchangeRates = purchaseInvoice.new ExchangeRates(
        mainPage);
    if (purchaseInvoiceExchangeRates.getRecordCount() == 0) {
      purchaseInvoiceExchangeRates.create(purchaseInvoiceExchangeRatesData);
    }

    /* Test cases for the Payments */
    final PaymentIn paymentIn = new PaymentIn(mainPage).open();
    paymentIn.select(paymentInHeaderData);
    PaymentIn.ExchangeRates paymentInExchangeRates = paymentIn.new ExchangeRates(mainPage);
    if (paymentInExchangeRates.getRecordCount() == 0) {
      paymentInExchangeRates.create(paymentInExchangeRatesData);
    }
    paymentIn.select(secondPpaymentInHeaderData);
    paymentInExchangeRates = paymentIn.new ExchangeRates(mainPage);
    if (paymentInExchangeRates.getRecordCount() == 0) {
      paymentInExchangeRates.create(secodPaymentInExchangeRatesData);
    }
    paymentIn.select(thirdPpaymentInHeaderData);
    paymentInExchangeRates = paymentIn.new ExchangeRates(mainPage);
    if (paymentInExchangeRates.getRecordCount() == 0) {
      paymentInExchangeRates.create(thirdPaymentInExchangeRatesData);
    }
    paymentIn.select(fourthPpaymentInHeaderData);
    paymentInExchangeRates = paymentIn.new ExchangeRates(mainPage);
    if (paymentInExchangeRates.getRecordCount() == 0) {
      paymentInExchangeRates.create(fourthPaymentInExchangeRatesData);
    }
    paymentIn.select(fifthPpaymentInHeaderData);
    paymentInExchangeRates = paymentIn.new ExchangeRates(mainPage);
    if (paymentInExchangeRates.getRecordCount() == 0) {
      paymentInExchangeRates.create(fifthPaymentInExchangeRatesData);
    }
    paymentIn.select(sixthPpaymentInHeaderData);
    paymentInExchangeRates = paymentIn.new ExchangeRates(mainPage);
    if (paymentInExchangeRates.getRecordCount() == 0) {
      paymentInExchangeRates.create(sixthPaymentInExchangeRatesData);
    }
    final PaymentOut paymentOut = new PaymentOut(mainPage).open();
    paymentOut.select(paymentOutHeaderData);
    PaymentOut.ExchangeRates paymentOutExchangeRates = paymentOut.new ExchangeRates(mainPage);
    if (paymentOutExchangeRates.getRecordCount() == 0) {
      paymentOutExchangeRates.create(paymentOutExchangeRatesData);
    }
    paymentOut.select(secondPaymentOutHeaderData);
    paymentOutExchangeRates = paymentOut.new ExchangeRates(mainPage);
    if (paymentOutExchangeRates.getRecordCount() == 0) {
      paymentOutExchangeRates.create(secondPaymentOutExchangeRatesData);
    }
    paymentOut.select(thirdPaymentOutHeaderData);
    paymentOutExchangeRates = paymentOut.new ExchangeRates(mainPage);
    if (paymentOutExchangeRates.getRecordCount() == 0) {
      paymentOutExchangeRates.create(thirdPaymentOutExchangeRatesData);
    }
    paymentOut.select(fourthPaymentOutHeaderData);
    paymentOutExchangeRates = paymentOut.new ExchangeRates(mainPage);
    if (paymentOutExchangeRates.getRecordCount() == 0) {
      paymentOutExchangeRates.create(fourthPaymentOutExchangeRatesData);
    }

    /* Test cases for the Transactions */
    final FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.Transaction transaction = financialAccount.new Transaction(mainPage);
    transaction.select(transactionLinesData);
    Transaction.ExchangeRates transactionExchangeRates = transaction.new ExchangeRates(mainPage);
    if (transactionExchangeRates.getRecordCount() == 0) {
      transactionExchangeRates.create(transactionExchangeRatesData);
    }
    transaction.select(secondTransactionLinesData);
    transactionExchangeRates = transaction.new ExchangeRates(mainPage);
    if (transactionExchangeRates.getRecordCount() == 0) {
      transactionExchangeRates.create(secondTransactionExchangeRatesData);
    }
    transaction.select(thirdTransactionLinesData);
    transactionExchangeRates = transaction.new ExchangeRates(mainPage);
    if (transactionExchangeRates.getRecordCount() == 0) {
      transactionExchangeRates.create(thirdTransactionExchangeRatesData);
    }
    transaction.select(fourthTransactionLinesData);
    transactionExchangeRates = transaction.new ExchangeRates(mainPage);
    if (transactionExchangeRates.getRecordCount() == 0) {
      transactionExchangeRates.create(fourthTransactionExchangeRatesData);
    }
    transaction.select(fifthTransactionLinesData);
    transactionExchangeRates = transaction.new ExchangeRates(mainPage);
    if (transactionExchangeRates.getRecordCount() == 0) {
      transactionExchangeRates.create(fifthTransactionExchangeRatesData);
    }
    transaction.select(sixthTransactionLinesData);
    transactionExchangeRates = transaction.new ExchangeRates(mainPage);
    if (transactionExchangeRates.getRecordCount() == 0) {
      transactionExchangeRates.create(sixthTransactionExchangeRatesData);
    }
    transaction.select(seventhTransactionLinesData);
    transactionExchangeRates = transaction.new ExchangeRates(mainPage);
    if (transactionExchangeRates.getRecordCount() == 0) {
      transactionExchangeRates.create(seventhTransactionExchangeRatesData);
    }
    transaction.select(eighthTransactionLinesData);
    transactionExchangeRates = transaction.new ExchangeRates(mainPage);
    if (transactionExchangeRates.getRecordCount() == 0) {
      transactionExchangeRates.create(eighthTransactionExchangeRatesData);
    }
    transaction.select(ninthTransactionLinesData);
    transactionExchangeRates = transaction.new ExchangeRates(mainPage);
    if (transactionExchangeRates.getRecordCount() == 0) {
      transactionExchangeRates.create(ninthTransactionExchangeRatesData);
    }
    logger.info("** End of test case [FAMc_000] Insert data into Exchange Rates Tabs. **");
  }

}
