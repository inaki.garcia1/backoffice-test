package com.openbravo.test.integration.erp.gui.masterdata.product;

import com.openbravo.test.integration.erp.data.masterdata.product.ProductCharacteristicData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;

/**
 * Executes and verifies actions on OpenbravoERP Product tab.
 *
 * @author jbu
 *
 */
public class ProductCharacteristicTab extends GeneratedTab<ProductCharacteristicData> {

  /** The tab title. */
  static final String TITLE = "Characteristics";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  static final String IDENTIFIER = "A661A0A05DCD4650BCB14B010C87F0AA";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /**
   * Class constructor
   *
   */
  public ProductCharacteristicTab() {
    super(TITLE, IDENTIFIER);
    addChildTab(new ProductCharacteristicValueTab(this));
  }

}
