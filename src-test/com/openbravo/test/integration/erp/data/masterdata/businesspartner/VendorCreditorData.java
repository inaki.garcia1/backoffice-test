/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.businesspartner;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for VendorCreditorData
 *
 * @author plujan
 *
 */
public class VendorCreditorData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the pOPaymentTerms value
     *
     * Description: The setup and timing defined to complete a specified payment.
     *
     * @param value
     *          The pOPaymentTerms value.
     * @return The builder for this class.
     */
    public Builder pOPaymentTerms(String value) {
      this.dataFields.put("pOPaymentTerms", value);
      return this;
    }

    /**
     * Set the pOFinancialAccount value
     *
     * Description: Financial account used to deposit / withdrawal money such as bank accounts or
     * petty cash
     *
     * @param value
     *          The pOFinancialAccount value.
     * @return The builder for this class.
     */
    public Builder pOFinancialAccount(String value) {
      this.dataFields.put("pOFinancialAccount", value);
      return this;
    }

    /**
     * Set the pOPaymentMethod value
     *
     * Description: PO Payment method
     *
     * @param value
     *          The pOPaymentMethod value.
     * @return The builder for this class.
     */
    public Builder pOPaymentMethod(String value) {
      this.dataFields.put("pOPaymentMethod", value);
      return this;
    }

    /**
     * Set the taxCategory value
     *
     * Description: Bussines Partner Tax Category as a Vendor
     *
     * @param value
     *          The taxCategory value.
     * @return The builder for this class.
     */
    public Builder taxCategory(String value) {
      this.dataFields.put("taxCategory", value);
      return this;
    }

    /**
     * Set the bankAccount value
     *
     * Description: Default Vendor Bank Account
     *
     * @param value
     *          The bankAccount value.
     * @return The builder for this class.
     */
    public Builder bankAccount(String value) {
      this.dataFields.put("bankAccount", value);
      return this;
    }

    /**
     * Set the pOMaturityDate3 value
     *
     * Description: Day of the month of the due date
     *
     * @param value
     *          The pOMaturityDate3 value.
     * @return The builder for this class.
     */
    public Builder pOMaturityDate3(String value) {
      this.dataFields.put("pOMaturityDate3", value);
      return this;
    }

    /**
     * Set the pOMaturityDate2 value
     *
     * Description: Day of the month of the due date
     *
     * @param value
     *          The pOMaturityDate2 value.
     * @return The builder for this class.
     */
    public Builder pOMaturityDate2(String value) {
      this.dataFields.put("pOMaturityDate2", value);
      return this;
    }

    /**
     * Set the pOMaturityDate1 value
     *
     * Description: The day of the month that invoices are due. 3 maturity dates can be defined.
     *
     * @param value
     *          The pOMaturityDate1 value.
     * @return The builder for this class.
     */
    public Builder pOMaturityDate1(String value) {
      this.dataFields.put("pOMaturityDate1", value);
      return this;
    }

    /**
     * Set the vendor value
     *
     * Description: A business partner who sells products or services.
     *
     * @param value
     *          The vendor value.
     * @return The builder for this class.
     */
    public Builder vendor(Boolean value) {
      this.dataFields.put("vendor", value);
      return this;
    }

    /**
     * Set the purchasePricelist value
     *
     * Description: A catalog of selected products which can be purchased each with a specified
     * price.
     *
     * @param value
     *          The purchasePricelist value.
     * @return The builder for this class.
     */
    public Builder purchasePricelist(String value) {
      this.dataFields.put("purchasePricelist", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public VendorCreditorData build() {
      return new VendorCreditorData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private VendorCreditorData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
