/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions9;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.AddPaymentGrid;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount.PaymentMethod;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 36753
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class APRRegression36753Out1 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  AccountData accountData;
  PaymentMethodData paymentMethodData;
  PurchaseOrderHeaderData purchaseOrderHeaderData;
  PurchaseOrderHeaderData purchaseOrderHeaderVerficationData;
  PurchaseOrderLinesData purchaseOrderLinesData;
  PurchaseOrderLinesData purchaseOrderLinesVerificationData;
  PurchaseOrderLinesData purchaseOrderLinesData2;
  PurchaseOrderLinesData purchaseOrderLinesVerificationData2;
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData;
  PaymentPlanData orderPendingPaymentOutPlanData;
  AddPaymentPopUpData orderPaymentVerificationData;
  PaymentPlanData orderPaidPaymentOutPlanData;
  PaymentDetailsData orderPaidPaymentOutDetailsData;
  PaymentOutHeaderData paymentInHeaderVerificationData;
  PaymentOutLinesData paymentInLinesVerificationData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;//
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePaidPaymentOutPlanData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoicePaidPaymentOutDetailsData;
  PaymentPlanData orderInvoicedPaymentOutPlanData2;
  PaymentDetailsData orderInvoicedPaymentOutDetailsData;
  PaymentDetailsData orderInvoicedPaymentOutDetailsData2;
  PaymentOutLinesData paymentInLinesInvoicedVerificationData;
  PaymentOutLinesData paymentInLinesInvoicedVerificationData2;
  PaymentOutHeaderData paymentReactivatedInHeaderVerificationData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePendingPaymentOutPlanData;
  PaymentOutHeaderData paymentProcessInHeaderVerificationData3;
  String[][] journalEntryLines1;
  String[][] journalEntryLines2;
  PaymentPlanData orderPaymentReceivedPaymentOutPlanData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePaymentOutPlanData2;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoicePaymentinDetailsData2;
  String[][] journalEntryLines3;
  String[][] journalEntryLines4;

  /**
   * Class constructor.
   *
   */
  public APRRegression36753Out1(AccountData accountData, PaymentMethodData paymentMethodData,
      PurchaseOrderHeaderData purchaseOrderHeaderData,
      PurchaseOrderHeaderData purchaseOrderHeaderVerficationData,
      PurchaseOrderLinesData purchaseOrderLinesData,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData,
      PurchaseOrderLinesData purchaseOrderLinesData2,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData2,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData,
      PaymentPlanData orderPendingPaymentOutPlanData,
      AddPaymentPopUpData orderPaymentVerificationData, PaymentPlanData orderPaidPaymentOutPlanData,
      PaymentDetailsData orderPaidPaymentOutDetailsData,
      PaymentOutHeaderData paymentInHeaderVerificationData,
      PaymentOutLinesData paymentInLinesVerificationData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePaidPaymentOutPlanData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoicePaidPaymentOutDetailsData,
      PaymentPlanData orderInvoicedPaymentOutPlanData2,
      PaymentDetailsData orderInvoicedPaymentOutDetailsData,
      PaymentDetailsData orderInvoicedPaymentOutDetailsData2,
      PaymentOutLinesData paymentInLinesInvoicedVerificationData,
      PaymentOutLinesData paymentInLinesInvoicedVerificationData2,
      PaymentOutHeaderData paymentReactivatedInHeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePendingPaymentOutPlanData,
      PaymentOutHeaderData paymentProcessInHeaderVerificationData3, String[][] journalEntryLines1,
      String[][] journalEntryLines2, PaymentPlanData orderPaymentReceivedPaymentOutPlanData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePaymentOutPlanData2,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoicePaymentinDetailsData2,
      String[][] journalEntryLines3, String[][] journalEntryLines4) {
    this.accountData = accountData;
    this.paymentMethodData = paymentMethodData;
    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    this.purchaseOrderHeaderVerficationData = purchaseOrderHeaderVerficationData;
    this.purchaseOrderLinesData = purchaseOrderLinesData;
    this.purchaseOrderLinesVerificationData = purchaseOrderLinesVerificationData;
    this.purchaseOrderLinesData2 = purchaseOrderLinesData2;
    this.purchaseOrderLinesVerificationData2 = purchaseOrderLinesVerificationData2;
    this.bookedPurchaseOrderHeaderVerificationData = bookedPurchaseOrderHeaderVerificationData;
    this.orderPendingPaymentOutPlanData = orderPendingPaymentOutPlanData;
    this.orderPaymentVerificationData = orderPaymentVerificationData;
    this.orderPaidPaymentOutPlanData = orderPaidPaymentOutPlanData;
    this.orderPaidPaymentOutDetailsData = orderPaidPaymentOutDetailsData;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    this.paymentInLinesVerificationData = paymentInLinesVerificationData;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineVerificationData = purchaseInvoiceLineVerificationData;
    this.invoicePaidPaymentOutPlanData = invoicePaidPaymentOutPlanData;
    this.invoicePaidPaymentOutDetailsData = invoicePaidPaymentOutDetailsData;
    this.orderInvoicedPaymentOutPlanData2 = orderInvoicedPaymentOutPlanData2;
    this.orderInvoicedPaymentOutDetailsData = orderInvoicedPaymentOutDetailsData;
    this.orderInvoicedPaymentOutDetailsData2 = orderInvoicedPaymentOutDetailsData2;
    this.paymentInLinesInvoicedVerificationData = paymentInLinesInvoicedVerificationData;
    this.paymentInLinesInvoicedVerificationData2 = paymentInLinesInvoicedVerificationData2;
    this.paymentReactivatedInHeaderVerificationData = paymentReactivatedInHeaderVerificationData;
    this.invoicePendingPaymentOutPlanData = invoicePendingPaymentOutPlanData;
    this.paymentProcessInHeaderVerificationData3 = paymentProcessInHeaderVerificationData3;
    this.journalEntryLines1 = journalEntryLines1;
    this.journalEntryLines2 = journalEntryLines2;
    this.orderPaymentReceivedPaymentOutPlanData = orderPaymentReceivedPaymentOutPlanData;
    this.invoicePaymentOutPlanData2 = invoicePaymentOutPlanData2;
    this.invoicePaymentinDetailsData2 = invoicePaymentinDetailsData2;
    this.journalEntryLines3 = journalEntryLines3;
    this.journalEntryLines4 = journalEntryLines4;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> PurchaseOrderValues() {
    Object[][] data = new Object[][] { {

        new AccountData.Builder().name("Accounting Documents EURO").build(),

        new PaymentMethodData.Builder().automaticDeposit(false)
            .payoutExecutionType("Automatic")
            .payoutExecutionProcess("Simple Execution Process")
            .payoutDeferred(true)
            .build(),

        new PurchaseOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Purchase Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paymentTerms("90 days")
            .build(),

        new PurchaseOrderHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("USA warehouse")
            .paymentTerms("90 days")
            .priceList("Purchase")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paymentTerms("90 days")
            .build(),

        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMC").priceListVersion("Purchase").build())
            .orderedQuantity("10")
            .tax("VAT 10% USA")
            .build(),

        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("20.00")
            .build(),

        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMA").priceListVersion("Purchase").build())
            .orderedQuantity("15")
            .tax("VAT 10% USA")
            .build(),

        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("30.00")
            .build(),

        new PurchaseOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("50.00")
            .grandTotalAmount("55.00")
            .currency("EUR")
            .build(),

        new PaymentPlanData.Builder().dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("55.00")
            .paid("0.00")
            .outstanding("55.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Vendor A")
            .fin_paymentmethod_id("Acc-3 (Payment-Trx-Reconciliation)")
            .fin_financial_account_id("Accounting Documents EURO - EUR")
            .c_currency_id("EUR")
            .actual_payment("55.00")
            .expected_payment("55.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("55.00")
            .total("55.00")
            .difference("0.00")
            .build(),

        new PaymentPlanData.Builder().dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("55.00")
            .paid("0.00")
            .outstanding("55.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),

        new PaymentDetailsData.Builder().paymentDate(OBDate.CURRENT_DATE)
            .expected("55.00")
            .paid("55.00")
            .writeoff("0.00")
            .status("Awaiting Execution")
            .build(),

        new PaymentOutHeaderData.Builder().organization("USA")
            .documentType("AP Payment")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("55.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Awaiting Execution")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new PaymentOutLinesData.Builder().dueDate(OBDate.CURRENT_DATE)
            .invoiceAmount("55.00")
            .expected("55.00")
            .paid("55.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().organization("USA")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentTerms("90 days")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new PurchaseInvoiceHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .priceList("Purchase")
            .paymentTerms("90 days")
            .build(),

        new PurchaseInvoiceLinesData.Builder().invoicedQuantity("10")
            .lineNetAmount("20.00")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("22.00")
            .summedLineAmount("20.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("22.00")
            .paid("0.00")
            .outstanding("22.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData.Builder()
            .paid("22.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("22.00")
            .account("Accounting Documents EURO - EUR")
            .status("Awaiting Execution")
            .build(),

        new PaymentPlanData.Builder().dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("55.00")
            .paid("0.00")
            .outstanding("55.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("2")
            .currency("EUR")
            .build(),

        new PaymentDetailsData.Builder().expected("22.00")
            .paid("22.00")
            .writeoff("0.00")
            .status("Awaiting Execution")
            .build(),

        new PaymentDetailsData.Builder().expected("55.00")
            .paid("33.00")
            .writeoff("0.00")
            .status("Awaiting Execution")
            .build(),

        new PaymentOutLinesData.Builder().invoiceAmount("22.00")
            .expected("22.00")
            .paid("22.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PaymentOutLinesData.Builder().invoiceAmount("55.00")
            .expected("55.00")
            .paid("33.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PaymentOutHeaderData.Builder().organization("USA")
            .documentType("AP Payment")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("0.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Awaiting Payment")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("22.00")
            .paid("0.00")
            .outstanding("22.00")
            .currency("EUR")
            .daysOverdue("0")
            .numberOfPayments("0")
            .build(),

        new PaymentOutHeaderData.Builder().organization("USA")
            .documentType("AP Payment")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("55.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Payment Made")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new String[][] { { "40700", "Anticipos a proveedores", "33.00", "" },
            { "40000", "Proveedores (euros)", "22.00", "" },
            { "40100", "Proveedores efectos comerciales a pagar", "", "55.00" } },
        new String[][] { { "1410", "Vendor prepayment", "82.50", "" },
            { "21100", "Accounts payable", "55.00", "" },
            { "11300", "Bank in transit", "", "137.50" } },

        new PaymentPlanData.Builder().dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("55.00")
            .paid("55.00")
            .outstanding("0.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("2")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("22.00")
            .paid("22.00")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData.Builder()
            .paid("22.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("22.00")
            .account("Accounting Documents EURO - EUR")
            .status("Payment Made")
            .build(),

        new String[][] { { "60000", "Compras de mercaderías", "20.00", "" },
            { "47200", "Hacienda Pública IVA soportado", "2.00", "" },
            { "40000", "Proveedores (euros)", "", "22.00" }, },

        new String[][] { { "51200", "Service costs", "50.00", "" },
            { "11600", "Tax Receivables", "5.00", "" },
            { "21100", "Accounts payable", "", "55.00" }, },

        } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 36753 Paying from order
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression36753Out1Test() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression36753Out1] Test regression 36753 Paying from order**");

    // Change payment method configuration
    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountData);
    PaymentMethod paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod
        .select(new PaymentMethodData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build());
    paymentMethod.edit(paymentMethodData);

    // // Get Business partner current balance
    // BusinessPartnerWindow businessPartnerWindow = new BusinessPartnerWindow();
    // mainPage.openView(businessPartnerWindow);
    // mainPage.waitForDataToLoad();
    //
    // BusinessPartnerTab businessPartnerTab = businessPartnerWindow.selectBusinessPartnerTab();
    // businessPartnerTab.select(new BusinessPartnerData.Builder().name("Vendor A")
    // .searchKey("CUSA").build());
    // String currentBalance = (String) businessPartnerTab.getData("creditUsed");
    // currentBalance = currentBalance.replace(",", "");
    // BigDecimal bpCurrentBalance = new BigDecimal(currentBalance);

    // Register a Purchase order
    PurchaseOrder order = new PurchaseOrder(mainPage).open();
    order.create(purchaseOrderHeaderData);
    order.assertSaved();
    order.assertData(purchaseOrderHeaderVerficationData);
    String orderNo = order.getData("documentNo").toString();

    // Create order lines
    PurchaseOrder.Lines orderLines = order.new Lines(mainPage);
    orderLines.create(purchaseOrderLinesData);
    orderLines.assertSaved();
    orderLines.assertData(purchaseOrderLinesVerificationData);

    orderLines = order.new Lines(mainPage);
    orderLines.create(purchaseOrderLinesData2);
    orderLines.assertSaved();
    orderLines.assertData(purchaseOrderLinesVerificationData2);

    // Book the order
    order.book();
    order.assertProcessCompletedSuccessfully2();
    order.assertData(bookedPurchaseOrderHeaderVerificationData);

    // Check pending order payment plan
    PurchaseOrder.PaymentOutPlan orderPaymentPlan = order.new PaymentOutPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderPendingPaymentOutPlanData);

    // Pay the order
    order.addPayment("Accounting Documents EURO - EUR", "Process Made Payment(s) and Withdrawal",
        orderPaymentVerificationData);
    order.assertPaymentCreatedSuccessfully();
    String purchaseOrderIdentifier = String.format("%s - %s - %s", orderNo,
        order.getData("orderDate"),
        bookedPurchaseOrderHeaderVerificationData.getDataField("grandTotalAmount"));

    // Check order payment plan
    orderPaymentPlan = order.new PaymentOutPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderPaidPaymentOutPlanData);

    // Check order payment out details
    PurchaseOrder.PaymentOutPlan.PaymentOutDetails orderPaymentOutDetails = orderPaymentPlan.new PaymentOutDetails(
        mainPage);
    orderPaymentOutDetails.assertCount(1);
    orderPaymentOutDetails.assertData(orderPaidPaymentOutDetailsData);

    String paymentNo = orderPaymentOutDetails.getData("payment").toString();
    paymentNo = paymentNo.substring(0, paymentNo.indexOf("-") - 1);

    // // Check Business Partner current balance
    // bpCurrentBalance = bpCurrentBalance.add(new BigDecimal(
    // (String) bookedPurchaseOrderHeaderVerificationData.getDataField("grandTotalAmount")));
    // DecimalFormat format = new DecimalFormat("#,##0.00");
    // businessPartnerWindow = new BusinessPartnerWindow();
    // mainPage.openView(businessPartnerWindow);
    // mainPage.waitForDataToLoad();
    //
    // businessPartnerTab = businessPartnerWindow.selectBusinessPartnerTab();
    // businessPartnerTab.select(new BusinessPartnerData.Builder().name("Vendor A")
    // .searchKey("CUSA").build());
    // businessPartnerTab.assertData(new BusinessPartnerData.Builder().creditUsed(
    // format.format(bpCurrentBalance)).build());

    // Check payment out created in add payment
    PaymentOut payment = new PaymentOut(mainPage).open();
    payment.select(new PaymentOutHeaderData.Builder().documentNo(paymentNo).build());
    payment.assertData(paymentInHeaderVerificationData);
    PaymentOut.Lines paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(1);
    paymentInLines.assertData((PaymentOutLinesData) paymentInLinesVerificationData
        .addDataField("orderPaymentSchedule$order$documentNo", orderNo));

    // Create a purchase invoice from order
    PurchaseInvoice invoice = new PurchaseInvoice(mainPage).open();
    invoice.create(purchaseInvoiceHeaderData);
    invoice.assertSaved();
    invoice.assertData(purchaseInvoiceHeaderVerificationData);
    invoice.createLinesFrom(purchaseOrderIdentifier, 1);
    invoice.assertProcessCompletedSuccessfully();

    // Check invoice line
    PurchaseInvoice.Lines invoiceLines = invoice.new Lines(mainPage);
    invoiceLines.assertCount(1);
    invoiceLines.assertData(purchaseInvoiceLineVerificationData);
    invoice.complete();
    invoice.assertProcessCompletedSuccessfully2();
    invoice.assertData(completedPurchaseInvoiceHeaderVerificationData);
    String invoiceNo = (String) invoice.getData("documentNo");

    // Check invoice payment plan
    PurchaseInvoice.PaymentOutPlan invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoicePaidPaymentOutPlanData);

    // Check invoice payment details
    PurchaseInvoice.PaymentOutPlan.PaymentOutDetails invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(
        mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoicePaidPaymentOutDetailsData);

    // Check invoiced purchase order payment plan
    order = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentPlan = order.new PaymentOutPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderInvoicedPaymentOutPlanData2);

    orderPaymentOutDetails = orderPaymentPlan.new PaymentOutDetails(mainPage);
    orderPaymentOutDetails.assertCount(2);
    orderPaymentOutDetails
        .select(new PaymentDetailsData.Builder().expected("22.00").paid("22.00").build());
    orderPaymentOutDetails.assertData(orderInvoicedPaymentOutDetailsData);

    orderPaymentOutDetails
        .select(new PaymentDetailsData.Builder().paid("33.00").expected("55.00").build());
    orderPaymentOutDetails.assertData(orderInvoicedPaymentOutDetailsData2);

    // Check invoiced payment out
    payment = new PaymentOut(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentOutHeaderData.Builder().documentNo(paymentNo).build());
    payment.assertData(paymentInHeaderVerificationData);
    paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(2);

    paymentInLinesInvoicedVerificationData.addDataField("orderPaymentSchedule$order$documentNo",
        orderNo);
    paymentInLinesInvoicedVerificationData
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo)
        .addDataField("invoiceAmount", "22.00");

    // Check invoiced payment out lines
    paymentInLines.select(new PaymentOutLinesData.Builder().paid("22.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData);
    paymentInLines.closeForm();

    paymentInLines.select(new PaymentOutLinesData.Builder().paid("33.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData2);
    paymentInLines.closeForm();

    // Reactivate payment and delete lines & check
    payment.reactivate("Reactivate and Delete Lines");
    payment.assertProcessCompletedSuccessfully2();
    payment.assertData(paymentReactivatedInHeaderVerificationData);

    // Check purchase order pending payment plan
    order = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentPlan = order.new PaymentOutPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderPendingPaymentOutPlanData);

    // Check invoice pending payment plan
    invoice = new PurchaseInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    invoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoiceNo).build());
    invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoicePendingPaymentOutPlanData);

    // Add Payment out details
    payment = new PaymentOut(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentOutHeaderData.Builder().documentNo(paymentNo).build());

    AddPaymentProcess paymentDetails = payment.addDetailsOpen();
    paymentDetails.setParameterValue("transaction_type", "Orders and Invoices");

    AddPaymentGrid orderInvoiceGrid = paymentDetails.getOrderInvoiceGrid();
    orderInvoiceGrid.unselectAll();
    orderInvoiceGrid.filter(new SelectedPaymentData.Builder().orderNo(orderNo).build());
    Sleep.trySleep(5000);
    orderInvoiceGrid.waitForDataToLoad();
    orderInvoiceGrid.selectAll();
    Sleep.trySleep(7000);
    paymentDetails.process("Process Made Payment(s) and Withdrawal");
    Sleep.trySleep(15000);

    // Check payment out and lines
    payment.assertData(paymentInHeaderVerificationData);
    paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(2);

    paymentInLinesInvoicedVerificationData.addDataField("orderPaymentSchedule$order$documentNo",
        orderNo);
    paymentInLinesInvoicedVerificationData
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo)
        .addDataField("invoiceAmount", "22.00");
    paymentInLines.select(new PaymentOutLinesData.Builder().paid("22.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData);
    paymentInLines.closeForm();

    paymentInLines.select(new PaymentOutLinesData.Builder().paid("33.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData2);
    paymentInLines.closeForm();

    // Check purchase order payment plan
    order = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentPlan = order.new PaymentOutPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderInvoicedPaymentOutPlanData2);

    orderPaymentOutDetails = orderPaymentPlan.new PaymentOutDetails(mainPage);
    orderPaymentOutDetails.assertCount(2);
    orderPaymentOutDetails
        .select(new PaymentDetailsData.Builder().expected("22.00").paid("22.00").build());
    orderPaymentOutDetails.assertData(orderInvoicedPaymentOutDetailsData);

    orderPaymentOutDetails
        .select(new PaymentDetailsData.Builder().paid("33.00").expected("55.00").build());
    orderPaymentOutDetails.assertData(orderInvoicedPaymentOutDetailsData2);

    // Check invoice payment plan
    invoice = new PurchaseInvoice(mainPage).open();
    invoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoiceNo).build());

    invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoicePaidPaymentOutPlanData);

    invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoicePaidPaymentOutDetailsData);

    // Execute Payment out and check
    payment = new PaymentOut(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentOutHeaderData.Builder().documentNo(paymentNo).build());
    payment.open();
    // TODO L1
    Sleep.trySleep(8000);
    payment.execute();
    payment.assertPaymentExecutedSuccessfully();
    // TODO L1
    Sleep.trySleep(12000);
    payment.assertData(paymentProcessInHeaderVerificationData3);

    // Check payment lines
    paymentInLines.select(new PaymentOutLinesData.Builder().paid("22.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData);
    paymentInLines.closeForm();

    paymentInLines.select(new PaymentOutLinesData.Builder().paid("33.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData2);
    paymentInLines.closeForm();

    // Post Payment out and check
    payment.open();
    payment.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines1.length);
    post.assertJournalLines2(journalEntryLines1);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines2.length);
    post.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Check payment made purchase order payment plan
    order = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentPlan = order.new PaymentOutPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderPaymentReceivedPaymentOutPlanData);

    orderInvoicedPaymentOutDetailsData.addDataField("status", "Payment Made");
    orderInvoicedPaymentOutDetailsData2.addDataField("status", "Payment Made");

    orderPaymentOutDetails = orderPaymentPlan.new PaymentOutDetails(mainPage);
    orderPaymentOutDetails.assertCount(2);
    orderPaymentOutDetails
        .select(new PaymentDetailsData.Builder().expected("22.00").paid("22.00").build());
    orderPaymentOutDetails.assertData(orderInvoicedPaymentOutDetailsData);

    orderPaymentOutDetails
        .select(new PaymentDetailsData.Builder().paid("33.00").expected("55.00").build());
    orderPaymentOutDetails.assertData(orderInvoicedPaymentOutDetailsData2);

    // Check payment made invoice payment plan
    invoice = new PurchaseInvoice(mainPage).open();
    invoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoiceNo).build());

    invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoicePaymentOutPlanData2);

    invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoicePaymentinDetailsData2);

    // Post invoice and check post
    invoice.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines3.length);
    post.assertJournalLines2(journalEntryLines3);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines4.length);
    post.assertJournalLines2(journalEntryLines4);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Restore the payment method configuration
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountData);
    paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod
        .select(new PaymentMethodData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build());
    paymentMethod.edit(new PaymentMethodData.Builder().automaticDeposit(false)
        .payoutExecutionType("Manual")
        .build());

    logger.info(
        "** End of test case [APRRegression36753Out1] Test regression 36753 Paying from order**");
  }
}
