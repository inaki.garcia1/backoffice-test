/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.financialaccount.classic;

import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.selectors.LocationSelectorData;
import com.openbravo.test.integration.erp.gui.popups.AddressSelectorPopUp;
import com.openbravo.test.integration.erp.gui.popups.AddressSelectorPopUpSc;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Account tab.
 *
 * @author elopio
 *
 */
public class AccountTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Account";
  /** The tab level. */
  private static final int LEVEL = 0;

  /* GUI components. */
  /** Identifier of the Account tab. */
  public static final String TAB = "tabname2845D761A8394468BD3BA4710AA888D4";

  /** Number of the name column. */
  public static final int COLUMN_NAME = 1;

  /** Identifier of the name text field. */
  private static final String TEXT_FIELD_NAME = "Name";
  /** Identifier of the currency combo box. */
  private static final String COMBO_BOX_CURRENCY = "reportC_Currency_ID_S";
  /** Identifier of the type combo box. */
  private static final String COMBO_BOX_TYPE = "reportType_S";
  /** Identifier of the bank code text field. */
  private static final String TEXT_FIELD_BANK_CODE = "Codebank";
  /** Identifier of the branch code text field. */
  private static final String TEXT_FIELD_BRANCH_CODE = "Codebranch";
  /** Identifier of the bank control digit text field. */
  private static final String TEXT_FIELD_BANK_CONTROL_DIGIT = "Bank_Digitcontrol";
  /** Identifier of the account control digit text field. */
  private static final String TEXT_FIELD_ACCOUNT_CONTROL_DIGIT = "Account_Digitcontrol";
  /** Identifier of the partial account no. text field. */
  private static final String TEXT_FIELD_PARTIAL_ACCOUNT_NO = "Codeaccount";
  /** Identifier of the matching algorithm combo box. */
  private static final String COMBO_BOX_MATCHING_ALGORITHM = "reportFIN_Matching_Algorithm_ID_S";

  /**
   * Class constructor.
   *
   */
  public AccountTab() {
    super(TITLE, LEVEL, TAB);
    addSubTab(new PaymentMethodTab());
  }

  /**
   * Create a new Account.
   *
   * @param data
   *          The data of the Account that will be created.
   */
  public void create(AccountData data) {
    clickCreateNewRecord();
    if (data.getDataField("name") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_NAME)
          .sendKeys(data.getDataField("name").toString());
    }
    if (data.getDataField("currency") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_CURRENCY))
          .selectByVisibleText(data.getDataField("currency").toString());
    }
    if (data.getDataField("type") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_TYPE))
          .selectByVisibleText(data.getDataField("type").toString());
    }
    if (data.getDataField("locationAddress") != null) {
      String windowHandle = SeleniumSingleton.INSTANCE.getWindowHandle();
      SeleniumSingleton.INSTANCE.findElementById(AddressSelectorPopUp.BUTTON_ADDRESS).click();
      final AddressSelectorPopUpSc addressSelector = new AddressSelectorPopUpSc();
      addressSelector.selectAddress((LocationSelectorData) data.getDataField("locationAddress"));
      SeleniumSingleton.INSTANCE.switchTo().window(windowHandle);
      waitForFrame();
    }
    if (data.getDataField("bankCode") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_BANK_CODE)
          .sendKeys(data.getDataField("bankCode").toString());
    }
    if (data.getDataField("branchCode") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_BRANCH_CODE)
          .sendKeys(data.getDataField("branchCode").toString());
    }
    if (data.getDataField("bankDigitcontrol") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_BANK_CONTROL_DIGIT)
          .sendKeys(data.getDataField("bankDigitcontrol").toString());
    }
    if (data.getDataField("accountDigitcontrol") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_ACCOUNT_CONTROL_DIGIT)
          .sendKeys(data.getDataField("accountDigitcontrol").toString());
    }
    if (data.getDataField("partialAccountNo") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_PARTIAL_ACCOUNT_NO)
          .sendKeys(data.getDataField("partialAccountNo").toString());
    }
    if (data.getDataField("matchingAlgorithm") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_MATCHING_ALGORITHM))
          .selectByVisibleText(data.getDataField("matchingAlgorithm").toString());
    }
    clickSave();
  }

}
