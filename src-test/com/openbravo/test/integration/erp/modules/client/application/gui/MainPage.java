/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 *  David Miguélez <david.miguelez@openbravo.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.erp.common.TimeoutConstants;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.gui.Page;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.NavigationBar;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.TabSet;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Executes and verify actions on OpenbravoERP the Main Page, part of the client application module.
 *
 * @author elopio
 *
 */
public class MainPage implements Page {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Identifiers. */
  /** Identifier of the main tab set. */
  private static final String REGISTRY_KEY_TAB_SET_MAIN = "org.openbravo.client.application.mainview.tabset";

  private static final String SYS_ADMIN_ROLE = "0";

  /* Components. */
  /** The pop up that appears when data is loading. */
  private final LoadingPopUp loadingPopUp;
  /** The navigation bar. */
  // FIXME there can be other types of navigation bars. This should be a
  // generic that implements navigation bar.
  private NavigationBar navigationBar;
  /** The main tab set. */
  private MainTabSet tabSetMain;
  /** The list of open views. */
  private Map<String, View> views;
  /** The current layout type. */
  private String currentLayoutType = "NEW";

  /**
   * Class constructor
   *
   */
  public MainPage() {
    loadingPopUp = new LoadingPopUp();
    waitForDataToLoad(TimeoutConstants.LOGGING_TIMEOUT);
    navigationBar = new NavigationBar();
    tabSetMain = new MainTabSet(TestRegistry.getObjectString(REGISTRY_KEY_TAB_SET_MAIN));
    views = new LinkedHashMap<String, View>();
    openLayout(ConfigurationProperties.INSTANCE.getInterfaceType());
  }

  /**
   * Select the main window.
   */
  public void selectWindow() {
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
  }

  /**
   * Open the classic layout.
   */
  public void openClassicLayout() {
    logger.info("Open the classic layout.");
    SeleniumSingleton.INSTANCE.get(ConfigurationProperties.INSTANCE.getOpenbravoBaseURL()
        + ConfigurationProperties.INSTANCE.getOpenbravoContext() + "/?mode=classic&test=true");
    currentLayoutType = "CLASSIC";
    waitForDataToLoad();
    // Instantiate again the components.
    navigationBar = new NavigationBar();
    tabSetMain = new MainTabSet(TestRegistry.getObjectString(REGISTRY_KEY_TAB_SET_MAIN));
    views = new LinkedHashMap<String, View>();
    closeInstancePurposePopUp();
    closeHeartbeatPopUp();
  }

  /**
   * Open the new layout.
   */
  public void openNewLayout() {
    logger.info("Open the new layout.");
    SeleniumSingleton.INSTANCE.get(ConfigurationProperties.INSTANCE.getOpenbravoBaseURL()
        + ConfigurationProperties.INSTANCE.getOpenbravoContext() + "/?mode=new&test=true");
    currentLayoutType = "NEW";
    waitForDataToLoad();
    // Instantiate again the components.
    navigationBar = new NavigationBar();
    tabSetMain = new MainTabSet(TestRegistry.getObjectString(REGISTRY_KEY_TAB_SET_MAIN));
    views = new LinkedHashMap<String, View>();
    closeInstancePurposePopUp();
    closeHeartbeatPopUp();
  }

  /**
   * Open the chosen layout if proper parameter was set:
   *
   * @param layoutType
   *          the layout type, either CLASSIC or NEW.
   *
   */
  public void openLayout(String layoutType) {
    if (!layoutType.equals(currentLayoutType)) {
      if (layoutType.equals("CLASSIC")) {
        openClassicLayout();
      } else if (layoutType.equals("NEW")) {
        openNewLayout();
      } else {
        throw new IllegalArgumentException(String.format(
            "Unknown layout type: '%s'. Valid values are 'CLASSIC' and 'NEW'.", layoutType));
      }
    }
  }

  /**
   * Check if the Main Page is on New layout.
   *
   * @return true if the Main Page is on New layout. Otherwise, false.
   */
  public boolean isOnNewLayout() {
    return currentLayoutType.equals("NEW");
  }

  /**
   * Get the navigation bar.
   *
   * @return the navigation bar.
   */
  public NavigationBar getNavigationBar() {
    return navigationBar;
  }

  /**
   * Get the main tab set.
   *
   * @return the main tab set.
   */
  public TabSet getTabSetMain() {
    return tabSetMain;
  }

  /**
   * Verify that the login succeeded.
   *
   * @param userName
   *          The user name of the logged user.
   */
  public void assertLogin(String userName) {
    closeInstancePurposePopUp();
    closeHeartbeatPopUp();
    // TODO verify all the visible elements.
    navigationBar.verifyUserName(userName);
  }

  /**
   *
   * Close the instance purpose pop up, if visible.
   */
  public void closeHeartbeatPopUp() {
    String scLocator = "//OBClassicPopup[ID='Heartbeat_dbXE8hjGuKyMefVf']/closeButton/";
    closePopUp(scLocator);
  }

  /**
   * Close the instance purpose pop up, if visible.
   */
  public void closeInstancePurposePopUp() {
    String scLocator = "//OBClassicPopup[ID='InstancePurpose_dbXE8hjGuKyMefVf']/closeButton/";
    closePopUp(scLocator);
  }

  /**
   * Closes the given pop up if exists or visible
   *
   * @param scLocator
   *          The SmartClient locator of the pop up
   */
  private void closePopUp(String scLocator) {
    String roleId = getRoleId();
    logger.debug("Role ID: {}", roleId);
    if (!SYS_ADMIN_ROLE.equals(roleId)) {
      return;
    }
    if (existsPopUp(scLocator)) {
      final WebElement popUp = SeleniumSingleton.INSTANCE.findElementByScLocator(scLocator);
      if (popUp.isEnabled()) {
        popUp.click();
      }
    }
  }

  /**
   * Checks whether the pop up matching with the given locator exists within 3 seconds
   *
   * @param scLocator
   *          The SmartClient locator of the pop up
   * @return The existence of the pop up
   */
  private boolean existsPopUp(String scLocator) {
    try {
      Sleep.setFluentWait(300, 3000, NoSuchElementException.class)
          .until(wd -> SeleniumSingleton.INSTANCE.findElementByScLocator(scLocator, true));
    } catch (WebDriverException wde) {
      return false;
    }
    return true;
  }

  private String getRoleId() {
    try {
      return (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn("OB.User.roleId");
    } catch (JavascriptException jse) {
      logger.info(
          "JavascriptException was thrown. Probably, 'OB' was not ready yet. Sleeping and trying again.");
      // TODO L3: Reduce the following static sleep if possible
      Sleep.trySleep(9000);
      return (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn("OB.User.roleId");
    }
  }

  /**
   * Change the user profile.
   *
   * @param newProfile
   *          The new profile of the user.
   */
  public void changeProfile(ProfileData newProfile) {
    logger.info("Change the profile information.");
    getNavigationBar().changeProfile(newProfile);
    waitForDataToLoad();
    closeInstancePurposePopUp();
    closeHeartbeatPopUp();
  }

  /**
   * Close the user profile.
   *
   */
  public void closeProfile() {
    logger.info("Close the profile.");
    getNavigationBar().closeProfile();
    waitForDataToLoad();
    closeInstancePurposePopUp();
    closeHeartbeatPopUp();
  }

  /**
   * Verify the user profile data.
   *
   * @param expectedProfileData
   *          The expected profile data of the user.
   */
  public void assertProfile(ProfileData expectedProfileData) {
    getNavigationBar().assertProfile(expectedProfileData);
  }

  /**
   * End the user session.
   */
  public void quit() {
    views.clear();
    logger.info("Log out.");
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    navigationBar.quit();
    logger.debug("Confirming the log out warning.");
    final WarnDialog warnDialog = new WarnDialog();
    warnDialog.waitUntilVisible();
    warnDialog.clickOk();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
  }

  /**
   * Add a view.
   *
   * @param view
   *          The view that will be added.
   */
  private void addView(View view) {
    views.put(view.getTitle(), view);
  }

  /**
   * Removes all of the mappings from this map called 'views' (optional operation). The map will be
   * empty after this call returns.
   *
   * @throws UnsupportedOperationException
   *           if the <tt>clear</tt> operation is not supported by this map
   */
  public void clearStoredViews() {
    views.clear();
  }

  public void openView(View view) {
    boolean opensInNewWindow = false;
    openView(view, opensInNewWindow);
  }

  /**
   * Open a view.
   *
   * @param view
   *          The view that will be opened.
   */
  public void openView(View view, boolean opensInNewWindow) {
    if (view.getMenuPath() != null) {
      openView(view, navigationBar.getViewLauncher(NavigationBar.ViewLaunchers.MENU),
          opensInNewWindow);
    } else {
      openView(view, navigationBar.getViewLauncher(NavigationBar.ViewLaunchers.QUICK_LAUNCH),
          opensInNewWindow);
    }
  }

  public OBParameterWindow<?> openParameterWindowFromMenu(
      GeneratedParameterWindowMenuItem<?> parameterWindowMenuItem) {
    if (parameterWindowMenuItem.getMenuPath() != null) {
      openView(parameterWindowMenuItem,
          navigationBar.getViewLauncher(NavigationBar.ViewLaunchers.MENU));
    } else {
      openView(parameterWindowMenuItem,
          navigationBar.getViewLauncher(NavigationBar.ViewLaunchers.QUICK_LAUNCH));
    }
    return parameterWindowMenuItem.loadParameterWindow();
  }

  /**
   * Check if a view is open.
   *
   * @param title
   *          The title of the view.
   * @return true if the view is open. Otherwise, false.
   */
  public boolean isViewOpen(String title) {
    return views.containsKey(title);
  }

  public void openView(View view, ViewLauncher viewLauncher) {
    boolean opensInNewWindow = false;
    openView(view, viewLauncher, opensInNewWindow);
  }

  /**
   * Open a view.
   *
   * @param view
   *          The view that will be opened.
   * @param viewLauncher
   *          The launcher that will be used to open the view.
   */
  public void openView(View view, ViewLauncher viewLauncher, boolean opensInNewWindow) {
    logger.debug("Opening a view.");
    if (isViewOpen(view.getTitle())) {
      logger.debug("The view is already open.");
      if (views.size() > 1) {
        // XXX currently selenium can work only with one view at a time.
        closeAllOtherViews(view.getTitle());
      }
      tabSetMain.selectTab(view.getTitle());
      Sleep.trySleep();
      // XXX when the view is already open, the objects are not the same.
      view.load();
      views.remove(view.getTitle());
      addView(view);
    } else {
      // XXX currently selenium can work only with one view at a time.
      if (!views.isEmpty()) {
        closeAllViews();
      }
      if (view.getMenuPath() != null) {
        viewLauncher.launch(view.getMenuPath());
      } else {
        viewLauncher.launch(view.getTitle());
      }
      if (opensInNewWindow) {
        // view opens in new window, it is not an Openbravo tab
        Sleep.trySleep();
      } else {
        loadView(view);
      }
    }
  }

  /**
   * Load an open view.
   *
   * @param view
   *          The open view to load.
   */
  public void loadView(View view) {
    tabSetMain.waitUntilTabIsOpen(view.getTitle());
    tabSetMain.selectTab(view.getTitle());
    if (view.toString()
        .contains(
            "com.openbravo.test.integration.erp.gui.general.application.sessionpreferences.SessionPreferencesWindow")) {
      logger.debug("Sleeping to avoid false positives in SessionPreferencesWindow");
      // TODO L00: Change the following static sleep from 50000 to 10000 once it is stable
      Sleep.trySleep(10000);
    }
    view.load();
    addView(view);
  }

  /**
   * Get an open view.
   *
   * @param title
   *          The title of the view.
   * @return the view.
   */
  public View getView(String title) {
    return views.get(title);
  }

  /**
   * Close all opened views.
   */
  public void closeAllViews() {
    logger.debug("Closing all views.");
    Collection<View> viewsCollection = new ArrayList<View>();
    viewsCollection.addAll(views.values());
    for (final View view : viewsCollection) {
      closeView(view.getTitle());
    }
  }

  /**
   * Close all the views that don't have this title.
   *
   * @param title
   *          The title of the view to conserve open.
   */
  public void closeAllOtherViews(String title) {
    logger.debug("Closing all other views.");
    Collection<View> viewsCollection = new ArrayList<View>();
    viewsCollection.addAll(views.values());
    for (final View view : viewsCollection) {
      if (!view.getTitle().equals(title)) {
        closeView(view.getTitle());
        Sleep.trySleep();
      }
    }
  }

  /**
   * Close a view.
   *
   * @param title
   *          The title of the view.
   */
  public void closeView(String title) {
    logger.debug("Closing the view with title {}.", title);
    if (views.containsKey(title)) {
      // XXX: <performance issues> Added custom closing of tab to ensure that the target tab is
      // closed whenever the Journal Entry tabs appear
      tabSetMain.closeTabAndWait(title);
      views.remove(title);
    } else {
      throw new IllegalArgumentException(
          String.format("The view with title %s is not open.", title));
    }
  }

  /**
   * Assert that a view is open.
   *
   * @param title
   *          The title of the tab.
   */
  public void assertOpenView(String title) {
    assertTrue("The view was never opened.", isViewOpen(title));
    tabSetMain.verifyOpenTab(title);
  }

  /**
   * Verify that a Community Logo is present.
   *
   */
  public void verifyCommunityLogo() {
    logger.info("Verifying Openbravo community logo.");
    navigationBar.verifyCommunityLogo();
  }

  /**
   * Verify that a Professional Logo is present.
   *
   */
  public void verifyProfessionalLogo() {
    logger.info("Verifying Openbravo professional logo.");
    navigationBar.verifyProfessionalLogo();
  }

  /**
   * Verify that a Your Company Logo is present.
   *
   */
  public void verifyYourCompanyLogo() {
    logger.info("Verifying your company logo.");
    navigationBar.verifyYourCompanyLogo();
  }

  /**
   * Wait for data to be fully loaded.
   */
  public void waitForDataToLoad() {
    waitForDataToLoad(TimeoutConstants.PAGE_LOAD_TIMEOUT);
  }

  /**
   * Wait for data to be fully loaded.
   *
   * @param timeout
   *          The time to wait for main page to load before reporting an error.
   */
  public void waitForDataToLoad(long timeout) {
    logger.debug("Waiting for Main Page to load.");
    // XXX switch to default window to workaround selenium issue 1438 -
    // http://code.google.com/p/selenium/issues/detail?id=1438
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    loadingPopUp.waitForDataToLoad(timeout);
    // // FIXME some weird errors are occurring. This wait might help.
    // Sleep.trySleep();
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting for data to load.");
        try {
          return (Boolean) SeleniumSingleton.INSTANCE.executeScript("return window.OB != null")
              && (Boolean) SeleniumSingleton.INSTANCE
                  .executeScript("return window.OB.TestRegistry != null");
        } catch (final WebDriverException exception) {
          // XXX Added because a common error related to the Selenium's evaluate.js.
          logger.error(exception);
          return false;
        }
      }
    });
  }

}
