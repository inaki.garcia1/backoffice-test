/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions1;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.data.sharedtabs.UsedCreditSourceData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 29615
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression29615Out extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PaymentOutHeaderData paymentOutHeaderData;
  PaymentOutHeaderData paymentOutHeaderVerificationData;
  AddPaymentPopUpData addPaymentVerificationData;
  PaymentOutHeaderData paymentOutHeaderVerificationData2;
  PaymentOutLinesData paymentOutLinesVerificationData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineData;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;
  PaymentOutHeaderData paymentOutHeaderData2;
  PaymentOutHeaderData paymentOutHeaderVerificationData3;
  AddPaymentPopUpData addPaymentVerificationData2;
  PaymentOutHeaderData paymentOutHeaderVerificationData4;
  PaymentOutLinesData paymentOutLinesVerificationData2;
  UsedCreditSourceData paymentOutUsedCreditVerificationData;
  PaymentOutHeaderData paymentOutHeaderVerificationData5;
  PaymentOutLinesData paymentOutLinesVerificationData3;
  UsedCreditSourceData paymentOutUsedCreditVerificationData2;
  PaymentOutHeaderData paymentOutHeaderVerificationData6;

  /**
   * Class constructor.
   *
   */
  public APRRegression29615Out(PaymentOutHeaderData paymentOutHeaderData,
      PaymentOutHeaderData paymentOutHeaderVerificationData,
      AddPaymentPopUpData addPaymentVerificationData,
      PaymentOutHeaderData paymentOutHeaderVerificationData2,
      PaymentOutLinesData paymentOutLinesVerificationData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineData,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      PaymentOutHeaderData paymentOutHeaderData2,
      PaymentOutHeaderData paymentOutHeaderVerificationData3,
      AddPaymentPopUpData addPaymentVerificationData2,
      PaymentOutHeaderData paymentOutHeaderVerificationData4,
      PaymentOutLinesData paymentOutLinesVerificationData2,
      UsedCreditSourceData paymentOutUsedCreditVerificationData,
      PaymentOutHeaderData paymentOutHeaderVerificationData5,
      PaymentOutLinesData paymentOutLinesVerificationData3,
      UsedCreditSourceData paymentOutUsedCreditVerificationData2,
      PaymentOutHeaderData paymentOutHeaderVerificationData6) {
    this.paymentOutHeaderData = paymentOutHeaderData;
    this.paymentOutHeaderVerificationData = paymentOutHeaderVerificationData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.paymentOutHeaderVerificationData2 = paymentOutHeaderVerificationData2;
    this.paymentOutLinesVerificationData = paymentOutLinesVerificationData;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineData = purchaseInvoiceLineData;
    this.purchaseInvoiceLineVerificationData = purchaseInvoiceLineVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.paymentOutHeaderData2 = paymentOutHeaderData2;
    this.paymentOutHeaderVerificationData3 = paymentOutHeaderVerificationData3;
    this.addPaymentVerificationData2 = addPaymentVerificationData2;
    this.paymentOutHeaderVerificationData4 = paymentOutHeaderVerificationData4;
    this.paymentOutLinesVerificationData2 = paymentOutLinesVerificationData2;
    this.paymentOutUsedCreditVerificationData = paymentOutUsedCreditVerificationData;
    this.paymentOutHeaderVerificationData5 = paymentOutHeaderVerificationData5;
    this.paymentOutLinesVerificationData3 = paymentOutLinesVerificationData3;
    this.paymentOutUsedCreditVerificationData2 = paymentOutUsedCreditVerificationData2;
    this.paymentOutHeaderVerificationData6 = paymentOutHeaderVerificationData6;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> purchaseOrderValues() {
    Object[][] data = new Object[][] { {
        new PaymentOutHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .generatedCredit("100.00")
            .build(),
        new PaymentOutHeaderData.Builder().organization("Spain")
            .documentType("AP Payment")
            .paymentMethod("1 (Spain)")
            .account("Spain Cashbook - EUR")
            .currency("EUR")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Vendor A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Cashbook - EUR")
            .c_currency_id("EUR")
            .actual_payment("100.00")
            .expected_payment("0.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("0.00")
            .total("0.00")
            .difference("100.00")
            .build(),
        new PaymentOutHeaderData.Builder().status("Withdrawn not Cleared")
            .amount("100.00")
            .usedCredit("0.00")
            .build(),
        new PaymentOutLinesData.Builder().dueDate("")
            .invoiceAmount("")
            .expected("")
            .paid("100.00")
            .orderno("")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PurchaseInvoiceHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),
        new PurchaseInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AP Invoice")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .paymentTerms("90 days")
            .paymentMethod("1 (Spain)")
            .priceList("Purchase")
            .build(),
        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Raw material A")
                .priceListVersionName("Purchase")
                .build())
            .invoicedQuantity("36")
            .tax("VAT 10%")
            .build(),
        new PurchaseInvoiceLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("72.00")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("79.20")
            .summedLineAmount("72.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new PaymentOutHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),
        new PaymentOutHeaderData.Builder().organization("Spain")
            .documentType("AP Payment")
            .paymentMethod("1 (Spain)")
            .generatedCredit("0.00")
            .account("Spain Cashbook - EUR")
            .currency("EUR")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Vendor A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Cashbook - EUR")
            .c_currency_id("EUR")
            .actual_payment("0.00")
            .expected_payment("79.20")
            .amount_gl_items("0.00")
            .amount_inv_ords("79.20")
            .total("79.20")
            .difference("20.80")
            .build(),
        new PaymentOutHeaderData.Builder().status("Payment Made")
            .amount("0.00")
            .usedCredit("79.20")
            .build(),
        new PaymentOutLinesData.Builder().dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .invoiceAmount("79.20")
            .expected("79.20")
            .paid("79.20")
            .orderno("")
            .glitemname("")
            .build(),
        new UsedCreditSourceData.Builder().amount("79.2").toCurrency("EUR").build(),

        new PaymentOutHeaderData.Builder().status("Withdrawn not Cleared")
            .amount("-20.80")
            .usedCredit("20.80")
            .organization("Spain")
            .documentType("AP Payment")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("1 (Spain)")
            .account("Spain Cashbook - EUR")
            .currency("EUR")
            .generatedCredit("0.00")
            .build(),
        new PaymentOutLinesData.Builder().dueDate("")
            .invoiceAmount("")
            .expected("")
            .paid("-20.80")
            .orderno("")
            .invoiceno("")
            .glitemname("")
            .build(),
        new UsedCreditSourceData.Builder().amount("20.8").toCurrency("EUR").build(),

        new PaymentOutHeaderData.Builder().status("Withdrawn not Cleared")
            .amount("100.00")
            .usedCredit("100.00")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 29615 - Payment Out flow
   *
   * Payment from Order
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression29615OutTest() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression29615Out] Test regression 29615 - Payment Out flow. **");

    PaymentOut paymentOut1 = new PaymentOut(mainPage).open();
    paymentOut1.create(paymentOutHeaderData);
    paymentOut1.assertSaved();
    paymentOut1.assertData(paymentOutHeaderVerificationData);
    AddPaymentProcess addPaymentProcess1 = paymentOut1.addDetailsOpen();
    String paymentNo = addPaymentProcess1.getParameterValue("payment_documentno").toString();
    addPaymentProcess1.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess1.getOrderInvoiceGrid().unselectAll();
    Sleep.trySleep(1000);
    addPaymentProcess1.process("Process Made Payment(s) and Withdrawal",
        "Leave the credit to be used later", addPaymentVerificationData);
    paymentOut1.assertPaymentCreatedSuccessfully();
    paymentOut1.assertData(paymentOutHeaderVerificationData2);
    PaymentOut.Lines paymentOutLines1 = paymentOut1.new Lines(mainPage);
    paymentOutLines1.assertCount(1);
    paymentOutLines1.assertData(paymentOutLinesVerificationData);

    PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    String invoiceNo = purchaseInvoice.getData("documentNo").toString();
    PurchaseInvoice.Lines purchaseInvoiceLines = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLines.create(purchaseInvoiceLineData);
    purchaseInvoiceLines.assertSaved();
    purchaseInvoiceLines.assertData(purchaseInvoiceLineVerificationData);
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData);

    PaymentOut paymentOut2 = new PaymentOut(mainPage).open();
    Sleep.trySleep(15000);
    paymentOut2.create(paymentOutHeaderData2);
    paymentOut2.assertSaved();
    paymentOut2.assertData(paymentOutHeaderVerificationData3);
    AddPaymentProcess addPaymentProcess2 = paymentOut2.addDetailsOpen();
    addPaymentProcess2.process("Invoices", invoiceNo, paymentNo,
        "Process Made Payment(s) and Withdrawal", "Refund amount to customer",
        addPaymentVerificationData2);
    paymentOut2.assertRefundedPaymentCreatedSuccessfully();
    paymentOut2.assertData(paymentOutHeaderVerificationData4);
    PaymentOut.Lines paymentOutLines2 = paymentOut2.new Lines(mainPage);
    paymentOutLines2.assertCount(1);
    paymentOutLines2.assertData((PaymentOutLinesData) paymentOutLinesVerificationData2
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo));
    PaymentOut.UsedCreditSource paymentOutUsedCredit2 = paymentOut2.new UsedCreditSource(mainPage);
    paymentOutUsedCredit2.assertCount(1);
    paymentOutUsedCredit2.assertData((UsedCreditSourceData) paymentOutUsedCreditVerificationData
        .addDataField("creditPaymentUsed", paymentNo));

    PaymentOut paymentOut3 = new PaymentOut(mainPage).open();
    paymentOut3.select(new PaymentOutHeaderData.Builder()
        .documentNo(String.valueOf(Integer.valueOf(paymentNo) + 2))
        .build());
    paymentOut3.assertData(paymentOutHeaderVerificationData5);
    PaymentOut.Lines paymentOutLines3 = paymentOut3.new Lines(mainPage);
    paymentOutLines3.assertCount(1);
    paymentOutLines3.assertData(paymentOutLinesVerificationData3);
    PaymentOut.UsedCreditSource paymentOutUsedCredit3 = paymentOut3.new UsedCreditSource(mainPage);
    paymentOutUsedCredit3.assertCount(1);
    paymentOutUsedCredit3.assertData((UsedCreditSourceData) paymentOutUsedCreditVerificationData2
        .addDataField("creditPaymentUsed", paymentNo));

    paymentOut1 = new PaymentOut(mainPage).open();
    paymentOut1.select(new PaymentOutHeaderData.Builder().documentNo(paymentNo).build());
    paymentOut1.assertData(paymentOutHeaderVerificationData6);
    paymentOutLines1 = paymentOut1.new Lines(mainPage);
    paymentOutLines1.assertCount(1);
    paymentOutLines1.assertData(paymentOutLinesVerificationData);
    PaymentOut.UsedCreditSource paymentOutUsedCredit1 = paymentOut1.new UsedCreditSource(mainPage);
    paymentOutUsedCredit1.assertCount(0);

    logger.info(
        "** End of test case [APRRegression29615Out] Test regression 29615 - Payment Out flow. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
