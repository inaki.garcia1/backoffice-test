/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:10
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.applicationdictionary.automatedtest.command;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for CommandData
 *
 * @author plujan
 *
 */
public class CommandData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the assertion value
     *
     * Description: Assertion
     *
     * @param value
     *          The assertion value.
     * @return The builder for this class.
     */
    public Builder assertion(Boolean value) {
      this.dataFields.put("assertion", value);
      return this;
    }

    /**
     * Set the checkIfWritable value
     *
     * Description: Check if Writable
     *
     * @param value
     *          The checkIfWritable value.
     * @return The builder for this class.
     */
    public Builder checkIfWritable(Boolean value) {
      this.dataFields.put("checkIfWritable", value);
      return this;
    }

    /**
     * Set the tabReminder value
     *
     * Description: Tab Reminder
     *
     * @param value
     *          The tabReminder value.
     * @return The builder for this class.
     */
    public Builder tabReminder(Boolean value) {
      this.dataFields.put("tabReminder", value);
      return this;
    }

    /**
     * Set the frameReminder value
     *
     * Description: Frame Reminder
     *
     * @param value
     *          The frameReminder value.
     * @return The builder for this class.
     */
    public Builder frameReminder(Boolean value) {
      this.dataFields.put("frameReminder", value);
      return this;
    }

    /**
     * Set the windowReminder value
     *
     * Description: Window Reminder
     *
     * @param value
     *          The windowReminder value.
     * @return The builder for this class.
     */
    public Builder windowReminder(Boolean value) {
      this.dataFields.put("windowReminder", value);
      return this;
    }

    /**
     * Set the prefix value
     *
     * Description: Prefix
     *
     * @param value
     *          The prefix value.
     * @return The builder for this class.
     */
    public Builder prefix(Boolean value) {
      this.dataFields.put("prefix", value);
      return this;
    }

    /**
     * Set the lineInterchange value
     *
     * Description: Line Interchange
     *
     * @param value
     *          The lineInterchange value.
     * @return The builder for this class.
     */
    public Builder lineInterchange(Boolean value) {
      this.dataFields.put("lineInterchange", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the help3 value
     *
     * Description: A step by step help description to guide the user through processes and tasks.
     *
     * @param value
     *          The help3 value.
     * @return The builder for this class.
     */
    public Builder help3(String value) {
      this.dataFields.put("help3", value);
      return this;
    }

    /**
     * Set the help2 value
     *
     * Description: A step by step help description to guide the user through processes and tasks.
     *
     * @param value
     *          The help2 value.
     * @return The builder for this class.
     */
    public Builder help2(String value) {
      this.dataFields.put("help2", value);
      return this;
    }

    /**
     * Set the help1 value
     *
     * Description: A step by step help description to guide the user through processes and tasks.
     *
     * @param value
     *          The help1 value.
     * @return The builder for this class.
     */
    public Builder help1(String value) {
      this.dataFields.put("help1", value);
      return this;
    }

    /**
     * Set the argNo value
     *
     * Description: Arg. No.
     *
     * @param value
     *          The argNo value.
     * @return The builder for this class.
     */
    public Builder argNo(String value) {
      this.dataFields.put("argNo", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public CommandData build() {
      return new CommandData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private CommandData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
