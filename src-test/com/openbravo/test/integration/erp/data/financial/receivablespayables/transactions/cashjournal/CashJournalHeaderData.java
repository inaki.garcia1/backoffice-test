/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.cashjournal;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for CashJournalHeaderData
 *
 * @author plujan
 *
 */
public class CashJournalHeaderData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the statementDifference value
     *
     * Description: Difference between statement ending balance and actual ending balance
     *
     * @param value
     *          The statementDifference value.
     * @return The builder for this class.
     */
    public Builder statementDifference(String value) {
      this.dataFields.put("statementDifference", value);
      return this;
    }

    /**
     * Set the transactionDate value
     *
     * Description: The date the transaction is carried out and registered in the cash journal.
     *
     * @param value
     *          The transactionDate value.
     * @return The builder for this class.
     */
    public Builder transactionDate(String value) {
      this.dataFields.put("transactionDate", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the endingBalance value
     *
     * Description: Ending or closing balance
     *
     * @param value
     *          The endingBalance value.
     * @return The builder for this class.
     */
    public Builder endingBalance(String value) {
      this.dataFields.put("endingBalance", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the cashbook value
     *
     * Description: A document used to manage all cash transactions.
     *
     * @param value
     *          The cashbook value.
     * @return The builder for this class.
     */
    public Builder cashbook(String value) {
      this.dataFields.put("cashbook", value);
      return this;
    }

    /**
     * Set the beginningBalance value
     *
     * Description: Balance prior to any transactions
     *
     * @param value
     *          The beginningBalance value.
     * @return The builder for this class.
     */
    public Builder beginningBalance(String value) {
      this.dataFields.put("beginningBalance", value);
      return this;
    }

    /**
     * Set the accountingDate value
     *
     * Description: The date this transaction is recorded for in the general ledger.
     *
     * @param value
     *          The accountingDate value.
     * @return The builder for this class.
     */
    public Builder accountingDate(String value) {
      this.dataFields.put("accountingDate", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public CashJournalHeaderData build() {
      return new CashJournalHeaderData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private CashJournalHeaderData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
