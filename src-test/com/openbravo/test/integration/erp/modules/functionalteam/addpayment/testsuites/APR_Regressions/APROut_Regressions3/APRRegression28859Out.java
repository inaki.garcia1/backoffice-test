/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions3;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;

/**
 * Test regression 28859
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression28859Out extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineData;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineData2;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData2;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData2;
  AddPaymentPopUpData addPaymentVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData3;

  /**
   * Class constructor.
   *
   */
  public APRRegression28859Out(PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineData,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineData2,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData2,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData2,
      AddPaymentPopUpData addPaymentVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData3) {
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineData = purchaseInvoiceLineData;
    this.purchaseInvoiceLineVerificationData = purchaseInvoiceLineVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineData2 = purchaseInvoiceLineData2;
    this.purchaseInvoiceLineVerificationData2 = purchaseInvoiceLineVerificationData2;
    this.completedPurchaseInvoiceHeaderVerificationData2 = completedPurchaseInvoiceHeaderVerificationData2;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData3 = completedPurchaseInvoiceHeaderVerificationData3;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new PurchaseInvoiceHeaderData.Builder().transactionDocument("Reversed Purchase Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),
        new PurchaseInvoiceHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .paymentTerms("90 days")
            .paymentMethod("1 (Spain)")
            .priceList("Purchase")
            .build(),
        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Raw material A")
                .priceListVersionName("Purchase")
                .build())
            .invoicedQuantity("-80")
            .build(),
        new PurchaseInvoiceLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("-160.00")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("-176.00")
            .summedLineAmount("-160.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Raw material B")
                .priceListVersionName("Purchase")
                .build())
            .invoicedQuantity("-100")
            .build(),
        new PurchaseInvoiceLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("-200.00")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("-220.00")
            .summedLineAmount("-200.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Vendor A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Cashbook - EUR")
            .c_currency_id("EUR")
            .actual_payment("-176.00")
            .expected_payment("-176.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("-176.00")
            .total("-176.00")
            .difference("0.00")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("-176.00")
            .summedLineAmount("-160.00")
            .currency("EUR")
            .paymentComplete(true)
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 28859 - Payment In flow
   *
   * @throws ParseException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void APRRegression28859OutTest() throws ParseException, OpenbravoERPTestException {
    logger.info(
        "** Start of test case [APRRegression28859Out] Test regression 28859 - Payment In flow. **");

    PurchaseInvoice purchaseInvoice1 = new PurchaseInvoice(mainPage).open();
    purchaseInvoice1.create(purchaseInvoiceHeaderData);
    purchaseInvoice1.assertSaved();
    purchaseInvoice1.assertData(purchaseInvoiceHeaderVerificationData);
    String invoice1No = purchaseInvoice1.getData("documentNo").toString();
    PurchaseInvoice.Lines purchaseInvoiceLines1 = purchaseInvoice1.new Lines(mainPage);
    purchaseInvoiceLines1.create(purchaseInvoiceLineData);
    purchaseInvoiceLines1.assertSaved();
    purchaseInvoiceLines1.assertData(purchaseInvoiceLineVerificationData);
    purchaseInvoice1.complete();
    purchaseInvoice1.assertProcessCompletedSuccessfully2();
    purchaseInvoice1.assertData(completedPurchaseInvoiceHeaderVerificationData);
    purchaseInvoice1.closeForm();

    PurchaseInvoice purchaseInvoice2 = new PurchaseInvoice(mainPage).open();
    purchaseInvoice2.create(purchaseInvoiceHeaderData);
    purchaseInvoice2.assertSaved();
    purchaseInvoice2.assertData(purchaseInvoiceHeaderVerificationData);
    String invoice2No = purchaseInvoice2.getData("documentNo").toString();
    PurchaseInvoice.Lines purchaseInvoiceLines2 = purchaseInvoice2.new Lines(mainPage);
    purchaseInvoiceLines2.create(purchaseInvoiceLineData2);
    purchaseInvoiceLines2.assertSaved();
    purchaseInvoiceLines2.assertData(purchaseInvoiceLineVerificationData2);
    purchaseInvoice2.complete();
    purchaseInvoice2.assertProcessCompletedSuccessfully2();
    purchaseInvoice2.assertData(completedPurchaseInvoiceHeaderVerificationData2);
    purchaseInvoice2.closeForm();

    purchaseInvoice1.open();
    purchaseInvoice1.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoice1No).build());
    AddPaymentProcess addPaymentProcess = purchaseInvoice1.openAddPayment();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.assertData(addPaymentVerificationData);
    addPaymentProcess.process("Process Made Payment(s) and Withdrawal");
    purchaseInvoice1.assertPaymentCreatedSuccessfully();
    purchaseInvoice1.assertData(completedPurchaseInvoiceHeaderVerificationData3);
    purchaseInvoice1.closeForm();

    purchaseInvoice2.open();
    purchaseInvoice2.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoice2No).build());
    purchaseInvoice2.assertData(completedPurchaseInvoiceHeaderVerificationData2);
    purchaseInvoice2.closeForm();

    logger.info(
        "** End of test case [APRRegression28859Out] Test regression 28859 - Payment In flow. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
