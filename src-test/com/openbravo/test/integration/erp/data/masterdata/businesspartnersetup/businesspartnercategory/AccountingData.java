/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.businesspartnersetup.businesspartnercategory;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.AccountSelectorData;

/**
 *
 * Class for AccountingData
 *
 * @author plujan
 *
 */
public class AccountingData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the writeOff value
     *
     * Description: Account for Receivables write-off
     *
     * @param value
     *          The writeOff value.
     * @return The builder for this class.
     */
    public Builder writeOff(AccountSelectorData value) {
      this.dataFields.put("writeOff", value);
      return this;
    }

    /**
     * Set the vendorPrepayment value
     *
     * Description: An account number in the general ledger used to mark transactions with this
     * business partner.
     *
     * @param value
     *          The vendorPrepayment value.
     * @return The builder for this class.
     */
    public Builder vendorPrepayment(AccountSelectorData value) {
      this.dataFields.put("vendorPrepayment", value);
      return this;
    }

    /**
     * Set the vendorLiability value
     *
     * Description: An account number in the general ledger used to mark transactions with this
     * business partner.
     *
     * @param value
     *          The vendorLiability value.
     * @return The builder for this class.
     */
    public Builder vendorLiability(AccountSelectorData value) {
      this.dataFields.put("vendorLiability", value);
      return this;
    }

    /**
     * Set the nonInvoicedReceipts value
     *
     * Description: Account for not-invoiced Material Receipts
     *
     * @param value
     *          The nonInvoicedReceipts value.
     * @return The builder for this class.
     */
    public Builder nonInvoicedReceipts(AccountSelectorData value) {
      this.dataFields.put("nonInvoicedReceipts", value);
      return this;
    }

    /**
     * Set the customerReceivablesNo value
     *
     * Description: An account number in the general ledger to mark transactions with a business
     * partner.
     *
     * @param value
     *          The customerReceivablesNo value.
     * @return The builder for this class.
     */
    public Builder customerReceivablesNo(AccountSelectorData value) {
      this.dataFields.put("customerReceivablesNo", value);
      return this;
    }

    /**
     * Set the customerPrepayment value
     *
     * Description: x not implemented
     *
     * @param value
     *          The customerPrepayment value.
     * @return The builder for this class.
     */
    public Builder customerPrepayment(AccountSelectorData value) {
      this.dataFields.put("customerPrepayment", value);
      return this;
    }

    /**
     * Set the businessPartnerCategory value
     *
     * Description: A classification of business partners based on defined similarities.
     *
     * @param value
     *          The businessPartnerCategory value.
     * @return The builder for this class.
     */
    public Builder businessPartnerCategory(String value) {
      this.dataFields.put("businessPartnerCategory", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public AccountingData build() {
      return new AccountingData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private AccountingData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
