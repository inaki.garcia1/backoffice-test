/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.procurement.transactions.managerequisitions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.procurement.transactions.managerequisitions.ManageRequisitionsHeaderData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Manage Requisitions Header tab.
 *
 * @author elopio
 *
 */
public class ManageRequisitionsHeaderTab extends GeneratedTab<ManageRequisitionsHeaderData> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The tab title. */
  static final String TITLE = "Manage Requisitions";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "1004400001";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /* Registry keys. */
  /** Registry key of the create purchase order button. */
  private static final String REGISTRY_KEY_BUTTON_CREATE_PURCHASE_ORDER = "org.openbravo.client.application.toolbar.button.createPO.1004400001";

  /* Toolbar buttons. */
  /** The create purchase order button. */
  private static Button buttonCreatePurchaseOrder;

  /**
   * Class constructor
   *
   */
  public ManageRequisitionsHeaderTab() {
    super(TITLE, IDENTIFIER);
    addChildTab(new ManageRequisitionsLinesTab(this));
    buttonCreatePurchaseOrder = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_CREATE_PURCHASE_ORDER));
  }

  /**
   * Create a purchase order.
   *
   * @param orderDate
   *          The date of the order.
   * @param organization
   *          The name of the organization.
   * @param warehouse
   *          The name of the warehouse.
   */
  public void createPurchaseOrder(String orderDate, String organization, String warehouse) {
    logger.debug("Creating purchase order.");
    buttonCreatePurchaseOrder.click();
    CreatePurchaseOrderPopUp popUp = new CreatePurchaseOrderPopUp();
    popUp.createPurchaseOrder(orderDate, organization, warehouse);
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }

  /**
   * Assert that the order creation message was displayed.
   *
   */
  public void assertOrderCreateSuccessfully() {
    assertProcessCompletedSuccessfullyContentMatch("Created: 1, Order No\\. .*");
  }
}
