/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;

/**
 *
 * Class for BankStatementLinesData
 *
 * @author plujan
 *
 */
public class BankStatementLinesData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the lineNo value
     *
     * Description: A line stating the position of this request in the document.
     *
     * @param value
     *          The lineNo value.
     * @return The builder for this class.
     */
    public Builder lineNo(String value) {
      this.dataFields.put("lineNo", value);
      return this;
    }

    /**
     * Set the referenceNo value
     *
     * Description: The number for a specific reference.
     *
     * @param value
     *          The referenceNo value.
     * @return The builder for this class.
     */
    public Builder referenceNo(String value) {
      this.dataFields.put("referenceNo", value);
      return this;
    }

    /**
     * Set the matchingtype value
     *
     * Description: Type of matching for the given line
     *
     * @param value
     *          The matchingtype value.
     * @return The builder for this class.
     */
    public Builder matchingtype(String value) {
      this.dataFields.put("matchingtype", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the financialAccountTransaction value
     *
     * Description: Financial account transaction
     *
     * @param value
     *          The financialAccountTransaction value.
     * @return The builder for this class.
     */
    public Builder financialAccountTransaction(String value) {
      this.dataFields.put("financialAccountTransaction", value);
      return this;
    }

    /**
     * Set the bankStatement value
     *
     * Description: Bank statements related to a financial account
     *
     * @param value
     *          The bankStatement value.
     * @return The builder for this class.
     */
    public Builder bankStatement(String value) {
      this.dataFields.put("bankStatement", value);
      return this;
    }

    /**
     * Set the dramount value
     *
     * Description: Amount Debited
     *
     * @param value
     *          The dramount value.
     * @return The builder for this class.
     */
    public Builder dramount(String value) {
      this.dataFields.put("dramount", value);
      return this;
    }

    /**
     * Set the transactionDate value
     *
     * Description: The date that a specified transaction is entered into the application.
     *
     * @param value
     *          The transactionDate value.
     * @return The builder for this class.
     */
    public Builder transactionDate(String value) {
      this.dataFields.put("transactionDate", value);
      return this;
    }

    /**
     * Set the cramount value
     *
     * Description: Amount credited
     *
     * @param value
     *          The cramount value.
     * @return The builder for this class.
     */
    public Builder cramount(String value) {
      this.dataFields.put("cramount", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the bpartnername value
     *
     * Description: Business Partner Name
     *
     * @param value
     *          The bpartnername value.
     * @return The builder for this class.
     */
    public Builder bpartnername(String value) {
      this.dataFields.put("bpartnername", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the GL Item value
     *
     * Description: GL Item
     *
     * @param value
     *          The GL Item value.
     * @return The builder for this class.
     */
    public Builder gLItem(String value) {
      this.dataFields.put("gLItem", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: Description
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public BankStatementLinesData build() {
      return new BankStatementLinesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private BankStatementLinesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
