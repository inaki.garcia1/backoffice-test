/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.product;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ShipmentReceiptLineSelectorData;

/**
 *
 * Class for CostingData
 *
 * @author plujan
 *
 */
public class CostingData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the manual value
     *
     * Description: A task or process completed directly by the user not automatically by the
     * application.
     *
     * @param value
     *          The manual value.
     * @return The builder for this class.
     */
    public Builder manual(Boolean value) {
      this.dataFields.put("manual", value);
      return this;
    }

    /**
     * Set the endingDate value
     *
     * Description: A parameter stating the ending time range related for a specified request query
     * etc.
     *
     * @param value
     *          The endingDate value.
     * @return The builder for this class.
     */
    public Builder endingDate(String value) {
      this.dataFields.put("endingDate", value);
      return this;
    }

    /**
     * Set the cost value
     *
     * Description: A charge related to conducting business.
     *
     * @param value
     *          The cost value.
     * @return The builder for this class.
     */
    public Builder cost(String value) {
      this.dataFields.put("cost", value);
      return this;
    }

    /**
     * Set the permanent value
     *
     * Description: Permanent
     *
     * @param value
     *          The permanent value.
     * @return The builder for this class.
     */
    public Builder permanent(Boolean value) {
      this.dataFields.put("permanent", value);
      return this;
    }

    /**
     * Set the costType value
     *
     * Description: A distinct cost characteristic used for processes.
     *
     * @param value
     *          The costType value.
     * @return The builder for this class.
     */
    public Builder costType(String value) {
      this.dataFields.put("costType", value);
      return this;
    }

    /**
     * Set the totalMovementQuantity value
     *
     * Description: The final quantity to be moved based on the related business process.
     *
     * @param value
     *          The totalMovementQuantity value.
     * @return The builder for this class.
     */
    public Builder totalMovementQuantity(String value) {
      this.dataFields.put("totalMovementQuantity", value);
      return this;
    }

    /**
     * Set the price value
     *
     * Description: The cost or value of a good or service.
     *
     * @param value
     *          The price value.
     * @return The builder for this class.
     */
    public Builder price(String value) {
      this.dataFields.put("price", value);
      return this;
    }

    /**
     * Set the quantity value
     *
     * Description: The number of a certain item.
     *
     * @param value
     *          The quantity value.
     * @return The builder for this class.
     */
    public Builder quantity(String value) {
      this.dataFields.put("quantity", value);
      return this;
    }

    /**
     * Set the goodsShipmentLine value
     *
     * Description: A statement displaying one item charge or movement in a receipt.
     *
     * @param value
     *          The goodsShipmentLine value.
     * @return The builder for this class.
     */
    public Builder goodsShipmentLine(ShipmentReceiptLineSelectorData value) {
      this.dataFields.put("goodsShipmentLine", value);
      return this;
    }

    /**
     * Set the startingDate value
     *
     * Description: A parameter stating the starting time range related to a specified request.
     *
     * @param value
     *          The startingDate value.
     * @return The builder for this class.
     */
    public Builder startingDate(String value) {
      this.dataFields.put("startingDate", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public CostingData build() {
      return new CostingData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private CostingData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
