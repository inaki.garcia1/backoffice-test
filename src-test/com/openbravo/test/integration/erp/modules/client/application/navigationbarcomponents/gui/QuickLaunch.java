/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriverException;

import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.client.application.gui.ViewLauncher;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.SelectItem;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verify actions on OpenbravoERP Quick Launch widget added by Navigation Bar
 * Components module.
 *
 * There are three ways to open a window from this widget: typing it's name on the combo box,
 * selecting the name from the combo box and selecting the name from the recent windows list if it
 * was recently opened. The first way is the default.
 *
 * @author elopio
 *
 */
public class QuickLaunch implements ViewLauncher {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Component elements. */
  /** The quick launch button. */
  private final Button buttonQuickLaunch;
  /** The recent items form. */
  private RecentForm recentItemsForm;
  /** The role combo box. */
  private SelectItem comboBoxQuickLaunch;

  /* Registry keys. */
  /** Registry key of the Quick Launch button. */
  private static final String REGISTRY_KEY_BUTTON_QUICK_LAUNCH = "UINAVBA_RecentLaunchList_BUTTON";
  /** Registry key of the Quick Launch recent items form. */
  private static final String REGISTRY_KEY_RECENT_FORM_QUICK_LAUNCH = "UINAVBA_RecentLaunchList_RECENTFORM";
  /** Registry key of the Quick Launch combo box. */
  private static final String REGISTRY_KEY_COMBO_BOX_QUICK_LAUNCH = "UINAVBA_RecentLaunchList_FIELD";

  /**
   * Class constructor.
   *
   * @param selenium
   *          the selenium object that will be used to execute actions on the application.
   */
  QuickLaunch() {
    this.buttonQuickLaunch = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_QUICK_LAUNCH));
  }

  /**
   * Check if the widget is open.
   *
   * @return true if the widget is open. Otherwise, false.
   */
  public boolean isOpen() {
    // XXX it would be better to use the parent component.
    return comboBoxQuickLaunch != null && comboBoxQuickLaunch.isPresent()
        && comboBoxQuickLaunch.isVisible();
  }

  /**
   * Open the widget.
   */
  public void open() {
    if (!isOpen()) {
      logger.debug("Opening the quick launcher.");
      buttonQuickLaunch.smartClientClick();
      comboBoxQuickLaunch = Sleep.setFluentWait(300, 1000, WebDriverException.class)
          .until(wd -> new SelectItem(
              TestRegistry.getObjectString(REGISTRY_KEY_COMBO_BOX_QUICK_LAUNCH)));
      try {
        waitUntilVisible();
      } catch (TimeoutException toe) {
        SeleniumSingleton.INSTANCE.switchTo().defaultContent();
        waitUntilVisible();
      }
      recentItemsForm = new RecentForm(
          TestRegistry.getObjectString(REGISTRY_KEY_RECENT_FORM_QUICK_LAUNCH));
    }
  }

  /**
   * Close the widget.
   */
  public void close() {
    if (isOpen()) {
      logger.debug("Closing the quick launcher.");
      buttonQuickLaunch.smartClientClick();
      comboBoxQuickLaunch = null;
      recentItemsForm = null;
      // TODO wait?
    }
  }

  /**
   * Wait until the widget is visible.
   */
  public void waitUntilVisible() {
    logger.debug("Waiting for the quick launcher widget to open.");
    // FIXME wait for the parent component.
    comboBoxQuickLaunch.waitUntilVisible();
  }

  /**
   * Open a window from quick launcher.
   *
   * @param windowName
   *          The name of the window to open.
   */
  public void quickLaunch(String windowName) {
    logger.info("Launch the window '{}'.", windowName);
    quickLaunchFromTextField(windowName);
  }

  /**
   * Open a window from quick launcher, selecting the window name on the combo box.
   *
   * @param windowName
   *          The name of the window to open.
   */
  public void quickLaunchFromComboBox(String windowName) {
    open();
    logger.debug("Launching the window '{}' selecting its name on the combo box.", windowName);
    comboBoxQuickLaunch.selectItem(windowName, "_identifier");
    // TODO: <chromedriver> Static sleep added to solve issue with some of the NAV tests when run on
    // Chrome
    if (SeleniumSingleton.runsChrome()) {
      Sleep.trySleep(3000);
    }
  }

  /**
   * Open a window from quick launcher, typing the window name on the combo box.
   *
   * @param windowName
   *          The name of the window to open.
   */
  public void quickLaunchFromTextField(String windowName) {
    open();
    logger.debug("Launching the window '{}' typing its name on the combo box.", windowName);
    comboBoxQuickLaunch.enterText(windowName);
    Sleep.trySleep();
    comboBoxQuickLaunch.pressEnter();
  }

  /**
   * Open a window from quick launcher, selecting it from the recent form.
   *
   * @param windowName
   *          The name of the windowName to open.
   */
  public void quickLaunchFromRecentItems(String windowName) {
    open();
    // XXX click somewhere to hide the tool tip that might hide the link. This can be removed when
    // the canvas hover is implemented.
    comboBoxQuickLaunch.smartClientClick();
    logger.debug("Launching the window '{}' selecting its name on the recent items list.",
        windowName);
    recentItemsForm.selectRecentItem(windowName);
  }

  /**
   * Launch a view.
   *
   * @param menuPath
   *          The path on the menu to the view that will be launched.
   */
  @Override
  public void launch(String... menuPath) {
    // The window is opened only with the last part of the menu path, i.e, the window name.
    quickLaunch(menuPath[menuPath.length - 1]);
  }

}
