/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Atul Gaware <atul.gaware@openbravo.com>,
 ************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.procurement.testsuites.PRO_Regressions;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.DiscountsData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.TaxData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.selenium.Sleep;

@RunWith(Parameterized.class)
public class PRORegression34392 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The purchase invoice header data */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;

  /** The data to verify creation of the purchase invoice header */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;

  /** The purchase invoice Line Data */
  PurchaseInvoiceLinesData purchaseInvoiceLineData;

  /** The purchase invoice Line verification data */
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData;

  /** The purchase invoice discount data */
  DiscountsData purchaseInvoiceDiscountData;

  /** The completed purchase invoice header verification data */
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;

  /** The purchase invoice discount data line filter */
  PurchaseInvoiceLinesData purchaseInvoiceDiscountLineFilterData;

  /** The purchase invoice discount line verification data */
  PurchaseInvoiceLinesData purchaseInvoiceDiscountLineVerificationData;

  /** The purchase invoice tax data */
  TaxData purchaseInvoiceTaxData;

  /** The voided purchase invoice header verification data */
  PurchaseInvoiceHeaderData voidPurchaseInvoiceHeaderVerificationData;

  /** The reversed purchase invoice header verification data */
  PurchaseInvoiceHeaderData reversedPurchaseInvoiceHeaderVerificationData;

  /** The reverse purchase invoice line data filter */
  PurchaseInvoiceLinesData reversePurchaseInvoiceLineFilterData;

  /** The reversed purchase invoice line verification data */
  PurchaseInvoiceLinesData reversedPurchaseInvoiceLineVerificationData;

  /** The reversed purchase invoice Discount line verification data */
  PurchaseInvoiceLinesData reversedPurchaseInvoiceDiscountLineVerificationData;

  /** The reversed purchase invoice tax data */
  TaxData reversedPurchaseInvoiceTaxData;

  public PRORegression34392(PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineData,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData,
      DiscountsData purchaseInvoiceDiscountData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceDiscountLineFilterData,
      PurchaseInvoiceLinesData purchaseInvoiceDiscountLineVerificationData,
      TaxData purchaseInvoiceTaxData,
      PurchaseInvoiceHeaderData voidPurchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceHeaderData reversedPurchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData reversePurchaseInvoiceLineFilterData,
      PurchaseInvoiceLinesData reversedPurchaseInvoiceLineVerificationData,
      PurchaseInvoiceLinesData reversedPurchaseInvoiceDiscountLineVerificationData,
      TaxData reversedPurchaseInvoiceTaxData) {

    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineData = purchaseInvoiceLineData;
    this.purchaseInvoiceLineVerificationData = purchaseInvoiceLineVerificationData;
    this.purchaseInvoiceDiscountData = purchaseInvoiceDiscountData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceDiscountLineFilterData = purchaseInvoiceDiscountLineFilterData;
    this.purchaseInvoiceDiscountLineVerificationData = purchaseInvoiceDiscountLineVerificationData;
    this.purchaseInvoiceTaxData = purchaseInvoiceTaxData;
    this.voidPurchaseInvoiceHeaderVerificationData = voidPurchaseInvoiceHeaderVerificationData;
    this.reversedPurchaseInvoiceHeaderVerificationData = reversedPurchaseInvoiceHeaderVerificationData;
    this.reversedPurchaseInvoiceLineVerificationData = reversedPurchaseInvoiceLineVerificationData;
    this.reversePurchaseInvoiceLineFilterData = reversePurchaseInvoiceLineFilterData;
    this.reversedPurchaseInvoiceDiscountLineVerificationData = reversedPurchaseInvoiceDiscountLineVerificationData;
    this.reversedPurchaseInvoiceTaxData = reversedPurchaseInvoiceTaxData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> purchaseInvoiceValues() {
    Object[][] data = new Object[][] { {

        new PurchaseInvoiceHeaderData.Builder().organization("USA")
            .transactionDocument("AP Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),
        new PurchaseInvoiceHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .paymentTerms("90 days")
            .priceList("Purchase")
            .build(),
        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Raw material A")
                .priceListVersionName("Purchase")
                .build())
            .invoicedQuantity("100")
            .tax("VAT 10% USA")
            .build(),
        new PurchaseInvoiceLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("200.00")
            .build(),
        new DiscountsData.Builder().discount("Discount 10%").build(),
        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .summedLineAmount("180.00")
            .grandTotalAmount("198.00")
            .currency("EUR")
            .build(),
        new PurchaseInvoiceLinesData.Builder().lineNo("20").invoicedQuantity("1").build(),
        new PurchaseInvoiceLinesData.Builder().uOM("Unit")
            .unitPrice("-20.00")
            .lineNetAmount("-20.00")
            .tax("VAT 10% USA")
            .build(),
        new TaxData.Builder().tax("VAT 10% USA - VAT 10%")
            .taxAmount("18.00")
            .taxableAmount("180.00")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().documentStatus("Voided")
            .summedLineAmount("180.00")
            .grandTotalAmount("198.00")
            .currency("EUR")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().documentStatus("Voided")
            .summedLineAmount("-180.00")
            .grandTotalAmount("-198.00")
            .currency("EUR")
            .build(),
        new PurchaseInvoiceLinesData.Builder().lineNo("10").invoicedQuantity("-100").build(),
        new PurchaseInvoiceLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("-200.00")
            .tax("VAT 10% USA")
            .build(),
        new PurchaseInvoiceLinesData.Builder().uOM("Unit")
            .unitPrice("20.00")
            .lineNetAmount("20.00")
            .tax("VAT 10% USA")
            .invoicedQuantity("1")
            .build(),
        new TaxData.Builder().tax("VAT 10% USA - VAT 10%")
            .taxAmount("-18.00")
            .taxableAmount("-180.00")
            .build() } };
    return Arrays.asList(data);
  }

  @Test
  public void PRORegression34392Test() throws ParseException {

    logger.info("** Start of test case PRORegression34392. **");

    PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();

    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    PurchaseInvoice.BasicDiscounts basicDiscounts = purchaseInvoice.new BasicDiscounts(mainPage);
    basicDiscounts.create(purchaseInvoiceDiscountData);

    PurchaseInvoice.Lines purchaseInvoiceLine = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLine.create(purchaseInvoiceLineData);
    purchaseInvoiceLine.assertData(purchaseInvoiceLineVerificationData);

    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData);
    purchaseInvoiceLine.getTab().clearFilters();
    purchaseInvoiceLine.assertCount(2);
    purchaseInvoiceLine.select(purchaseInvoiceDiscountLineFilterData);
    purchaseInvoiceLine.assertData(purchaseInvoiceDiscountLineVerificationData);
    PurchaseInvoice.Tax tax = purchaseInvoice.new Tax(mainPage);
    tax.assertCount(1);
    tax.assertData(purchaseInvoiceTaxData);

    // Store invoice documentno
    final String invoiceNo = purchaseInvoice.getData("documentNo").toString();

    if (purchaseInvoice.isPosted()) {
      purchaseInvoice.unpost();
    }
    String reversedDocumentNo = String.valueOf(Integer.valueOf(invoiceNo) + 1);
    purchaseInvoice.close();
    purchaseInvoice
        .assertProcessCompletedSuccessfully2("Reversed by document: " + reversedDocumentNo);

    mainPage.closeAllViews();
    purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.getTab().refresh();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    purchaseInvoice
        .select(new PurchaseInvoiceHeaderData.Builder().documentNo(reversedDocumentNo).build());

    purchaseInvoice.assertData(reversedPurchaseInvoiceHeaderVerificationData);
    purchaseInvoiceLine = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLine.getTab().clearFilters();
    purchaseInvoiceLine.assertCount(2);
    purchaseInvoiceLine.select(reversePurchaseInvoiceLineFilterData);
    purchaseInvoiceLine.assertData(reversedPurchaseInvoiceLineVerificationData);
    purchaseInvoiceLine.getTab().clearFilters();
    purchaseInvoiceLine.select(purchaseInvoiceDiscountLineFilterData);
    purchaseInvoiceLine.assertData(reversedPurchaseInvoiceDiscountLineVerificationData);
    basicDiscounts = purchaseInvoice.new BasicDiscounts(mainPage);
    basicDiscounts.assertCount(1);
    basicDiscounts.assertData(purchaseInvoiceDiscountData);
    tax = purchaseInvoice.new Tax(mainPage);
    tax.assertCount(1);
    tax.assertData(reversedPurchaseInvoiceTaxData);

    logger.info("** End of test case PRORegression34392. **");
  }
}
