/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_AddPaymentRefactor.APR_AddPaymentRefactor2;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Reactivating a payment.
 *
 * @author Unai Martirena
 */
@RunWith(Parameterized.class)
public class APRSales009 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  private static final String PARAM_ACTUAL_PAYMENT = "actual_payment";
  private static final String PARAM_REFERENCE_NO = "reference_no";

  /* Data for this test. */

  /** The Sales Invoice header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  /** The data to verify the creation of the Sales Invoice header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  /** The data to verify the completion of the sales invoice. */
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  /** The data to verify the creation of the Sales Invoice lines data. */
  SalesInvoiceLinesData salesInvoiceLineData;
  /** The data to verify the creation of the Sales Invoice line. */
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  /** The data to verify the payment in plan. */
  PaymentPlanData paymentInPlanData;
  /** The data to verify the data in the add payment pop up. */
  AddPaymentPopUpData addPaymentInVerificationData;
  /** The data to verify the totals data in the add payment pop up. */
  AddPaymentPopUpData addPaymentTotalsVerificationData;
  /** The data to verify the application of the payment. */
  SalesInvoiceHeaderData payedSalesInvoiceHeaderVerificationData;
  /** The data to verify the payment in plan. */
  PaymentPlanData paymentInPlanData2;
  /** The data to verify the payment in details data. */
  PaymentDetailsData paymentInDetailsData;

  PaymentInHeaderData paymentInHeaderData;
  /** The data to verify the data in the add payment pop up. */
  AddPaymentPopUpData addPaymentInVerificationData2;
  /** The data to verify the totals data in the add payment pop up. */
  AddPaymentPopUpData addPaymentTotalsVerificationData2;

  /**
   * Class constructor.
   *
   * @param paymentInHeaderData
   *          Payment In header data to be selected @param addPaymentInVerificationData2 The data to
   *          verify the data in the add payment pop up. @param addPaymentTotalsVerificationData2
   *          The data to verify the totals data in the add payment pop up.
   */
  public APRSales009(SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData, PaymentPlanData paymentInPlanData,
      AddPaymentPopUpData addPaymentInVerificationData,
      AddPaymentPopUpData addPaymentTotalsVerificationData,
      SalesInvoiceHeaderData payedSalesInvoiceHeaderVerificationData,
      PaymentPlanData paymentInPlanData2, PaymentDetailsData paymentInDetailsData,
      PaymentInHeaderData paymentInHeaderData, AddPaymentPopUpData addPaymentInVerificationData2,
      AddPaymentPopUpData addPaymentTotalsVerificationData2) {

    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData = salesInvoiceLineData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.paymentInPlanData = paymentInPlanData;
    this.addPaymentInVerificationData = addPaymentInVerificationData;
    this.addPaymentTotalsVerificationData = addPaymentTotalsVerificationData;
    this.payedSalesInvoiceHeaderVerificationData = payedSalesInvoiceHeaderVerificationData;
    this.paymentInPlanData2 = paymentInPlanData2;
    this.paymentInDetailsData = paymentInDetailsData;

    this.paymentInHeaderData = paymentInHeaderData;
    this.addPaymentInVerificationData2 = addPaymentInVerificationData2;
    this.addPaymentTotalsVerificationData2 = addPaymentTotalsVerificationData2;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        /* Parameters for [APRSales009] Select Payment. */
        new SalesInvoiceHeaderData.Builder().transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new SalesInvoiceHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentMethod("1 (Spain)")
            .paymentTerms("30 days, 5")
            .build(),
        new SalesInvoiceHeaderData.Builder().totalPaid("0.00")
            .outstandingAmount("207.00")
            .documentStatus("Completed")
            .summedLineAmount("200.00")
            .grandTotalAmount("207.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("FGA")
                .priceListVersionName("Customer A")
                .build())
            .invoicedQuantity("100")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good A").build())
            .invoicedQuantity("100")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT(3)+CHARGE(0.5)")
            .lineNetAmount("200.00")
            .build(),
        new PaymentPlanData.Builder().dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .paymentMethod("1 (Spain)")
            .expected("207.00")
            .received("0.00")
            .outstanding("207.00")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Customer A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("EUR")
            .expected_payment("207.00")
            .actual_payment("207.00")
            .payment_date(OBDate.CURRENT_DATE)
            .transaction_type("Invoices")
            .build(),
        new AddPaymentPopUpData.Builder().amount_gl_items("0.00")
            .amount_inv_ords("207.00")
            .total("207.00")
            .difference("0.00")
            .build(),
        new SalesInvoiceHeaderData.Builder().paymentComplete(true)
            .documentStatus("Completed")
            .summedLineAmount("200.00")
            .grandTotalAmount("207.00")
            .currency("EUR")
            .build(),
        new PaymentPlanData.Builder().dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .paymentMethod("1 (Spain)")
            .expected("207.00")
            .received("207.00")
            .outstanding("0.00")
            .build(),
        new PaymentDetailsData.Builder().paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("1 (Spain)")
            .received("207.00")
            .build(),

        new PaymentInHeaderData.Builder().organization("Spain").build(),
        new AddPaymentPopUpData.Builder().received_from("Customer A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("EUR")
            .expected_payment("207.00")
            .actual_payment("205.00")
            .payment_date(OBDate.CURRENT_DATE)
            .transaction_type("Invoices")
            .build(),
        new AddPaymentPopUpData.Builder().amount_gl_items("0.00")
            .amount_inv_ords("205.00")
            .total("205.00")
            .difference("0.00")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test to create a sales order, and a goods receipt and sales invoice from that order.
   *
   * @throws ParseException
   */
  @Test
  public void APRSales009Test() throws ParseException {
    logger.info("** Start of test case [APRSales009] Reactivating a payment. **");

    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    SalesInvoice.Lines salesInvoiceLines = salesInvoice.new Lines(mainPage);
    salesInvoiceLines.create(salesInvoiceLineData);
    salesInvoiceLines.assertSaved();
    salesInvoiceLines.assertData(salesInvoiceLineVerificationData);
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);

    SalesInvoice.PaymentInPlan paymentInPlan = salesInvoice.new PaymentInPlan(mainPage);
    paymentInPlan.assertCount(1);
    paymentInPlan.assertData(paymentInPlanData);

    AddPaymentProcess addPaymentProcess = salesInvoice.openAddPayment();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.assertData(addPaymentInVerificationData);
    String paymentNo = addPaymentProcess.getParameterValue("payment_documentno").toString();
    paymentNo = paymentNo.substring(1, paymentNo.length() - 1);
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.getField(PARAM_REFERENCE_NO).focus();
    Sleep.trySleep(1000);
    addPaymentProcess.assertTotalsData(addPaymentTotalsVerificationData);
    addPaymentProcess.process("Process Received Payment(s)");

    salesInvoice.assertPaymentCreatedSuccessfully();
    salesInvoice.assertData(payedSalesInvoiceHeaderVerificationData);

    paymentInPlan = salesInvoice.new PaymentInPlan(mainPage);
    paymentInPlan.assertCount(1);
    paymentInPlan.assertData(paymentInPlanData2);
    SalesInvoice.PaymentInPlan.PaymentInDetails paymentInDetails = paymentInPlan.new PaymentInDetails(
        mainPage);
    paymentInDetails.assertCount(1);
    paymentInDetails.assertData(paymentInDetailsData);

    // Reactivate payment
    PaymentIn paymentIn = new PaymentIn(mainPage).open();
    paymentInHeaderData.addDataField("documentNo", paymentNo);
    paymentIn.select(paymentInHeaderData);
    paymentIn.process("Reactivate");
    paymentIn.assertAddPaymentInData("Customer A", "207.00", "Invoices");
    AddPaymentProcess addPaymentProcess2 = paymentIn.addDetailsOpen();
    addPaymentProcess2.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess2.setParameterValue(PARAM_ACTUAL_PAYMENT, "205.00");
    addPaymentProcess2.editOrderInvoiceRecord(0,
        new SelectedPaymentData.Builder().writeoff(true).build());
    addPaymentProcess2.getField(PARAM_REFERENCE_NO).focus();
    addPaymentProcess2.assertData(addPaymentInVerificationData2);
    addPaymentProcess2.assertTotalsData(addPaymentTotalsVerificationData2);
    addPaymentProcess2.process("Process Received Payment(s)");

    paymentIn.assertPaymentCreatedSuccessfully();

    logger.info("** End of test case [APRSales009] Reactivating a payment. **");
  }

  @AfterClass
  public static void tearDown() {
    SeleniumSingleton.INSTANCE.quit();
    OpenbravoERPTest.seleniumStarted = false;
    OpenbravoERPTest.forceLoginRequired();
  }
}
