/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions3;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentproposal.PaymentProposalHeaderData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentProposal;

/**
 * Test regression 30751
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression30751Out extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PaymentProposalHeaderData paymentProposalHeaderData;
  PaymentProposalHeaderData paymentProposalHeaderVerificationData;

  /**
   * Class constructor.
   *
   */
  public APRRegression30751Out(PaymentProposalHeaderData paymentProposalHeaderData,
      PaymentProposalHeaderData paymentProposalHeaderVerificationData) {
    this.paymentProposalHeaderData = paymentProposalHeaderData;
    this.paymentProposalHeaderVerificationData = paymentProposalHeaderVerificationData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] {
        { new PaymentProposalHeaderData.Builder().account("Spain Bank - EUR").build(),
            new PaymentProposalHeaderData.Builder().organization("Spain")
                .documentType("Payment Proposal")
                .businessPartner(new BusinessPartnerSelectorData.Builder().name("").build())
                .paymentMethod("1 (Spain)")
                .currency("EUR")
                .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 30751
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression30751Test() throws ParseException {
    logger.info("** Start of test case [APRRegression30751] Test regression 30751. **");

    PaymentProposal paymentProposal = new PaymentProposal(mainPage).open();
    paymentProposal.create(paymentProposalHeaderData);
    paymentProposal.assertSaved();
    paymentProposal.assertData(paymentProposalHeaderVerificationData);

    PaymentProposal.Lines paymentProposalLines = paymentProposal.new Lines(mainPage);
    paymentProposalLines.assertCount(0);

    paymentProposal.selectAllExpectedPayments();
    paymentProposal.assertProcessCompletedSuccessfully2();
    int rows = paymentProposalLines.getRecordCount();
    paymentProposalLines.assertCount(rows);

    paymentProposal.generatePayments();
    paymentProposalLines.assertCount(rows);

    paymentProposal.generatePayments();
    paymentProposal.assertProcessCompletedSuccessfully2();
    paymentProposalLines.assertCount(rows);

    paymentProposal.unselectAllExpectedPayments();
    paymentProposal.assertProcessCompletedSuccessfully2();
    paymentProposalLines.assertCount(0);

    logger.info("** End of test case [APRRegression30751] Test regression 30751. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
