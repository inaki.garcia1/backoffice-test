/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions8;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 26951
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class APRRegression26951Out extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PurchaseOrderHeaderData purchaseOrderHeaderData;
  PurchaseOrderHeaderData purchaseOrderHeaderVerficationData;
  PurchaseOrderLinesData purchaseOrderLines1Data;
  PurchaseOrderLinesData purchaseOrderLines1VerificationData;
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData;
  PaymentPlanData orderPaymentOutPlanData;
  PaymentPlanData orderPrePayPaymentOutPlanData;
  PaymentDetailsData orderPrePaidPaymentOutDetailsData;
  PaymentOutHeaderData paymentInPrepaidHeaderVerificationData;
  PaymentOutLinesData paymentInPrePaidLinesVerificationData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice1PaymentOutPlanData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoice1PaymentOutDetailsData;
  PaymentPlanData orderInvoicedPaymentOutPlanData;
  PaymentDetailsData orderInvoicedPaymentOutDetailsData;
  PaymentDetailsData orderInvoicedPaymentOutDetailsData1;
  PaymentOutLinesData paymentInLinesInvoicedVerificationData;
  PaymentOutLinesData paymentInLinesInvoicedVerificationData1;
  PurchaseInvoiceHeaderData completedPurchaseInvoice2HeaderVerificationData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice2PaymentOutPlanData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoice2PaymentOutDetailsData;
  String[][] journalEntryLines;
  String[][] journalEntryLines2;
  String[][] journalEntryLines3;
  String[][] journalEntryLines4;
  String[][] journalEntryLines5;
  String[][] journalEntryLines6;

  /**
   * Class constructor.
   *
   */
  public APRRegression26951Out(PurchaseOrderHeaderData purchaseOrderHeaderData,
      PurchaseOrderHeaderData purchaseOrderHeaderVerficationData,
      PurchaseOrderLinesData purchaseOrderLines1Data,
      PurchaseOrderLinesData purchaseOrderLines1VerificationData,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData,
      PaymentPlanData orderPaymentOutPlanData, PaymentPlanData orderPrePayPaymentOutPlanData,
      PaymentDetailsData orderPaymentOutDetailsData,
      PaymentOutHeaderData paymentInPrepaidHeaderVerificationData,
      PaymentOutLinesData paymentInPrePaidLinesVerificationData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice1PaymentOutPlanData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoice1PaymentOutDetailsData,
      PaymentPlanData orderInvoicedPaymentOutPlanData,
      PaymentDetailsData orderInvoicedPaymentOutDetailsData,
      PaymentDetailsData orderInvoicedPaymentOutDetailsData1,
      PaymentOutLinesData paymentInLinesInvoicedVerificationData,
      PaymentOutLinesData paymentInLinesInvoicedVerificationData1,
      PurchaseInvoiceHeaderData completedPurchaseInvoice2HeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice2PaymentOutPlanData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoice2PaymentOutDetailsData,
      String[][] journalEntryLines, String[][] journalEntryLines2, String[][] journalEntryLines3,
      String[][] journalEntryLines4, String[][] journalEntryLines5, String[][] journalEntryLines6) {
    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    this.purchaseOrderHeaderVerficationData = purchaseOrderHeaderVerficationData;
    this.purchaseOrderLines1Data = purchaseOrderLines1Data;
    this.purchaseOrderLines1VerificationData = purchaseOrderLines1VerificationData;
    this.bookedPurchaseOrderHeaderVerificationData = bookedPurchaseOrderHeaderVerificationData;
    this.orderPaymentOutPlanData = orderPaymentOutPlanData;
    this.orderPrePayPaymentOutPlanData = orderPrePayPaymentOutPlanData;
    this.orderPrePaidPaymentOutDetailsData = orderPaymentOutDetailsData;
    this.paymentInPrepaidHeaderVerificationData = paymentInPrepaidHeaderVerificationData;
    this.paymentInPrePaidLinesVerificationData = paymentInPrePaidLinesVerificationData;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineVerificationData = purchaseInvoiceLineVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.invoice1PaymentOutPlanData = invoice1PaymentOutPlanData;
    this.invoice1PaymentOutDetailsData = invoice1PaymentOutDetailsData;
    this.orderInvoicedPaymentOutPlanData = orderInvoicedPaymentOutPlanData;
    this.orderInvoicedPaymentOutDetailsData = orderInvoicedPaymentOutDetailsData;
    this.orderInvoicedPaymentOutDetailsData1 = orderInvoicedPaymentOutDetailsData1;
    this.paymentInLinesInvoicedVerificationData = paymentInLinesInvoicedVerificationData;
    this.paymentInLinesInvoicedVerificationData1 = paymentInLinesInvoicedVerificationData1;
    this.completedPurchaseInvoice2HeaderVerificationData = completedPurchaseInvoice2HeaderVerificationData;
    this.invoice2PaymentOutPlanData = invoice2PaymentOutPlanData;
    this.invoice2PaymentOutDetailsData = invoice2PaymentOutDetailsData;
    this.journalEntryLines = journalEntryLines;
    this.journalEntryLines2 = journalEntryLines2;
    this.journalEntryLines3 = journalEntryLines3;
    this.journalEntryLines4 = journalEntryLines4;
    this.journalEntryLines5 = journalEntryLines5;
    this.journalEntryLines6 = journalEntryLines6;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> PurchaseOrderValues() {
    Object[][] data = new Object[][] { {
        new PurchaseOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Purchase Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paymentTerms("90 days")
            .build(),

        new PurchaseOrderHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("USA warehouse")
            .paymentTerms("90 days")
            .priceList("Purchase")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMC").priceListVersion("Purchase").build())
            .orderedQuantity("9")
            .tax("VAT 10% USA")
            .build(),

        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("18.00")
            .build(),

        new PurchaseOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("18.00")
            .grandTotalAmount("19.80")
            .currency("EUR")
            .build(),

        new PaymentPlanData.Builder().dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("19.80")
            .paid("0.00")
            .outstanding("19.80")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new PaymentPlanData.Builder().dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("19.80")
            .paid("6.00")
            .outstanding("13.80")
            .numberOfPayments("1")
            .currency("EUR")
            .build(),

        new PaymentDetailsData.Builder().paymentDate(OBDate.CURRENT_DATE)
            .expected("19.80")
            .paid("6.00")
            .writeoff("0.00")
            .status("Withdrawn not Cleared")
            .build(),

        new PaymentOutHeaderData.Builder().organization("USA")
            .documentType("AP Payment")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("6.00")
            .currency("EUR")
            .status("Withdrawn not Cleared")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new PaymentOutLinesData.Builder().invoiceAmount("19.80")
            .expected("19.80")
            .paid("6.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().organization("USA")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentTerms("90 days")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new PurchaseInvoiceHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .priceList("Purchase")
            .paymentTerms("90 days")
            .build(),

        new PurchaseInvoiceLinesData.Builder().invoicedQuantity("2")
            .lineNetAmount("4.00")
            .tax("VAT 10% USA")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("4.40")
            .summedLineAmount("4.00")
            .currency("EUR")
            .paymentComplete(true)
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("4.40")
            .paid("4.40")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData.Builder()
            .paid("4.40")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("4.40")
            .status("Withdrawn not Cleared")
            .build(),

        new PaymentPlanData.Builder().dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("19.80")
            .paid("6.00")
            .outstanding("13.80")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("2")
            .currency("EUR")
            .build(),

        new PaymentDetailsData.Builder().paymentDate(OBDate.CURRENT_DATE)
            .expected("19.80")
            .paid("1.60")
            .writeoff("0.00")
            .status("Withdrawn not Cleared")
            .build(),

        new PaymentDetailsData.Builder().paymentDate(OBDate.CURRENT_DATE)
            .expected("4.40")
            .paid("4.40")
            .writeoff("0.00")
            .status("Withdrawn not Cleared")
            .build(),

        new PaymentOutLinesData.Builder().invoiceAmount("19.80")
            .expected("19.80")
            .paid("1.60")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PaymentOutLinesData.Builder().invoiceAmount("4.40")
            .expected("4.40")
            .paid("4.40")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("4.40")
            .summedLineAmount("4.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("4.40")
            .paid("1.60")
            .outstanding("2.80")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData.Builder()
            .paid("1.60")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("4.40")
            .status("Withdrawn not Cleared")
            .build(),

        new String[][] { { "60000", "Compras de mercaderías", "4.00", "" },
            { "47200", "Hacienda Pública IVA soportado", "0.40", "" },
            { "40000", "Proveedores (euros)", "", "2.80" },
            { "40700", "Anticipos a proveedores", "", "1.60" } },

        new String[][] { { "51200", "Service costs", "10.00", "" },
            { "11600", "Tax Receivables", "1.00", "" }, { "21100", "Accounts payable", "", "7.00" },
            { "1410", "Vendor prepayment", "", "4.00" } },

        new String[][] { { "60000", "Compras de mercaderías", "4.00", "" },
            { "47200", "Hacienda Pública IVA soportado", "0.40", "" },
            { "40700", "Anticipos a proveedores", "", "4.40" } },

        new String[][] { { "51200", "Service costs", "10.00", "" },
            { "11600", "Tax Receivables", "1.00", "" },
            { "1410", "Vendor prepayment", "", "11.00" } },

        new String[][] { { "40700", "Anticipos a proveedores", "6.00", "" },
            { "40100", "Proveedores efectos comerciales a pagar", "", "6.00" } },

        new String[][] { { "1410", "Vendor prepayment", "15.00", "" },
            { "11300", "Bank in transit", "", "15.00" }, },

        } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 26951
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression26951Test() throws ParseException {
    logger.info("** Start of test case [APRRegression26951] Test regression 26951**");

    // Register a Purchase order
    PurchaseOrder order = new PurchaseOrder(mainPage).open();
    order.create(purchaseOrderHeaderData);
    order.assertSaved();
    order.assertData(purchaseOrderHeaderVerficationData);
    String orderNo = order.getData("documentNo").toString();

    // Create order lines
    PurchaseOrder.Lines orderLines = order.new Lines(mainPage);
    orderLines.create(purchaseOrderLines1Data);
    orderLines.assertSaved();
    orderLines.assertData(purchaseOrderLines1VerificationData);

    String purchaseOrderIdentifier = String.format("%s - %s - %s", orderNo,
        order.getData("orderDate"),
        bookedPurchaseOrderHeaderVerificationData.getDataField("grandTotalAmount"));

    // Book the order
    order.book();
    order.assertProcessCompletedSuccessfully2();
    order.assertData(bookedPurchaseOrderHeaderVerificationData);

    // Check pending order payment plan
    PurchaseOrder.PaymentOutPlan orderPaymentOutPlan = order.new PaymentOutPlan(mainPage);
    orderPaymentOutPlan.assertCount(1);
    orderPaymentOutPlan.assertData(orderPaymentOutPlanData);

    // Pre pay the order
    order.addPayment("6.00", "Process Made Payment(s) and Withdrawal");

    // Check pre pay order payment plan and details
    orderPaymentOutPlan = order.new PaymentOutPlan(mainPage);
    orderPaymentOutPlan.assertCount(1);
    orderPaymentOutPlan.assertData(orderPrePayPaymentOutPlanData);

    PurchaseOrder.PaymentOutPlan.PaymentOutDetails orderPaymentOutDetails = orderPaymentOutPlan.new PaymentOutDetails(
        mainPage);
    orderPaymentOutDetails.assertCount(1);
    orderPaymentOutDetails.selectWithoutFiltering(0);
    orderPaymentOutDetails.assertData(orderPrePaidPaymentOutDetailsData);

    String payment1No = orderPaymentOutDetails.getData("payment").toString();
    payment1No = payment1No.substring(0, payment1No.indexOf("-") - 1);

    PaymentOut payment = new PaymentOut(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentOutHeaderData.Builder().documentNo(payment1No).build());
    payment.assertData(paymentInPrepaidHeaderVerificationData);
    PaymentOut.Lines paymentLines = payment.new Lines(mainPage);
    paymentLines.assertCount(1);
    paymentInPrePaidLinesVerificationData.addDataField("orderPaymentSchedule$order$documentNo",
        orderNo);
    paymentLines.assertData(paymentInPrePaidLinesVerificationData);

    // Create a Purchase invoice from order
    PurchaseInvoice invoice = new PurchaseInvoice(mainPage).open();
    invoice.create(purchaseInvoiceHeaderData);
    invoice.assertSaved();
    invoice.assertData(purchaseInvoiceHeaderVerificationData);
    invoice.createLinesFrom(purchaseOrderIdentifier);
    invoice.assertProcessCompletedSuccessfully();
    PurchaseInvoice.Lines invoiceLine = invoice.new Lines(mainPage);
    invoiceLine.assertCount(1);
    invoiceLine.edit(new PurchaseInvoiceLinesData.Builder().invoicedQuantity("2").build());
    invoiceLine.assertData(purchaseInvoiceLineVerificationData);
    invoice.complete();
    invoice.assertProcessCompletedSuccessfully2();
    invoice.assertData(completedPurchaseInvoiceHeaderVerificationData);
    String invoice1No = (String) invoice.getData("documentNo");

    // Check first invoice payment plan and details
    PurchaseInvoice.PaymentOutPlan invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoice1PaymentOutPlanData);

    PurchaseInvoice.PaymentOutPlan.PaymentOutDetails invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(
        mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoice1PaymentOutDetailsData);

    // Check Purchase order payment in plan
    order = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentOutPlan = order.new PaymentOutPlan(mainPage);
    orderPaymentOutPlan.assertCount(1);
    orderPaymentOutPlan.assertData(orderInvoicedPaymentOutPlanData);

    // Check Purchase order payment in details
    orderPaymentOutDetails = orderPaymentOutPlan.new PaymentOutDetails(mainPage);
    orderPaymentOutDetails.assertCount(2);
    orderPaymentOutDetails
        .select(new PaymentDetailsData.Builder().expected("19.80").paid("1.60").build());
    orderPaymentOutDetails.assertData(orderInvoicedPaymentOutDetailsData);

    orderPaymentOutDetails
        .select(new PaymentDetailsData.Builder().expected("4.40").paid("4.40").build());
    orderPaymentOutDetails.assertData(orderInvoicedPaymentOutDetailsData1);

    // Check Payment In
    payment = new PaymentOut(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentOutHeaderData.Builder().documentNo(payment1No).build());
    payment.assertData(paymentInPrepaidHeaderVerificationData);
    paymentLines = payment.new Lines(mainPage);
    paymentLines.assertCount(2);

    paymentInLinesInvoicedVerificationData.addDataField("orderPaymentSchedule$order$documentNo",
        orderNo);
    paymentInLinesInvoicedVerificationData1
        .addDataField("orderPaymentSchedule$order$documentNo", orderNo)
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoice1No);

    paymentLines.select(new PaymentOutLinesData.Builder().paid("1.60").orderno(orderNo).build());
    paymentLines.assertData(paymentInLinesInvoicedVerificationData);
    paymentLines.closeForm();

    paymentLines.select(new PaymentOutLinesData.Builder().paid("4.40")
        .orderno(orderNo)
        .invoiceno(invoice1No)
        .build());
    paymentLines.assertData(paymentInLinesInvoicedVerificationData1);
    paymentLines.closeForm();

    // Create a second invoice from order
    invoice = new PurchaseInvoice(mainPage).open();
    invoice.create(purchaseInvoiceHeaderData);
    invoice.assertSaved();
    invoice.assertData(purchaseInvoiceHeaderVerificationData);
    invoice.createLinesFrom(purchaseOrderIdentifier);
    invoice.assertProcessCompletedSuccessfully();
    invoiceLine = invoice.new Lines(mainPage);
    invoiceLine.assertCount(1);
    invoiceLine.edit(new PurchaseInvoiceLinesData.Builder().invoicedQuantity("2").build());
    invoiceLine.assertData(purchaseInvoiceLineVerificationData);
    invoice.complete();
    invoice.assertProcessCompletedSuccessfully2();
    invoice.assertData(completedPurchaseInvoice2HeaderVerificationData);
    String invoice2No = (String) invoice.getData("documentNo");

    // Check invoice2 payment plan and details
    invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoice2PaymentOutPlanData);

    invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoice2PaymentOutDetailsData);

    // Post invoice2 and check post
    invoice.open();
    invoice.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines.length);
    post.assertJournalLines2(journalEntryLines);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines2.length);
    post.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Check invoice1 payment plan and details
    invoice = new PurchaseInvoice(mainPage).open();
    invoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoice1No).build());

    invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoice1PaymentOutPlanData);

    invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoice1PaymentOutDetailsData);

    // Post invoice1 and check post
    invoice.open();
    invoice.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines3.length);
    post.assertJournalLines2(journalEntryLines3);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines4.length);
    post.assertJournalLines2(journalEntryLines4);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Check Purchase order payment in plan
    order = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentOutPlan = order.new PaymentOutPlan(mainPage);
    orderPaymentOutPlan.assertCount(1);
    orderPaymentOutPlan.assertData(orderInvoicedPaymentOutPlanData);

    // Check Purchase order payment in details
    orderInvoicedPaymentOutDetailsData.addDataField("expected", "4.40");
    orderPaymentOutDetails = orderPaymentOutPlan.new PaymentOutDetails(mainPage);
    orderPaymentOutDetails.assertCount(2);
    orderPaymentOutDetails
        .select(new PaymentDetailsData.Builder().expected("4.40").paid("1.60").build());
    orderPaymentOutDetails.assertData(orderInvoicedPaymentOutDetailsData);

    orderPaymentOutDetails
        .select(new PaymentDetailsData.Builder().paid("4.40").expected("4.40").build());
    orderPaymentOutDetails.assertData(orderInvoicedPaymentOutDetailsData1);

    // Check Payment In
    payment = new PaymentOut(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentOutHeaderData.Builder().documentNo(payment1No).build());
    payment.assertData(paymentInPrepaidHeaderVerificationData);
    paymentLines = payment.new Lines(mainPage);
    paymentLines.assertCount(2);

    paymentInLinesInvoicedVerificationData.addDataField("expected", "4.40")
        .addDataField("invoiceAmount", "4.40")
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoice2No);

    paymentLines.select(new PaymentOutLinesData.Builder().paid("1.60").orderno(orderNo).build());
    paymentLines.assertData(paymentInLinesInvoicedVerificationData);
    paymentLines.closeForm();

    paymentLines.select(new PaymentOutLinesData.Builder().paid("4.40")
        .orderno(orderNo)
        .invoiceno(invoice1No)
        .build());
    paymentLines.assertData(paymentInLinesInvoicedVerificationData1);
    paymentLines.closeForm();

    payment.open();
    payment.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines5.length);
    post.assertJournalLines2(journalEntryLines5);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines6.length);
    post.assertJournalLines2(journalEntryLines6);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    logger.info("** End of test case [APRRegression26951] Test regression 26951**");
  }
}
