/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.application.instanceactivation;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.data.generalsetup.application.instanceactivation.ActivateOnlineData;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicButtonPopUp;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions on activate online pop up.
 *
 * @author elopio
 *
 */
public class ActivateOnlinePopUp extends OBClassicButtonPopUp {

  /** Identifier of the purpose combo box. */
  private static final String COMBO_BOX_PURPOSE = "purpose";
  /** Identifier of the system key text field. */
  private static final String TEXT_FIELD_SYSTEM_KEY = "publicKey";

  /**
   * Enumerator with the purpose values.
   *
   * @author elopio
   *
   */
  public enum Purpose {
    DEVELOPMENT("Development"), PRODUCTION("Production"), TESTING("Testing");

    private final String purpose;

    private Purpose(String purpose) {
      this.purpose = purpose;
    }

    public String purpose() {
      return purpose;
    }
  }

  /**
   * Class constructor.
   *
   */
  public ActivateOnlinePopUp() {
    super();
  }

  /**
   * Fill the pop up.
   *
   * @param activateOnlineData
   *          The data that will be filled on the pop up.
   */
  public void fill(ActivateOnlineData activateOnlineData) {
    if (activateOnlineData.getPurpose() != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_PURPOSE))
          .selectByVisibleText(activateOnlineData.getPurpose());
    }
    if (activateOnlineData.getSystemKey() != null) {
      // TODO see issue 11794 -
      // https://issues.openbravo.com/view.php?id=11794
      String systemKey = activateOnlineData.getSystemKey().replaceAll("\n", "");
      WebElement systemKeyField = SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_SYSTEM_KEY);
      systemKeyField.clear();
      // TODO: <chromedriver> specific workaround to sendKeys in this case: send systemKey via JS
      // call
      if (systemKey.contains("/") && SeleniumSingleton.runsChrome()) {
        SeleniumSingleton.INSTANCE.executeScriptWithReturn(String.format(
            "document.getElementById('%s').value = '%s'", TEXT_FIELD_SYSTEM_KEY, systemKey));
      } else {
        systemKeyField.sendKeys(systemKey);
      }
    }
  }

  /**
   * Activate the subscription.
   *
   * @param purpose
   *          The purpose of the system.
   * @param systemKey
   *          The activation key.
   */
  public void activate(String purpose, String systemKey) {
    selectPopUpWithOKButton();
    new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_PURPOSE))
        .selectByVisibleText(purpose);
    SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_SYSTEM_KEY).sendKeys(systemKey);
    clickOK();
  }

  /**
   * Activate the instance.
   *
   * @param activateOnlineData
   *          The date for instance activation.
   */
  public void activate(ActivateOnlineData activateOnlineData) {
    selectPopUpWithOKButton();
    fill(activateOnlineData);
    clickOK();
  }
}
