/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Pablo Lujan <pablo.lujan@openbravo.com>,
 *  Lorenzo Fidalgo<lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testsuites.smoke.procurement;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderDisplayLogic;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;

/**
 * Order to invoice.
 *
 * @author Lorenzo Fidalgo
 */
@RunWith(Parameterized.class)
public class PurchaseOrderDisplayLogic extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  /** The purchase order header data. */
  PurchaseOrderHeaderData purchaseOrderHeaderData;
  /** PurchaseOrderHeaderDisplayLogic to verify the PurchaseOrderHeader display logic. */
  PurchaseOrderHeaderDisplayLogic purchaseOrderHeaderDisplayLogic;

  /**
   * Class constructor.
   *
   * @param purchaseOrderHeaderData
   *          The purchase order header data.
   * @param purchaseOrderHeaderDisplayLogic
   *          The data to verify the display logics.
   */
  public PurchaseOrderDisplayLogic(PurchaseOrderHeaderData purchaseOrderHeaderData,
      PurchaseOrderHeaderDisplayLogic purchaseOrderHeaderDisplayLogic) {
    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    this.purchaseOrderHeaderDisplayLogic = purchaseOrderHeaderDisplayLogic;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<Object[]> purchaseOrderDisplayLogic() {

    return Arrays.asList(new Object[][] { {
        new PurchaseOrderHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .build(),
        new PurchaseOrderHeaderDisplayLogic.Builder().organization(true)
            .transactionDocument(true)
            .documentNo(true)
            .deliveryNotes(false)
            .build() } });
  }

  /**
   * Test the display logic of the PurchaseOrderHeader
   */
  @Test
  public void PROa_createPurchaseOrderAndInvoice() {
    logger.info("** Start of test case PurchaseOrderHeaderDisplayLogic. **");
    final PurchaseOrder purchaseOrder = new PurchaseOrder(mainPage).open();
    purchaseOrder.create(purchaseOrderHeaderData);
    purchaseOrder.assertDisplayLogicVisible(purchaseOrderHeaderDisplayLogic);
    logger.info("** End of test PurchaseOrderHeaderDisplayLogic. **");
  }
}
