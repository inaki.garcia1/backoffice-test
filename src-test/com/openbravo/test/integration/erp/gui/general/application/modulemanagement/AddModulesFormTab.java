/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.application.modulemanagement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;

import com.openbravo.test.integration.erp.gui.tabs.FormTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verify actions on Add Modules form tab.
 *
 * @author elopio
 *
 */
public class AddModulesFormTab extends FormTab {
  private static final Logger log = LogManager.getLogger();

  /** The tab title. */
  static final String TITLE = "Add Modules";

  /** Identifier of the Add Modules tab. */
  public static final String TAB = "tabInstall";

  /** Identifier of the browse file system button. */
  private static final String BUTTON_BROWSE_FILE_SYSTEM = "buttonScan";
  /**
   * String format used to localize the install button using the name of the module.
   */
  private static final String FORMAT_BUTTON_INSTALL = "//div[@id='name' and text()='%s']/../div[@class='AddModule_Button_Install']/div/button[@id='buttonInstall']";
  /**
   * String format used to localize the details link using the name of the module.
   */
  private static final String FORMAT_LINK_DETAILS = "//div[@id='name' and text()='%s']/../../div[@class='AddModule_Options']/div[@class='AddModule_Text_Option']/a[@id='details']";

  /**
   * Class constructor.
   *
   */
  public AddModulesFormTab() {
    super(TITLE, TAB);
  }

  /**
   * Click the browse file system button.
   */
  public void clickBrowseFileSystem() {
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_BROWSE_FILE_SYSTEM).click();
  }

  public void searchModule(String moduleName) {
    log.trace("Searching module in CR: {}", moduleName);
    SeleniumSingleton.INSTANCE.findElementById("inpSearchText").sendKeys(moduleName);
    SeleniumSingleton.INSTANCE.findElementById("buttonSearch").click();
    waitWhileProcessing();
  }

  /**
   * Click the install now button of a module.
   *
   * @param moduleName
   *          The name of the module.
   */
  public void clickInstallNow(String moduleName) {
    SeleniumSingleton.INSTANCE.findElementByXPath(String.format(FORMAT_BUTTON_INSTALL, moduleName))
        .click();
  }

  /**
   * Returns true if the module is available for installation from the central repository.
   *
   * @param moduleName
   *          The name of the module.
   * @return true if the module is available for installation from the central repository.
   *         Otherwise, false.
   */
  public boolean isModuleAvailable(String moduleName) {
    try {
      SeleniumSingleton.INSTANCE.findElementByXPath(String.format(FORMAT_LINK_DETAILS, moduleName));
    } catch (NoSuchElementException exception) {
      return false;
    }
    return true;
  }

  /**
   * Get the version of the module.
   *
   * @param moduleName
   *          The name of the module.
   * @return The version of the module.
   */
  public String getModuleVersion(String moduleName) {
    String moduleVersion;
    SeleniumSingleton.INSTANCE.findElementByXPath(String.format(FORMAT_LINK_DETAILS, moduleName))
        .click();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    final ModuleDetailsPopUp moduleDetailsPopUp = new ModuleDetailsPopUp();
    moduleVersion = moduleDetailsPopUp.getModuleVersion();
    moduleDetailsPopUp.clickOK();
    waitForFrame();
    return moduleVersion;
  }

}
