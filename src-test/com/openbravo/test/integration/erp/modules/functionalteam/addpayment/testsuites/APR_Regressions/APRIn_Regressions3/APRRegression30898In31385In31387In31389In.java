/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015-2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions3;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddTransactionData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.MatchStatementData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.bankstatement.BankStatementHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.BankStatementLinesData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.PaymentSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddTransactionProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementGrid;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regressions 30898 - 31385 - 31387 - 31389
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression30898In31385In31387In31389In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager
      .getLogger();

  /* Data for this test. */

  AccountData accountHeaderData;
  PaymentMethodData paymentMethodData;
  PaymentMethodData paymentMethodData2;
  PaymentInHeaderData paymentInHeaderData;
  PaymentInHeaderData paymentInHeaderVerificationData;
  AddPaymentPopUpData addPaymentVerificationData;
  PaymentInHeaderData paymentInHeaderVerificationData2;
  BankStatementHeaderData bankStatementHeaderData;
  BankStatementLinesData bankStatementLinesData;
  MatchStatementData matchStatementData;
  AddTransactionData addTransactionVerificationData;
  AddTransactionData addTransactionVerificationData2;
  MatchStatementData matchStatementData2;
  PaymentMethodData paymentMethodData3;

  /**
   * Class constructor.
   *
   */
  public APRRegression30898In31385In31387In31389In(AccountData accountHeaderData,
      PaymentMethodData paymentMethodData, PaymentMethodData paymentMethodData2,
      PaymentInHeaderData paymentInHeaderData, PaymentInHeaderData paymentInHeaderVerificationData,
      AddPaymentPopUpData addPaymentVerificationData,
      PaymentInHeaderData paymentInHeaderVerificationData2,
      BankStatementHeaderData bankStatementHeaderData,
      BankStatementLinesData bankStatementLinesData, MatchStatementData matchStatementData,
      AddTransactionData addTransactionVerificationData,
      AddTransactionData addTransactionVerificationData2, MatchStatementData matchStatementData2,
      PaymentMethodData paymentMethodData3) {
    this.accountHeaderData = accountHeaderData;
    this.paymentMethodData = paymentMethodData;
    this.paymentMethodData2 = paymentMethodData2;
    this.paymentInHeaderData = paymentInHeaderData;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.paymentInHeaderVerificationData2 = paymentInHeaderVerificationData2;
    this.bankStatementHeaderData = bankStatementHeaderData;
    this.bankStatementLinesData = bankStatementLinesData;
    this.matchStatementData = matchStatementData;
    this.addTransactionVerificationData = addTransactionVerificationData;
    this.addTransactionVerificationData2 = addTransactionVerificationData2;
    this.matchStatementData2 = matchStatementData2;
    this.paymentMethodData3 = paymentMethodData3;
    logInData = new LogInData.Builder().userName("Openbravo").password("openbravo").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { { new AccountData.Builder().name("Cuenta de Banco").build(),
        new PaymentMethodData.Builder().paymentMethod("Transferencia").build(),
        new PaymentMethodData.Builder().automaticDeposit(false).build(),

        new PaymentInHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Alimentos y Supermercados, S.A")
                    .build())
            .amount("15.00")
            .build(),
        new PaymentInHeaderData.Builder().organization("F&B España - Región Norte")
            .documentType("AR Receipt")
            .paymentMethod("Transferencia")
            .account("Cuenta de Banco - EUR")
            .currency("EUR")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Alimentos y Supermercados, S.A")
            .fin_paymentmethod_id("Transferencia")
            .fin_financial_account_id("Cuenta de Banco - EUR")
            .c_currency_id("EUR")
            .actual_payment("15.00")
            .expected_payment("0.00")
            .amount_gl_items("15.00")
            .amount_inv_ords("0.00")
            .total("15.00")
            .difference("0.00")
            .build(),
        new PaymentInHeaderData.Builder().status("Payment Received")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new BankStatementHeaderData.Builder().name("APRRegression30898In31385In31387In31389In")
            .build(),
        new BankStatementLinesData.Builder().referenceNo("30898In")
            .bpartnername("AlimentosySupermercados business partner")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Alimentos y Supermercados, S.A")
                    .build())
            .gLItem("")
            .dramount("0.00")
            .cramount("15.00")
            .description("AlimentosySupermercados description")
            .build(),

        new MatchStatementData.Builder().bankStatementType("D")
            .sender("Alimentos y Supermercados, S.A")
            .bpartnername("AlimentosySupermercados business partner")
            .referenceNo("30898In")
            .glitem("")
            .description("AlimentosySupermercados description")
            .amount("15")
            .affinity("")
            .matchedDocument("")
            .matchingType("")
            .transactionDate("")
            .businessPartner("")
            .transactionAmount("")
            .transactionGLItem("")
            .trxDescription("")
            .build(),
        new AddTransactionData.Builder().trxtype("BP Deposit")
            .description(
                "AlimentosySupermercados business partner\nAlimentosySupermercados description")
            .currency("EUR")
            .depositamt("15.00")
            .withdrawalamt("0.00")
            .organization("F&B España, S.A")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Alimentos y Supermercados, S.A")
                    .build())
            .build(),
        new AddTransactionData.Builder().trxtype("BP Deposit")
            .description(
                "AlimentosySupermercados business partner\nAlimentosySupermercados description\nGL Item: Capital social\n")
            .currency("EUR")
            .depositamt("15.00")
            .withdrawalamt("0.00")
            .organization("F&B España, S.A")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Alimentos y Supermercados, S.A")
                    .build())
            .build(),
        new MatchStatementData.Builder().bankStatementType("D")
            .sender("Alimentos y Supermercados, S.A")
            .bpartnername("AlimentosySupermercados business partner")
            .referenceNo("30898In")
            .glitem("")
            .description("AlimentosySupermercados description")
            .amount("15")
            .affinity("AD")
            .matchedDocument("T")
            .matchingType("AD")
            .businessPartner("Alimentos y Supermercados, S.A")
            .transactionAmount("15")
            .transactionGLItem("")
            .trxDescription(
                "AlimentosySupermercados business partner\nAlimentosySupermercados description\nGL Item: Capital social\n")
            .build(),

        new PaymentMethodData.Builder().automaticDeposit(true).build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regressions 30898 - 31385 - 31387 - 31389 - Payment In flow
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression30898In31385In31387In31389InTest() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression30898In31385In31387In31389In] Test regressions 30898 - 31385 - 31387 - 31389 - Payment In flow. **");

    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);

    FinancialAccount.PaymentMethod paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodData2);

    PaymentIn paymentIn = new PaymentIn(mainPage).open();
    paymentIn.create(paymentInHeaderData);
    paymentIn.assertSaved();
    paymentIn.assertData(paymentInHeaderVerificationData);
    AddPaymentProcess addPaymentProcess = paymentIn.addDetailsOpen();
    String paymentNo = addPaymentProcess.getParameterValue("payment_documentno").toString();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.getOrderInvoiceGrid().selectAll();
    addPaymentProcess.addGLItem(
        new SelectedPaymentData.Builder().gLItem("Capital social").receivedIn("15.00").build());
    addPaymentProcess.getField("reference_no").focus();
    addPaymentProcess.process("Process Received Payment(s)", addPaymentVerificationData);
    paymentIn.assertPaymentCreatedSuccessfully();
    paymentIn.assertData(paymentInHeaderVerificationData2);

    financialAccount = new FinancialAccount(mainPage).open();
    Sleep.trySleep(15000);
    financialAccount.select(accountHeaderData);

    FinancialAccount.BankStatements bankStatement = financialAccount.new BankStatements(mainPage);
    String date;
    if (bankStatement.getRecordCount() == 0) {
      date = OBDate.CURRENT_DATE;
    } else {
      bankStatement.selectWithoutFiltering(0);
      date = OBDate.addDaysToDate((String) bankStatement.getData("transactionDate"), 1);
    }
    bankStatementHeaderData.addDataField("transactionDate", date);
    bankStatementHeaderData.addDataField("importdate", date);
    bankStatement.create(bankStatementHeaderData);
    bankStatement.assertSaved();

    FinancialAccount.BankStatements.BankStatementLines bankStatementLines = bankStatement.new BankStatementLines(
        mainPage);
    bankStatementLinesData.addDataField("transactionDate", date);
    bankStatementLines.create(bankStatementLinesData);
    bankStatementLines.assertSaved();
    bankStatement.process();
    bankStatement.assertProcessCompletedSuccessfully2();

    @SuppressWarnings("rawtypes")
    MatchStatementProcess matchStatementProcess = financialAccount.openMatchStatement(false);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementData.addDataField("banklineDate", date);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData);

    AddTransactionProcess addTransactionProcess = ((MatchStatementGrid) matchStatementProcess
        .getMatchStatementGrid()).clickAdd(0);
    addTransactionVerificationData.addDataField("trxdate", date);
    addTransactionVerificationData.addDataField("dateacct", date);
    addTransactionProcess.assertData(addTransactionVerificationData);
    addTransactionProcess.setParameterValue("fin_payment_id",
        new PaymentSelectorData.Builder().documentNo(paymentNo).build());
    addTransactionVerificationData2.addDataField("trxdate", date);
    addTransactionVerificationData2.addDataField("dateacct", date);
    addTransactionProcess.assertData(addTransactionVerificationData2);
    addTransactionProcess.process();

    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementData2.addDataField("banklineDate", date);
    matchStatementData2.addDataField("trxDate", date);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData2);
    matchStatementProcess.process();
    financialAccount.assertProcessCompletedSuccessfully2();

    paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodData3);

    logger.info(
        "** End of test case [APRRegression30898In31385In31387In31389In] Test regressions 30898 - 31385 - 31387 - 31389 - Payment In flow. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
