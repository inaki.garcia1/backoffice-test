/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.accounting.analysis;

import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.generalledgerjournal.GeneralLedgerJournalWindow;

/**
 * Executes and verifies actions on OpenbravoERP Post window.
 *
 * @author elopio
 *
 */
public class PostWindow extends GeneralLedgerJournalWindow {

  /** The window title. */
  @SuppressWarnings("hiding")
  public static String TITLE = "Journal Entries Report";

  /**
   * Class constructor.
   */
  public PostWindow() {
    super(TITLE);
  }

  public PostWindow(String name) {
    super(name);
    PostWindow.TITLE = name;
  }
}
