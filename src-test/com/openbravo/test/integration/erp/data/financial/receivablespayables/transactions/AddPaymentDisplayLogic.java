/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:15
 * Contributor(s):
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class to structure Add Payment Data to execute assert display logic
 *
 * @author lorenzo.fidalgo
 *
 */
public class AddPaymentDisplayLogic extends DataObject {

  /**
   * Class builder
   *
   * @author lorenzo.fidalgo
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the Document value.
     *
     * @param value
     *          The Document value.
     * @return The builder for this class.
     */
    public Builder trxtype(boolean value) {
      this.dataFields.put("trxtype", value);
      return this;
    }

    /**
     * Set the Received From value.
     *
     * @param value
     *          The Received From value.
     * @return The builder for this class.
     */
    public Builder received_from(boolean value) {
      this.dataFields.put("received_from", value);
      return this;
    }

    /**
     * Set the Payment Method value.
     *
     * @param value
     *          The Payment Method value.
     * @return The builder for this class.
     */
    public Builder fin_paymentmethod_id(boolean value) {
      this.dataFields.put("fin_paymentmethod_id", value);
      return this;
    }

    /**
     * Set the Payment document number value.
     *
     * @param value
     *          The Payment document number value.
     * @return The builder for this class.
     */
    public Builder payment_documentno(boolean value) {
      this.dataFields.put("payment_documentno", value);
      return this;
    }

    /**
     * Set the Reference number value.
     *
     * @param value
     *          The Reference number value.
     * @return The builder for this class.
     */
    public Builder reference_no(boolean value) {
      this.dataFields.put("reference_no", value);
      return this;
    }

    /**
     * Set the Document Action value.
     *
     * @param value
     *          The Document Action value.
     * @return The builder for this class.
     */
    public Builder document_action(boolean value) {
      this.dataFields.put("document_action", value);
      return this;
    }

    /**
     * Set the Overpayment Action value.
     *
     * @param value
     *          The Overpayment Action value.
     * @return The builder for this class.
     */
    public Builder overpayment_action(boolean value) {
      this.dataFields.put("overpayment_action", value);
      return this;
    }

    /**
     * Set the Deposit To value.
     *
     * @param value
     *          The Deposit To value.
     * @return The builder for this class.
     */
    public Builder fin_financial_account_id(boolean value) {
      this.dataFields.put("fin_financial_account_id", value);
      return this;
    }

    /**
     * Set the Currency value.
     *
     * @param value
     *          The Currency value.
     * @return The builder for this class.
     */
    public Builder c_currency_id(boolean value) {
      this.dataFields.put("c_currency_id", value);
      return this;
    }

    /**
     * Set the Expected Payment In value.
     *
     * @param value
     *          The Expected Payment In value.
     * @return The builder for this class.
     */
    public Builder expected_payment(boolean value) {
      this.dataFields.put("expected_payment", value);
      return this;
    }

    /**
     * Set the Actual Payment In value.
     *
     * @param value
     *          The Actual Payment In value.
     * @return The builder for this class.
     */
    public Builder actual_payment(boolean value) {
      this.dataFields.put("actual_payment", value);
      return this;
    }

    /**
     * Set the Payment In Date value.
     *
     * @param value
     *          The Payment In Date value.
     * @return The builder for this class.
     */
    public Builder payment_date(boolean value) {
      this.dataFields.put("payment_date", value);
      return this;
    }

    /**
     * Set the Transaction Type value.
     *
     * @param value
     *          The Transaction Type value.
     * @return The builder for this class.
     */
    public Builder transaction_type(boolean value) {
      this.dataFields.put("transaction_type", value);
      return this;
    }

    /**
     * Set the Amount GL Items value.
     *
     * @param value
     *          The Amount GL Items value.
     * @return The builder for this class.
     */
    public Builder amount_gl_items(boolean value) {
      this.dataFields.put("amount_gl_items", value);
      return this;
    }

    /**
     * Set the Amount Order Invoices value.
     *
     * @param value
     *          The Amount Order Invoices value.
     * @return The builder for this class.
     */
    public Builder amount_inv_ords(boolean value) {
      this.dataFields.put("amount_inv_ords", value);
      return this;
    }

    /**
     * Set the Total value.
     *
     * @param value
     *          The Total value.
     * @return The builder for this class.
     */
    public Builder total(boolean value) {
      this.dataFields.put("total", value);
      return this;
    }

    /**
     * Set the Difference value.
     *
     * @param value
     *          The Difference value.
     * @return The builder for this class.
     */
    public Builder difference(boolean value) {
      this.dataFields.put("difference", value);
      return this;
    }

    /**
     * Set the Currency To value.
     *
     * @param value
     *          The Currency To value.
     * @return The builder for this class.
     */
    public Builder c_currency_to_id(boolean value) {
      this.dataFields.put("c_currency_to_id", value);
      return this;
    }

    /**
     * Set the Converted Amount value.
     *
     * @param value
     *          The Converted Amount value.
     * @return The builder for this class.
     */
    public Builder converted_amount(boolean value) {
      this.dataFields.put("converted_amount", value);
      return this;
    }

    /**
     * Set the Conversion Rate value.
     *
     * @param value
     *          The Conversion Rate value.
     * @return The builder for this class.
     */
    public Builder conversion_rate(boolean value) {
      this.dataFields.put("conversion_rate", value);
      return this;
    }

    /* Groups to assert */
    /*                  */
    /**
     * Set the section field_group_glitem value.
     *
     * @param value
     *          The section field_group_glitem value.
     * @return The builder for this class.
     */
    public Builder field_group_glitem(boolean value) {
      this.dataFields.put("7B6B5F5475634E35A85CF7023165E50B", value);
      return this;
    }

    /**
     * Set the section field_group_glitem value.
     *
     * @param value
     *          The section field_group_glitem value.
     * @return The builder for this class.
     */
    public Builder field_group_order(boolean value) {
      this.dataFields.put("0C672A3B7CDF416F9522DF3FA5AE4022", value);
      return this;
    }

    /**
     * Set the section field_group_glitem value.
     *
     * @param value
     *          The section field_group_glitem value.
     * @return The builder for this class.
     */
    public Builder field_group_credit(boolean value) {
      this.dataFields.put("CB265F2D7ACF439F9FB5EFBFA0B50363", value);
      return this;
    }

    /**
     * Set the section field_group_glitem value.
     *
     * @param value
     *          The section field_group_glitem value.
     * @return The builder for this class.
     */
    public Builder field_group_totals(boolean value) {
      this.dataFields.put("BFFF70E721654110AD5BACF3D4216D3A", value);
      return this;
    }

    /*                      */
    /* End groups to assert */

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public AddPaymentDisplayLogic build() {
      return new AddPaymentDisplayLogic(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private AddPaymentDisplayLogic(Builder builder) {
    dataFields = builder.dataFields;
  }

}
