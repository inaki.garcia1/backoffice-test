/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  David Miguélez <david.miguelez@openbravo.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.accounting.testsuites.FAMc_MulticurrencySchema;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.ReconciliationsLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;

/**
 * This test case posts a reconciliation.
 *
 * @author David Miguélez
 */

@RunWith(Parameterized.class)
public class FAMc_040SalesPostREC extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /* Data for [FAMc_040] Post a reconciliation. */
  /** The sales invoice header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  /**
   * Expected journal entry lines. Is an array of arrays. Each line has account number, name, debit
   * and credit.
   */
  private String[][] journalEntryLines1;
  private String[][] journalEntryLines2;
  /** The account header data. */
  AccountData accountHeaderData;
  /** The reconciliation lines data. */
  ReconciliationsLinesData reconciliationsLinesData;
  /**
   * Expected journal entry lines for the reconciliation. Is an array of arrays. Each line has
   * account number, name, debit and credit.
   */
  private String[][] reconciliationJournalEntryLines1;
  private String[][] reconciliationJournalEntryLines2;

  /**
   * Class constructor.
   *
   * @param salesInvoiceHeaderData
   *          The sales invoice header data.
   * @param journalEntryLines1
   *          Expected journal entry lines. Is an array of arrays. Each line has account number,
   *          name, debit and credit.
   * @param journalEntryLines2
   *          Expected journal entry lines. Is an array of arrays. Each line has account number,
   *          name, debit and credit.
   * @param accountHeaderData
   *          The account Header Data.
   * @param reconciliationsLinesData
   *          The reconciliation lines Data.
   * @param reconciliationJournalEntryLines1
   *          Expected journal entry lines for the reconciliation. Is an array of arrays. Each line
   *          has account number, name, debit and credit.
   * @param reconciliationJournalEntryLines2
   *          Expected journal entry lines for the reconciliation. Is an array of arrays. Each line
   *          has account number, name, debit and credit.
   */

  public FAMc_040SalesPostREC(SalesInvoiceHeaderData salesInvoiceHeaderData,
      String[][] journalEntryLines1, String[][] journalEntryLines2, AccountData accountHeaderData,
      ReconciliationsLinesData reconciliationsLinesData,
      String[][] reconciliationJournalEntryLines1, String[][] reconciliationJournalEntryLines2) {

    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.journalEntryLines1 = journalEntryLines1;
    this.journalEntryLines2 = journalEntryLines2;
    this.accountHeaderData = accountHeaderData;
    this.reconciliationsLinesData = reconciliationsLinesData;
    this.reconciliationJournalEntryLines1 = reconciliationJournalEntryLines1;
    this.reconciliationJournalEntryLines2 = reconciliationJournalEntryLines2;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<Object[]> orderToCashValues() {

    return Arrays.asList(new Object[][] { {
        // Parameters for [FAMc_040] Post a reconciliation.
        new SalesInvoiceHeaderData.Builder().documentNo("I/9").build(),
        new String[][] { { "43000", "Clientes (euros)", "500.00", "" },
            { "70000", "Ventas de mercaderías", "", "500.00" } },
        new String[][] { { "11400", "Accounts receivable", "1,000.00", "" },
            { "41100", "Sales", "", "1,000.00" } },
        new AccountData.Builder().name("Accounting Documents DOLLAR").build(),
        new ReconciliationsLinesData.Builder().documentNo("1000041").build(),
        new String[][] {
            { "57200", "Bancos e instituciones de crédito c/c vista euros", "416.67", "" },
            { "66800", "Diferencias negativas de cambio", "83.33", "" },
            { "43000", "Clientes (euros)", "", "500.00" } },
        new String[][] { { "11100", "Petty Cash", "1,250.00", "" },
            { "11400", "Accounts receivable", "", "1,000.00" },
            { "45200", "Bank revaluation gain", "", "250.00" } } } });
  }

  /**
   * Test case post a reconciliation.
   */
  @Test
  public void FAMc_040PostAReconciliation() {
    logger.info("** Start of test case [FAMc_040] Post a reconciliation. **");

    final SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.select(salesInvoiceHeaderData);
    if (salesInvoice.isPosted()) {
      salesInvoice.unpost();
      // salesInvoice
      // .assertProcessCompletedSuccessfully2("Number of unposted documents = 1, Number of deleted
      // journal entries = 2");
    }
    salesInvoice.post();
    mainPage.closeView("Sales Invoice");

    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines1.length);
    post.assertJournalLines2(journalEntryLines1);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");

    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines2.length);
    post.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    final FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.Reconciliations reconciliations = financialAccount.new Reconciliations(
        mainPage);
    reconciliations.select(reconciliationsLinesData);
    if (reconciliations.isPosted()) {
      reconciliations.unpost();
      // reconciliations
      // .assertProcessCompletedSuccessfully2("Number of unposted documents = 1, Number of deleted
      // journal entries = 2");
    }
    reconciliations.post();

    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(reconciliationJournalEntryLines1.length);
    post.assertJournalLines2(reconciliationJournalEntryLines1);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");

    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(reconciliationJournalEntryLines2.length);
    post.assertJournalLines2(reconciliationJournalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");
    logger.info("** End of test case [FAMc_040] Post a reconciliation. **");
  }

}
