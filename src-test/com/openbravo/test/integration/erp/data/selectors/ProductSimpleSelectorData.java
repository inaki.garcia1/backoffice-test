/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2013 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * Contributor(s):
 *   Sandra Huget <shug@openbravo.comt>
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.selectors;

import java.util.LinkedHashMap;

/**
 *
 * Class for product selector data.
 *
 * @author elopio.
 *
 */
public class ProductSimpleSelectorData extends SelectorDataObject {

  /**
   * Class builder.
   *
   * @author elopio
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the search key value.
     *
     * @param value
     *          The search key value.
     * @return The builder for this class.
     */
    public Builder searchKey(String value) {
      this.dataFields.put("searchKey", value);
      return this;
    }

    /**
     * Set the name value.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder productName(String value) {
      this.dataFields.put("productName", value);
      return this;
    }

    /**
     * Set the standardPrice value.
     *
     * @param value
     *          The standardPrice value.
     * @return The builder for this class.
     */
    public Builder standardPrice(String value) {
      this.dataFields.put("standardPrice", value);
      return this;
    }

    /**
     * Set the netListPrice value.
     *
     * @param value
     *          The netListPrice value.
     * @return The builder for this class.
     */
    public Builder netListPrice(String value) {
      this.dataFields.put("netListPrice", value);
      return this;
    }

    /**
     * Set the priceListVersion value.
     *
     * @param value
     *          The priceListVersion value.
     * @return The builder for this class.
     */
    public Builder priceListVersion(String value) {
      this.dataFields.put("priceListVersion", value);
      return this;
    }

    /**
     * Set the priceListVersionName value.
     *
     * @param value
     *          The priceListVersionName value.
     * @return The builder for this class.
     */
    public Builder priceListVersionName(String value) {
      this.dataFields.put("priceListVersionName", value);
      return this;
    }

    /**
     * Set the priceLimit value.
     *
     * @param value
     *          The priceLimit value.
     * @return The builder for this class.
     */
    public Builder priceLimit(String value) {
      this.dataFields.put("priceLimit", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ProductSimpleSelectorData build() {
      return new ProductSimpleSelectorData(this);
    }
  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ProductSimpleSelectorData(Builder builder) {
    dataFields = builder.dataFields;
  }

  /**
   * Get the search key of the product selector data object.
   *
   * @return the search key of the product selector data object.
   */
  @Override
  public String getSearchKey() {
    return (String) dataFields.get("searchKey");
  }

  /**
   * Get the display value of the selector data object.
   *
   * @return the value displayed by the selector data object.
   */
  @Override
  public String getDisplayValue() {
    return (String) dataFields.get("productName");
  }
}
