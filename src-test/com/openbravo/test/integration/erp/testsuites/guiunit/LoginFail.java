/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  plu@openbravo.com
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testsuites.guiunit;

import java.util.Arrays;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.testscripts.gui.FailedLogin;

/**
 * Test error messages on Logging
 *
 * @author plujan
 */
@RunWith(Parameterized.class)
public class LoginFail extends FailedLogin {

  /**
   * Class constructor.
   *
   * @param userName
   *          Name of the user that will be used for this test.
   * @param password
   *          Password of the user that will be used for this test.
   */
  public LoginFail(String userName, String password) {
    super(userName, password);
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<String[]> loggingValues() {
    return Arrays.asList(new String[][] { { "Openbravo", "badpassword" } });
  }
}
