/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_TransactionRefactor;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.util.OBDate;

/**
 * Create a Transaction with a GL Item.
 *
 * @author Unai Martirena
 */
@RunWith(Parameterized.class)
public class APRTransaction008 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /** The account header data. */
  AccountData accountHeaderData;
  /** The transaction header data. */
  TransactionsData transactionLinesData;
  /** The transaction verification header data. */
  TransactionsData transactionLinesVerificationData;

  /**
   * Class constructor.
   *
   * @param accountHeaderData
   *          The account Header Data.
   * @param transactionLinesData
   *          The transaction Header Data.
   */
  public APRTransaction008(AccountData accountHeaderData, TransactionsData transactionLinesData,
      TransactionsData transactionLinesVerificationData) {
    this.accountHeaderData = accountHeaderData;
    this.transactionLinesData = transactionLinesData;
    this.transactionLinesVerificationData = transactionLinesVerificationData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        /* Parameters for [APRTransaction008] Create Financial Account Transaction. */
        new AccountData.Builder().name("USA Bank").build(),
        new TransactionsData.Builder().transactionType("BP Withdrawal")
            .gLItem("Salaries")
            .paymentAmount("15.00")
            .build(),
        new TransactionsData.Builder().transactionType("BP Withdrawal")
            .transactionDate(OBDate.CURRENT_DATE)
            .accountingDate(OBDate.CURRENT_DATE)
            .currency("USD")
            .depositAmount("0.00")
            .paymentAmount("15.00")
            .organization("USA")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("").build())
            .product(new ProductSelectorData.Builder().name("").build())
            .project("")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test to create a sales order, and a goods receipt and sales invoice from that order.
   *
   * @throws ParseException
   */
  @Test
  public void APRTransaction008Test() throws ParseException {
    logger.info(
        "** Start of test case [APRTransaction008] Create a withdrawal transaction with a GL Item. **");

    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.Transaction transactions = financialAccount.new Transaction(mainPage);
    transactions.create(transactionLinesData);
    transactions.assertSaved();
    transactions.assertData(transactionLinesVerificationData);
    transactions.process();

    logger.info(
        "** End of test case [APRTransaction008] Create a withdrawal transaction with a GL Item. **");
  }
}
