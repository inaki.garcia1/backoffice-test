# *************************************************************************
# * The contents of this file are subject to the Openbravo  Public  License
# * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
# * Version 1.1  with a permitted attribution clause; you may not  use this
# * file except in compliance with the License. You  may  obtain  a copy of
# * the License at http://www.openbravo.com/legal/license.html
# * Software distributed under the License  is  distributed  on  an "AS IS"
# * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
# * License for the specific  language  governing  rights  and  limitations
# * under the License.
# * The Original Code is Openbravo ERP.
# * The Initial Developer of the Original Code is Openbravo SL
# * All portions are Copyright (C) 2008-2018 Openbravo SLU
# * All Rights Reserved.
# * Contributor(s):  Leo Arias <leo.arias@openbravo.com>,
#                    Pablo Lujan <plu@openbravo.com>,
#                    Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>,
#					 Iñaki Garcia <inaki.garcia@openbravo.com>.
# ************************************************************************

# Selenium properties
selenium.port=4444
selenium.speed=50
selenium.browser=firefox

# browserPath is the full path to the browser 
# Example (Linux)
# selenium.browserPath=/usr/lib/firefox-3.6/firefox
# Example (Windows)
# selenium.browserPath="C:\\Program Files\\Mozilla Firefox\\firefox.exe"
selenium.browserPath=

# Openbravo server properties
openbravo.url=http://localhost:8080/
openbravo.context=openbravo

# Preferred interface
# CLASSIC: 2.50 based windows
# NEW: New layout
openbravo.interface=NEW

# Preferred maturity level
#	- Used for retrieving modules via the Module Management 
#		window. Change this in case a different level is required
#	- Allowed values (default value QA_APPROVED): 
#     TEST
#     QA
#     QA_APPROVED
#     CONFIRMED_STABLE 
maturity=QA_APPROVED

# Test properties
test.systemAdministratorUser=Openbravo
test.systemAdministratorPassword=openbravo
test.clientAdministratorUser=SampleClientAdmin
test.clientAdministratorPassword=SampleClientAdmin
test.user=QAAdmin
test.password=QAAdmin
test.format.number.output=#,##0.00
test.format.number.decimal=.
test.format.number.grouping=,
#Date format configuration. Provisional feature. (Spanish date format=dd-MM-yyyy; USA date format=MM-dd-yyyy)
dateFormat.java=dd-MM-yyyy

#Video record feature ('t' to activate it)
test.videorecord=f

#Stop the suite if failure or continue executing all the test suite. By default, it will NOT execute next test of the suite if a test fails ('f' to stop if test fails).
test.stopSuiteWhenFailure=t

# Active module properties
#################
# Autosave feature:
	# 1 autosave routine
	# 2 save by closing form
	# 3 / <default> clicking save button
test.autosave=3

# Selector input method preferences:
	# TEXTBOX t
	# COMBO c
	# POPUP p
test.selectorInputPref=t

# DB properties 
# - Used for SQL ant task:
#     * scenario.clean
# - bbdd.rdbms used as an argument for a workaround in smoke tests that
#   open Stock Reservations window and run on Oracle database
#	* Change this value to 'ORACLE' if it's the case 
bbdd.sid=openbravo
bbdd.user=openbravo
bbdd.password=mypassword
bbdd.driver=org.postgresql.Driver
bbdd.url=jdbc:postgresql://localhost:5432
bbdd.rdbms=POSTGRE

####### Deprecated #########
## Tomcat commands
##tomcat.start=/etc/init.d/tomcat6 start
##tomcat.stop=/etc/init.d/tomcat6 stop
############################

####### Deprecated #########
## Example (Linux)
## openbravo.application=/opt/openbravoERP/
## Example (Windows)
##openbravo.application=c:\\OpenbravoERP\\
##openbravo.application=/opt/openbravoERP/
############################