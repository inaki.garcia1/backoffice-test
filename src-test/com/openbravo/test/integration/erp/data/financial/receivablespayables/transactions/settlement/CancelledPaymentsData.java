/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.settlement;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;

/**
 *
 * Class for CancelledPaymentsData
 *
 * @author plujan
 *
 */
public class CancelledPaymentsData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the invoice value
     *
     * Description: A document listing products quantities and prices payment terms etc.
     *
     * @param value
     *          The invoice value.
     * @return The builder for this class.
     */
    public Builder invoice(String value) {
      this.dataFields.put("invoice", value);
      return this;
    }

    /**
     * Set the settlementGenerate value
     *
     * Description: Settlement generate
     *
     * @param value
     *          The settlementGenerate value.
     * @return The builder for this class.
     */
    public Builder settlementGenerate(String value) {
      this.dataFields.put("settlementGenerate", value);
      return this;
    }

    /**
     * Set the status value
     *
     * Description: A defined state or position of a payment.
     *
     * @param value
     *          The status value.
     * @return The builder for this class.
     */
    public Builder status(String value) {
      this.dataFields.put("status", value);
      return this;
    }

    /**
     * Set the bankStatementLine value
     *
     * Description: A statement displaying one transaction in the bank statement.
     *
     * @param value
     *          The bankStatementLine value.
     * @return The builder for this class.
     */
    public Builder bankStatementLine(String value) {
      this.dataFields.put("bankStatementLine", value);
      return this;
    }

    /**
     * Set the dueDate value
     *
     * Description: The date when a specified request must be carried out by.
     *
     * @param value
     *          The dueDate value.
     * @return The builder for this class.
     */
    public Builder dueDate(String value) {
      this.dataFields.put("dueDate", value);
      return this;
    }

    /**
     * Set the writeOffAmount value
     *
     * Description: A monetary sum that can be deducted from tax obligations.
     *
     * @param value
     *          The writeOffAmount value.
     * @return The builder for this class.
     */
    public Builder writeOffAmount(String value) {
      this.dataFields.put("writeOffAmount", value);
      return this;
    }

    /**
     * Set the amount value
     *
     * Description: Amount in a defined currency
     *
     * @param value
     *          The amount value.
     * @return The builder for this class.
     */
    public Builder amount(String value) {
      this.dataFields.put("amount", value);
      return this;
    }

    /**
     * Set the paymentComplete value
     *
     * Description: A confirmation stating whether the request has been closed through a monetary
     * transaction.
     *
     * @param value
     *          The paymentComplete value.
     * @return The builder for this class.
     */
    public Builder paymentComplete(Boolean value) {
      this.dataFields.put("paymentComplete", value);
      return this;
    }

    /**
     * Set the formOfPayment value
     *
     * Description: The method used for payment of this transaction.
     *
     * @param value
     *          The formOfPayment value.
     * @return The builder for this class.
     */
    public Builder formOfPayment(String value) {
      this.dataFields.put("formOfPayment", value);
      return this;
    }

    /**
     * Set the cashbook value
     *
     * Description: A document used to manage all cash transactions.
     *
     * @param value
     *          The cashbook value.
     * @return The builder for this class.
     */
    public Builder cashbook(String value) {
      this.dataFields.put("cashbook", value);
      return this;
    }

    /**
     * Set the bankAccount value
     *
     * Description: A monetary account of funds held in a recognized banking institution.
     *
     * @param value
     *          The bankAccount value.
     * @return The builder for this class.
     */
    public Builder bankAccount(String value) {
      this.dataFields.put("bankAccount", value);
      return this;
    }

    /**
     * Set the cashJournalLine value
     *
     * Description: A statement displaying one transaction in the cash journal.
     *
     * @param value
     *          The cashJournalLine value.
     * @return The builder for this class.
     */
    public Builder cashJournalLine(String value) {
      this.dataFields.put("cashJournalLine", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the settlement value
     *
     * Description: The process of exchanging or carrying out a payment once a transaction is made.
     *
     * @param value
     *          The settlement value.
     * @return The builder for this class.
     */
    public Builder settlement(String value) {
      this.dataFields.put("settlement", value);
      return this;
    }

    /**
     * Set the receipt value
     *
     * Description: Indicates a sales or purchase transaction.
     *
     * @param value
     *          The receipt value.
     * @return The builder for this class.
     */
    public Builder receipt(Boolean value) {
      this.dataFields.put("receipt", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public CancelledPaymentsData build() {
      return new CancelledPaymentsData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private CancelledPaymentsData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
