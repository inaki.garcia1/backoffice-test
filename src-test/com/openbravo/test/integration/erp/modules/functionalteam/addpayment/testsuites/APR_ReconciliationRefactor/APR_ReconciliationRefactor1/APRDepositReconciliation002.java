/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_ReconciliationRefactor.APR_ReconciliationRefactor1;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.Keys;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.MatchStatementData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.bankstatement.BankStatementHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.BankStatementLinesData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.FindTransactionsToMatchProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementGrid;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Match Automatically a Transaction and a Bank Statement Line
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRDepositReconciliation002 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /** The account header data. */
  AccountData accountHeaderData;
  /** The transaction data. */
  TransactionsData transactionData;
  /** The bank statement data. */
  BankStatementHeaderData bankStatementHeaderData;
  BankStatementLinesData bankStatementLinesData;
  MatchStatementData matchStatementData;
  MatchStatementData matchStatementData2;

  /**
   * Class constructor.
   *
   * @param accountHeaderData
   *          The account Header Data.
   * @param bankStatementHeaderData
   *          The bank statement Header Data.
   * @param bankStatementLinesData
   *          The bank statement lines Data.
   */
  public APRDepositReconciliation002(AccountData accountHeaderData,
      TransactionsData transactionData, BankStatementHeaderData bankStatementHeaderData,
      BankStatementLinesData bankStatementLinesData, MatchStatementData matchStatementData,
      MatchStatementData matchStatementData2) {
    this.accountHeaderData = accountHeaderData;
    this.transactionData = transactionData;
    this.bankStatementHeaderData = bankStatementHeaderData;
    this.bankStatementLinesData = bankStatementLinesData;
    this.matchStatementData = matchStatementData;
    this.matchStatementData2 = matchStatementData2;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> aPRDepositReconciliation002Values() {
    Object[][] data = new Object[][] { {
        /* Parameters for [APRDepositReconciliation002Test] */
        new AccountData.Builder().name("Spain Bank").build(),
        new TransactionsData.Builder().transactionType("BP Deposit")
            .gLItem("Salaries")
            .depositAmount("200.00")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer B").build())
            .build(),
        new BankStatementHeaderData.Builder().name("TestReconciliationD002").build(),
        new BankStatementLinesData.Builder().referenceNo("D002")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer B").build())
            .gLItem("Salaries")
            .cramount("200.00")
            .build(),
        new MatchStatementData.Builder().bankStatementType("D")
            .sender("Customer B")
            .referenceNo("D002")
            .glitem("Salaries")
            .amount("200")
            .affinity("")
            .matchedDocument("")
            .matchingType("")
            .businessPartner("")
            .transactionAmount("")
            .transactionGLItem("")
            .build(),
        new MatchStatementData.Builder().bankStatementType("D")
            .sender("Customer B")
            .referenceNo("D002")
            .glitem("Salaries")
            .amount("200")
            .affinity("MA")
            .matchedDocument("T")
            .matchingType("MA")
            .businessPartner("Customer B")
            .transactionAmount("200")
            .transactionGLItem("Salaries")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Match Automatically a Transaction and a Bank Statement Line
   *
   * @throws ParseException
   */
  @Test
  public void APRDepositReconciliation002Test() throws ParseException {
    logger.info("** Start of test case [APRDepositReconciliation002Test]");

    // Select Financial Account
    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);

    // Get the date of the latest Bank Statement
    FinancialAccount.BankStatements bankStatement = financialAccount.new BankStatements(mainPage);
    String date;
    Sleep.trySleep();
    if (bankStatement.getRecordCount() == 0) {
      date = OBDate.CURRENT_DATE;
    } else {
      bankStatement.selectWithoutFiltering(0);
      date = OBDate.addDaysToDate((String) bankStatement.getData("transactionDate"), 1);
    }

    // Create a Transaction
    FinancialAccount.Transaction transactions = financialAccount.new Transaction(mainPage);
    transactionData.addDataField("transactionDate", date);
    transactionData.addDataField("dateAcct", date);
    transactions.create(transactionData);
    transactions.assertSaved();
    transactions.assertData(transactionData);
    transactions.process();
    transactions.assertProcessCompletedSuccessfully2();

    // Create Bank Statement
    bankStatementHeaderData.addDataField("transactionDate", date);
    bankStatementHeaderData.addDataField("importdate", date);
    bankStatement.create(bankStatementHeaderData);
    bankStatement.assertSaved();
    FinancialAccount.BankStatements.BankStatementLines bankStatementLines = bankStatement.new BankStatementLines(
        mainPage);
    bankStatementLinesData.addDataField("transactionDate", date);
    bankStatementLines.create(bankStatementLinesData);
    bankStatementLines.assertSaved();
    bankStatementLines.assertData(bankStatementLinesData);
    bankStatement.process();
    bankStatement.assertProcessCompletedSuccessfully2();

    // Open Match Statement Grid
    @SuppressWarnings("rawtypes")
    MatchStatementProcess matchStatementProcess = financialAccount.openMatchStatement(false);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    Sleep.trySleep();
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    SeleniumSingleton.INSTANCE.switchTo().activeElement().sendKeys(Keys.TAB);
    SeleniumSingleton.INSTANCE.switchTo().activeElement().sendKeys(Keys.SHIFT, Keys.TAB);
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    SeleniumSingleton.INSTANCE.switchTo().activeElement().sendKeys(Keys.TAB);
    SeleniumSingleton.INSTANCE.switchTo().activeElement().sendKeys(Keys.SHIFT, Keys.TAB);
    matchStatementData.addDataField("banklineDate", date);
    matchStatementData.addDataField("trxDate", "");
    // TODO L: This assert is asserting data below the popup. That must be avoided.
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData);

    // Select Transaction
    // TODO L4: Remove the following static sleep if not required
    Sleep.trySleep(6000);
    FindTransactionsToMatchProcess findTransactionsToMatchProcess = ((MatchStatementGrid) matchStatementProcess
        .getMatchStatementGrid()).clickSearch(0);
    // TODO L4: Remove the following static sleep if not required
    Sleep.trySleep(6000);
    findTransactionsToMatchProcess.getTransactionsToMatchGrid().clearFilters();
    Sleep.trySleep();
    findTransactionsToMatchProcess.getTransactionsToMatchGrid()
        .filter("depositAmount", (String) transactionData.getDataField("depositAmount"));
    SeleniumSingleton.INSTANCE.switchTo().activeElement().sendKeys(Keys.TAB);
    SeleniumSingleton.INSTANCE.switchTo().activeElement().sendKeys(Keys.SHIFT, Keys.TAB);
    findTransactionsToMatchProcess.getTransactionsToMatchGrid().filter("transactionDate", date);
    SeleniumSingleton.INSTANCE.switchTo().activeElement().sendKeys(Keys.TAB);
    SeleniumSingleton.INSTANCE.switchTo().activeElement().sendKeys(Keys.SHIFT, Keys.TAB);
    findTransactionsToMatchProcess.getTransactionsToMatchGrid()
        .filter("businessPartner", "Customer B");
    SeleniumSingleton.INSTANCE.switchTo().activeElement().sendKeys(Keys.TAB);
    SeleniumSingleton.INSTANCE.switchTo().activeElement().sendKeys(Keys.SHIFT, Keys.TAB);
    findTransactionsToMatchProcess.getTransactionsToMatchGrid().selectRecord(0);
    findTransactionsToMatchProcess.process();
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    Sleep.trySleep();
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    SeleniumSingleton.INSTANCE.switchTo().activeElement().sendKeys(Keys.TAB);
    SeleniumSingleton.INSTANCE.switchTo().activeElement().sendKeys(Keys.SHIFT, Keys.TAB);
    Sleep.trySleep();
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    SeleniumSingleton.INSTANCE.switchTo().activeElement().sendKeys(Keys.TAB);
    SeleniumSingleton.INSTANCE.switchTo().activeElement().sendKeys(Keys.SHIFT, Keys.TAB);
    Sleep.trySleep();
    matchStatementData2.addDataField("banklineDate", date);
    matchStatementData2.addDataField("trxDate", date);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData2);

    // Process Match Statement
    matchStatementProcess.process();
    financialAccount.assertProcessCompletedSuccessfully2();

    logger.info("** End of test case [APRDepositReconciliation002Test]");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
