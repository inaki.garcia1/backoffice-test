/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2019 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.reservation.testsuites.RES_Regressions.RES_Regressions1;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.masterdata.product.PriceData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductStockData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.warehouse.transactions.stockreservation.ReservationData;
import com.openbravo.test.integration.erp.data.warehouse.transactions.stockreservation.StockData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.masterdata.product.Product;
import com.openbravo.test.integration.erp.testscripts.masterdata.product.Product.ProductStock;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;
import com.openbravo.test.integration.erp.testscripts.warehouse.transactions.stockreservation.StockReservation;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test regression 38799
 */
@RunWith(Parameterized.class)
public class RESRegression38799 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  ProductData productData;
  PriceData productPriceData;
  GoodsReceiptHeaderData goodsReceiptHeaderData;
  GoodsReceiptHeaderData goodsReceiptHeaderVerificationData;
  GoodsReceiptLinesData goodsReceiptLinesData;
  GoodsReceiptLinesData goodsReceiptLineVerificationData;
  GoodsReceiptHeaderData completedGoodsReceiptHeaderVerificationData;
  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderLinesData;
  SalesOrderLinesData salesOrderLinesVerificationData;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;
  ReservationData reservationVerificationData;
  StockData stockVerificationData;
  SalesOrderLinesData salesOrderLinesData2;
  SalesOrderLinesData salesOrderLinesVerificationData2;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData2;
  ProductStockData productStockData;

  /**
   * Class constructor.
   *
   */
  public RESRegression38799(ProductData productData, PriceData productPriceData,
      GoodsReceiptHeaderData goodsReceiptHeaderData,
      GoodsReceiptHeaderData goodsReceiptHeaderVerificationData,
      GoodsReceiptLinesData goodsReceiptLinesData,
      GoodsReceiptLinesData goodsReceiptLineVerificationData,
      GoodsReceiptHeaderData completedGoodsReceiptHeaderVerificationData,
      SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData, SalesOrderLinesData salesOrderLinesData,
      SalesOrderLinesData salesOrderLinesVerificationData,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData,
      ReservationData reservationVerificationData, StockData stockVerificationData,
      SalesOrderLinesData salesOrderLinesData2,
      SalesOrderLinesData salesOrderLinesVerificationData2,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData2,
      ProductStockData productStockData) {
    this.productData = productData;
    this.productPriceData = productPriceData;
    this.goodsReceiptHeaderData = goodsReceiptHeaderData;
    this.goodsReceiptHeaderVerificationData = goodsReceiptHeaderVerificationData;
    this.goodsReceiptLinesData = goodsReceiptLinesData;
    this.goodsReceiptLineVerificationData = goodsReceiptLineVerificationData;
    this.completedGoodsReceiptHeaderVerificationData = completedGoodsReceiptHeaderVerificationData;
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesData = salesOrderLinesData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    this.reservationVerificationData = reservationVerificationData;
    this.stockVerificationData = stockVerificationData;
    this.reservationVerificationData = reservationVerificationData;
    this.stockVerificationData = stockVerificationData;
    this.salesOrderLinesData2 = salesOrderLinesData2;
    this.salesOrderLinesVerificationData2 = salesOrderLinesVerificationData2;
    this.bookedSalesOrderHeaderVerificationData2 = bookedSalesOrderHeaderVerificationData2;
    this.productStockData = productStockData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new ProductData.Builder().organization("Spain")
            .uOM("Unit")
            .productCategory("Finished Goods")
            .taxCategory("VAT 10%")
            .purchase(false)
            .sale(true)
            .productType("Item")
            .stocked(true)
            .build(),
        new PriceData.Builder().priceListVersion("Customer A")
            .standardPrice("2.00")
            .listPrice("2.00")
            .build(),
        new GoodsReceiptHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),
        new GoodsReceiptHeaderData.Builder().organization("Spain")
            .documentType("MM Receipt")
            .warehouse("Spain warehouse")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .build(),
        new GoodsReceiptLinesData.Builder().storageBin("L01").build(),
        new GoodsReceiptLinesData.Builder().lineNo("10").uOM("Unit").organization("Spain").build(),
        new GoodsReceiptHeaderData.Builder().documentStatus("Completed").build(),
        new SalesOrderHeaderData.Builder().transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new SalesOrderHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentMethod("1 (Spain)")
            .paymentTerms("30 days, 5")
            .warehouse("Spain warehouse")
            .invoiceTerms("Customer Schedule After Delivery")
            .invoiceAddress(".Pamplona, Street Customer center nº1")
            .build(),
        new SalesOrderLinesData.Builder().orderedQuantity("10").build(),
        new SalesOrderLinesData.Builder().uOM("Unit")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("20.00")
            .build(),
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("20.00")
            .grandTotalAmount("22.00")
            .currency("EUR")
            .build(),
        new ReservationData.Builder().organization("Spain")
            .quantity("10")
            .reservedQuantity("10")
            .releasedQuantity("0")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Completed")
            .build(),
        new StockData.Builder().storageBin("L01")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("10")
            .allocated(false)
            .build(),
        new SalesOrderLinesData.Builder().orderedQuantity("20").build(),
        new SalesOrderLinesData.Builder().lineNetAmount("40.00").build(),
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("40.00")
            .grandTotalAmount("44.00")
            .currency("EUR")
            .build(),
        new ProductStockData.Builder().storageBin("L01")
            .quantityOnHand("20")
            .reservedQty("20")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 38799
   *
   * @throws ParseException
   */
  @Test
  public void RESRegression38799Test() throws ParseException {
    logger.info("** Start of test case [RESRegression38799] Test regression 38799. **");

    // Create Product for this test
    String productName = "Test38799";

    int num1 = Product.ProductTab.getRecordCount(mainPage,
        new ProductData.Builder().name(productName).build());
    productName = productName + String.valueOf(num1);

    ProductData productData1 = productData;
    productData1.addDataField("searchKey", productName);
    productData1.addDataField("name", productName);

    Product.ProductTab.create(mainPage, productData1);
    Product.Price.create(mainPage, productPriceData);

    // Initialize stock quantity for the product
    GoodsReceipt goodsReceipt = new GoodsReceipt(mainPage).open();
    goodsReceipt.create(goodsReceiptHeaderData);
    goodsReceipt.assertSaved();
    goodsReceipt.assertData(goodsReceiptHeaderVerificationData);
    GoodsReceipt.Lines goodsReceiptLines1 = goodsReceipt.new Lines(mainPage);
    goodsReceiptLinesData.addDataField("product",
        new ProductCompleteSelectorData.Builder().name(productName).build());
    goodsReceiptLinesData.addDataField("movementQuantity", "20");
    goodsReceiptLines1.create(goodsReceiptLinesData);
    goodsReceiptLines1.assertSaved();
    goodsReceiptLines1.assertData(goodsReceiptLineVerificationData);
    goodsReceipt.complete();
    goodsReceipt.assertProcessCompletedSuccessfully2();
    goodsReceipt.assertData(completedGoodsReceiptHeaderVerificationData);

    // Book first Sales Order
    SalesOrder salesOrder = new SalesOrder(mainPage).open();
    salesOrder.create(salesOrderHeaderData);
    salesOrder.assertSaved();
    salesOrder.assertData(salesOrderHeaderVerficationData);
    String orderNo = salesOrder.getData("documentNo").toString();
    SalesOrder.Lines salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLinesData.addDataField("product",
        new ProductCompleteSelectorData.Builder().name(productName).build());
    salesOrderLines.create(salesOrderLinesData);
    salesOrderLines.assertSaved();
    salesOrderLines.assertData(salesOrderLinesVerificationData);
    String orderLineIdentifier = String.format(" - %s - %s", salesOrderLines.getData("lineNo"),
        salesOrderLines.getData("lineNetAmount"));
    salesOrder.book();
    salesOrder.assertProcessCompletedSuccessfully2();
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);
    String orderIdentifier = String.format("%s - %s - %s", orderNo, salesOrder.getData("orderDate"),
        bookedSalesOrderHeaderVerificationData.getDataField("grandTotalAmount"));
    orderLineIdentifier = orderIdentifier + orderLineIdentifier;
    Sleep.trySleep(15000);
    salesOrderLines.manageReservation();
    Sleep.trySleep(15000);
    StockReservation stockReservation = new StockReservation(mainPage).open();
    Sleep.trySleep(3000);
    stockReservation.select(new ReservationData.Builder().salesOrderLine(orderLineIdentifier)
        .product(productName)
        .build());
    Sleep.trySleep(3000);
    stockReservation.assertCount(1);
    stockReservation.assertData(
        (ReservationData) reservationVerificationData.addDataField("product", productName)
            .addDataField("salesOrderLine", orderLineIdentifier));
    StockReservation.Stock stock = stockReservation.new Stock(mainPage);
    stock.assertCount(1);
    stock.assertData(stockVerificationData);

    Sleep.trySleep();

    // Book Second Sales Order
    SalesOrder salesOrder2 = new SalesOrder(mainPage).open();
    salesOrder2.create(salesOrderHeaderData);
    salesOrder2.assertSaved();
    salesOrder2.assertData(salesOrderHeaderVerficationData);
    String orderNo2 = salesOrder2.getData("documentNo").toString();
    SalesOrder.Lines salesOrderLines2 = salesOrder2.new Lines(mainPage);
    salesOrderLinesData.addDataField("product",
        new ProductCompleteSelectorData.Builder().name(productName).build());
    Sleep.trySleep();
    salesOrderLines2.create(salesOrderLinesData);
    salesOrderLines2.assertSaved();
    salesOrderLines2.assertData(salesOrderLinesVerificationData);
    String orderLineIdentifier2 = String.format(" - %s - %s", salesOrderLines2.getData("lineNo"),
        salesOrderLines2.getData("lineNetAmount"));
    salesOrder2.book();
    salesOrder2.assertProcessCompletedSuccessfully2();
    salesOrder2.assertData(bookedSalesOrderHeaderVerificationData);
    String orderIdentifier2 = String.format("%s - %s - %s", orderNo2,
        salesOrder2.getData("orderDate"),
        bookedSalesOrderHeaderVerificationData.getDataField("grandTotalAmount"));
    orderLineIdentifier2 = orderIdentifier2 + orderLineIdentifier2;
    Sleep.trySleep(15000);
    salesOrderLines2.manageReservation();
    Sleep.trySleep(15000);
    StockReservation stockReservation2 = new StockReservation(mainPage).open();
    Sleep.trySleep(3000);
    stockReservation2.select(new ReservationData.Builder().salesOrderLine(orderLineIdentifier2)
        .product(productName)
        .build());
    Sleep.trySleep(3000);
    stockReservation2.assertCount(1);
    stockReservation2.assertData(
        (ReservationData) reservationVerificationData.addDataField("product", productName)
            .addDataField("salesOrderLine", orderLineIdentifier2));
    StockReservation.Stock stock2 = stockReservation2.new Stock(mainPage);
    stock2.assertCount(1);
    stock2.assertData(stockVerificationData);

    Sleep.trySleep(5000);

    // Verify the Product Stock for the product
    Product product = (Product) new Product(mainPage).open();
    product.select(new ProductData.Builder().name(productName).searchKey(productName).build());
    ProductStock productStock = product.new ProductStock(mainPage);
    productStock.assertCount(1);
    productStock.assertData(productStockData);

    Sleep.trySleep(5000);

    // Reactivate the Sales Order, increase ordered quantity, re-book the order
    salesOrder2 = new SalesOrder(mainPage).open();
    salesOrder2.select(new SalesOrderHeaderData.Builder().documentNo(orderNo2).build());
    salesOrder2.reactivate();
    salesOrderLines2 = salesOrder.new Lines(mainPage);
    salesOrderLines2.edit(salesOrderLinesData2);
    salesOrderLines2.assertSaved();
    salesOrderLines2.assertData(salesOrderLinesVerificationData2);
    salesOrder2.book();
    salesOrder2.assertProcessCompletedSuccessfully2();
    salesOrder2.assertData(bookedSalesOrderHeaderVerificationData2);

    Sleep.trySleep(5000);

    // Verify the Product Stock for the product
    product = (Product) new Product(mainPage).open();
    product.select(new ProductData.Builder().name(productName).searchKey(productName).build());
    productStock = product.new ProductStock(mainPage);
    productStock.assertCount(1);
    productStock.assertData(productStockData);

    logger.info("** End of test case [RESRegression38799] Test regression 38799. **");
  }
}
