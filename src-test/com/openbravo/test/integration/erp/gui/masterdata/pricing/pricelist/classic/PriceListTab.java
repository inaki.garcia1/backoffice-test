/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.masterdata.pricing.pricelist.classic;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelist.PriceListData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Price List tab.
 *
 * @author elopio
 *
 */
public class PriceListTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Price List";
  /** The tab level. */
  private static final int LEVEL = 0;

  /* GUI components. */
  /** Identifier of the Price List tab. */
  public static final String TAB = "tabname191";

  /** Index of the name column */
  public static final int COLUMN_NAME = 2;

  /** Identifier of the name text field */
  private static final String TEXT_FIELD_NAME = "Name";
  /** Identifier of the active check box */
  @SuppressWarnings("unused")
  private static final String CHECK_BOX_ACTIVE = "IsActive";
  /** Identifier of the is sales check box */
  private static final String CHECK_BOX_IS_SALES = "IsSOPriceList";
  /** Identifier of the currency combo box */
  private static final String COMBO_BOX_CURRENCY = "reportC_Currency_ID_S";

  /**
   * Class constructor
   *
   */
  public PriceListTab() {
    super(TITLE, LEVEL, TAB);
    addSubTab(new PriceListVersionTab());
  }

  /**
   * Create a new Price List header.
   *
   * @param data
   *          The data of the Price List that will be created.
   */
  public void create(PriceListData data) {
    clickCreateNewRecord();
    if (data.getDataField("name") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_NAME)
          .sendKeys(data.getDataField("name").toString());
    }
    if (data.getDataField("salesPriceList") != null) {
      WebElement checkBoxSalesPriceList = SeleniumSingleton.INSTANCE
          .findElementById(CHECK_BOX_IS_SALES);
      if (!data.getDataField("salesPriceList").equals(checkBoxSalesPriceList.isSelected())) {
        checkBoxSalesPriceList.click();
      }
    }
    if (data.getDataField("currency") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_CURRENCY))
          .selectByVisibleText(data.getDataField("currency").toString());
    }
    clickSave();
  }
}
