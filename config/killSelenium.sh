#!/bin/bash
echo "Seek and destroy selenium-server processes"
#try to normal stop
SPID="$(jps -l | grep selenium-server | cut -f1 -d' ')"
[ "$SPID" != "" ] && echo "Selenium server(s) detected, will try to stop them. PIDS: $SPID"
for PID in $SPID; do kill $PID; done

sleep 3
# ensure to kill
SPID="$(jps -l | grep selenium-server | cut -f1 -d' ')"
[ "$SPID" != "" ] && echo "Stuck Selenium server(s) detected, will try to kill them. PIDS: $SPID"
for PID in $SPID; do kill -9 $PID; done


echo "Selenium-server processes killing finished."
