/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.security.role;

import com.openbravo.test.integration.erp.data.generalsetup.security.role.RoleData;
import com.openbravo.test.integration.erp.gui.general.security.role.classic.RoleTab;
import com.openbravo.test.integration.erp.gui.general.security.role.classic.RoleWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the User tab of OpenbravoERP.
 *
 * @author Pablo Lujan
 */
class RoleClassicScript {

  static void verify(MainPage mainPage, RoleData roleTabData) {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }

  /**
   * Create a record.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param roleData
   *          Record data.
   */
  static void create(MainPage mainPage, RoleData roleData) {
    final RoleWindow roleWindow = new RoleWindow();
    mainPage.openView(roleWindow);

    final RoleTab roleTab = roleWindow.selectRoleTab();
    roleTab.create(roleData);
    roleTab.verifySave();
  }

  /**
   * Grant access to the role.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param grantAccessData
   *          The grant access data.
   */
  static void grantAccess(MainPage mainPage/*
                                            * , GrantAccessPopUpData grantAccessData
                                            */) {
    throw new UnsupportedOperationException();
    // RoleWindow roleWindow = new RoleWindow();
    // mainPage.openView(roleWindow);
    //
    // RoleTab roleTab = roleWindow.selectRoleTab();
    // roleTab.grantAccess(grantAccessData);
    // roleWindow.verifyProcessCompletedSuccessfully2();
  }
}
