/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 * 	Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.accounting.setup.fiscalcalendar.classic;

import com.openbravo.test.integration.erp.gui.popups.PopUp;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Period screen
 *
 * @author elopio
 *
 */
public class PeriodTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Period";
  /** The tab level. */
  private static final int LEVEL = 2;

  /* GUI components. */
  /** Identifier of the Period tab */
  public static final String TAB = "tabname130";

  /** Number of the name column */
  public static final int COLUMN_NAME = 2;

  /**
   * Class constructor
   *
   */
  public PeriodTab() {
    super(TITLE, LEVEL, TAB);
  }

  /**
   * Open or close all period documents
   */
  public void openCloseAll() throws OpenbravoERPTestException {
    switchToFormView();

    SeleniumSingleton.INSTANCE.findElementById(BUTTON_PROCESS).click();
    final PopUp openCloseAll = new PopUp();
    openCloseAll.selectPopUpWithOKButton();
    openCloseAll.clickOK();

    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
  }

}
