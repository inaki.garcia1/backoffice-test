/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Pablo Lujan <pablo.lujan@openbravo.com>,
 *  Rowinson Gallego <rwn.gallego@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.common.TestFiles;
import com.openbravo.test.integration.erp.data.ChangePasswordData;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.client.application.gui.ViewLauncher;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Image;

/**
 * Executes and verify actions on OpenbravoERP navigation bar, added by the Navigation Bar
 * Components module.
 *
 * @author elopio
 *
 */
public class NavigationBar {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Registry keys. */
  /** Registry key of the alerts button */
  private static final String REGISTRY_KEY_BUTTON_ALERT = "org.openbravo.client.application.AlertButton";
  /** Registry key of the end session button. */
  private static final String REGISTRY_KEY_BUTTON_QUIT = "org.openbravo.client.application.navigationbarcomponents.QuitButton";

  /** Registry key of the openbravo logo. */
  private static final String REGISTRY_KEY_OPENBRAVO_LOGO = "org.openbravo.client.application.openbravologo";
  /** Registry key of the company logo. */
  private static final String REGISTRY_KEY_COMPANY_LOGO = "org.openbravo.client.application.companylogo";

  /* The path to the images that will be used to compare the ones got from the application */
  private static final String IDENTIFIER_OPENBRAVO_COMMUNITY_FILE = "GetOpenbravoLogo_Community.png";
  private static final String IDENTIFIER_OPENBRAVO_PROFESSIONAL_FILE = "GetOpenbravoLogo_Professional.png";
  private static final String IDENTIFIER_COMPANY_FILE = "ShowImageLogo.png";

  /* Component elements. */
  /** Create new component. */
  private final CreateNew componentCreateNew;
  /** Quick launch component. */
  private final QuickLaunch componentQuickLaunch;
  /** Menu component. */
  private final Menu componentMenu;
  /** The alert button */
  private final Button buttonAlerts;
  /** Help widget component */
  private final HelpWidget componentHelp;
  /** User profile component. */
  private final UserProfile componentUserProfile;
  /** The quit button. */
  private final Button buttonQuit;
  /** The YourCompany logo. */
  private final Image yourCompanyLogo;
  /** The Openbravo logo. */
  private final Image openbravoLogo;

  /**
   * Enumerator with the available view launchers.
   */
  public enum ViewLaunchers {
    /**
     * Menu component.
     */
    MENU,
    /**
     * Quick launch widget.
     */
    QUICK_LAUNCH,
    /**
     * Create new widget.
     */
    CREATE_NEW;
  }

  /**
   * Class constructor
   *
   */
  public NavigationBar() {
    this.componentCreateNew = new CreateNew();
    this.componentQuickLaunch = new QuickLaunch();
    this.componentMenu = new Menu();
    this.buttonAlerts = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ALERT));
    this.componentHelp = new HelpWidget();
    this.componentUserProfile = new UserProfile();
    this.buttonQuit = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_QUIT));
    this.yourCompanyLogo = new Image(TestRegistry.getObjectString(REGISTRY_KEY_COMPANY_LOGO));
    this.openbravoLogo = new Image(TestRegistry.getObjectString(REGISTRY_KEY_OPENBRAVO_LOGO));
  }

  /**
   * Get the create new component.
   *
   * @return the create new component.
   */
  public CreateNew getComponentCreateNew() {
    return componentCreateNew;
  }

  /**
   * Get the quick launch component.
   *
   * @return the quick launch component.
   */
  public QuickLaunch getComponentQuickLaunch() {
    return componentQuickLaunch;
  }

  /**
   * Get the menu component.
   *
   * @return the menu component.
   */
  public Menu getComponentMenu() {
    return componentMenu;
  }

  /**
   * Get the help widget.
   *
   * @return the help widget element.
   */
  public HelpWidget getComponentHelp() {
    return componentHelp;
  }

  /**
   * Get the user profile widget.
   *
   * @return the user profile widget.
   */
  public UserProfile getComponentUserProfile() {
    return componentUserProfile;
  }

  /**
   * Get a view launcher.
   *
   * @param type
   *          The type of the view launcher requested.
   * @return a view launcher.
   */
  public ViewLauncher getViewLauncher(ViewLaunchers type) {
    ViewLauncher launcher = null;
    switch (type) {
      case MENU:
        launcher = componentMenu;
        break;
      case CREATE_NEW:
        launcher = componentCreateNew;
        break;
      case QUICK_LAUNCH:
        launcher = componentQuickLaunch;
        break;
    }
    return launcher;
  }

  /**
   * Open a new record.
   *
   * @param record
   *          The name of the record to create.
   */
  public void createNewRecord(String record) {
    componentCreateNew.createNew(record);
  }

  /**
   * Open a window from quick launcher.
   *
   * @param windowName
   *          The name of the window to open.
   */
  public void quickLaunchWindow(String windowName) {
    componentQuickLaunch.launch(windowName);
  }

  /**
   * Return true if the menu item is present.
   *
   * @param item
   *          The text of the menu item.
   * @return true if the menu item is present.
   */
  public boolean isMenuItemPresent(String item) {
    return componentMenu.isMenuItemPresent(item);
  }

  /**
   * Select an item from the menu.
   *
   * @param items
   *          the path of the item to select.
   */
  public void selectMenuItem(String... items) {
    componentMenu.select(items);
  }

  /**
   * Get the alerts window.
   */
  public void openAlerts() {
    logger.info("Open Alerts.");
    buttonAlerts.smartClientClick();
  }

  /**
   * Get the user name of the Navigation Bar
   *
   * @return the name of the Navigation Bar
   */
  public String getUserName() {
    return componentUserProfile.getUserName();
  }

  /**
   * Verify the displayed user name
   *
   * @param userName
   *          The expected user name.
   */
  public void verifyUserName(String userName) {
    logger.debug("Verifying that user name is '{}'.", userName);
    componentUserProfile.verifyUserName(userName);
  }

  /**
   * End the user session.
   */
  public void quit() {
    logger.debug("Logging out.");
    buttonQuit.smartClientClick();
  }

  /**
   * Change the user profile.
   *
   * @param newProfile
   *          The new profile of the user.
   */
  public void changeProfile(ProfileData newProfile) {
    componentUserProfile.changeProfile(newProfile);
  }

  /**
   * Close the user profile.
   *
   */
  public void closeProfile() {
    componentUserProfile.close();
  }

  /**
   * Assert the user profile information.
   *
   * @param expectedProfileData
   *          The expected profile data of the user.
   */
  public void assertProfile(ProfileData expectedProfileData) {
    componentUserProfile.assertProfile(expectedProfileData);
  }

  /**
   * Change the user password.
   *
   * @param passwordData
   *          The new password data.
   */
  public void changePassword(ChangePasswordData passwordData) {
    componentUserProfile.changePassword(passwordData);
  }

  /**
   * Checks if the Company logo is present.
   *
   */
  public void verifyYourCompanyLogo() {
    if (yourCompanyLogo.isPresent()) {
      yourCompanyLogo.verifyImage(TestFiles.getRelativeFilePath(IDENTIFIER_COMPANY_FILE));
    }
  }

  /**
   * Checks if the Community logo is present.
   *
   */
  public void verifyCommunityLogo() {
    if (openbravoLogo.isPresent()) {
      openbravoLogo.verifyImage(TestFiles.getRelativeFilePath(IDENTIFIER_OPENBRAVO_COMMUNITY_FILE));
    }
  }

  /**
   * Checks if the Community logo is present.
   *
   */
  public void verifyProfessionalLogo() {
    if (openbravoLogo.isPresent()) {
      openbravoLogo
          .verifyImage(TestFiles.getRelativeFilePath(IDENTIFIER_OPENBRAVO_PROFESSIONAL_FILE));
    }
  }

}
