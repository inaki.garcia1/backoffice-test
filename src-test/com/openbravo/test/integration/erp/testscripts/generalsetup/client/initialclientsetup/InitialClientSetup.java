/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Luján <plu@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Arun Kumar <arun.kumar@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.client.initialclientsetup;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.generalsetup.client.initialclientsetup.InitialClientSetupData;
import com.openbravo.test.integration.erp.gui.general.client.initialclientsetup.InitialClientSetupProcess;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Test the Initial Client Setup form of Openbravo.
 *
 * @author Leo Arias
 */
public class InitialClientSetup {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Parameter to be replaced by the client name on the output file log. */
  @SuppressWarnings("unused")
  private static final String PARAMETER_CLIENT = "\'client\'";
  /** Parameter to be replaced by the admin user name on the output file log. */
  @SuppressWarnings("unused")
  private static final String PARAMETER_ADMIN_USER_NAME = "\'adminUserName\'";

  /**
   * Setup the Initial Client.
   *
   * @param mainPage
   *          The application main page.
   * @param initialClientSetupData
   *          The initial client setup data.
   * @param outputLog
   *          The process output log. TODO should the output log be a parameter?
   * @throws IOException
   */
  public static void setupInitialClient(MainPage mainPage,
      InitialClientSetupData initialClientSetupData, String outputLog) throws IOException {
    InitialClientSetupProcess initialClientSetupProcess = new InitialClientSetupProcess();
    mainPage.openView(initialClientSetupProcess);

    logger.info("Set up the client:\n{}", initialClientSetupData::toString);
    initialClientSetupProcess.setUp(initialClientSetupData);
    // FIXME We are not verifying that accounting file can't be selected.
    initialClientSetupProcess.verifyProcessCompletedSuccessfully();

    // logger.info("Verify the output log.");
    // String finalOutputLog = outputLog.replaceAll(PARAMETER_CLIENT,
    // initialClientSetupData.getClient());
    // finalOutputLog = outputLog.replaceAll(PARAMETER_ADMIN_USER_NAME,
    // initialClientSetupData.getClientUserName());
    // TODO see issue 15335 - https://issues.openbravo.com/view.php?id=15335
    // initialClientSetupProcess.verifyResult(finalOutputLog);
  }
}
