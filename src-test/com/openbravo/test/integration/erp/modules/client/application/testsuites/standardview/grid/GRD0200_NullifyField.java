/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testsuites.standardview.grid;

import java.util.Arrays;
import java.util.Collection;
import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.masterdata.productsetup.unitofmeasure.UnitOfMeasureData;
import com.openbravo.test.integration.erp.gui.masterdata.productsetup.unitofmeasure.UnitOfMeasureTab;
import com.openbravo.test.integration.erp.gui.masterdata.productsetup.unitofmeasure.UnitOfMeasureWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Asserts it is possible to set a field as null editing from grid.
 *
 * See https://issues.openbravo.com/view.php?id=28454
 *
 * @author alostale
 *
 */
@RunWith(Parameterized.class)
public class GRD0200_NullifyField extends OpenbravoERPTest {

  private UnitOfMeasureData originalRecord;
  private UnitOfMeasureData editedRecord;

  public GRD0200_NullifyField(UnitOfMeasureData originalRecord, UnitOfMeasureData editedRecord) {
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
    this.originalRecord = originalRecord;
    this.editedRecord = editedRecord;
  }

  @Parameters
  public static Collection<Object[]> uomValues() {
    Random randomGenerator = new Random();
    int originalRandomNumber = randomGenerator.nextInt();

    Object[][] data = new Object[][] { {
        // original record
        new UnitOfMeasureData.Builder().eDICode("QA")
            .name("Test UOM " + originalRandomNumber)
            .symbol("aa")
            .build(),

        // edited record: simply nullify symbol
        new UnitOfMeasureData.Builder().symbol("").build() } };
    return Arrays.asList(data);
  }

  @Test
  public void shouldBePossibleToNullify() {
    UnitOfMeasureWindow unitOfMeasureWindow = new UnitOfMeasureWindow();
    mainPage.openView(unitOfMeasureWindow);
    @SuppressWarnings("unchecked")
    GeneratedTab<UnitOfMeasureData> uomTab = (GeneratedTab<UnitOfMeasureData>) unitOfMeasureWindow
        .selectTab(UnitOfMeasureTab.IDENTIFIER);

    uomTab.createOnGridAndSave(originalRecord);
    uomTab.assertSaved();

    uomTab.select(originalRecord);
    uomTab.editOnGrid(editedRecord);
    uomTab.assertSaved();
    uomTab.assertData(editedRecord);
  }
}
