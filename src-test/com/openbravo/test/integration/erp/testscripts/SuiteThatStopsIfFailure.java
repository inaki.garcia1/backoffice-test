/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts;

/**
 * This class extends Suite.java, stops if a failure happens and will not continue with the rest of the tests of the Suite.
 * @author lorenzo.fidalgo
 *
 */

import org.junit.runner.notification.RunNotifier;
import org.junit.runner.notification.StoppedByUserException;
import org.junit.runners.Suite;
import org.junit.runners.model.InitializationError;

import com.openbravo.test.integration.util.ConfigurationProperties;

public class SuiteThatStopsIfFailure extends Suite {

  public SuiteThatStopsIfFailure(Class<?> klass, Class<?>[] suiteClasses)
      throws InitializationError {
    super(klass, suiteClasses);
  }

  public SuiteThatStopsIfFailure(Class<?> klass) throws InitializationError {
    super(klass, klass.getAnnotation(SuiteClasses.class).value());
  }

  /*
   * (non-Javadoc)
   *
   * @see org.junit.runners.ParentRunner#run(org.junit.runner.notification.RunNotifier)
   */
  @Override
  public void run(RunNotifier runNotifier) {
    if (ConfigurationProperties.INSTANCE.getStopSuiteWhenFailure() != 'f') {
      runNotifier.addListener(new FailureListener(runNotifier));
    }
    try {
      super.run(runNotifier);
    } catch (StoppedByUserException sbue) {
      // This catch avoids log that would not be useful for developers to track failures
      // Specifically the following exception:
      // "Exception in thread "main" org.junit.runner.notification.StoppedByUserException".
    }
  }
}
