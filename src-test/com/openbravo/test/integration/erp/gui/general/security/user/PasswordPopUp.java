/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 * 	Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.security.user;

import com.openbravo.test.integration.erp.gui.popups.PopUp;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions on user password PopUp
 *
 * @author elopio
 *
 */
public class PasswordPopUp extends PopUp {
  // TODO should we name it as Password or as EncryptedButtons?

  /** Identifier of the password text field */
  private static final String TEXT_FIELD_PASSWORD = "inpValue";

  /**
   * Class constructor
   */
  public PasswordPopUp() {
    super();
  }

  /**
   * Select the password PopUp
   */
  public void select() {
    super.selectPopUpWithOKButton();
  }

  /**
   * Set the password on User password PopUp
   *
   * @param password
   *          password that will be entered
   */
  public void setPassword(String password) {
    select();
    SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_PASSWORD).sendKeys(password);
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_OK).click();
  }

}
