/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Lujan <pablo.lujan@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.gui;

import org.junit.Test;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Test Logging in and Logging out processes.
 *
 * @author Leo Arias
 */
public abstract class LoginAndLogout extends OpenbravoERPTest {

  /**
   * Class constructor.
   *
   * @param logInData
   */
  public LoginAndLogout(LogInData logInData) {
    this.logInData = logInData;
  }

  /**
   * Test the log in and log out on the application.
   */
  @Test
  public void userShouldLoginAndLogout() {
    // Do nothing. The base test class will execute the login and logout, and do the corresponding
    // assertions.
  }
}
