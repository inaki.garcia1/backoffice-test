/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2018 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *   Nono Carballo <f.carballo@nectus.com>
 *************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.aum;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductData;
import com.openbravo.test.integration.erp.gui.masterdata.product.AUMTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.masterdata.product.Product;

/**
 *
 * Class for Test AUMa040
 *
 * @author Rafael Queralta Pozo
 *
 */
@RunWith(Parameterized.class)
public class AUMa040RemoveAUMProduct extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Data for Test */

  ProductData selectProductFGA;
  ProductData selectProductFGB;
  ProductData selectProductRMA;
  ProductData selectProductTB;
  ProductData selectProductSB;

  public AUMa040RemoveAUMProduct(ProductData selectProductFGA, ProductData selectProductFGB,
      ProductData selectProductRMA, ProductData selectProductTB, ProductData selectProductSB) {
    super();
    this.selectProductFGA = selectProductFGA;
    this.selectProductFGB = selectProductFGB;
    this.selectProductRMA = selectProductRMA;
    this.selectProductSB = selectProductTB;
    this.selectProductTB = selectProductSB;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  @Parameters
  public static Collection<Object[]> returnToVendorValues() {
    Object[][] data = new Object[][] { { new ProductData.Builder().name("Final good A").build(),
        new ProductData.Builder().name("Final good B").build(),
        new ProductData.Builder().name("Raw material A").build(),
        new ProductData.Builder().name("Tennis ball").build(),
        new ProductData.Builder().name("Soccer Ball").build() } };
    return Arrays.asList(data);
  }

  @Test
  public void AUMa040test() throws ParseException {
    logger.debug("** Start test case [AUMa040] Remove AUM to product **");

    ProductWindow productWindow = Product.ProductTab.open(mainPage);
    ProductTab productTab = productWindow.selectProductTab();
    productTab.filter(selectProductFGA);

    AUMTab aumTab = productWindow.selectAUMTab();
    aumTab.deleteMultipleRecordsOneCheckboxOnGrid();

    productTab.filter(selectProductFGB);
    aumTab = productWindow.selectAUMTab();
    aumTab.deleteMultipleRecordsOneCheckboxOnGrid();

    productTab.filter(selectProductRMA);
    aumTab = productWindow.selectAUMTab();
    aumTab.deleteMultipleRecordsOneCheckboxOnGrid();

    ProductData setProductNotReturnable = new ProductData.Builder().build();
    setProductNotReturnable.addDataField("returnable", false);

    productTab.filter(selectProductTB);
    productTab.editOnForm(setProductNotReturnable);
    productTab.assertSaved();
    aumTab = productWindow.selectAUMTab();
    aumTab.deleteMultipleRecordsOneCheckboxOnGrid();

    productTab.filter(selectProductSB);
    productTab.editOnForm(setProductNotReturnable);
    productTab.assertSaved();
    aumTab = productWindow.selectAUMTab();
    aumTab.deleteMultipleRecordsOneCheckboxOnGrid();

    logger.debug("** End test case [AUMa040] Remove AUM to product **");
  }

}
