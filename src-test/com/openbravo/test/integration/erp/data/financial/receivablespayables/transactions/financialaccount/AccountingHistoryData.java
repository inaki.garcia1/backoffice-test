/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;

/**
 *
 * Class for AccountingHistoryData
 *
 * @author plujan
 *
 */
public class AccountingHistoryData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the accountingSchema value
     *
     * Description: The structure used in accounting including costing methods currencies and the
     * calendar.
     *
     * @param value
     *          The accountingSchema value.
     * @return The builder for this class.
     */
    public Builder accountingSchema(String value) {
      this.dataFields.put("accountingSchema", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the accountingEntryDescription value
     *
     * Description: Description of the accounting entry
     *
     * @param value
     *          The accountingEntryDescription value.
     * @return The builder for this class.
     */
    public Builder accountingEntryDescription(String value) {
      this.dataFields.put("accountingEntryDescription", value);
      return this;
    }

    /**
     * Set the financialAccount value
     *
     * Description: Financial account used to deposit / withdrawal money such as bank accounts or
     * petty cash
     *
     * @param value
     *          The financialAccount value.
     * @return The builder for this class.
     */
    public Builder financialAccount(String value) {
      this.dataFields.put("financialAccount", value);
      return this;
    }

    /**
     * Set the asset value
     *
     * Description: An item which is owned and exchangeable for cash.
     *
     * @param value
     *          The asset value.
     * @return The builder for this class.
     */
    public Builder asset(String value) {
      this.dataFields.put("asset", value);
      return this;
    }

    /**
     * Set the value value
     *
     * Description: Value of the account used in the entry.
     *
     * @param value
     *          The value value.
     * @return The builder for this class.
     */
    public Builder value(String value) {
      this.dataFields.put("value", value);
      return this;
    }

    /**
     * Set the debit value
     *
     * Description: The amount debited to an account converted to the organization default currency.
     *
     * @param value
     *          The debit value.
     * @return The builder for this class.
     */
    public Builder debit(String value) {
      this.dataFields.put("debit", value);
      return this;
    }

    /**
     * Set the period value
     *
     * Description: A specified time period.
     *
     * @param value
     *          The period value.
     * @return The builder for this class.
     */
    public Builder period(String value) {
      this.dataFields.put("period", value);
      return this;
    }

    /**
     * Set the accountingDate value
     *
     * Description: The date this transaction is recorded for in the general ledger.
     *
     * @param value
     *          The accountingDate value.
     * @return The builder for this class.
     */
    public Builder accountingDate(String value) {
      this.dataFields.put("accountingDate", value);
      return this;
    }

    /**
     * Set the financialAccountTransaction value
     *
     * Description: Financial account transaction
     *
     * @param value
     *          The financialAccountTransaction value.
     * @return The builder for this class.
     */
    public Builder financialAccountTransaction(String value) {
      this.dataFields.put("financialAccountTransaction", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the credit value
     *
     * Description: The amount credited to an account converted to the organization default
     * currency.
     *
     * @param value
     *          The credit value.
     * @return The builder for this class.
     */
    public Builder credit(String value) {
      this.dataFields.put("credit", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the accountElement value
     *
     * Description: A identification code for an account type.
     *
     * @param value
     *          The accountElement value.
     * @return The builder for this class.
     */
    public Builder accountElement(String value) {
      this.dataFields.put("accountElement", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public AccountingHistoryData build() {
      return new AccountingHistoryData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private AccountingHistoryData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
