/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.product;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.AccountSelectorData;

/**
 *
 * Class for AccountingData
 *
 * @author plujan
 *
 */
public class AccountingData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the productExpense value
     *
     * Description: An account number in the general ledger to mark transactions with this item.
     *
     * @param value
     *          The productExpense value.
     * @return The builder for this class.
     */
    public Builder productExpense(AccountSelectorData value) {
      this.dataFields.put("productExpense", value);
      return this;
    }

    /**
     * Set the invoicePriceVariance value
     *
     * Description: The Invoice Price Variance is used reflects the difference between the current
     * Costs and the Invoice Price.
     *
     * @param value
     *          The invoicePriceVariance value.
     * @return The builder for this class.
     */
    public Builder invoicePriceVariance(AccountSelectorData value) {
      this.dataFields.put("invoicePriceVariance", value);
      return this;
    }

    /**
     * Set the productRevenue value
     *
     * Description: An account number in the general ledger to mark transactions with this product.
     *
     * @param value
     *          The productRevenue value.
     * @return The builder for this class.
     */
    public Builder productRevenue(AccountSelectorData value) {
      this.dataFields.put("productRevenue", value);
      return this;
    }

    /**
     * Set the fixedAsset value
     *
     * Description: A tangible asset used for business without expectation of sale in the coming
     * fiscal year.
     *
     * @param value
     *          The fixedAsset value.
     * @return The builder for this class.
     */
    public Builder fixedAsset(AccountSelectorData value) {
      this.dataFields.put("fixedAsset", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(String value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public AccountingData build() {
      return new AccountingData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private AccountingData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
