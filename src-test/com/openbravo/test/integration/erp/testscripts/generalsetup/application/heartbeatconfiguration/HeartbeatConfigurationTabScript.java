/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.application.heartbeatconfiguration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.generalsetup.application.heartbeatconfiguration.HeartbeatConfigurationData;
import com.openbravo.test.integration.erp.gui.general.application.heartbeatconfiguration.HeartbeatConfigurationPopUp;
import com.openbravo.test.integration.erp.gui.general.application.heartbeatconfiguration.HeartbeatConfigurationTab;
import com.openbravo.test.integration.erp.gui.general.application.heartbeatconfiguration.HeartbeatConfigurationWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on the Heartbeat Configuration tab of OpenbravoERP.
 *
 * @author Pablo Lujan
 */
public class HeartbeatConfigurationTabScript
    extends SmokeTabScript<HeartbeatConfigurationData, HeartbeatConfigurationTab>
    implements SmokeWindowScript<HeartbeatConfigurationWindow> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public HeartbeatConfigurationTabScript(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Open the Heartbeat Configuration window.
   *
   * @return this Heartbeat Configuration test script.
   */
  @Override
  public HeartbeatConfigurationTabScript open() {
    HeartbeatConfigurationWindow window = new HeartbeatConfigurationWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the Heartbeat Configuration tab.
   *
   * Before this, the Heartbeat Configuration window must be opened.
   *
   * @return the Heartbeat Configuration tab GUI object.
   */
  @Override
  protected HeartbeatConfigurationTab selectTab() {
    HeartbeatConfigurationWindow window = (HeartbeatConfigurationWindow) mainPage
        .getView(HeartbeatConfigurationWindow.TITLE);
    mainPage.getTabSetMain().selectTab(HeartbeatConfigurationWindow.TITLE);
    Sleep.trySleep();
    HeartbeatConfigurationTab heartbeatConfigurationTab = (HeartbeatConfigurationTab) window
        .getTopLevelTab();
    heartbeatConfigurationTab.waitUntilSelected();
    return heartbeatConfigurationTab;
  }

  /**
   * Enable the heartbeat.
   *
   * @return true if the heartbeat was enabled. Otherwise, false.
   */
  public boolean enableHeartbeat() {
    HeartbeatConfigurationTab tab = selectTab();
    // TODO we are not verifying that both buttons are displayed.
    if (!tab.isHeartbeatEnabled()) {
      logger.info("Enable Heartbeat.");
      tab.enableHeartbeat();
      final HeartbeatConfigurationPopUp heartbeatConfigurationPopUp = new HeartbeatConfigurationPopUp();
      heartbeatConfigurationPopUp.assertProcessCompletedSuccessfully();
      mainPage.selectWindow();
      return true;
    } else {
      logger.info("Heartbeat is already enabled.");
      return false;
    }
  }

  /**
   * Disable the heartbeat.
   *
   * @return true if the heartbeat was disabled. Otherwise, false.
   */
  public boolean disableHeartbeat() {
    final HeartbeatConfigurationTab tab = selectTab();
    if (tab.isHeartbeatEnabled()) {
      logger.info("Disable Heartbeat.");
      tab.disableHeartbeat();
      final HeartbeatConfigurationPopUp heartbeatConfigurationPopUp = new HeartbeatConfigurationPopUp();
      heartbeatConfigurationPopUp.assertProcessCompletedSuccessfully();
      mainPage.selectWindow();
      return true;
    } else {
      logger.info("Heartbeat is already disabled.");
      return false;
    }
  }

  /**
   * Asserts that Heartbeat is disabled.
   */
  public void assertDisabled() {
    final HeartbeatConfigurationTab tab = selectTab();
    // TODO: Enrich the verification. Check that proper buttons are displayed.
    tab.assertDisabledStatus();
  }

  /**
   * Assert that Heartbeat is enabled.
   */
  public void assertEnabled() {
    final HeartbeatConfigurationTab tab = selectTab();
    // TODO: Enrich the verification. Check that proper buttons are displayed.
    tab.assertEnabledStatus();
  }
}
