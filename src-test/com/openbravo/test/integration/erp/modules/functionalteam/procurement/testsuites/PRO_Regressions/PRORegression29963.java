/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.procurement.testsuites.PRO_Regressions;

import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test regression 29963
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class PRORegression29963 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineData;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;
  AddPaymentPopUpData addPaymentVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData2;

  /**
   * Class constructor.
   *
   */
  public PRORegression29963(PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineData,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      AddPaymentPopUpData addPaymentVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData2) {
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineData = purchaseInvoiceLineData;
    this.purchaseInvoiceLineVerificationData = purchaseInvoiceLineVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData2 = completedPurchaseInvoiceHeaderVerificationData2;
    logInData = new LogInData.Builder().userName("Openbravo").password("openbravo").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new PurchaseInvoiceHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Bebidas Alegres, S.L.").build())
            .build(),
        new PurchaseInvoiceHeaderData.Builder().organization("F&B España - Región Norte")
            .transactionDocument("AP Invoice")
            .partnerAddress(".Barcelona, C\\Mayor, 23")
            .paymentTerms("30 days")
            .paymentMethod("Transferencia")
            .priceList("Tarifa Bebidas Alegres")
            .build(),
        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Cerveza Ale 0,5L")
                .priceListVersionName("Tarifa Bebidas Alegres")
                .build())
            .invoicedQuantity("25")
            .build(),
        new PurchaseInvoiceLinesData.Builder().uOM("Unit")
            .unitPrice("1.36")
            .listPrice("1.36")
            .tax("Adquisiciones IVA 21%")
            .lineNetAmount("34.00")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("41.14")
            .summedLineAmount("34.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Bebidas Alegres, S.L.")
            .fin_paymentmethod_id("Transferencia")
            .fin_financial_account_id("Cuenta de Banco - EUR")
            .c_currency_id("EUR")
            .actual_payment("41.14")
            .expected_payment("41.14")
            .amount_gl_items("0.00")
            .amount_inv_ords("41.14")
            .total("41.14")
            .difference("0.00")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("41.14")
            .summedLineAmount("34.00")
            .currency("EUR")
            .paymentComplete(true)
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 29963 - Purchase flow
   *
   * @throws ParseException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void PRORegression29963Test() throws ParseException, OpenbravoERPTestException {
    logger.info(
        "** Start of test case [PRORegression29963] Test regression 29963 - Purchase flow. **");

    mainPage.getNavigationBar()
        .changeProfile(
            new ProfileData.Builder().role("F&B España, S.A - Finance - F&B International Group")
                .build());
    Sleep.smartWaitButtonEnabled("UINAVBA_RecentLaunchList_BUTTON", 200);
    Sleep.trySleep(10000);

    PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    String supplierNo = purchaseInvoice.getData("orderReference").toString();
    PurchaseInvoice.Lines purchaseInvoiceLines = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLines.create(purchaseInvoiceLineData);
    purchaseInvoiceLines.assertSaved();
    purchaseInvoiceLines.assertData(purchaseInvoiceLineVerificationData);
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData);

    purchaseInvoice.post();
    purchaseInvoice.open();
    assertTrue(purchaseInvoice.isPosted());

    AddPaymentProcess addPaymentProcess = purchaseInvoice.openAddPayment();
    addPaymentProcess.process("Invoices", supplierNo, "Process Made Payment(s) and Withdrawal",
        addPaymentVerificationData);
    purchaseInvoice.assertPaymentCreatedSuccessfully();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData2);

    logger
        .info("** End of test case [PRORegression29963] Test regression 29963 - Purchase flow. **");
  }

  @AfterClass
  public static void tearDown() {
    SeleniumSingleton.INSTANCE.quit();
    OpenbravoERPTest.seleniumStarted = false;
    OpenbravoERPTest.forceLoginRequired();
  }
}
