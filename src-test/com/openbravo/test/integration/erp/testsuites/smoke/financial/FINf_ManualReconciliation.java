/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testsuites.smoke.financial;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount.Transaction;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.util.OBDate;

/**
 * Execute the Setup Client and Organization flow of the smoke test suite.
 *
 * @author Pablo Lujan
 */
@RunWith(Parameterized.class)
public class FINf_ManualReconciliation extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  /* Data for [FINf010] Create Purchase Invoice */
  /** The purchase invoice header data. */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  /** The data to verify the purchase invoice header. */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  /** The purchase invoice lines data. */
  PurchaseInvoiceLinesData purchaseInvoiceLinesData;
  /** The data to verify the purchase invoice lines creation. */
  PurchaseInvoiceLinesData purchaseInvoiceLinesVerificationData;
  /** The data to verify the completed purchase invoice header. */
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderData;
  /** The data to verify the payment out plan. */
  PaymentPlanData purchaseInvoicePaymentOutPlanData;
  /** The data to verify the payment out details. */
  PaymentDetailsData purchaseInvoicePaymentDetailsData;
  /** The data to verify the payment out. */
  PaymentOutHeaderData paymentOutData;
  /** The data to select the account. */
  AccountData financialAccountData;
  /** The data to verify the transactions data. */
  TransactionsData financialAccountTransactionsData;

  /**
   * Class constructor.
   *
   * @param purchaseInvoiceHeaderData
   *          The purchase invoice header data.
   * @param purchaseInvoiceHeaderVerificationData
   *          The data to verify the purchase invoice header.
   * @param purchaseInvoiceLinesData
   *          The purchase invoice lines data.
   * @param purchaseInvoiceLinesVerificationData
   *          The data to verify the purchase invoice lines creation.
   * @param completedPurchaseInvoiceHeaderData
   *          The data to verify the completed purchase invoice header.
   * @param purchaseInvoicePaymentOutPlanData
   *          The data to verify the payment out plan data.
   * @param purchaseInvoicePaymentDetailsData
   *          The data to verify the payment out details.
   * @param paymentOutData
   *          The data to verify the payment out.
   * @param financialAccountData
   *          The data to select the account.
   * @param financialAccountTransactionsData
   *          The data to verify the transactions data.
   */
  public FINf_ManualReconciliation(PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLinesData,
      PurchaseInvoiceLinesData purchaseInvoiceLinesVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderData,
      PaymentPlanData purchaseInvoicePaymentOutPlanData,
      PaymentDetailsData purchaseInvoicePaymentDetailsData, PaymentOutHeaderData paymentOutData,
      AccountData financialAccountData, TransactionsData financialAccountTransactionsData) {
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLinesData = purchaseInvoiceLinesData;
    this.purchaseInvoiceLinesVerificationData = purchaseInvoiceLinesVerificationData;
    this.completedPurchaseInvoiceHeaderData = completedPurchaseInvoiceHeaderData;
    this.purchaseInvoicePaymentOutPlanData = purchaseInvoicePaymentOutPlanData;
    this.purchaseInvoicePaymentDetailsData = purchaseInvoicePaymentDetailsData;
    this.paymentOutData = paymentOutData;
    this.financialAccountData = financialAccountData;
    this.financialAccountTransactionsData = financialAccountTransactionsData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   * @throws IOException
   *
   */
  @Parameters
  public static Collection<DataObject[]> paymentProposalValues() throws IOException {
    DataObject[][] data = new DataObject[][] { {
        // Parameters for [FINd010] Create Purchase Invoice
        new PurchaseInvoiceHeaderData.Builder().transactionDocument("AP Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .paymentMethod("6 (Spain)")
            .build(),
        new PurchaseInvoiceHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .invoiceDate(OBDate.CURRENT_DATE)
            .priceList("Purchase")
            .paymentMethod("6 (Spain)")
            .paymentTerms("90 days")
            .build(),
        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("RMC").build())
            .invoicedQuantity("15.2")
            .build(),
        new PurchaseInvoiceLinesData.Builder().uOM("Bag")
            .listPrice("2.00")
            .unitPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("30.40")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().paymentComplete(true)
            .summedLineAmount("30.40")
            .grandTotalAmount("33.44")
            .documentStatus("Completed")
            .build(),
        new PaymentPlanData.Builder().dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("6 (Spain)")
            .expected("33.44")
            .paid("33.44")
            .outstanding("0.00")
            .lastPaymentDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),
        new PaymentDetailsData.Builder().paymentDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("6 (Spain)")
            .paid("33.44")
            .build(),
        new PaymentOutHeaderData.Builder().status("Withdrawn not Cleared").build(),
        new AccountData.Builder().name("Spain Cashbook").build(),
        new TransactionsData.Builder().build() } };
    return Arrays.asList(data);
  }

  /**
   * This test case proves that you can create the payment proposal.
   *
   * @throws IOException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void reconciliationShouldBeReconciliated() throws IOException, OpenbravoERPTestException {
    logger.info("** Start of test case [FINf010] Create Purchase Invoice. **");
    final PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    PurchaseInvoice.Lines purchaseInvoiceLines = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLines.create(purchaseInvoiceLinesData);
    purchaseInvoiceLines.assertSaved();
    purchaseInvoiceLines.assertData(purchaseInvoiceLinesVerificationData);
    purchaseInvoice.complete();
    String documentNumber = (String) purchaseInvoice.getData("documentNo");
    purchaseInvoice
        .assertProcessCompletedSuccessfullyContentMatch("Payment No. .* has been created");
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderData);
    final PurchaseInvoice.PaymentOutPlan purchaseInvoicePaymentOutPlan = purchaseInvoice.new PaymentOutPlan(
        mainPage);
    purchaseInvoicePaymentOutPlan.assertCount(1);
    purchaseInvoicePaymentOutPlan.assertData(purchaseInvoicePaymentOutPlanData);
    PurchaseInvoice.PaymentOutPlan.PaymentOutDetails purchaseInvoicePaymentOutDetails = purchaseInvoicePaymentOutPlan.new PaymentOutDetails(
        mainPage);
    purchaseInvoicePaymentOutDetails.assertCount(1);
    purchaseInvoicePaymentOutDetails.assertData(purchaseInvoicePaymentDetailsData);
    final PaymentOut paymentOut = new PaymentOut(mainPage).open();
    String description = String.format("Invoice No.: %s", documentNumber);
    paymentOut.select(new PaymentOutHeaderData.Builder().description(description).build());
    paymentOut.assertData(paymentOutData);
    final FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(financialAccountData);
    final Transaction financialAccountTransactions = financialAccount.new Transaction(mainPage);
    financialAccountTransactions.select((TransactionsData) financialAccountTransactionsData
        .addDataField("description", description));
    logger.info("** End of test case [FINf010] Create Purchase Invoice. **");
    logger.info("** Start of test case [FINf020] Manual reconciliation. **");
    financialAccount.select(financialAccountData);
    financialAccount.reconcile("-33.44", false, description);
    // List<String> capturingGroups = financialAccount
    // .assertProcessCompletedSuccessfullyContentMatch("Reconciliation No\\.: (.*)");
    // String reconciliationNumber = capturingGroups.get(1);
    logger.info("** End of test case [FINf020] Manual reconciliation. **");
  }
}
